//
//  MessagesVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/26/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "MessagesVC.h"
#import "NewsVC.h"
#import "UIColor+CL.h"
#import "EventVC.h"
#import "SWRevealViewController.h"
#import "LearningUploadVC.h"
#import "DeshboardVC.h"
#import "UIColor+CL.h"
#import "WYPopoverController.h"
#import "Constants.h"
#import "DSCircularLayout.h"
#import "DSCollectionViewCell.h"
#import "TeamBuild1VC.h"
#import "LearningCell.h"
#import "MessageCell.h"
#import "RequestCell.h"
#import "SVPullToRefresh.h"
#import "MessagePopVC.h"
#import "NewHomeVC.h"
#import "AllNotificationVC.h"


#import "MessageChat.h"


@interface MessagesVC ()

@end

@implementation MessagesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    // Do any additional setup after loading the view.
    page_no=@"0";
    
    _btn_request.layer.backgroundColor=[UIColor clearColor].CGColor;
    _btn_msg.layer.backgroundColor=[UIColor colorWithHex:0x01B5A9].CGColor;

    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, self.btn_request.frame.size.height - 1, self.btn_request.frame.size.width, 1.0f);
    bottomBorder.backgroundColor = [UIColor colorWithHex:0xADADAD].CGColor;
    [self.btn_request.layer addSublayer:bottomBorder];

    
    [self customSetup];
    
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];
    
    
    [self CallGetMSG];
    [self CallGetREQ];
    
    if ([_isfronN isEqualToString:@"1"]) {
        [self CallRequestMethod];
    }
    else
    {
        [self CallMessageMethod];
    }
    
    _txt_req_count.textColor=[UIColor blackColor];
    _txt_msg_count.textColor=[UIColor whiteColor];
    
    arrUnread=[[NSMutableArray alloc]init];

    
    _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
    _lbl_notifi_count.clipsToBounds=YES;
    _lbl_notifi_count.text=theAppDelegate.n_Count;
    
    if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
        _lbl_notifi_count.hidden=YES;
    }
    else
    {
        _lbl_notifi_count.hidden=NO;
    }
    
}
- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if (IsIphone7 || IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = 145;
    }
    else{
        self.revealViewController.rearViewRevealWidth = 145;
    }
}
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self customSetup];
}

-(void)CallSearchMethod1
{
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    
    
    [AddPost setValue:[NSString stringWithFormat:@"%@",page_no] forKey:@"page_no"];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@get_message_people_list",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        page_no=[responseObject valueForKey:@"page_no"];
        
        if ([[responseObject valueForKey:@"data"] isEqual:[NSNull null]]) {
            //mutDict=[[NSMutableArray alloc] init];
            [_tbl_message.infiniteScrollingView stopAnimating];
        }
        else
        {
            int64_t delayInSeconds = 2.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                //[_tbl_event beginUpdates];
                
                NSArray *arr = [responseObject valueForKey:@"data"];
                if (arr.count<1) {
                    //_tbl_message.showsInfiniteScrolling = NO;
                }
                NSMutableArray *newarr = [[NSMutableArray alloc] init];
                NSLog(@"---%@",newarr);
                for (NSString *str in mutDict) {
                    [newarr addObject:str];
                }
                for (int i=0; i<arr.count; i++) {
                    //[mutDict addObject:arr[i]];
                    [newarr addObject:arr[i]];
                }
                NSLog(@"---%@",newarr);
                mutDict = [[NSMutableArray alloc] init];
                for (NSString*str  in newarr) {
                    [mutDict addObject:str];
                }
                [_tbl_message.infiniteScrollingView stopAnimating];
                [_tbl_message reloadData];
                
            });
            
        }
        
        //[_tbl_event reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    
    
}

#pragma mark - GetAllPosts
-(void)CallGetMSG
{
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    [manager POST:[NSString stringWithFormat:@"%@get_message_people_list",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        
        
        mutDict=[responseObject valueForKey:@"data"];
        page_no=[responseObject valueForKey:@"page_no"];
        
      /*  for (int i=0; i<mutDict.count; i++) {
            if ([[[mutDict valueForKey:@"Is_read"] objectAtIndex:i] intValue]==0) {
                [arrUnread addObject:[[mutDict valueForKey:@"MessageID"] objectAtIndex:i]];
            }
        }
        
        _txt_msg_count.text=[NSString stringWithFormat:@"(%lu)",(unsigned long)arrUnread.count];
        [self CallUpdateViewMsg];*/
        
        [_tbl_message reloadData];
       
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}
-(void)CallUpdateViewMsg
{
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    //String
    
    int tot=arrUnread.count;
    NSString *strID=[self convertToCommaSeparatedFromArray:arrUnread];
    [AddPost setValue:[NSString stringWithFormat:@"%@",strID] forKey:@"message_ids"];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    [manager POST:[NSString stringWithFormat:@"%@update_view_message",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        //---------------------------
        NSUserDefaults *standardUserDefaults =[NSUserDefaults standardUserDefaults];
        int val=[[standardUserDefaults valueForKey:@"msg_req_count"] intValue];
        [standardUserDefaults setObject:[NSString stringWithFormat:@"%d",val-tot] forKey:@"msg_req_count"];
        
        
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

-(void)CallGetREQ
{
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    [manager POST:[NSString stringWithFormat:@"%@get_request",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
         _txt_req_count.text=[NSString stringWithFormat:@"(%@)",[responseObject valueForKey:@"total_request"]];
        
        mutDictreq=[responseObject valueForKey:@"data"];
        page_no=[responseObject valueForKey:@"page_no"];
        
        [_tbl_request reloadData];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}



#pragma mark- Wrapper Class Delegate Methods
//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary* )dicsResponse
{
    if(wrapper == httpmsg && httpmsg != nil)
    {
        
    }
}


//fetchDataFail
- (void) HttpWrapper:(HttpWrapper* )wrapper fetchDataFail:(NSError* )error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (tableView == _tbl_message) {
        return mutDict.count;
    }
    else
    {
        return mutDictreq.count;
    }
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tbl_message) {
        static NSString *simpleTableIdentifier = @"Cell";
        //tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        tableView.separatorColor = [UIColor clearColor];
        MessageCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[MessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        //cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
        
        [cell.img_profile_image sd_setImageWithURL:[NSURL URLWithString:[mutDict valueForKey:@"UserImageURL"][indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
        cell.lbl_sender_name.text=[NSString stringWithFormat:@"%@",[[mutDict valueForKey:@"FName"] objectAtIndex:indexPath.row]];
        //cell.lbl_sender_msg.text=[NSString stringWithFormat:@"%@",[[mutDict valueForKey:@"Message"] objectAtIndex:indexPath.row]];
        
        NSString *question=[NSString stringWithFormat:@"%@",[[mutDict valueForKey:@"Message"] objectAtIndex:indexPath.row]];
        CGSize maximumLabelSize = CGSizeMake(296, FLT_MAX);
        CGSize expectedLabelSize = [question sizeWithFont:cell.lbl_sender_msg.font constrainedToSize:maximumLabelSize lineBreakMode:cell.lbl_sender_msg.lineBreakMode];
        CGRect newFrame = cell.lbl_sender_msg.frame;
        newFrame.size.height = expectedLabelSize.height;
        cell.lbl_sender_msg.frame = newFrame;
        cell.lbl_sender_msg.text=question;
        
        hei=cell.lbl_sender_msg.frame.size.height+55;
        

        return cell;

    }
    else
    {
        static NSString *simpleTableIdentifier = @"Cell";
        //tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        tableView.separatorColor = [UIColor clearColor];
        RequestCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[RequestCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        //cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
     
        if ([[[mutDictreq valueForKey:@"Status"] objectAtIndex:indexPath.row]isEqualToString:@"0"]) {
            
            [cell.img_profileimg sd_setImageWithURL:[NSURL URLWithString:[mutDictreq valueForKey:@"ImageURL"][indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
            cell.lbl_user_nm.text=[NSString stringWithFormat:@"%@",[[mutDictreq valueForKey:@"FName"] objectAtIndex:indexPath.row]];
            
            
            
            if ([[[mutDictreq valueForKey:@"MemberType"] objectAtIndex:indexPath.row]isEqualToString:@"Joined"]) {
                cell.lbl_msges.text=[NSString stringWithFormat:@"Want to join in your team : \"%@\"",[mutDictreq valueForKey:@"ProjectTitle"][indexPath.row]];
            }
            else
            {
                cell.lbl_msges.text=[NSString stringWithFormat:@"Want to invite in my team : \"%@\" ",[mutDictreq valueForKey:@"ProjectTitle"][indexPath.row]];
            }

            
            [cell.btn_accept addTarget:self action:@selector(Accept_Action:)forControlEvents:UIControlEventTouchUpInside];
            
            [cell.btn_denied addTarget:self action:@selector(Denied_Action:)forControlEvents:UIControlEventTouchUpInside];
            
        }
        
        
       
    return cell;

    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // some code that compute row's height
    if (tableView == _tbl_message) {
        
    return 75;
    }
    else
    {
        return 75;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
  /*  MessagePopVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MessagePopVC"];
    vc.view.frame = CGRectMake(0, 0, 233.0f, 236.0f);
    vc.Uid=[[mutDict valueForKey:@"MessageFromID"] objectAtIndex:indexPath.row];//indexpath.row
    [self presentPopUpViewController:vc];*/
    
   /* MessagePopVC *homescreen = [self.storyboard instantiateViewControllerWithIdentifier:@"MessagePopVC"];
    homescreen.Uid=[[mutDict valueForKey:@"MessageFromID"] objectAtIndex:indexPath.row];
    homescreen.view.frame = self.view.bounds;
    [homescreen.view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5f]];
    [self.view addSubview:homescreen.view];
    [self addChildViewController:homescreen];
    [homescreen didMoveToParentViewController:self];
    
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];*/
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    MessageChat * next = [self.storyboard instantiateViewControllerWithIdentifier:@"MessageChat"];
    next.dictFrom=[mutDict objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:next animated:YES];
    
    
    
    
    
    ///JSQ Message
    
    
    
  
}

-(void)Accept_Action:(UIButton *)btn
{
    NSLog(@"Request Accept   %ld",(long)btn.tag);
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];//
    [AddPost setValue:[[mutDictreq valueForKey:@"ProjectMemberID"] objectAtIndex:btn.tag] forKey:@"project_member_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@accept_request",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSUserDefaults *standardUserDefaults =[NSUserDefaults standardUserDefaults];
        int val=[[standardUserDefaults valueForKey:@"msg_req_count"] intValue];
        [standardUserDefaults setObject:[NSString stringWithFormat:@"%d",val-1] forKey:@"msg_req_count"];
        
        [self CallGetREQ];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];

    
    
    
}
-(void)Denied_Action:(UIButton *)btn
{
    NSLog(@"Request Denied");
    NSLog(@"Request Accept   %ld",(long)btn.tag);
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];//
    [AddPost setValue:[[mutDictreq valueForKey:@"ProjectMemberID"] objectAtIndex:btn.tag] forKey:@"project_member_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@denied_request",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSUserDefaults *standardUserDefaults =[NSUserDefaults standardUserDefaults];
        int val=[[standardUserDefaults valueForKey:@"msg_req_count"] intValue];
        [standardUserDefaults setObject:[NSString stringWithFormat:@"%d",val-1] forKey:@"msg_req_count"];
        
        
        [self CallGetREQ];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];

}
-(void)handleLPress
{
    
    // Your code here
    MessagePopVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MessagePopVC"];
    vc.view.frame = CGRectMake(0, 0, 233.0f, 236.0f);
    vc.Uid=[[mutDict valueForKey:@"MessageFromID"] objectAtIndex:0];//indexpath.row
    [self presentPopUpViewController:vc];

    
}

-(void)cellTapped:(UITapGestureRecognizer*)tap
{
    // Your code here
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    int lastRow=[mutDict count]-1;
    if(([indexPath row] == lastRow)&&(lastRow<[mutDict count]))
    {
        //[self Getnewdata];
      /*  NSLog(@"i m call");
        
        if ([mutDict count]>=2) {
            __weak MessagesVC *weakSelf = self;
            // setup infinite scrolling
            [_tbl_message addInfiniteScrollingWithActionHandler:^{
                [weakSelf CallSearchMethod1];
            }];
        }
       */
        /*
        last_eventid=[[mutDict valueForKey:@"EventID"]lastObject];
        if ([last_eventid integerValue]<1) {
            _tbl_event.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        }*/
        //[self CallSearchMethod];
        
    }
}


- (IBAction)btn_HOME:(id)sender {
    NewHomeVC* next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:YES];

}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}


- (IBAction)btn_MESSAGE:(id)sender {
    
    [self CallMessageMethod];
    

}

- (IBAction)btn_REQUEST_ACTION:(id)sender {
    
    [self CallRequestMethod];
    

}

- (IBAction)btn_NOTIFY:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}

-(NSString *)convertToCommaSeparatedFromArray:(NSArray*)array{
    return [array componentsJoinedByString:@","];
}

-(void)CallMessageMethod
{
    //_btn_msg.layer.cornerRadius=_btn_msg.frame.size.height/2;
    _btn_msg.layer.backgroundColor=[UIColor colorWithHex:0x01B5A9].CGColor;
    [_btn_msg setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _btn_msg.layer.borderColor=[UIColor clearColor].CGColor;
    
    _txt_req_count.textColor=[UIColor blackColor];
    _txt_msg_count.textColor=[UIColor whiteColor];
    
    //_btn_request.layer.cornerRadius=_btn_request.frame.size.height/2;
    _btn_request.layer.backgroundColor=[UIColor clearColor].CGColor;
    [_btn_request setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    // Bottom border
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, self.btn_request.frame.size.height - 1, self.btn_request.frame.size.width, 1.0f);
    bottomBorder.backgroundColor = [UIColor colorWithHex:0xADADAD].CGColor;
    [self.btn_request.layer addSublayer:bottomBorder];
    
    
    _tbl_message.hidden=NO;
    _tbl_request.hidden=YES;
}

-(void)CallRequestMethod
{
    //_btn_request.layer.cornerRadius=_btn_request.frame.size.height/2;
    _btn_request.layer.backgroundColor=[UIColor colorWithHex:0x01B5A9].CGColor;
    [_btn_request setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _btn_request.layer.borderColor=[UIColor clearColor].CGColor;
    
    _txt_req_count.textColor=[UIColor whiteColor];
    _txt_msg_count.textColor=[UIColor blackColor];
    
    //_btn_msg.layer.cornerRadius=_btn_msg.frame.size.height/2;
    _btn_msg.layer.backgroundColor=[UIColor clearColor].CGColor;
    [_btn_msg setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
   
    // Bottom border
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, self.btn_msg.frame.size.height - 1, self.btn_msg.frame.size.width, 1.0f);
    bottomBorder.backgroundColor = [UIColor colorWithHex:0xADADAD].CGColor;
    [self.btn_msg.layer addSublayer:bottomBorder];
    
    
    _tbl_message.hidden=YES;
    _tbl_request.hidden=NO;
}



@end
