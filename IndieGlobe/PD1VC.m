//
//  PD1VC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/26/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "PD1VC.h"
#import "NewsVC.h"
#import "PD1Cell.h"
#import "ProfileVC.h"
#import "EventVC.h"
#import "NewHomeVC.h"
#import "AllNotificationVC.h"
#import "AFTPagingBaseViewController.h"



@interface PD1VC ()

@end

@implementation PD1VC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    // Do any additional setup after loading the view.
    //_btn_add_memb.layer.cornerRadius=_btn_add_memb.frame.size.height/2;
    
    _btn_join_now.layer.cornerRadius=_btn_join_now.frame.size.height/2;
    
     _tbl_pd1.tableHeaderView = _vw_header;
    
    _lbl_title.text=[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"ProjectTitle"]];
    _lbl_desc.text=[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"ShortDesc"]];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    if ([[_mutDict valueForKey:@"CreatedBy"] isEqualToString:[NSString stringWithFormat:@"%@",[standardUserDefaults valueForKey:@"UserID"]]]) {
        _btn_join_now.alpha=0.40;
        _btn_join_now.enabled=NO;
    }
    else
    {
        _btn_join_now.alpha=1.00;
        _btn_join_now.enabled=YES;
        
        if ([[_mutDict valueForKey:@"IsJoined"]isEqualToString:@"1"] || [[_mutDict valueForKey:@"IsInvited"]isEqualToString:@"1"])
        {
            _btn_join_now.alpha=0.40;
            _btn_join_now.enabled=NO;
        }
        else
        {
            _btn_join_now.alpha=1.00;
            _btn_join_now.enabled=YES;
        }
    }
    
    
    [_img_proj_image sd_setImageWithURL:[NSURL URLWithString:[_mutDict valueForKey:@"ProjectImage"]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
    
    _img_user.clipsToBounds=YES;
    _img_user.layer.cornerRadius=_img_user.frame.size.height/2;
    [_img_user sd_setImageWithURL:[NSURL URLWithString:[_mutDict valueForKey:@"ImageURL"]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
    _lbl_usernm.text=[NSString stringWithFormat:@"%@ %@",[_mutDict valueForKey:@"FName"],[_mutDict valueForKey:@"LName"]];
    
    
    //AUTO RESIZE
   
    CGRect mainframe = _lbl_desc.frame;
    mainframe.size.height =  [self heightForText:[_mutDict valueForKey:@"ShortDesc"]];
    _lbl_desc.frame = mainframe;
    
    CGRect mainframe1 = _vw_header.frame;
    mainframe1.size.height = [self heightForText:[_mutDict valueForKey:@"ShortDesc"]]+ _btn_join_now.frame.size.height + _lbl_title.frame.size.height+ _img_proj_image.frame.size.height +34;
    _vw_header.frame = mainframe1;
    
    CGRect buttonframe = _btn_join_now.frame;
    buttonframe.origin.y = _lbl_desc.frame.origin.y+_lbl_desc.frame.size.height+8;
    _btn_join_now.frame = buttonframe;
    
    CGRect buttonframe1 = _lbldrawline.frame;
    buttonframe1.origin.y = _btn_join_now.frame.origin.y+_btn_join_now.frame.size.height+11;
    _lbldrawline.frame = buttonframe1;
    
    CGRect imageframe = _joinimage.frame;
    imageframe.origin.y = _btn_join_now.frame.origin.y+7;
    _joinimage.frame = imageframe;
    
    //PD1VC
    _img_proj_image.userInteractionEnabled=YES;
    
    UITapGestureRecognizer *letterTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(highlightLetter:)];
    letterTapRecognizer.numberOfTapsRequired = 1;
    [_img_proj_image addGestureRecognizer:letterTapRecognizer];
    
    [_img_proj_image setContentMode:UIViewContentModeScaleAspectFill];
    [_img_proj_image setClipsToBounds:YES];
   
    
    _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
    _lbl_notifi_count.clipsToBounds=YES;
    _lbl_notifi_count.text=theAppDelegate.n_Count;
    
    if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
        _lbl_notifi_count.hidden=YES;
    }
    else
    {
        _lbl_notifi_count.hidden=NO;
    }
    
}

-(void)highlightLetter:(UITapGestureRecognizer*)sender
{
    NSLog(@"click on image");
    
    [APP_DELEGATE showLoadingView:@""];
    
    NSURL *url = [NSURL URLWithString:[_mutDict valueForKey:@"ProjectImage"]];
    NSData *data = [NSData dataWithContentsOfURL:url];
    UIImage* immg = [[UIImage alloc]initWithData:data];
    NSArray *landscapeImages = [[NSArray alloc]initWithObjects:immg, nil];
    
    Class cls = NSClassFromString(@"AFTNormalPagingViewController");
    AFTPagingBaseViewController *vc1 = [cls new];
    AFTPagingBaseViewController *bvc = (AFTPagingBaseViewController *)vc1;
    
    bvc.title = @"Photo";
    bvc.images = landscapeImages;
    [APP_DELEGATE hideLoadingView];
    [self presentViewController:bvc animated:YES completion:nil];
    
    
}

-(CGFloat)heightForText:(NSString *)text
{
    NSInteger MAX_HEIGHT = 2000;
    UITextView * textView = [[UITextView alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width-80, MAX_HEIGHT+150)];
    textView.text = text;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        textView.font = [UIFont fontWithName:@"Roboto-Light" size:13.0];
    }
    else
    {
        textView.font = [UIFont fontWithName:@"Roboto-Light" size:13.0];
    }
    
    [textView sizeToFit];
    
    return textView.frame.size.height;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    page_no=@"0";
    [self CallInvitedList];
}

-(void)CallInvitedList
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",page_no] forKey:@"page_no"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"ProjectID"]] forKey:@"project_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);//get_invited_people_list
    [manager POST:[NSString stringWithFormat:@"%@get_invited_joined_people_list",JsonUrlConstant] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        page_no=[responseObject valueForKey:@"page_no"];
        inviteList=[responseObject valueForKey:@"data"];
        
        [_tbl_pd1 reloadData];
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return inviteList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Cell";
    
    tableView.separatorColor = [UIColor clearColor];
    PD1Cell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[PD1Cell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    [cell.img_prof_image sd_setImageWithURL:[NSURL URLWithString:[inviteList valueForKey:@"ImageURL"][indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
    cell.lbl_usernm.text=[NSString stringWithFormat:@"%@",[[inviteList valueForKey:@"FName"] objectAtIndex:indexPath.row]];
    cell.lbl_invited.text=[NSString stringWithFormat:@"%@",[[inviteList valueForKey:@"MemberType"] objectAtIndex:indexPath.row]];
    
    NSArray *arrT=[[inviteList valueForKey:@"ProfessionData"] objectAtIndex:indexPath.row];
    
    cell.lbl_prof.text=[arrT componentsJoinedByString:@","];
    NSMutableArray *NewArr = [[NSMutableArray alloc] init];
    for (int i=0; i<arrT.count; i++) {
        cell.lbl_prof.text=[arrT componentsJoinedByString:@","];
        [NewArr addObject:[[arrT objectAtIndex:i] valueForKey:@"Profession"]];
    }
    cell.lbl_prof.text=[NewArr componentsJoinedByString:@","];
    
    //DATE CONVERT
    //------TODATE--------------
    NSString * yourJSONString = [inviteList valueForKey:@"CreatedOn"][indexPath.row];
    NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
    [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateFromString = [currentDTFormatter dateFromString:yourJSONString];
    [currentDTFormatter setDateFormat:@"MMM"];
    NSString *eventDateStr = [currentDTFormatter stringFromDate:dateFromString];
    [currentDTFormatter setDateFormat:@"dd"];
    NSString *onlydt = [currentDTFormatter stringFromDate:dateFromString];
    [currentDTFormatter setDateFormat:@"yyyy"];
    NSString *onlyyear = [currentDTFormatter stringFromDate:dateFromString];
    
    cell.lbl_datetime.text=[NSString stringWithFormat:@"%@ %@ %@",eventDateStr,onlydt,onlyyear];

    
    
    
    //cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.selected) {
        // ... Uncheck
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    ProfileVC* next = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileVC"];
    next.mutDict=[inviteList objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:next animated:YES];
}








- (IBAction)btn_HOME:(id)sender {
    NewHomeVC* next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:YES];
}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}


- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)btn_JOIN_TEAM:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:@"Your request to join the team has been sent"
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
    
    //CALLJOIN TEAM
    //Param
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"ProjectID"]] forKey:@"project_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);//get_invited_people_list
    [manager POST:[NSString stringWithFormat:@"%@join_project",JsonUrlConstant] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];

    
    
    
    

}

- (IBAction)btn_NOTIFY:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}
@end
