//
//  AddEventVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/7/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopOverView.h"
#import "HttpWrapper.h"
#import "ApplicationConstant.h"
#import "NewHomeSearchVC.h"
#import "UzysImageCropperViewController.h"


@interface AddEventVC : UIViewController<UzysImageCropperDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPickerViewDelegate,UIPickerViewDataSource,HttpWrapperDelegate,UICollectionViewDelegate
,UICollectionViewDataSource>
{
    UIView *picker;
    UITextField *currentTextField;
    
    NSMutableArray *AddNew;
    
    UIImagePickerController *ci;
    UIPopoverController *popover;
    
    //DatePicker
    UIDatePicker *datePicker;
    UIPickerView *pickerView;
    NSMutableArray *dataArray;
    
    
    HttpWrapper* httpaddevent,*httpcountry,*httpstate,*httpcity;
    
    UIImage *select_image;
    
    
    //VAR
    int isCheck;
    NSMutableArray* country;
    IBOutlet UITextField *txt_country;
    NSString *countryid;
    
    NSMutableArray* state;
    IBOutlet UITextField *txt_state;
    NSString * stateid;
    
    NSMutableArray* arrcity;
    IBOutlet UITextField *txt_city;
    NSString * cityid;

}

@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;
@property (nonatomic,retain) UzysImageCropperViewController *imgCropperViewController;
@property (nonatomic,retain) UIImagePickerController *imgpicker;
//Outlet
@property (strong, nonatomic) IBOutlet UIButton *btn_menu;
@property (strong, nonatomic) IBOutlet UITextField *txt_title;
@property (strong, nonatomic) IBOutlet UIImageView *img_choose_img;
@property (strong, nonatomic) IBOutlet UIButton *btn_add_event;
@property (strong, nonatomic) IBOutlet UITextView *txt_address;
@property (strong, nonatomic) IBOutlet UITextView *txt_desc;

@property (strong, nonatomic) IBOutlet UITextField *txt_from_date;
@property (strong, nonatomic) IBOutlet UITextField *txt_to_date;

@property (strong, nonatomic) IBOutlet UIImageView *img_extra;
@property (strong, nonatomic) IBOutlet UILabel *lbl_extra;


///ACTION
- (IBAction)btn_ADDEVENT:(id)sender;
- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_SCOUNTRY:(id)sender;
- (IBAction)btn_SSTATE:(id)sender;
- (IBAction)btn_SCITY:(id)sender;

- (IBAction)btn_SFROM_DATE:(id)sender;
- (IBAction)btn_STO_DATE:(id)sender;
- (IBAction)btn_NOTIFY:(id)sender;

@property (weak, nonatomic) IBOutlet UICollectionView *colle_addevent;



-(void)Popvalueselected:(NSString*)strValue;
-(void)Popvalveselected:(NSString*)strValve;
-(void)dismissPopUpViewController;


@end
