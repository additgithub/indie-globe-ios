//
//  ShortFilmCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/18/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShortFilmCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIButton *btn_close_sf;
@property (strong, nonatomic) IBOutlet UIImageView *img_short_film;
@property (strong, nonatomic) IBOutlet UILabel *lbl_addnew;




@end
