//
//  MyEventDetailsCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/7/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyEventDetailsCell : UITableViewCell

//OUTLET
@property (strong, nonatomic) IBOutlet UIImageView *img_profileimg;
@property (strong, nonatomic) IBOutlet UILabel *lbl_name;
@property (strong, nonatomic) IBOutlet UIButton *btn_rsvp;
@property (strong, nonatomic) IBOutlet UIView *vw_rsvp;
@property (strong, nonatomic) IBOutlet UIView *vw_scmi;


//Action



@end
