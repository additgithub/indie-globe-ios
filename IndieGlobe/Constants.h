//
//  Constants.h
//  DAYT
//
//  Created by Amit Ajmera on 10/9/15.
//  Copyright © 2015 The FreeBird. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#import "AppDelegate.h"
#define itos(value) [NSString stringWithFormat:@"%d",value]
#define APP_DELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define IS_OS_9_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0)
#define IS_OS_10_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0)
#define IsIphone4 (fabs((double)[[UIScreen mainScreen] bounds].size.height - (double) 480) < DBL_EPSILON)

#define IsIphone5 (fabs((double)[[UIScreen mainScreen] bounds].size.height - (double) 568) < DBL_EPSILON)

#define IsIphone6 ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && MAX([UIScreen mainScreen].bounds.size.height,[UIScreen mainScreen].bounds.size.width) == 667)

#define IsIphone7 ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && MAX([UIScreen mainScreen].bounds.size.height,[UIScreen mainScreen].bounds.size.width) == 667)

#define IsIphone6Plus ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && MAX([UIScreen mainScreen].bounds.size.height,[UIScreen mainScreen].bounds.size.width) == 736)

//#define IsIphone7Plus ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && MAX([UIScreen mainScreen].bounds.size.height,[UIScreen mainScreen].bounds.size.width) == 736)
// For View moves up when keypad appears

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

#define MainUrl @"https://facetofacetech.com/f2fapi/"
// Alertview
#define showAlert(title,amessage) UIAlertView *alert=[[UIAlertView alloc]initWithTitle:title message:amessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];[alert show];

#define AlertCongrates @"Congratulations!"
#define AlertSorry @"Sorry"
#define AlertTitle @"Alert"
#define AlertTITLE @""
#define AlertInternet @"Please Check Internet Connection"
#define AlertNoresult @"No result found"
#define AlertServerError @"Cannot connect to Server,Please try again later"






//======================================================================================
//--------------AFNetworking------------------------------------------------------------
//======================================================================================









#endif /* Constants_h */
