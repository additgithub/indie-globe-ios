//
//  SetHeaderView.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 8/29/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SetHeaderView : UIView
{
    
}

@property (nonatomic,retain) IBOutlet UIView *headerView;

-(void)setHeaderView:(UIView *)headerView;


@end
