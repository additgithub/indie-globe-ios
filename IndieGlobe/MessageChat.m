//
//  MessageChat.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 9/11/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "MessageChat.h"
#import <AFNetworking/AFNetworking.h>
#import "ApplicationConstant.h"
#import "MessageChatCell.h"
#import "UIImageView+WebCache.h"
#import "IQKeyboardManager.h"


@interface MessageChat ()

@end

@implementation MessageChat

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    
    NSLog(@"== %@",_dictFrom);

    _img_header.clipsToBounds=YES;
    _img_header.layer.cornerRadius=_img_header.layer.frame.size.height/2;
    [_img_header sd_setImageWithURL:[NSURL URLWithString:[_dictFrom valueForKey:@"UserImageURL"]] placeholderImage:[UIImage imageNamed:@"avatar"]];
    
    _lbl_headernm.text=[NSString stringWithFormat:@"%@",[_dictFrom valueForKey:@"FName"]];

    [self CallChatList];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void) touchesEnded:(NSSet *)touches
            withEvent:(UIEvent *)event {
    //some code
    [self.view endEditing:YES];
}

- (void)touchesBegan:(NSSet* )touches withEvent:(UIEvent* )event {
    [self.view endEditing:YES];
}

-(void)CallChatList
{
    
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    //NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[_dictFrom valueForKey:@"MessageFromID"] forKey:@"from_id"];
    [AddPost setValue:[_dictFrom valueForKey:@"MessageToID"] forKey:@"to_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@get_message",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        arrList=[[NSMutableArray alloc]initWithArray:[responseObject valueForKey:@"data"]];
        
        [APP_DELEGATE hideLoadingView];
        
        [_tbl_chat reloadData];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
}

-(void)CallSendMSG
{
    
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[_dictFrom valueForKey:@"MessageFromID"] forKey:@"receiver_id"];
    [AddPost setValue:_txt_msg.text forKey:@"message"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@send_message",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSLocale* currentLocale = [NSLocale currentLocale];
        [[NSDate date] descriptionWithLocale:currentLocale];
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
        NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
        NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
        
        
        NSMutableDictionary *tempDict=[[NSMutableDictionary alloc]init];
        [tempDict setValue:_txt_msg.text forKey:@"Message"];
        [tempDict setValue:[dateFormatter stringFromDate:[NSDate date]] forKey:@"CreatedOn"];//MessageFromID
        [tempDict setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"MessageFromID"];
        
        [arrList insertObject:tempDict atIndex:arrList.count];
        
        _txt_msg.text=@"";
        
        [APP_DELEGATE hideLoadingView];
        
        [_tbl_chat reloadData];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    
    
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return arrList.count;
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"Cell";
    //tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.separatorColor = [UIColor clearColor];
    MessageChatCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[MessageChatCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.vw_me.hidden=YES;
    cell.vw_sender.hidden=YES;
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *userid=[NSString stringWithFormat:@"%@",[standardUserDefaults valueForKey:@"UserID"]];
    
    if ([[arrList valueForKey:@"MessageFromID"][indexPath.row]isEqualToString:userid]) {
        cell.vw_me.hidden=NO;
        
        cell.lbl_me_msg.text=[NSString stringWithFormat:@"%@",[arrList valueForKey:@"Message"][indexPath.row]];
        //DATETIME
        NSString * yourJSONString = [arrList valueForKey:@"CreatedOn"][indexPath.row];//EndDate
        NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
        [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *dateFromString = [currentDTFormatter dateFromString:yourJSONString];
        [currentDTFormatter setDateFormat:@"hh:mm a dd-MM-yyyy"];
        NSString *time = [currentDTFormatter stringFromDate:dateFromString];
        
        cell.lbl_me_time.text=time;
        
        ///////SET HEIGHT
        cell.lbl_me_msg.text=[NSString stringWithFormat:@"%@",[arrList valueForKey:@"Message"][indexPath.row]];
        
        CGRect autosizeprof = cell.lbl_me_msg.frame;
        autosizeprof.size.height = [self heightForText:cell.lbl_me_msg.text];
        cell.lbl_me_msg.frame = autosizeprof;
        
        
        
        CellHeight=cell.lbl_me_msg.frame.size.height+35;
        
        
        CGRect frame = cell.lbl_me_time.frame;
        frame.origin.y=cell.lbl_me_msg.frame.size.height;
        cell.lbl_me_time.frame = frame;
        
        [cell.lbl_me_msg setNumberOfLines:0];
        //[cell.lbl_me_msg sizeToFit];
        //cell.lbl_me_msg.adjustsFontSizeToFitWidth=YES;
        
        //CGSize yourLabelSize = [cell.lbl_me_msg.text sizeWithAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"Helvetica" size:12]}];
        
        //NSLog(@"yourLabelSize %@",yourLabelSize);
        
        
        
    }
    else
    {
        cell.vw_sender.hidden=NO;
        
        
        //DATETIME
        NSString * yourJSONString = [arrList valueForKey:@"CreatedOn"][indexPath.row];//EndDate
        NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
        [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *dateFromString = [currentDTFormatter dateFromString:yourJSONString];
        [currentDTFormatter setDateFormat:@"hh:mm a dd-MM-yyyy"];
        NSString *time = [currentDTFormatter stringFromDate:dateFromString];
        
        cell.lbl_sender_time.text=time;
        
        ///////SET HEIGHT
        
        cell.lbl_sender_msg.text=[NSString stringWithFormat:@"%@",[arrList valueForKey:@"Message"][indexPath.row]];
        
        CGRect autosizeprof = cell.lbl_sender_msg.frame;
        autosizeprof.size.height = [self heightForText:cell.lbl_sender_msg.text];
        cell.lbl_sender_msg.frame = autosizeprof;
        
        CellHeight=cell.lbl_sender_msg.frame.size.height+35;
        
        CGRect frame = cell.lbl_sender_time.frame;
        frame.origin.y=cell.lbl_sender_msg.frame.size.height;
        cell.lbl_sender_time.frame = frame;
        
        [cell.lbl_sender_msg setNumberOfLines:0];
        //[cell.lbl_sender_msg sizeToFit];
        
    }
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    [self.view endEditing:YES];
}

-(CGFloat)heightForText:(NSString *)text
{
    NSInteger MAX_HEIGHT = 2000;
    UITextView * textView = [[UITextView alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width-80, MAX_HEIGHT+150)];
    textView.text = text;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        textView.font = [UIFont fontWithName:@"Roboto-Light" size:12.0];
    }
    else
    {
        textView.font = [UIFont fontWithName:@"Roboto-Light" size:12.0];
    }
    
    [textView sizeToFit];
    
    return textView.frame.size.height;
}

- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_SENDMSG:(id)sender {
    
    //

    if (![_txt_msg.text isEqualToString:@""]) {
        [self.view endEditing:YES];
        [self CallSendMSG];
    }
    else
    {
        [self.view endEditing:YES];
    }
    
    
    
}
@end
