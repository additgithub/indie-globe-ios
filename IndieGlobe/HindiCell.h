//
//  HindiCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 3/24/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShadowView.h"


@interface HindiCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *img_image;
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;
@property (strong, nonatomic) IBOutlet UILabel *lbl_postdt;

@property (strong, nonatomic) IBOutlet UIImageView *img_prof;
@property (weak, nonatomic) IBOutlet UIButton *btn_indi_play;
@property (weak, nonatomic) IBOutlet ShadowView *vw_shadow;

@property (weak, nonatomic) IBOutlet UIButton *btn_like;
@property (weak, nonatomic) IBOutlet UILabel *lbl_like;
@property (weak, nonatomic) IBOutlet UIButton *btn_dislike;
@property (weak, nonatomic) IBOutlet UILabel *lbl_dislike;
@property (weak, nonatomic) IBOutlet UIButton *btn_comment;
@property (weak, nonatomic) IBOutlet UILabel *lbl_comment;


@property (weak, nonatomic) IBOutlet UIImageView *img_vw1;
@property (weak, nonatomic) IBOutlet UILabel *lbl_nm_vw1;
@property (weak, nonatomic) IBOutlet UILabel *lbl_desc_vw1;

@property (weak, nonatomic) IBOutlet UIImageView *img_vw2;
@property (weak, nonatomic) IBOutlet UILabel *lbl_nm_vw2;
@property (weak, nonatomic) IBOutlet UILabel *lbl_desc_vw2;

@property (weak, nonatomic) IBOutlet UIView *vw1;
@property (weak, nonatomic) IBOutlet UIView *vw2;



@end
