//
//  IndieCell.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/1/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "IndieCell.h"

@implementation IndieCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.vw_shadow setShadow];
    
    
    _img_vw1.layer.cornerRadius=_img_vw1.layer.frame.size.height/2;
    _img_vw1.clipsToBounds=YES;
    
    _img_vw2.layer.cornerRadius=_img_vw2.layer.frame.size.height/2;
    _img_vw2.clipsToBounds=YES;
    
    CALayer *TopBorder = [CALayer layer];
    TopBorder.frame = CGRectMake(0.0f, 0.0f, _vw_1.frame.size.width, 1.0f);
    TopBorder.backgroundColor = [UIColor lightGrayColor].CGColor;
    [_vw_1.layer addSublayer:TopBorder];
    
    
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
