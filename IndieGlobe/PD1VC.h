//
//  PD1VC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/26/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
#import "ApplicationConstant.h"
#import "NewHomeSearchVC.h"

@interface PD1VC : UIViewController<UITableViewDelegate,UITableViewDataSource,HttpWrapperDelegate>
{
    HttpWrapper* httppd1vc;
    NSString *page_no;
    NSMutableArray* inviteList;
}

@property (retain,nonatomic)NSMutableArray *mutDict;

//OUTLET
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;
@property (strong, nonatomic) IBOutlet UITextView *lbl_desc;
@property (strong, nonatomic) IBOutlet UILabel *lbldrawline;
@property (strong, nonatomic) IBOutlet UIImageView *joinimage;

@property (strong, nonatomic) IBOutlet UIView *vw_header;
@property (strong, nonatomic) IBOutlet UITableView *tbl_pd1;
@property (strong, nonatomic) IBOutlet UIButton *btn_join_now;
@property (strong, nonatomic) IBOutlet UIImageView *img_proj_image;
@property (strong, nonatomic) IBOutlet UIImageView *img_user;
@property (strong, nonatomic) IBOutlet UILabel *lbl_usernm;

@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;


//ACTION
- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_JOIN_TEAM:(id)sender;
- (IBAction)btn_NOTIFY:(id)sender;





@end
