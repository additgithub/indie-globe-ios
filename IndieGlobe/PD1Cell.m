//
//  PD1Cell.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/26/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "PD1Cell.h"

@implementation PD1Cell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _img_prof_image.clipsToBounds=YES;
    _img_prof_image.layer.cornerRadius=_img_prof_image.frame.size.height/2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
