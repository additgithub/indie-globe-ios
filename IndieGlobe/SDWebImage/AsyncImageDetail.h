//
//  AsyncImage.h
//  AsyncImage
//
//  Created by Chintan on 16/02/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface AsyncImageDetail : UIImageView
{
    NSURLConnection* connection; 
	NSMutableData* data; 

	UIActivityIndicatorView *scrollingWheel;
    NSString *imgName;
    NSData *xmlData;
}
-(void)loadImageFromString:(NSString*)url;
-(void)loadimagewithName:(NSString *)name;

-(id)initWithCoder:(NSCoder *)aDecoder;
-(void)cancelConnection;

@end
