//  AsyncImageDetail.m
//  AsyncImageDetail
//
//  Created by Chintan on 16/02/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.


#import "AsyncImageDetail.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>

@implementation AsyncImageDetail

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder]))
    {
		scrollingWheel = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
		float x = self.bounds.size.width/2;
		float y = self.bounds.size.height/2;
		scrollingWheel.center = CGPointMake(x, y);
		scrollingWheel.hidesWhenStopped = YES;
		[self addSubview:scrollingWheel];
//		self.clipsToBounds = YES;
    }
    return self;
}
-(void)loadimagewithName:(NSString *)name
{
    [self setImage:[UIImage imageNamed:name]];
}
-(void)loadImageFromString:(NSString*)url
{
    NSLog(@"%@",url);
    [self setImage:[UIImage imageNamed:@"default"]];

    [scrollingWheel startAnimating];
    
    if (connection!=nil)
    {
        [connection cancel];
        connection = nil;
    }
    if (data!=nil)
    {
        data = nil;
    }
    NSArray *arr = [url componentsSeparatedByString:@"/"];
    
    imgName = [arr lastObject];
    

    NSString *imagePath = [NSString stringWithFormat:@"%@/%@",[[AppDelegate sharedAppDelegate] applicationDocumentDirectoryString],imgName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSLog(@"Image local path : %@",imagePath);
    
    if ([fileManager fileExistsAtPath:imagePath] == NO)
    {
        NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60.0];
        connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        [self setImage:nil];
    }
    else
    {
        [scrollingWheel stopAnimating];
        [self setImage:[UIImage imageWithContentsOfFile:imagePath]];
    }
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
	data=nil;
    [scrollingWheel stopAnimating];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	data = [[NSMutableData alloc] init] ;
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)dataObj {
	[data appendData:dataObj];
}

- (void) connectionDidFinishLoading:(NSURLConnection *)theConnection {
	connection=nil;
    
    NSString *imagePath = [NSString stringWithFormat:@"%@/%@",[[AppDelegate sharedAppDelegate] applicationDocumentDirectoryString],imgName];
    
    [data writeToFile:imagePath atomically:YES];
	data=nil;
	[scrollingWheel stopAnimating];
}
- (void)dealloc 
{
    
}

+(void)loadimagewithURL:(NSString *)name
{
    
}

-(void)cancelConnection{
    if (connection !=nil) {
        [connection cancel];
        connection=nil;
    }
    if(data!=nil){
        data=nil;
    }
	[scrollingWheel stopAnimating];
}

@end
