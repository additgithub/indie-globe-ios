//
//  NewProfileVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/16/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//
#import <Photos/Photos.h>
#import "NewProfileVC.h"
#import "NewProfileCell.h"


#import "SWRevealViewController.h"
#import "ProfileCell.h"
#import "DeshboardVC.h"
#import "Constants.h"
#import "UIColor+CL.h"
#import "EditProfileVC.h"
#import "MovieCell.h"
#import "ShortFilmCell.h"
#import "ProfilePhotoViewVC.h"
#import "AddPhotosVC.h"
#import "iCarousel.h"
#import "FollowListVC.h"
#import "NewHomeVC.h"
#import "WebPlayerVC.h"
#import "UzysImageCropperViewController.h"

#import "YKMediaPlayerKit.h"
#import "YKVimeoVideo.h"
#import "YKYouTubeVideo.h"
#import "YKDirectVideo.h"
#import "AllNotificationVC.h"
#import "NewHomeSearchVC.h"
#import "ProfilePhotoForHome.h"
#import "AFTPagingBaseViewController.h"




@interface NewProfileVC ()<UIActionSheetDelegate>

@property (nonatomic, assign) BOOL wrap;
@property (nonatomic, strong) NSMutableArray *items;

@end

@implementation NewProfileVC
{
    int flag;
    
    YKYouTubeVideo  *_youTubeVideo;
    YKVimeoVideo    *_vimeoVideo;
    YKDirectVideo   *_directVideo;
    YKDirectVideo   *_otherVideo;
    YKUnKnownVideo  *_unknownVideo;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    [APP_DELEGATE showLoadingView:@""];
    flag=0;
    resFlag=0;
    // Do any additional setup after loading the view.
    
    
    
    [self customSetup];
    [self CallCategory];
    [self CallGetportfoliocategorydetails];
    
    [self setUpView];
   
    
    
    
    
    
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];

    
        //New Scroll
        _wrap = YES;
        _vw_caro.type = iCarouselTypeRotary;

    
    
    
    _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
    _lbl_notifi_count.clipsToBounds=YES;
    _lbl_notifi_count.text=theAppDelegate.n_Count;
    
    if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
        _lbl_notifi_count.hidden=YES;
    }
    else
    {
        _lbl_notifi_count.hidden=NO;
    }
    
    
}


- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if (IsIphone7 || IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = 145;
    }
    else{
        self.revealViewController.rearViewRevealWidth = 145;
    }
}
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}

-(void)setUpView
{
    _scroll_vw.delegate=self;
    self.scroll_vw.contentSize =CGSizeMake(320, 420);
    
    
    _img_profile.layer.masksToBounds=YES;
    _img_profile.layer.cornerRadius=_img_profile.frame.size.height/2;
    //_img_profile.layer.cornerRadius=_img_profile.frame.size.width/2;
    
    _img_profile.layer.borderWidth=1.0;
    _img_profile.layer.borderColor=[UIColor colorWithHex:0x196a84].CGColor;
    
    _btn_msgme.layer.borderWidth=1.2;
    _btn_msgme.layer.cornerRadius=_btn_msgme.frame.size.height/2;
    _btn_msgme.layer.borderColor=[UIColor colorWithHex:0x196a84].CGColor;
    
    
    self.lbl_sbio = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(self.lbl_sbio.frame.origin.x, self.lbl_sbio.frame.origin.y,self.lbl_sbio.frame.size.width,self.lbl_sbio.frame.size.height) ];
    [self.lbl_sbio setFont:[UIFont fontWithName:@"roboto" size:11]];
    self.lbl_sbio.delegate = self;
    self.lbl_sbio.enabledTextCheckingTypes=NSTextCheckingTypeLink|NSTextCheckingTypePhoneNumber;
    
    [self.view addSubview:self.lbl_sbio];
    
    
    
}



-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [self CallFollowing];
    [self CallFollower];
    [self CallMyMethod];
    [self setUpView];
    
    
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSLog(@"Do some work");
        [self CallGetPortfolio];
    });
    
    CGRect autosizeprof = _lbl_profession.frame;
    autosizeprof.size.height = [self heightForText:_lbl_profession.text];
    _lbl_profession.frame = autosizeprof;
    
    
    AddNew=[[NSMutableArray alloc] init];
    FFilms=[[NSMutableArray alloc] init];
    SFilms=[[NSMutableArray alloc] init];
    Reel=[[NSMutableArray alloc] init];
    BTS=[[NSMutableArray alloc] init];
    Video=[[NSMutableArray alloc] init];
    HShots=[[NSMutableArray alloc] init];
    
    AddNewnm=[[NSMutableArray alloc] init];
    FFilmsnm=[[NSMutableArray alloc] init];
    SFilmsnm=[[NSMutableArray alloc] init];
    Reelnm=[[NSMutableArray alloc] init];
    BTSnm=[[NSMutableArray alloc] init];
    Videonm=[[NSMutableArray alloc] init];
    HShotsnm=[[NSMutableArray alloc] init];
    
    
    
    
    if (AddNew.count <= 0) {
        //Array Allocation
        AddNew = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
        AddNewnm = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
    }
    
    
    if (AddNew_movie.count <= 0) {
        //Array Allocation
        AddNew_movie = [[NSMutableArray alloc] initWithObjects:@"addNewMovie", nil];
    }
    
    if (AddNew_sf.count <= 0) {
        //Array Allocation
        AddNew_sf = [[NSMutableArray alloc] initWithObjects:@"addNewShortFilm", nil];
    }
    
    ////-----------------------
    
    if (FFilms.count <= 0) {
        //Array Allocation
        FFilms = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
        FFilmsnm = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
    }
    
    if (SFilms.count <= 0) {
        //Array Allocation
        SFilms = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
        SFilmsnm = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
    }
    
    if (Reel.count <= 0) {
        //Array Allocation
        Reel = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
        Reelnm = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
    }
    
    if (BTS.count <= 0) {
        //Array Allocation
        BTS = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
        BTSnm = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
    }
    
    if (Video.count <= 0) {
        //Array Allocation
        Video = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
        Videonm = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
    }
    
    if (HShots.count <= 0) {
        //Array Allocation
        HShots = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
        HShotsnm = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
    }
    
    [self customSetup];
}



- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    NSString *val=[[NSString alloc]initWithFormat:@"%@",url];
    if ([[url scheme] hasPrefix:@"mailto"]) {
        NSLog(@" mail URL Selected : %@",url);
        MFMailComposeViewController *comp=[[MFMailComposeViewController alloc]init];
        [comp setMailComposeDelegate:self];
        if([MFMailComposeViewController canSendMail])
        {
            NSString *recp=[[val substringToIndex:[val length]] substringFromIndex:7];
            NSLog(@"Recept : %@",recp);
            [comp setToRecipients:[NSArray arrayWithObjects:recp, nil]];
            [comp setSubject:@"From my app"];
            [comp setMessageBody:@"Hello bro" isHTML:NO];
            [comp setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            [self presentViewController:comp animated:YES completion:nil];
        }
    }
    else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:val]];
    }
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if(error)
    {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"Erorr" message:@"Some error occureed" delegate:nil cancelButtonTitle:@"" otherButtonTitles:nil, nil];
        [alrt show];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithPhoneNumber:(NSString *)phoneNumber
{
    NSLog(@"Phone Number Selected : %@",phoneNumber);
    UIDevice *device = [UIDevice currentDevice];
    if ([[device model] isEqualToString:@"iPhone"] ) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",phoneNumber]]];
    } else {
        UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [Notpermitted show];
    }
}



-(void)CallCategory
{
    
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    //------------
    
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
        [manager POST:[NSString stringWithFormat:@"%@get_portfolio_category",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            
            NSLog(@"---%@",responseObject);
            if ([[responseObject valueForKey:@"status"] intValue]==1) {
                resFlag=1;
                arrimgnm=[responseObject valueForKey:@"data"];
            }
            [APP_DELEGATE hideLoadingView];
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [APP_DELEGATE hideLoadingView];
        }];
   
    
    
}
-(void)CallGetPortfolio
{
    
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    //------------
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@get_portfolio",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSLog(@"---%@",responseObject);
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
           
            dictres=[[NSMutableDictionary alloc] init];
            dictres=[responseObject valueForKey:@"data"];
            arrdictres=[[NSMutableArray alloc]initWithArray:[responseObject valueForKey:@"data"]];
         //[self performSelector:@selector(setHidden:) withObject:self afterDelay:3.0];
            
            
            for (int i=0; i<dictres.count; i++) {
                
                if ([[[dictres valueForKey:@"PortfolioCategoryName"] objectAtIndex:i]isEqualToString:@"Photos"])
                {
                    [AddNew insertObject:[[dictres  valueForKey:@"ImageURL"]objectAtIndex:i] atIndex:AddNew.count];
                    //[AddNewnm insertObject:[[dictres  valueForKey:@"PortfolioName"]objectAtIndex:i] atIndex:1];
                    [AddNewnm insertObject:[arrdictres objectAtIndex:i] atIndex:AddNewnm.count];
                }
                else if ([[[dictres valueForKey:@"PortfolioCategoryName"] objectAtIndex:i]isEqualToString:@"Feature Films"])
                {
                    [FFilms insertObject:[[dictres  valueForKey:@"URL"]objectAtIndex:i] atIndex:FFilms.count];
                    [FFilmsnm insertObject:[arrdictres objectAtIndex:i] atIndex:FFilmsnm.count];
                }
                else if ([[[dictres valueForKey:@"PortfolioCategoryName"] objectAtIndex:i]isEqualToString:@"Short Films"])
                {
                    [SFilms insertObject:[[dictres  valueForKey:@"URL"]objectAtIndex:i] atIndex:SFilms.count];
                    [SFilmsnm insertObject:[arrdictres objectAtIndex:i] atIndex:SFilmsnm.count];
                }
                else if ([[[dictres valueForKey:@"PortfolioCategoryName"] objectAtIndex:i]isEqualToString:@"Reel"])
                {
                    [Reel insertObject:[[dictres  valueForKey:@"URL"]objectAtIndex:i] atIndex:Reel.count];
                    [Reelnm insertObject:[arrdictres objectAtIndex:i] atIndex:Reelnm.count];
                }
                else if ([[[dictres valueForKey:@"PortfolioCategoryName"] objectAtIndex:i]isEqualToString:@"BTS"])
                {
                    NSString *url=[NSString stringWithFormat:@"%@",[[dictres  valueForKey:@"ImageURL"]objectAtIndex:i]];
                    NSString *extension = [url pathExtension];
                  
                    if ([extension isEqualToString:@"jpg"]) {
                        [BTS insertObject:[[dictres  valueForKey:@"ImageURL"]objectAtIndex:i] atIndex:BTS.count];
                        [BTSnm insertObject:[arrdictres objectAtIndex:i] atIndex:BTSnm.count];
                    }
                    else
                    {
                        [BTS insertObject:[[dictres  valueForKey:@"URL"]objectAtIndex:i] atIndex:BTS.count];
                        [BTSnm insertObject:[arrdictres objectAtIndex:i] atIndex:BTSnm.count];
                    }
                    
                    
                }
                else if ([[[dictres valueForKey:@"PortfolioCategoryName"] objectAtIndex:i]isEqualToString:@"Videos"])
                {
                    [Video insertObject:[[dictres  valueForKey:@"URL"]objectAtIndex:i] atIndex:Video.count];
                    [Videonm insertObject:[arrdictres objectAtIndex:i] atIndex:Videonm.count];
                }
                else if ([[[dictres valueForKey:@"PortfolioCategoryName"] objectAtIndex:i]isEqualToString:@"Head Shots"])
                {
                    [HShots insertObject:[[dictres  valueForKey:@"ImageURL"]objectAtIndex:i] atIndex:HShots.count];
                    [HShotsnm insertObject:[arrdictres objectAtIndex:i] atIndex:HShotsnm.count];
                }
                
            }
            [_collec_vw reloadData];
            [APP_DELEGATE hideLoadingView];
           //[AddNew insertObject:originalImage atIndex:1];
           
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
    
    
}


#pragma View-carousal
- (IBAction)reloadCarousel
{
    _wrap = YES;
    _vw_caro.type = iCarouselTypeRotary;
    [_vw_caro reloadData];
}

- (void)setUp
{
    //set up data
    _wrap = YES;
    _vw_caro.type = iCarouselTypeRotary;
    self.items = [NSMutableArray array];
    for (int i = 0; i < 7; i++)
    {
        [_items addObject:@(i)];
    }
    arrimg=[[NSMutableArray alloc]initWithObjects:@"add-cover.png",@"add-cover.png",@"add-cover.png",@"add-cover.png",@"add-cover.png",@"add-cover.png",@"add-cover.png", nil];
    
    arrimgnm=[[NSMutableArray alloc]initWithObjects:@"Photos",@"Feature Films",@"Short Films",@"Reel",@"BTS",@"Videos",@"Head Shots", nil];
    
    //arrimgnm=[[NSMutableArray alloc]init];

    
    
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder]))
    {
        [self setUp];
    }
    return self;
}

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return [_items count];
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    int k=carousel.currentItemIndex;
    

    //NSLog(@"carousel---  %@",carousel.dataSource);
    
    NSMutableArray *arrtemp=[[NSMutableArray alloc]init];
    arrtemp=carousel.dataSource;
    
    if (resFlag==0) {
        _lbl_cover_title.text=[NSString stringWithFormat:@"%@",[[arrtemp valueForKey:@"arrimgnm"]objectAtIndex:k]];
    }
    else
    {
        _lbl_cover_title.text=[NSString stringWithFormat:@"%@",[[[arrtemp valueForKey:@"arrimgnm"]objectAtIndex:k] valueForKey:@"PortfolioCategoryName"]];
    }
    
    
    
    
    //_lbl_cover_title.text=[arrtemp valueForKey:@"arrimgnm"]
    
    
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            return _wrap;
        }
        case iCarouselOptionFadeMax:
        {
            if (carousel.type == iCarouselTypeCustom)
            {
                NSLog(@"carousel  %@",carousel.dataSource);
                return 0.0f;
            }
            return value;
        }
        case iCarouselOptionArc:
        {
            return 2 * M_PI * 0.33400;
        }
        case iCarouselOptionRadius:
        {
            return value * 0.467433;
        }
        case iCarouselOptionTilt:
        {
            return 1;
        }
        case iCarouselOptionSpacing:
        {
            return value * 0.277052;
        }
        default:
        {
            return value;
        }
    }
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)Iview
{
    
    if (Iview == nil)
    {
        Iview=[[UIView alloc] initWithFrame:CGRectMake(0,0,200,200)];
        Iview.layer.cornerRadius=15.0;
        
        UIImageView *dot =[[UIImageView alloc] initWithFrame:Iview.bounds];
        
        dot.layer.backgroundColor=[UIColor whiteColor].CGColor;
        dot.layer.borderWidth=1.5;
        dot.layer.borderColor=[UIColor colorWithHex:0xffffff].CGColor;
        
        
      
        
        NSString *srr=[NSString stringWithFormat:@"%@",arrimg[index]];
        if ([srr isEqualToString:@"add-cover.png"]) {
              dot.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",arrimg[index]]];
        }
        else
        {
            //dot.image = arrimg[index];
            [dot sd_setImageWithURL:[NSURL URLWithString:arrimg[index]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
        }
        
        [Iview addSubview:dot];
    }
    else
    {
        _lbl_cover_title = (UILabel *)[Iview viewWithTag:1];
    }
    return Iview;
}




#pragma mark - GetAllPosts
-(void)CallMyMethod
{
    
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"profile_id"];
    
    //------------
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@user_profile",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        _responcedic=responseObject;
        
        NSMutableDictionary *dict=[responseObject valueForKey:@"data"];
        
        _lbl_username.text=[NSString stringWithFormat:@"%@ %@",[dict valueForKey:@"FName"],[dict valueForKey:@"LName"]];
        
        NSArray *arrT=[dict valueForKey:@"ProfessionData"];
        
        _lbl_profession.text=[arrT componentsJoinedByString:@","];
        NSMutableArray *NewArr = [[NSMutableArray alloc] init];
        for (int i=0; i<arrT.count; i++) {
            _lbl_profession.text=[arrT componentsJoinedByString:@","];
            [NewArr addObject:[[arrT objectAtIndex:i] valueForKey:@"Profession"]];
        }
        _lbl_profession.text=[NewArr componentsJoinedByString:@","];
        
        
        CGRect autosizeprof = _lbl_profession.frame;
        autosizeprof.size.height = [self heightForText:_lbl_profession.text];
        _lbl_profession.frame = autosizeprof;
        
        [_lbl_profession setNumberOfLines:0];
        [_lbl_profession sizeToFit];
        
       
        _lbl_location.text=[NSString stringWithFormat:@"%@,%@",[dict valueForKey:@"CountryName"],[dict valueForKey:@"CityName"]];
        
        [_img_profile sd_setImageWithURL:[NSURL URLWithString:[dict valueForKey:@"ImageURL"]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
       
        img_prof=[[NSArray alloc]initWithObjects:dict, nil];
        
     NSString *string = [NSString stringWithFormat:@"%@",[dict valueForKey:@"ShortDesc"]];

       
        [_lbl_sbio setText:@""];
        //[_lbl_sbio setText:string];
        
        
        
        _lbl_sbio.userInteractionEnabled = YES;
       // [_lbl_sbio addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoctorsClick)]];
        
        CGRect autosize4 = _lbl_sbio.frame;
        autosize4.size.height = [self heightForText:[NSString stringWithFormat:@"%@",[dict valueForKey:@"ShortDesc"]]];
        _lbl_sbio.frame = autosize4;
        
        
        _lbl_sbio.numberOfLines = 0;
        _lbl_sbio.lineBreakMode = NSLineBreakByWordWrapping;
        _lbl_sbio.font = [UIFont systemFontOfSize:12];
        _lbl_sbio.text = string;
        _lbl_sbio.backgroundColor = [UIColor clearColor];
        [_lbl_sbio sizeToFit];
        
        
       
        
        CGRect autosize7 = _lbl_sbio.frame;
        autosize7.origin.y = _lbl_profession.frame.origin.y + _lbl_profession.frame.size.height + 4;
        _lbl_sbio.frame = autosize7;
        
        
        CGRect frameRect = _scroll_vw.frame;
        frameRect.origin.y=self.lbl_sbio.frame.origin.y+self.lbl_sbio.frame.size.height+12;
        _scroll_vw.frame = frameRect;
        
        
        //AGE 
        _vw_gen_age.hidden=YES;
        CGRect border = _vw_address.frame;
        border.origin.x = _vw_gen_age.frame.origin.x;
        _vw_address.frame = border;
        
      
        NSString *emojiEscaped = @"Youtube\\ud83d\\ude27\\ud83d\\ude2e\\ud83d\\ude2f";
        NSData *emojiData = [emojiEscaped dataUsingEncoding:NSUTF8StringEncoding];
        NSString *emojiString = [[NSString alloc] initWithData:emojiData encoding:NSNonLossyASCIIStringEncoding];
        NSLog(@"emojiString: %@", emojiString);
        
        
        [APP_DELEGATE hideLoadingView];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];

    
}

-(void)handleDoctorsClick
{
    NSLog(@"nice");
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.daledietrich.com"]];
}

#pragma mark- Wrapper Class Delegate Methods
//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary* )dicsResponse
{
    if(wrapper == httpnewprof && httpnewprof != nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        [APP_DELEGATE hideLoadingView];
        
        if ([[dicsResponse valueForKey:@"status"] integerValue] == 0) {
            
            NSLog(@"Data fatch error!!!");
            
        }
        else
        {
         /*   _responcedic=dicsResponse;
            
            NSMutableDictionary *dict=[dicsResponse valueForKey:@"data"];
            _lbl_username.text=[NSString stringWithFormat:@"%@ %@",[dict valueForKey:@"FName"],[dict valueForKey:@"LName"]];
            
            NSArray *arrT=[dict valueForKey:@"ProfessionData"];
            
            _lbl_profession.text=[arrT componentsJoinedByString:@","];
            NSMutableArray *NewArr = [[NSMutableArray alloc] init];
            for (int i=0; i<arrT.count; i++) {
                _lbl_profession.text=[arrT componentsJoinedByString:@","];
                [NewArr addObject:[[arrT objectAtIndex:i] valueForKey:@"Profession"]];
            }
            _lbl_profession.text=[NewArr componentsJoinedByString:@","];
            
            _lbl_location.text=[NSString stringWithFormat:@"%@,%@",[dict valueForKey:@"CountryName"],[dict valueForKey:@"CityName"]];
            
            [_img_profile sd_setImageWithURL:[NSURL URLWithString:[dict valueForKey:@"ImageURL"]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
          */
        }

    }
}


//fetchDataFail
- (void) HttpWrapper:(HttpWrapper* )wrapper fetchDataFail:(NSError* )error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self customSetup];
    //[self CallGetPortfolio];
    if (AddNew.count <= 0) {
        //Array Allocation
        AddNew = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
    }
    
    
    if (AddNew_movie.count <= 0) {
        //Array Allocation
        AddNew_movie = [[NSMutableArray alloc] initWithObjects:@"addNewMovie", nil];
    }
    
    if (AddNew_sf.count <= 0) {
        //Array Allocation
        AddNew_sf = [[NSMutableArray alloc] initWithObjects:@"addNewShortFilm", nil];
    }
    
    ////-----------------------
    
    if (FFilms.count <= 0) {
        //Array Allocation
        FFilms = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
    }
    
    if (SFilms.count <= 0) {
        //Array Allocation
        SFilms = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
    }
    
    if (Reel.count <= 0) {
        //Array Allocation
        Reel = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
    }
    
    if (BTS.count <= 0) {
        //Array Allocation
        BTS = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
    }
    
    if (Video.count <= 0) {
        //Array Allocation
        Video = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
    }
    
    if (HShots.count <= 0) {
        //Array Allocation
        HShots = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
    }
    

}
*/

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    
   /* if (collectionView==_collec_vw) {
        return AddNew.count;
    }
    else if (collectionView==collectinmovie)
    {
        return AddNew_movie.count;
    }
    else if (collectionView==collection_SF)
    {
        return AddNew_sf.count;
    }*/
    
    if ([_lbl_cat_title.text isEqualToString:@"Photos"])
    {
        return AddNew.count;
    }
    else if ([_lbl_cat_title.text isEqualToString:@"Feature Films"])
    {
        return FFilms.count;
    }
    else if ([_lbl_cat_title.text isEqualToString:@"Short Films"])
    {
        return SFilms.count;
    }
    else if ([_lbl_cat_title.text isEqualToString:@"Reel"])
    {
        return Reel.count;
    }
    else if ([_lbl_cat_title.text isEqualToString:@"BTS"])
    {
        return BTS.count;
    }
    else if ([_lbl_cat_title.text isEqualToString:@"Videos"])
    {
        return Video.count;
    }
    else if ([_lbl_cat_title.text isEqualToString:@"Head Shots"])
    {
        return HShots.count;
    }
    
    return nil;
}

- (NSString *)extractYoutubeIdFromLink:(NSString *)link {
    NSString *regexString = @"((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)";
    NSRegularExpression *regExp = [NSRegularExpression regularExpressionWithPattern:regexString
                                                                            options:NSRegularExpressionCaseInsensitive
                                                                              error:nil];
    
    NSArray *array = [regExp matchesInString:link options:0 range:NSMakeRange(0,link.length)];
    if (array.count > 0) {
        NSTextCheckingResult *result = array.firstObject;
        NSString* thumbImageUrl = [NSString stringWithFormat:@"https://img.youtube.com/vi/%@/default.jpg",[link substringWithRange:result.range]];
        return thumbImageUrl;
    }
    return nil;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([_lbl_cat_title.text isEqualToString:@"Photos"])
    {
        NewProfileCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        cell = nil;
        if (cell == nil)
        {
            cell=(NewProfileCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        }
        cell.img_movi_image.clipsToBounds = YES;
        cell.img_movi_image.layer.masksToBounds = YES;
        cell.img_movi_image.layer.cornerRadius = 10.0f;
        cell.layer.cornerRadius = 10.0f;
        
        NSLog(@"AddNew %@",AddNew);
        if ([AddNew[indexPath.row] isEqual:@"addNew"]) {
            cell.img_movi_image.image = [UIImage imageNamed:@"add-photo-withplus"];
            cell.btn_close_b.hidden = YES;
            cell.lbl_movie_title.hidden = YES;
            cell.btn_play_b.hidden=YES;
        }
        else
        {
            cell.btn_close_b.hidden = NO;
            cell.lbl_movie_title.hidden = YES;
            cell.btn_play_b.hidden=YES;
            cell.btn_close_b.tag = indexPath.row;
            [cell.btn_close_b addTarget:self action:@selector(masterAction:) forControlEvents:UIControlEventTouchUpInside];
            //cell.img_movi_image.image = AddNew[indexPath.row];
            
            [cell.img_movi_image sd_setImageWithURL:AddNew[indexPath.row] placeholderImage:[UIImage imageNamed:@"Avatar"]];
            //cell.Img_Photos.image=[UIImage imageNamed:[AddNew objectAtIndex:indexPath.row]];
        }
        return cell;
    }
    else if ([_lbl_cat_title.text isEqualToString:@"Feature Films"])
    {
        NewProfileCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        cell = nil;
        if (cell == nil)
        {
            cell=(NewProfileCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        }
        cell.img_movi_image.clipsToBounds = YES;
        cell.img_movi_image.layer.masksToBounds = YES;
        cell.img_movi_image.layer.cornerRadius = 10.0f;
        cell.layer.cornerRadius = 10.0f;
        
        NSLog(@"FFilms %@",FFilms);
        if ([FFilms[indexPath.row] isEqual:@"addNew"]) {
            cell.img_movi_image.image = [UIImage imageNamed:@"add-video-withplus"];
            cell.btn_close_b.hidden = YES;
            cell.lbl_movie_title.hidden = YES;
            cell.btn_play_b.hidden=YES;
        }
        else
        {
            cell.btn_close_b.hidden = NO;
            cell.lbl_movie_title.hidden = NO;
            cell.btn_play_b.hidden=NO;
            cell.btn_close_b.tag = indexPath.row;
            [cell.btn_close_b addTarget:self action:@selector(masterAction:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.btn_play_b.tag = indexPath.row;
            [cell.btn_play_b addTarget:self action:@selector(masterActionVideo:) forControlEvents:UIControlEventTouchUpInside];
            cell.lbl_movie_title.text=[NSString stringWithFormat:@"%@",[[FFilmsnm objectAtIndex:indexPath.row] valueForKey:@"PortfolioName"]];
           
            
            
            //cell.img_movi_image.image = AddNew[indexPath.row];
            //[cell.img_movi_image sd_setImageWithURL:AddNew[indexPath.row] placeholderImage:[UIImage imageNamed:@"Avatar"]];
            
            //NSString *strurl=[FFilms objectAtIndex:indexPath.row];
            //NSURL *movieURL = [NSURL URLWithString:strurl];
            //cell.img_movi_image.image= [self imageFromMovie:movieURL atTime:5.0];
            //cell.Img_Photos.image=[UIImage imageNamed:[AddNew objectAtIndex:indexPath.row]];
            NSString *str=[NSString stringWithFormat:@"%@", [FFilms objectAtIndex:indexPath.row]];  //is
           
            [cell.img_movi_image sd_setImageWithURL:[NSString stringWithFormat:@"%@",[self extractYoutubeIdFromLink:str]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
            
            
            /* if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
                
                                   //Thard party thambanil
                        _youTubeVideo = [[YKYouTubeVideo alloc] initWithContent:[NSURL URLWithString:[FFilms objectAtIndex:indexPath.row]]];
                            [_youTubeVideo parseWithCompletion:^(NSError *error) {
                            [_youTubeVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                                        cell.img_movi_image.image = thumbImage;
                            }];
                        }];
            }
            else
            {
                //NSURL *movieURL = [NSURL URLWithString:[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
                _directVideo = [[YKDirectVideo alloc] initWithContent:[NSURL URLWithString:[FFilms objectAtIndex:indexPath.row]]];
                [_directVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                    cell.img_movi_image.image = thumbImage;
                }];
            }*/
            
        }
        return cell;
    }
    else if ([_lbl_cat_title.text isEqualToString:@"Short Films"])
    {
        NewProfileCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        cell = nil;
        if (cell == nil)
        {
            cell=(NewProfileCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        }
        cell.img_movi_image.clipsToBounds = YES;
        cell.img_movi_image.layer.masksToBounds = YES;
        cell.img_movi_image.layer.cornerRadius = 10.0f;
        cell.layer.cornerRadius = 10.0f;
        
        NSLog(@"SFilms %@",SFilms);
        if ([SFilms[indexPath.row] isEqual:@"addNew"]) {
            cell.img_movi_image.image = [UIImage imageNamed:@"add-video-withplus"];
            cell.btn_close_b.hidden = YES;
            cell.lbl_movie_title.hidden = YES;
            cell.btn_play_b.hidden=YES;
        }
        else
        {
            
            cell.btn_close_b.hidden = NO;
            cell.lbl_movie_title.hidden = NO;
            cell.btn_play_b.hidden=NO;
            cell.btn_close_b.tag = indexPath.row;
            [cell.btn_close_b addTarget:self action:@selector(masterAction:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.btn_play_b.tag = indexPath.row;
            [cell.btn_play_b addTarget:self action:@selector(masterActionVideo:) forControlEvents:UIControlEventTouchUpInside];
             cell.lbl_movie_title.text=[NSString stringWithFormat:@"%@",[[SFilmsnm objectAtIndex:indexPath.row] valueForKey:@"PortfolioName"]];
            //cell.img_movi_image.image = AddNew[indexPath.row];
            //[cell.img_movi_image sd_setImageWithURL:AddNew[indexPath.row] placeholderImage:[UIImage imageNamed:@"Avatar"]];
           // cell.img_movi_image.image= [self imageFromMovie:[SFilms objectAtIndex:indexPath.row] atTime:5.0];
            //NSString *strurl=[SFilms objectAtIndex:indexPath.row];
            //NSURL *movieURL = [NSURL URLWithString:strurl];
            //cell.img_movi_image.image= [self imageFromMovie:movieURL atTime:5.0];
            //cell.Img_Photos.image=[UIImage imageNamed:[AddNew objectAtIndex:indexPath.row]];
            
            NSString *str=[NSString stringWithFormat:@"%@", [SFilms objectAtIndex:indexPath.row]];  //is
            
            [cell.img_movi_image sd_setImageWithURL:[NSString stringWithFormat:@"%@",[self extractYoutubeIdFromLink:str]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
            
           /* NSString *str=[NSString stringWithFormat:@"%@", [SFilms objectAtIndex:indexPath.row]];  //is your str
            if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
                
                //Thard party thambanil
                _youTubeVideo = [[YKYouTubeVideo alloc] initWithContent:[NSURL URLWithString:[SFilms objectAtIndex:indexPath.row]]];
                [_youTubeVideo parseWithCompletion:^(NSError *error) {
                    [_youTubeVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                        cell.img_movi_image.image = thumbImage;
                    }];
                }];
            }
            else
            {
                //NSURL *movieURL = [NSURL URLWithString:[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
                _directVideo = [[YKDirectVideo alloc] initWithContent:[NSURL URLWithString:[SFilms objectAtIndex:indexPath.row]]];
                [_directVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                    cell.img_movi_image.image = thumbImage;
                }];
            }*/
            
            
            
            
        }
        return cell;
    }
    else if ([_lbl_cat_title.text isEqualToString:@"Reel"])
    {
        NewProfileCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        cell = nil;
        if (cell == nil)
        {
            cell=(NewProfileCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        }
        cell.img_movi_image.clipsToBounds = YES;
        cell.img_movi_image.layer.masksToBounds = YES;
        cell.img_movi_image.layer.cornerRadius = 10.0f;
        cell.layer.cornerRadius = 10.0f;
        
        NSLog(@"Reel %@",Reel);
        if ([Reel[indexPath.row] isEqual:@"addNew"]) {
            cell.img_movi_image.image = [UIImage imageNamed:@"add-video-withplus"];
            cell.btn_close_b.hidden = YES;
            cell.lbl_movie_title.hidden = YES;
            cell.btn_play_b.hidden=YES;
        }
        else
        {
            cell.btn_close_b.hidden = NO;
            cell.lbl_movie_title.hidden = NO;
            cell.btn_play_b.hidden=NO;
            cell.btn_close_b.tag = indexPath.row;
            [cell.btn_close_b addTarget:self action:@selector(masterAction:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.btn_play_b.tag = indexPath.row;
            [cell.btn_play_b addTarget:self action:@selector(masterActionVideo:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.lbl_movie_title.text=[NSString stringWithFormat:@"%@",[[Reelnm objectAtIndex:indexPath.row] valueForKey:@"PortfolioName"]];
            //cell.img_movi_image.image = AddNew[indexPath.row];
            //[cell.img_movi_image sd_setImageWithURL:AddNew[indexPath.row] placeholderImage:[UIImage imageNamed:@"Avatar"]];
            //cell.img_movi_image.image= [self imageFromMovie:[Reel objectAtIndex:indexPath.row] atTime:5.0];
            //NSString *strurl=[Reel objectAtIndex:indexPath.row];
            //NSURL *movieURL = [NSURL URLWithString:strurl];
            //cell.img_movi_image.image= [self imageFromMovie:movieURL atTime:5.0];
            //cell.Img_Photos.image=[UIImage imageNamed:[AddNew objectAtIndex:indexPath.row]];
            
            NSString *str=[NSString stringWithFormat:@"%@", [Reel objectAtIndex:indexPath.row]];  //is your str
            
            [cell.img_movi_image sd_setImageWithURL:[NSString stringWithFormat:@"%@",[self extractYoutubeIdFromLink:str]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
           
            
            /*if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
                
                //Thard party thambanil
                _youTubeVideo = [[YKYouTubeVideo alloc] initWithContent:[NSURL URLWithString:[Reel objectAtIndex:indexPath.row]]];
                [_youTubeVideo parseWithCompletion:^(NSError *error) {
                    [_youTubeVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                        cell.img_movi_image.image = thumbImage;
                    }];
                }];
            }
            else
            {
                //NSURL *movieURL = [NSURL URLWithString:[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
                _directVideo = [[YKDirectVideo alloc] initWithContent:[NSURL URLWithString:[Reel objectAtIndex:indexPath.row]]];
                [_directVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                    cell.img_movi_image.image = thumbImage;
                }];
            }*/
            
            
        }
        return cell;
    }
    else if ([_lbl_cat_title.text isEqualToString:@"BTS"])
    {
        NewProfileCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        cell = nil;
        if (cell == nil)
        {
            cell=(NewProfileCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        }
        cell.img_movi_image.clipsToBounds = YES;
        cell.img_movi_image.layer.masksToBounds = YES;
        cell.img_movi_image.layer.cornerRadius = 10.0f;
        cell.layer.cornerRadius = 10.0f;
        
        NSLog(@"BTS %@",BTS);
        if ([BTS[indexPath.row] isEqual:@"addNew"]) {
            cell.img_movi_image.image = [UIImage imageNamed:@"add-video-withplus-Image"];
            cell.btn_close_b.hidden = YES;
            cell.lbl_movie_title.hidden = YES;
            cell.btn_play_b.hidden=YES;
        }
        else
        {
            
            
            
            cell.btn_close_b.hidden = NO;
            cell.lbl_movie_title.hidden = NO;
            cell.btn_play_b.hidden=NO;
            cell.btn_close_b.tag = indexPath.row;
            [cell.btn_close_b addTarget:self action:@selector(masterAction:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.btn_play_b.tag = indexPath.row;
            [cell.btn_play_b addTarget:self action:@selector(masterActionVideo:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.lbl_movie_title.text=[NSString stringWithFormat:@"%@",[[BTSnm objectAtIndex:indexPath.row] valueForKey:@"PortfolioName"]];
            //cell.img_movi_image.image = AddNew[indexPath.row];
            //[cell.img_movi_image sd_setImageWithURL:AddNew[indexPath.row] placeholderImage:[UIImage imageNamed:@"Avatar"]];
            //cell.img_movi_image.image= [self imageFromMovie:[BTS objectAtIndex:indexPath.row] atTime:5.0];
            //NSString *strurl=[BTS objectAtIndex:indexPath.row];
            //NSURL *movieURL = [NSURL URLWithString:strurl];
            //cell.img_movi_image.image= [self imageFromMovie:movieURL atTime:5.0];
            //cell.Img_Photos.image=[UIImage imageNamed:[AddNew objectAtIndex:indexPath.row]];
            
            NSString *str=[NSString stringWithFormat:@"%@", [BTS objectAtIndex:indexPath.row]];  //is your str
            
            NSString *extension = [str pathExtension];
            
            if ([extension isEqualToString:@"jpg"])
            {
                [cell.img_movi_image sd_setImageWithURL:BTS[indexPath.row] placeholderImage:[UIImage imageNamed:@"Avatar"]];
                cell.btn_play_b.hidden=YES;
            }
            else
            {
                [cell.img_movi_image sd_setImageWithURL:[NSString stringWithFormat:@"%@",[self extractYoutubeIdFromLink:str]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
                
                
            }
            
            
            
            
            
            
            /*if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
                
                //Thard party thambanil
                _youTubeVideo = [[YKYouTubeVideo alloc] initWithContent:[NSURL URLWithString:[BTS objectAtIndex:indexPath.row]]];
                [_youTubeVideo parseWithCompletion:^(NSError *error) {
                    [_youTubeVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                        cell.img_movi_image.image = thumbImage;
                    }];
                }];
            }
            else
            {
                //NSURL *movieURL = [NSURL URLWithString:[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
                _directVideo = [[YKDirectVideo alloc] initWithContent:[NSURL URLWithString:[BTS objectAtIndex:indexPath.row]]];
                [_directVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                    cell.img_movi_image.image = thumbImage;
                }];
            }*/
            
        }
        return cell;
    }
    else if ([_lbl_cat_title.text isEqualToString:@"Videos"])
    {
        NewProfileCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        cell = nil;
        if (cell == nil)
        {
            cell=(NewProfileCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        }
        cell.img_movi_image.clipsToBounds = YES;
        cell.img_movi_image.layer.masksToBounds = YES;
        cell.img_movi_image.layer.cornerRadius = 10.0f;
        cell.layer.cornerRadius = 10.0f;
        
        NSLog(@"Videos %@",Video);
        if ([Video[indexPath.row] isEqual:@"addNew"]) {
            cell.img_movi_image.image = [UIImage imageNamed:@"add-video-withplus"];
            cell.btn_close_b.hidden = YES;
            cell.lbl_movie_title.hidden = YES;
            cell.btn_play_b.hidden=YES;
        }
        else
        {
            cell.btn_close_b.hidden = NO;
            cell.lbl_movie_title.hidden = NO;
            cell.btn_play_b.hidden=NO;
            cell.btn_close_b.tag = indexPath.row;
            [cell.btn_close_b addTarget:self action:@selector(masterAction:) forControlEvents:UIControlEventTouchUpInside];
           
            cell.btn_play_b.tag = indexPath.row;
            [cell.btn_play_b addTarget:self action:@selector(masterActionVideo:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.lbl_movie_title.text=[NSString stringWithFormat:@"%@",[[Videonm objectAtIndex:indexPath.row] valueForKey:@"PortfolioName"]];
            //cell.img_movi_image.image = AddNew[indexPath.row];
            //[cell.img_movi_image sd_setImageWithURL:AddNew[indexPath.row] placeholderImage:[UIImage imageNamed:@"Avatar"]];
            //cell.img_movi_image.image= [self imageFromMovie:[Video objectAtIndex:indexPath.row] atTime:5.0];
            //NSString *strurl=[Video objectAtIndex:indexPath.row];
            //NSURL *movieURL = [NSURL URLWithString:strurl];
            //cell.img_movi_image.image= [self imageFromMovie:movieURL atTime:5.0];
            //cell.Img_Photos.image=[UIImage imageNamed:[AddNew objectAtIndex:indexPath.row]];
            
            NSString *str=[NSString stringWithFormat:@"%@", [Video objectAtIndex:indexPath.row]];  //is your str
            [cell.img_movi_image sd_setImageWithURL:[NSString stringWithFormat:@"%@",[self extractYoutubeIdFromLink:str]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
            
            
            /*if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
                
                //Thard party thambanil
                _youTubeVideo = [[YKYouTubeVideo alloc] initWithContent:[NSURL URLWithString:[Video objectAtIndex:indexPath.row]]];
                [_youTubeVideo parseWithCompletion:^(NSError *error) {
                    [_youTubeVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                        cell.img_movi_image.image = thumbImage;
                    }];
                }];
            }
            else
            {
                //NSURL *movieURL = [NSURL URLWithString:[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
                _directVideo = [[YKDirectVideo alloc] initWithContent:[NSURL URLWithString:[Video objectAtIndex:indexPath.row]]];
                [_directVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                    cell.img_movi_image.image = thumbImage;
                }];
            }*/
            
            
            
        }
        return cell;
    }
    else if ([_lbl_cat_title.text isEqualToString:@"Head Shots"])
    {
        //ImageURL
        
        NewProfileCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        cell = nil;
        if (cell == nil)
        {
            cell=(NewProfileCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        }
        cell.img_movi_image.clipsToBounds = YES;
        cell.img_movi_image.layer.masksToBounds = YES;
        cell.img_movi_image.layer.cornerRadius = 10.0f;
        cell.layer.cornerRadius = 10.0f;
        
        
        NSLog(@"HShots %@",HShots);
        if ([HShots[indexPath.row] isEqual:@"addNew"]) {
            cell.img_movi_image.image = [UIImage imageNamed:@"add-photo-withplus"];
            cell.btn_close_b.hidden = YES;
            cell.lbl_movie_title.hidden = YES;
            cell.btn_play_b.hidden=YES;
        }
        else
        {
            
            [cell.img_movi_image sd_setImageWithURL:HShots[indexPath.row] placeholderImage:[UIImage imageNamed:@"Avatar"]];
            
            //cell.btn_close_b.hidden = NO;
            //cell.lbl_movie_title.hidden = NO;
            cell.btn_play_b.hidden=YES;
            //cell.btn_close_b.tag = indexPath.row;
            ////[cell.btn_close_b addTarget:self action:@selector(masterAction:) forControlEvents:UIControlEventTouchUpInside];
            
            //cell.btn_play_b.tag = indexPath.row;
            //[cell.btn_play_b addTarget:self action:@selector(masterActionVideo:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.lbl_movie_title.text=[NSString stringWithFormat:@"%@",[[HShotsnm objectAtIndex:indexPath.row] valueForKey:@"PortfolioName"]];
            //cell.img_movi_image.image = AddNew[indexPath.row];
            //[cell.img_movi_image sd_setImageWithURL:AddNew[indexPath.row] placeholderImage:[UIImage imageNamed:@"Avatar"]];
            //cell.img_movi_image.image= [self imageFromMovie:[HShots objectAtIndex:indexPath.row] atTime:5.0];
            //NSString *strurl=[HShots objectAtIndex:indexPath.row];
            //NSURL *movieURL = [NSURL URLWithString:strurl];
            //cell.img_movi_image.image= [self imageFromMovie:movieURL atTime:5.0];
            //cell.Img_Photos.image=[UIImage imageNamed:[AddNew objectAtIndex:indexPath.row]];
            
            //NSString *str=[NSString stringWithFormat:@"%@", [HShots objectAtIndex:indexPath.row]];  //is your str
           // [cell.img_movi_image sd_setImageWithURL:[NSString stringWithFormat:@"%@",[self extractYoutubeIdFromLink:str]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
            
            
            /*if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
                
                //Thard party thambanil
                _youTubeVideo = [[YKYouTubeVideo alloc] initWithContent:[NSURL URLWithString:[HShots objectAtIndex:indexPath.row]]];
                [_youTubeVideo parseWithCompletion:^(NSError *error) {
                    [_youTubeVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                        cell.img_movi_image.image = thumbImage;
                    }];
                }];
            }
            else
            {
                //NSURL *movieURL = [NSURL URLWithString:[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
                _directVideo = [[YKDirectVideo alloc] initWithContent:[NSURL URLWithString:[HShots objectAtIndex:indexPath.row]]];
                [_directVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                    cell.img_movi_image.image = thumbImage;
                }];
            }*/
        }
        return cell;
    }
    
    
    return nil;
}
//didSelectItemAtIndexPath
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([_lbl_cat_title.text isEqualToString:@"Photos"])
    {
        if ([AddNew[indexPath.row] isEqual:@"addNew"]) {
            AddPhotosVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AddPhotosVC"];
            next.cat_id=_lbl_cat_id.text;
            next.cat_name=_lbl_cat_title.text;
            [self.navigationController pushViewController:next animated:NO];
        }
        else
        {
           /* ProfilePhotoViewVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilePhotoViewVC"];
            vc.arrimgs=AddNewnm[indexPath.row];
            vc.view.frame = CGRectMake(0, 0, vc.view.frame.size.width, 320.0f);
            [self presentPopUpViewController:vc];
            */
            
          
            NSURL *url = [NSURL URLWithString:AddNew[indexPath.row]];
            NSData *data = [NSData dataWithContentsOfURL:url];
            UIImage* immg = [[UIImage alloc]initWithData:data];
            
            NSArray *landscapeImages=[[NSArray alloc]initWithObjects:immg, nil];
            
            
            Class cls = NSClassFromString(@"AFTNormalPagingViewController");
            AFTPagingBaseViewController *vc = [cls new];
            AFTPagingBaseViewController *bvc = (AFTPagingBaseViewController *)vc;
            
            bvc.title = @"Photo";
            bvc.images = landscapeImages;
            [self presentViewController:bvc animated:YES completion:nil];
            
        }
    }
    else if ([_lbl_cat_title.text isEqualToString:@"Feature Films"])
    {
        if ([FFilms[indexPath.row] isEqual:@"addNew"]) {
            AddPhotosVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AddPhotosVC"];
            next.cat_id=_lbl_cat_id.text;
            next.cat_name=_lbl_cat_title.text;
            [self.navigationController pushViewController:next animated:NO];
        }
        else
        {
            NSLog(@"Video Play");
        }
    }
    else if ([_lbl_cat_title.text isEqualToString:@"Short Films"])
    {
        if ([SFilms[indexPath.row] isEqual:@"addNew"]) {
            AddPhotosVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AddPhotosVC"];
            next.cat_id=_lbl_cat_id.text;
            next.cat_name=_lbl_cat_title.text;
            [self.navigationController pushViewController:next animated:NO];
        }
        else
        {
            NSLog(@"Video Play");
        }
    }
    else if ([_lbl_cat_title.text isEqualToString:@"Reel"])
    {
        if ([Reel[indexPath.row] isEqual:@"addNew"]) {
            AddPhotosVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AddPhotosVC"];
            next.cat_id=_lbl_cat_id.text;
            next.cat_name=_lbl_cat_title.text;
            [self.navigationController pushViewController:next animated:NO];
        }
        else
        {
            NSLog(@"Video Play");
        }
    }
    else if ([_lbl_cat_title.text isEqualToString:@"BTS"])
    {
        if ([BTS[indexPath.row] isEqual:@"addNew"]) {
            AddPhotosVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AddPhotosVC"];
            next.cat_id=_lbl_cat_id.text;
            next.cat_name=_lbl_cat_title.text;
            [self.navigationController pushViewController:next animated:NO];
        }
        else
        {
            
            
            NSString *str=[NSString stringWithFormat:@"%@", [BTS objectAtIndex:indexPath.row]];  //is your str
            NSString *extension = [str pathExtension];
            
            if ([extension isEqualToString:@"jpg"])
            {
                /*ProfilePhotoViewVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilePhotoViewVC"];
                vc.arrimgs=BTSnm[indexPath.row];
                vc.view.frame = CGRectMake(0, 0, vc.view.frame.size.width, 320.0f);
                [self presentPopUpViewController:vc];
                */
                
                NSURL *url = [NSURL URLWithString:BTS[indexPath.row]];
                NSData *data = [NSData dataWithContentsOfURL:url];
                UIImage* immg = [[UIImage alloc]initWithData:data];
                
                NSArray *landscapeImages=[[NSArray alloc]initWithObjects:immg, nil];
                
                
                Class cls = NSClassFromString(@"AFTNormalPagingViewController");
                AFTPagingBaseViewController *vc = [cls new];
                AFTPagingBaseViewController *bvc = (AFTPagingBaseViewController *)vc;
                
                bvc.title = @"Photo";
                bvc.images = landscapeImages;
                [self presentViewController:bvc animated:YES completion:nil];
                
                
                
                
                
                
            }
            else
            {
                NSLog(@"Video Play");
            }
        }
        
    }
    else if ([_lbl_cat_title.text isEqualToString:@"Videos"])
    {
        if ([Video[indexPath.row] isEqual:@"addNew"]) {
            AddPhotosVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AddPhotosVC"];
            next.cat_id=_lbl_cat_id.text;
            next.cat_name=_lbl_cat_title.text;
            [self.navigationController pushViewController:next animated:NO];
        }
        else
        {
            NSLog(@"Video Play");
        }
        
    }
    else if ([_lbl_cat_title.text isEqualToString:@"Head Shots"])
    {
        if ([HShots[indexPath.row] isEqual:@"addNew"]) {
            AddPhotosVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AddPhotosVC"];
            next.cat_id=_lbl_cat_id.text;
            next.cat_name=_lbl_cat_title.text;
            [self.navigationController pushViewController:next animated:NO];
        }
        else
        {
            
            
            NSLog(@"Video Play");
        }
        
    }
    
    
    
    
    
    
    /*
    if (![_lbl_cat_title.text isEqualToString:@"Photos"]) {
      
        if ([AddNew[indexPath.row] isEqual:@"addNew"])
        {
            AddPhotosVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AddPhotosVC"];
            next.cat_id=_lbl_cat_id.text;
            next.cat_name=_lbl_cat_title.text;
            [self.navigationController pushViewController:next animated:NO];
        }
        
    }
    else
    {
        if (collectionView==_collec_vw)
        {
            flag=1;
            if ([AddNew[indexPath.row] isEqual:@"addNew"]) {
                if (AddNew.count < 11) {
                    //ipc= [[UIImagePickerController alloc] init];
                    //ipc.delegate = self;
                    //ipc.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
                    //ipc.allowsEditing = NO;
                    //[self presentViewController:ipc animated:NO completion:nil];
     
                    AddPhotosVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AddPhotosVC"];
                    next.cat_id=_lbl_cat_id.text;
                    next.cat_name=_lbl_cat_title.text;
                    [self.navigationController pushViewController:next animated:NO];
                }
            }
            else
            {
                //ProfilePhotoViewVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilePhotoViewVC"];
                //next.arr_imgs = AddNew;
                
                ProfilePhotoViewVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilePhotoViewVC"];
                vc.arrimgs=AddNew[indexPath.row];
                
                vc.view.frame = CGRectMake(0, 0, vc.view.frame.size.width, 320.0f);
                [self presentPopUpViewController:vc];
            }
        }
        else if (collectionView==collectinmovie)
        {
            flag=2;
            if ([AddNew_movie[indexPath.row] isEqual:@"addNewMovie"]) {
                if (AddNew_movie.count < 11) {
                    AddPhotosVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"AddPhotosVC"];
                    [self.navigationController pushViewController:next animated:NO];
                }
            }
            
        }
        else if (collectionView==collection_SF)
        {
            flag=3;
            if ([AddNew_sf[indexPath.row] isEqual:@"addNewShortFilm"]) {
                if (AddNew_sf.count < 11) {
                    AddPhotosVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"AddPhotosVC"];
                    [self.navigationController pushViewController:next animated:NO];
                }
            }
            
        }

    }
*/
    
    
    
    
    
    
    
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        
        
    }
    else if (buttonIndex == 1)
    {
        // No, cancel etc...
        NSLog(@"Cancel logout");
    }
}

-(void)masterAction:(UIButton*)sender
{
    /*
    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setTitle:@"Alert"];
    [alert setMessage:@"Are you sure Detele!"];
    [alert setDelegate:self];
    [alert addButtonWithTitle:@"Yes"];
    [alert addButtonWithTitle:@"No"];
    [alert show];*/
    
    
    
    if ([_lbl_cat_title.text isEqualToString:@"Photos"])
    {
        UIAlertController * alert=[UIAlertController
                                   
                                   alertControllerWithTitle:@"" message:@"Are you sure you want to delete"preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Yes"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        //What we write here????????**
                                        NSLog(@"YES");
                                        [self CallDelete:[[dictres valueForKey:@"AppUserPortdolioID"]objectAtIndex:sender.tag-1]];
                                        [AddNew removeObjectAtIndex:sender.tag];
                                        [AddNewnm removeObjectAtIndex:sender.tag];
                                        [_collec_vw reloadData];
                                        
                                    }];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"No"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       //What we write here????????**
                                       NSLog(@"NO");
                                       
                                   }];
        
        [alert addAction:yesButton];
        [alert addAction:noButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else if ([_lbl_cat_title.text isEqualToString:@"Feature Films"])
    {
        UIAlertController * alert=[UIAlertController
                                   
                                   alertControllerWithTitle:@"" message:@"Are you sure you want to delete"preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Yes"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        //What we write here????????**
                                        [self CallDelete:[[dictres valueForKey:@"AppUserPortdolioID"]objectAtIndex:sender.tag-1]];
                                        [FFilms removeObjectAtIndex:sender.tag];
                                        [FFilmsnm removeObjectAtIndex:sender.tag];
                                        [_collec_vw reloadData];
                                        
                                    }];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"No"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       //What we write here????????**
                                       NSLog(@"NO");
                                   }];
        
        [alert addAction:yesButton];
        [alert addAction:noButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
        
    }
    else if ([_lbl_cat_title.text isEqualToString:@"Short Films"])
    {
        UIAlertController * alert=[UIAlertController
                                   
                                   alertControllerWithTitle:@"" message:@"Are you sure you want to delete"preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Yes"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        [self presentViewController:alert animated:YES completion:nil];
                                        [self CallDelete:[[dictres valueForKey:@"AppUserPortdolioID"]objectAtIndex:sender.tag-1]];
                                        [SFilms removeObjectAtIndex:sender.tag];
                                        [SFilmsnm removeObjectAtIndex:sender.tag];
                                        [_collec_vw reloadData];
                                        
                                    }];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"No"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       //What we write here????????**
                                       
                                   }];
        
        [alert addAction:yesButton];
        [alert addAction:noButton];
        
        
    }
    else if ([_lbl_cat_title.text isEqualToString:@"Reel"])
    {
        UIAlertController * alert=[UIAlertController
                                   
                                   alertControllerWithTitle:@"" message:@"Are you sure you want to delete"preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Yes"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        //What we write here????????**
                                        [self CallDelete:[[dictres valueForKey:@"AppUserPortdolioID"]objectAtIndex:sender.tag-1]];
                                        [Reel removeObjectAtIndex:sender.tag];
                                        [Reelnm removeObjectAtIndex:sender.tag];
                                        [_collec_vw reloadData];
                                        
                                        
                                    }];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"No"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       NSLog(@"NO");
                                       //What we write here????????**
                                   }];
        
        [alert addAction:yesButton];
        [alert addAction:noButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
       
    }
    else if ([_lbl_cat_title.text isEqualToString:@"BTS"])
    {
        UIAlertController * alert=[UIAlertController
                                   
                                   alertControllerWithTitle:@"" message:@"Are you sure you want to delete"preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Yes"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        //What we write here????????**
                                        [self CallDelete:[[dictres valueForKey:@"AppUserPortdolioID"]objectAtIndex:sender.tag-1]];
                                        [BTS removeObjectAtIndex:sender.tag];
                                        [BTSnm removeObjectAtIndex:sender.tag];
                                        [_collec_vw reloadData];
                                        
                                    }];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"No"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       //What we write here????????**
                                       NSLog(@"NO");
                                   }];
        
        [alert addAction:yesButton];
        [alert addAction:noButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }
    else if ([_lbl_cat_title.text isEqualToString:@"Videos"])
    {
        UIAlertController * alert=[UIAlertController
                                   
                                   alertControllerWithTitle:@"" message:@"Are you sure you want to delete"preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Yes"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        //What we write here????????**
                                        [self CallDelete:[[dictres valueForKey:@"AppUserPortdolioID"]objectAtIndex:sender.tag-1]];
                                        [Video removeObjectAtIndex:sender.tag];
                                        [Videonm removeObjectAtIndex:sender.tag];
                                        [_collec_vw reloadData];
                                        
                                    }];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"No"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       //What we write here????????**
                                       NSLog(@"NO");
                                   }];
        
        [alert addAction:yesButton];
        [alert addAction:noButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else if ([_lbl_cat_title.text isEqualToString:@"Head Shots"])
    {
        UIAlertController * alert=[UIAlertController
                                   
                                   alertControllerWithTitle:@"" message:@"Are you sure you want to delete"preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Yes"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        //What we write here????????**
                                        [self CallDelete:[[dictres valueForKey:@"AppUserPortdolioID"]objectAtIndex:sender.tag-1]];
                                        [HShots removeObjectAtIndex:sender.tag];
                                        [HShotsnm removeObjectAtIndex:sender.tag];
                                        [_collec_vw reloadData];
                                        
                                    }];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"No"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       //What we write here????????**
                                       NSLog(@"NO");
                                   }];
        
        [alert addAction:yesButton];
        [alert addAction:noButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
       
    }
}

-(void)masterActionVideo:(UIButton*)sender
{
    
    if ([_lbl_cat_title.text isEqualToString:@"Feature Films"])
    {
        NSString *str=[NSString stringWithFormat:@"%@", [FFilms objectAtIndex:sender.tag]];  //is your str
        if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
            
            WebPlayerVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"WebPlayerVC"];
            next.strurl=[FFilms objectAtIndex:sender.tag];
            [self.navigationController pushViewController:next animated:NO];
            
        }
        else
        {
            NSURL *videoURL = [NSURL URLWithString:[[dictres valueForKey:@"URL"]objectAtIndex:sender.tag-1]];
            AVPlayer *player = [AVPlayer playerWithURL:videoURL];
            AVPlayerViewController *avplayerViewController = [AVPlayerViewController new];
            avplayerViewController.player = player;
            //[self.view addSubview:avplayerViewController.view];
            [self presentViewController:avplayerViewController animated:YES completion:nil];
        }
    }
    else if ([_lbl_cat_title.text isEqualToString:@"Short Films"])
    {
        NSString *str=[NSString stringWithFormat:@"%@", [[dictres valueForKey:@"URL"]objectAtIndex:sender.tag-1]];  //is your str
        if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
            
            WebPlayerVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"WebPlayerVC"];
            next.strurl=[[dictres valueForKey:@"URL"]objectAtIndex:sender.tag-1];
            [self.navigationController pushViewController:next animated:NO];
            
        }
        else
        {
            NSURL *videoURL = [NSURL URLWithString:[[dictres valueForKey:@"URL"]objectAtIndex:sender.tag-1]];
            AVPlayer *player = [AVPlayer playerWithURL:videoURL];
            AVPlayerViewController *avplayerViewController = [AVPlayerViewController new];
            avplayerViewController.player = player;
            //[self.view addSubview:avplayerViewController.view];
            [self presentViewController:avplayerViewController animated:YES completion:nil];
        }
    }
    else if ([_lbl_cat_title.text isEqualToString:@"Reel"])
    {
        NSString *str=[NSString stringWithFormat:@"%@", [[dictres valueForKey:@"URL"]objectAtIndex:sender.tag-1]];  //is your str
        if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
            
            WebPlayerVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"WebPlayerVC"];
            next.strurl=[[dictres valueForKey:@"URL"]objectAtIndex:sender.tag-1];
            [self.navigationController pushViewController:next animated:NO];
            
        }
        else
        {
            NSURL *videoURL = [NSURL URLWithString:[[dictres valueForKey:@"URL"]objectAtIndex:sender.tag-1]];
            AVPlayer *player = [AVPlayer playerWithURL:videoURL];
            AVPlayerViewController *avplayerViewController = [AVPlayerViewController new];
            avplayerViewController.player = player;
            //[self.view addSubview:avplayerViewController.view];
            [self presentViewController:avplayerViewController animated:YES completion:nil];
        }
    }
    else if ([_lbl_cat_title.text isEqualToString:@"BTS"])
    {
        NSString *str=[NSString stringWithFormat:@"%@", [[dictres valueForKey:@"URL"]objectAtIndex:sender.tag-1]];  //is your str
        if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
            
            WebPlayerVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"WebPlayerVC"];
            next.strurl=[[dictres valueForKey:@"URL"]objectAtIndex:sender.tag-1];
            [self.navigationController pushViewController:next animated:NO];
        }
        else
        {
            NSURL *videoURL = [NSURL URLWithString:[[dictres valueForKey:@"URL"]objectAtIndex:sender.tag-1]];
            AVPlayer *player = [AVPlayer playerWithURL:videoURL];
            AVPlayerViewController *avplayerViewController = [AVPlayerViewController new];
            avplayerViewController.player = player;
            //[self.view addSubview:avplayerViewController.view];
            [self presentViewController:avplayerViewController animated:YES completion:nil];
        }
    }
    else if ([_lbl_cat_title.text isEqualToString:@"Videos"])
    {
        NSString *str=[NSString stringWithFormat:@"%@", [[dictres valueForKey:@"URL"]objectAtIndex:sender.tag-1]];  //is your str
        if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
            
            WebPlayerVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"WebPlayerVC"];
            next.strurl=[[dictres valueForKey:@"URL"]objectAtIndex:sender.tag-1];
            [self.navigationController pushViewController:next animated:NO];
            
        }
        else
        {
            NSURL *videoURL = [NSURL URLWithString:[[dictres valueForKey:@"URL"]objectAtIndex:sender.tag-1]];
            AVPlayer *player = [AVPlayer playerWithURL:videoURL];
            AVPlayerViewController *avplayerViewController = [AVPlayerViewController new];
            avplayerViewController.player = player;
            //[self.view addSubview:avplayerViewController.view];
            [self presentViewController:avplayerViewController animated:YES completion:nil];
        }
    }
    else if ([_lbl_cat_title.text isEqualToString:@"Head Shots"])
    {
        NSString *str=[NSString stringWithFormat:@"%@", [[dictres valueForKey:@"URL"]objectAtIndex:sender.tag-1]];  //is your str
        if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
            
            WebPlayerVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"WebPlayerVC"];
            next.strurl=[[dictres valueForKey:@"URL"]objectAtIndex:sender.tag-1];
            [self.navigationController pushViewController:next animated:NO];
            
        }
        else
        {
            NSURL *videoURL = [NSURL URLWithString:[[dictres valueForKey:@"URL"]objectAtIndex:sender.tag-1]];
            AVPlayer *player = [AVPlayer playerWithURL:videoURL];
            AVPlayerViewController *avplayerViewController = [AVPlayerViewController new];
            avplayerViewController.player = player;
            //[self.view addSubview:avplayerViewController.view];
            [self presentViewController:avplayerViewController animated:YES completion:nil];
        }
    }
}





-(void)CallDelete:(NSString *)strimgpath
{
  /*  NSString *strid=[[NSString alloc]init];
    for (int i=0; i<dictres.count; i++) {
        NSString *str=[[dictres valueForKey:@"ImageURL"] objectAtIndex:i];
        
        if ([str isEqualToString:strimgpath]) {
            NSLog(@"same");
            strid=[[dictres valueForKey:@"AppUserPortdolioID"] objectAtIndex:i];
        }
        
    }*/
    
    NSLog(@"DICTRES %@",dictres);
    NSLog(@"str ID %@",strimgpath);
    
    
    //[APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    //NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    //[AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",strimgpath] forKey:@"portfolio_id"];
    //------------
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@delete_portfolio",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSLog(@"---%@",responseObject);
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            NSLog(@"delete successfully");
        }
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
    
    
}



-(void)masterAction_m:(UIButton*)sender
{
    [AddNew_movie removeObjectAtIndex:sender.tag];
    [collectinmovie reloadData];
}
-(void)masterAction_sf:(UIButton*)sender
{
    [AddNew_sf removeObjectAtIndex:sender.tag];
    [collection_SF reloadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (UIImage* )imageFromMovie:(NSURL* )movieURL atTime:(NSTimeInterval)time {
    // set up the movie player
    MPMoviePlayerController *mp = [[MPMoviePlayerController alloc]
                                   initWithContentURL:movieURL];
    mp.shouldAutoplay = NO;
    mp.initialPlaybackTime = time;
    mp.currentPlaybackTime = time;
    // get the thumbnail
    thumbnail = [mp thumbnailImageAtTime:time
                              timeOption:MPMovieTimeOptionNearestKeyFrame];
    // clean up the movie player
    [mp stop];
    return(thumbnail);
}
-(void)CallGetportfoliocategorydetails
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    //------------
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@get_portfolio_category_details",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSLog(@"---%@",responseObject);
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            arrprotdtl=[responseObject valueForKey:@"data"];
              
            
            for (int i=0; i<arrprotdtl.count; i++) {
                int j=[[[arrprotdtl objectAtIndex:i ]valueForKey:@"PortfolioCategoryID"]intValue];
                
                for (int k=1; k<=arrimg.count; k++) {
                    if (j == k) {
                       [arrimg replaceObjectAtIndex:j-1 withObject:[[arrprotdtl objectAtIndex:i ] valueForKey:@"PortfolioCategoryCoverImage"]];
                    }
                    
                }
                
            }
            
            
            
            
            
            [_vw_caro reloadData];
        }
        [APP_DELEGATE hideLoadingView];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
    
}


-(void)CallAddPortfolioCatDtl
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:_lbl_cat_id.text forKey:@"portfolio_category_id"];
    
    //[AddPost setValue:@"" forKey:@"portfolio_image"];
    
    
    //[AddPost setValue:_txt_short_disc.text forKey:@"description"];
    if (ISimageDtl>0) {
        //Create manager
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
        //Now post
        NSLog(@"AddPost %@",AddPost);
        NSString *url = [NSString stringWithFormat:@"%@add_portfolio_category_details",JsonUrlConstant];
        [manager POST:url parameters:[AddPost copy] constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            //add img data one by one
            
            NSData *imageData = UIImageJPEGRepresentation(ISimageDtl,0.5);
            [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"portfolio_category_image"] fileName:[NSString stringWithFormat:@"user_image.jpg"] mimeType:@"image/jpeg"];
            
        } success:^(AFHTTPRequestOperation *operation, id dicsResponse) {
            NSLog(@"Success: %@", dicsResponse);
            NSLog(@"Edit profile---%@",dicsResponse);
            
            //[self CallGetPortfolio];
            [self CallGetportfoliocategorydetails];
            [APP_DELEGATE hideLoadingView];
   
            
            if ([[dicsResponse valueForKey:@"status"] integerValue] == 0) {
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Add Data not match" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                //[alert show];
                
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Add Successfull" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                //[alert show];
                [self.navigationController popViewControllerAnimated:YES];
                
            }
            
        }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  NSLog(@"Error: %@", error);
                  [APP_DELEGATE hideLoadingView];
              }];
    }
    else
    {
        
        [AddPost setObject:@"" forKey:@"portfolio_image"];
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
            httpnewprof  = [[HttpWrapper alloc] init];
            httpnewprof.delegate=self;
            httpnewprof.getbool=NO;
            [httpnewprof requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@add_portfolio",JsonUrlConstant] param:[AddPost copy]];
        });
    }
}

-(void)CallAddPortfolio
{
   [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:@"1" forKey:@"is_image"];
    [AddPost setValue:@"1" forKey:@"portfolio_category_id"];//description
    [AddPost setValue:@"Tesing Image" forKey:@"description"];
    //[AddPost setValue:@"" forKey:@"portfolio_image"];
    
    
    //[AddPost setValue:_txt_short_disc.text forKey:@"description"];
    if (ISimage>0) {
        //Create manager
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
        //Now post
        NSLog(@"AddPost %@",AddPost);
        NSString *url = [NSString stringWithFormat:@"%@add_portfolio",JsonUrlConstant];
        [manager POST:url parameters:[AddPost copy] constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            //add img data one by one
            
            NSData *imageData = UIImageJPEGRepresentation(ISimage,0.5);
            [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"portfolio_image"] fileName:[NSString stringWithFormat:@"user_image.jpg"] mimeType:@"image/jpeg"];
            
        } success:^(AFHTTPRequestOperation *operation, id dicsResponse) {
            NSLog(@"Success: %@", dicsResponse);
            NSLog(@"Edit profile---%@",dicsResponse);
            NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            
            //[self CallGetPortfolio];
            [APP_DELEGATE hideLoadingView];
            
           
            
            if ([[dicsResponse valueForKey:@"status"] integerValue] == 0) {
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Add Data not match" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                //[alert show];
                
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Add Successfull" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                //[alert show];
                [self.navigationController popViewControllerAnimated:YES];
                
            }
            
        }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  NSLog(@"Error: %@", error);
                  [APP_DELEGATE hideLoadingView];
              }];
    }
    else
    {
        
        [AddPost setObject:@"" forKey:@"portfolio_image"];
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
            httpnewprof  = [[HttpWrapper alloc] init];
            httpnewprof.delegate=self;
            httpnewprof.getbool=NO;
            [httpnewprof requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@add_portfolio",JsonUrlConstant] param:[AddPost copy]];
        });
    }
}



#pragma mark - ImagePickerController Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    

    UIImage* originalImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    
    if (flag==1) {
        
        _imgCropperViewController = [[UzysImageCropperViewController alloc] initWithImage:originalImage andframeSize:picker.view.frame.size andcropSize:CGSizeMake(1180, 420)];
        _imgCropperViewController.delegate = self;
        [picker presentViewController:_imgCropperViewController animated:YES completion:nil];
        [_imgCropperViewController release];
        
        
        
        //ISimage=[info objectForKey:UIImagePickerControllerEditedImage];
        
        //[ipc dismissViewControllerAnimated:YES completion:nil];
        //[AddNew insertObject:originalImage atIndex:[AddNew count]];
        //[_collec_vw reloadData];
        //[self CallAddPortfolio];
        
       /* for (int i=0; i<[AddNew count]; i++) {
            [AddNew removeObjectAtIndex:i];
        }*/
        
        AddNew=nil;
        AddNewnm=nil;
        
     /*   if (AddNew.count <= 0) {
            //Array Allocation
            AddNew = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
            AddNewnm = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
        }*/
        
        [self CallAddPortfolio];
      
    }
    else if (flag==2)
    {
        [ipc dismissViewControllerAnimated:YES completion:nil];
        [AddNew_movie insertObject:originalImage atIndex:1];
        [collectinmovie reloadData];
        //UIImage *newImage = image;
    }
    else if (flag==3)
    {
        [ipc dismissViewControllerAnimated:YES completion:nil];
        [AddNew_sf insertObject:originalImage atIndex:1];
        [collection_SF reloadData];
        //UIImage *newImage = image;
    }
    else if (flag==4)
    {
        NSURL *imageURL = [info valueForKey:UIImagePickerControllerReferenceURL];
        PHAsset *phAsset = [[PHAsset fetchAssetsWithALAssetURLs:@[imageURL] options:nil] lastObject];
        NSString *imageName = [phAsset valueForKey:@"filename"];
        UIImage *photo = [info valueForKey:UIImagePickerControllerEditedImage];
        NSLog(@"Picked image: %@ width: %f x height: %f",imageName, photo.size.width, photo.size.height);
      //REMOVE
        ISimageDtl=[info valueForKey:UIImagePickerControllerEditedImage];
      /*  if (_vw_caro.numberOfItems > 0)
        {
            NSInteger index = _vw_caro.currentItemIndex;
            //[_vw_caro removeItemAtIndex:index animated:YES];
           // [_items removeObjectAtIndex:index];
            [arrimg removeObjectAtIndex:index];
            //[arrimgnm removeObjectAtIndex:index];
        }
        //ADD
        NSInteger index = _vw_caro.currentItemIndex;
       // [_items insertObject:@(_vw_caro.numberOfItems) atIndex:index];
        //[_vw_caro insertItemAtIndex:index animated:NO];
        
        [arrimg insertObject:photo atIndex:index];
        //[arrimgnm insertObject:imageName atIndex:index];
        [_vw_caro reloadData];*/
        
        [self CallAddPortfolioCatDtl];
        
        [self dismissViewControllerAnimated:YES completion:nil];
      }

}
- (void)imageCropper:(UzysImageCropperViewController *)cropper didFinishCroppingWithImage:(UIImage *)image
{
    
   if (flag==1) {
       ISimage=image;
       AddNew=nil;
       AddNewnm=nil;
       
       [self CallAddPortfolio];
       
   }
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imageCropperDidCancel:(UzysImageCropperViewController *)cropper
{
    [self dismissViewControllerAnimated:YES completion:nil];
    // [_picker dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)btn_HOME:(id)sender {
    
    NewHomeVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:YES];

}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}

- (IBAction)btn_EDIT:(id)sender {
    EditProfileVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"EditProfileVC"];
    next.responcedic=_responcedic;
    [self.navigationController pushViewController:next animated:YES];
    
}

- (IBAction)btn_MOVE_LEFT:(id)sender {
    
    NSInteger indexInt = _vw_caro.currentItemIndex-1;
    [_vw_caro scrollToItemAtIndex:indexInt animated:YES];
    
}

- (IBAction)btn_MOVE_RIGHT:(id)sender {
    
    NSInteger indexInt = _vw_caro.currentItemIndex+1;
    [_vw_caro scrollToItemAtIndex:indexInt animated:YES];
    
}

- (IBAction)btn_FOLLOWLIST:(id)sender {
    
    FollowListVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"FollowListVC"];
    next.arrFollow=arrFollow;
    next.isFollow=@"1";
    next.arrFollowing=arrFollowing;
    
    [self.navigationController pushViewController:next animated:YES];
    
}

- (IBAction)btn_FOLLOWING:(id)sender {
    FollowListVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"FollowListVC"];

    next.arrFollow=arrFollow;
    next.isFollow=@"2";
    next.arrFollowing=arrFollowing;
    
    [self.navigationController pushViewController:next animated:YES];
}

- (IBAction)btn_NOTIFY:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
    
}

- (IBAction)btn_IMG_PROFILE:(id)sender {
    
   /* ProfilePhotoViewVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilePhotoViewVC"];
    vc.arrimgs=[img_prof objectAtIndex:0];
    
    
    vc.view.frame = CGRectMake(0, 0, vc.view.frame.size.width, 320.0f);
    [self presentPopUpViewController:vc];
    */
    
  /*  ProfilePhotoForHome *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfilePhotoForHome"];
    vc.isFrom=@"NewProfileVC";
    vc.arrimgs=[img_prof objectAtIndex:0];
    vc.view.frame = CGRectMake(0, 0, vc.view.frame.size.width, 320.0f);
    [self presentPopUpViewController:vc];
    //[[img_prof valueForKey:@"ImageURL"] objectAtIndex:0]
    */
    
    
    NSURL *url = [NSURL URLWithString:[[img_prof valueForKey:@"ImageURL"] objectAtIndex:0]];
    NSData *data = [NSData dataWithContentsOfURL:url];
    UIImage* immg = [[UIImage alloc]initWithData:data];
    NSArray *landscapeImages = [[NSArray alloc]initWithObjects:immg, nil];
    
    Class cls = NSClassFromString(@"AFTNormalPagingViewController");
    AFTPagingBaseViewController *vc1 = [cls new];
    AFTPagingBaseViewController *bvc = (AFTPagingBaseViewController *)vc1;
    
    bvc.title = @"Photo";
    bvc.images = landscapeImages;
    [APP_DELEGATE hideLoadingView];
    [self presentViewController:bvc animated:YES completion:nil];
    
    
}
- (IBAction)btn_SUB_BACK:(id)sender {
    _vw_details.hidden=YES;
}

- (IBAction)btn_MSG_ME:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index{
    // attach long press gesture to collectionView
    NSLog(@"Selected carousel index is %ld",(long)index);
    
    
    NSLog(@"image name %@",[arrimg objectAtIndex:index]);
    if ([arrimg[index] isEqual:@"add-cover.png"])
    {
        flag=4;
        _lbl_cat_id.text=[[arrimgnm valueForKey:@"PortfolioCategoryID"] objectAtIndex:index];
        ipc= [[UIImagePickerController alloc] init];
        ipc.delegate = self;
        ipc.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        ipc.allowsEditing = YES;
        [self presentViewController:ipc animated:YES completion:nil];
        
    }
    else
    {
        _vw_details.hidden=NO;
        _lbl_cat_title.text=[[arrimgnm valueForKey:@"PortfolioCategoryName"] objectAtIndex:index];
        _lbl_cat_id.text=[[arrimgnm valueForKey:@"PortfolioCategoryID"] objectAtIndex:index];
        
        [self.collec_vw reloadData];
        
    }
    
    
}

-(void)carousel:(iCarousel *)carousel lognPress:(NSInteger)index{

    NSLog(@"LONG PRESS %ld",(long)index);
    
    flag=4;
    _lbl_cat_id.text=[[arrimgnm valueForKey:@"PortfolioCategoryID"] objectAtIndex:index];
    ipc= [[UIImagePickerController alloc] init];
    ipc.delegate = self;
    ipc.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    ipc.allowsEditing = YES;
    [self presentViewController:ipc animated:YES completion:nil];
    
    
}

/////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////FOLLOWING SCREEN API///////////////////////////////////////////
-(void)CallFollower
{
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    //------------
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@get_follower_list",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSLog(@"---%@",responseObject);
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            _lbl_follow_count.text=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"Total"]];
            
            
            
            
            arrFollow=[responseObject valueForKey:@"data"];
           //[self.navigationController pushViewController:next animated:YES];
            
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];
    
}
-(void)CallFollowing
{
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    //------------
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@get_following_list",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSLog(@"---%@",responseObject);
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
           
            _lbl_following_count.text=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"Total"]];
            
            
            
            arrFollowing=[responseObject valueForKey:@"data"];
           //[self.navigationController pushViewController:next animated:YES];
            //[AddNew insertObject:originalImage atIndex:1];
            
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];
    
}


-(CGFloat)heightForText:(NSString *)text
{
    NSInteger MAX_HEIGHT = 2000;
    UITextView * textView = [[UITextView alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width-80, MAX_HEIGHT+150)];
    textView.text = text;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        textView.font = [UIFont fontWithName:@"Roboto-Light" size:10.0];
    }
    else
    {
        textView.font = [UIFont fontWithName:@"Roboto-Light" size:10.0];
    }
    
    [textView sizeToFit];
    
    return textView.frame.size.height;
}




@end
