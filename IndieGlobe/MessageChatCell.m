//
//  MessageChatCell.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 9/11/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "MessageChatCell.h"

@implementation MessageChatCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    
    
    //_lbl_me_msg.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"me_img"]];
    //_lbl_sender_msg.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"sender_img"]];
    
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)layoutSubviews
{
    //_vw_me.layer.cornerRadius=7;
    //_vw_sender.layer.cornerRadius=7;
    
    
 /*   CALayer *layer = [CALayer layer];
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRoundedRect:_vw_me.frame byRoundingCorners:(UIRectCornerTopLeft|UIRectCornerTopRight|UIRectCornerBottomLeft) cornerRadii:CGSizeMake(7.0, 7.0)];
    layer.shadowPath = shadowPath.CGPath;
    _vw_me.layer.mask = layer;
   */
    
    [self setCorner];
    
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: _vw_me.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerBottomLeft | UIRectCornerTopRight  cornerRadii: (CGSize){12.0,12.0}].CGPath;
    _vw_me.layer.mask = maskLayer;
    
}
-(void)setCorner
{
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: _vw_sender.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerBottomRight | UIRectCornerTopRight  cornerRadii: (CGSize){12.0,12.0}].CGPath;
    _vw_sender.layer.mask = maskLayer;
}

@end
