//
//  ProfileVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/27/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "ProfileVC.h"
#import "SWRevealViewController.h"
#import "ProfileCell.h"
#import "DeshboardVC.h"
#import "Constants.h"
#import "UIColor+CL.h"
#import "EditProfileVC.h"
#import "MessagePopVC.h"
#import "ApplicationConstant.h"
#import "FollowListVC.h"
#import "NewHomeVC.h"
#import "WebPlayerVC.h"
#import "AFTPagingBaseViewController.h"



#import "YKMediaPlayerKit.h"
#import "YKVimeoVideo.h"
#import "YKYouTubeVideo.h"
#import "YKDirectVideo.h"




@interface ProfileVC ()<UIActionSheetDelegate>
@property (nonatomic, assign) BOOL wrap;
@property (nonatomic, strong) NSMutableArray *items;

@property (retain,nonatomic)NSMutableArray *marrimage;

@end

@implementation ProfileVC
{
    NSInteger _numberOfCells;
    
    YKYouTubeVideo  *_youTubeVideo;
    YKVimeoVideo    *_vimeoVideo;
    YKDirectVideo   *_directVideo;
    YKDirectVideo   *_otherVideo;
    YKUnKnownVideo  *_unknownVideo;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    
    _scrollVW.delegate=self;
    self.scrollVW.contentSize =CGSizeMake(320, 420);
    
    recipeImages=[[NSArray alloc]init];
    recipeImages = [NSArray arrayWithObjects:@"angry_birds_cake.jpg", @"creme_brelee.jpg", @"egg_benedict.jpg", @"full_breakfast.jpg", @"green_tea.jpg", @"ham_and_cheese_panini.jpg", @"ham_and_egg_sandwich.jpg", @"hamburger.jpg", @"instant_noodle_with_egg.jpg", @"japanese_noodle_with_pork.jpg", @"mushroom_risotto.jpg", @"noodle_with_bbq_pork.jpg", @"starbucks_coffee.jpg", @"thai_shrimp_cake.jpg", @"vegetable_curry.jpg", @"white_chocolate_donut.jpg", nil];
    _img_profile.layer.masksToBounds=YES;
    _img_profile.layer.cornerRadius=_img_profile.frame.size.height/2;
    //_img_profile.layer.cornerRadius=_img_profile.frame.size.width/2;
    
    _img_profile.layer.borderWidth=1.0;
    _img_profile.layer.borderColor=[UIColor colorWithHex:0x196a84].CGColor;
    _btn_msgme.layer.borderWidth=1.2;
    _btn_msgme.layer.cornerRadius=_btn_msgme.frame.size.height/2;
    _btn_msgme.layer.borderColor=[UIColor colorWithHex:0x196a84].CGColor;
    //_imgphoto.layer.masksToBounds=YES;
    //_imgphoto.layer.cornerRadius=15;
    
    _btn_follow_unf.layer.borderWidth=1.2;
    _btn_follow_unf.layer.cornerRadius=_btn_follow_unf.frame.size.height/2;
    _btn_follow_unf.layer.borderColor=[UIColor colorWithHex:0x196a84].CGColor;
    if ([[_mutDict valueForKey:@"IsFollowed"]isEqualToString:@"0"]) {
        
        [_btn_follow_unf setTitle: @"FOLLOW" forState: UIControlStateNormal];
        
    }
    else
    {
        
        [_btn_follow_unf setTitle: @"UNFOLLOW" forState: UIControlStateNormal];
    }
    
    [self customSetup];
    
    
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];
    
    //HTTP
    //[self CallMyMethod];
    //IsFollowed
    
    NSLog(@"MUTDICT ---%@",_mutDict);
    [_img_profile sd_setImageWithURL:[NSURL URLWithString:[_mutDict valueForKey:@"ImageURL"]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
    _lbl_usernm.text=[NSString stringWithFormat:@"%@ %@",[_mutDict valueForKey:@"FName"],[_mutDict valueForKey:@"LName"]];
    _lbl_address.text=[NSString stringWithFormat:@"%@,%@",[_mutDict valueForKey:@"CityName"],[_mutDict valueForKey:@"CountryName"]];//
    //_lbl_s_bio.text=[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"ShortDesc"]];
    
    self.lbl_about_me = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(self.lbl_about_me.frame.origin.x, self.lbl_about_me.frame.origin.y,self.lbl_about_me.frame.size.width,self.lbl_about_me.frame.size.height) ];
    [self.lbl_about_me setFont:[UIFont fontWithName:@"roboto" size:11]];
    self.lbl_about_me.delegate = self;
    self.lbl_about_me.enabledTextCheckingTypes=NSTextCheckingTypeLink|NSTextCheckingTypePhoneNumber;
    
    [self.view addSubview:self.lbl_about_me];
    
    //_lbl_about_me.text=[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"ShortDesc"]];
    
    
    NSArray *arrT=[_mutDict valueForKey:@"ProfessionData"];
    _lbl_profe.text=[arrT componentsJoinedByString:@","];
    NSMutableArray *NewArr = [[NSMutableArray alloc] init];
    for (int i=0; i<arrT.count; i++) {
        _lbl_profe.text=[arrT componentsJoinedByString:@","];
        [NewArr addObject:[[arrT objectAtIndex:i] valueForKey:@"Profession"]];
    }
    _lbl_profe.text=[NewArr componentsJoinedByString:@","];
    
    CGRect autosizeprof = _lbl_profe.frame;
    autosizeprof.size.height = [self heightForText:_lbl_profe.text];
    _lbl_profe.frame = autosizeprof;
    
    [_lbl_profe setNumberOfLines:0];
    [_lbl_profe sizeToFit];
    
    
    
    
    
    /*  //AGE CONVERT
     NSString* yourJSONString = [NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"BirthDate"]];
     NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
     [currentDTFormatter setDateFormat:@"yyyy-MM-dd"];
     NSDate *birthday = [currentDTFormatter dateFromString:yourJSONString];
     
     NSDate* now = [NSDate date];
     NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
     components:NSCalendarUnitYear
     fromDate:birthday
     toDate:now
     options:0];
     NSInteger age = [ageComponents year];*/
    _vw_age_gen.hidden=YES;
    CGRect border = _vw_address.frame;
    border.origin.x = _vw_age_gen.frame.origin.x;
    _vw_address.frame = border;
    
    /* if ([[_mutDict valueForKey:@"Gender"]isEqualToString:@""] &&[[_mutDict valueForKey:@"BirthDate"]isEqualToString:@"0000-00-00"]) {
     _vw_age_gen.hidden=YES;
     CGRect border = _vw_address.frame;
     border.origin.x = _vw_age_gen.frame.origin.x;
     _vw_address.frame = border;
     }
     else
     {
     _vw_age_gen.hidden=NO;
     CGRect border = _vw_address.frame;
     border.origin.x = _vw_age_gen.frame.origin.x+_vw_age_gen.frame.size.width+10;
     _vw_address.frame = border;
     //AGE CONVERT
     NSString* yourJSONString = [NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"BirthDate"]];
     NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
     [currentDTFormatter setDateFormat:@"yyyy-MM-dd"];
     NSDate *birthday = [currentDTFormatter dateFromString:yourJSONString];
     
     NSDate* now = [NSDate date];
     NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
     components:NSCalendarUnitYear
     fromDate:birthday
     toDate:now
     options:0];
     NSInteger age = [ageComponents year];
     
     _lbl_user_age.text=[NSString stringWithFormat:@"%@, Age-%ld",[_mutDict valueForKey:@"Gender"],(long)age];
     
     }*/
    _lbl_about_me.text=[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"ShortDesc"]];
    //////////AUTO SPACING
    [_lbl_about_me setNumberOfLines:0];
    //[_lbl_about_me sizeToFit];
    CGRect autosize4 = _lbl_about_me.frame;
    autosize4.size.height = [self heightForText:[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"ShortDesc"]]];
    _lbl_about_me.frame = autosize4;
    
    _lbl_about_me.numberOfLines = 0;
    _lbl_about_me.lineBreakMode = NSLineBreakByWordWrapping;
    _lbl_about_me.font = [UIFont systemFontOfSize:12];
    _lbl_about_me.text = _lbl_about_me.text;
    _lbl_about_me.backgroundColor = [UIColor clearColor];
    [_lbl_about_me sizeToFit];
    
    CGRect autosize7 = _lbl_about_me.frame;
    autosize7.origin.y = _lbl_profe.frame.origin.y + _lbl_profe.frame.size.height + 4;
    _lbl_about_me.frame = autosize7;
    
    
    UITapGestureRecognizer *letterTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(highlightLetter:)];
    letterTapRecognizer.numberOfTapsRequired = 1;
    [_img_profile addGestureRecognizer:letterTapRecognizer];
    
    _img_profile.userInteractionEnabled=YES;
    
    
    //New Scroll
    _wrap = YES;
    _vw_caro.type = iCarouselTypeRotary;
    
    [self CallCategory];
    [self CallGetPortfolio];
    
    [self CallFollower];
    [self CallFollowing];
    
    [self CallGetportfoliocategorydetails];
    
    
    _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
    _lbl_notifi_count.clipsToBounds=YES;
    _lbl_notifi_count.text=theAppDelegate.n_Count;
    
    if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
        _lbl_notifi_count.hidden=YES;
    }
    else
    {
        _lbl_notifi_count.hidden=NO;
    }
}

-(void)highlightLetter:(UITapGestureRecognizer*)sender
{
    NSLog(@"click on image");
    
    [APP_DELEGATE showLoadingView:@""];
    
    NSURL *url = [NSURL URLWithString:[_mutDict valueForKey:@"ImageURL"]];
    NSData *data = [NSData dataWithContentsOfURL:url];
    UIImage* immg = [[UIImage alloc]initWithData:data];
    NSArray *landscapeImages = [[NSArray alloc]initWithObjects:immg, nil];
    
    Class cls = NSClassFromString(@"AFTNormalPagingViewController");
    AFTPagingBaseViewController *vc1 = [cls new];
    AFTPagingBaseViewController *bvc = (AFTPagingBaseViewController *)vc1;
    
    bvc.title = @"Photo";
    bvc.images = landscapeImages;
    [APP_DELEGATE hideLoadingView];
    [self presentViewController:bvc animated:YES completion:nil];
    
}



- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    NSString *val=[[NSString alloc]initWithFormat:@"%@",url];
    if ([[url scheme] hasPrefix:@"mailto"]) {
        NSLog(@" mail URL Selected : %@",url);
        MFMailComposeViewController *comp=[[MFMailComposeViewController alloc]init];
        [comp setMailComposeDelegate:self];
        if([MFMailComposeViewController canSendMail])
        {
            NSString *recp=[[val substringToIndex:[val length]] substringFromIndex:7];
            NSLog(@"Recept : %@",recp);
            [comp setToRecipients:[NSArray arrayWithObjects:recp, nil]];
            [comp setSubject:@"From my app"];
            [comp setMessageBody:@"Hello bro" isHTML:NO];
            [comp setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            [self presentViewController:comp animated:YES completion:nil];
        }
    }
    else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:val]];
    }
}

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithPhoneNumber:(NSString *)phoneNumber
{
    NSLog(@"Phone Number Selected : %@",phoneNumber);
    UIDevice *device = [UIDevice currentDevice];
    if ([[device model] isEqualToString:@"iPhone"] ) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",phoneNumber]]];
    } else {
        UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [Notpermitted show];
    }
}

-(void)CallGetportfoliocategorydetails
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    //NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[_mutDict valueForKey:@"UserID"] forKey:@"user_id"];
    
    //------------
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@get_portfolio_category_details",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSLog(@"---%@",responseObject);
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            arrprotdtl=[responseObject valueForKey:@"data"];
            
            
            for (int i=0; i<arrprotdtl.count; i++) {
                int j=[[[arrprotdtl objectAtIndex:i ]valueForKey:@"PortfolioCategoryID"]intValue];
                
                for (int k=1; k<=arrimg.count; k++) {
                    if (j == k) {
                        [arrimg replaceObjectAtIndex:j-1 withObject:[[arrprotdtl objectAtIndex:i ] valueForKey:@"PortfolioCategoryCoverImage"]];
                    }
                    
                }
                
            }
            
            
            
            
            
            [_vw_caro reloadData];
        }
        [APP_DELEGATE hideLoadingView];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
    
}

#pragma View-carousal
- (IBAction)reloadCarousel
{
    _wrap = YES;
    _vw_caro.type = iCarouselTypeRotary;
    [_vw_caro reloadData];
}

- (void)setUp
{
    //set up data
    _wrap = YES;
    _vw_caro.type = iCarouselTypeRotary;
    self.items = [NSMutableArray array];
    for (int i = 0; i < 7; i++)
    {
        [_items addObject:@(i)];
    }
    arrimg=[[NSMutableArray alloc]initWithObjects:@"1.jpg",@"2.jpg",@"3.jpg",@"4.png",@"5.jpg",@"6.jpg",@"7.jpg", nil];
    arrimgnm=[[NSMutableArray alloc]initWithObjects:@"Photos",@"Feature Films",@"Short Films",@"Reel",@"BTS",@"Videos",@"Head Shots", nil];
    
    
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder]))
    {
        [self setUp];
    }
    return self;
}

-(void)CallCategory
{
    
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[_mutDict valueForKey:@"UserID"] forKey:@"user_id"];
    
    //------------
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@get_portfolio_category",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSLog(@"---%@",responseObject);
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            resFlag=1;
            arrimgnm=[responseObject valueForKey:@"data"];
        }
        [APP_DELEGATE hideLoadingView];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
    
    
}

-(void)CallGetPortfolio
{
    
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[_mutDict valueForKey:@"UserID"] forKey:@"user_id"];
    
    //------------
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@get_portfolio",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSLog(@"---%@",responseObject);
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            dictres=[[NSMutableDictionary alloc] init];
            dictres=[responseObject valueForKey:@"data"];
            
            
            
            for (int i=0; i < dictres.count; i++) {
                
                if ([[[dictres valueForKey:@"PortfolioCategoryName"] objectAtIndex:i]isEqualToString:@"Photos"])
                {
                    [AddNew insertObject:[[dictres  valueForKey:@"ImageURL"]objectAtIndex:i] atIndex:0];
                    [AddNewnm insertObject:[[dictres  valueForKey:@"PortfolioName"]objectAtIndex:i] atIndex:0];
                }
                else if ([[[dictres valueForKey:@"PortfolioCategoryName"] objectAtIndex:i]isEqualToString:@"Feature Films"])
                {
                    [FFilms insertObject:[[dictres  valueForKey:@"URL"]objectAtIndex:i] atIndex:0];
                    [FFilmsnm insertObject:[[dictres  valueForKey:@"PortfolioName"]objectAtIndex:i] atIndex:0];
                }
                else if ([[[dictres valueForKey:@"PortfolioCategoryName"] objectAtIndex:i]isEqualToString:@"Short Films"])
                {
                    [SFilms insertObject:[[dictres  valueForKey:@"URL"]objectAtIndex:i] atIndex:0];
                    [SFilmsnm insertObject:[[dictres  valueForKey:@"PortfolioName"]objectAtIndex:i] atIndex:0];
                }
                else if ([[[dictres valueForKey:@"PortfolioCategoryName"] objectAtIndex:i]isEqualToString:@"Reel"])
                {
                    [Reel insertObject:[[dictres  valueForKey:@"URL"]objectAtIndex:i] atIndex:0];
                    [Reelnm insertObject:[[dictres  valueForKey:@"PortfolioName"]objectAtIndex:i] atIndex:0];
                }
                else if ([[[dictres valueForKey:@"PortfolioCategoryName"] objectAtIndex:i]isEqualToString:@"BTS"])
                {
                    [BTS insertObject:[[dictres  valueForKey:@"URL"]objectAtIndex:i] atIndex:0];
                    [BTSnm insertObject:[[dictres  valueForKey:@"PortfolioName"]objectAtIndex:i] atIndex:0];
                }
                else if ([[[dictres valueForKey:@"PortfolioCategoryName"] objectAtIndex:i]isEqualToString:@"Videos"])
                {
                    [Video insertObject:[[dictres  valueForKey:@"URL"]objectAtIndex:i] atIndex:0];
                    [Videonm insertObject:[[dictres  valueForKey:@"PortfolioName"]objectAtIndex:i] atIndex:0];
                }
                else if ([[[dictres valueForKey:@"PortfolioCategoryName"] objectAtIndex:i]isEqualToString:@"Videos"])
                {
                    [HShots insertObject:[[dictres  valueForKey:@"Head Shots"]objectAtIndex:i] atIndex:0];
                    [HShotsnm insertObject:[[dictres  valueForKey:@"PortfolioName"]objectAtIndex:i] atIndex:0];
                }
       
            }
            
            
            //[_coll_movies reloadData];
            
            
            [APP_DELEGATE hideLoadingView];
            //[AddNew insertObject:originalImage atIndex:1];
            
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
    
    
}


- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return [_items count];
}
-(CGFloat)heightForText:(NSString *)text
{
    NSInteger MAX_HEIGHT = 2000;
    UITextView * textView = [[UITextView alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width-80, MAX_HEIGHT+150)];
    textView.text = text;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        textView.font = [UIFont fontWithName:@"Roboto-Light" size:10.0];
    }
    else
    {
        textView.font = [UIFont fontWithName:@"Roboto-Light" size:10.0];
    }
    
    [textView sizeToFit];
    
    return textView.frame.size.height;
}


- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    
    
    int k=carousel.currentItemIndex;
    
    
    //NSLog(@"carousel---  %@",carousel.dataSource);
    
    NSMutableArray *arrtemp=[[NSMutableArray alloc]init];
    arrtemp=carousel.dataSource;
    
    if (resFlag==0) {
        _lbl_cover_title.text=[NSString stringWithFormat:@"%@",[[arrtemp valueForKey:@"arrimgnm"]objectAtIndex:k]];
    }
    else
    {
        _lbl_cover_title.text=[NSString stringWithFormat:@"%@",[[[arrtemp valueForKey:@"arrimgnm"]objectAtIndex:k] valueForKey:@"PortfolioCategoryName"]];
    }
    
    
    
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            return _wrap;
        }
        case iCarouselOptionFadeMax:
        {
            if (carousel.type == iCarouselTypeCustom)
            {
                NSLog(@"carousel  %@",carousel.dataSource);
                return 0.0f;
            }
            return value;
        }
        case iCarouselOptionArc:
        {
            return 2 * M_PI * 0.33400;
        }
        case iCarouselOptionRadius:
        {
            return value * 0.467433;
        }
        case iCarouselOptionTilt:
        {
            return 1;
        }
        case iCarouselOptionSpacing:
        {
            return value * 0.277052;
        }
        default:
        {
            return value;
        }
    }
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)Iview
{
    
    
    
    
    if (Iview == nil)
    {
        Iview=[[UIView alloc] initWithFrame:CGRectMake(0,0,200,200)];
        Iview.layer.cornerRadius=15.0;
        
        UIImageView *dot =[[UIImageView alloc] initWithFrame:Iview.bounds];
        
        dot.layer.borderWidth=1.5;
        dot.layer.borderColor=[UIColor colorWithHex:0xffffff].CGColor;
        dot.layer.backgroundColor=[UIColor whiteColor].CGColor;
        
        NSString *srr=[NSString stringWithFormat:@"%@",arrimg[index]];
        
        if ([srr isEqualToString:@"add-cover.png"]) {
            dot.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",arrimg[index]]];
        }
        else
        {
            //dot.image = arrimg[index];
            [dot sd_setImageWithURL:[NSURL URLWithString:arrimg[index]] placeholderImage:[UIImage imageNamed:@"profile-no-image"]];
        }
        
        
        //dot.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",arrimg[index]]];
        [Iview addSubview:dot];
        
    }
    else
    {
        _lbl_cover_title = (UILabel *)[Iview viewWithTag:1];
    }
    
    
    return Iview;
}


- (void)viewWillAppear:(BOOL)animated
{
    [self customSetup];
    
    AddNew=[[NSMutableArray alloc] init];
    FFilms=[[NSMutableArray alloc]init];
    SFilms=[[NSMutableArray alloc]init];
    Reel=[[NSMutableArray alloc] init];
    BTS=[[NSMutableArray alloc] init];
    Video=[[NSMutableArray alloc]init];
    HShots=[[NSMutableArray alloc]init];
    
    
    AddNewnm=[[NSMutableArray alloc] init];
    FFilmsnm=[[NSMutableArray alloc] init];
    SFilmsnm=[[NSMutableArray alloc] init];
    Reelnm=[[NSMutableArray alloc] init];
    BTSnm=[[NSMutableArray alloc] init];
    Videonm=[[NSMutableArray alloc] init];
    HShotsnm=[[NSMutableArray alloc] init];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self customSetup];
    
    
    
    /*
     
     if (AddNew.count <= 0) {
     //Array Allocation
     AddNew = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
     }
     
     if (FFilms.count <= 0) {
     //Array Allocation
     FFilms = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
     }
     
     if (SFilms.count <= 0) {
     //Array Allocation
     SFilms = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
     }
     
     if (Reel.count <= 0) {
     //Array Allocation
     Reel = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
     }
     
     if (BTS.count <= 0) {
     //Array Allocation
     BTS = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
     }
     
     if (Video.count <= 0) {
     //Array Allocation
     Video = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
     }
     
     if (HShots.count <= 0) {
     //Array Allocation
     HShots = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
     }
     */
}


- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if (IsIphone7 || IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = 145;
    }
    else{
        self.revealViewController.rearViewRevealWidth = 145;
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if ([_lbl_titlenm.text isEqualToString:@"Photos"])
    {
        return AddNew.count;
    }
    else if ([_lbl_titlenm.text isEqualToString:@"Feature Films"])
    {
        return FFilms.count;
    }
    else if ([_lbl_titlenm.text isEqualToString:@"Short Films"])
    {
        return SFilms.count;
    }
    else if ([_lbl_titlenm.text isEqualToString:@"Reel"])
    {
        return Reel.count;
    }
    else if ([_lbl_titlenm.text isEqualToString:@"BTS"])
    {
        return BTS.count;
    }
    else if ([_lbl_titlenm.text isEqualToString:@"Videos"])
    {
        return Video.count;
    }
    else if ([_lbl_titlenm.text isEqualToString:@"Head Shots"])
    {
        return HShots.count;
    }
    else{
        return nil;
    }
    
    
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([_lbl_titlenm.text isEqualToString:@"Photos"])
    {
        ProfileCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        cell = nil;
        if (cell == nil)
        {
            cell=(ProfileCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        }
        cell.img_movies.clipsToBounds = YES;
        cell.img_movies.layer.masksToBounds = YES;
        cell.img_movies.layer.cornerRadius = 10.0f;
        cell.layer.cornerRadius = 10.0f;
        
        NSLog(@"AddNew %@",AddNew);
        if ([AddNew[indexPath.row] isEqual:@"addNew"]) {
            //cell.img_movies.image = [UIImage imageNamed:@"add-photo-withplus"];
            //cell.btn_close_b.hidden = YES;
            cell.lbl_movie_title.hidden = YES;
            //cell.btn_play_b.hidden=YES;
            
        }
        else
        {
            //cell.btn_close_b.hidden = NO;
            cell.lbl_movie_title.hidden = YES;
            //cell.btn_play_b.hidden=YES;
            //cell.btn_close_b.tag = indexPath.row;
            //[cell.btn_close_b addTarget:self action:@selector(masterAction:) forControlEvents:UIControlEventTouchUpInside];
            //cell.img_movi_image.image = AddNew[indexPath.row];
            
            [cell.img_movies sd_setImageWithURL:AddNew[indexPath.row] placeholderImage:[UIImage imageNamed:@"Avatar"]];
            //cell.Img_Photos.image=[UIImage imageNamed:[AddNew objectAtIndex:indexPath.row]];
        }
        return cell;
    }
    else if ([_lbl_titlenm.text isEqualToString:@"Feature Films"])
    {
        ProfileCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        cell = nil;
        if (cell == nil)
        {
            cell=(ProfileCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        }
        cell.img_movies.clipsToBounds = YES;
        cell.img_movies.layer.masksToBounds = YES;
        cell.img_movies.layer.cornerRadius = 10.0f;
        cell.layer.cornerRadius = 10.0f;
        
        NSLog(@"FFilms %@",FFilms);
        if ([FFilms[indexPath.row] isEqual:@"addNew"]) {
            cell.img_movies.image = [UIImage imageNamed:@"add-photo-withplus"];
            // cell.btn_close_b.hidden = YES;
            cell.lbl_movie_title.hidden = YES;
            //cell.btn_play_b.hidden=YES;
        }
        else
        {
            //cell.btn_close_b.hidden = NO;
            cell.lbl_movie_title.hidden = NO;
            cell.btn_play_b.hidden=NO;
            //cell.btn_close_b.tag = indexPath.row;
            // [cell.btn_close_b addTarget:self action:@selector(masterAction:) forControlEvents:UIControlEventTouchUpInside];
            //cell.img_movi_image.image = AddNew[indexPath.row];
            //[cell.img_movi_image sd_setImageWithURL:AddNew[indexPath.row] placeholderImage:[UIImage imageNamed:@"Avatar"]];
            cell.btn_play_b.tag = indexPath.row;
            [cell.btn_play_b addTarget:self action:@selector(masterActionVideo:) forControlEvents:UIControlEventTouchUpInside];
            cell.lbl_movie_title.text=[NSString stringWithFormat:@"%@",[FFilmsnm objectAtIndex:indexPath.row]];
            
            NSString *str=[NSString stringWithFormat:@"%@", [FFilms objectAtIndex:indexPath.row]];  //is your str
            if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
                
                //Thard party thambanil
                _youTubeVideo = [[YKYouTubeVideo alloc] initWithContent:[NSURL URLWithString:[FFilms objectAtIndex:indexPath.row]]];
                [_youTubeVideo parseWithCompletion:^(NSError *error) {
                    [_youTubeVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                        cell.img_movies.image = thumbImage;
                    }];
                }];
            }
            else
            {
                //NSURL *movieURL = [NSURL URLWithString:[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
                _directVideo = [[YKDirectVideo alloc] initWithContent:[NSURL URLWithString:[FFilms objectAtIndex:indexPath.row]]];
                [_directVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                    cell.img_movies.image = thumbImage;
                }];
            }
            
            
            
            
        }
        return cell;
    }
    else if ([_lbl_titlenm.text isEqualToString:@"Short Films"])
    {
        ProfileCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        cell = nil;
        if (cell == nil)
        {
            cell=(ProfileCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        }
        cell.img_movies.clipsToBounds = YES;
        cell.img_movies.layer.masksToBounds = YES;
        cell.img_movies.layer.cornerRadius = 10.0f;
        cell.layer.cornerRadius = 10.0f;
        
        NSLog(@"SFilms %@",SFilms);
        if ([SFilms[indexPath.row] isEqual:@"addNew"]) {
            cell.img_movies.image = [UIImage imageNamed:@"add-photo-withplus"];
            //cell.btn_close_b.hidden = YES;
            cell.lbl_movie_title.hidden = YES;
            cell.btn_play_b.hidden=YES;
        }
        else
        {
            //cell.btn_close_b.hidden = NO;
            cell.lbl_movie_title.hidden = NO;
            cell.btn_play_b.hidden=NO;
            //cell.btn_close_b.tag = indexPath.row;
            //[cell.btn_close_b addTarget:self action:@selector(masterAction:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.btn_play_b.tag = indexPath.row;
            [cell.btn_play_b addTarget:self action:@selector(masterActionVideo:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.lbl_movie_title.text=[NSString stringWithFormat:@"%@",[SFilmsnm objectAtIndex:indexPath.row]];
            
            NSString *str=[NSString stringWithFormat:@"%@", [SFilms objectAtIndex:indexPath.row]];  //is your str
            if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
                
                //Thard party thambanil
                _youTubeVideo = [[YKYouTubeVideo alloc] initWithContent:[NSURL URLWithString:[SFilms objectAtIndex:indexPath.row]]];
                [_youTubeVideo parseWithCompletion:^(NSError *error) {
                    [_youTubeVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                        cell.img_movies.image = thumbImage;
                    }];
                }];
            }
            else
            {
                //NSURL *movieURL = [NSURL URLWithString:[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
                _directVideo = [[YKDirectVideo alloc] initWithContent:[NSURL URLWithString:[SFilms objectAtIndex:indexPath.row]]];
                [_directVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                    cell.img_movies.image = thumbImage;
                }];
            }
        }
        return cell;
    }
    else if ([_lbl_titlenm.text isEqualToString:@"Reel"])
    {
        ProfileCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        cell = nil;
        if (cell == nil)
        {
            cell=(ProfileCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        }
        cell.img_movies.clipsToBounds = YES;
        cell.img_movies.layer.masksToBounds = YES;
        cell.img_movies.layer.cornerRadius = 10.0f;
        cell.layer.cornerRadius = 10.0f;
        
        NSLog(@"Reel %@",Reel);
        if ([Reel[indexPath.row] isEqual:@"addNew"]) {
            cell.img_movies.image = [UIImage imageNamed:@"add-photo-withplus"];
            //cell.btn_close_b.hidden = YES;
            cell.lbl_movie_title.hidden = YES;
            //cell.btn_play_b.hidden=YES;
        }
        else
        {
            //cell.btn_close_b.hidden = NO;
            cell.lbl_movie_title.hidden = NO;
            
            
            cell.btn_play_b.hidden=NO;
            //cell.btn_close_b.tag = indexPath.row;
            //[cell.btn_close_b addTarget:self action:@selector(masterAction:) forControlEvents:UIControlEventTouchUpInside];
            //cell.img_movi_image.image = AddNew[indexPath.row];
            //[cell.img_movi_image sd_setImageWithURL:AddNew[indexPath.row] placeholderImage:[UIImage imageNamed:@"Avatar"]];
            //cell.img_movi_image.image= [self imageFromMovie:[Reel objectAtIndex:indexPath.row] atTime:5.0];
            cell.btn_play_b.tag = indexPath.row;
            [cell.btn_play_b addTarget:self action:@selector(masterActionVideo:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.lbl_movie_title.text=[NSString stringWithFormat:@"%@",[Reelnm objectAtIndex:indexPath.row]];
            NSString *str=[NSString stringWithFormat:@"%@", [Reel objectAtIndex:indexPath.row]];  //is your str
            if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
                
                //Thard party thambanil
                _youTubeVideo = [[YKYouTubeVideo alloc] initWithContent:[NSURL URLWithString:[Reel objectAtIndex:indexPath.row]]];
                [_youTubeVideo parseWithCompletion:^(NSError *error) {
                    [_youTubeVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                        cell.img_movies.image = thumbImage;
                    }];
                }];
            }
            else
            {
                //NSURL *movieURL = [NSURL URLWithString:[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
                _directVideo = [[YKDirectVideo alloc] initWithContent:[NSURL URLWithString:[Reel objectAtIndex:indexPath.row]]];
                [_directVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                    cell.img_movies.image = thumbImage;
                }];
            }
        }
        return cell;
    }
    else if ([_lbl_titlenm.text isEqualToString:@"BTS"])
    {
        ProfileCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        cell = nil;
        if (cell == nil)
        {
            cell=(ProfileCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        }
        cell.img_movies.clipsToBounds = YES;
        cell.img_movies.layer.masksToBounds = YES;
        cell.img_movies.layer.cornerRadius = 10.0f;
        cell.layer.cornerRadius = 10.0f;
        
        NSLog(@"BTS %@",BTS);
        if ([BTS[indexPath.row] isEqual:@"addNew"]) {
            cell.img_movies.image = [UIImage imageNamed:@"add-photo-withplus"];
            //cell.btn_close_b.hidden = YES;
            cell.lbl_movie_title.hidden = YES;
            //cell.btn_play_b.hidden=YES;
        }
        else
        {
            //cell.btn_close_b.hidden = NO;
            cell.lbl_movie_title.hidden = NO;
            cell.btn_play_b.hidden=NO;
            //cell.btn_close_b.tag = indexPath.row;
            //[cell.btn_close_b addTarget:self action:@selector(masterAction:) forControlEvents:UIControlEventTouchUpInside];
            //cell.img_movi_image.image = AddNew[indexPath.row];
            //[cell.img_movi_image sd_setImageWithURL:AddNew[indexPath.row] placeholderImage:[UIImage imageNamed:@"Avatar"]];
            //cell.img_movi_image.image= [self imageFromMovie:[BTS objectAtIndex:indexPath.row] atTime:5.0];
            cell.btn_play_b.tag = indexPath.row;
            [cell.btn_play_b addTarget:self action:@selector(masterActionVideo:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.lbl_movie_title.text=[NSString stringWithFormat:@"%@",[BTSnm objectAtIndex:indexPath.row]];
            
            NSString *str=[NSString stringWithFormat:@"%@", [BTS objectAtIndex:indexPath.row]];  //is your str
            if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
                
                //Thard party thambanil
                _youTubeVideo = [[YKYouTubeVideo alloc] initWithContent:[NSURL URLWithString:[BTS objectAtIndex:indexPath.row]]];
                [_youTubeVideo parseWithCompletion:^(NSError *error) {
                    [_youTubeVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                        cell.img_movies.image = thumbImage;
                    }];
                }];
            }
            else
            {
                //NSURL *movieURL = [NSURL URLWithString:[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
                _directVideo = [[YKDirectVideo alloc] initWithContent:[NSURL URLWithString:[BTS objectAtIndex:indexPath.row]]];
                [_directVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                    cell.img_movies.image = thumbImage;
                }];
            }
            
            
        }
        return cell;
    }
    else if ([_lbl_titlenm.text isEqualToString:@"Videos"])
    {
        ProfileCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        cell = nil;
        if (cell == nil)
        {
            cell=(ProfileCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        }
        cell.img_movies.clipsToBounds = YES;
        cell.img_movies.layer.masksToBounds = YES;
        cell.img_movies.layer.cornerRadius = 10.0f;
        cell.layer.cornerRadius = 10.0f;
        
        NSLog(@"Videos %@",Video);
        if ([Video[indexPath.row] isEqual:@"addNew"]) {
            cell.img_movies.image = [UIImage imageNamed:@"add-photo-withplus"];
            //cell.btn_close_b.hidden = YES;
            cell.lbl_movie_title.hidden = YES;
            //cell.btn_play_b.hidden=YES;
        }
        else
        {
            //cell.btn_close_b.hidden = NO;
            cell.lbl_movie_title.hidden = NO;
            cell.btn_play_b.hidden=NO;
            //cell.btn_close_b.tag = indexPath.row;
            //[cell.btn_close_b addTarget:self action:@selector(masterAction:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.btn_play_b.tag = indexPath.row;
            [cell.btn_play_b addTarget:self action:@selector(masterActionVideo:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.lbl_movie_title.text=[NSString stringWithFormat:@"%@",[Videonm objectAtIndex:indexPath.row]];
            
            NSString *str=[NSString stringWithFormat:@"%@", [Video objectAtIndex:indexPath.row]];  //is your str
            if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
                
                //Thard party thambanil
                _youTubeVideo = [[YKYouTubeVideo alloc] initWithContent:[NSURL URLWithString:[Video objectAtIndex:indexPath.row]]];
                [_youTubeVideo parseWithCompletion:^(NSError *error) {
                    [_youTubeVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                        cell.img_movies.image = thumbImage;
                    }];
                }];
            }
            else
            {
                //NSURL *movieURL = [NSURL URLWithString:[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
                _directVideo = [[YKDirectVideo alloc] initWithContent:[NSURL URLWithString:[Video objectAtIndex:indexPath.row]]];
                [_directVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                    cell.img_movies.image = thumbImage;
                }];
            }
        }
        return cell;
    }
    else if ([_lbl_titlenm.text isEqualToString:@"Head Shots"])
    {
        ProfileCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        cell = nil;
        if (cell == nil)
        {
            cell=(ProfileCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        }
        cell.img_movies.clipsToBounds = YES;
        cell.img_movies.layer.masksToBounds = YES;
        cell.img_movies.layer.cornerRadius = 10.0f;
        cell.layer.cornerRadius = 10.0f;
        
        NSLog(@"HShots %@",HShots);
        if ([HShots[indexPath.row] isEqual:@"addNew"]) {
            cell.img_movies.image = [UIImage imageNamed:@"add-photo-withplus"];
            //cell.btn_close_b.hidden = YES;
            cell.lbl_movie_title.hidden = YES;
            //cell.btn_play_b.hidden=YES;
        }
        else
        {
            //cell.btn_close_b.hidden = NO;
            cell.lbl_movie_title.hidden = NO;
            cell.btn_play_b.hidden=NO;
            //cell.btn_close_b.tag = indexPath.row;
            //[cell.btn_close_b addTarget:self action:@selector(masterAction:) forControlEvents:UIControlEventTouchUpInside];
            cell.btn_play_b.tag = indexPath.row;
            [cell.btn_play_b addTarget:self action:@selector(masterActionVideo:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.lbl_movie_title.text=[NSString stringWithFormat:@"%@",[HShotsnm objectAtIndex:indexPath.row]];
            NSString *str=[NSString stringWithFormat:@"%@", [HShots objectAtIndex:indexPath.row]];  //is your str
            if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
                
                //Thard party thambanil
                _youTubeVideo = [[YKYouTubeVideo alloc] initWithContent:[NSURL URLWithString:[HShots objectAtIndex:indexPath.row]]];
                [_youTubeVideo parseWithCompletion:^(NSError *error) {
                    [_youTubeVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                        cell.img_movies.image = thumbImage;
                    }];
                }];
            }
            else
            {
                //NSURL *movieURL = [NSURL URLWithString:[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
                _directVideo = [[YKDirectVideo alloc] initWithContent:[NSURL URLWithString:[HShots objectAtIndex:indexPath.row]]];
                [_directVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                    cell.img_movies.image = thumbImage;
                }];
            }
        }
        return cell;
    }
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    ProfileCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    cell = nil;
    if (cell == nil)
    {
        cell=(ProfileCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    }
    
    if ([_lbl_titlenm.text isEqualToString:@"Photos"])
    {
     
        [cell.img_movies sd_setImageWithURL:AddNew[indexPath.row] placeholderImage:[UIImage imageNamed:@"Avatar"]];
        
        [APP_DELEGATE showLoadingView:@""];
        
        NSURL *url = [NSURL URLWithString:AddNew[indexPath.row]];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage* immg = [[UIImage alloc]initWithData:data];
        NSArray *landscapeImages = [[NSArray alloc]initWithObjects:immg, nil];
        
        Class cls = NSClassFromString(@"AFTNormalPagingViewController");
        AFTPagingBaseViewController *vc1 = [cls new];
        AFTPagingBaseViewController *bvc = (AFTPagingBaseViewController *)vc1;
        
        bvc.title = @"Photo";
        bvc.images = landscapeImages;
        [APP_DELEGATE hideLoadingView];
        [self presentViewController:bvc animated:YES completion:nil];
        
    }
    
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 10, 0, 0); // top, left, bottom, right
}

-(void)masterActionVideo:(UIButton*)sender
{
    
    if ([_lbl_titlenm.text isEqualToString:@"Feature Films"])
    {
        NSString *str=[NSString stringWithFormat:@"%@", [[dictres valueForKey:@"URL"]objectAtIndex:sender.tag]];  //is your str
        if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
            
            WebPlayerVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"WebPlayerVC"];
            next.strurl=[[dictres valueForKey:@"URL"]objectAtIndex:sender.tag];
            [self.navigationController pushViewController:next animated:NO];
            
        }
        else
        {
            NSURL *videoURL = [NSURL URLWithString:[[dictres valueForKey:@"URL"]objectAtIndex:sender.tag]];
            AVPlayer *player = [AVPlayer playerWithURL:videoURL];
            AVPlayerViewController *avplayerViewController = [AVPlayerViewController new];
            avplayerViewController.player = player;
            //[self.view addSubview:avplayerViewController.view];
            [self presentViewController:avplayerViewController animated:YES completion:nil];
        }
    }
    else if ([_lbl_titlenm.text isEqualToString:@"Short Films"])
    {
        NSString *str=[NSString stringWithFormat:@"%@", [[dictres valueForKey:@"URL"]objectAtIndex:sender.tag]];  //is your str
        if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
            
            WebPlayerVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"WebPlayerVC"];
            next.strurl=[[dictres valueForKey:@"URL"]objectAtIndex:sender.tag];
            [self.navigationController pushViewController:next animated:NO];
            
        }
        else
        {
            NSURL *videoURL = [NSURL URLWithString:[[dictres valueForKey:@"URL"]objectAtIndex:sender.tag]];
            AVPlayer *player = [AVPlayer playerWithURL:videoURL];
            AVPlayerViewController *avplayerViewController = [AVPlayerViewController new];
            avplayerViewController.player = player;
            //[self.view addSubview:avplayerViewController.view];
            [self presentViewController:avplayerViewController animated:YES completion:nil];
        }
    }
    else if ([_lbl_titlenm.text isEqualToString:@"Reel"])
    {
        NSString *str=[NSString stringWithFormat:@"%@", [[dictres valueForKey:@"URL"]objectAtIndex:sender.tag]];  //is your str
        if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
            
            WebPlayerVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"WebPlayerVC"];
            next.strurl=[[dictres valueForKey:@"URL"]objectAtIndex:sender.tag];
            [self.navigationController pushViewController:next animated:NO];
            
        }
        else
        {
            NSURL *videoURL = [NSURL URLWithString:[[dictres valueForKey:@"URL"]objectAtIndex:sender.tag]];
            AVPlayer *player = [AVPlayer playerWithURL:videoURL];
            AVPlayerViewController *avplayerViewController = [AVPlayerViewController new];
            avplayerViewController.player = player;
            //[self.view addSubview:avplayerViewController.view];
            [self presentViewController:avplayerViewController animated:YES completion:nil];
        }
    }
    else if ([_lbl_titlenm.text isEqualToString:@"BTS"])
    {
        NSString *str=[NSString stringWithFormat:@"%@", [[dictres valueForKey:@"URL"]objectAtIndex:sender.tag]];  //is your str
        if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
            
            WebPlayerVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"WebPlayerVC"];
            next.strurl=[[dictres valueForKey:@"URL"]objectAtIndex:sender.tag];
            [self.navigationController pushViewController:next animated:NO];
            
        }
        else
        {
            NSURL *videoURL = [NSURL URLWithString:[[dictres valueForKey:@"URL"]objectAtIndex:sender.tag]];
            AVPlayer *player = [AVPlayer playerWithURL:videoURL];
            AVPlayerViewController *avplayerViewController = [AVPlayerViewController new];
            avplayerViewController.player = player;
            //[self.view addSubview:avplayerViewController.view];
            [self presentViewController:avplayerViewController animated:YES completion:nil];
        }
    }
    else if ([_lbl_titlenm.text isEqualToString:@"Videos"])
    {
        NSString *str=[NSString stringWithFormat:@"%@", [[dictres valueForKey:@"URL"]objectAtIndex:sender.tag]];  //is your str
        if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
            
            WebPlayerVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"WebPlayerVC"];
            next.strurl=[[dictres valueForKey:@"URL"]objectAtIndex:sender.tag];
            [self.navigationController pushViewController:next animated:NO];
            
        }
        else
        {
            NSURL *videoURL = [NSURL URLWithString:[[dictres valueForKey:@"URL"]objectAtIndex:sender.tag]];
            AVPlayer *player = [AVPlayer playerWithURL:videoURL];
            AVPlayerViewController *avplayerViewController = [AVPlayerViewController new];
            avplayerViewController.player = player;
            //[self.view addSubview:avplayerViewController.view];
            [self presentViewController:avplayerViewController animated:YES completion:nil];
        }
    }
    else if ([_lbl_titlenm.text isEqualToString:@"Head Shots"])
    {
        NSString *str=[NSString stringWithFormat:@"%@", [[dictres valueForKey:@"URL"]objectAtIndex:sender.tag]];  //is your str
        if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
            
            WebPlayerVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"WebPlayerVC"];
            next.strurl=[[dictres valueForKey:@"URL"]objectAtIndex:sender.tag];
            [self.navigationController pushViewController:next animated:NO];
            
        }
        else
        {
            NSURL *videoURL = [NSURL URLWithString:[[dictres valueForKey:@"URL"]objectAtIndex:sender.tag]];
            AVPlayer *player = [AVPlayer playerWithURL:videoURL];
            AVPlayerViewController *avplayerViewController = [AVPlayerViewController new];
            avplayerViewController.player = player;
            //[self.view addSubview:avplayerViewController.view];
            [self presentViewController:avplayerViewController animated:YES completion:nil];
        }
    }
}



- (IBAction)btn_MSG_ME:(id)sender {
    //[self.navigationController popViewControllerAnimated:YES];
    
    
    /* MessagePopVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MessagePopVC"];
     vc.view.frame = CGRectMake(0, 0, 233.0f, 236.0f);
     vc.Uid=[_mutDict valueForKey:@"UserID"];
     [self presentPopUpViewController:vc];*/
    
    MessagePopVC *homescreen = [self.storyboard instantiateViewControllerWithIdentifier:@"MessagePopVC"];
    homescreen.Uid=[_mutDict valueForKey:@"UserID"];
    homescreen.view.frame = self.view.bounds;
    [homescreen.view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5f]];
    [self.view addSubview:homescreen.view];
    [self addChildViewController:homescreen];
    [homescreen didMoveToParentViewController:self];
}

- (IBAction)btn_HOME:(id)sender {
    
    NewHomeVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:YES];
}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}

- (IBAction)btn_EDIT:(id)sender {
    EditProfileVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"EditProfileVC"];
    [self.navigationController pushViewController:next animated:YES];
    
}
- (IBAction)btn_MOVE_LEFT:(id)sender {
    
    NSInteger indexInt = _vw_caro.currentItemIndex+1;
    [_vw_caro scrollToItemAtIndex:indexInt animated:YES];
    
}

- (IBAction)btn_MOVE_RIGHT:(id)sender {
    
    NSInteger indexInt = _vw_caro.currentItemIndex-1;
    [_vw_caro scrollToItemAtIndex:indexInt animated:YES];
    
}

- (IBAction)btn_SUB_BACK:(id)sender {
    _vw_details.hidden=YES;
}

- (IBAction)btn_FOLL_UNF:(id)sender {
    
    [APP_DELEGATE showLoadingView:@""];
    
    
    if ([_btn_follow_unf.titleLabel.text isEqualToString:@"FOLLOW"])
    {
        [_btn_follow_unf setTitle: @"UNFOLLOW" forState: UIControlStateNormal];
        NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
        NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
        [AddPost setValue:[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"UserID"]] forKey:@"follower_id"];
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
        NSLog(@"---%@",AddPost);
        [manager POST:[NSString stringWithFormat:@"%@user_follow",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            [APP_DELEGATE hideLoadingView];
        }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  NSLog(@"Error: %@", error);
                  [APP_DELEGATE hideLoadingView];
              }];
    }
    else
    {
        [_btn_follow_unf setTitle: @"FOLLOW" forState: UIControlStateNormal];
        NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
        NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
        [AddPost setValue:[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"UserID"]] forKey:@"follower_id"];
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
        NSLog(@"---%@",AddPost);
        [manager POST:[NSString stringWithFormat:@"%@user_unfollow",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            [APP_DELEGATE hideLoadingView];
        }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  NSLog(@"Error: %@", error);
                  [APP_DELEGATE hideLoadingView];
              }];
    }
}

- (IBAction)btn_FOLLOWLIST:(id)sender {
    FollowListVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"FollowListVC"];
    
    next.arrFollow=arrFollow;
    next.arrFollowing=arrFollowing;
    
    [self.navigationController pushViewController:next animated:YES];
}
-(void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index{
    // attach long press gesture to collectionView
    NSLog(@"Selected carousel index is %ld",(long)index);
    
    self.vw_details.hidden=NO;
    
    //   _vw_caro.hidden = YES;
    
    _lbl_titlenm.text=[[arrimgnm valueForKey:@"PortfolioCategoryName"] objectAtIndex:index];
    _lbl_cat_id.text=[[arrimgnm valueForKey:@"PortfolioCategoryID"] objectAtIndex:index];
    
    [_coll_movies reloadData];
}

- (IBAction)btn_BACK:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

/////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////FOLLOWING SCREEN API///////////////////////////////////////////
-(void)CallFollower
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[_mutDict valueForKey:@"UserID"] forKey:@"user_id"];
    
    //------------
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@get_follower_list",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSLog(@"---%@",responseObject);
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            _lbl_follow_count.text=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"Total"]];
            
            
            
            
            arrFollow=[responseObject valueForKey:@"data"];
            //[self.navigationController pushViewController:next animated:YES];
            
        }
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
}
-(void)CallFollowing
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[_mutDict valueForKey:@"UserID"] forKey:@"user_id"];
    
    //------------
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@get_following_list",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSLog(@"---%@",responseObject);
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            _lbl_following_count.text=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"Total"]];
            
            arrFollowing=[responseObject valueForKey:@"data"];
            
        }
        [APP_DELEGATE hideLoadingView];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
}
- (UIImage* )imageFromMovie:(NSURL* )movieURL atTime:(NSTimeInterval)time {
    // set up the movie player
    MPMoviePlayerController *mp = [[MPMoviePlayerController alloc]
                                   initWithContentURL:movieURL];
    mp.shouldAutoplay = NO;
    mp.initialPlaybackTime = time;
    mp.currentPlaybackTime = time;
    // get the thumbnail
    thumbnail = [mp thumbnailImageAtTime:time
                              timeOption:MPMovieTimeOptionNearestKeyFrame];
    // clean up the movie player
    [mp stop];
    return(thumbnail);
}

@end
