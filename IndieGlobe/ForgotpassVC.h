//
//  ForgotpassVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/11/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextFieldValidator.h"
#import "HttpWrapper.h"
#import "ApplicationConstant.h"

@interface ForgotpassVC : UIViewController<HttpWrapperDelegate>
{
    HttpWrapper* httpforgot;
}
//Outlet
@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_forgotpass;
@property (strong, nonatomic) IBOutlet UIButton *btn_send;



//Action
- (IBAction)btn_SENDPASS:(id)sender;
- (IBAction)btn_GOLOGIN:(id)sender;


@end
