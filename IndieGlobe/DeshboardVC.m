//
//  DeshboardVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/27/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "DeshboardVC.h"
#import "ProfileVC.h"
#import "CritiquesVC.h"
#import "NewsVC.h"
#import "ClassifiedVC.h"
#import "SWRevealViewController.h"
#import "LearningVC.h"
#import "LoginVC.h"
#import "IndieVC.h"
#import "FundingVC.h"
#import "EventVC.h"
#import "ProjectVC.h"
#import "TeamBuildVC.h"
#import "PDVC.h"
#import "InviteTeamVC.h"
#import "Constants.h"
#import "circleVW.h"
#import "PaymentVC.h"
#import "EditEventVC.h"
#import "GlobeFundPop.h"
#import "RegisterVC.h"

@interface DeshboardVC ()

@end

@implementation DeshboardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    
    [self customSetup];
    // Do any additional setup after loading the view.
    
    
    //TAP GESTURE SIDE BAR
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];
    
    _vw_scroll.delegate=self;
    self.vw_scroll.contentSize =CGSizeMake(320, 420);
    
    
        
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self customSetup];
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if (IsIphone7 || IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = 145;
    }
    else{
        self.revealViewController.rearViewRevealWidth = 145;
    }

}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
       picker.hidden=YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)btn_EVENT:(id)sender {
    
    EventVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"EventVC"];
    [self.navigationController pushViewController:next animated:YES];

    
}



- (IBAction)btn_NEWS:(id)sender {
    NewsVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsVC"];
    [self.navigationController pushViewController:next animated:YES];
}

- (IBAction)btn_CRITIQUES:(id)sender {
    CritiquesVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"CritiquesVC"];
    [self.navigationController pushViewController:next animated:YES];
    
}

- (IBAction)btn_CLASSIFIED:(id)sender {
    ClassifiedVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"ClassifiedVC"];
    [self.navigationController pushViewController:next animated:YES];
}

- (IBAction)btn_LEARNING:(id)sender {
    LearningVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"LearningVC"];
    [self.navigationController pushViewController:next animated:YES];

}

- (IBAction)btn_INDIE:(id)sender {
    
    IndieVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"IndieVC"];
    [self.navigationController pushViewController:next animated:YES];
    
    
}

- (IBAction)btn_FUNDING:(id)sender {
    FundingVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"FundingVC"];
    [self.navigationController pushViewController:next animated:YES];
}

- (IBAction)BTN_2:(id)sender {
    TeamBuildVC* next = [self.storyboard instantiateViewControllerWithIdentifier:@"TeamBuildVC"];
    [self.navigationController pushViewController:next animated:YES];

    
}

- (IBAction)btn_3:(id)sender {
    ProjectVC* next = [self.storyboard instantiateViewControllerWithIdentifier:@"ProjectVC"];
    [self.navigationController pushViewController:next animated:YES];

}

- (IBAction)btn_4:(id)sender {
    PDVC* next = [self.storyboard instantiateViewControllerWithIdentifier:@"PDVC"];
    [self.navigationController pushViewController:next animated:YES];

}

- (IBAction)btn_SKIP:(id)sender {
    //
    /*UIViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"GlobeFundPop"];
    vc.view.frame = CGRectMake(0, 0, 233.0f, 236.0f);
    [self presentPopUpViewController:vc];*/

    /*RegisterVC* next = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterVC"];
    [self.navigationController pushViewController:next animated:YES];*/
    
    EditEventVC* next = [self.storyboard instantiateViewControllerWithIdentifier:@"EditEventVC"];
    [self.navigationController pushViewController:next animated:YES];
    
    
}

@end
