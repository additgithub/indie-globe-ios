//
//  AddEventVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/7/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "AddEventVC.h"
#import "WYPopoverController.h"
#import "DeshboardVC.h"
#import "Constants.h"
#import "NewsVC.h"
#import "HcdDateTimePickerView.h"
#import "UIColor+CL.h"
#import "NewHomeVC.h"
#import "AllNotificationVC.h"
#import "NewProfileCell.h"



@interface AddEventVC ()<WYPopoverControllerDelegate>{
    WYPopoverController* popoverController;
    HcdDateTimePickerView * dateTimePickerView;
}


@end

@implementation AddEventVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    [self customSetup];
    [self CustomizeTextField:_txt_title];
    _txt_title.placeholder=@"Title";
    [self CustomizeTextField:txt_country];
    txt_country.placeholder=@"Country";
    [self CustomizeTextField:txt_state];
    txt_state.placeholder=@"State";
    [self CustomizeTextField:txt_city];
    txt_city.placeholder=@"City";
    
    [self CustomizeTextField:_txt_from_date];
    _txt_from_date.placeholder=@"From Date";
    [self CustomizeTextField:_txt_to_date];
    _txt_to_date.placeholder=@"To Date";
    

    
   
    _txt_address.delegate=self;
    _txt_address.text = @"Address";
    _txt_address.textColor = [UIColor lightGrayColor];
    _txt_address.layer.borderWidth=1.0;
    _txt_address.layer.cornerRadius=6;
    _txt_address.layer.borderColor=[UIColor colorWithRed:(160.0f/255.0) green:(160.0f/255.0) blue:(160.0f/255.0) alpha:1].CGColor;
    _txt_address.textContainerInset = UIEdgeInsetsMake(5, 7, 0, 0);
    
    _txt_desc.delegate=self;
    _txt_desc.text = @"Description";
    _txt_desc.textColor = [UIColor lightGrayColor];
    _txt_desc.layer.borderWidth=1.0;
    _txt_desc.layer.cornerRadius=6;
    _txt_desc.layer.borderColor=[UIColor colorWithRed:(160.0f/255.0) green:(160.0f/255.0) blue:(160.0f/255.0) alpha:1].CGColor;
    _txt_desc.textContainerInset = UIEdgeInsetsMake(5, 7, 0, 0);


    
    // Do any additional setup after loading the view.

    _img_choose_img.clipsToBounds=YES;
    _img_choose_img.layer.cornerRadius=6;
    
    _btn_add_event.layer.cornerRadius=_btn_add_event.frame.size.height/2;
    
    
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];
    
    
    //Choose image
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
    [singleTap setNumberOfTapsRequired:1];
    [_img_choose_img addGestureRecognizer:singleTap];
    
    [self DatePickert];
    [self CallCountry];
    
    
    datePicker=[[UIDatePicker alloc]init];
    datePicker.datePickerMode=UIDatePickerModeDateAndTime;
    [_txt_to_date setInputView:datePicker];
    [_txt_from_date setInputView:datePicker];
    
    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolBar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(ShowSelectedDate)];
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn, nil]];
    [_txt_to_date setInputAccessoryView:toolBar];
    
    UIToolbar *toolBar1=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolBar1 setTintColor:[UIColor grayColor]];
    UIBarButtonItem *doneBtn1=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(ShowSelectedDate1)];
    UIBarButtonItem *space1=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar1 setItems:[NSArray arrayWithObjects:space1,doneBtn1, nil]];
    [_txt_from_date setInputAccessoryView:toolBar1];
    
    
    AddNew=[[NSMutableArray alloc] init];
    
    if (AddNew.count <= 0) {
        //Array Allocation
        AddNew = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
        
    }
    
    _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
    _lbl_notifi_count.clipsToBounds=YES;
    _lbl_notifi_count.text=theAppDelegate.n_Count;
    
    if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
        _lbl_notifi_count.hidden=YES;
    }
    else
    {
        _lbl_notifi_count.hidden=NO;
    }
    
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return AddNew.count;
    
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NewProfileCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    cell = nil;
    if (cell == nil)
    {
        cell=(NewProfileCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    }
    cell.img_movi_image.clipsToBounds = YES;
    cell.img_movi_image.layer.masksToBounds = YES;
    cell.img_movi_image.layer.cornerRadius = 10.0f;
    cell.layer.cornerRadius = 10.0f;
    
    NSLog(@"AddNew %@",AddNew);
    if ([AddNew[indexPath.row] isEqual:@"addNew"]) {
        cell.img_movi_image.image = [UIImage imageNamed:@"add-photo-withplus"];
        cell.btn_close_b.hidden = YES;
        cell.lbl_movie_title.hidden = YES;
        cell.btn_play_b.hidden=YES;
    }
    else
    {
        cell.btn_close_b.hidden = NO;
        cell.lbl_movie_title.hidden = YES;
        cell.btn_play_b.hidden=YES;
        cell.btn_close_b.tag = indexPath.row;
        [cell.btn_close_b addTarget:self action:@selector(masterAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.img_movi_image.image = AddNew[indexPath.row];
        
        //[cell.img_movi_image sd_setImageWithURL:AddNew[indexPath.row] placeholderImage:[UIImage imageNamed:@"Avatar"]];
        //cell.Img_Photos.image=[UIImage imageNamed:[AddNew objectAtIndex:indexPath.row]];
    }
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    if ([AddNew[indexPath.row] isEqual:@"addNew"]) {
        ci= [[UIImagePickerController alloc] init];
        ci.delegate = self;
        ci.allowsEditing=YES;
        ci.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        
        if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
            [self presentViewController:ci animated:YES completion:nil];
        else
        {
            popover=[[UIPopoverController alloc]initWithContentViewController:ci];
            [popover presentPopoverFromRect:_img_choose_img.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
    }
    
    
    
}

-(void)masterAction:(UIButton*)sender
{
    UIAlertController * alert=[UIAlertController
                               
                               alertControllerWithTitle:@"" message:@"Are you sure you want to delete"preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Yes"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    //What we write here????????**
                                    NSLog(@"YES");
                                    
                                    [AddNew removeObjectAtIndex:sender.tag];
                                    
                                    [_colle_addevent reloadData];
                                    
                                }];
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"No"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   //What we write here????????**
                                   NSLog(@"NO");
                                   
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)ShowSelectedDate
{   NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"MM-dd-YYYY hh:mm a"];
    _txt_to_date.text=[NSString stringWithFormat:@"%@",[formatter stringFromDate:datePicker.date]];
    [_txt_to_date resignFirstResponder];
}
-(void)ShowSelectedDate1
{   NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"MM-dd-YYYY hh:mm a"];
    _txt_from_date.text=[NSString stringWithFormat:@"%@",[formatter stringFromDate:datePicker.date]];
    [_txt_from_date resignFirstResponder];
}
#pragma DatePickerView

-(void)DatePickert
{
    // make the textfield its own delegate
    _txt_from_date.delegate = self;
    _txt_to_date.delegate=self;
    
    // alloc/init your date picker, and (optional) set its initial date
    datePicker = [[UIDatePicker alloc]init];
    
    datePicker.datePickerMode=UIDatePickerModeDate;
    datePicker.backgroundColor=[UIColor groupTableViewBackgroundColor];
    datePicker.minimumDate = [NSDate date];
    
  /*  UIDatePickerModeTime,
    UIDatePickerModeDate,
    UIDatePickerModeDateAndTime,
    UIDatePickerModeCountDownTimer*/
    
    [datePicker setDate:[NSDate date]]; //this returns today's date
    
    // theMinimumDate (which signifies the oldest a person can be) and theMaximumDate (defines the youngest a person can be) are the dates you need to define according to your requirements, declare them:
    
    // the date string for the minimum age required (change according to your needs)
    NSString *maxDateString = @"2017";
    // the date formatter used to convert string to date
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // the specific format to use
    dateFormatter.dateFormat = @"yyyy-mm-dd HH:mm";
    // converting string to date
    NSDate *theMaximumDate = [dateFormatter dateFromString: maxDateString];
    
    // repeat the same logic for theMinimumDate if needed
    
    // here you can assign the max and min dates to your datePicker
    //[datePicker setMaximumDate:theMaximumDate]; //the min age restriction
    [datePicker setMinimumDate:theMaximumDate]; //the max age restriction (if needed, or else dont use this line)
    
    // set the mode
    //[datePicker setDatePickerMode:UIDatePickerModeDate];
    
    // update the textfield with the date everytime it changes with selector defined below
    [datePicker addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
    
    // and finally set the datePicker as the input mode of your textfield
    //[_txt_from_date setInputView:datePicker];
    //[_txt_to_date setInputView:datePicker];
    
    
}


#pragma mark - Picker Delegate
// Number of components.
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

// Total rows in our component.
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [dataArray count];
}

// Display each row's data.
-(NSString* )pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    _txt_from_date.text = [dataArray objectAtIndex: row];
    return [dataArray objectAtIndex: row];
}

// Do something with the selected row.
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSLog(@"You selected this: %@", [dataArray objectAtIndex: row]);
    if (pickerView==_txt_to_date) {
        _txt_from_date.text = [dataArray objectAtIndex: row];
    }
    else
    {
        _txt_to_date.text = [dataArray objectAtIndex: row];
    }
}

-(void)updateTextField:(id)sender {
    
    UIDatePicker *picker = (UIDatePicker*)_txt_from_date.inputView;
    _txt_from_date.text = [self formatDate:picker.date];
    
    
    
}

- (NSString *)formatDate:(NSDate* )date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"dd-mm-yyyy HH:mm"];//hh:mm a
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    
    return formattedDate;
}

#pragma mark - GetAllPosts
-(void)CallAddEvent
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:_txt_title.text forKey:@"title"];
    [AddPost setValue:_txt_address.text forKey:@"address"];
    [AddPost setValue:countryid forKey:@"country"];
    [AddPost setValue:stateid forKey:@"state"];
    [AddPost setValue:cityid forKey:@"city"];
    [AddPost setValue:_txt_desc.text forKey:@"description"];
    
    
    //--------DATE CONVERT------------------
    NSString *strdt=[[NSString alloc]init];
    strdt=_txt_from_date.text;
    NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
    [formatedt setDateFormat:@"MM-dd-yyyy hh:mm a"];
    NSDate *fulldate=[formatedt dateFromString:strdt];
    [formatedt setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *finaldt=[formatedt stringFromDate:fulldate];
    _txt_from_date.text=[NSString stringWithFormat:@"%@",finaldt];
    
    NSString *strdt1=[[NSString alloc]init];
    strdt1=_txt_to_date.text;
    NSDateFormatter *formatedt1=[[NSDateFormatter alloc]init];
    [formatedt1 setDateFormat:@"MM-dd-yyyy hh:mm a"];
    NSDate *fulldate1=[formatedt1 dateFromString:strdt1];
    [formatedt1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *finaldt1=[formatedt1 stringFromDate:fulldate1];
    _txt_to_date.text=[NSString stringWithFormat:@"%@",finaldt1];
    
    
    [AddPost setValue:_txt_from_date.text forKey:@"start_date"];
    [AddPost setValue:_txt_to_date.text forKey:@"end_date"];
    
    if (select_image>0) {
        //Create manager
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
        //Now post
        NSLog(@"AddPost %@",AddPost);
        NSString *url = [NSString stringWithFormat:@"%@add_event",JsonUrlConstant];
        [manager POST:url parameters:[AddPost copy] constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            //add img data one by one
            
            NSData *imageData = UIImageJPEGRepresentation(select_image,0.5);
            [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"event_image"] fileName:[NSString stringWithFormat:@"user_image.jpg"] mimeType:@"image/jpeg"];
            
        } success:^(AFHTTPRequestOperation *operation, id dicsResponse) {
            // [(AppDelegate *)[[UIApplication sharedApplication] delegate] hideProgress];
            NSLog(@"Success: %@", dicsResponse);
            NSLog(@"Edit profile---%@",dicsResponse);
            NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            
            if ([[dicsResponse valueForKey:@"status"] integerValue] == 0) {
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
               // [alert show];
                
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
               // [alert show];
                [self.navigationController popViewControllerAnimated:YES];
                
            }
            [APP_DELEGATE hideLoadingView];
            
        }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  NSLog(@"Error: %@", error);
                  [APP_DELEGATE hideLoadingView];
              }];
    }
    else
    {
        NSLog(@" url is %@login",JsonUrlConstant);
        [AddPost setObject:@"" forKey:@"event_image"];
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
            httpaddevent  = [[HttpWrapper alloc] init];
            httpaddevent.delegate=self;
            httpaddevent.getbool=NO;
            [httpaddevent requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@add_event",JsonUrlConstant] param:[AddPost copy]];
        });

    }

    
}



-(void)CallAddEventMultipleImage
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:_txt_title.text forKey:@"title"];
    [AddPost setValue:_txt_address.text forKey:@"address"];
    [AddPost setValue:countryid forKey:@"country"];
    [AddPost setValue:stateid forKey:@"state"];
    [AddPost setValue:cityid forKey:@"city"];
    [AddPost setValue:_txt_desc.text forKey:@"description"];
    
    //--------DATE CONVERT------------------
    NSString *strdt=[[NSString alloc]init];
    strdt=_txt_from_date.text;
    NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
    [formatedt setDateFormat:@"MM-dd-yyyy hh:mm a"];
    NSDate *fulldate=[formatedt dateFromString:strdt];
    [formatedt setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *finaldt=[formatedt stringFromDate:fulldate];
    _txt_from_date.text=[NSString stringWithFormat:@"%@",finaldt];
    
    NSString *strdt1=[[NSString alloc]init];
    strdt1=_txt_to_date.text;
    NSDateFormatter *formatedt1=[[NSDateFormatter alloc]init];
    [formatedt1 setDateFormat:@"MM-dd-yyyy hh:mm a"];
    NSDate *fulldate1=[formatedt1 dateFromString:strdt1];
    [formatedt1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *finaldt1=[formatedt1 stringFromDate:fulldate1];
    _txt_to_date.text=[NSString stringWithFormat:@"%@",finaldt1];
    
    
    [AddPost setValue:_txt_from_date.text forKey:@"start_date"];
    [AddPost setValue:_txt_to_date.text forKey:@"end_date"];
    
    // NSLog(@"myRequestString:%@",strURL);
    // Create Data from request
    NSString *url = [NSString stringWithFormat:@"%@add_event",JsonUrlConstant];
    NSMutableURLRequest *urequest = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString:url]];
    
    NSLog(@"myRequestString:%@",urequest);
    
    [urequest setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [urequest setHTTPShouldHandleCookies:NO];
    [urequest setTimeoutInterval:60];
    [urequest setHTTPMethod:@"POST"];
    
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [urequest setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in AddPost) {
        NSLog(@"Param:%@",param);
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [AddPost objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // Add __VIEWSTATE
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"__VIEWSTATE\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"/wEPDwUKLTQwMjY2MDA0M2RkXtxyHItfb0ALigfUBOEHb/mYssynfUoTDJNZt/K8pDs=" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"Body:%@",body);
    
    // add image data profileImage
    
    for (int i = 0; i < AddNew.count-1; i++) {
        
        NSData* imgData = [[NSData alloc] init];
        UIImage *eachImage  = [AddNew objectAtIndex:i+1];
        
        imgData = UIImageJPEGRepresentation(eachImage, 0.5);
        
        
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"event_image[%@]\"; filename=\"IMG_%@_%f.jpg\"\r\n",[NSString stringWithFormat:@"%d",i],[NSString stringWithFormat:@"%d",i],[[NSDate date] timeIntervalSince1970]] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imgData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        
    }
    
    
    ///....................remove
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [urequest setHTTPBody:body];
    //return and test
    NSHTTPURLResponse *response = nil;
    NSData *returnData = [NSURLConnection sendSynchronousRequest:urequest returningResponse:&response error:nil];
    //remove..
    NSLog(@"jsourequestn :%@",urequest);
    
    NSError* error;
    // NSLog(@"return data..%@",returnData);
    if(returnData != nil)
    {
        
        NSLog(@"jsourequestn :%@",returnData);
        NSLog(@"response: %@", [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding]);
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:returnData
                                                             options:kNilOptions
                                                               error:&error];
        
        NSLog(@"json :%@",json);
        
        
        
        NSLog(@"json :%@",json);
        
        if([json valueForKey:@"status"])
        {
            UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Event  added." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [Notpermitted show];
            
            
            [self.navigationController popViewControllerAnimated:YES];
            // [self saveSignUpData:json];
            // return json;
        }else{
            UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [Notpermitted show];
            
            
            // return json;
        }
        
    }
    
}


-(void)CallCountry
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    NSLog(@" url is %@login",JsonUrlConstant);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpcountry  = [[HttpWrapper alloc] init];
        httpcountry.delegate=self;
        httpcountry.getbool=NO;
        [httpcountry requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_country",JsonUrlConstant] param:[AddPost copy]];
        
        
        
        
    });
}
-(void)Callstate:(NSString *)str
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:str forKey:@"country_id"];
    NSLog(@"%@",AddPost);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpstate  = [[HttpWrapper alloc] init];
        httpstate.delegate=self;
        httpstate.getbool=NO;
        [httpstate requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_state"
                                                  ,JsonUrlConstant] param:[AddPost copy]];
    });
    
}
-(void)CallCity:(NSString *)str
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:str forKey:@"state_id"];
    NSLog(@"%@",AddPost);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpcity  = [[HttpWrapper alloc] init];
        httpcity.delegate=self;
        httpcity.getbool=NO;
        [httpcity requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_city"
                                                 ,JsonUrlConstant] param:[AddPost copy]];
    });
    
}

#pragma mark- Wrapper Class Delegate Methods
//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary* )dicsResponse
{
    [APP_DELEGATE hideLoadingView];
    if(wrapper == httpaddevent && httpaddevent != nil)
    {
        NSLog(@"---%@",dicsResponse);
        NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
        
        if ([[dicsResponse valueForKey:@"status"] integerValue] == 0) {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"ADD" message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //[alert show];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"ADD" message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //[alert show];
            [self.navigationController popViewControllerAnimated:YES];
            
        }

        
    }
    else if(wrapper == httpcountry && httpcountry != nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        dict=[dicsResponse valueForKey:@"data"];
        country= [dicsResponse valueForKey:@"data"];
        NSLog(@"%@",country);
        
    }
    else if (wrapper==httpstate && httpstate !=nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        if ([dicsResponse valueForKey:@"status"]!=0) {
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[dicsResponse valueForKey:@"data"];
            state= [dicsResponse valueForKey:@"data"];
            NSLog(@"%@",state);
        }
    }
    else if (wrapper==httpcity && httpcity !=nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        if ([dicsResponse valueForKey:@"status"]!=0) {
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[dicsResponse valueForKey:@"data"];
            arrcity= [dicsResponse valueForKey:@"data"];
            NSLog(@"%@",arrcity);
        }
    }

}


//fetchDataFail
- (void) HttpWrapper:(HttpWrapper* )wrapper fetchDataFail:(NSError* )error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}




#pragma ImagePicker
-(void)singleTapping:(UIGestureRecognizer *)recognizer {
    [self.view endEditing:YES];
    NSLog(@"image clicked");
    ci= [[UIImagePickerController alloc] init];
    ci.delegate = self;
    ci.allowsEditing = NO;
    ci.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
        [self presentViewController:ci animated:YES completion:nil];
    else
    {
        popover=[[UIPopoverController alloc]initWithContentViewController:ci];
        [popover presentPopoverFromRect:_img_choose_img.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}

#pragma mark - ImagePickerController Delegate
-(void)imagePickerController:(UIImagePickerController *)pick didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        //[pick dismissViewControllerAnimated:YES completion:nil];
    } else {
        [popover dismissPopoverAnimated:YES];
    }
    
    
    _img_choose_img.image = [info objectForKey:UIImagePickerControllerEditedImage];
    //image = [info objectForKey:UIImagePickerControllerEditedImage];
    _img_extra.hidden=YES;
    _lbl_extra.hidden=YES;
    

    
    select_image=[info objectForKey:UIImagePickerControllerEditedImage];
    _img_choose_img.image=[info objectForKey:UIImagePickerControllerEditedImage];
    //_img_chooseimg.image=image;
    _img_extra.hidden=YES;
    //_lbl_extra_lab.hidden=YES;
    
    [AddNew insertObject:select_image atIndex:[AddNew count]];
    [_colle_addevent reloadData];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)imageCropper:(UzysImageCropperViewController *)cropper didFinishCroppingWithImage:(UIImage *)image
{
    select_image=image;
    _img_choose_img.image=image;
    //_img_chooseimg.image=image;
    _img_extra.hidden=YES;
    //_lbl_extra_lab.hidden=YES;
    
    [AddNew insertObject:image atIndex:[AddNew count]];
    [_colle_addevent reloadData];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)pick
{
    [pick dismissViewControllerAnimated:YES completion:nil];
}


- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if (IsIphone7 || IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = 145;
    }
    else{
        self.revealViewController.rearViewRevealWidth = 145;
    }
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([textView.text isEqualToString:@"Address"]||[textView.text isEqual: @"Description"]) {
        return YES;
    }
    else
    {
        return textView.text.length + (text.length - range.length) <= 500;
    }
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if([textView.text isEqual: @"Address"]||[textView.text isEqual: @"Description"])
    {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    else
    {
        //textView.text = @"";
        textView.textColor = [UIColor blackColor];
        //Discription
    }
    return YES;
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self customSetup];
}


-(void)CustomizeTextField:(UITextField *)myTextField
{
    myTextField.layer.borderWidth=1.0;
    myTextField.layer.borderColor=[UIColor colorWithRed:(160.0f/255.0) green:(160.0f/255.0) blue:(160.0f/255.0) alpha:1].CGColor;
    CGRect frameRect = myTextField.frame;
    frameRect.size.height = 28;
    myTextField.frame = frameRect;
    myTextField.layer.cornerRadius=myTextField.frame.size.height/2;
    
    
    if (myTextField==_txt_from_date || myTextField==_txt_to_date) {
        [myTextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 13, 20)];
        myTextField.leftView = paddingView;
        myTextField.leftViewMode = UITextFieldViewModeAlways;

    }
    else
    {
        [myTextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
        myTextField.leftView = paddingView;
        myTextField.leftViewMode = UITextFieldViewModeAlways;

    }
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_ADDEVENT:(id)sender {
    
    
    if ([self CheckTextValidation:_txt_title :@"Please Enter Title."]) {
        if (![_txt_address.text isEqualToString:@""] && ![_txt_address.text isEqualToString:@"Address"] ) {
            if ([self CheckTextValidation:txt_country :@"Please Enter Country."]) {
                if ([self CheckTextValidation:txt_state :@"Please Enter State."]) {
                    if ([self CheckTextValidation:txt_city :@"Please Enter City."]) {
                        if ([self CheckTextValidation:_txt_from_date :@"Please Enter From Date."]) {
                            if ([self CheckTextValidation:_txt_to_date :@"Please Enter To Date."]) {
                                if (AddNew.count>0) {
                                    if (![_txt_desc .text isEqualToString:@""] && ![_txt_desc.text isEqualToString:@"Description"]) {
                                        [self CallAddEventMultipleImage];
                                    }
                                    else
                                    {
                                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Enter Description." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                                        [alert show];
                                    }
                                }
                                else
                                {
                                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Image must be add." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                                    [alert show];
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Enter Address." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
    }
    
    
}
-(BOOL)CheckTextValidation :(UITextField*)txtF : (NSString *)msg
{
    
    if ([txtF.text isEqualToString:@""]) {
       
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
        return NO;
    }
    else
    {
        return  YES;
    }
    
    
    
}

- (IBAction)btn_HOME:(id)sender {
    [self.view endEditing:YES];
    NewHomeVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:YES];
    
}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}


- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_SCOUNTRY:(id)sender {
    [self.view endEditing:YES];
    isCheck=0;
    [txt_country setTag:0];
    currentTextField = txt_country;
    [self showPopover:sender];
}

- (IBAction)btn_SSTATE:(id)sender {
    [self.view endEditing:YES];
    isCheck=1;
    [txt_state setTag:1];
    currentTextField = txt_state;
    [self showPopover:sender];

}

- (IBAction)btn_SCITY:(id)sender {
    [self.view endEditing:YES];
    isCheck=2;
    [txt_city setTag:2];
    currentTextField = txt_city;
    [self showPopover:sender];

}

- (IBAction)btn_SFROM_DATE:(id)sender {
    
    __block AddEventVC *weakSelf = self;
    dateTimePickerView = [[HcdDateTimePickerView alloc] initWithDatePickerMode:DatePickerDateTimeMode defaultDateTime:[[NSDate alloc]initWithTimeIntervalSinceNow:1000]];
    dateTimePickerView.topViewColor = [UIColor colorWithHex:0x999999];
    dateTimePickerView.buttonTitleColor = [UIColor whiteColor];

    
    [dateTimePickerView setMinYear:2016];
    [dateTimePickerView setMaxYear:2030];
    dateTimePickerView.clickedOkBtn = ^(NSString * datetimeStr){
        NSLog(@"%@", datetimeStr);
        weakSelf.txt_from_date.text = datetimeStr;
        weakSelf.txt_to_date.text=@"";
    };
    
    if (dateTimePickerView) {
        [self.view addSubview:dateTimePickerView];
        [dateTimePickerView showHcdDateTimePicker];
    }
}

- (IBAction)btn_STO_DATE:(id)sender {
   
    __block AddEventVC *weakSelf = self;
    dateTimePickerView = [[HcdDateTimePickerView alloc] initWithDatePickerMode:DatePickerDateTimeMode defaultDateTime:[[NSDate alloc]initWithTimeIntervalSinceNow:1000]];
    dateTimePickerView.topViewColor = [UIColor colorWithHex:0x999999];
    dateTimePickerView.buttonTitleColor = [UIColor whiteColor];
    [dateTimePickerView setMinYear:2010];
    [dateTimePickerView setMaxYear:2030];
    dateTimePickerView.clickedOkBtn = ^(NSString * datetimeStr){
        NSLog(@"%@", datetimeStr);
        
       //==================COMAPARE DATE================
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *date1 = [dateFormatter dateFromString:_txt_from_date.text];
        
        NSDateFormatter *dateFormatte = [[NSDateFormatter alloc] init];
        [dateFormatte setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *date2 = [dateFormatte dateFromString:datetimeStr];
        
        switch ([date1 compare:date2]) {
            case NSOrderedAscending:
                //Do your logic when date1 > date2
                NSLog(@"OK");
                weakSelf.txt_to_date.text = datetimeStr;
                break;
                
            case NSOrderedDescending:
                //Do your logic when date1 < date2
                NSLog(@"NOT OK");
                weakSelf.txt_to_date.text = @"Select Date!";
                break;
                
            case NSOrderedSame:
                //Do your logic when date1 = date2
                NSLog(@"SAME DATE");
                weakSelf.txt_to_date.text = datetimeStr;
                break;
        }
        
        //===============================================
        
        
    };
    
    if (dateTimePickerView) {
        [self.view addSubview:dateTimePickerView];
        [dateTimePickerView showHcdDateTimePicker];
    }

}

- (IBAction)btn_NOTIFY:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}

- (void)touchesBegan:(NSSet* )touches withEvent:(UIEvent* )event {
    [self.view endEditing:YES];
}

- (void)showPopover:(UIView*)btn;
{
    
    if (popoverController == nil)
    {
        PopOverView *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PopOverView"];
        
        settingsViewController.index = currentTextField.tag;
        
        if (isCheck == 0) {
            settingsViewController.marrcountry = country;
        }
        else if (isCheck==1)
        {
            settingsViewController.marrstate = state;
        }
        else if (isCheck==2)
        {
            settingsViewController.marrcity = arrcity;
        }
        else{
            NSLog(@"NO DATA FROM API");
        }

        
        
        if ([settingsViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
            settingsViewController.preferredContentSize = CGSizeMake(200, 170);             // iOS 7
        }
        else {
#pragma clang diagnostic pushc
#pragma GCC diagnostic ignored "-Wdeprecated"
            settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 170);      // iOS < 7
#pragma clang diagnostic pop
        }
        settingsViewController.delegates = self;
        
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        
        settingsViewController.modalInPopover = NO;
        popoverController = [[WYPopoverController alloc] initWithContentViewController:settingsViewController];
        
        popoverController.delegate = self;
        
        popoverController.passthroughViews = @[btn];
        popoverController.popoverLayoutMargins = UIEdgeInsetsMake(0, 10, 10 , 10);
        popoverController.wantsDefaultContentAppearance = YES;
        
        [popoverController presentPopoverFromRect:btn.bounds
                                           inView:btn
                         permittedArrowDirections:WYPopoverArrowDirectionAny
                                         animated:YES
                                          options:WYPopoverAnimationOptionFadeWithScale];
    }
    
}

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    popoverController.delegate = nil;
    popoverController = nil;
}

-(void)dismissPopUpViewController
{
    [popoverController dismissPopoverAnimated:YES];
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)Popvalueselected:(NSString*)strValue{
    NSLog(@"POP:%@",strValue);
    
    currentTextField.text = strValue;
    
    if (isCheck==0) {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [country filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        countryid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        
        txt_state.text=@"";
        txt_city.text=@"";
        
        [self Callstate:countryid];
        
    }
    else if (isCheck==1)
    {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [state filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        stateid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        txt_city.text=@"";
        [self CallCity:stateid];
    }
    else if (isCheck==2)
    {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [arrcity filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        cityid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        
    }
    else
    {
        currentTextField.text = strValue;
    }

}


@end
