//
//  LoginVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/24/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextFieldValidator.h"
#import "HttpWrapper.h"


@interface LoginVC : UIViewController<CAAnimationDelegate,UITextFieldDelegate,HttpWrapperDelegate>
{
    BOOL isChecked;
    HttpWrapper *httplogin;
    
    NSString * textMultiLine1;
    NSString * PlayerId;
}

//Outlet

@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_username;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_password;
@property (strong, nonatomic) IBOutlet UIButton *btn_login;
@property (strong, nonatomic) IBOutlet UIImageView *img_chkbox;


//Action
- (IBAction)btn_LOGIN:(id)sender;
- (IBAction)btn_FORGOT:(id)sender;
- (IBAction)btn_SIGNUP:(id)sender;
- (IBAction)btn_CHKBOX:(id)sender;
- (IBAction)btn_MENUBAR:(id)sender;
- (IBAction)btn_BACK:(id)sender;

@end
