//
//  LearningCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/30/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShadowView.h"


@interface LearningCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *img_main_image;
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;
@property (strong, nonatomic) IBOutlet UIButton *img_playbtn;
@property (strong, nonatomic) IBOutlet UILabel *lbl_profe;
@property (strong, nonatomic) IBOutlet UILabel *lbl_smon;
@property (strong, nonatomic) IBOutlet UILabel *lbl_desc;
@property (strong, nonatomic) IBOutlet UILabel *lbl_sdate;


@property (weak, nonatomic) IBOutlet ShadowView *shadow_vw;

@end
