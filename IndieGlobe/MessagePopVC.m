//
//  MessagePopVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/28/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "MessagePopVC.h"
#import "UIColor+CL.h"
#import "JWBlurView.h"
#import "ProfileVC.h"

@interface MessagePopVC ()

@end

@implementation MessagePopVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    ///////////TEXT VIEW ————
    _txt_msg.delegate=self;
    _txt_msg.text = @"Enter your message";
    _txt_msg.textColor = [UIColor lightGrayColor];
    _txt_msg.layer.borderWidth=1.0;
    _txt_msg.layer.cornerRadius=10;
    _txt_msg.layer.borderColor=[UIColor colorWithHex:0x01B5A9].CGColor;
    
    _btn_send.layer.cornerRadius=_btn_send.frame.size.height/2;
    
    NSLog(@"USER ID %@",_Uid);
    
    _vw_backgroung.layer.cornerRadius=5;
    
    
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [_vw_backgroung addGestureRecognizer:singleFingerTap];
    
    
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if([textView.text isEqual: @"Enter your message"])
    {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    return YES;
}
-(void)CallSendMSG
{
    [APP_DELEGATE showLoadingView:@""];
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:_txt_msg.text forKey:@"message"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",_Uid] forKey:@"receiver_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    [manager POST:[NSString stringWithFormat:@"%@send_message",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        [APP_DELEGATE hideLoadingView];
        
        [self dismissViewControllerAnimated:NO completion:nil];
        [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil];
    }
    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
     }];

}

//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    
   /* ProfileVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileVC"];
    [self.navigationController pushViewController:next animated:NO];*/
    
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (IBAction)btn_SEND:(id)sender {
    
    if ((![_txt_msg.text isEqualToString:@"Enter your message"]) && (![_txt_msg.text isEqualToString:@""]))
    {
        [self CallSendMSG];
        _txt_msg.text=@"";
        [self performSelector:@selector(handleSingleTap:) withObject:nil afterDelay:0.5];
    }
    
    
   // [self.navigationController popToRootViewControllerAnimated:NO];
    
}
@end
