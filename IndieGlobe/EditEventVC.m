//
//  EditEventVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/23/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "EditEventVC.h"
#import "EditEventCell.h"
#import "UIColor+CL.h"
#import "PopOverView.h"
#import "WYPopoverController.h"
#import "Constants.h"
#import "HcdDateTimePickerView.h"
#import "TextFieldValidator.h"
#import "NewHomeVC.h"
#import "AllNotificationVC.h"
#import "NewProfileCell.h"



@interface EditEventVC ()<WYPopoverControllerDelegate>{
    WYPopoverController* popoverController;
    HcdDateTimePickerView * dateTimePickerView;
    
}

@end

@implementation EditEventVC
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self customSetup];
    
    
    [self CustomizeTextField:_txt_skv];
    _txt_skv.placeholder=@"SKV open studio tours";
    
    [self CustomizeTextField:txt_country];
    txt_country.placeholder=@"Country";
    
    [self CustomizeTextField:txt_city];
    txt_city.placeholder=@"City";
    
    [self CustomizeTextField:txt_state];
    txt_state.placeholder=@"state";
    
    
    [self CustomizeTextField:_txt_date];
    _txt_date.placeholder=@"From";
    [self CustomizeTextField:_txt_from_date];
    _txt_from_date.placeholder=@"To";
    
    
    //_txt_disc.delegate=self;
    //_txt_disc.text = @"Address";
    _txt_disc.textColor = [UIColor blackColor];
    _txt_disc.layer.borderWidth=1.0;
    _txt_disc.layer.cornerRadius=10;
    _txt_disc.layer.borderColor=[UIColor colorWithRed:(160.0f/255.0) green:(160.0f/255.0) blue:(160.0f/255.0) alpha:1].CGColor;
    
    
    //_txt_new_disc.delegate=self;
    //_txt_new_disc.text = @"Discription";
    _txt_new_disc.textColor = [UIColor blackColor];
    _txt_new_disc.layer.borderWidth=1.0;
    _txt_new_disc.layer.cornerRadius=10;
    _txt_new_disc.layer.borderColor=[UIColor colorWithRed:(160.0f/255.0) green:(160.0f/255.0) blue:(160.0f/255.0) alpha:1].CGColor;
    
    _btn_save_event.layer.cornerRadius=_btn_save_event.frame.size.height/2;
    
    
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];
    
    
    //Choose image
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
    [singleTap setNumberOfTapsRequired:1];
    [_img_images addGestureRecognizer:singleTap];
    
    
    
    NSLog(@"Mut Dict ---%@",_mutDict);
    Default=0;
    _txt_skv.text=[_mutDict valueForKey:@"Title"];
    _txt_disc.text=[_mutDict valueForKey:@"Address"];
    _txt_new_disc.text=[_mutDict valueForKey:@"EventDetail"];
    
    // [_img_images sd_setImageWithURL:[NSURL URLWithString:[_mutDict valueForKey:@"EventImageURL"]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
    
    NSURL * url = [NSURL URLWithString:[_mutDict valueForKey:@"EventImageURL"]];
    NSData * data = [NSData dataWithContentsOfURL:url];
    ISimage= [UIImage imageWithData:data];
    
    start_dt=[_mutDict valueForKey:@"StartDate"];
    end_dt=[_mutDict valueForKey:@"EndDate"];
    
    
    NSMutableArray *image_arr=[[NSMutableArray alloc]init];
    image_arr=[_mutDict valueForKey:@"event_images"];
    
    
    AddNew=[[NSMutableArray alloc] init];
    
    
    for (int i=0; i<image_arr.count+1; i++) {
        if ( i == 0) {
            //Array Allocation
            AddNew = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
            
        }
        else
        {//
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[image_arr valueForKey:@"ImageURL"][i-1]]];
            
            NSData *data = [NSData dataWithContentsOfURL:url];
            UIImage *image = [UIImage imageWithData:data];
            
            
            [AddNew addObject:image];
        }
        
        NSLog(@"%@",AddNew);
    }
    
    NSLog(@"%@",AddNew);
    
    
    
    
    
    //--------DATE CONVERT------------------
    NSString *strSdt=[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"StartDate"]];
    NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
    [formatedt setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *fulldate=[formatedt dateFromString:strSdt];
    [formatedt setDateFormat:@"MM-dd-yyyy hh:mm a"];
    NSString *finaldt=[formatedt stringFromDate:fulldate];
    _txt_date.text=[NSString stringWithFormat:@"%@",finaldt];
    
    
    NSString *strEdt=[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"EndDate"]];
    NSDateFormatter *formatedt1=[[NSDateFormatter alloc]init];
    [formatedt1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *fulldate1=[formatedt1 dateFromString:strEdt];
    
    [formatedt1 setDateFormat:@"MM-dd-yyyy hh:mm a"];
    NSString *finaldt1=[formatedt1 stringFromDate:fulldate1];
    _txt_from_date.text=[NSString stringWithFormat:@"%@",finaldt1];
    
    [self CallCountry];
    
    //////---------------NEW DATE PICKER-----------
    datePicker=[[UIDatePicker alloc]init];
    datePicker.datePickerMode=UIDatePickerModeDateAndTime;
    [_txt_date setInputView:datePicker];
    [_txt_from_date setInputView:datePicker];
    
    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolBar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(ShowSelectedDate)];
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn, nil]];
    [_txt_date setInputAccessoryView:toolBar];
    
    UIToolbar *toolBar1=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolBar1 setTintColor:[UIColor grayColor]];
    UIBarButtonItem *doneBtn1=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(ShowSelectedDate1)];
    UIBarButtonItem *space1=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar1 setItems:[NSArray arrayWithObjects:space1,doneBtn1, nil]];
    [_txt_from_date setInputAccessoryView:toolBar1];
    
   
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return AddNew.count;
    
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NewProfileCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    cell = nil;
    if (cell == nil)
    {
        cell=(NewProfileCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    }
    cell.img_movi_image.clipsToBounds = YES;
    cell.img_movi_image.layer.masksToBounds = YES;
    cell.img_movi_image.layer.cornerRadius = 10.0f;
    cell.layer.cornerRadius = 10.0f;
    
    NSLog(@"AddNew %@",AddNew);
    if ([AddNew[indexPath.row] isEqual:@"addNew"]) {
        cell.img_movi_image.image = [UIImage imageNamed:@"add-photo-withplus"];
        cell.btn_close_b.hidden = YES;
        cell.lbl_movie_title.hidden = YES;
        cell.btn_play_b.hidden=YES;
    }
    else
    {
        cell.btn_close_b.hidden = NO;
        cell.lbl_movie_title.hidden = YES;
        cell.btn_play_b.hidden=YES;
        cell.btn_close_b.tag = indexPath.row;
        [cell.btn_close_b addTarget:self action:@selector(masterAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.img_movi_image.image = AddNew[indexPath.row];
        //[cell.img_movi_image sd_setImageWithURL:AddNew[indexPath.row] placeholderImage:[UIImage imageNamed:@"Avatar"]];
        
        
        //cell.Img_Photos.image=[UIImage imageNamed:[AddNew objectAtIndex:indexPath.row]];
    }
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    if ([AddNew[indexPath.row] isEqual:@"addNew"]) {
        ci= [[UIImagePickerController alloc] init];
        ci.delegate = self;
        ci.allowsEditing=NO;
        ci.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        
        if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
            [self presentViewController:ci animated:YES completion:nil];
        else
        {
            popover=[[UIPopoverController alloc]initWithContentViewController:ci];
            [popover presentPopoverFromRect:_img_images.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
    }
    
    
    
}

-(void)masterAction:(UIButton*)sender
{
    UIAlertController * alert=[UIAlertController
                               
                               alertControllerWithTitle:@"" message:@"Are you sure you want to delete"preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Yes"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    //What we write here????????**
                                    NSLog(@"YES");
                                    
                                    [AddNew removeObjectAtIndex:sender.tag];
                                    
                                    [_colle_edit_event reloadData];
                                    
                                }];
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"No"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   //What we write here????????**
                                   NSLog(@"NO");
                                   
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)ShowSelectedDate
{   NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"MM-dd-YYYY hh:mm a"];
    _txt_date.text=[NSString stringWithFormat:@"%@",[formatter stringFromDate:datePicker.date]];
    [_txt_date resignFirstResponder];
    
    
    NSString *strdt=[[NSString alloc]init];
    strdt=_txt_date.text;
    NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
    [formatedt setDateFormat:@"MM-dd-yyyy hh:mm a"];
    NSDate *fulldate=[formatedt dateFromString:strdt];
    [formatedt setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *finaldt=[formatedt stringFromDate:fulldate];
    start_dt=[NSString stringWithFormat:@"%@",finaldt];
    
}
-(void)ShowSelectedDate1
{   NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"MM-dd-YYYY hh:mm a"];
    _txt_from_date.text=[NSString stringWithFormat:@"%@",[formatter stringFromDate:datePicker.date]];
    [_txt_from_date resignFirstResponder];
    
    NSString *strdt=[[NSString alloc]init];
    strdt=_txt_from_date.text;
    NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
    [formatedt setDateFormat:@"MM-dd-yyyy hh:mm a"];
    NSDate *fulldate=[formatedt dateFromString:strdt];
    [formatedt setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *finaldt=[formatedt stringFromDate:fulldate];
    end_dt=[NSString stringWithFormat:@"%@",finaldt];
    
    
    
    
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    if (IsIphone7 || IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = 145;
    }
    else{
        self.revealViewController.rearViewRevealWidth = 145;
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}
#pragma mark - GetAllPosts


-(void)CallSaveEvent
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[_mutDict valueForKey:@"EventID"] forKey:@"event_id"];
    [AddPost setValue:_txt_skv.text forKey:@"title"];
    [AddPost setValue:_txt_disc.text forKey:@"address"];
    [AddPost setValue:countryid forKey:@"country"];
    [AddPost setValue:stateid forKey:@"state"];
    [AddPost setValue:cityid forKey:@"city"];
    [AddPost setValue:start_dt forKey:@"start_date"];
    [AddPost setValue:end_dt forKey:@"end_date"];
    [AddPost setValue:_txt_new_disc.text forKey:@"description"];
    
    
    // NSLog(@"myRequestString:%@",strURL);
    // Create Data from request
    NSString *url = [NSString stringWithFormat:@"%@edit_event",JsonUrlConstant];
    NSMutableURLRequest *urequest = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString:url]];
    
    NSLog(@"myRequestString:%@",urequest);
    
    [urequest setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [urequest setHTTPShouldHandleCookies:NO];
    [urequest setTimeoutInterval:60];
    [urequest setHTTPMethod:@"POST"];
    
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [urequest setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in AddPost) {
        NSLog(@"Param:%@",param);
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [AddPost objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // Add __VIEWSTATE
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"__VIEWSTATE\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"/wEPDwUKLTQwMjY2MDA0M2RkXtxyHItfb0ALigfUBOEHb/mYssynfUoTDJNZt/K8pDs=" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"Body:%@",body);
    
    // add image data profileImage
    
    for (int i = 0; i < AddNew.count-1; i++) {
        
        NSData* imgData = [[NSData alloc] init];
        UIImage *eachImage  = [AddNew objectAtIndex:i+1];
        
        
        imgData = UIImageJPEGRepresentation(eachImage, 0.5);
        
        
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"event_image[%@]\"; filename=\"IMG_%@_%f.jpg\"\r\n",[NSString stringWithFormat:@"%d",i],[NSString stringWithFormat:@"%d",i],[[NSDate date] timeIntervalSince1970]] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imgData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        
    }
    
    
    ///....................remove
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [urequest setHTTPBody:body];
    //return and test
    NSHTTPURLResponse *response = nil;
    NSData *returnData = [NSURLConnection sendSynchronousRequest:urequest returningResponse:&response error:nil];
    //remove..
    NSLog(@"jsourequestn :%@",urequest);
    
    NSError* error;
    // NSLog(@"return data..%@",returnData);
    if(returnData != nil)
    {
        
        NSLog(@"jsourequestn :%@",returnData);
        NSLog(@"response: %@", [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding]);
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:returnData
                                                             options:kNilOptions
                                                               error:&error];
        
        NSLog(@"json :%@",json);
        
        
        
        NSLog(@"json :%@",json);
        
        if([json valueForKey:@"status"])
        {
            UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Event  added." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [Notpermitted show];
            
            
            [self.navigationController popViewControllerAnimated:YES];
            // [self saveSignUpData:json];
            // return json;
        }else{
            UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [Notpermitted show];
            
            
            // return json;
        }
        
    }
    
}

-(void)CallSaveEdit
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[_mutDict valueForKey:@"EventID"] forKey:@"event_id"];
    [AddPost setValue:_txt_skv.text forKey:@"title"];
    [AddPost setValue:_txt_disc.text forKey:@"address"];
    [AddPost setValue:countryid forKey:@"country"];
    [AddPost setValue:stateid forKey:@"state"];
    [AddPost setValue:cityid forKey:@"city"];
    [AddPost setValue:start_dt forKey:@"start_date"];
    [AddPost setValue:end_dt forKey:@"end_date"];
    [AddPost setValue:_txt_new_disc.text forKey:@"description"];
    
    if (ISimage>0) {
        //Create manager
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
        //Now post
        NSLog(@"AddPost %@",AddPost);
        NSString *url = [NSString stringWithFormat:@"%@edit_event",JsonUrlConstant];
        [manager POST:url parameters:[AddPost copy] constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            //add img data one by one
            
            NSData *imageData = UIImageJPEGRepresentation(ISimage,0.5);
            [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"event_image"] fileName:[NSString stringWithFormat:@"user_image.jpg"] mimeType:@"image/jpeg"];
            
        } success:^(AFHTTPRequestOperation *operation, id dicsResponse) {
            // [(AppDelegate *)[[UIApplication sharedApplication] delegate] hideProgress];
            NSLog(@"Success: %@", dicsResponse);
            NSLog(@"Edit profile---%@",dicsResponse);
            NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            [APP_DELEGATE hideLoadingView];
            if ([[dicsResponse valueForKey:@"status"] integerValue] == 0) {
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                //[alert show];
                
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                //[alert show];
                [self.navigationController popViewControllerAnimated:YES];
                
            }
            
        }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  NSLog(@"Error: %@", error);
                  [APP_DELEGATE hideLoadingView];
              }];
    }
    else
    {
        [AddPost setObject:@"" forKey:@"event_image"];
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
            httpeditevent  = [[HttpWrapper alloc] init];
            httpeditevent.delegate=self;
            httpeditevent.getbool=NO;
            [httpeditevent requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@edit_event",JsonUrlConstant] param:[AddPost copy]];
        });
        
    }
    
    
}
-(void)CallCountry
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    
    NSLog(@" url is %@login",JsonUrlConstant);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpcountry  = [[HttpWrapper alloc] init];
        httpcountry.delegate=self;
        httpcountry.getbool=NO;
        [httpcountry requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_country",JsonUrlConstant] param:nil];
        
        
        
        
    });
}
-(void)Callstate:(NSString *)str
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue:str forKey:@"country_id"];
    NSLog(@"%@",AddPost);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpstate  = [[HttpWrapper alloc] init];
        httpstate.delegate=self;
        httpstate.getbool=NO;
        [httpstate requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_state"
                                                  ,JsonUrlConstant] param:[AddPost copy]];
    });
    
}
-(void)CallCity:(NSString *)str
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue:str forKey:@"state_id"];
    NSLog(@"%@",AddPost);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpcity  = [[HttpWrapper alloc] init];
        httpcity.delegate=self;
        httpcity.getbool=NO;
        [httpcity requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_city"
                                                 ,JsonUrlConstant] param:[AddPost copy]];
    });
    
}


#pragma mark- Wrapper Class Delegate Methods
//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary* )dicsResponse
{
    [APP_DELEGATE hideLoadingView];
    if(wrapper == httpeditevent && httpeditevent != nil)
    {
        NSLog(@"httpeditevent ---%@",dicsResponse);
        NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
        if ([[dicsResponse valueForKey:@"status"] integerValue] == 0) {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //[alert show];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //[alert show];
            // [self.navigationController popViewControllerAnimated:YES];
            
        }
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else if(wrapper == httpcountry && httpcountry != nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        dict=[dicsResponse valueForKey:@"data"];
        country= [dicsResponse valueForKey:@"data"];
        NSLog(@"%@",country);
        
        //------------
        
        if (Default==0) {
            NSString *stringToSearch = [_mutDict valueForKey:@"CountryID"];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
            
            NSArray *results = [country filteredArrayUsingPredicate:predicate];
            NSLog(@"%@",[results valueForKey:@"LocationName"]);
            txt_country.text = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationName"] objectAtIndex:0]];
            countryid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
            
            [self Callstate:countryid];
        }
        
        
        
        
    }
    else if (wrapper==httpstate && httpstate !=nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        if ([dicsResponse valueForKey:@"status"]!=0) {
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[dicsResponse valueForKey:@"data"];
            state= [dicsResponse valueForKey:@"data"];
            NSLog(@"%@",state);
        }
        
        //////-------------------
        
        if (Default==0) {
            NSString *stringToSearch = [_mutDict valueForKey:@"StateID"];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
            
            NSArray *results = [state filteredArrayUsingPredicate:predicate];
            NSLog(@"%@",[results valueForKey:@"LocationName"]);
            txt_state.text = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationName"] objectAtIndex:0]];
            stateid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
            
            [self CallCity:stateid];
        }
        
        
        
        
        
    }
    else if (wrapper==httpcity && httpcity !=nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        if ([dicsResponse valueForKey:@"status"]!=0) {
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[dicsResponse valueForKey:@"data"];
            arrcity= [dicsResponse valueForKey:@"data"];
            NSLog(@"%@",arrcity);
        }
        
        /////////////------------------------
        
        if (Default==0) {
            Default=1;
            NSString *stringToSearch = [_mutDict valueForKey:@"CityID"];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
            
            NSArray *results = [arrcity filteredArrayUsingPredicate:predicate];
            NSLog(@"%@",[results valueForKey:@"LocationID"]);
            txt_city.text = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationName"] objectAtIndex:0]];
            cityid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        }
        
        
        
    }
    
    [APP_DELEGATE hideLoadingView];
}


//fetchDataFail
- (void) HttpWrapper:(HttpWrapper* )wrapper fetchDataFail:(NSError* )error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
////TEXTBOX

-(void)CustomizeTextField:(UITextField *)myTextField
{
    myTextField.layer.borderWidth=1.0;
    myTextField.layer.borderColor=[UIColor colorWithHex:0xACACAC].CGColor;
    CGRect frameRect = myTextField.frame;
    frameRect.size.height = 28;
    myTextField.frame = frameRect;
    myTextField.layer.cornerRadius=myTextField.frame.size.height/2;
    
    
    if (myTextField==_txt_date || myTextField==_txt_from_date) {
        [myTextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 14, 20)];
        myTextField.leftView = paddingView;
        myTextField.leftViewMode = UITextFieldViewModeAlways;
    }
    else
    {
        [myTextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
        myTextField.leftView = paddingView;
        myTextField.leftViewMode = UITextFieldViewModeAlways;
    }
    
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if([textView.text isEqual: @"Address"])
    {
        //Address
        
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    else
    {
        //Discription
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    return YES;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    if (AddNew.count <= 0) {
        //Array Allocation
        AddNew = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
    }
    
    [self customSetup];
    
    
    
    
}

#pragma ImagePicker
-(void)singleTapping:(UIGestureRecognizer *)recognizer {
    NSLog(@"image clicked");
    ci= [[UIImagePickerController alloc] init];
    ci.delegate = self;
    ci.allowsEditing = NO;
    ci.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
        [self presentViewController:ci animated:YES completion:nil];
    else
    {
        popover=[[UIPopoverController alloc]initWithContentViewController:ci];
        [popover presentPopoverFromRect:_img_images.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}

#pragma mark - ImagePickerController Delegate
-(void)imagePickerController:(UIImagePickerController *)pick didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    /* if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
     [pick dismissViewControllerAnimated:YES completion:nil];
     } else {
     [popover dismissPopoverAnimated:YES];
     }
     */
    _img_images.image = [info objectForKey:UIImagePickerControllerEditedImage];
    ISimage=[info objectForKey:UIImagePickerControllerOriginalImage];
    _img_extra.hidden=YES;
    _lbl_extra.hidden=YES;
    
    _imgCropperViewController = [[UzysImageCropperViewController alloc] initWithImage:ISimage andframeSize:ci.view.frame.size andcropSize:CGSizeMake(400, 240)];
    _imgCropperViewController.delegate = self;
    [ci presentViewController:_imgCropperViewController animated:YES completion:nil];
    [_imgCropperViewController release];
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)pick
{
    [pick dismissViewControllerAnimated:YES completion:nil];
}

- (void)imageCropper:(UzysImageCropperViewController *)cropper didFinishCroppingWithImage:(UIImage *)image
{
    _img_images=image;
    
    //_img_chooseimg.image=image;
    _img_extra.hidden=YES;
    //_lbl_extra_lab.hidden=YES;
    
    [AddNew insertObject:image atIndex:[AddNew count]];
    [_colle_edit_event reloadData];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


/*
 - (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
 {
 // Dismiss the image selection, hide the picker and
 
 //show the image view with the picked image
 
 
 UIImage* originalImage = nil;
 originalImage = [info objectForKey:UIImagePickerControllerEditedImage];
 if(originalImage==nil)
 {
 originalImage = [info objectForKey:UIImagePickerControllerOriginalImage];
 }
 if(originalImage==nil)
 {
 originalImage = [info objectForKey:UIImagePickerControllerCropRect];
 }
 [ipc dismissViewControllerAnimated:YES completion:nil];
 [AddNew insertObject:originalImage atIndex:1];
 [_collection_view reloadData];
 }
 
 -(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
 {
 [picker dismissViewControllerAnimated:YES completion:nil];
 }
 -(void)masterAction:(UIButton*)sender
 {
 [AddNew removeObjectAtIndex:sender.tag];
 [_collection_view reloadData];
 }
 */
#pragma DROPDOWN LIST
- (void)touchesBegan:(NSSet* )touches withEvent:(UIEvent* )event {
    [self.view endEditing:YES];
}
- (void)showPopover:(UIView*)btn;
{
    
    if (popoverController == nil)
    {
        PopOverView *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PopOverView"];
        
        settingsViewController.index = currentTextField.tag;
        
        if (isCheck == 0) {
            settingsViewController.marrcountry = country;
        }
        else if (isCheck==1)
        {
            settingsViewController.marrstate = state;
        }
        else if (isCheck==2)
        {
            settingsViewController.marrcity = arrcity;
        }
        else{
            NSLog(@"NO DATA FROM API");
        }
        
        
        if ([settingsViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
            settingsViewController.preferredContentSize = CGSizeMake(200, 170);             // iOS 7
        }
        else {
#pragma clang diagnostic pushc
#pragma GCC diagnostic ignored "-Wdeprecated"
            settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 170);      // iOS < 7
#pragma clang diagnostic pop
        }
        settingsViewController.delegates = self;
        
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        
        settingsViewController.modalInPopover = NO;
        popoverController = [[WYPopoverController alloc] initWithContentViewController:settingsViewController];
        
        popoverController.delegate = self;
        
        popoverController.passthroughViews = @[btn];
        popoverController.popoverLayoutMargins = UIEdgeInsetsMake(0, 10, 10 , 10);
        popoverController.wantsDefaultContentAppearance = YES;
        
        [popoverController presentPopoverFromRect:btn.bounds
                                           inView:btn
                         permittedArrowDirections:WYPopoverArrowDirectionAny
                                         animated:YES
                                          options:WYPopoverAnimationOptionFadeWithScale];
    }
    
}

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    popoverController.delegate = nil;
    popoverController = nil;
}

-(void)dismissPopUpViewController
{
    [popoverController dismissPopoverAnimated:YES];
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)Popvalueselected:(NSString*)strValue{
    NSLog(@"POP:%@",strValue);
    
    currentTextField.text = strValue;
    
    if (isCheck==0) {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [country filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        countryid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        
        txt_state.text=@"";
        txt_city.text=@"";
        
        [self Callstate:countryid];
        
    }
    else if (isCheck==1)
    {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [state filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        stateid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        
        txt_city.text=@"";
        
        [self CallCity:stateid];
    }
    else if (isCheck==2)
    {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [arrcity filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        cityid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        
    }
    else
    {
        currentTextField.text = strValue;
    }
    
    
}

- (IBAction)btn_HOME:(id)sender {
    NewHomeVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:YES];
    
}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}


- (IBAction)btn_SAVEEVENT:(id)sender {
    
    if ((![_txt_skv.text isEqualToString:@""])&&(![_txt_date.text isEqual:@""])&&(![_txt_from_date.text isEqual:@""])&&(![_txt_disc.text isEqual:@""])&&(![_txt_new_disc.text isEqual:@""])&& AddNew.count>1) {
        [self CallSaveEvent];
        //[self CallSaveEdit];
    }
    else
    {
        NSLog(@"Filed Blank not alow");
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please Fill all Filed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        //[alert show];
    }
    
}

- (IBAction)btn_SSATET:(id)sender {
    isCheck=1;
    [txt_state setTag:1];
    currentTextField = txt_state;
    [self showPopover:sender];
}

- (IBAction)btn_SCOUNTRY:(id)sender {
    isCheck=0;
    [txt_country setTag:0];
    currentTextField = txt_country;
    [self showPopover:sender];
}

- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_SCITY:(id)sender {
    isCheck=2;
    [txt_city setTag:2];
    currentTextField = txt_city;
    [self showPopover:sender];
}

- (IBAction)btn_SFORM_DT:(id)sender {
    __block EditEventVC *weakSelf = self;
    dateTimePickerView = [[HcdDateTimePickerView alloc] initWithDatePickerMode:DatePickerDateTimeMode defaultDateTime:[[NSDate alloc]initWithTimeIntervalSinceNow:1000]];
    dateTimePickerView.topViewColor = [UIColor darkGrayColor];
    dateTimePickerView.buttonTitleColor = [UIColor whiteColor];
    [dateTimePickerView setMinYear:2010];
    [dateTimePickerView setMaxYear:2030];
    dateTimePickerView.clickedOkBtn = ^(NSString * datetimeStr){
        NSLog(@"%@", datetimeStr);
        start_dt=datetimeStr;
        //--------DATE CONVERT------------------
        NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
        [formatedt setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *fulldate=[formatedt dateFromString:datetimeStr];
        [formatedt setDateFormat:@"MM-dd-yyyy HH:mm:ss"];
        NSString *finaldt=[formatedt stringFromDate:fulldate];
        
        weakSelf.txt_date.text = finaldt;
        weakSelf.txt_from_date.text=@"";
    };
    
    if (dateTimePickerView) {
        [self.view addSubview:dateTimePickerView];
        [dateTimePickerView showHcdDateTimePicker];
    }
    
    
}

- (IBAction)btn_STO_DT:(id)sender {
    __block EditEventVC *weakSelf = self;
    dateTimePickerView = [[HcdDateTimePickerView alloc] initWithDatePickerMode:DatePickerDateTimeMode defaultDateTime:[[NSDate alloc]initWithTimeIntervalSinceNow:1000]];
    dateTimePickerView.topViewColor = [UIColor darkGrayColor];
    dateTimePickerView.buttonTitleColor = [UIColor whiteColor];
    [dateTimePickerView setMinYear:2010];
    [dateTimePickerView setMaxYear:2030];
    dateTimePickerView.clickedOkBtn = ^(NSString * datetimeStr){
        NSLog(@"%@", datetimeStr);
        end_dt=datetimeStr;
        
        //--------DATE CONVERT------------------
        NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
        [formatedt setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *fulldate=[formatedt dateFromString:datetimeStr];
        [formatedt setDateFormat:@"MM-dd-yyyy HH:mm:ss"];
        NSString *finaldt=[formatedt stringFromDate:fulldate];
        
        //==================COMAPARE DATE================
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *date1 = [dateFormatter dateFromString:start_dt];
        
        NSDateFormatter *dateFormatte = [[NSDateFormatter alloc] init];
        [dateFormatte setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *date2 = [dateFormatte dateFromString:datetimeStr];
        
        switch ([date1 compare:date2]) {
            case NSOrderedAscending:
                //Do your logic when date1 > date2
                NSLog(@"OK");
                weakSelf.txt_from_date.text = finaldt;
                break;
                
            case NSOrderedDescending:
                //Do your logic when date1 < date2
                NSLog(@"NOT OK");
                weakSelf.txt_from_date.text = @"Select Date!";
                break;
                
            case NSOrderedSame:
                //Do your logic when date1 = date2
                NSLog(@"SAME DATE");
                weakSelf.txt_from_date.text = finaldt;
                break;
        }
        
        
        //weakSelf.txt_from_date.text = finaldt;
        
    };
    
    if (dateTimePickerView) {
        [self.view addSubview:dateTimePickerView];
        [dateTimePickerView showHcdDateTimePicker];
    }
    
    
}

- (IBAction)btn_NOTIFY:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}
@end
