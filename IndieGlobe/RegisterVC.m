//
//  RegisterVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/24/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//



#import "RegisterVC.h"
#import "DeshboardVC.h"
#import "UIColor+CL.h"
#import "WYPopoverController.h"
#import "TextFieldValidator.h"
#import "PaymentVC.h"
#import "LoginVC.h"
#import "PlatinumInfoVC.h"
#import "ApplicationConstant.h"
#import "NewHomeVC.h"


#define REGEX_USER_NAME_LIMIT @"^.{3,}$"
#define REGEX_USER_NAME @"[A-Za-z0-9]{3,}"
#define REGEX_EMAIL @"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
#define REGEX_PASSWORD_LIMIT @"^.{6,20}$"
#define REGEX_PASSWORD @"[A-Za-z0-9_@]{6,20}"
#define REGEX_PHONE_DEFAULT @"[0-9]{3}\\-[0-9]{3}\\-[0-9]{4}"
#define REGEX_FNAME @"[A-Za-z]{3,}"

#define kPayPalEnvironment PayPalEnvironmentSandbox


@interface RegisterVC ()<WYPopoverControllerDelegate>{
    WYPopoverController* popoverController;
    
}
@property(nonatomic, strong, readwrite) PayPalConfiguration *payPalConfig;

@end

@implementation RegisterVC


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupAlerts];
    
    self.navigationController.navigationBarHidden=YES;
    
    ButtonFlage=1;
    PackageFlage=1;
    
    // Set up payPalConfig
    _payPalConfig = [[PayPalConfiguration alloc] init];
    _payPalConfig.acceptCreditCards = YES;
    _payPalConfig.merchantName = @"IOS";
    _payPalConfig.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/privacy-full"];
    _payPalConfig.merchantUserAgreementURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/useragreement-full"];
    
    _payPalConfig.languageOrLocale = [NSLocale preferredLanguages][0];
    
    // See PayPalConfiguration.h for details.
    
    _payPalConfig.payPalShippingAddressOption = PayPalShippingAddressOptionPayPal;
    //self.successView.hidden = YES;
    
    // use default environment, should be Production in real life
    self.environment = kPayPalEnvironment;
    
    NSLog(@"PayPal iOS SDK version: %@", [PayPalMobile libraryVersion]);
    
    
    _scrollV.delegate=self;
    self.scrollV.contentSize =CGSizeMake(320, 950);
    
    
    _txt_fname.placeholder=@"FIRST NAME";
    _txt_lname.placeholder=@"LAST NAME";
    _txt_email.placeholder=@"E-MAIL";
    _txt_pass.placeholder=@"PASSWORD";
    _txt_confpass.placeholder=@"CONFIRM PASSWORD";
    _txt_profession.placeholder=@"SELECT PROFESSION";
    txt_country.placeholder=@"SELECT COUNTRY";
    txt_state.placeholder=@"SELECT STATE";
    txt_city.placeholder=@"SELECT CITY";
    
    [self textlayers:_txt_fname];
    [self textlayers:_txt_lname];
    [self textlayers:_txt_email];
    [self textlayers:_txt_pass];
    [self textlayers:_txt_confpass];
    [self textlayers:_txt_profession];
    [self textlayers:txt_country];
    [self textlayers:txt_state];
    [self textlayers:txt_city];
    
    
    CGRect frame= _seg_select_plan.frame;
    [_seg_select_plan setFrame:CGRectMake(frame.origin.x, frame.origin.y, 286, 36)];
    
    _btn_join_now.layer.cornerRadius=20.0;
    _seg_select_plan.layer.borderColor = [UIColor whiteColor].CGColor;
    _seg_select_plan.layer.borderWidth = 2.3;
    _seg_select_plan.layer.cornerRadius = 18.0;
    _seg_select_plan.layer.masksToBounds = YES;
    _seg_select_plan.selectedSegmentIndex = 0;
    [_seg_select_plan addTarget:self action:@selector(valueChangedMethod:) forControlEvents:UIControlEventValueChanged];
    
    //default Button View
    //-------------------------------------------------------------------------------
    //btn_search
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.btn_gold.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(_btn_gold.frame.size.height/2, _btn_gold.frame.size.height/2)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    self.btn_gold.layer.mask = maskLayer;
    
    //-------------------------------------------------------------------------------
    //btn_search
    UIBezierPath *maskPath1 = [UIBezierPath bezierPathWithRoundedRect:self.btn_diamond.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(_btn_diamond.frame.size.height/2, _btn_diamond.frame.size.height/2)];
    
    CAShapeLayer *maskLayer1 = [[CAShapeLayer alloc] init];
    maskLayer1.frame = self.view.bounds;
    maskLayer1.path  = maskPath1.CGPath;
    self.btn_diamond.layer.mask = maskLayer1;
    
    //View
    /*UIBezierPath *maskPath1 = [UIBezierPath bezierPathWithRoundedRect:self.vw_buttons.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(_vw_buttons.frame.size.height/2, _vw_buttons.frame.size.height/2)];
     
     CAShapeLayer *maskLayer1 = [[CAShapeLayer alloc] init];
     maskLayer1.frame = self.view.bounds;
     maskLayer1.path  = maskPath1.CGPath;
     self.vw_buttons.layer.mask = maskLayer1;*/
    _vw_buttons.layer.cornerRadius=_vw_buttons.frame.size.height/2;
    
    //-------------------------------------------------------------------------------
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(monthly:)];
    [_vw_monthy addGestureRecognizer:singleFingerTap];
    
    UITapGestureRecognizer *singleFingerTap1 =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(yearly:)];
    [_vw_yearly addGestureRecognizer:singleFingerTap1];
    
    
    
    //arryList=@[@"ACTOR",@"SINGER",@"DIRECTOR",@"SCREEN WRITER",@"MANAGER",@"M D"];
    
    
    [self CallGetPlan];
    [self CallContry];
    [self CallProfession];
    
    ///////////////------DROPDOWN LIST------------
    /*   appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
     if ([appdelegate.profe isEqualToString:@"(null)"]) {
     
     appdelegate.profe = [NSString stringWithFormat:@""];
     }
     
     
     
     _txt_profession.text = [NSString stringWithFormat:@"%@",appdelegate.profe];
     */
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)valueChangedMethod:(UISegmentedControl *)segmentedControl
{
    //continue your code here...
    
    NSLog(@"Change SEGE  %ld",(long)_seg_select_plan.selectedSegmentIndex);
    if (_seg_select_plan.selectedSegmentIndex==0) {
        _vw_info.hidden=YES;
        _lbl_info.hidden=NO;
    }
    else if (_seg_select_plan.selectedSegmentIndex==1)
    {
        _vw_info.hidden=NO;
        _lbl_info.hidden=YES;
        
    }
    else if (_seg_select_plan.selectedSegmentIndex==2)
    {
        _vw_info.hidden=NO;
        _lbl_info.hidden=YES;
        
    }
    
}

#pragma mark - GetAllPosts
-(void)CallMyMethod
{
    [APP_DELEGATE showLoadingView:@""];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    [AddPost setValue:_txt_fname.text forKey:@"first_name"];
    [AddPost setValue:_txt_lname.text forKey:@"last_name"];
    [AddPost setValue:_txt_email.text forKey:@"email"];
    [AddPost setValue:_txt_pass.text forKey:@"password"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",ProfessionalID] forKey:@"profession"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",countryid] forKey:@"country"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",stateid] forKey:@"state"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",cityid] forKey:@"city"];
    [AddPost setValue:[NSString stringWithFormat:@"%d",ButtonFlage] forKey:@"plan_type"];
    if (PackageFlage==1) {
        [AddPost setValue:@"monthly" forKey:@"plan_terms"];
    }
    else
    {
        [AddPost setValue:@"yearly" forKey:@"plan_terms"];
    }
    NSLog(@"%@",AddPost);
    
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpreg  = [[HttpWrapper alloc] init];
        httpreg.delegate=self;
        httpreg.getbool=NO;
        [httpreg requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@register"
                                                ,JsonUrlConstant] param:AddPost];
    });
    
    
}
-(void)CallGetPlan
{
    [APP_DELEGATE showLoadingView:@""];
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpgetplan  = [[HttpWrapper alloc] init];
        httpgetplan.delegate=self;
        httpgetplan.getbool=NO;
        [httpgetplan requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_plan"
                                                    ,JsonUrlConstant] param:nil];
    });
    
}
-(void)CallProfession
{
    [APP_DELEGATE showLoadingView:@""];
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpprofes  = [[HttpWrapper alloc] init];
        httpprofes.delegate=self;
        httpprofes.getbool=NO;
        [httpprofes requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_profession"
                                                   ,JsonUrlConstant] param:nil];
    });
    
}

-(void)CallContry
{
    [APP_DELEGATE showLoadingView:@""];
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httcontry  = [[HttpWrapper alloc] init];
        httcontry.delegate=self;
        httcontry.getbool=NO;
        [httcontry requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_country"
                                                  ,JsonUrlConstant] param:nil];
    });
    
}
-(void)Callstate:(NSString *)str
{
    [APP_DELEGATE showLoadingView:@""];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue:str forKey:@"country_id"];
    NSLog(@"%@",AddPost);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpstate  = [[HttpWrapper alloc] init];
        httpstate.delegate=self;
        httpstate.getbool=NO;
        [httpstate requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_state"
                                                  ,JsonUrlConstant] param:[AddPost copy]];
    });
    
}

-(void)CallCity:(NSString *)str
{
    [APP_DELEGATE showLoadingView:@""];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue:str forKey:@"state_id"];
    NSLog(@"%@",AddPost);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpcity  = [[HttpWrapper alloc] init];
        httpcity.delegate=self;
        httpcity.getbool=NO;
        [httpcity requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_city"
                                                 ,JsonUrlConstant] param:[AddPost copy]];
    });
    
}

-(void)CallCheckEmail
{
    [APP_DELEGATE showLoadingView:@""];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue:_txt_email.text forKey:@"email"];
    NSLog(@"%@",AddPost);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpemail  = [[HttpWrapper alloc] init];
        httpemail.delegate=self;
        httpemail.getbool=NO;
        [httpemail requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@check_email_avilable"
                                                  ,JsonUrlConstant] param:[AddPost copy]];
    });
    
}

#pragma mark- Wrapper Class Delegate Methods
//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary* )dicsResponse
{
    if(wrapper == httpreg && httpreg != nil)
    {
        NSLog(@"Responce: %@",dicsResponse);
        
        if ([dicsResponse valueForKey:@"status"]!=0) {
            
             UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"REGISTER" message:@"Register successfull Login with register email and password" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
             [alert show];
             [APP_DELEGATE hideLoadingView];
            /* LoginVC* next = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
             [self.navigationController pushViewController:next animated:YES];*/
            NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
            dic=[dicsResponse valueForKey:@"data"];
            
            NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
            [standardUserDefaults setObject:[dic valueForKey:@"UserID"] forKey:@"UserID"];
            [standardUserDefaults setObject:[dic valueForKey:@"Email"] forKey:@"Email"];
            [standardUserDefaults setObject:[dic valueForKey:@"FName"] forKey:@"FName"];
            [standardUserDefaults setObject:[dic valueForKey:@"LName"] forKey:@"LName"];
            [standardUserDefaults setObject:[dic valueForKey:@"PlanID"] forKey:@"PlanID"];
            [standardUserDefaults setObject:[dic valueForKey:@"PlanTerms"] forKey:@"PlanTerms"];
            [standardUserDefaults setObject:[dic valueForKey:@"PhotoURL"] forKey:@"PhotoURL"];
            [standardUserDefaults setObject:[dic valueForKey:@"PlanName"] forKey:@"PlanName"];
            
            [standardUserDefaults setObject:[dic valueForKey:@"CountryID"] forKey:@"CountryID"];
            [standardUserDefaults setObject:[dic valueForKey:@"StateID"] forKey:@"StateID"];
            [standardUserDefaults setObject:[dic valueForKey:@"CityID"] forKey:@"CityID"];
            
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"remember"];
            
            NSMutableDictionary *Dict = [[NSMutableDictionary alloc]init];
            [Dict setValue:[dic valueForKey:@"Email"] forKey:@"username"];
            [Dict setValue:_txt_pass.text forKey:@"password"];
            [[NSUserDefaults standardUserDefaults] setObject:Dict forKey:@"dictionaryKey"];
            
            NewHomeVC* next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
            [self.navigationController pushViewController:next animated:YES];
            
        }
        
        
    }
    else if (wrapper==httcontry && httcontry !=nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        dict=[dicsResponse valueForKey:@"data"];
        country= [dicsResponse valueForKey:@"data"];
        NSLog(@"%@",country);
        [APP_DELEGATE hideLoadingView];
    }
    else if (wrapper==httpstate && httpstate !=nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        if ([dicsResponse valueForKey:@"status"]!=0) {
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[dicsResponse valueForKey:@"data"];
            state= [dicsResponse valueForKey:@"data"];
            NSLog(@"%@",state);
        }
        [APP_DELEGATE hideLoadingView];
    }
    else if (wrapper==httpcity && httpcity !=nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        if ([dicsResponse valueForKey:@"status"]!=0) {
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[dicsResponse valueForKey:@"data"];
            arrcity= [dicsResponse valueForKey:@"data"];
            NSLog(@"%@",arrcity);
        }
        [APP_DELEGATE hideLoadingView];
    }
    else if (wrapper==httpgetplan )
    {
        @try {
            
            
            NSLog(@"Responce : %@",dicsResponse);
            
            NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
            if ([dicsResponse valueForKey:@"status"]!=0) {
                
                NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
                dict=[dicsResponse valueForKey:@"data"];
                arrplan= [dicsResponse valueForKey:@"data"];
                NSLog(@"%@",arrplan);
                
                [_btn_gold setTitle:[NSString stringWithFormat:@"%@",[[arrplan valueForKey:@"PlanName"] objectAtIndex:0]] forState:UIControlStateNormal];
                [_btn_platinum setTitle:[NSString stringWithFormat:@"%@",[[arrplan valueForKey:@"PlanName"] objectAtIndex:1]] forState:UIControlStateNormal];
                [_btn_diamond setTitle:[NSString stringWithFormat:@"%@",[[arrplan valueForKey:@"PlanName"] objectAtIndex:2]] forState:UIControlStateNormal];
            }
            [APP_DELEGATE hideLoadingView];
        } @catch (NSException *e) {
            NSLog(@" ERROR %@",e);
        }
    }
    else if (wrapper==httpprofes && httpprofes !=nil)
    {
        @try {
            
            NSLog(@"Responce : %@",dicsResponse);
            
            NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
            if ([dicsResponse valueForKey:@"status"]!=0) {
                
                NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
                dict=[dicsResponse valueForKey:@"data"];
                arryList= [dicsResponse valueForKey:@"data"];
                NSLog(@"%@",arryList);
                
            }
            [APP_DELEGATE hideLoadingView];
        } @catch (NSException *e) {
            NSLog(@" ERROR %@",e);
        }
        
    }
    else if (wrapper==httpemail && httpemail !=nil)
    {
       NSLog(@"Responce : %@",dicsResponse);
        
        //status
        
        if ([[dicsResponse valueForKey:@"status"] intValue]==1) {
            [self CallRegisterButtonClick];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Email Already Exist, Try Another One." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
    }
    [APP_DELEGATE hideLoadingView];
    
}

//fetchDataFail
- (void) HttpWrapper:(HttpWrapper* )wrapper fetchDataFail:(NSError* )error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        //Code for OK button
        LoginVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        [self.navigationController pushViewController:next animated:YES];
    }
    
}


-(void)setupAlerts{
    
    [_txt_fname addRegx:REGEX_FNAME withMsg:@"Only allow A-Z char "];
    
    [_txt_lname addRegx:REGEX_FNAME withMsg:@"Only allow A-Z char"];
    
    [_txt_email addRegx:REGEX_EMAIL withMsg:@"Enter valid email."];
    
    [_txt_pass addRegx:REGEX_PASSWORD_LIMIT withMsg:@"Password characters limit should be come between 6-20"];
    [_txt_pass addRegx:REGEX_PASSWORD withMsg:@"Password must contain alpha numeric characters."];
    
    [_txt_confpass addConfirmValidationTo:_txt_pass withMsg:@"Confirm password didn't match."];
    
}


- (void)monthly:(UITapGestureRecognizer *)recognizer
{
    PackageFlage=1;
    NSData *imgData1 = UIImagePNGRepresentation([UIImage imageNamed:@"radiobutton-off.png"]);
    NSData *imgData2 = UIImagePNGRepresentation(_img_monthly.image);
    BOOL isCompare =  [imgData1 isEqualToData:imgData2];
    if(isCompare)
    {
        NSLog(@"Image View contains image.png");
        _img_monthly.image=[UIImage imageNamed:@"radiobutton-on.png"];
        _img_yearly.image=[UIImage imageNamed:@"radiobutton-off.png"];
    }
}

- (void)yearly:(UITapGestureRecognizer *)recognizer
{
    PackageFlage=2;
    NSData *imgData1 = UIImagePNGRepresentation([UIImage imageNamed:@"radiobutton-off.png"]);
    NSData *imgData2 = UIImagePNGRepresentation(_img_yearly.image);
    BOOL isCompare =  [imgData1 isEqualToData:imgData2];
    if(isCompare)
    {
        NSLog(@"Image View contains image.png");
        _img_yearly.image=[UIImage imageNamed:@"radiobutton-on.png"];
        _img_monthly.image=[UIImage imageNamed:@"radiobutton-off.png"];
    }
}
-(void)textlayers :(UITextField *)UITextField
{
    UITextField.layer.borderColor=[[UIColor clearColor]CGColor];
    UITextField.layer.borderWidth=1.5;
    UITextField.layer.borderColor=[[UIColor whiteColor] CGColor];
    UITextField.layer.cornerRadius=15.0;
    [UITextField setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
    UITextField.leftView = paddingView;
    UITextField.leftViewMode = UITextFieldViewModeAlways;
    
}
//-----------
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}




- (IBAction)btn_LOGIN:(id)sender {
    // [self.navigationController popViewControllerAnimated:YES];
    LoginVC* next = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    [self.navigationController pushViewController:next animated:YES];
}



- (IBAction)btn_JOIN_NOW:(id)sender {
    
   
        if([_txt_fname validate] && [_txt_lname validate] && [_txt_pass validate] && [_txt_confpass validate] && [_txt_email validate] && [self txtFielfValidation:_txt_profession :@"Please Select Profession"] && [self txtFielfValidation:txt_country :@"Please Select Country"] && [self txtFielfValidation:txt_state :@"Please Select State"]&& [self txtFielfValidation:txt_city :@"Please Select City"]){
            
            //[self CallMyMethod];
            [self CallCheckEmail];
            
        }
    
    
    
    
    
}

-(void)CallRegisterButtonClick
{
  
        if (ButtonFlage==1) {
            if([_txt_fname validate] && [_txt_lname validate] && [_txt_pass validate] && [_txt_confpass validate] && [_txt_email validate] && [self txtFielfValidation:_txt_profession :@"Please Select Profession"] && [self txtFielfValidation:txt_country :@"Please Select Country"] && [self txtFielfValidation:txt_state :@"Please Select State"]&& [self txtFielfValidation:txt_city :@"Please Select City"]){
                
                [self CallMyMethod];
                
            }
        }
        
        
        
        
        NSString *amt=[[NSString alloc]init];
        if (ButtonFlage==2) {
            
            if([_txt_fname validate] && [_txt_lname validate] && [_txt_pass validate] && [_txt_confpass validate] && [_txt_email validate] && [self txtFielfValidation:_txt_profession :@"Please Select Profession"] && [self txtFielfValidation:txt_country :@"Please Select Country"] && [self txtFielfValidation:txt_state :@"Please Select State"]&& [self txtFielfValidation:txt_city :@"Please Select City"]){
                
                if (PackageFlage==1) {
                    amt=[[arrplan valueForKey:@"MonthlyFee"] objectAtIndex:1];
                }
                else
                {
                    amt=[[arrplan valueForKey:@"yearlyFees"] objectAtIndex:1];
                }
                
                PayPalItem *item1 = [PayPalItem itemWithName:[NSString stringWithFormat:@"%@",[[arrplan valueForKey:@"PlanName"]objectAtIndex:1 ]]
                                                withQuantity:1
                                                   withPrice:[NSDecimalNumber decimalNumberWithString:amt]
                                                withCurrency:@"USD"
                                                     withSku:@"SK-Platinum"];
                
                
                NSArray*items=@[item1];
                NSDecimalNumber *subtotal = [PayPalItem totalPriceForItems:items];
                
                // Optional: include payment details
                NSDecimalNumber *shipping = [[NSDecimalNumber alloc] initWithString:@"0.00"];
                NSDecimalNumber *tax = [[NSDecimalNumber alloc] initWithString:@"0.00"];
                PayPalPaymentDetails *paymentDetails = [PayPalPaymentDetails paymentDetailsWithSubtotal:subtotal
                                                                                           withShipping:shipping
                                                                                                withTax:tax];
                NSDecimalNumber *total = [[subtotal decimalNumberByAdding:shipping] decimalNumberByAdding:tax];
                
                PayPalPayment *payment = [[PayPalPayment alloc] init];
                payment.amount = total;
                payment.currencyCode = @"USD";
                payment.shortDescription = [NSString stringWithFormat:@"%@",[[arrplan valueForKey:@"PlanName"] objectAtIndex:1]];
                payment.items = items;  // if not including multiple items, then leave payment.items as nil
                payment.paymentDetails = paymentDetails;
                
                if (!payment.processable) {
                    // This particular payment will always be processable. If, for
                    // example, the amount was negative or the shortDescription was
                    // empty, this payment wouldn't be processable, and you'd want
                    // to handle that here.
                }
                
                PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                                                            configuration:self.payPalConfig
                                                                                                                 delegate:self];
                [self presentViewController:paymentViewController animated:YES completion:nil];
                
            }
        }
        
        
        if (ButtonFlage==3) {
            
            if([_txt_fname validate] && [_txt_lname validate] && [_txt_pass validate] && [_txt_confpass validate] && [_txt_email validate] && [self txtFielfValidation:_txt_profession :@"Please Select Profession"] && [self txtFielfValidation:txt_country :@"Please Select Country"] && [self txtFielfValidation:txt_state :@"Please Select State"]&& [self txtFielfValidation:txt_city :@"Please Select City"]){
                
                if (PackageFlage==1) {
                    amt=[[arrplan valueForKey:@"MonthlyFee"] objectAtIndex:2];
                }
                else
                {
                    amt=[[arrplan valueForKey:@"yearlyFees"] objectAtIndex:2];
                }
                
                PayPalItem *item1 = [PayPalItem itemWithName:[NSString stringWithFormat:@"%@",[[arrplan valueForKey:@"PlanName"]objectAtIndex:2 ]]
                                                withQuantity:1
                                                   withPrice:[NSDecimalNumber decimalNumberWithString:amt]
                                                withCurrency:@"USD"
                                                     withSku:@"SK-Platinum"];
                
                
                NSArray*items=@[item1];
                NSDecimalNumber *subtotal = [PayPalItem totalPriceForItems:items];
                
                // Optional: include payment details
                NSDecimalNumber *shipping = [[NSDecimalNumber alloc] initWithString:@"0.00"];
                NSDecimalNumber *tax = [[NSDecimalNumber alloc] initWithString:@"0.00"];
                PayPalPaymentDetails *paymentDetails = [PayPalPaymentDetails paymentDetailsWithSubtotal:subtotal
                                                                                           withShipping:shipping
                                                                                                withTax:tax];
                NSDecimalNumber *total = [[subtotal decimalNumberByAdding:shipping] decimalNumberByAdding:tax];
                
                PayPalPayment *payment = [[PayPalPayment alloc] init];
                payment.amount = total;
                payment.currencyCode = @"USD";
                payment.shortDescription = [NSString stringWithFormat:@"%@",[[arrplan valueForKey:@"PlanName"] objectAtIndex:2]];
                payment.items = items;  // if not including multiple items, then leave payment.items as nil
                payment.paymentDetails = paymentDetails;
                
                if (!payment.processable) {
                    // This particular payment will always be processable. If, for
                    // example, the amount was negative or the shortDescription was
                    // empty, this payment wouldn't be processable, and you'd want
                    // to handle that here.
                }
                
                PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                                                            configuration:self.payPalConfig
                                                                                                                 delegate:self];
                [self presentViewController:paymentViewController animated:YES completion:nil];
                
            }
        }
    
}



-(BOOL)txtFielfValidation :(UITextField*)txtF :(NSString*)msg
{
    if ([txtF.text isEqualToString:@""] ||!([txtF.text length]>0)) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    else
    {
        return YES;
    }
    
}


#pragma  PAYPAL PAYMENT METHODS
- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment {
    NSLog(@"PayPal Payment Success!");
    self.resultText = [completedPayment description];
    
    [self CallMyMethod];
   
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    NSLog(@"PayPal Payment Canceled");
    self.resultText = nil;
    //self.successView.hidden = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)btngold:(id)sender {
    ButtonFlage=1;
    
    [_btn_gold setTitleColor:[UIColor colorWithHex:0x01B5A9] forState:UIControlStateNormal];
    [_btn_gold setBackgroundColor:[UIColor whiteColor]];
    [_btn_platinum setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btn_platinum setBackgroundColor:[UIColor colorWithHex:0x01B5A9]];
    [_btn_diamond setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btn_diamond setBackgroundColor:[UIColor colorWithHex:0x01B5A9]];
    
    _vw_info.hidden=YES;
    
}

- (IBAction)btnplatinum:(id)sender {
    ButtonFlage=2;
    
    [_btn_gold setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btn_gold setBackgroundColor:[UIColor colorWithHex:0x01B5A9]];
    [_btn_platinum setTitleColor:[UIColor colorWithHex:0x01B5A9] forState:UIControlStateNormal];
    [_btn_platinum setBackgroundColor:[UIColor whiteColor]];
    [_btn_diamond setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btn_diamond setBackgroundColor:[UIColor colorWithHex:0x01B5A9]];
    
    _vw_info.hidden=NO;
    
    _lbl_mfee.text=[NSString stringWithFormat:@"$%@",[[arrplan valueForKey:@"MonthlyFee"] objectAtIndex:1]];
    _lbl_yfee.text=[NSString stringWithFormat:@"$%@",[[arrplan valueForKey:@"yearlyFees"] objectAtIndex:1]];
    
    
}

- (IBAction)btndiamond:(id)sender {
    ButtonFlage=3;
    
    [_btn_gold setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btn_gold setBackgroundColor:[UIColor colorWithHex:0x01B5A9]];
    [_btn_platinum setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btn_platinum setBackgroundColor:[UIColor colorWithHex:0x01B5A9]];
    [_btn_diamond setTitleColor:[UIColor colorWithHex:0x01B5A9] forState:UIControlStateNormal];
    [_btn_diamond setBackgroundColor:[UIColor whiteColor]];
    
    _vw_info.hidden=NO;
    
    _lbl_mfee.text=[NSString stringWithFormat:@"$%@",[[arrplan valueForKey:@"MonthlyFee"] objectAtIndex:2]];
    _lbl_yfee.text=[NSString stringWithFormat:@"$%@",[[arrplan valueForKey:@"yearlyFees"] objectAtIndex:2]];
}


- (IBAction)btn_SPROFESSION:(id)sender {
  
    [self.view endEditing:YES];
    
    NSMutableArray *arrteam=[[NSMutableArray alloc] init];
    
    for (int i=0; i<arryList.count; i++) {
        //arrteam=[[arryList objectAtIndex:i] valueForKey:@"Profession"];
        [arrteam addObject:[[arryList objectAtIndex:i] valueForKey:@"Profession"]];
    }
    
    [Dropobj fadeOut];
    [self showPopUpWithTitle:@"Select Profession" withOption:arrteam xy:CGPointMake(35, 58) size:CGSizeMake(300, 330) isMultiple:YES];
    
    
}

- (IBAction)btn_SCOUNTRY:(id)sender {
    [self.view endEditing:YES];
    isCheck=0;
    [txt_country setTag:0];
    currentTextField = txt_country;
    [self showPopover:sender];
}

- (IBAction)btn_SSTATE:(id)sender {
    [self.view endEditing:YES];
    isCheck=1;
    [txt_state setTag:1];
    currentTextField = txt_state;
    [self showPopover:sender];
}

- (IBAction)btn_SCITY:(id)sender {
    [self.view endEditing:YES];
    isCheck=2;
    [txt_city setTag:2];
    currentTextField = txt_city;
    [self showPopover:sender];
    
}

- (IBAction)btn_PLANINFO:(id)sender {
    PlatinumInfoVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"PlatinumInfoVC"];
    next.ButtonFlag=ButtonFlage;
    next.PackageFlag=PackageFlage;
    next.arrplan=arrplan;
    
    [self.navigationController pushViewController:next animated:YES];
}

- (void)showPopover:(UIView*)btn;
{
    
    if (popoverController == nil)
    {
        PopOverView *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PopOverView"];
        
        settingsViewController.index = currentTextField.tag;
        
        if (isCheck == 0) {
            settingsViewController.marrcountry = country;
        }
        else if (isCheck==1)
        {
            settingsViewController.marrstate = state;
        }
        else if (isCheck==2)
        {
            settingsViewController.marrcity = arrcity;
        }
        else{
            NSLog(@"NO DATA FROM API");
        }
        
        
        
        if ([settingsViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
            settingsViewController.preferredContentSize = CGSizeMake(200, 170);             // iOS 7
        }
        else {
#pragma clang diagnostic pushc
#pragma GCC diagnostic ignored "-Wdeprecated"
            settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 170);      // iOS < 7
#pragma clang diagnostic pop
        }
        settingsViewController.delegates = self;
        
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        
        settingsViewController.modalInPopover = NO;
        popoverController = [[WYPopoverController alloc] initWithContentViewController:settingsViewController];
        
        popoverController.delegate = self;
        
        popoverController.passthroughViews = @[btn];
        popoverController.popoverLayoutMargins = UIEdgeInsetsMake(0, 10, 10 , 10);
        popoverController.wantsDefaultContentAppearance = YES;
        
        [popoverController presentPopoverFromRect:btn.bounds
                                           inView:btn
                         permittedArrowDirections:WYPopoverArrowDirectionAny
                                         animated:YES
                                          options:WYPopoverAnimationOptionFadeWithScale];
    }
    
}

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    popoverController.delegate = nil;
    popoverController = nil;
}

-(void)dismissPopUpViewController
{
    [popoverController dismissPopoverAnimated:YES];
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)Popvalueselected:(NSString*)strValue{
    NSLog(@"POP:%@",strValue);
    
    currentTextField.text = strValue;
    if (isCheck==0) {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [country filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        countryid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        
        txt_state.text=@"";
        txt_city.text=@"";
        
        [self Callstate:countryid];
        
    }
    else if (isCheck==1)
    {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [state filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        stateid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        
        txt_city.text=@"";
        [self CallCity:stateid];
    }
    else if (isCheck==2)
    {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [arrcity filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        cityid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        
    }
    else
    {
        currentTextField.text = strValue;
    }
    
    
    
}
// MULTISELECTION DROPDOWN
-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSMutableArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple{
    
    
    
    Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
    Dropobj.delegate = self;
    [Dropobj showInView:self.view animated:YES];
    
    /*----------------Set DropDown backGroundColor-----------------*/
    [Dropobj SetBackGroundDropDown_R:1.0 G:181.0 B:169.0 alpha:1.00];
    
}
- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
    /*----------------Get Selected Value[Single selection]-----------------*/
    for (int i=0; i<arryList.count; i++) {
        _txt_profession.text=[[arryList objectAtIndex:i] valueForKey:@"Profession"];
    }
    _txt_profession.text=[[arryList objectAtIndex:anIndex] valueForKey:@"Profession"];
}
- (void)DropDownListView:(DropDownListView *)dropdownListView Datalist:(NSMutableArray*)ArryData{
    
    /*----------------Get Selected Value[Multiple selection]-----------------*/
    if (ArryData.count>0) {
        _txt_profession.text=[ArryData componentsJoinedByString:@", "];
        NSMutableArray *NewArr = [[NSMutableArray alloc] init];
        NSMutableArray *myarr = [[NSMutableArray alloc] init];
        
        for (int i=0; i<arryList.count; i++) {
            [NewArr addObject:[[arryList objectAtIndex:i] valueForKey:@"Profession"]];
        }
        
        for (int i=0; i<ArryData.count; i++) {
            NSUInteger index = [NewArr indexOfObject:ArryData[i]];
            NSLog(@"index %lu",(unsigned long)index);
            [myarr addObject:[[arryList objectAtIndex:index] valueForKey:@"ProfessionalID"]];
        }
        NSLog(@"my arr %@",myarr);
        
        ProfessionalID = [myarr componentsJoinedByString:@","];
        
        NSLog(@"ProfessionalID %@",ProfessionalID);
        //CGSize size=[self GetHeightDyanamic:_txt_profession];
        //_txt_profession.frame=CGRectMake(16, 240, 287, size.height);
    }
    else{
        _txt_profession.text=@"";
    }
    
}
- (void)DropDownListViewDidCancel{
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    
    if ([touch.view isKindOfClass:[UIView class]]) {
        [Dropobj fadeOut];
    }
}


-(CGSize)GetHeightDyanamic:(UILabel*)lbl
{
    NSRange range = NSMakeRange(0, [lbl.text length]);
    CGSize constraint;
    constraint= CGSizeMake(288 ,MAXFLOAT);
    CGSize size;
    
    if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)) {
        NSDictionary *attributes = [lbl.attributedText attributesAtIndex:0 effectiveRange:&range];
        CGSize boundingBox = [lbl.text boundingRectWithSize:constraint options: NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
        
        size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    }
    else{
        
        
        size = [lbl.text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    }
    return size;
}




@end
