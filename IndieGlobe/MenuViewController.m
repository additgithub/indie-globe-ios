//
//  MenuViewController.m
//  RevealControllerStoryboardExample
//
//  Created by Nick Hodapp on 1/9/13.
//  Copyright (c) 2013 CoDeveloper. All rights reserved.
//
#import "MenuViewController.h"
#import "SWRevealViewController.h"
//#import "ColorViewController.h"
#import "DeshboardVC.h"
#import "NewsVC.h"
#import "ProfileVC.h"
#import "CritiquesVC.h"
#import "PackageList.h"
#import "LearningVC.h"
#import "EventVC.h"
#import "TeamBuildVC.h"
#import "UIViewController+ENPopUp.h"
#import "MyEventVC.h"
#import "SettingVC.h"
#import "NewProfileVC.h"
#import "MessagesVC.h"
#import "MesCell.h"
#import "MyFundingVC.h"
#import "MySuggetionVC.h"




@implementation SWUITableViewCell
@end

@implementation MenuViewController

MesCell *cell;

- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    
    NSLog(@"CLICK MENU:::");
    
}

-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"Side Menu Open!!");
        //cell = [self.tableView dequeueReusableCellWithIdentifier: @"mymsg"];
        NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
        //cell.txt_msg_count.text=[standardUserDefaults valueForKey:@"msg_req_count"];
    
    [self.tableView reloadData];
    //cell.txt_msg_count.text=[standardUserDefaults valueForKey:@"msg_req_count"];
    NSLog(@"count--- %@",[standardUserDefaults valueForKey:@"msg_req_count"]);
    
}


#pragma mark - Table view data source
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
   
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    self.tableView.separatorColor = [UIColor clearColor];


    switch ( indexPath.row )
    {
        case 0:
            CellIdentifier = @"profile";
            break;

        case 1:
            CellIdentifier = @"myevent";
            break;
        case 2:
            CellIdentifier = @"mycrit";
            break;

        case 3:
            CellIdentifier = @"myfunding";
            break;
            
        case 4:
            CellIdentifier = @"myteam";
            break;

        case 5:
        {
            CellIdentifier = @"mymsg";
            cell = [self.tableView dequeueReusableCellWithIdentifier: @"mymsg"];
            NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
            if ([[standardUserDefaults valueForKey:@"msg_req_count"] intValue]<=0) {
                cell.txt_msg_count.text=@"0";
                cell.txt_msg_count.hidden=YES;
            }
            else
            {
                cell.txt_msg_count.hidden=YES;
                cell.txt_msg_count.text=[standardUserDefaults valueForKey:@"msg_req_count"];
            }
            
            return cell;
            break;
        }
        case 6:
            CellIdentifier = @"sugg";
            break;
        case 7:
            CellIdentifier = @"appsetting";
            break;
        

    }


    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier forIndexPath: indexPath];
    
   
    
    
 
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
       
    NSLog(@"I'm selected %ld",(long)indexPath.row);
    //self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.separatorColor = [UIColor clearColor];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSInteger row = indexPath.row;
    
    // Grab a handle to the reveal controller, as if you'd do with a navigtion controller via self.navigationController.
    SWRevealViewController *revealController = self.revealViewController;
    UIViewController *newFrontController = nil;
    
    switch (row) {
    case 0:
        {
            
            newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"NewProfileVC"];
            newFrontController.hidesBottomBarWhenPushed = YES;
            break;
        }
        case 1:
        {
            newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"MyEventVC"];
            newFrontController.hidesBottomBarWhenPushed = YES;
            break;
        }
        case 2:
        {
            CritiquesVC *critic =[self.storyboard instantiateViewControllerWithIdentifier:@"CritiquesVC"];
            critic.fromMyCriti = @"1";
            newFrontController = critic;
            newFrontController.hidesBottomBarWhenPushed = YES;
            
            break;
            
        }
        case 3:
        {
            newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"MyFundingVC"];
            newFrontController.hidesBottomBarWhenPushed = YES;
            break;
            
        }
        case 4:
        {
            newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"TeamBuildVC"];
            newFrontController.hidesBottomBarWhenPushed = YES;
            break;
            
        }
        case 5:
        {
            newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"MessagesVC"];
            newFrontController.hidesBottomBarWhenPushed = YES;
            break;
            
        }

        case 6:
        {
            newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"MySuggetionVC"];
            newFrontController.hidesBottomBarWhenPushed = YES;
            break;
            
            
            
        }
        case 7:
        {
            newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingVC"];
            newFrontController.hidesBottomBarWhenPushed = YES;
            break;
            
        }
        
        default:
        {
            newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsVC"];
            newFrontController.hidesBottomBarWhenPushed = YES;
            break;
        }
       
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newFrontController];

    [revealController pushFrontViewController:navigationController animated:YES];
    
}

#pragma mark state preservation / restoration
- (void)encodeRestorableStateWithCoder:(NSCoder *)coder {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // TODO save what you need here
    
    [super encodeRestorableStateWithCoder:coder];
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // TODO restore what you need here
    
    [super decodeRestorableStateWithCoder:coder];
}

- (void)applicationFinishedRestoringState {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // TODO call whatever function you need to visually restore
}

- (IBAction)btn_POP_PLAN:(id)sender {
    
    UIViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PackageList"];
    vc.view.frame = CGRectMake(0, 0, 233.0f, 236.0f);
    [self presentPopUpViewController:vc];

    
}
@end
