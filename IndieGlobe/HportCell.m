//
//  HportCell.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 3/26/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "HportCell.h"

@implementation HportCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _img_prof.clipsToBounds=YES;
    _img_prof.layer.cornerRadius=_img_prof.frame.size.height/2;
    
    [self.shadow_vw setShadow];
    
    _img_vw1.layer.cornerRadius=_img_vw1.layer.frame.size.height/2;
    _img_vw1.clipsToBounds=YES;
    
    _img_vw2.layer.cornerRadius=_img_vw2.layer.frame.size.height/2;
    _img_vw2.clipsToBounds=YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
