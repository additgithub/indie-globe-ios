//
//  ProjectVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/2/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "ProjectVC.h"
#import "DeshboardVC.h"
#import "SWRevealViewController.h"
#import "UIColor+CL.h"
#import "Constants.h"
#import "NewsVC.h"
#import "EventVC.h"
#import "WYPopoverController.h"
#import "NewHomeVC.h"
#import "AllNotificationVC.h"

@interface ProjectVC ()<WYPopoverControllerDelegate>{
    WYPopoverController* popoverController;
}


@end

@implementation ProjectVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    [self customSetup];
    // Do any additional setup after loading the view.
    [self CustomizeTextField:_txt_title];
    
    [self CustomizeTextField:_txt_teamsize];
    
    [self CustomizeTextField:txt_city];
    [self CustomizeTextField:txt_state];
    [self CustomizeTextField:txt_country];
    
    txt_city.placeholder=@"City";
    txt_state.placeholder=@"State";
    txt_country.placeholder=@"country";
    
    
    
    
    _txt_title.placeholder=@"Team Title";
    
    _txt_teamsize.placeholder=@"Team size";

   
    _btn_submit.layer.cornerRadius=_btn_submit.frame.size.height/2;
    _txt_desc.delegate=self;
    _txt_desc.text = @"About Team";
    _txt_desc.textColor = [UIColor lightGrayColor];
    _txt_desc.layer.borderWidth=1.0;
    _txt_desc.layer.cornerRadius=6;
    _txt_desc.layer.borderColor=[UIColor colorWithHex:0xebebeb].CGColor;
    _txt_desc.textContainerInset = UIEdgeInsetsMake(7, 7, 0, 0);
    
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];
    
    
    _img_select_image.clipsToBounds=YES;
    _img_select_image.layer.cornerRadius=6;
    
    //Choose image
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
    [singleTap setNumberOfTapsRequired:1];
    [_img_select_image addGestureRecognizer:singleTap];

    
    [self CallCountry];
    
    _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
    _lbl_notifi_count.clipsToBounds=YES;
    _lbl_notifi_count.text=theAppDelegate.n_Count;
    
    if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
        _lbl_notifi_count.hidden=YES;
    }
    else
    {
        _lbl_notifi_count.hidden=NO;
    }
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self customSetup];
}
#pragma mark - GetAllPosts
-(void)CallAddProject
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:_txt_title.text forKey:@"project_title"];
    [AddPost setValue:_txt_desc.text forKey:@"description"];
    [AddPost setValue:_txt_teamsize.text forKey:@"team_size"];
    
    [AddPost setValue:[NSString stringWithFormat:@"%@",countryid] forKey:@"country_id"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",stateid] forKey:@"state_id"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",cityid] forKey:@"city_id"];
    
    if (select_image>0) {
        //Create manager
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
        //Now post
        NSLog(@"AddPost %@",AddPost);
        NSString *url = [NSString stringWithFormat:@"%@add_project",JsonUrlConstant];
        [manager POST:url parameters:[AddPost copy] constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            //add img data one by one
            
            NSData *imageData = UIImageJPEGRepresentation(select_image,0.5);
            [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"project_image"] fileName:[NSString stringWithFormat:@"user_image.jpg"] mimeType:@"image/jpeg"];
            
        } success:^(AFHTTPRequestOperation *operation, id dicsResponse) {
            // [(AppDelegate *)[[UIApplication sharedApplication] delegate] hideProgress];
            NSLog(@"Success: %@", dicsResponse);
            NSLog(@"Edit profile---%@",dicsResponse);
            NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            
            if ([[dicsResponse valueForKey:@"status"] integerValue] == 0) {
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Add Data not match" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                //[alert show];
                
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Add Successfull" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                //[alert show];
                [self.navigationController popViewControllerAnimated:YES];
                
            }
            [APP_DELEGATE hideLoadingView];
        }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  NSLog(@"Error: %@", error);
                  [APP_DELEGATE hideLoadingView];
              }];
    }
    else
    {
        [AddPost setObject:@"" forKey:@"project_image"];

        [APP_DELEGATE showLoadingView:@""];
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpaddproj  = [[HttpWrapper alloc] init];
        httpaddproj.delegate=self;
        httpaddproj.getbool=NO;
        [httpaddproj requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@add_project",JsonUrlConstant] param:[AddPost copy]];
    });
    }
}

#pragma mark- Wrapper Class Delegate Methods
//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary* )dicsResponse
{
    [APP_DELEGATE hideLoadingView];
    if(wrapper == httpaddproj && httpaddproj != nil)
    {
       
        NSLog(@"----%@",dicsResponse);
        [self.navigationController popViewControllerAnimated:YES];

    }
    else if(wrapper == httpcountry && httpcountry != nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        dict=[dicsResponse valueForKey:@"data"];
        country= [dicsResponse valueForKey:@"data"];
        NSLog(@"%@",country);
        
    }
    else if (wrapper==httpstate && httpstate !=nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        if ([dicsResponse valueForKey:@"status"]!=0) {
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[dicsResponse valueForKey:@"data"];
            state= [dicsResponse valueForKey:@"data"];
            NSLog(@"%@",state);
        }
    }
    else if (wrapper==httpcity && httpcity !=nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        if ([dicsResponse valueForKey:@"status"]!=0) {
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[dicsResponse valueForKey:@"data"];
            arrcity= [dicsResponse valueForKey:@"data"];
            NSLog(@"%@",arrcity);
        }
    }

    [APP_DELEGATE hideLoadingView];
}


//fetchDataFail
- (void) HttpWrapper:(HttpWrapper* )wrapper fetchDataFail:(NSError* )error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}

-(void)CallCountry
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    NSLog(@" url is %@login",JsonUrlConstant);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpcountry  = [[HttpWrapper alloc] init];
        httpcountry.delegate=self;
        httpcountry.getbool=NO;
        [httpcountry requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_country",JsonUrlConstant] param:[AddPost copy]];
        
        
        
        
    });
}
-(void)Callstate:(NSString *)str
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:str forKey:@"country_id"];
    NSLog(@"%@",AddPost);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpstate  = [[HttpWrapper alloc] init];
        httpstate.delegate=self;
        httpstate.getbool=NO;
        [httpstate requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_state"
                                                  ,JsonUrlConstant] param:[AddPost copy]];
    });
    
}
-(void)CallCity:(NSString *)str
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:str forKey:@"state_id"];
    NSLog(@"%@",AddPost);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpcity  = [[HttpWrapper alloc] init];
        httpcity.delegate=self;
        httpcity.getbool=NO;
        [httpcity requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_city"
                                                 ,JsonUrlConstant] param:[AddPost copy]];
    });
    
}


- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if (IsIphone7 || IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = 145;
    }
    else{
        self.revealViewController.rearViewRevealWidth = 145;
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if([textView.text isEqual: @"About Team"])
    {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    return YES;
}


-(void)CustomizeTextField:(UITextField *)myTextField
{
    myTextField.layer.borderWidth=1.0;
    myTextField.layer.borderColor=[UIColor colorWithHex:0xebebeb].CGColor;
    CGRect frameRect = myTextField.frame;
    frameRect.size.height = 24;
    myTextField.frame = frameRect;
    myTextField.layer.cornerRadius=myTextField.frame.size.height/2;
    
    [myTextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
    myTextField.leftView = paddingView;
    myTextField.leftViewMode = UITextFieldViewModeAlways;
    
    
    /* UIColor *color = [UIColor lightGrayColor];
     myTextField.attributedPlaceholder =
     [[NSAttributedString alloc] initWithString:@"Placeholder Text"
     attributes:@{
     NSForegroundColorAttributeName: color,
     NSFontAttributeName : [UIFont fontWithName:@"Roboto-Bold" size:10.0]
     }
     ];*/
    
    
}

-(void)commentview:(UITextField *)myTextField
{
    myTextField.layer.borderWidth=1.0;
    myTextField.layer.borderColor=[UIColor colorWithRed:(160.0f/255.0) green:(160.0f/255.0) blue:(160.0f/255.0) alpha:1].CGColor;
    CGRect frameRect = myTextField.frame;
    frameRect.size.height = 140;
    myTextField.frame = frameRect;
    myTextField.layer.cornerRadius=15;
    
    [myTextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
    myTextField.leftView = paddingView;
    myTextField.leftViewMode = UITextFieldViewModeAlways;
    
    /*UIColor *color = [UIColor lightGrayColor];
     myTextField.attributedPlaceholder =
     [[NSAttributedString alloc] initWithString:@"Placeholder Text"
     attributes:@{
     NSForegroundColorAttributeName: color,
     NSFontAttributeName : [UIFont fontWithName:@"Roboto-Bold" size:10.0]
     }
     ];
     */
    
    
}
-(void)singleTapping:(UIGestureRecognizer *)recognizer {
        NSLog(@"image clicked");
        ci= [[UIImagePickerController alloc] init];
        ci.delegate = self;
        ci.allowsEditing=YES;
        ci.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        
        if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
            [self presentViewController:ci animated:YES completion:nil];
        else
        {
            popover=[[UIPopoverController alloc]initWithContentViewController:ci];
            [popover presentPopoverFromRect:_img_select_image.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
    
}

#pragma mark - ImagePickerController Delegate
-(void)imagePickerController:(UIImagePickerController *)pick didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        //[pick dismissViewControllerAnimated:YES completion:nil];
    } else {
        [popover dismissPopoverAnimated:YES];
    }
    
    _img_select_image.image = [info objectForKey:UIImagePickerControllerEditedImage];
    //image = [info objectForKey:UIImagePickerControllerEditedImage];
    _img_extra.hidden=YES;
    _lbl_extra.hidden=YES;
    
    
    //_imgCropperViewController = [[UzysImageCropperViewController alloc] initWithImage:_img_select_image.image andframeSize:ci.view.frame.size andcropSize:CGSizeMake(320, 240)];
    //_imgCropperViewController.delegate = self;
    //[ci presentViewController:_imgCropperViewController animated:YES completion:nil];
    //[_imgCropperViewController release];
    
    select_image=[info objectForKey:UIImagePickerControllerEditedImage];
    //_img_select_image.image=[info objectForKey:UIImagePickerControllerOriginalImage];
    //_img_chooseimg.image=image;
    _img_extra.hidden=YES;
    //_lbl_extra_lab.hidden=YES;
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}

- (void)imageCropper:(UzysImageCropperViewController *)cropper didFinishCroppingWithImage:(UIImage *)image
{
    select_image=image;
    _img_select_image.image=image;
    //_img_chooseimg.image=image;
    _img_extra.hidden=YES;
    //_lbl_extra_lab.hidden=YES;
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)pick
{
    [pick dismissViewControllerAnimated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_HOMEs:(id)sender {
    
    NewHomeVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:YES];
}

- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_SCITY:(id)sender {
    [self resignFirstResponder];
    isCheck=2;
    [txt_city setTag:2];
    currentTextField = txt_city;
    [self showPopover:sender];

}

- (IBAction)btn_SSTATE:(id)sender {
    [self resignFirstResponder];
    isCheck=1;
    [txt_state setTag:1];
    currentTextField = txt_state;
    [self showPopover:sender];
}

- (IBAction)btn_SCOUNTRY:(id)sender {
    [self resignFirstResponder];
    isCheck=0;
    [txt_country setTag:0];
    currentTextField = txt_country;
    [self showPopover:sender];
}

- (IBAction)btn_ADD_PROJ:(id)sender {
    
    if (!(select_image == nil)) {
        if ([self TextFieldValidation:_txt_title :@"Please Enter title."]) {
            if (![_txt_desc.text isEqualToString:@""] && ![_txt_desc.text isEqualToString:@"About Team"] ) {
                if ([self TextFieldValidation:txt_country :@"Please Enter Country."]) {
                    if ([self TextFieldValidation:txt_state :@"Please Enter State."]) {
                        if ([self TextFieldValidation:txt_city :@"Please Enter City."]) {
                            if ([self TextFieldValidation:_txt_teamsize :@"Please Enter Team size."]) {
                                [self CallAddProject];
                            }
                        }
                    }
                }
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Enter About Team" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
            }
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please select Image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
   
}

-(BOOL)TextFieldValidation :(UITextField*)txtF :(NSString*)msg
{
    if ([txtF.text isEqualToString:@""] ||!([txtF.text length]>0)) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    else
    {
        return YES;
    }
    
}


- (IBAction)btn_NOTIFY:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}
- (void)touchesBegan:(NSSet* )touches withEvent:(UIEvent* )event {
    [self.view endEditing:YES];
}
- (void)showPopover:(UIView*)btn;
{
    
    if (popoverController == nil)
    {
        PopOverView *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PopOverView"];
        
        settingsViewController.index = currentTextField.tag;
        
        if (isCheck == 0) {
            settingsViewController.marrcountry = country;
        }
        else if (isCheck==1)
        {
            settingsViewController.marrstate = state;
        }
        else if (isCheck==2)
        {
            settingsViewController.marrcity = arrcity;
        }
        else{
            NSLog(@"NO DATA FROM API");
        }
        
        
        
        if ([settingsViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
            settingsViewController.preferredContentSize = CGSizeMake(200, 170);             // iOS 7
        }
        else {
#pragma clang diagnostic pushc
#pragma GCC diagnostic ignored "-Wdeprecated"
            settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 170);      // iOS < 7
#pragma clang diagnostic pop
        }
        settingsViewController.delegates = self;
        
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        
        settingsViewController.modalInPopover = NO;
        popoverController = [[WYPopoverController alloc] initWithContentViewController:settingsViewController];
        
        popoverController.delegate = self;
        
        popoverController.passthroughViews = @[btn];
        popoverController.popoverLayoutMargins = UIEdgeInsetsMake(0, 10, 10 , 10);
        popoverController.wantsDefaultContentAppearance = YES;
        
        [popoverController presentPopoverFromRect:btn.bounds
                                           inView:btn
                         permittedArrowDirections:WYPopoverArrowDirectionAny
                                         animated:YES
                                          options:WYPopoverAnimationOptionFadeWithScale];
    }
    
}

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    popoverController.delegate = nil;
    popoverController = nil;
}

-(void)dismissPopUpViewController
{
    [popoverController dismissPopoverAnimated:YES];
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)Popvalueselected:(NSString*)strValue{
    NSLog(@"POP:%@",strValue);
    
    currentTextField.text = strValue;
    
    if (isCheck==0) {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [country filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        countryid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        txt_state.text=@"";
        txt_city.text=@"";
        [self Callstate:countryid];
        
    }
    else if (isCheck==1)
    {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [state filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        stateid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        txt_city.text=@"";
        [self CallCity:stateid];
    }
    else if (isCheck==2)
    {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [arrcity filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        cityid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        
    }
    else
    {
        currentTextField.text = strValue;
    }
    
}




@end
