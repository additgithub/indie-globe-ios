//
//  FollowingListCell.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 3/13/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "FollowingListCell.h"

@implementation FollowingListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _img_prof.clipsToBounds=YES;
    _img_prof.layer.cornerRadius=_img_prof.frame.size.height/2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
