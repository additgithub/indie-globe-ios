//
//  CritiquesVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/28/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopOverView.h"
#import "IGCMenu.h"
#import "DeshboardVC.h"
#import "ProfileVC.h"
#import "CritiquesVC.h"
#import "ClassifiedVC.h"
#import "PackageList.h"
#import "LearningVC.h"
#import "EventVC.h"
#import "TeamBuildVC.h"
#import "UIViewController+ENPopUp.h"
#import "MyEventVC.h"
#import "SettingVC.h"
#import "NewProfileVC.h"
#import "IndieVC.h"
#import "FundingVC.h"
#import "NewsVC.h"
#import "HttpWrapper.h"
#import "ApplicationConstant.h"
#import "NewHomeSearchVC.h"





@interface CritiquesVC : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,HttpWrapperDelegate,UITableViewDelegate,UITableViewDataSource>
{
    UIView *picker;
    UITextField *currentTextField;
    NSMutableArray *arrimg;
    NSMutableArray *arr_temp;
    
    //HTTP
    HttpWrapper* httpcriti,*httpcountry,*httpstate,*httpcity,*httplike,*httpdislike;
    
    NSString *page_no;
    NSString *localapi;
    NSString *last_eventid;
    
    
    //VAR
    NSMutableArray *mutDict;
    
    int isCheck;
    NSMutableArray* country;
    IBOutlet UITextField *txt_country;
    NSString *countryid;
    
    NSMutableArray* state;
    IBOutlet UITextField *txt_state;
    NSString * stateid;
    
    NSMutableArray* arrcity;
    IBOutlet UITextField *txt_city;
    NSString * cityid;


}

//Outlet
@property (retain,nonatomic) NSString *fromMyCriti;
@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;

@property (strong, nonatomic) IBOutlet UITableView *tbl_criti;

@property (strong, nonatomic) IBOutlet UITextField *txt_serachboc;
@property (strong, nonatomic) IBOutlet UIButton *btn_postnew;
@property (strong, nonatomic) IBOutlet UIButton *btn_menu;
@property (strong, nonatomic) IBOutlet UIButton *btn_search;
@property (strong, nonatomic) IBOutlet UIButton *btn_new_menu;
@property (strong, nonatomic) IBOutlet UICollectionView *collection_ds;
@property (strong, nonatomic) IBOutlet UIImageView *img_beltimage;
@property (strong, nonatomic) IBOutlet UILabel *lbl_headernm;
@property (strong, nonatomic) IBOutlet UIImageView *img_colle_back;

@property (strong, nonatomic) IBOutlet UIButton *btn_country;
@property (strong, nonatomic) IBOutlet UIButton *btn_state;
@property (strong, nonatomic) IBOutlet UIButton *btn_city;




//Action
- (IBAction)btn_POST_NEW:(id)sender;
- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;
- (IBAction)btn_SCITY:(id)sender;
- (IBAction)btn_NEWMENU:(id)sender;
- (IBAction)btn_SCOUNTRY:(id)sender;
- (IBAction)btn_SSTATE:(id)sender;
- (IBAction)btn_NOTIFY:(id)sender;



-(void)Popvalueselected:(NSString*)strValue;
-(void)Popvalveselected:(NSString*)strValve;
-(void)dismissPopUpViewController;



@end
