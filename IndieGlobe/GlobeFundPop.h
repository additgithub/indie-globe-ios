//
//  GlobeFundPop.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/23/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GlobeFundPop : UIViewController

//Outlet
@property (strong, nonatomic) IBOutlet UIButton *btn_already_have;
@property (strong, nonatomic) IBOutlet UIButton *btn_gfm;
@property (strong, nonatomic) IBOutlet UIButton *btn_ks;



//Action
- (IBAction)btn_ALREADY:(id)sender;
- (IBAction)btn_GFM_ACTION:(id)sender;
- (IBAction)btn_KS_ACTION:(id)sender;



@end
