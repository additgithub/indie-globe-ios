//
//  Commentsview.m
//  FlexCash
//
//  Created by My Mac on 22/11/17.
//  Copyright © 2017 Rishi. All rights reserved.
//

#import "Commentsview.h"
#import "Constants.h"
#import "ExampleTableViewCell.h"
#import "OfferMain.h"
#import "OfferChild.h"
#import "ApplicationConstant.h"
#import "HttpWrapper.h"
#import "UIImage+HanekeDemo.h"
#import "UIImageView+WebCache.h"



@interface Commentsview ()<HVTableViewDataSource, HVTableViewDelegate, ExampleTableViewCellDelegate>
{
    
    NSArray* dataset;
}
@end

@implementation Commentsview
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
      appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.tableView.HVTableViewDelegate = self;
    self.tableView.HVTableViewDataSource = self;
    dataset = @[@"Twitowie", @"Bill Greyskull", @"Moonglampers", @"Psit", @"Duncan WJ Palmer", @"Sajuma", @"Victor_lee", @"Jugger-naut", @"Javiersanagustin", @"Velouria!",  @"Bill Greyskull", @"Moonglampers", @"Psit", @"Duncan WJ Palmer", @"Sajuma", @"Victor_lee", @"Jugger-naut"];
    
    NSLog(@"DICT1 %@",_Dict1);
    NSLog(@"DICT2 %@",_Dict2);
    
    
    Dict3=[[NSMutableArray alloc]init];
    for (int i=0; i<_Dict1.count; i++) {
        if ([[[_Dict1 valueForKey:@"ParentCritiqueReplyID"] objectAtIndex:i]isEqualToString:_critiID]) {
            NSLog(@"----");
            //[Dict3 setDictionary:[_Dict1 objectAtIndex:i]];
            [Dict3 addObject:[_Dict1 objectAtIndex:i]];
        }
        
    }
    
    
    if (Dict3.count<1) {
        [_lbl_nodata setCenter:self.view.center];
        _lbl_nodata.hidden=NO;
    }
    else
    {
        _lbl_nodata.hidden=YES;
    }
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [_tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
-(void)ExampleTableViewCellDidTapPurchaseButton:(ExampleTableViewCell *)cell
{
    NSString *alertTitle = [NSString stringWithFormat:@"'%@' Purchased Successfully.", cell.titlesLabel.text];
    UIAlertController* purchaseAlert = [UIAlertController alertControllerWithTitle:nil message: alertTitle preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:purchaseAlert animated:YES completion:nil];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [purchaseAlert dismissViewControllerAnimated:YES completion:nil];
    });
}
*/

#pragma mark HVTableViewDatasource
-(void)tableView:(UITableView *)tableView expandCell:(ExampleTableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath{
    cell.purchaseButton.alpha = 1;
    
    [UIView animateWithDuration:.5 animations:^{

      cell.detailLabel.text = [NSString stringWithFormat:@"%@",[[Dict3 objectAtIndex:indexPath.row] valueForKey:@"Comments"] ];
        cell.purchaseButton.alpha = 1;
    //    cell.arrow.transform = CGAffineTransformMakeRotation(0);
    }];
    
}

-(void)tableView:(UITableView *)tableView collapseCell:(ExampleTableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath{
    cell.purchaseButton.alpha = 1;
    cell.detailLabel.text = [NSString stringWithFormat:@"%@",[[Dict3 objectAtIndex:indexPath.row] valueForKey:@"Comments"] ];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return Dict3.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath isExpanded:(BOOL)isExpanded
{
    ExampleTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:[ExampleTableViewCell cellIdentifier]];
    cell.delegate = self;

        cell.backgroundColor = [UIColor whiteColor];
    cell.lbldate.text = [NSString stringWithFormat:@"%@",[[Dict3 valueForKey:@"CommentedDate"] objectAtIndex:indexPath.row] ];
    cell.titlesLabel.text = [NSString stringWithFormat:@"%@ %@",[[Dict3 valueForKey:@"FName"] objectAtIndex:indexPath.row],[[Dict3 valueForKey:@"LName"] objectAtIndex:indexPath.row]];
    
    
     //NSString *url=[_Dict1 valueForKey:@"ImageURL"][indexPath.row];
     //[cell.theImageView sd_setImageWithURL:[NSURL URLWithString:url]
                      // placeholderImage:[UIImage imageNamed:@"Avatar"]];

   
    
    if (!isExpanded) {
        cell.detailLabel.text =[NSString stringWithFormat:@"%@",[Dict3 valueForKey:@"Comments"][indexPath.row]];
        cell.purchaseButton.alpha = 1;
    }
    else
    {
        cell.detailLabel.text = [NSString stringWithFormat:@"%@",[Dict3 valueForKey:@"Comments"][indexPath.row]];
        cell.purchaseButton.alpha = 1;
    }
   
    return cell;
}
-(CGFloat)heightForText:(NSString *)text
{
    NSInteger MAX_HEIGHT = 2000;
    UITextView * textView = [[UITextView alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width-80, MAX_HEIGHT+150)];
    textView.text = text;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        textView.font = [UIFont fontWithName:@"Roboto-Light" size:13.0];
    }
    else
    {
        textView.font = [UIFont fontWithName:@"Roboto-Light" size:13.0];
    }
    
    [textView sizeToFit];
    
    return textView.frame.size.height;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath isExpanded:(BOOL)isexpanded
{
   
        if (![[[_Dict1  valueForKey:@"Type"] objectAtIndex:indexPath.row] isEqualToString:@"Reply"]) {
            
            return 0;
            
        }
        else
        {
            
            NSString * yourText = [NSString stringWithFormat:@"%@",[[Dict3 objectAtIndex:indexPath.row] valueForKey:@"Comments"] ];
            
            if (isexpanded)
                return 60+ [self heightForText:yourText];
            
            return 60;
        }
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

-(void)CallSendMSG
{
    [APP_DELEGATE showLoadingView:@""];
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:_txtcomment.text forKey:@"comment"];
    [AddPost setValue:[_Dict2 valueForKey:@"CritiquesID"]  forKey:@"critique_id"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",_critiID] forKey:@"critique_reply_id"];
    [AddPost setValue:[[_Dict1 objectAtIndex:0]valueForKey:@"UserID"] forKey:@"reply_to_id"];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    [manager POST:[NSString stringWithFormat:@"%@add_solution_reply",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        responseObject=[responseObject valueForKey:@"data"];
        NSUserDefaults *userD=[NSUserDefaults standardUserDefaults];
        
        Dict4=[[NSMutableDictionary alloc]init];
        [Dict4 setValue:[responseObject valueForKey:@"CommentedDate"] forKey:@"CommentedDate"];
        [Dict4 setValue:@"htps://indieglobe.blenzabi.com/uploads/user/" forKey:@"ImageURL"];
        [Dict4 setValue:[userD valueForKey:@"FName"] forKey:@"FName"];
        [Dict4 setValue:[userD valueForKey:@"LName"] forKey:@"LName"];
        [Dict4 setValue:[responseObject valueForKey:@"Comments"] forKey:@"Comments"];
        
        [Dict3 addObject:Dict4];
        
        
        
        [APP_DELEGATE hideLoadingView];
        

        [self.tableView reloadData];
        
        
       
       
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
              [APP_DELEGATE hideLoadingView];
          }];
    
}



- (IBAction)BtnSend:(id)sender {
    if ((![_txtcomment.text isEqualToString:@""]))
    {
        [self CallSendMSG];
        _txtcomment.text=@"";
        [self.view endEditing:YES];
    }
}
-(void)send{
    if ((![_txtcomment.text isEqualToString:@""]))
    {
        [self CallSendMSG];
        _txtcomment.text=@"";
    }
    
}
- (void)scrollToBottom
{

    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[Offerlistarray count]-1 inSection:0];
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
}
- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
