//
//  ProfilePhotoViewVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/25/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "ProfilePhotoViewVC.h"
#import "UIImageView+WebCache.h"

@interface ProfilePhotoViewVC ()

@end

@implementation ProfilePhotoViewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after tloading the view.
    
    //img_from_profile.image=_arrimgs[1];
    
    [img_from_profile sd_setImageWithURL:[NSString stringWithFormat:@"%@",[_arrimgs valueForKey:@"ImageURL"]]  placeholderImage:[UIImage imageNamed:@"Avatar"]];
    
    img_from_profile.contentMode = UIViewContentModeScaleAspectFit;
    
    lbl_title.text=[NSString stringWithFormat:@"%@",[_arrimgs valueForKey:@"PortfolioName"]];
    lbl_desc.text=[NSString stringWithFormat:@"%@",[_arrimgs valueForKey:@"Description"]];
    
    
    NSString *str=[NSString stringWithFormat:@"%@",[_arrimgs valueForKey:@"PortfolioName"]];
    
    UIPinchGestureRecognizer *pgr = [[UIPinchGestureRecognizer alloc]
                                     initWithTarget:self action:@selector(handlePinchGesture:)];
    
    img_from_profile.userInteractionEnabled = YES;
    //img_from_profile.contentMode = UIViewContentModeScaleAspectFill;
    img_from_profile.clipsToBounds = YES;
    
    pgr.delegate = self;
    
    [img_from_profile addGestureRecognizer:pgr];
    
    //_scroll_view = [[UIScrollView alloc]initWithFrame:img_from_profile.bounds.size];
    
    //
   
  /*  if (str == (id)[NSNull null] || str.length == 0 )
    {
        lbl_title.text=@"";
    }
    
    
    if (lbl_title.text == nil || [lbl_title.text isKindOfClass:[NSNull class]]) {
        //do something
         lbl_title.text=@"";
    }
    
    if ([lbl_title.text isEqual: [NSNull null]] || [lbl_title.text length]==0) {
        lbl_title.text=@"";
    }
    
    if ([lbl_desc.text isEqual: [NSNull null]]) {
        lbl_title.text=@"";
    }
   */
    
    ///lbl_title.text=[NSString stringWithFormat:@"%@",]
    
}

- (void)handlePinchGesture:(UIPinchGestureRecognizer *)gestureRecognizer {
    
    if([gestureRecognizer state] == UIGestureRecognizerStateBegan) {
        // Reset the last scale, necessary if there are multiple objects with different scales.
        lastScale = [gestureRecognizer scale];
    }
    
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan ||
        [gestureRecognizer state] == UIGestureRecognizerStateChanged) {
        
        CGFloat currentScale = [[[gestureRecognizer view].layer valueForKeyPath:@"transform.scale"] floatValue];
        
        // Constants to adjust the max/min values of zoom.
        const CGFloat kMaxScale = 3.0;
        const CGFloat kMinScale = 1.0;
//
        CGFloat newScale = 1 -  (lastScale - [gestureRecognizer scale]); // new scale is in the range (0-1)
        newScale = MIN(newScale, kMaxScale / currentScale);
        newScale = MAX(newScale, kMinScale / currentScale);
        CGAffineTransform transform = CGAffineTransformScale([[gestureRecognizer view] transform], newScale, newScale);
        [gestureRecognizer view].transform = transform;
        
        lastScale = [gestureRecognizer scale];  // Store the previous. scale factor for the next pinch gesture call
        
        
        
        [_scroll_view setContentSize:CGSizeMake(3*self.scroll_view.frame.size.width, 3*self.scroll_view.frame.size.height)];
        
        _scroll_view.contentOffset = CGPointMake(-200,-200);
        
        
       
    }
}


- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
