//
//  AddCriticsVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/22/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "AddCriticsVC.h"
#import "UIColor+CL.h"
#import "SWRevealViewController.h"
#import "DeshboardVC.h"
#import "WYPopoverController.h"
#import "Constants.h"
#import "NewsVC.h"
#import "EventVC.h"
#import "NewHomeVC.h"
#import "AllNotificationVC.h"
#import "UzysImageCropperViewController.h"



@interface AddCriticsVC ()

@end

@implementation AddCriticsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customSetup];
    // Do any additional setup after loading the view.
    [self CustomizeTextField:_txt_title];
    _txt_title.placeholder=@"Critique Title";
    
    [self CustomizeTextField:_txt_conc];
    _txt_conc.placeholder=@"Concrens";

    [self CustomizeTextField:_txt_question];
    _txt_question.placeholder=@"Question";
    
    _btn_sumbit_critic.layer.cornerRadius=_btn_sumbit_critic.frame.size.height/2;
    
    _txt_short_disc.delegate=self;
    _txt_short_disc.text = @"Question";
    _txt_short_disc.textColor = [UIColor lightGrayColor];
    _txt_short_disc.layer.borderWidth=1.0;
    _txt_short_disc.layer.cornerRadius=6;
    _txt_short_disc.layer.borderColor=[UIColor colorWithHex:0xDEDEDE].CGColor;
    _txt_short_disc.textContainerInset = UIEdgeInsetsMake(5, 7, 0, 0);
    
    
    _img_chooseimg.layer.cornerRadius=7;
    

    
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];
    
    //Choose image
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
    [singleTap setNumberOfTapsRequired:1];
    [_img_chooseimg addGestureRecognizer:singleTap];


    _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
    _lbl_notifi_count.clipsToBounds=YES;
    _lbl_notifi_count.text=theAppDelegate.n_Count;
    
    if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
        _lbl_notifi_count.hidden=YES;
    }
    else
    {
        _lbl_notifi_count.hidden=NO;
    }
    
}
-(void)singleTapping:(UIGestureRecognizer *)recognizer {
    NSLog(@"image clicked");
    ci= [[UIImagePickerController alloc] init];
    ci.delegate = self;
    ci.allowsEditing=YES;
    ci.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
        [self presentViewController:ci animated:YES completion:nil];
    else
    {
        popover=[[UIPopoverController alloc]initWithContentViewController:ci];
        [popover presentPopoverFromRect:_img_chooseimg.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if([textView.text isEqual: @"Question"])
    {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    return YES;
}

#pragma mark - ImagePickerController Delegate

-(void)imagePickerController:(UIImagePickerController *)pick didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        //[pick dismissViewControllerAnimated:YES completion:nil];
    } else {
        [popover dismissPopoverAnimated:YES];
    }
    
    UIImage *tempImage=[info objectForKey:UIImagePickerControllerEditedImage];
    
    ISimage=tempImage;
    _img_chooseimg.image=tempImage;
    _img_extra.hidden=YES;
    _lbl_extra_lab.hidden=YES;
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
  /*  ISimage=[info objectForKey:UIImagePickerControllerEditedImage];
    [_img_chooseimg setImage:[self imageWithImage:tempImage convertToSize:CGSizeMake(self.view.frame.size.height, self.view.frame.size.width)]];
   // _img_chooseimg.image = [info objectForKey:UIImagePickerControllerEditedImage];
    _img_extra.hidden=YES;
    _lbl_extra_lab.hidden=YES;
    */
    
   /* _imgCropperViewController = [[UzysImageCropperViewController alloc] initWithImage:tempImage andframeSize:ci.view.frame.size andcropSize:CGSizeMake(400, 240)];
    _imgCropperViewController.delegate = self;
    [ci presentViewController:_imgCropperViewController animated:YES completion:nil];
    [_imgCropperViewController release];
    */

    
    
}
- (void)imageCropper:(UzysImageCropperViewController *)cropper didFinishCroppingWithImage:(UIImage *)image
{
    ISimage=image;
    _img_chooseimg.image=image;
    _img_extra.hidden=YES;
    _lbl_extra_lab.hidden=YES;
    
    [self dismissViewControllerAnimated:YES completion:nil];
}



- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)pick
{
    [pick dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - GetAllPosts
-(void)CallAddCriti
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:_txt_title.text forKey:@"title"];
    [AddPost setValue:_txt_short_disc.text forKey:@"questions"];
    //[AddPost setValue:_txt_short_disc.text forKey:@"description"];
    if (ISimage>0) {
        //Create manager
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
        //Now post
        NSLog(@"AddPost %@",AddPost);
        NSString *url = [NSString stringWithFormat:@"%@add_critiques",JsonUrlConstant];
        [manager POST:url parameters:[AddPost copy] constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            //add img data one by one
            
            NSData *imageData = UIImageJPEGRepresentation(ISimage,0.5);
            [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"critiques_image"] fileName:[NSString stringWithFormat:@"user_image.jpg"] mimeType:@"image/jpeg"];
            
        } success:^(AFHTTPRequestOperation *operation, id dicsResponse) {
            // [(AppDelegate *)[[UIApplication sharedApplication] delegate] hideProgress];
            NSLog(@"Success: %@", dicsResponse);
            NSLog(@"Edit profile---%@",dicsResponse);
            NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            
            if ([[dicsResponse valueForKey:@"status"] integerValue] == 0) {
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Add Data not match" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                //[alert show];
                
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Add Successfull" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                //[alert show];
                [self.navigationController popViewControllerAnimated:YES];
                
            }
            [APP_DELEGATE hideLoadingView];
        }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  NSLog(@"Error: %@", error);
                  [APP_DELEGATE hideLoadingView];
              }];
    }
    else
    {

        [APP_DELEGATE showLoadingView:@""];
        [AddPost setObject:@"" forKey:@"critiques_image"];
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
            httpaddcriti  = [[HttpWrapper alloc] init];
            httpaddcriti.delegate=self;
            httpaddcriti.getbool=NO;
            [httpaddcriti requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@add_critiques",JsonUrlConstant] param:[AddPost copy]];
        });
    }

    
    
}

#pragma mark- Wrapper Class Delegate Methods
//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary* )dicsResponse
{
    if(wrapper == httpaddcriti && httpaddcriti != nil)
    {
        NSLog(@"---%@",dicsResponse);
    }
    [APP_DELEGATE hideLoadingView];
}


//fetchDataFail
- (void) HttpWrapper:(HttpWrapper* )wrapper fetchDataFail:(NSError* )error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}



-(void)CustomizeTextField:(UITextField *)myTextField
{
    myTextField.layer.borderWidth=1.0;
    myTextField.layer.borderColor=[UIColor colorWithHex:0xDEDEDE].CGColor;
    CGRect frameRect = myTextField.frame;
    frameRect.size.height = 28;
    myTextField.frame = frameRect;
    myTextField.layer.cornerRadius=myTextField.frame.size.height/2;
    
    [myTextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
    myTextField.leftView = paddingView;
    myTextField.leftViewMode = UITextFieldViewModeAlways;
    
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if (IsIphone7 || IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = 145;
    }
    else{
        self.revealViewController.rearViewRevealWidth = 145;
    }
}
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self customSetup];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_HOME:(id)sender {
    NewHomeVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:YES];

}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}

- (IBAction)btn_SUBMIT:(id)sender {
    if ((ISimage != 0)) {
        if ([self TextFieldValidation:_txt_title :@"Please Enter Title."]) {
            if (![_txt_short_disc.text isEqualToString:@""] && ![_txt_short_disc.text isEqualToString:@"Question"] ) {
                [self CallAddCriti];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Enter Question." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
            }
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Image is required." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
   
}

-(BOOL)TextFieldValidation :(UITextField*)txtF :(NSString*)msg
{
    if ([txtF.text isEqualToString:@""] ||!([txtF.text length]>0)) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    else
    {
        return YES;
    }
    
}

- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_NOTIFY:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}
@end
