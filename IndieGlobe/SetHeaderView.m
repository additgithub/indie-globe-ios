//
//  SetHeaderView.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 8/29/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "SetHeaderView.h"

@interface TextFieldValidatorSupport : NSObject


@end

@implementation SetHeaderView


-(void)setHeaderView:(UIView *)headerView
{
    CGRect newFrame = headerView.frame;
    
    newFrame.size.width = 200;
    
    newFrame.size.height = 200;
    
    [headerView setFrame:newFrame];
    
}

@end
