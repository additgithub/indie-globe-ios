//
//  TBCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/2/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TBCell : UITableViewCell

//Outlets

@property (strong, nonatomic) IBOutlet UILabel *lbl_title;
@property (strong, nonatomic) IBOutlet UILabel *lbl_invited;
@property (strong, nonatomic) IBOutlet UILabel *lbl_joined;

@property (strong, nonatomic) IBOutlet UITextView *txt_desc;
@property (strong, nonatomic) IBOutlet UIButton *btn_add_memb;
@property (strong, nonatomic) IBOutlet UIImageView *img_proj_image;
@property (strong, nonatomic) IBOutlet UILabel *lbl_usernm;
@property (strong, nonatomic) IBOutlet UIImageView *img_user;


//Action




@end
