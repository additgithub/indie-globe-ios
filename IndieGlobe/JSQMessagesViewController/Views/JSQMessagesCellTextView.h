//
//  Created by Jesse Squires
//  htps://www.jessesquires.com
//
//
//  Documentation
//  htps://cocoadocs.org/docsets/JSQMessagesViewController
//
//
//  GitHub
//  https://github.com/jessesquires/JSQMessagesViewController
//
//
//  License
//  Copyright (c) 2014 Jesse Squires
//  Released under an MIT license: htps://opensource.org/licenses/MIT
//

#import <UIKit/UIKit.h>

/**
 *  `JSQMessagesCellTextView` is a subclass of `UITextView` that is used to display text
 *  in a `JSQMessagesCollectionViewCell`.
 */
@interface JSQMessagesCellTextView : UITextView

@end
