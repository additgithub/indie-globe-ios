//
//  ProfileVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/27/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
#import "iCarousel.h"
#import "NewHomeSearchVC.h"
#import "TTTAttributedLabel.h"

@interface ProfileVC : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,HttpWrapperDelegate,TTTAttributedLabelDelegate>
{
    NSArray *recipeImages;
    UIView *picker;
    
    NSMutableArray *arrFollow;
    NSMutableArray *arrFollowing;
    
    //HTTP
    HttpWrapper* httpprof;
    int resFlag;
    
    NSMutableArray *arrprotdtl;
    
    NSMutableArray *arrimg;
    NSMutableArray *arrimgnm;

    NSMutableDictionary *dictres;
    
    NSMutableArray *AddNew;
    NSMutableArray *FFilms;
    NSMutableArray *SFilms;
    NSMutableArray *Reel;
    NSMutableArray *BTS;
    NSMutableArray *Video;
    NSMutableArray *HShots;
    
    NSMutableArray *AddNewnm;
    NSMutableArray *FFilmsnm;
    NSMutableArray *SFilmsnm;
    NSMutableArray *Reelnm;
    NSMutableArray *BTSnm;
    NSMutableArray *Videonm;
    NSMutableArray *HShotsnm;
    
    UIImage *thumbnail ;
}

@property (strong, nonatomic) IBOutlet UILabel *lbl_s_bio;

@property (retain,nonatomic)NSMutableArray *mutDict;
//Outlets
@property (strong, nonatomic) IBOutlet iCarousel *vw_caro;
@property (strong, nonatomic) IBOutlet UILabel *lbl_cover_title;
@property (strong, nonatomic) IBOutlet UIView *vw_details;
@property (strong, nonatomic) IBOutlet UILabel *lbl_titlenm;



@property (strong, nonatomic) IBOutlet UIScrollView *scrollVW;
@property (strong, nonatomic) IBOutlet UILabel *lbl_usernm;
@property (strong, nonatomic) IBOutlet UILabel *lbl_profe;
@property (strong, nonatomic) IBOutlet UILabel *lbl_address;
@property (strong, nonatomic) IBOutlet UILabel *lbl_user_age;

@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;

@property (strong, nonatomic) IBOutlet UICollectionView *coll_photos;
@property (strong, nonatomic) IBOutlet UICollectionView *coll_movies;
@property (strong, nonatomic) IBOutlet UIButton *btn_msgme;
@property (strong, nonatomic) IBOutlet UIImageView *img_profile;
@property (strong, nonatomic) IBOutlet UIButton *btn_GO_BACK;
@property (strong, nonatomic) IBOutlet UIButton *btn_menu;
@property (strong, nonatomic) IBOutlet UIButton *btn_follow_unf;
//@property (strong, nonatomic) IBOutlet UILabel *lbl_about_me;
@property (strong, nonatomic) IBOutlet TTTAttributedLabel *lbl_about_me;
//VW
@property (strong, nonatomic) IBOutlet UIView *vw_age_gen;
@property (strong, nonatomic) IBOutlet UIView *vw_address;
@property (strong, nonatomic) IBOutlet UILabel *lbl_follow_count;
@property (strong, nonatomic) IBOutlet UILabel *lbl_following_count;
@property (strong, nonatomic) IBOutlet UILabel *lbl_cat_id;






//Actions
- (IBAction)btn_MSG_ME:(id)sender;
- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;
- (IBAction)btn_EDIT:(id)sender;
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_MOVE_LEFT:(id)sender;
- (IBAction)btn_MOVE_RIGHT:(id)sender;
- (IBAction)btn_SUB_BACK:(id)sender;
- (IBAction)btn_FOLL_UNF:(id)sender;
- (IBAction)btn_FOLLOWLIST:(id)sender;






@end
