//
//  FundingCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/1/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShadowView.h"


@interface FundingCell : UITableViewCell
//Outlet
@property (strong, nonatomic) IBOutlet UIButton *btn_donatenow;
@property (strong, nonatomic) IBOutlet UITextView *txt_desc;
@property (weak, nonatomic) IBOutlet UILabel *lbl_desc;
@property (strong, nonatomic) IBOutlet UIImageView *img_profile;
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;
@property (strong, nonatomic) IBOutlet UILabel *lbl_goal_amt;
@property (weak, nonatomic) IBOutlet UIButton *btn_follow;

@property (weak, nonatomic) IBOutlet ShadowView *vw_shadow;

//Action
@property (strong, nonatomic) IBOutlet UIButton *btn_DONATE;


@end
