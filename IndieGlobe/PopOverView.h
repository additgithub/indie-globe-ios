//
//  PopOverView.h
//  f2f
//
//  Created by Parth Patoliya on 11/05/16.
//  Copyright (c) 2016 rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventVC.h"


@class EventVC;

@interface PopOverView : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray* arrHostel;
    NSMutableArray* arrYear;
    NSMutableArray *arrMonth;
    NSMutableArray *arrCurrent;
    NSMutableArray *arrtype;
    NSMutableArray *arrhours;
    int ischecked;
    NSMutableArray *arrCity;
    NSMutableArray *arrCategory;
    NSMutableArray *arrsortby;
    NSMutableArray *arrlearn;
    NSMutableArray *arrprofe;
    NSMutableArray *arrcountry;
    NSMutableArray *arrstate;
    
    NSMutableArray *arrDuration;

}
@property(nonatomic,assign) NSMutableArray *marrgender;
@property(nonatomic,assign) NSMutableArray *marrdays;
@property(nonatomic,assign) NSMutableArray *marrcategor;
@property(nonatomic,assign) NSMutableArray *marrcountry;
@property(nonatomic,assign) NSMutableArray *marrstate;
@property(nonatomic,assign) NSMutableArray *marrcity;

@property(nonatomic,assign) NSMutableArray *marrBranch;




@property (weak, nonatomic) IBOutlet UITableView *tblPopOver;
@property(nonatomic,assign)EventVC *delegates;

@property (nonatomic) NSInteger index;

//USING AFN


-(void)Popvalueselected:(NSString*)strValue;
-(void)dismissPopUpViewController;

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath;

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section;

- (void)encodeWithCoder:(nonnull NSCoder *)aCoder;

- (void)traitCollectionDidChange:(nullable UITraitCollection *)previousTraitCollection;

- (void)preferredContentSizeDidChangeForChildContentContainer:(nonnull id<UIContentContainer>)container;

- (CGSize)sizeForChildContentContainer:(nonnull id<UIContentContainer>)container withParentContainerSize:(CGSize)parentSize;

- (void)systemLayoutFittingSizeDidChangeForChildContentContainer:(nonnull id<UIContentContainer>)container;

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(nonnull id<UIViewControllerTransitionCoordinator>)coordinator;

- (void)willTransitionToTraitCollection:(nonnull UITraitCollection *)newCollection withTransitionCoordinator:(nonnull id<UIViewControllerTransitionCoordinator>)coordinator;

- (void)didUpdateFocusInContext:(nonnull UIFocusUpdateContext *)context withAnimationCoordinator:(nonnull UIFocusAnimationCoordinator *)coordinator;

- (void)setNeedsFocusUpdate;

- (BOOL)shouldUpdateFocusInContext:(nonnull UIFocusUpdateContext *)context;

- (void)updateFocusIfNeeded;

@end
