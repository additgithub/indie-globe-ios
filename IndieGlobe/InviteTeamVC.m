//
//  InviteTeamVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/4/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "InviteTeamVC.h"
#import "SWRevealViewController.h"
#import "UIColor+CL.h"
#import "DeshboardVC.h"
#import "WYPopoverController.h"
#import "Constants.h"
#import "ProfileVC.h"
#import "InviteTCell.h"
#import "NewHomeVC.h"
#import "AllNotificationVC.h"



@interface InviteTeamVC ()<WYPopoverControllerDelegate>{
   
    WYPopoverController* popoverController;
    
}

@end

@implementation InviteTeamVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customSetup];
    // Do any additional setup after loading the view.
    //btn_search
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.btn_search.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(10.0, 10.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    self.btn_search.layer.mask = maskLayer;
    
    [self CustomizeTextField:txt_categ];
    [self CustomizeTextField:_txt_search];
    //_txt_search.layer.borderColor=[UIColor clearColor].CGColor;
    _txt_search.placeholder=@"Search People";
    txt_categ.placeholder=@"Category";
    
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];
    
    [self CallGetCategory];
    
    _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
    _lbl_notifi_count.clipsToBounds=YES;
    _lbl_notifi_count.text=theAppDelegate.n_Count;
    
    if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
        _lbl_notifi_count.hidden=YES;
    }
    else
    {
        _lbl_notifi_count.hidden=NO;
    }

}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    page_no=@"0";
    [self CallTeamList];
    [self customSetup];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *userID = [NSString stringWithFormat:@"%@",[standardUserDefaults valueForKey:@"UserID"]];
    _refOnline = [[FIRDatabase database] reference];

    self.refOnline =[[self.refOnline child:userID] child:@"isOnline"];
    [self.refOnline setValue:@"1"];
    NSLog(@" %@",[self.refOnline child:@"isOnline"]);
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *userID = [NSString stringWithFormat:@"%@",[standardUserDefaults valueForKey:@"UserID"]];
    self.refOnline = [[FIRDatabase database] reference];
    
    self.refOnline =[[self.refOnline child:userID] child:@"isOnline"];
    [self.refOnline setValue:@"0"];
    
    
    [self.refOnline observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        NSDictionary *dict = snapshot.value;
        NSLog(@"%@",dict);
        
        
        
    } withCancelBlock:^(NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
    }];
    
    NSLog(@" %@",[self.refOnline child:@"isOnline"]);
    
    
    
}

-(void)CallGetCategory
{
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    
    [manager POST:[NSString stringWithFormat:@"%@get_profession",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        dict=[responseObject valueForKey:@"data"];
        categor= [responseObject valueForKey:@"data"];
        NSLog(@"%@",categor);
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    
    
}

#pragma mark - GetAllPosts
-(void)CallTeamList
{
    [APP_DELEGATE showLoadingView:@""];
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",page_no] forKey:@"page_no"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",_projID] forKey:@"project_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@get_people_list",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        //page_no=[responseObject valueForKey:@"page_no"];
        
        mutDict=[responseObject valueForKey:@"data"];
        [_coll_invite_team reloadData];
        
        if (mutDict.count>0) {
            //[_lbl_myevent reloadData];
        }
        else
        {
            //_lbl_myevent.showsInfiniteScrolling = NO;
        }
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
}

-(void)CallSearchTeamList
{
    [APP_DELEGATE showLoadingView:@""];
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    page_no=@"0";
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",page_no] forKey:@"page_no"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",_projID] forKey:@"project_id"];
    [AddPost setValue:categorid forKey:@"category_id"];
    if (![_txt_search.text isEqualToString:@""]) {
        [AddPost setValue:_txt_search.text forKey:@"search_text"];
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@get_people_list",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        //page_no=[responseObject valueForKey:@"page_no"];
        
        mutDict=[responseObject valueForKey:@"data"];
        [_coll_invite_team reloadData];
        
        if (mutDict.count>0) {
            //[_lbl_myevent reloadData];
        }
        else
        {
            //_lbl_myevent.showsInfiniteScrolling = NO;
        }
        [APP_DELEGATE hideLoadingView];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
}



- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if (IsIphone7 || IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = 145;
    }
    else{
        self.revealViewController.rearViewRevealWidth = 145;
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}

-(void)CustomizeTextField:(UITextField *)myTextField
{
    if (myTextField==_txt_search) {
        myTextField.layer.borderWidth=1.0;
        myTextField.layer.borderColor=[UIColor colorWithHex:0x196A84].CGColor;
    }
    myTextField.layer.borderWidth=1.0;
    myTextField.layer.borderColor=[UIColor colorWithHex:0x196A84].CGColor;
    CGRect frameRect = myTextField.frame;
    frameRect.size.height = 26;
    myTextField.frame = frameRect;
    myTextField.layer.cornerRadius=myTextField.frame.size.height/2;
    
    [myTextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
    myTextField.leftView = paddingView;
    myTextField.leftViewMode = UITextFieldViewModeAlways;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return mutDict.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    
    InviteTCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    //UIImageView *img = (UIImageView *)[cell viewWithTag:100];
    [cell.img_profile sd_setImageWithURL:[NSURL URLWithString:[mutDict valueForKey:@"ImageURL"][indexPath.row]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
    
    cell.lbl_name.text=[NSString stringWithFormat:@"%@ %@",[[mutDict  valueForKey:@"FName"] objectAtIndex:indexPath.row],[[mutDict  valueForKey:@"LName"] objectAtIndex:indexPath.row]];
    
    NSArray *arrT=[[mutDict valueForKey:@"ProfessionData"] objectAtIndex:indexPath.row];
    
    cell.lbl_prof.text=[arrT componentsJoinedByString:@","];
    NSMutableArray *NewArr = [[NSMutableArray alloc] init];
    for (int i=0; i<arrT.count; i++) {
        cell.lbl_prof.text=[arrT componentsJoinedByString:@","];
        [NewArr addObject:[[arrT objectAtIndex:i] valueForKey:@"Profession"]];
    }
    cell.lbl_prof.text=[NewArr componentsJoinedByString:@","];
    
    if ([[[mutDict valueForKey:@"IsInvited"] objectAtIndex:indexPath.row]isEqualToString:@"1"]) {
        //cell.btn_rsvp.layer.backgroundColor=[UIColor colorWithHex:0xAAAAAA].CGColor;
        cell.btn_invite.alpha=0.40;
        cell.btn_invite.enabled=NO;
    }
    else
    {
        cell.btn_invite.alpha=1.00;
        cell.btn_invite.enabled=YES;
    }
    cell.btn_invite.tag= indexPath.row;
    [cell.btn_invite addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    //UserID
    FIRDatabaseReference *observRver =  [[FIRDatabase database] reference];
    observRver =[observRver child:[[mutDict  valueForKey:@"UserID"] objectAtIndex:indexPath.row]];
    
    [observRver observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
      if (snapshot.value != [NSNull null]){
        NSMutableArray *dict = snapshot.value;
      
        
        NSString *isOnline = [dict valueForKey:@"isOnline"];
        
        if([isOnline isEqualToString:@"1"]){
            cell.img_on_offline.image = [UIImage imageNamed:@"green_on"];
            cell.lbl_on_offline.text=@"Online";
            
        } else {
            cell.img_on_offline.image = [UIImage imageNamed:@"red_off"];
            cell.lbl_on_offline.text=@"Offline";
        }
      } else {
          cell.img_on_offline.image = [UIImage imageNamed:@"red_off"];
          cell.lbl_on_offline.text=@"Offline";
//           FIRDatabaseReference *newRefrence =  [[FIRDatabase database] reference];
//          newRefrence =[[newRefrence child:[[mutDict  valueForKey:@"UserID"] objectAtIndex:indexPath.row]] child:@"isOnline"];
          //[newRefrence setValue:@"1"];
         // NSLog(@" %@",[newRefrence child:@"isOnline"]);
      }
        
    } withCancelBlock:^(NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
    }];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (IsIphone7 || IsIphone6Plus ) {
        CGFloat height = self.view.frame.size.height;
        CGFloat width  = self.view.frame.size.width;
        return CGSizeMake(width*0.40,height*0.24);
    }
    else{
        CGFloat height = self.view.frame.size.height;
        CGFloat width  = self.view.frame.size.width;
        
        return CGSizeMake(width*0.40,163);
    }
    // in case you you want the cell to be 40% of your controllers view
    // return retval;
}
-(void)yourButtonClicked:(UIButton*)sender
{
    NSLog(@"Button click");
    NSLog(@"buttonPressedDeny: %ld", (long)sender.tag);
    
    [APP_DELEGATE showLoadingView:@""];
   
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:_projID forKey:@"project_id"];
    [AddPost setValue:[[mutDict valueForKey:@"UserID"]objectAtIndex:sender.tag] forKey:@"invited_user_id"];
    NSLog(@"ADD POST %@",AddPost);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    
    NSLog(@"---%@",AddPost);//get_invited_people_list
    [manager POST:[NSString stringWithFormat:@"%@invite_member",JsonUrlConstant] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        [self CallTeamList];
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];


    //[self CallInviteMember:sender.tag];
    
    
}

-(void)CallInviteMember:(NSInteger)inviteid
{
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"project_id"];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"invited_user_id"];

    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@invite_member",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];

}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath

{
    ProfileVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileVC"];
    
    next.mutDict=[mutDict objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:next animated:YES];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_HOME:(id)sender {
    NewHomeVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:YES];
    
}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}


- (IBAction)btn_SCATEG:(id)sender {
    isCheck=3;
    [txt_categ setTag:3];
    currentTextField = txt_categ;
    [self showPopover:sender];
    
}

- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_NOTIFY:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}

- (void)touchesBegan:(NSSet* )touches withEvent:(UIEvent* )event {
    [self.view endEditing:YES];
}
- (void)showPopover:(UIView*)btn;
{
    
    if (popoverController == nil)
    {
        PopOverView *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PopOverView"];
        
        settingsViewController.index = currentTextField.tag;
        
        if (isCheck==3)
        {
            settingsViewController.marrcategor = categor;
        }
        else
        {
            NSLog(@"NO DATA FROM API");
        }

        
        
        if ([settingsViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
            settingsViewController.preferredContentSize = CGSizeMake(200, 170);             // iOS 7
        }
        else {
#pragma clang diagnostic pushc
#pragma GCC diagnostic ignored "-Wdeprecated"
            settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 170);      // iOS < 7
#pragma clang diagnostic pop
        }
        settingsViewController.delegates = self;
        
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        
        settingsViewController.modalInPopover = NO;
        popoverController = [[WYPopoverController alloc] initWithContentViewController:settingsViewController];
        
        popoverController.delegate = self;
        
        popoverController.passthroughViews = @[btn];
        popoverController.popoverLayoutMargins = UIEdgeInsetsMake(0, 10, 10 , 10);
        popoverController.wantsDefaultContentAppearance = YES;
        
        [popoverController presentPopoverFromRect:btn.bounds
                                           inView:btn
                         permittedArrowDirections:WYPopoverArrowDirectionAny
                                         animated:YES
                                          options:WYPopoverAnimationOptionFadeWithScale];
    }
    
}

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    popoverController.delegate = nil;
    popoverController = nil;
}

-(void)dismissPopUpViewController
{
    [popoverController dismissPopoverAnimated:YES];
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)Popvalueselected:(NSString*)strValue{
    NSLog(@"POP:%@",strValue);
    
    currentTextField.text = strValue;
    
    if (isCheck==3)
    {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [categor filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"ProfessionalID"]);
        categorid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"ProfessionalID"] objectAtIndex:0]];
        
        [self CallSearchTeamList];
        
        
    }
    else
    {
        currentTextField.text = strValue;
    }

    
}




@end
