//
//  AddfundVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/1/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopOverView.h"
#import "HttpWrapper.h"
#import "ApplicationConstant.h"
#import "NewHomeSearchVC.h"
#import "UzysImageCropperViewController.h"

@interface AddfundVC : UIViewController<UzysImageCropperDelegate,UIImagePickerControllerDelegate,HttpWrapperDelegate,UINavigationControllerDelegate>
{
    UIView *picker;
    
    
    UIImagePickerController *ci;
    UIPopoverController *popover;
    
    HttpWrapper *httpaddfund,*httpeditfund;
    
    UIImage *ISimage;
    
    UITextField *currentTextField;
    int isCheck;
    NSMutableArray* days;
    IBOutlet UITextField *txt_days;
    NSString *daysid;
    

}

@property (nonatomic,retain) UzysImageCropperViewController *imgCropperViewController;
@property (nonatomic,retain) UIImagePickerController *imgpicker;

@property (retain , nonatomic)NSString *isFrom;
@property (retain , nonatomic)NSMutableArray *arrDetails;


//Outlet
@property (strong, nonatomic) IBOutlet UITextField *txt_title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;

@property (strong, nonatomic) IBOutlet UIImageView *img_selectlogo;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_link;
@property (strong, nonatomic) IBOutlet UITextField *txt_goal;

@property (strong, nonatomic) IBOutlet UIButton *btn_submit;
@property (strong, nonatomic) IBOutlet UIButton *btn_menu;
@property (strong, nonatomic) IBOutlet UITextView *txt_aboutwork;
@property (strong, nonatomic) IBOutlet UIScrollView *vw_scroll;
@property (strong, nonatomic) IBOutlet UIImageView *img_extra;
@property (strong, nonatomic) IBOutlet UILabel *lbl_extra_lab;


//Action

- (IBAction)btn_SUBMIT_ACTION:(id)sender;
- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;
- (IBAction)btn_SDAY:(id)sender;
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_NOTIFY:(id)sender;


-(void)Popvalueselected:(NSString*)strValue;
-(void)Popvalveselected:(NSString*)strValve;
-(void)dismissPopUpViewController;



@end
