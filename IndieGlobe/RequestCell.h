//
//  RequestCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/26/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RequestCell : UITableViewCell

//Outlet
@property (strong, nonatomic) IBOutlet UIImageView *img_profileimg;
@property (strong, nonatomic) IBOutlet UIButton *btn_accept;
@property (strong, nonatomic) IBOutlet UIButton *btn_denied;
@property (strong, nonatomic) IBOutlet UILabel *lbl_user_nm;
@property (strong, nonatomic) IBOutlet UILabel *lbl_msges;

//Action


@end
