//
//  LearningCell.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/30/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "LearningCell.h"

@implementation LearningCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.shadow_vw setShadow];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
