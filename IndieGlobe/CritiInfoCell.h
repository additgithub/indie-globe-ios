//
//  CritiInfoCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/30/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CritiInfoCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UITextField *txt_name;
@property (strong, nonatomic) IBOutlet UITextField *txt_email;
@property (strong, nonatomic) IBOutlet UITextField *txt_phone;

@property (strong, nonatomic) IBOutlet UILabel *txt_user_comment;

@property (strong, nonatomic) IBOutlet UIImageView *img_user_img;
@property (strong, nonatomic) IBOutlet UILabel *txt_user_nm;
@property (strong, nonatomic) IBOutlet UILabel *lbl_comment_dt;
@property (strong, nonatomic) IBOutlet UIView *ve_cellview;
@property (strong, nonatomic) IBOutlet UIButton *btn_replay;


//------------

@property (weak, nonatomic) IBOutlet UIImageView *img_prof;

@property (weak, nonatomic) IBOutlet UIButton *btn_send;
@property (weak, nonatomic) IBOutlet UITextField *txt_comme;





@end
