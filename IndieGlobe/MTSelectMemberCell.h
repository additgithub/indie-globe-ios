//
//  MTSelectMemberCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 4/23/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MTSelectMemberCell : UITableViewCell



@property (strong, nonatomic) IBOutlet UILabel *lbl_usernm;
@property (strong, nonatomic) IBOutlet UIButton *btn_check;



@end
