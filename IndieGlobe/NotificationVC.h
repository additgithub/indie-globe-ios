//
//  NotificationVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 4/23/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationVC : UIViewController <UIAlertViewDelegate>


@property ( retain , nonatomic)NSString *strUserID;

@property (strong, nonatomic) IBOutlet UITextView *txt_notif;
@property (strong, nonatomic) IBOutlet UIButton *btn_send;


@property (strong, nonatomic) IBOutlet UIView *vw_background;

- (IBAction)btn_SEND_A:(id)sender;

@end
