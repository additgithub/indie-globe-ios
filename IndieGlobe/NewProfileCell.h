//
//  NewProfileCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 2/7/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewProfileCell : UICollectionViewCell


//Outlets
@property (strong, nonatomic) IBOutlet UIImageView *img_movi_image;
@property (strong, nonatomic) IBOutlet UILabel *lbl_movie_title;
@property (strong, nonatomic) IBOutlet UIButton *btn_close_b;
@property (strong, nonatomic) IBOutlet UIButton *btn_play_b;





@end
