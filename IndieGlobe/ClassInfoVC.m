//
//  ClassInfoVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/26/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "ClassInfoVC.h"
#import "EventVC.h"
#import "NewHomeVC.h"
#import "AllNotificationVC.h"
#import "CChatVC.h"
#import "ChangePass.h"
#import "ApplicationData.h"
#import "ApplicationConstant.h"
#import "UIImageView+WebCache.h"
#import "ChatListVC.h"
#import "AFTPagingBaseViewController.h"




@interface ClassInfoVC ()

@end

@implementation ClassInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSLog(@"---%@",_mutDict);
    
    if ([_isFrom isEqualToString:@"ClassifiedVC"] ) {
        [_img_image sd_setImageWithURL:[NSURL URLWithString:[_mutDict valueForKey:@"PhotoURL"]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
    }
    else
    {
        [_img_image sd_setImageWithURL:[NSURL URLWithString:[_mutDict valueForKey:@"Image"]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
    }
    
    _lbl_title.text=[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"Title"]];
    _lbl_address.text=[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"Cityname"]];
    _lbl_contact.text=[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"ContactNo"]];
    _lbl_desc.text=[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"ShortDesc"]];
    _lbl_sell_price.text=[NSString stringWithFormat:@"$ %@",[_mutDict valueForKey:@"Price"]];
    
    _img_user_image.clipsToBounds=YES;
    _img_user_image.layer.cornerRadius=_img_user_image.frame.size.height/2;
    [_img_user_image sd_setImageWithURL:[NSURL URLWithString:[_mutDict valueForKey:@"UserImageURL"]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
    _lbl_usernm.text=[NSString stringWithFormat:@"%@ %@",[_mutDict valueForKey:@"FName"],[_mutDict valueForKey:@"LName"]];
    
    
    ///--------------------------------
    
    if ([_isFrom isEqualToString:@"ClassifiedVC"]) {
        NSMutableArray *image_arr=[[NSMutableArray alloc]init];
        image_arr=[_mutDict valueForKey:@"Clasified_images"];
        
        _scroll_classi_img.pagingEnabled=YES;
        
        int x=0;
        NSLog(@"arr  :: %@", image_arr);
        for (int k=0; k<[image_arr count]; k++)
        {
            
            NSString * strImage = [[image_arr objectAtIndex:k] valueForKey:@"ImageURL"];
            
            strImage = [strImage stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
            UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(x, 0,_scroll_classi_img.frame.size.width, _scroll_classi_img.frame.size.height)];
            
            
            [img sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:@"avatar"]];
            
            img.contentMode = UIViewContentModeScaleAspectFit;
            
            
            x=x+_scroll_classi_img.frame.size.width;
            
            [_scroll_classi_img addSubview:img];
            
        }
        
        
        _scroll_classi_img.contentSize=CGSizeMake(x, _scroll_classi_img.frame.size.height);
        _scroll_classi_img.contentOffset=CGPointMake(0, 0);
    }
    else
    {
        
        _lbl_address.text=[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"CityName"]];
        
        NSMutableArray *image_arr=[[NSMutableArray alloc]init];
        image_arr=[_mutDict valueForKey:@"classified_images"];
        
        _scroll_classi_img.pagingEnabled=YES;
        
        int x=0;
        
        //Load images on ImageView
        NSLog(@"arr  :: %@", image_arr);
        
        
        for (int k=0; k<[image_arr count]; k++)
        {
            
            NSString * strImage = [[image_arr objectAtIndex:k] valueForKey:@"ImageURL"];
            
            strImage = [strImage stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
            UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(x, 0,_scroll_classi_img.frame.size.width, _scroll_classi_img.frame.size.height)];
            
            
            [img sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:@"avatar"]];
            
            img.contentMode = UIViewContentModeScaleAspectFit;
            
            
            x=x+_scroll_classi_img.frame.size.width;
            
            [_scroll_classi_img addSubview:img];
            
        }
        
        
        _scroll_classi_img.contentSize=CGSizeMake(x, _scroll_classi_img.frame.size.height);
        _scroll_classi_img.contentOffset=CGPointMake(0, 0);
    }
    
    UITapGestureRecognizer *letterTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(highlightLetter:)];
    letterTapRecognizer.numberOfTapsRequired = 1;
    [_scroll_classi_img addGestureRecognizer:letterTapRecognizer];
    
    //==========================DATE FORMATE================
    
    NSString * yourJSONString = [NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"PostedDate"]];
    
    _lbl_postdt.text=[NSString stringWithFormat:@"%@",[APP_DELEGATE DateCovertforClassified:yourJSONString]];
    
    
    _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
    _lbl_notifi_count.clipsToBounds=YES;
    _lbl_notifi_count.text=theAppDelegate.n_Count;
    
    if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
        _lbl_notifi_count.hidden=YES;
    }
    else
    {
        _lbl_notifi_count.hidden=NO;
    }
    
}

-(void)highlightLetter:(UITapGestureRecognizer*)sender
{
    NSLog(@"click on image");
    
    [APP_DELEGATE showLoadingView:@""];
    
    if ([_isFrom isEqualToString:@"ClassifiedVC"]) {
        [APP_DELEGATE showLoadingView:@""];
        
        NSMutableArray *lImages=[[NSMutableArray alloc]init];
        
        NSMutableArray *image_arr=[[NSMutableArray alloc]init];
        image_arr=[_mutDict valueForKey:@"Clasified_images"];
        
        
        for (int k=0; k<[image_arr count]; k++)
        {
            
            NSString * strImage = [[image_arr objectAtIndex:k] valueForKey:@"ImageURL"];
            
            strImage = [strImage stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
            
            NSURL *url = [NSURL URLWithString:strImage];
            NSData *data = [NSData dataWithContentsOfURL:url];
            UIImage* immg = [[UIImage alloc]initWithData:data];
            
            [lImages addObject:immg];
            
        }
        
        NSArray *landscapeImages = [NSArray arrayWithArray:lImages];
        
        
        Class cls = NSClassFromString(@"AFTNormalPagingViewController");
        AFTPagingBaseViewController *vc = [cls new];
        AFTPagingBaseViewController *bvc = (AFTPagingBaseViewController *)vc;
        
        bvc.title = @"Photo";
        bvc.images = landscapeImages;
        [APP_DELEGATE hideLoadingView];
        [self presentViewController:bvc animated:YES completion:nil];
        
    }
    else
    {
        [APP_DELEGATE showLoadingView:@""];
        
        NSMutableArray *lImages=[[NSMutableArray alloc]init];
        
        NSMutableArray *image_arr=[[NSMutableArray alloc]init];
        image_arr=[_mutDict valueForKey:@"classified_images"];
        
        
        for (int k=0; k<[image_arr count]; k++)
        {
            
            NSString * strImage = [[image_arr objectAtIndex:k] valueForKey:@"ImageURL"];
            
            strImage = [strImage stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
            
            NSURL *url = [NSURL URLWithString:strImage];
            NSData *data = [NSData dataWithContentsOfURL:url];
            UIImage* immg = [[UIImage alloc]initWithData:data];
            
            [lImages addObject:immg];
            
        }
        
        NSArray *landscapeImages = [NSArray arrayWithArray:lImages];
        
        
        Class cls = NSClassFromString(@"AFTNormalPagingViewController");
        AFTPagingBaseViewController *vc = [cls new];
        AFTPagingBaseViewController *bvc = (AFTPagingBaseViewController *)vc;
        
        bvc.title = @"Photo";
        bvc.images = landscapeImages;
        [APP_DELEGATE hideLoadingView];
        [self presentViewController:bvc animated:YES completion:nil];
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - GetAllPosts
-(void)CallMyMethod
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    [AddPost setValue:@"Admin" forKey:@"email"];
    [AddPost setValue:@"Admin" forKey:@"password"];
    
    NSLog(@" url is %@login",JsonUrlConstant);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpclassiinfo  = [[HttpWrapper alloc] init];
        httpclassiinfo.delegate=self;
        httpclassiinfo.getbool=NO;
        [httpclassiinfo requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@login",JsonUrlConstant] param:[AddPost copy]];
    });
}

#pragma mark- Wrapper Class Delegate Methods
//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary* )dicsResponse
{
    [APP_DELEGATE hideLoadingView];
    if(wrapper == httpclassiinfo && httpclassiinfo != nil)
    {
        
    }
}


//fetchDataFail
- (void) HttpWrapper:(HttpWrapper* )wrapper fetchDataFail:(NSError* )error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)btn_HOME:(id)sender {
    NewHomeVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:YES];

}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}


- (IBAction)btn_NOTIFY:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}

- (IBAction)btn_MSGCHAT:(id)sender {
    
    
    
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *strId=[NSString stringWithFormat:@"%@",[standardUserDefaults valueForKey:@"UserID"]];
    NSString *PostedId=[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"PostedBy"]];
    
    
   if ([strId intValue]==[PostedId intValue]) {
      
       
       CChatVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"CChatVC"];
       next.mutDict=_mutDict;
       next.isFrom=_isFrom;
       [self.navigationController pushViewController:next animated:YES];
    }
    else
    {
        NSMutableDictionary *headerDict=[[NSMutableDictionary alloc]init];
        [headerDict setValue:[_mutDict valueForKey:@"FName"] forKey:@"FName"];//UserImageURL
        [headerDict setValue:[_mutDict valueForKey:@"UserImageURL"] forKey:@"UserImageURL"];
        
        
        NSUserDefaults *stander=[[NSUserDefaults alloc]init];
        [stander setValue:headerDict forKey:@"chatListHeader"];
        NSMutableDictionary *mutD=[[NSMutableDictionary alloc]init];
        [mutD setValue:[_mutDict valueForKey:@"PostedBy"] forKey:@"FromID"];
        [mutD setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"ToID"];
        
        ChatListVC *vc = [ChatListVC messagesViewController];
        vc.mutDict=mutD;
        vc.classedID=[_mutDict valueForKey:@"ClassfiedID"];
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    
  
    
    
    

    
    
    
    
    
}
@end
