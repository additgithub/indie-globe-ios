//
//  AllNotificationVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 4/25/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewHomeSearchVC.h"

@interface AllNotificationVC : UIViewController<UITabBarDelegate,UITableViewDataSource>
{
    NSMutableDictionary *Dict;
    float hei;
}



//Outlets
@property (strong, nonatomic) IBOutlet UITableView *tbl_notif;






//Action

- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;




@end
