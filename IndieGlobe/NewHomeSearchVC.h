//
//  NewHomeSearchVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 6/28/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "HttpWrapper.h"
#import "ApplicationConstant.h"
#import "NewHomeSearchVC.h"

#import "SetHeaderView.h"

@interface NewHomeSearchVC : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *arrResponce,*arrdummy;
    float hei;
    BOOL isFilterd;
     UIView *picker;
    //ArrayCreation
    NSMutableArray *filterdarray;
    
   
}
- (IBAction)btnBack:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;

@property (strong, nonatomic) IBOutlet UIView *Search_View;
//Outlet
@property (weak, nonatomic) IBOutlet UITableView *tbl_searchlist;
@property (weak, nonatomic) IBOutlet UIButton *btn_menu;
@property (weak, nonatomic) IBOutlet UITextField *txt_search;





///Action
- (IBAction)btn_NOTIF:(id)sender;
- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;




@end
