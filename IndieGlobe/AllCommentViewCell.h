//
//  AllCommentViewCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 10/22/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllCommentViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *img_prof;
@property (weak, nonatomic) IBOutlet UILabel *lbl_nm;
@property (weak, nonatomic) IBOutlet UILabel *lbl_comm;

//Cell2
@property (weak, nonatomic) IBOutlet UIImageView *img_prof2;
@property (weak, nonatomic) IBOutlet UITextField *txt_comm;
@property (weak, nonatomic) IBOutlet UIButton *btn_send;




@end
