//
//  CChatVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 6/29/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "CChatVC.h"
#import "ApplicationData.h"
#import "ApplicationConstant.h"
#import "CChatCell.h"
#import "UIImageView+WebCache.h"
#import "ChatListVC.h"
#import "NewHomeVC.h"

@interface CChatVC ()

@end

@implementation CChatVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self CallChatList];
    
}

-(void)CallChatList
{
    [APP_DELEGATE showLoadingView:@""];
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    
    if ([_isFrom isEqualToString:@"AllNotificationVC"]) {
        [AddPost setValue:_critiID forKey:@"classified_id"];
    }
    else if ([_isFrom isEqualToString:@"ClassifiedVC"]) {
         [AddPost setValue:[_mutDict valueForKey:@"ClassfiedID"] forKey:@"classified_id"];//ClassifiedVC
    }else
    {
        [AddPost setValue:[_mutDict valueForKey:@"ID"] forKey:@"classified_id"];
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"PARAM---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@get_classified_chat_people_list",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        if ([responseObject valueForKey:@"status"]) {
            arrChatList=[responseObject valueForKey:@"data"];
        }
        
        [APP_DELEGATE hideLoadingView];
        
        [_tbl_chatlist reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
        
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrChatList.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Cell";
    //tableView.separatorColor = [UIColor clearColor];
    CChatCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[CChatCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
  
        cell.lbl_usernm.text=[NSString stringWithFormat:@"%@ %@",[arrChatList valueForKey:@"FName"][indexPath.row],[arrChatList valueForKey:@"LName"][indexPath.row]];
    
        [cell.img_user sd_setImageWithURL:[NSURL URLWithString:[[arrChatList valueForKey:@"UserImageURL"] objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
    
    cell.lbl_lastmsg.text=[NSString stringWithFormat:@"%@",[[[arrChatList valueForKey:@"single_chat"] objectAtIndex:indexPath.row] valueForKey:@"Comments"]];
    
    //================DATE TIME CALCULATION------------
    //CreatedOn
    NSString * yourJSONString = [NSString stringWithFormat:@"%@",[[[arrChatList valueForKey:@"single_chat"] objectAtIndex:indexPath.row] valueForKey:@"CreatedOn"]];
    
    NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
    [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateFromString = [currentDTFormatter dateFromString:yourJSONString];
    [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *eventDateStr = [currentDTFormatter stringFromDate:dateFromString];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *currentDate = [NSDate date];
    NSString *start = [formatter stringFromDate:dateFromString];
    NSString *end = [formatter stringFromDate:currentDate];
    //NSString *end = @"2018-01-11";
    
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *startDate = [f dateFromString:start];
    NSDate *endDate = [f dateFromString:end];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:(NSCalendarUnitDay |NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitWeekOfYear)
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    
    NSLog(@"Day %ld", [components day]);
    NSLog(@"Hour %ld", [components hour]);
    NSLog(@"Minute %ld", [components minute]);
    
    
    
    if ([components weekOfYear]>0) {
        cell.lbl_date.text=[NSString stringWithFormat:@"%ld week ago",(long)[components weekOfYear]];
    }
    else if ([components hour]>0) {
        cell.lbl_date.text=[NSString stringWithFormat:@"%ld hours ago",(long)[components hour]];
    }
    else if ([components minute]>0)
    {
        cell.lbl_date.text=[NSString stringWithFormat:@"%ld min ago",(long)[components minute]];
    }
    else{
        cell.lbl_date.text=[NSString stringWithFormat:@"%ld day ago",(long)[components day]];
    }
    

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    NSUserDefaults *stander=[[NSUserDefaults alloc]init];
    [stander setValue:[arrChatList objectAtIndex:indexPath.row] forKey:@"chatListHeader"];
    ChatListVC *vc = [ChatListVC messagesViewController];
    vc.mutDict=[arrChatList objectAtIndex:indexPath.row];
    vc.classedID=[_mutDict valueForKey:@"ClassfiedID"];
    [self.navigationController pushViewController:vc animated:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)btn_BACK:(id)sender {
    if ([_isFrom isEqualToString:@"ClassifiedVC"])
    {
/*        NewHomeVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
        [self.navigationController pushViewController:next animated:NO];
  */
        [self.navigationController popViewControllerAnimated:YES];

    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

- (IBAction)btn_HOME:(id)sender {
   /* NewHomeVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:NO];
    */
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
@end
