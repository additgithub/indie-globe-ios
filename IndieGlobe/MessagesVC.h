//
//  MessagesVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/26/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
#import "ApplicationConstant.h"
#import "NewHomeSearchVC.h"



@interface MessagesVC : UIViewController<HttpWrapperDelegate>
{
    UIView *picker;
    
    HttpWrapper* httpmsg;
    NSString* page_no;
    NSMutableArray *mutDict;
    NSMutableArray *mutDictreq;
    
    NSMutableArray *arrUnread;
    float hei;
}

@property (retain , nonatomic)NSString *isfronN;
//Outlet
@property (strong, nonatomic) IBOutlet UIButton *btn_menu;
@property (strong, nonatomic) IBOutlet UIButton *btn_msg;
@property (strong, nonatomic) IBOutlet UIButton *btn_request;
@property (strong, nonatomic) IBOutlet UITableView *tbl_message;
@property (strong, nonatomic) IBOutlet UITableView *tbl_request;
@property (strong, nonatomic) IBOutlet UILabel *txt_msg_count;
@property (strong, nonatomic) IBOutlet UILabel *txt_req_count;
@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;


//Action
- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;
- (IBAction)btn_MESSAGE:(id)sender;
- (IBAction)btn_REQUEST_ACTION:(id)sender;
- (IBAction)btn_NOTIFY:(id)sender;



@end
