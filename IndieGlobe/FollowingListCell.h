//
//  FollowingListCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 3/13/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FollowingListCell : UITableViewCell

//Outlet
@property (strong, nonatomic) IBOutlet UIImageView *img_prof;
@property (strong, nonatomic) IBOutlet UILabel *lbl_usernm;



@end
