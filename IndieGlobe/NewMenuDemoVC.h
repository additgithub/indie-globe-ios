//
//  NewMenuDemoVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/15/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGCMenu.h"

@interface NewMenuDemoVC : UIViewController<IGCMenuDelegate>

@property (strong, nonatomic) IBOutlet UIButton *btn_menuclick;


- (IBAction)btn_CLICKMENU:(id)sender;

@end
