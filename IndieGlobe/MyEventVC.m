//
//  MyEventVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/7/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "MyEventVC.h"
#import "SWRevealViewController.h"
#import "DeshboardVC.h"
#import "MyEventDetailsVC.h"
#import "MyEventCell.h"
#import "Constants.h"
#import "EditEventVC.h"
#import "NewsVC.h"
#import "EventVC.h"
#import "SVPullToRefresh.h"
#import "NewHomeVC.h"
#import "AllNotificationVC.h"
#import "ProfilePhotoForHome.h"
#import "AllCommentView.h"
#import "AddEventVC.h"



@interface MyEventVC ()

@end

@implementation MyEventVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customSetup];
    self.navigationController.navigationBarHidden=YES;

    // Do any additional setup after loading the view.
    [self CustomizeTextField:_txt_searchbox];
    _txt_searchbox.placeholder=@"Search your event";
    
    page_no=@"0";
    
    //btn_search
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.btn_searchbtn.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(10.0, 10.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    self.btn_searchbtn.layer.mask = maskLayer;
    
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];
    
    [_btn_searchbtn addTarget:self
                       action:@selector(CallSearchMethod)
          forControlEvents:UIControlEventTouchUpInside];
    
    
    _btn_addevent.layer.cornerRadius=_btn_addevent.layer.frame.size.height/2;
    _btn_addevent.clipsToBounds=YES;



    [self CallGetEvent];
    
    _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
    _lbl_notifi_count.clipsToBounds=YES;
    _lbl_notifi_count.text=theAppDelegate.n_Count;
    
    if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
        _lbl_notifi_count.hidden=YES;
    }
    else
    {
        _lbl_notifi_count.hidden=NO;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)CustomizeTextField:(UITextField *)myTextField
{
    myTextField.layer.borderWidth=1.0;
    myTextField.layer.borderColor=[UIColor colorWithHex:0x196A84].CGColor;
    CGRect frameRect = myTextField.frame;
    frameRect.size.height = 26;
    myTextField.frame = frameRect;
    myTextField.layer.cornerRadius=myTextField.frame.size.height/2;
    
    [myTextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
    myTextField.leftView = paddingView;
    myTextField.leftViewMode = UITextFieldViewModeAlways;
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    page_no=@"0";
    [self CallGetEvent];
    [self customSetup];
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if (IsIphone7 || IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = 145;
    }
    else{
        self.revealViewController.rearViewRevealWidth = 145;
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}

-(void)CallSearchMethod
{
    [APP_DELEGATE showLoadingView:@""];
    page_no=@"0";
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    
    [AddPost setValue:[NSString stringWithFormat:@"%@",page_no] forKey:@"page_no"];
    
    if (![_txt_searchbox.text isEqualToString:@""]) {
        [AddPost setValue:_txt_searchbox.text forKey:@"search_text"];
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@get_user_events",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        page_no=[responseObject valueForKey:@"page_no"];
        
        mutDict=[responseObject valueForKey:@"data"];
        
        if (mutDict.count>0) {
            [_lbl_myevent reloadData];
        }
        else
        {
            _lbl_myevent.showsInfiniteScrolling = NO;
        }
        [APP_DELEGATE hideLoadingView];
        
        if (!(mutDict.count>0)) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"NO EVENTs AVAILABLE." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
        
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
}

-(void)CallSearchMethod1
{
    [APP_DELEGATE showLoadingView:@""];
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    //Optional
    
    [AddPost setValue:[NSString stringWithFormat:@"%@",page_no] forKey:@"page_no"];
    if (![_txt_searchbox.text isEqualToString:@""]) {
        [AddPost setValue:_txt_searchbox.text forKey:@"search_text"];
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@get_user_events",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        page_no=[responseObject valueForKey:@"page_no"];
        
        if ([[responseObject valueForKey:@"data"] count]>0) {
            
            int64_t delayInSeconds = 2.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                //[_tbl_event beginUpdates];
                NSArray *arr = [responseObject valueForKey:@"data"];
                NSMutableArray *newarr = [[NSMutableArray alloc] init];
                NSLog(@"---%@",newarr);
                for (NSString *str in mutDict) {
                    [newarr addObject:str];
                }
                for (int i=0; i<arr.count; i++) {
                    //[mutDict addObject:arr[i]];
                    [newarr addObject:arr[i]];
                }
                NSLog(@"---%@",newarr);
                mutDict = [[NSMutableArray alloc] init];
                for (NSString*str  in newarr) {
                    [mutDict addObject:str];
                }
                
                
                NSLog(@"--%@",mutDict);
                [_lbl_myevent.infiniteScrollingView stopAnimating];
                [_lbl_myevent reloadData];
            });
            
        }
        else
        {
            [_lbl_myevent.infiniteScrollingView stopAnimating];
        }
        [APP_DELEGATE hideLoadingView];
        if (!(mutDict.count>0)) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"NO EVENTs AVAILABLE." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
        //[_lbl_myevent reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
    
    
}

#pragma mark - GetAllPosts
-(void)CallGetEvent
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",page_no] forKey:@"page_no"];
    
    NSLog(@" url is %@get_user_events",JsonUrlConstant);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpmyevent  = [[HttpWrapper alloc] init];
        httpmyevent.delegate=self;
        httpmyevent.getbool=NO;
        [httpmyevent requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_user_events",JsonUrlConstant] param:[AddPost copy]];
    });
}
#pragma mark- Wrapper Class Delegate Methods
//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary* )dicsResponse
{
    [APP_DELEGATE hideLoadingView];
    if(wrapper == httpmyevent && httpmyevent != nil)
    {
        NSLog(@"---%@",dicsResponse);
        NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
        mutDict=[[NSMutableArray alloc] init];
        mutDict=[dicsResponse valueForKey:@"data"];
        page_no=[dicsResponse valueForKey:@"page_no"];
        
        
        
        if ([[dicsResponse valueForKey:@"status"] integerValue] == 0) {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Success" message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //[alert show];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Success" message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //[alert show];
            
            
            
            [_lbl_myevent reloadData];
            
            if (!(mutDict.count>0)) {
                _lbl_myevent.hidden=YES;
            }
            
        }
        
    }
    
}


//fetchDataFail
- (void) HttpWrapper:(HttpWrapper* )wrapper fetchDataFail:(NSError* )error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    if (mutDict.count>0) {
        return mutDict.count;
    }
    else
    {
        
        return mutDict.count;
    }
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Cell";
    tableView.separatorColor = [UIColor clearColor];
    
    MyEventCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[MyEventCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.btn_edit_cell.tag = indexPath.row;
    [cell.btn_edit_cell addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btn_prof_vw.tag = indexPath.row;
    [cell.btn_prof_vw addTarget:self action:@selector(profileImageView:) forControlEvents:UIControlEventTouchUpInside];
   
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [cell.img_user_prof sd_setImageWithURL:[NSURL URLWithString:[mutDict valueForKey:@"UserImageURL"][indexPath.row]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
    cell.lbl_usernm.text=[NSString stringWithFormat:@"%@ %@",[mutDict valueForKey:@"FName"][indexPath.row],[mutDict valueForKey:@"LName"][indexPath.row]];
    
    
    NSString *struserm=[standardUserDefaults valueForKey:@"FName"];
    if ([[mutDict valueForKey:@"FName"][indexPath.row] isEqualToString:struserm]) {
        cell.btn_edit_cell.hidden=NO;
    }
    else
    {
        cell.btn_edit_cell.hidden=YES;
    }
    
    ////---------------------------
    NSMutableArray *image_arr=[[NSMutableArray alloc]init];
    image_arr=[mutDict valueForKey:@"event_images"][indexPath.row];
    
    cell.scroll_event_img.pagingEnabled=YES;
    int x=0;
    //Load images on ImageView
    NSLog(@"arr  :: %@", image_arr);
    
    if (image_arr.count>0) {
        for (int k=0; k<[image_arr count]; k++)
        {
            
            NSString * strImage = [[image_arr objectAtIndex:k] valueForKey:@"ImageURL"];
            
            strImage = [strImage stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
            UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(x, 0,cell.scroll_event_img.frame.size.width, cell.scroll_event_img.frame.size.height)];
            
            
            [img sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:@"avatar"]];
            
            x=x+cell.scroll_event_img.frame.size.width;
            
            [img setContentMode:UIViewContentModeScaleAspectFill];
            [img setClipsToBounds:YES];
            
            [cell.scroll_event_img addSubview:img];
            
        }
    }
    else
    {
        //https://indieglobe.blenzabi.com/uploads/events/
        
        
        
        NSString * strImage = @"https://indieglobe.blenzabi.com/uploads/events/";
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(x, 0,cell.scroll_event_img.frame.size.width, cell.scroll_event_img.frame.size.height)];
        // img.contentMode = UIViewContentModeScaleAspectFit;
        //[img sd_setImageWithURL:[NSURL URLWithString:strImage]
        //placeholderImage:[UIImage imageNamed:@"avatar"]];
        //NSURL *url = [NSURL URLWithString:strImage];
        //[img hnk_setImageFromURL:url];
        
        [img sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
        
        x=x+cell.scroll_event_img.frame.size.width;
        [img setContentMode:UIViewContentModeScaleAspectFill];
        [img setClipsToBounds:YES];
        
        [cell.scroll_event_img addSubview:img];
    }
    
    cell.scroll_event_img.contentSize=CGSizeMake(x, cell.scroll_event_img.frame.size.height);
    cell.scroll_event_img.contentOffset=CGPointMake(0, 0);
    
    ///--------------------------------------
    
    ///-----------------------------
    
    if ([[[mutDict valueForKey:@"IsLike"] objectAtIndex:indexPath.row] intValue]==1) {
        cell.btn_like.selected=YES;
    }
    else
    {
        cell.btn_like.selected=NO;
    }
    
    
    if ([[[mutDict valueForKey:@"IsDisike"] objectAtIndex:indexPath.row] intValue]==1) {
        cell.btn_dislike.selected=YES;
    }
    else
    {
        cell.btn_dislike.selected=NO;
    }
    
    
    cell.lbl_like.text=[NSString stringWithFormat:@"%@",[[mutDict valueForKey:@"TotalLikes"] objectAtIndex:indexPath.row]];
    
    cell.lbl_dislike.text=[NSString stringWithFormat:@"%@",[[mutDict valueForKey:@"TotalDislikes"] objectAtIndex:indexPath.row]];
    
    
    ////Comment  TotalComment
    
    cell.lbl_comment.text=[NSString stringWithFormat:@"%@",[mutDict valueForKey:@"TotalComments"][indexPath.row]];
    
    //////LIKE DISLIKE
    [cell.btn_like setTag:indexPath.row];
    [cell.btn_like addTarget:self action:@selector(CallLike:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.btn_dislike setTag:indexPath.row];
    [cell.btn_dislike addTarget:self action:@selector(CallDisLike:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.btn_comment setTag:indexPath.row];
    [cell.btn_comment addTarget:self action:@selector(ViewComments:) forControlEvents:UIControlEventTouchUpInside];
    
    
    ////////COmment
    cell.lbl_comment.text=[NSString stringWithFormat:@"%@",[mutDict valueForKey:@"TotalComments"][indexPath.row]];
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]initWithArray:[[mutDict valueForKey:@"last_comments"] objectAtIndex:indexPath.row]];
    
    //NSLog(@"arrTemp %@",arrTemp);
    
    
    for (int i=0; i<arrTemp.count; i++) {
        switch (i) {
            case 0:
            {
                [cell.img_vw1 sd_setImageWithURL:[NSURL URLWithString:[arrTemp valueForKey:@"ImageURL"][0]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
                
                cell.lbl_title_vw1.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"FName"][0]];
                
                cell.lbl_desc_vw1.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"Comments"][0]];
                
            }
                break;
            case 1:
            {
                [cell.img_vw2 sd_setImageWithURL:[NSURL URLWithString:[arrTemp valueForKey:@"ImageURL"][1]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
                
                cell.lbl_title_vw2.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"FName"][1]];
                
                cell.lbl_desc_vw2.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"Comments"][1]];
            }
                break;
                
            default:
                break;
        }
    }
    
    int C=[[[mutDict valueForKey:@"last_comments"] objectAtIndex:indexPath.row] count];
    
    if (C==2) {
        cell.vw_2.hidden=NO;
        cell.vw_1.hidden=NO;
    }
    else if (C==1)
    {
        cell.vw_1.hidden=NO;
        cell.vw_2.hidden=YES;
        
    }
    else
    {
        
        cell.vw_2.hidden=YES;
        cell.vw_1.hidden=YES;
    }
    
    ///DATE 
    NSString *startDt=[APP_DELEGATE DateCovertforEvent:[mutDict valueForKey:@"StartDate"][indexPath.row]];
    NSString *endDt=[APP_DELEGATE DateCovertforEvent:[mutDict valueForKey:@"EndDate"][indexPath.row]];
    
    cell.lbl_postdt.text=[NSString stringWithFormat:@"%@ - %@",startDt,endDt];
    NSArray *arr = [startDt componentsSeparatedByString:@" "];
    
    cell.lbl_short_m.text=[arr objectAtIndex:0];
    cell.lbl_short_d.text=[arr objectAtIndex:1];
    
    
    
    cell.lbl_rsvp.text=[NSString stringWithFormat:@"%@ People RSVP",[[mutDict  valueForKey:@"TotalIsRSVP"] objectAtIndex:indexPath.row]];
    
    cell.lbl_title.text=[NSString stringWithFormat:@"%@",[[mutDict  valueForKey:@"Title"] objectAtIndex:indexPath.row]];
    
    cell.lbl_address.text=[NSString stringWithFormat:@"%@",[[mutDict  valueForKey:@"Address"] objectAtIndex:indexPath.row]];
    
    [cell.img_profile sd_setImageWithURL:[NSURL URLWithString:[mutDict valueForKey:@"EventImageURL"][indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
    
    
   
    

    
    //cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.selected) {
        // ... Uncheck
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
     [self.view endEditing:YES];
    /*MyEventDetailsVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"MyEventDetailsVC"];
    [self.navigationController pushViewController:next animated:YES];*/
    
    MyEventDetailsVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"MyEventDetailsVC"];
    NSArray *arr = [[NSArray alloc] initWithObjects:mutDict, nil][0];
    NSLog(@"arr %@",arr);
    next.mutdict=[arr objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:next animated:YES];

    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *simpleTableIdentifier = @"CellEvent";
    //tableView.separatorColor = [UIColor clearColor];
    MyEventCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[MyEventCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    
    int C=[[[mutDict valueForKey:@"last_comments"] objectAtIndex:indexPath.row] count];
    
    if (C==2) {
        
        return 490;
        
    }
    else if (C==1)
    {
        cell.vw_2.hidden=YES;
        return 490-50;
        
        
        
    }
    else
    {
        cell.vw_2.hidden=YES;
        cell.vw_1.hidden=YES;
        
        return 490-100;
        
        
    }
}

-(void)yourButtonClicked:(UIButton*)sender
{
    NSLog(@"Event is edit!!");
    
    EditEventVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"EditEventVC"];
    NSArray *arr = [[NSArray alloc] initWithObjects:mutDict, nil][0];
    NSLog(@"arr %@",arr);
    next.mutDict=[arr objectAtIndex:sender.tag];
    [self.navigationController pushViewController:next animated:YES];

}

-(void)profileImageView:(UIButton*)sender
{
    
    
}

-(void)ViewComments:(id)sender
{
    NSLog(@"ViewComments %@",sender);
    //NSInteger ival=[sender integerValue];
    
    UIButton *btn = (UIButton *)sender;
    CGPoint origin = btn.frame.origin;
    CGPoint point = [btn.superview convertPoint:origin toView:_lbl_myevent];
    NSIndexPath * indexPath = [_lbl_myevent indexPathForRowAtPoint:point];
    
    
    
    AllCommentView *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllCommentView"];
    next.user_portfolio_category_id=[mutDict valueForKey:@"EventID"][indexPath.row];
    next.category_nm=@"Events";
    [self.navigationController pushViewController:next animated:NO];
    
    
    
}


-(void)CallLike:(UIButton*)sender
{
    UIButton *btn=sender;
    
    NSLog(@"button was clicked for the following row %li", (long)btn.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[btn tag]  inSection:0];
    NSLog(@"indexpath.row %ld", (long)indexPath.row);
    NSLog(@"indexpath.row %ld", (long)indexPath.section);
    
    
    
    
    /*user_id
     indie_pick_id
     is_like */
    
    NSString *strID=[[mutDict valueForKey:@"EventID"] objectAtIndex:indexPath.row];
    NSString *strISLike=[[mutDict valueForKey:@"IsLike"] objectAtIndex:indexPath.row];
    
    if ([strISLike isEqualToString:@"0"]) {
        strISLike=@"1";
    }
    else
    {
        strISLike=@"0";
    }
    
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:strID forKey:@"event_id"];
    [AddPost setValue:strISLike forKey:@"is_like"];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@events_like",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        
        page_no=@"0";
        [self CallGetEvent];
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
}

-(void)CallDisLike:(UIButton*)sender
{
    UIButton *btn=sender;
    
    NSLog(@"button was clicked for the following row %li", (long)btn.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[btn tag]  inSection:0];
    NSLog(@"indexpath.row %ld", (long)indexPath.row);
    NSLog(@"indexpath.row %ld", (long)indexPath.section);
    
    
    
    
    /*user_id
     indie_pick_id
     is_like */
    
    NSString *strID=[[mutDict valueForKey:@"EventID"] objectAtIndex:indexPath.row];
    NSString *strISLike=[[mutDict valueForKey:@"IsDisike"] objectAtIndex:indexPath.row];
    
    if ([strISLike isEqualToString:@"0"]) {
        strISLike=@"1";
    }
    else
    {
        strISLike=@"0";
    }
    
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:strID forKey:@"event_id"];
    [AddPost setValue:strISLike forKey:@"is_dislike"];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@events_dislike",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        page_no=@"0";
        [self CallGetEvent];
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    int lastRow=[mutDict count]-1;
    if(([indexPath row] == lastRow)&&(lastRow<[mutDict count]))
    {
        //[self Getnewdata];
        NSLog(@"i m call");
        
        if ([mutDict count]>=2) {
            __weak MyEventVC *weakSelf = self;
            // setup infinite scrolling
            [_lbl_myevent addInfiniteScrollingWithActionHandler:^{
                [weakSelf CallSearchMethod1];
            }];
        }
        
       
        //[self CallSearchMethod];
        
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)touchesBegan:(NSSet* )touches withEvent:(UIEvent* )event {
    
    [self.view endEditing:YES];
    
}
- (IBAction)btn_ADDEVENT:(id)sender {
    
    AddEventVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"AddEventVC"];
    [self.navigationController pushViewController:next animated:YES];
    
}

- (IBAction)btn_NOTIFY:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}

- (IBAction)btn_HOME:(id)sender {
    
    NewHomeVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:YES];
}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}


- (IBAction)btn_EDIT_EVENT:(id)sender {
}

- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
