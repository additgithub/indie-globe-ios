//
//  ReplayCritiques.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 4/23/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "ReplayCritiques.h"
#import "UIColor+CL.h"
#import "AppDelegate.h"
#import "HttpWrapper.h"
#import "ApplicationConstant.h"




@interface ReplayCritiques ()

@end

@implementation ReplayCritiques

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSLog(@"Replay Dict..%@",_dict);
    
    _txt_repaly.delegate=self;
    _txt_repaly.text = @"Enter your message";
    _txt_repaly.textColor = [UIColor lightGrayColor];
    _txt_repaly.layer.borderWidth=1.0;
    _txt_repaly.layer.cornerRadius=10;
    _txt_repaly.layer.borderColor=[UIColor colorWithHex:0x01B5A9].CGColor;
    
    _btn_send.layer.cornerRadius=_btn_send.frame.size.height/2;
    
    NSLog(@"USER ID %@",__mutDict);
    
    _vw_background.layer.cornerRadius=5;
    
    
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [_vw_background addGestureRecognizer:singleFingerTap];
    
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if([textView.text isEqual: @"Enter your message"])
    {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    
    /* ProfileVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileVC"];
     [self.navigationController pushViewController:next animated:NO];*/
    
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}
-(void)CallSendMSG
{
    [APP_DELEGATE showLoadingView:@""];
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:_txt_repaly.text forKey:@"comment"];
    [AddPost setValue:[__mutDict valueForKey:@"CritiquesID"] forKey:@"critique_id"];
    [AddPost setValue:[_dict valueForKey:@"CritiqueReplyID"] forKey:@"critique_reply_id"];
    [AddPost setValue:[_dict valueForKey:@"UserID"] forKey:@"reply_to_id"];//
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    [manager POST:[NSString stringWithFormat:@"%@add_solution_reply",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        [APP_DELEGATE hideLoadingView];
        
        [self dismissViewControllerAnimated:NO completion:nil];
        [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil];
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
              [APP_DELEGATE hideLoadingView];
          }];
    
}



- (IBAction)btn_SEND_A:(id)sender {
    
    if ((![_txt_repaly.text isEqualToString:@"Enter your message"]) && (![_txt_repaly.text isEqualToString:@""]))
    {
        [self CallSendMSG];
        _txt_repaly.text=@"";
        [self performSelector:@selector(handleSingleTap:) withObject:nil afterDelay:0.5];
    }
    
    
    
}
@end
