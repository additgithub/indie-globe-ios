//
//  CritiInfoCell.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/30/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "CritiInfoCell.h"

@implementation CritiInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    
    [self CustomizeTextField:_txt_name];
    [self CustomizeTextField:_txt_email];
    [self CustomizeTextField:_txt_phone];
    //[self commentview:_txt_comment];
    
    _txt_name.placeholder=@"Your Name";
    _txt_email.placeholder=@"Your Email";
    _txt_phone.placeholder=@"Your Phone";
    //_txt_comment.placeholder=@"Your Comment";
    
        
    
    //COMMMENT BOX
    _img_user_img.layer.masksToBounds=YES;
    _img_user_img.layer.cornerRadius=_img_user_img.frame.size.height/2;
    
    _img_prof.layer.masksToBounds=YES;
    _img_prof.layer.cornerRadius=_img_prof.frame.size.height/2;
    
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if([textView.text isEqual: @"Your Comment"])
    {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    return YES;
}
-(void)CustomizeTextField:(UITextField *)myTextField
{
    myTextField.layer.borderWidth=1.0;
    myTextField.layer.borderColor=[UIColor colorWithRed:(160.0f/255.0) green:(160.0f/255.0) blue:(160.0f/255.0) alpha:1].CGColor;
    CGRect frameRect = myTextField.frame;
    frameRect.size.height = 30;
    myTextField.frame = frameRect;
    myTextField.layer.cornerRadius=myTextField.frame.size.height/2;
    
    [myTextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
    myTextField.leftView = paddingView;
    myTextField.leftViewMode = UITextFieldViewModeAlways;

    
}
-(void)commentview:(UITextField *)myTextField
{
    myTextField.layer.borderWidth=1.0;
    myTextField.layer.borderColor=[UIColor colorWithRed:(160.0f/255.0) green:(160.0f/255.0) blue:(160.0f/255.0) alpha:1].CGColor;
    CGRect frameRect = myTextField.frame;
    frameRect.size.height = 110;
    myTextField.frame = frameRect;
    myTextField.layer.cornerRadius=15;
    
    [myTextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
    myTextField.leftView = paddingView;
    myTextField.leftViewMode = UITextFieldViewModeAlways;
    
    
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
