//
//  FollowListVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 3/10/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "FollowListVC.h"
#import "UIColor+CL.h"
#import "FollowListCell.h"
#import "FollowingListCell.h"
#import "ApplicationConstant.h"
#import "Constants.h"
#import "UIImageView+WebCache.h"
#import "AllNotificationVC.h"
#import "ProfileVC.h"
#import "NewProfileVC.h"


@interface FollowListVC ()

@end

@implementation FollowListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    flag=1;
    
    NSLog(@"isf  %@",_isFollow);
    
    _vw_followbtns.layer.cornerRadius=_vw_followbtns.frame.size.height/2;
    _vw_followbtns.layer.borderWidth=1;
    _vw_followbtns.layer.borderColor=[UIColor colorWithHex:0xAFAFAF].CGColor;
    
    
    //_btn_monthly.layer.cornerRadius=_btn_monthly.frame.size.height/2;
    _btn_followers.layer.backgroundColor=[UIColor colorWithHex:0x01B5A9].CGColor;
    [_btn_followers setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //_btn_followers.textColor=[UIColor whiteColor];
    
    //_btn_yearly.layer.cornerRadius=_btn_yearly.frame.size.height/2;
    _btn_following.layer.backgroundColor=[UIColor clearColor].CGColor;
    [_btn_following setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //_btn_following.textColor=[UIColor blackColor];
    
    // Drawing code
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: _btn_followers.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerBottomLeft  cornerRadii: (CGSize){20.0, 20.0}].CGPath;
    _btn_followers.layer.mask = maskLayer;
    
    //[self CallFollower];
    //[self CallFollowing];
    
    
    
    if ([_isFollow isEqualToString:@"2"]) {
        //[self performSelector:@selector(btn_FOLLOWING_A)];
        [self CallFollowing];
        NSLog(@"FOLLOWING... %@",_arrFollowing);
    }
    else
    {
        //[self performSelector:@selector(btn_FOLLOW_A)];
        [self CallFollow];
        NSLog(@"Follow...%@",_arrFollow);
    }
    
    
    _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
    _lbl_notifi_count.clipsToBounds=YES;
    _lbl_notifi_count.text=theAppDelegate.n_Count;
    
    if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
        _lbl_notifi_count.hidden=YES;
    }
    else
    {
        _lbl_notifi_count.hidden=NO;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    //[self CallFollowerList];
    //[self CallFollowingList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////FOLLOWING SCREEN API///////////////////////////////////////////
-(void)CallFollowerList
{
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    //------------
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@get_follower_list",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSLog(@"---%@",responseObject);
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            //_lbl_follow_count.text=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"Total"]];
            
            _arrFollow=[responseObject valueForKey:@"data"];
            //[self.navigationController pushViewController:next animated:YES];
            
            [_tbl_fololist reloadData];
            
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];
    
}
-(void)CallFollowingList
{
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    //------------
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@get_following_list",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSLog(@"---%@",responseObject);
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            //_lbl_following_count.text=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"Total"]];
            
            
            
            _arrFollowing=[responseObject valueForKey:@"data"];
            //[self.navigationController pushViewController:next animated:YES];
            //[AddNew insertObject:originalImage atIndex:1];
            [_tbl_fololist reloadData];
            
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (flag==1) {
        if (_arrFollow.count<1) {
            _tbl_fololist.hidden=YES;
        }
        else
        {
            _tbl_fololist.hidden=NO;
        }
        return _arrFollow.count;
    }
    else
    {
        if (_arrFollowing.count<1) {
            _tbl_fololist.hidden=YES;
        }
        else
        {
            _tbl_fololist.hidden=NO;
        }
        return _arrFollowing.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (flag==1) {
        static NSString *simpleTableIdentifier = @"Cell";
        
        FollowListCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[FollowListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        
        //[cell.img_profile sd_setImageWithURL:[NSURL URLWithString:[[_arrFollow valueForKey:@"UserImageURL"] objectAtIndex:indexPath.row]]];
        
        
        [cell.img_profile sd_setImageWithURL:[NSURL URLWithString:[[_arrFollow valueForKey:@"UserImageURL"] objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
      
       
        
        cell.lbl_usernm.text=[NSString stringWithFormat:@"%@ %@",[[_arrFollow valueForKey:@"FName"]objectAtIndex:indexPath.row],[[_arrFollow valueForKey:@"LName"]objectAtIndex:indexPath.row]];
        
        
        return cell;
    }
    else
    {
        static NSString *simpleTableIdentifier = @"Cell1";
        
        FollowingListCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[FollowingListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        //[cell.img_prof sd_setImageWithURL:[NSURL URLWithString:[[_arrFollowing valueForKey:@"UserImageURL"] objectAtIndex:indexPath.row]]];
        
        [cell.img_prof sd_setImageWithURL:[NSURL URLWithString:[[_arrFollowing valueForKey:@"UserImageURL"] objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
        
        cell.lbl_usernm.text=[NSString stringWithFormat:@"%@ %@",[[_arrFollowing valueForKey:@"FName"]objectAtIndex:indexPath.row],[[_arrFollowing valueForKey:@"LName"]objectAtIndex:indexPath.row]];
        
        
        return cell;
    }
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Change the selected background view of the cell.
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *myID=[standardUserDefaults valueForKey:@"UserID"];
    
    if (flag==1) {
        NSLog(@"Follow");
        
        //[[_arrFollow objectAtIndex:indexPath]valueForKey:@"UserID"]
        
        if ([[[_arrFollow objectAtIndex:indexPath.row]valueForKey:@"UserID"] intValue]==[myID intValue]) {
            NSLog(@"Same User");
            NewProfileVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewProfileVC"];
            [self.navigationController pushViewController:next animated:YES];
        }
        else
        {
            [self CallProfile:indexPath];
        }
        
    }
    else
    {
        NSLog(@"Following");
        if ([[[_arrFollowing objectAtIndex:indexPath.row]valueForKey:@"UserID"] intValue]==[myID intValue]) {
            NSLog(@"Same User");
            NewProfileVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewProfileVC"];
            [self.navigationController pushViewController:next animated:YES];
        }
        else
        {
            [self CallProfile:indexPath];
        }
    }
}

-(void)CallProfile :(NSIndexPath*)indexp
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    
    if (flag==1) {
        NSLog(@"Follow");
        NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
        [AddPost setValue:[[_arrFollow objectAtIndex:indexp.row]valueForKey:@"UserID"] forKey:@"profile_id"];
    }
    else
    {
        NSLog(@"Following");
        NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
        [AddPost setValue:[[_arrFollowing objectAtIndex:indexp.row]valueForKey:@"UserID"] forKey:@"profile_id"];
    }
    //------------
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@user_profile",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        if ([responseObject valueForKey:@"status"]) {
            responseObject=[responseObject valueForKey:@"data"];
            
            ProfileVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileVC"];
            next.mutDict=responseObject;
            [self.navigationController pushViewController:next animated:YES];

        }
        
        [APP_DELEGATE hideLoadingView];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];

}


- (IBAction)btn_FOLLOW_A:(id)sender {
    
    [self CallFollow];
    
    }

- (IBAction)btn_FOLLOWING_A:(id)sender {
    [self CallFollowing];
}

- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_NOTIFY:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}


-(void)CallFollowing
{
    flag=2;
    NSLog(@"_arrFollowing %@",_arrFollowing);
    [_tbl_fololist reloadData];
    //_btn_yearly.layer.cornerRadius=_btn_yearly.frame.size.height/2;
    _btn_following.layer.backgroundColor=[UIColor colorWithHex:0x01B5A9].CGColor;
    [_btn_following setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //_btn_following.textColor=[UIColor whiteColor];
    
    
    //_btn_monthly.layer.cornerRadius=_btn_monthly.frame.size.height/2;
    _btn_followers.layer.backgroundColor=[UIColor clearColor].CGColor;
    [_btn_followers setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //_lbl_monthly.textColor=[UIColor blackColor];
    
    
    // Drawing code
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: _btn_following.bounds byRoundingCorners: UIRectCornerTopRight | UIRectCornerBottomRight  cornerRadii: (CGSize){20.0, 20.0}].CGPath;
    _btn_following.layer.mask = maskLayer;
}

-(void)CallFollow
{
    flag=1;
    
     NSLog(@"_arrFollow %@",_arrFollow);
    [_tbl_fololist reloadData];
    //_btn_monthly.layer.cornerRadius=_btn_monthly.frame.size.height/2;
    _btn_followers.layer.backgroundColor=[UIColor colorWithHex:0x01B5A9].CGColor;
    [_btn_followers setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //_btn_followers.textColor=[UIColor whiteColor];
    
    //_btn_yearly.layer.cornerRadius=_btn_yearly.frame.size.height/2;
    _btn_following.layer.backgroundColor=[UIColor clearColor].CGColor;
    [_btn_following setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //_btn_following.textColor=[UIColor blackColor];
    
    // Drawing code
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: _btn_followers.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerBottomLeft  cornerRadii: (CGSize){20.0, 20.0}].CGPath;
    _btn_followers.layer.mask = maskLayer;

}




@end
