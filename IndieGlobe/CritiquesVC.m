//
//  CritiquesVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/28/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "CritiquesVC.h"
#import <JSQSystemSoundPlayer/JSQSystemSoundPlayer.h>
#import "SWRevealViewController.h"
#import "DeshboardVC.h"
#import "CritiInfoVC.h"
#import "UIColor+CL.h"
#import "PopOverView.h"
#import "WYPopoverController.h"
#import "Constants.h"
#import "DSCollectionViewCell.h"
#import "DSCircularLayout.h"
#import "CritiquesCell.h"
#import "AddCriticsVC.h"
#import "TeamBuild1VC.h"
#import "SVPullToRefresh.h"
#import "NewHomeVC.h"
#import "AllNotificationVC.h"
#import "AllCommentView.h"



#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height
#define ITEM_WIDTH 75
#define ITEM_HEIGHT 75


@interface CritiquesVC ()<WYPopoverControllerDelegate>{
    WYPopoverController* popoverController;
}

@end

@implementation CritiquesVC
{
    
    int count;
    BOOL isShow;
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    // Do any additional setup after loading the view.
    
    //NEW MENU
    page_no=@"0";
    count = 0;
    isShow=false;
    [self setCircularLayout];
    [self customSetup];
    
    
    if ([_fromMyCriti intValue]==1) {
        _collection_ds.hidden=YES;
        _btn_new_menu.hidden=YES;
        _img_beltimage.hidden=YES;
        localapi=@"get_user_critiques";
        _lbl_headernm.text=@"MY CRITIQUES";
    }
    else
    {
        //_collection_ds.hidden=NO;
        _btn_new_menu.hidden=NO;
        _img_beltimage.hidden=NO;
        localapi=@"get_critiques";
        _lbl_headernm.text=@"CRITIQUES";
    }
    
    //CellIdentifier = @"Cell";
    //[Tbl_Vw registerNib:[UINib nibWithNibName:@"BookingHistoryTableViewCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
    //[_collection_view registerNib:[UINib nibWithNibName:@"DSCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:CellIdentifier];
    
    arrimg= [[NSMutableArray alloc]initWithObjects:@"evets4553.png",@"news4557.png",@"critique4552.png",@"classified4551.png",@"learning4556.png",@"indie-pic4555.png",@"global4554.png",@"team4558.png",nil];
    
    arr_temp=[[NSMutableArray alloc]initWithObjects:@"11.jpg",@"12.jpg",@"13.jpg",@"14.png",@"15.png",@"16.png",@"11.jpg",@"12.jpg",@"15.png",@"14.png",@"16.png",nil];
    
    
    _btn_postnew.layer.borderColor=[UIColor colorWithRed:(23.0f/255.0) green:(196.0f/255.0) blue:(199.0f/255.0) alpha:1].CGColor;
    _btn_postnew.layer.cornerRadius=13.0;
    
    [self CustomizeTextField:_txt_serachboc];
    [self CustomizeTextField:txt_country];
    [self CustomizeTextField:txt_state];
    [self CustomizeTextField:txt_city];
    
    
    _txt_serachboc.layer.borderWidth=0.4;
    _txt_serachboc.layer.borderColor=[UIColor lightGrayColor].CGColor;
    
    //btn_search
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.btn_search.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(10.0, 10.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    self.btn_search.layer.mask = maskLayer;
    
    
    
    
    //TEXT CITY
    txt_city.placeholder=@"City";
    txt_country.placeholder=@"Country";
    txt_state.placeholder=@"State";
    
    _txt_serachboc.placeholder=@"Search critiques";
    
    
    
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];
    
    [_btn_search addTarget:self
                    action:@selector(SearchMethod)
          forControlEvents:UIControlEventTouchUpInside];
    
    //============
    /*   self.coll_criti.alwaysBounceVertical = YES;
     
     UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
     refreshControl.tintColor = [UIColor grayColor];
     [refreshControl addTarget:self action:@selector(CallSearchMethod1) forControlEvents:UIControlEventValueChanged];
     [self.coll_criti addSubview:refreshControl];
     self.coll_criti.alwaysBounceVertical = YES;*/
    
    
    [self CallCountry];
    
    _lbl_notifi_count.text=theAppDelegate.n_Count;
    if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
        _lbl_notifi_count.hidden=YES;
        
        
        
    }
    else
    {
        _lbl_notifi_count.hidden=NO;
        
        _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
        _lbl_notifi_count.clipsToBounds=YES;
    }
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    page_no=@"0";
    
    
    [self CallGetCriti];
    [self customSetup];
}


- (void)customSetup
{
    [self.view endEditing:YES];
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if (IsIphone7 || IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = 145;
    }
    else{
        self.revealViewController.rearViewRevealWidth = 145;
    }
    
    
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}
-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

-(void)CustomizeTextField:(UITextField *)myTextField
{
    myTextField.layer.borderWidth=1.0;
    myTextField.layer.borderColor=[UIColor colorWithHex:0x17C4C7].CGColor;
    CGRect frameRect = myTextField.frame;
    frameRect.size.height = 28;
    myTextField.frame = frameRect;
    myTextField.layer.cornerRadius=myTextField.frame.size.height/2;
    
    [myTextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,6, 20)];
    myTextField.leftView = paddingView;
    myTextField.leftViewMode = UITextFieldViewModeAlways;
    
}
-(void)SearchMethod
{
    [self.view endEditing:YES];
    [APP_DELEGATE showLoadingView:@""];
    page_no=@"0";
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",page_no] forKey:@"page_no"];
    //Optional
    
    //[AddPost setValue:[NSString stringWithFormat:@"%@",last_eventid] forKey:@"last_id"];
    if (![_txt_serachboc.text isEqualToString:@""]) {
        [AddPost setValue:_txt_serachboc.text forKey:@"search_text"];
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    
    NSLog(@"PARAM---%@",AddPost);
    NSLog(@"URL --- %@%@",BaseURLAPI,localapi);
    
    [manager POST:[NSString stringWithFormat:@"%@%@",BaseURLAPI,localapi] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        
        if ([[responseObject valueForKey:@"data"] count]==0) {
            mutDict=[[NSMutableArray alloc] init];
            [_tbl_criti.infiniteScrollingView stopAnimating];
            _tbl_criti.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        }
        else
        {
            mutDict=[[NSMutableArray alloc] init];
            mutDict=[responseObject valueForKey:@"data"];
        }
        
        [_tbl_criti reloadData];
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
}
-(void)CallSearchMethod1
{
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",page_no] forKey:@"page_no"];
    //Optional
    
    //[AddPost setValue:[NSString stringWithFormat:@"%@",last_eventid] forKey:@"last_id"];
    if (![_txt_serachboc.text isEqualToString:@""]) {
        [AddPost setValue:_txt_serachboc.text forKey:@"search_text"];
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"PARAM---%@",AddPost);
    NSLog(@"URL --- %@%@",BaseURLAPI,localapi);
    
    [manager POST:[NSString stringWithFormat:@"%@%@",BaseURLAPI,localapi] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        page_no=[responseObject valueForKey:@"page_no"];
        
        if ([[responseObject valueForKey:@"data"] count]>0) {
            
            int64_t delayInSeconds = 2.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                //[_tbl_event beginUpdates];
                NSArray *arr = [responseObject valueForKey:@"data"];
                NSMutableArray *newarr = [[NSMutableArray alloc] init];
                NSLog(@"---%@",newarr);
                for (NSString *str in mutDict) {
                    [newarr addObject:str];
                }
                for (int i=0; i<arr.count; i++) {
                    //[mutDict addObject:arr[i]];
                    [newarr addObject:arr[i]];
                }
                NSLog(@"---%@",newarr);
                mutDict = [[NSMutableArray alloc] init];
                for (NSString*str  in newarr) {
                    [mutDict addObject:str];
                }
                
                
                NSLog(@"--%@",mutDict);
                [_tbl_criti.infiniteScrollingView stopAnimating];
                [_tbl_criti reloadData];
                [APP_DELEGATE hideLoadingView];
                
            });
            
        }
        else
        {
            [_tbl_criti.infiniteScrollingView stopAnimating];
            [APP_DELEGATE hideLoadingView];
            
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
        
    }];
}
#pragma mark - GetAllPosts

-(void)CallGetCriti
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",page_no] forKey:@"page_no"];
    
    //------------
    
    NSLog(@"PARAM ---%@",AddPost);
    NSLog(@"URL --- %@%@",BaseURLAPI,localapi);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@%@",BaseURLAPI,localapi] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        mutDict=[[NSMutableArray alloc] init];
        mutDict=[responseObject valueForKey:@"data"];
        page_no=[responseObject valueForKey:@"page_no"];
        
      

        
        //https://indieglobe.blenzabi.com/web_services/version_1/web_services/get_critiques
        
        [_tbl_criti reloadData];
        [APP_DELEGATE hideLoadingView];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
    
}

-(void)CallCountry
{
    
    
    //Post Method Request
    [APP_DELEGATE showLoadingView:@""];
    NSLog(@" url is %@login",JsonUrlConstant);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpcountry  = [[HttpWrapper alloc] init];
        httpcountry.delegate=self;
        httpcountry.getbool=NO;
        [httpcountry requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_country",JsonUrlConstant] param:nil];
        
        
        
        
    });
}
-(void)Callstate:(NSString *)str
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue:str forKey:@"country_id"];
    NSLog(@"%@",AddPost);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpstate  = [[HttpWrapper alloc] init];
        httpstate.delegate=self;
        httpstate.getbool=NO;
        [httpstate requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_state"
                                                  ,JsonUrlConstant] param:[AddPost copy]];
    });
    
}
-(void)CallCity:(NSString *)str
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue:str forKey:@"state_id"];
    NSLog(@"%@",AddPost);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpcity  = [[HttpWrapper alloc] init];
        httpcity.delegate=self;
        httpcity.getbool=NO;
        [httpcity requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_city"
                                                 ,JsonUrlConstant] param:[AddPost copy]];
    });
    
}



#pragma mark- Wrapper Class Delegate Methods
//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary* )dicsResponse
{
    if(wrapper == httpcriti && httpcriti != nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
        mutDict=[[NSMutableArray alloc] init];
        mutDict=[dicsResponse valueForKey:@"data"];
        
        if ([[dicsResponse valueForKey:@"status"] integerValue] == 0) {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Success" message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //[alert show];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Success" message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //[alert show];
            
            [_tbl_criti reloadData];
            [APP_DELEGATE hideLoadingView];
            
        }
        
        [APP_DELEGATE hideLoadingView];
        
    }
    else if(wrapper == httpcountry && httpcountry != nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        dict=[dicsResponse valueForKey:@"data"];
        country= [dicsResponse valueForKey:@"data"];
        NSLog(@"%@",country);
        [APP_DELEGATE hideLoadingView];
        
    }
    else if (wrapper==httpstate && httpstate !=nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        if ([dicsResponse valueForKey:@"status"]!=0) {
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[dicsResponse valueForKey:@"data"];
            state= [dicsResponse valueForKey:@"data"];
            NSLog(@"%@",state);
        }
        [APP_DELEGATE hideLoadingView];
    }
    else if (wrapper==httpcity && httpcity !=nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        if ([dicsResponse valueForKey:@"status"]!=0) {
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[dicsResponse valueForKey:@"data"];
            arrcity= [dicsResponse valueForKey:@"data"];
            NSLog(@"%@",arrcity);
        }
        [APP_DELEGATE hideLoadingView];
    }
    else if (wrapper==httplike && httplike !=nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        if ([dicsResponse valueForKey:@"status"]!=0) {
            page_no=@"0";
            [self CallGetCriti];
        }
        [APP_DELEGATE hideLoadingView];
    }
    else if (wrapper==httpdislike && httpdislike !=nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        if ([dicsResponse valueForKey:@"status"]!=0) {
            page_no=@"0";
            [self CallGetCriti];
        }
        [APP_DELEGATE hideLoadingView];
    }
    
    
    
    
}


//fetchDataFail
- (void) HttpWrapper:(HttpWrapper* )wrapper fetchDataFail:(NSError* )error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
    
}


-(void)setCircularLayout{
    // your collectionView
    
    if ([[UIScreen mainScreen] bounds].size.height == 736.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(190, 270)
                                radius:180
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:72];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_ds setCollectionViewLayout:circularLayout];
    }
    else  if ([[UIScreen mainScreen] bounds].size.height == 812.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(200, 260)
                                radius:180
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:72];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_ds setCollectionViewLayout:circularLayout];
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 667.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(190, 270)
                                radius:180
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:72];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_ds setCollectionViewLayout:circularLayout];
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 568.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(158, 230)
                                radius:150
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:58];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_ds setCollectionViewLayout:circularLayout];
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 480.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(158, 230)
                                radius:160
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:58];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_ds setCollectionViewLayout:circularLayout];
    }
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return mutDict.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Cell";
    
    CritiquesCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[CritiquesCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    [cell.img_user_image sd_setImageWithURL:[NSURL URLWithString:[mutDict valueForKey:@"UserImageURL"][indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
    
    [cell.img_critic sd_setImageWithURL:[NSURL URLWithString:[mutDict valueForKey:@"PhotoURL"][indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
    
    [cell.img_critic setContentMode:UIViewContentModeScaleAspectFill];
    [cell.img_critic setClipsToBounds:YES];
    
    //cell.img_critic.contentMode = UIViewContentModeScaleAspectFill;
    
    cell.lbl_title.text=[NSString stringWithFormat:@"%@ %@",[[mutDict  valueForKey:@"FName"] objectAtIndex:indexPath.row],[[mutDict  valueForKey:@"LName"] objectAtIndex:indexPath.row]];
    cell.lbl_about_que.text=[NSString stringWithFormat:@"%@",[[mutDict  valueForKey:@"Title"] objectAtIndex:indexPath.row]];
    
    
    
    /*
     //DATE FORMAT
     NSString * yourJSONString = [mutDict valueForKey:@"PostedDate"][indexPath.row];
     NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
     [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
     NSDate *dateFromString = [currentDTFormatter dateFromString:yourJSONString];
     [currentDTFormatter setDateFormat:@"MMMM"];
     NSString *eventDateStr = [currentDTFormatter stringFromDate:dateFromString];
     [currentDTFormatter setDateFormat:@"dd"];
     NSString *onlydt = [currentDTFormatter stringFromDate:dateFromString];
     [currentDTFormatter setDateFormat:@"MMM"];
     NSString *onlymon = [currentDTFormatter stringFromDate:dateFromString];
     
     //cell.lbl_short_mon.text=eventDateStr;
     //cell.lbl_short_dt.text=onlydt;
     cell.lbl_date.text=[NSString stringWithFormat:@"%@ %@",eventDateStr,onlydt];
     */
    
    //===============
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]initWithArray:[[mutDict valueForKey:@"last_comments"] objectAtIndex:indexPath.row]];
    
    for (int i=0; i<arrTemp.count; i++) {
        switch (i) {
            case 0:
            {
                [cell.img_vw1 sd_setImageWithURL:[NSURL URLWithString:[arrTemp valueForKey:@"ImageURL"][0]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
                
                cell.lbl_title_vw1.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"FName"][0]];
                
                cell.lbl_desc_vw1.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"Comments"][0]];
                
            }
                break;
            case 1:
            {
                [cell.img_vw2 sd_setImageWithURL:[NSURL URLWithString:[arrTemp valueForKey:@"ImageURL"][1]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
                
                cell.lbl_title_vw2.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"FName"][1]];
                
                cell.lbl_desc_vw2.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"Comments"][1]];
            }
                break;
                
            default:
                break;
        }
    }
    
    int C=[[[mutDict valueForKey:@"last_comments"] objectAtIndex:indexPath.row] count];
    
    if (C==2) {
        cell.vw_2.hidden=NO;
        cell.vw_1.hidden=NO;
    }
    else if (C==1)
    {
        cell.vw_1.hidden=NO;
        cell.vw_2.hidden=YES;
        
    }
    else
    {
        
        cell.vw_2.hidden=YES;
        cell.vw_1.hidden=YES;
    }
    
    
    
    
    cell.btn_like.tag = indexPath.row;
    [cell.btn_like addTarget:self action:@selector(CallLike:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btn_dislike.tag = indexPath.row;
    [cell.btn_dislike addTarget:self action:@selector(CallDisLike:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.btn_comment setTag:indexPath.row];
    [cell.btn_comment addTarget:self action:@selector(ViewComments:) forControlEvents:UIControlEventTouchUpInside];
    
    
    cell.lbl_like.text=[NSString stringWithFormat:@"%@",[mutDict valueForKey:@"TotalLikes"][indexPath.row]];
    cell.lbl_dislike.text=[NSString stringWithFormat:@"%@",[mutDict valueForKey:@"TotalDislikes"][indexPath.row]];
    cell.lbl_comment.text=[NSString stringWithFormat:@"%@",[mutDict valueForKey:@"TotalComment"][indexPath.row]];
    
    
    
    if ([[mutDict valueForKey:@"IsLike"][indexPath.row] intValue]==0) {
        cell.btn_like.selected=NO;
    }
    else
    {
        cell.btn_like.selected=YES;
    }
    
    if ([[mutDict valueForKey:@"IsDisike"][indexPath.row] intValue]==0) {
        cell.btn_dislike.selected=NO;
    }
    else
    {
        cell.btn_dislike.selected=YES;
    }
    
    //================DATE TIME CALCULATION------------
    cell.lbl_date.text=[APP_DELEGATE DateCovertforClassified:[mutDict valueForKey:@"PostedDate"][indexPath.row]];
    
    
    return cell;
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CritiquesCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.selected) {
        // ... Uncheck
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    _txt_serachboc.text=@"";
    [self.view endEditing:YES];
    
    NSUserDefaults *userD=[[NSUserDefaults alloc]init];
    [userD setValue:@"1" forKey:@"critiComment"];
    
    CritiInfoVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"CritiInfoVC"];
    next.fromMyCriti=_fromMyCriti;
    NSArray *arr = [[NSArray alloc] initWithObjects:mutDict, nil][0];
    NSLog(@"arr %@",arr);
    next.mutDict=[arr objectAtIndex:indexPath.row];
    next.FromFlag=@"1";
    [self.navigationController pushViewController:next animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *simpleTableIdentifier = @"Cell";
    
    CritiquesCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[CritiquesCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    
    int C=[[[mutDict valueForKey:@"last_comments"] objectAtIndex:indexPath.row] count];
    
    if (C==2) {
        
        return 403;
        
    }
    else if (C==1)
    {
        cell.vw_2.hidden=YES;
        return 403-50;
        
        
        
    }
    else
    {
        cell.vw_2.hidden=YES;
        cell.vw_1.hidden=YES;
        
        return 403-100;
        
        
    }
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    int lastRow=[mutDict count]-1;
    if(([indexPath row] == lastRow)&&(lastRow<[mutDict count]))
    {
        //[self Getnewdata];
        NSLog(@"i m call");
        
        if ([mutDict count]>=2) {
            __weak CritiquesVC *weakSelf = self;
            // setup infinite scrolling
            [_tbl_criti addInfiniteScrollingWithActionHandler:^{
                
                if ([_txt_serachboc.text isEqualToString:@""]) {
                    [weakSelf CallSearchMethod1];
                }
                
            }];
        }
        
        /* last_eventid=[[mutDict valueForKey:@"CritiquesID"]lastObject];
         if ([last_eventid integerValue]<=1) {
         _tbl_criti.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
         }*/
        //[self CallSearchMethod];
        
    }
}

-(void)ViewComments:(id)sender
{
    NSLog(@"ViewComments %@",sender);
    //NSInteger ival=[sender integerValue];
    
    UIButton *btn = (UIButton *)sender;
    CGPoint origin = btn.frame.origin;
    CGPoint point = [btn.superview convertPoint:origin toView:_tbl_criti];
    NSIndexPath * indexPath = [_tbl_criti indexPathForRowAtPoint:point];
    
    
    
    AllCommentView *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllCommentView"];
    next.user_portfolio_category_id=[mutDict valueForKey:@"CritiquesID"][indexPath.row];
    next.category_nm=@"Critiques";
    [self.navigationController pushViewController:next animated:NO];
    
    
    
}


-(void)CallLike:(UIButton*)sender
{
    UIButton *btn=sender;
    
    NSLog(@"button was clicked for the following row %li", (long)btn.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[btn tag]  inSection:0];
    NSLog(@"indexpath.row %ld", (long)indexPath.row);
    NSLog(@"indexpath.row %ld", (long)indexPath.section);
    
    
    [APP_DELEGATE showLoadingView:@""];
    
    /*user_id
     indie_pick_id
     is_like */
    
    NSString *strID=[[mutDict valueForKey:@"CritiquesID"] objectAtIndex:indexPath.row];
    NSString *strISLike=[[mutDict valueForKey:@"IsLike"] objectAtIndex:indexPath.row];
    
    if ([strISLike isEqualToString:@"0"]) {
        strISLike=@"1";
    }
    else
    {
        strISLike=@"0";
    }
    
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:strID forKey:@"critiques_id"];
    [AddPost setValue:strISLike forKey:@"is_like"];
    
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httplike  = [[HttpWrapper alloc] init];
        httplike.delegate=self;
        httplike.getbool=NO;
        [httplike requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@critiques_like",JsonUrlConstant] param:[AddPost copy]];
    });
    
}
-(void)CallDisLike:(UIButton*)sender
{
    UIButton *btn=sender;
    
    NSLog(@"button was clicked for the following row %li", (long)btn.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[btn tag]  inSection:0];
    NSLog(@"indexpath.row %ld", (long)indexPath.row);
    NSLog(@"indexpath.row %ld", (long)indexPath.section);
    
    
    [APP_DELEGATE showLoadingView:@""];
    
    /*user_id
     indie_pick_id
     is_like */
    
    NSString *strID=[[mutDict valueForKey:@"CritiquesID"] objectAtIndex:indexPath.row];
    NSString *strISLike=[[mutDict valueForKey:@"IsDisike"] objectAtIndex:indexPath.row];
    
    if ([strISLike isEqualToString:@"0"]) {
        strISLike=@"1";
    }
    else
    {
        strISLike=@"0";
    }
    
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:strID forKey:@"critiques_id"];
    [AddPost setValue:strISLike forKey:@"is_dislike"];
    
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpdislike  = [[HttpWrapper alloc] init];
        httpdislike.delegate=self;
        httpdislike.getbool=NO;
        [httpdislike requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@critiques_dislike",JsonUrlConstant] param:[AddPost copy]];
    });
    
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 8;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [self playAlertSoundPressed:nil];
    DSCollectionViewCell *cell = (DSCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    //cell.layer.cornerRadius=cell.frame.size.height/2;
    //cell.layer.borderWidth=1.5;
    //cell.layer.borderColor=[UIColor whiteColor].CGColor;
    cell.img_icon.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",[arrimg objectAtIndex:indexPath.row]]];
    
    return cell;
    
}
/*
 - (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
 {
 
 if (collectionView==_coll_criti) {
 int lastRow=[mutDict count]-1;
 if(([indexPath row] == lastRow)&&(lastRow<[mutDict count]))
 {
 //[self Getnewdata];
 NSLog(@"i m call");
 
 int last_eventid=[[mutDict valueForKey:@"CritiquesID"]lastObject];
 if (last_eventid > 1) {
 if ([_txt_serachboc.text isEqualToString:@""]) {
 
 [self CallSearchMethod1];
 }
 }
 }
 }
 else
 {
 
 }
 }*/


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self playAlertSoundPressed:nil];
    [self SelectedMenu:indexPath.row];
    
}
- (void)SelectedMenu:(NSInteger)index{
    
    NSLog(@"%ld",(long)index);
    
    switch (index) {
        case 0:
        {
            //Perform any action that u want on menu selection
            EventVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"EventVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            break;
            
        case 1:{
            NewsVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsVC"];
            [self.navigationController pushViewController:next animated:NO];
            
        }
            
            break;
        case 2:
        {
            /* CritiquesVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"CritiquesVC"];
             [self.navigationController pushViewController:next animated:NO];*/
        }
            
            break;
        case 3:
        {
            ClassifiedVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"ClassifiedVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 4:
        {
            LearningVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"LearningVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 5:
        {
            IndieVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"IndieVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 6:
        {
            FundingVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"FundingVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            break;
        case 7:
        {
            TeamBuild1VC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"TeamBuild1VC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            break;
            
            
            break;
        default:
            break;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (IsIphone7 || IsIphone6Plus ) {
        CGFloat height = self.view.frame.size.height;
        CGFloat width  = self.view.frame.size.width;
        return CGSizeMake(width*0.495,height*0.24);
    }
    else{
        CGFloat height = self.view.frame.size.height;
        CGFloat width  = self.view.frame.size.width;
        
        return CGSizeMake(width*0.495,163);
    }
    // in case you you want the cell to be 40% of your controllers view
    // return retval;
}

- (IBAction)btn_POST_NEW:(id)sender {
    
    AddCriticsVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"AddCriticsVC"];
    [self.navigationController pushViewController:next animated:YES];
}

- (IBAction)btn_HOME:(id)sender {
    
    NewHomeVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:YES];
    
}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}

- (IBAction)btn_SCITY:(id)sender {
    isCheck=2;
    [txt_city setTag:2];
    currentTextField = txt_city;
    [self showPopover:sender];
}
- (IBAction)btn_SCOUNTRY:(id)sender {
    isCheck=0;
    [txt_country setTag:0];
    currentTextField = txt_country;
    [self showPopover:sender];
}

- (IBAction)btn_SSTATE:(id)sender {
    isCheck=1;
    [txt_state setTag:1];
    currentTextField = txt_state;
    [self showPopover:sender];
}

- (IBAction)btn_NOTIFY:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}

- (IBAction)btn_NEWMENU:(id)sender {
    [self playAlertSoundPressed:nil];
    if (isShow==false) {
        _collection_ds.hidden=NO;
        _img_colle_back.hidden=NO;
        isShow=true;
        
    }
    else
    {
        _collection_ds.hidden=YES;
        _img_colle_back.hidden=YES;
        isShow=false;
    }
}

- (IBAction)playAlertSoundPressed:(UIButton *)sender
{
    [[JSQSystemSoundPlayer sharedPlayer] playAlertSoundWithFilename:@"Click2-Sebastian"
                                                      fileExtension:kJSQSystemSoundTypeAIFF
                                                         completion:nil];
}

- (void)touchesBegan:(NSSet* )touches withEvent:(UIEvent* )event {
    [self.view endEditing:YES];
}
- (void)showPopover:(UIView*)btn;
{
    
    if (popoverController == nil)
    {
        PopOverView *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PopOverView"];
        
        settingsViewController.index = currentTextField.tag;
        
        if (isCheck == 0) {
            settingsViewController.marrcountry = country;
        }
        else if (isCheck==1)
        {
            settingsViewController.marrstate = state;
        }
        else if (isCheck==2)
        {
            settingsViewController.marrcity = arrcity;
        }
        else{
            NSLog(@"NO DATA FROM API");
        }
        
        
        if ([settingsViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
            settingsViewController.preferredContentSize = CGSizeMake(200, 170);             // iOS 7
        }
        else {
#pragma clang diagnostic pushc
#pragma GCC diagnostic ignored "-Wdeprecated"
            settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 170);      // iOS < 7
#pragma clang diagnostic pop
        }
        settingsViewController.delegates = self;
        
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        
        settingsViewController.modalInPopover = NO;
        popoverController = [[WYPopoverController alloc] initWithContentViewController:settingsViewController];
        
        popoverController.delegate = self;
        
        popoverController.passthroughViews = @[btn];
        popoverController.popoverLayoutMargins = UIEdgeInsetsMake(0, 10, 10 , 10);
        popoverController.wantsDefaultContentAppearance = YES;
        
        [popoverController presentPopoverFromRect:btn.bounds
                                           inView:btn
                         permittedArrowDirections:WYPopoverArrowDirectionAny
                                         animated:YES
                                          options:WYPopoverAnimationOptionFadeWithScale];
    }
    
}

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)dismissPopUpViewController
{
    [popoverController dismissPopoverAnimated:YES];
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)Popvalueselected:(NSString*)strValue{
    NSLog(@"POP:%@",strValue);
    
    
    currentTextField.text = [NSString stringWithFormat:@"%@",strValue];
    
    if (isCheck==0) {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [country filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        countryid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        
        txt_state.text=@"";
        txt_city.text=@"";
        
        [self Callstate:countryid];
        
    }
    else if (isCheck==1)
    {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [state filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        stateid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        txt_city.text=@"";
        [self CallCity:stateid];
    }
    else if (isCheck==2)
    {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [arrcity filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        cityid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        
    }
    else
    {
        currentTextField.text = strValue;
    }
    
    
}




@end
