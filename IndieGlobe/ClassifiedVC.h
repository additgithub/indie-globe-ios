//
//  ClassifiedVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/28/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopOverView.h"
#import "IGCMenu.h"
#import "DeshboardVC.h"
#import "ProfileVC.h"
#import "CritiquesVC.h"
#import "ClassifiedVC.h"
#import "PackageList.h"
#import "LearningVC.h"
#import "EventVC.h"
#import "TeamBuildVC.h"
#import "UIViewController+ENPopUp.h"
#import "MyEventVC.h"
#import "SettingVC.h"
#import "NewProfileVC.h"
#import "IndieVC.h"
#import "FundingVC.h"
#import "NewsVC.h"
#import "HttpWrapper.h"
#import "ApplicationConstant.h"
#import "NewHomeSearchVC.h"



@interface ClassifiedVC : UIViewController<UITableViewDelegate,UITableViewDataSource,HttpWrapperDelegate,IGCMenuDelegate,UICollectionViewDataSource,UICollectionViewDelegate>
{
    UIView *picker;
    UITextField *currentTextField;
    NSMutableArray *arrimg;
    
    
    HttpWrapper* httpclassi,*httpcountry,*httpstate,*httpcity;
    
    NSMutableArray *mutDict;
    NSString *page_no;
    
    //VAR
    int isCheck;
    NSMutableArray* country;
    IBOutlet UITextField *txt_country;
    NSString *countryid;
    
    NSMutableArray* state;
    IBOutlet UITextField *txt_state;
    NSString * stateid;
    
    NSMutableArray* arrcity;
    IBOutlet UITextField *txt_city;
    NSString * cityid;
    
    NSMutableArray* categor;
    IBOutlet UITextField *txt_categ;
    NSString *categorid;
    
    
  


}

@property (strong, nonatomic) IBOutlet UITableView *tbl_classi;

//Outlet
@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;

@property (strong, nonatomic) IBOutlet UIButton *btn_new_menu;

@property (strong, nonatomic) IBOutlet UITextField *txt_shortby;
@property (strong, nonatomic) IBOutlet UITextField *txt_serachbox;
@property (strong, nonatomic) IBOutlet UIButton *btn_search;
@property (strong, nonatomic) IBOutlet UIButton *btn_menu;
@property (strong, nonatomic) IBOutlet UICollectionView *collection_view;
@property (strong, nonatomic) IBOutlet UIButton *btn_add_classi;
@property (strong, nonatomic) IBOutlet UIImageView *img_colle_back;


@property (strong, nonatomic) IBOutlet UIButton *btn_country;
@property (strong, nonatomic) IBOutlet UIButton *btn_state;
@property (strong, nonatomic) IBOutlet UIButton *btn_city;




//Action

- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;
- (IBAction)btn_SCITY:(id)sender;
- (IBAction)btn_SCATEG:(id)sender;
- (IBAction)btn_SSORT:(id)sender;
- (IBAction)btn_SCOUNTRY:(id)sender;
- (IBAction)btn_SSTATE:(id)sender;
- (IBAction)btn_ADD_CLASSIFIED:(id)sender;
- (IBAction)btn_NOTIFY:(id)sender;

- (IBAction)btn_NEWMENU:(id)sender;

-(void)Popvalueselected:(NSString*)strValue;
-(void)Popvalveselected:(NSString*)strValve;
-(void)dismissPopUpViewController;

@end
