//
//  NewHomeSearchVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 6/28/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "NewHomeSearchVC.h"
#import "ApplicationConstant.h"
#import "ApplicationData.h"
#import "Constants.h"
#import "HomeSearchCell.h"
#import "UIImageView+WebCache.h"
#import "NewHomeVC.h"
#import "AllNotificationVC.h"
#import "Constants.h"
#import "DSCircularLayout.h"
#import "DSCollectionViewCell.h"
#import <JSQSystemSoundPlayer/JSQSystemSoundPlayer.h>

//---------------------
#import "DeshboardVC.h"
#import "ProfileVC.h"
#import "CritiquesVC.h"
#import "ClassifiedVC.h"
#import "PackageList.h"
#import "LearningVC.h"
#import "EventVC.h"
#import "TeamBuildVC.h"
#import "UIViewController+ENPopUp.h"
#import "MyEventVC.h"
#import "SettingVC.h"
#import "NewProfileVC.h"
#import "IndieVC.h"
#import "FundingVC.h"
#import "PlatinumInfoVC.h"
#import "TeamBuildVC.h"
#import "TeamBuild1VC.h"
#import "NewsCell.h"
#import "HnewsCell.h"
#import "HcritiCell.h"
#import "HeventCell.h"
#import "HclassiCell.h"
#import "EventDetailsVC.h"
#import "HfundCell.h"
#import "ClassInfoVC.h"
#import "CritiInfoVC.h"
#import "HindiCell.h"
#import "HportCell.h"
#import "ProfilePhotoViewVC.h"
#import "NewHomeSearchVC.h"

#import "YKMediaPlayerKit.h"
#import "YKVimeoVideo.h"
#import "YKYouTubeVideo.h"
#import "YKDirectVideo.h"
#import "WebPlayerVC.h"
#import "AllNotificationVC.h"
#import <Crashlytics/Crashlytics.h>







@interface NewHomeSearchVC ()

@end

@implementation NewHomeSearchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self customSetup];
    
    // Do any additional setup after loading the view.
    arrResponce=[[NSMutableArray alloc]init];
    arrdummy=[[NSMutableArray alloc]init];
    
    [self CustomizeTextField:_txt_search];
    [self textlayers:_txt_search];
    _txt_search.placeholder=@"search for friends.";
    
    _txt_search.delegate=self;
    
    
    
    
    
    [self CallNewHome];
    
    arrdummy=arrResponce;
    
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];
    
    
    
    
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, _Search_View.frame.size.width, _Search_View.frame.size.height+5)];
    searchBar.autoresizingMask =0;
    searchBar.delegate = self;
    searchBar.placeholder = @"Search for Friends";
    searchBar.showsScopeBar = YES;
    searchBar.layer.borderWidth=0.8;
    [searchBar setBackgroundColor:[UIColor clearColor]];
    searchBar.layer.borderWidth = 0.0f;
    searchBar.layer.cornerRadius = searchBar.frame.size.height/2;
    searchBar.layer.borderColor = [UIColor whiteColor].CGColor;
    [searchBar setBarTintColor:[UIColor whiteColor]];//this is what you want
    
    [searchBar setShowsCancelButton:YES animated:YES];
    
    [_Search_View addSubview:searchBar];
    
    UITextField *searchField = [searchBar valueForKey:@"searchField"];
    
    // To change background color
    searchField.backgroundColor = [UIColor whiteColor];
    
    // To change text color
    searchField.textColor = [UIColor blackColor];
    searchField.font = [UIFont fontWithName:@"Lato-Regular" size:15];
    searchField.autocorrectionType = UITextAutocapitalizationTypeAllCharacters;
    // To change placeholder text color
    searchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search for Friends"];
    UILabel *placeholderLabel = [searchField valueForKey:@"placeholderLabel"];
    placeholderLabel.textColor = [UIColor grayColor];
    
    _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
    _lbl_notifi_count.clipsToBounds=YES;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}
- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if (IsIphone7 || IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = 145;
    }
    else{
        self.revealViewController.rearViewRevealWidth = 145;
    }
}
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}

-(void)CallNewHome
{
    
    [APP_DELEGATE showLoadingView:@""];
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"PARAM---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@get_home_screen_people_list",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        if ([responseObject valueForKey:@"status"]) {
            arrResponce=[responseObject valueForKey:@"data"];
        }
        
        
        
        _lbl_notifi_count.text=theAppDelegate.n_Count;
        
        if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
            _lbl_notifi_count.hidden=YES;
        }
        else
        {
            _lbl_notifi_count.hidden=NO;
        }

        
      
        [APP_DELEGATE hideLoadingView];
        
        [_tbl_searchlist reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
        
    }];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isFilterd == YES)
    {
        return filterdarray.count;
    }
    else
    {
        return arrResponce.count;
    }// in your
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Cell";
    //tableView.separatorColor = [UIColor clearColor];
    HomeSearchCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[HomeSearchCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    if (isFilterd == YES)
    {
        cell.lbl_usernm.text=[NSString stringWithFormat:@"%@ %@",[filterdarray valueForKey:@"FName"][indexPath.row],[filterdarray valueForKey:@"LName"][indexPath.row]];
        cell.lbl_profe.text=[NSString stringWithFormat:@"%@",[filterdarray valueForKey:@"ProfessionData"]];
        
        [cell.img_user sd_setImageWithURL:[NSURL URLWithString:[[filterdarray valueForKey:@"ImageURL"] objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
        
        if ([[filterdarray valueForKey:@"IsFollowed"][indexPath.row]isEqualToString:@"0"]) {
            [cell.btn_follow setTitle: @"FOLLOW" forState: UIControlStateNormal];
        }
        else
        {
            [cell.btn_follow setTitle: @"UNFOLLOW" forState: UIControlStateNormal];
        }
        
        ////////////////
        [cell.btn_follow setTag:indexPath.row];
        //  cell.btn_follow.tag = indexPath.row;
        [cell.btn_follow addTarget:self action:@selector(Follow:) forControlEvents:UIControlEventTouchUpInside];
        
        NSString *question=[filterdarray valueForKey:@"ProfessionData"][indexPath.row];
        
        if (question == nil || [question isKindOfClass:[NSNull class]] ) {
            NSLog(@"NULL");
            cell.lbl_profe.text=@"N/A";
        }
        else
        {
            CGSize maximumLabelSize = CGSizeMake(296, FLT_MAX);
            CGSize expectedLabelSize = [question sizeWithFont:cell.lbl_profe.font constrainedToSize:maximumLabelSize lineBreakMode:cell.lbl_profe.lineBreakMode];
            CGRect newFrame = cell.lbl_profe.frame;
            newFrame.size.height = expectedLabelSize.height;
            cell.lbl_profe.frame = newFrame;
            cell.lbl_profe.text=question;
            
        }
        
        
        
        
        hei=cell.lbl_profe.frame.size.height+50;
        
        return cell;
    }else{
    
    cell.lbl_usernm.text=[NSString stringWithFormat:@"%@ %@",[arrResponce valueForKey:@"FName"][indexPath.row],[arrResponce valueForKey:@"LName"][indexPath.row]];
    cell.lbl_profe.text=[NSString stringWithFormat:@"%@",[arrResponce valueForKey:@"ProfessionData"]];
    
    [cell.img_user sd_setImageWithURL:[NSURL URLWithString:[[arrResponce valueForKey:@"ImageURL"] objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
    
    if ([[arrResponce valueForKey:@"IsFollowed"][indexPath.row]isEqualToString:@"0"]) {
        [cell.btn_follow setTitle: @"FOLLOW" forState: UIControlStateNormal];
    }
    else
    {
        [cell.btn_follow setTitle: @"UNFOLLOW" forState: UIControlStateNormal];
    }
    
    ////////////////
     [cell.btn_follow setTag:indexPath.row];
   //  cell.btn_follow.tag = indexPath.row;
     [cell.btn_follow addTarget:self action:@selector(Follow:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *question=[arrResponce valueForKey:@"ProfessionData"][indexPath.row];
        
        if (question == nil || [question isKindOfClass:[NSNull class]] ) {
            NSLog(@"NULL");
            cell.lbl_profe.text=@"N/A";
        }
        else
        {
            CGSize maximumLabelSize = CGSizeMake(296, FLT_MAX);
            CGSize expectedLabelSize = [question sizeWithFont:cell.lbl_profe.font constrainedToSize:maximumLabelSize lineBreakMode:cell.lbl_profe.lineBreakMode];
            CGRect newFrame = cell.lbl_profe.frame;
            newFrame.size.height = expectedLabelSize.height;
            cell.lbl_profe.frame = newFrame;
            cell.lbl_profe.text=question;
            
        }
    
    
    hei=cell.lbl_profe.frame.size.height+50;
    
    return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // some code that compute row's height
    return hei;
}

#pragma mark - Search Methods

-(void)textFieldDidChange:(UITextField*)textField
{
    _txt_search.text = textField.text;
    [self updateSearchArray];
}
//update seach method where the textfield acts as seach bar
-(void)updateSearchArray
{
    if (_txt_search.text.length != 0) {
        arrResponce = [NSMutableArray array];
        for ( NSDictionary* item in arrdummy ) {
            if ([[[item objectForKey:@"FName"] lowercaseString] rangeOfString:[_txt_search.text lowercaseString]].location != NSNotFound) {
                [arrResponce addObject:item];
            }
        }
    } else {
        arrResponce = arrdummy;
    }
    
    [self.tbl_searchlist reloadData];
}



-(void)Follow:(id)sender
{
    NSLog(@"User Like %@",sender);
    //NSInteger ival=[sender integerValue];
    
    UIButton *btn1=sender;
    
    NSLog(@"button was clicked for the following row %li", (long)btn1.tag);
    
    UIButton *btn = (UIButton *)sender;
    CGPoint origin = btn.frame.origin;
    CGPoint point = [btn.superview convertPoint:origin toView:_tbl_searchlist];
    NSIndexPath * indexPath = [_tbl_searchlist indexPathForRowAtPoint:point];
    
    HomeSearchCell *cell=(HomeSearchCell *)[_tbl_searchlist cellForRowAtIndexPath:indexPath];
    
    if ([[arrResponce valueForKey:@"IsFollowed"][(long)btn1.tag] isEqualToString:@"0"]) {
        NSLog(@"Follow");
        
        NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
        NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
        //[AddPost setValue:[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"UserID"]] forKey:@"follower_id"];
        if (isFilterd == YES) {
            [AddPost setValue:[filterdarray valueForKey:@"UserID"][btn1.tag] forKey:@"follower_id"];
        }
        else
        {
            [AddPost setValue:[arrResponce valueForKey:@"UserID"][btn1.tag] forKey:@"follower_id"];
        }
        
        
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
        NSLog(@"---%@",AddPost);
        [manager POST:[NSString stringWithFormat:@"%@user_follow",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            [APP_DELEGATE hideLoadingView];
        }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  NSLog(@"Error: %@", error);
                  [APP_DELEGATE hideLoadingView];
              }];
        
    }
    else
    {
        NSLog(@"unfollow");
        NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
        NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
        //[AddPost setValue:[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"UserID"]] forKey:@"follower_id"];
        
        if (isFilterd == YES) {
            [AddPost setValue:[filterdarray valueForKey:@"UserID"][btn1.tag] forKey:@"follower_id"];

        }
        else
        {
            [AddPost setValue:[arrResponce valueForKey:@"UserID"][btn1.tag] forKey:@"follower_id"];

        }
        
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
        NSLog(@"---%@",AddPost);
        [manager POST:[NSString stringWithFormat:@"%@user_unfollow",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            [APP_DELEGATE hideLoadingView];
        }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  NSLog(@"Error: %@", error);
                  [APP_DELEGATE hideLoadingView];
              }];
        
    }
    double delayInSeconds = 0.8;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSLog(@"Do some work");
        [self CallNewHome];
    });
}



- (IBAction)btn_NOTIF:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}

- (IBAction)btn_HOME:(id)sender {
    
    NewHomeVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:NO];
    
}

- (IBAction)btn_SEARCH:(id)sender {
    
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
    
}

-(void)CustomizeTextField:(UITextField *)myTextField
{
    myTextField.layer.borderWidth=1.0;
    myTextField.layer.borderColor=[UIColor colorWithHex:0x196A84].CGColor;
    
    CGRect frameRect = myTextField.frame;
    frameRect.size.height = 28;
    myTextField.frame = frameRect;
    myTextField.layer.cornerRadius=myTextField.frame.size.height/2;
    
    
    
}

-(void)textlayers :(UITextField *)UITextField
{
    
    //UITextField.layer.borderColor=[[UIColor clearColor]CGColor];
    //UITextField.layer.borderWidth=1.5;
    //UITextField.layer.borderColor=[[UIColor whiteColor] CGColor];
    //UITextField.layer.cornerRadius=15.0;
    [UITextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    UITextField.leftView = paddingView;
    UITextField.leftViewMode = UITextFieldViewModeAlways;
    
}
///searchbar
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    isFilterd = NO;
    [self SearchBarDismiss];
    [_tbl_searchlist reloadData];
}

//SearchBarDismiss
#pragma mark - SearchBar Delegate

- (void)SearchBarDismiss
{
    isFilterd = NO;
    [self.view endEditing:YES];
    //  Btn_Search.hidden = NO;
    // Search_View.hidden = YES;
}

- (void)searchBar:(UISearchBar *)searchBar
    textDidChange:(NSString *)searchText
{
    if (searchText.length == 0)
    {
        isFilterd = NO;
    }
    else
    {
        isFilterd = YES;
        
        filterdarray = [[NSMutableArray alloc]init];
        
        //        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",searchText];
        //      filterdarray = [NSMutableArray arrayWithArray:[marrServicelist   filteredArrayUsingPredicate:predicate]];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"FName CONTAINS[cd] %@ or LName CONTAINS[cd] %@ ",searchText,searchText];
        filterdarray = [NSMutableArray arrayWithArray:[arrResponce   filteredArrayUsingPredicate:predicate]];
        
    }
    [_tbl_searchlist reloadData];
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    //  isFilterd = YES;
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    // isFilterd = NO;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar1
{
    [searchBar1 resignFirstResponder];
}

- (IBAction)btnBack:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
