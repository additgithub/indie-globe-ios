//
//  FundingVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/1/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "FundingVC.h"
#import "SWRevealViewController.h"
#import "DeshboardVC.h"
#import "AddfundVC.h"
#import "Constants.h"
#import "UIColor+CL.h"
#import "FundingCell.h"
#import "GlobeFundPop.h"
#import "DSCircularLayout.h"
#import "DSCollectionViewCell.h"
#import "TeamBuild1VC.h"
#import "PackageList.h"
#import "GlobeFundPop.h"
#import "SVPullToRefresh.h"
#import "NewHomeVC.h"
#import "AllNotificationVC.h"
#import <JSQSystemSoundPlayer/JSQSystemSoundPlayer.h>
#import "AFTPagingBaseViewController.h"







#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height
#define ITEM_WIDTH 75
#define ITEM_HEIGHT 75

@interface FundingVC ()

@end

@implementation FundingVC
{
    int count;
    BOOL isShow;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    [self customSetup];
    
    //NEW MENU
    count = 0;
    isShow=false;
    [self setCircularLayout];
    arrimg= [[NSMutableArray alloc]initWithObjects:@"evets4553.png",@"news4557.png",@"critique4552.png",@"classified4551.png",@"learning4556.png",@"indie-pic4555.png",@"global4554.png",@"team4558.png",nil];

    // Do any additional setup after loading the view.
    _btn_addfunding.layer.cornerRadius=_btn_addfunding.frame.size.height/2;
    
    
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];


    //[self CallGetFundList];
    
}
- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if (IsIphone7 || IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = 145;
    }
    else{
        self.revealViewController.rearViewRevealWidth = 145;
    }
    
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self customSetup];
    page_no=@"0";
    [self CallGetFundList];

   /* [self.collection_view performBatchUpdates:^{
        [self.collection_view insertItemsAtIndexPaths:[NSArray arrayWithObjects:
                                                      [NSIndexPath indexPathForRow:0 inSection:0],
                                                      [NSIndexPath indexPathForRow:1 inSection:0],
                                                      [NSIndexPath indexPathForRow:2 inSection:0],
                                                      [NSIndexPath indexPathForRow:3 inSection:0],
                                                      [NSIndexPath indexPathForRow:4 inSection:0],
                                                      [NSIndexPath indexPathForRow:5 inSection:0],
                                                      [NSIndexPath indexPathForRow:6 inSection:0],
                                                       [NSIndexPath indexPathForRow:7 inSection:0],
                                                      
                                                      nil]];
        count = 8;
    } completion:^(BOOL finished) {
        [self.collection_view reloadData];
    }];*/
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return mutDict.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Cell";
    
    //tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.separatorColor = [UIColor clearColor];
    
    FundingCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[FundingCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    //btn_donatenow
    
    cell.btn_donatenow.tag = indexPath.row;
    [cell.btn_donatenow addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
   
    cell.btn_follow.tag=indexPath.row;
    [cell.btn_follow addTarget:self action:@selector(yourButtonClicked1:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.img_profile sd_setImageWithURL:[NSURL URLWithString:[mutDict valueForKey:@"FRImage"][indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
    
    [cell.img_profile setContentMode:UIViewContentModeScaleAspectFill];
    [cell.img_profile setClipsToBounds:YES];
    
    
    cell.lbl_title.text=[NSString stringWithFormat:@"%@",[[mutDict  valueForKey:@"Title"] objectAtIndex:indexPath.row]];
    
    cell.lbl_desc.text=[NSString stringWithFormat:@"%@",[[mutDict  valueForKey:@"ShortDesc"] objectAtIndex:indexPath.row]];
    cell.lbl_goal_amt.text=[NSString stringWithFormat:@"GOAL:%@",[[mutDict  valueForKey:@"GoalAmount"] objectAtIndex:indexPath.row]];
    cell.lbl_title.text=[NSString stringWithFormat:@"%@",[[mutDict  valueForKey:@"Title"] objectAtIndex:indexPath.row]];
    
    if ([[[mutDict valueForKey:@"IsFollow"] objectAtIndex:indexPath.row]isEqualToString:@"0"]) {
        [cell.btn_follow setTitle: @"FOLLOW" forState: UIControlStateNormal];
    }
    else
    {
        [cell.btn_follow setTitle: @"UNFOLLOW" forState: UIControlStateNormal];
    }
    
    
       /* switch (indexPath.row) {
        case 1:
            cell.img_profile.image=[UIImage imageNamed:@"funding1.png"];
            cell.lbl_title.text=@"Chicks making memories";
            break;
        case 2:
            cell.img_profile.image=[UIImage imageNamed:@"funding2.png"];
            cell.lbl_title.text=@"The iris woodhold chargable trust";
            break;

            
        default:
            cell.img_profile.image=[UIImage imageNamed:@"funding1.png"];
            cell.lbl_title.text=@"Chicks making memories";
            break;
    }*/

    
    
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.selected) {
        // ... Uncheck
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    
    
    NSURL *url = [NSURL URLWithString:[mutDict valueForKey:@"FRImage"][indexPath.row]];
    NSData *data = [NSData dataWithContentsOfURL:url];
    UIImage* immg = [[UIImage alloc]initWithData:data];
    NSArray *landscapeImages = [[NSArray alloc]initWithObjects:immg, nil];
    
    Class cls = NSClassFromString(@"AFTNormalPagingViewController");
    AFTPagingBaseViewController *vc1 = [cls new];
    AFTPagingBaseViewController *bvc = (AFTPagingBaseViewController *)vc1;
    
    bvc.title = @"Photo";
    bvc.images = landscapeImages;
    [APP_DELEGATE hideLoadingView];
    [self presentViewController:bvc animated:YES completion:nil];
    
    
    
}
-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    int lastRow=[mutDict count]-1;
    if(([indexPath row] == lastRow)&&(lastRow<[mutDict count]))
    {
        //[self Getnewdata];
        NSLog(@"i m call");
        
        if ([mutDict count]>=2) {
            __weak FundingVC *weakSelf = self;
            // setup infinite scrolling
            [_tbl_funding addInfiniteScrollingWithActionHandler:^{
                [weakSelf CallSearchMethod1];
            }];
        }
        
        /* last_eventid=[[mutDict valueForKey:@"EventID"]lastObject];
         if ([last_eventid integerValue]<1) {
         _tbl_event.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
         }*/
        //[self CallSearchMethod];
        
    }
}




-(void)yourButtonClicked:(UIButton*)sender
{
    UIButton *btn=sender;
    
    NSLog(@"button was clicked for the following row %li", (long)btn.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[btn tag]  inSection:0];
    NSLog(@"indexpath.row %ld", (long)indexPath.row);
    NSLog(@"indexpath.row %ld", (long)indexPath.section);
    
    NSString *strurl=[[mutDict  valueForKey:@"GFMKSLink"] objectAtIndex:indexPath.row];//FundRaisingID
    
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://www.google.com"]];
    
  // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strurl ]];
    
    
}
-(void)yourButtonClicked1:(UIButton*)sender
{
    UIButton *btn=sender;
    
    NSLog(@"button was clicked for the following row %li", (long)btn.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[btn tag]  inSection:0];
    NSLog(@"indexpath.row %ld", (long)indexPath.row);
    NSLog(@"indexpath.row %ld", (long)indexPath.section);
    
    NSString *strurl=[[mutDict  valueForKey:@"IsFollow"] objectAtIndex:indexPath.row];
    NSString *FRId=[[mutDict  valueForKey:@"FundRaisingID"] objectAtIndex:indexPath.row];
    
    if ([strurl isEqualToString:@"0"]) {
        [self CallFollow :FRId];
    }
    else
    {
        [self CallUnFollow :FRId];
    }
    
    
}


-(void)setCircularLayout{
    if ([[UIScreen mainScreen] bounds].size.height == 736.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(190, 270)
                                radius:180
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:72];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }
    else  if ([[UIScreen mainScreen] bounds].size.height == 812.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(200, 260)
                                radius:180
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:72];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 667.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(190, 270)
                                radius:180
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:72];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 568.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(158, 230)
                                radius:150
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:58];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 480.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(158, 230)
                                radius:160
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:58];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }
}
-(void)CallSearchMethod1
{
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    //Optional
    
    [AddPost setValue:[NSString stringWithFormat:@"%@",page_no] forKey:@"page_no"];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    
    NSLog(@"---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@get_funding_list",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        page_no=[responseObject valueForKey:@"page_no"];
        
        if ([[responseObject valueForKey:@"data"] count]>0) {
            
            int64_t delayInSeconds = 2.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                //[_tbl_event beginUpdates];
                NSArray *arr = [responseObject valueForKey:@"data"];
                NSMutableArray *newarr = [[NSMutableArray alloc] init];
                NSLog(@"---%@",newarr);
                for (NSString *str in mutDict) {
                    [newarr addObject:str];
                }
                for (int i=0; i<arr.count; i++) {
                    //[mutDict addObject:arr[i]];
                    [newarr addObject:arr[i]];
                }
                NSLog(@"---%@",newarr);
                mutDict = [[NSMutableArray alloc] init];
                for (NSString*str  in newarr) {
                    [mutDict addObject:str];
                }
                
                
                NSLog(@"--%@",mutDict);
                [_tbl_funding.infiniteScrollingView stopAnimating];
                [_tbl_funding reloadData];
            });
            
        }
        else
        {
            [_tbl_funding.infiniteScrollingView stopAnimating];
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    
    
}

#pragma mark - GetAllPosts
-(void)CallGetFundList
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpfunding  = [[HttpWrapper alloc] init];
        httpfunding.delegate=self;
        httpfunding.getbool=NO;
        [httpfunding requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_funding_list",JsonUrlConstant] param:[AddPost copy]];
    });
    
}

-(void)CallFollow :(NSString *)FRId
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:FRId forKey:@"fund_raising_id"];//
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpfollow  = [[HttpWrapper alloc] init];
        httpfollow.delegate=self;
        httpfollow.getbool=NO;
        [httpfollow requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@fund_raising_follow",JsonUrlConstant] param:[AddPost copy]];
    });
    
}
-(void)CallUnFollow :(NSString *)FRId
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
     [AddPost setValue:FRId forKey:@"fund_raising_id"];//
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpunfollow  = [[HttpWrapper alloc] init];
        httpunfollow.delegate=self;
        httpunfollow.getbool=NO;
        [httpunfollow requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@fund_raising_unfollow",JsonUrlConstant] param:[AddPost copy]];
    });
    
}

#pragma mark- Wrapper Class Delegate Methods
//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary* )dicsResponse
{
    if(wrapper == httpfunding && httpfunding != nil)
    {
        NSLog(@"DicsResponse ---%@",dicsResponse);
        NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
        mutDict=[[NSMutableDictionary alloc] init];
        mutDict=[dicsResponse valueForKey:@"data"];
        page_no=[dicsResponse valueForKey:@"page_no"];
        
        theAppDelegate.n_Count = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"notification_count"]];
        
        _lbl_notifi_count.text=theAppDelegate.n_Count;
        
        if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
            _lbl_notifi_count.hidden=YES;
            
           
            
        }
        else
        {
            _lbl_notifi_count.hidden=NO;
            
            _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
            _lbl_notifi_count.clipsToBounds=YES;
        }
        
        if (mutDict.count <=0) {
            _tbl_funding.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
            _tbl_funding.hidden=YES;
        }
        else
        {
            [_tbl_funding reloadData];
            
        }
        [APP_DELEGATE hideLoadingView];
    }
    else if (wrapper == httpfollow && httpfollow != nil)
    {
        NSLog(@"DicsResponse ---%@",dicsResponse);
        //NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
        
        if ([[dicsResponse valueForKey:@"status"] intValue]==1) {
            [self CallGetFundList];
        }
        
        [APP_DELEGATE hideLoadingView];
    }
    else if (wrapper == httpunfollow && httpunfollow != nil)
    {
        NSLog(@"DicsResponse ---%@",dicsResponse);
        if ([[dicsResponse valueForKey:@"status"] intValue]==1) {
            [self CallGetFundList];
        }
        
        [APP_DELEGATE hideLoadingView];
    }
}


//fetchDataFail
- (void) HttpWrapper:(HttpWrapper* )wrapper fetchDataFail:(NSError* )error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
    _tbl_funding.hidden=YES;
    [APP_DELEGATE hideLoadingView];
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 8;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    DSCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    //cell.layer.cornerRadius=cell.frame.size.height/2;
    //cell.layer.borderWidth=1.5;
    //cell.layer.borderColor=[UIColor whiteColor].CGColor;
    cell.img_icon.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",[arrimg objectAtIndex:indexPath.row]]];
    [self playAlertSoundPressed:nil];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self playAlertSoundPressed:nil];
    [self SelectedMenu:indexPath.row];
}
- (void)SelectedMenu:(NSInteger)index{
    
    NSLog(@"%ld",(long)index);
    
    switch (index) {
        case 0:
        {
            //Perform any action that u want on menu selection
            EventVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"EventVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            break;
            
        case 1:{
              NewsVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsVC"];
             [self.navigationController pushViewController:next animated:NO];
            
        }
            
            break;
        case 2:
        {
            CritiquesVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"CritiquesVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 3:
        {
            ClassifiedVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"ClassifiedVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 4:
        {
            LearningVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"LearningVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 5:
        {
            IndieVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"IndieVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 6:
        {
           /* FundingVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"FundingVC"];
            [self.navigationController pushViewController:next animated:NO];*/
        }
            break;
        case 7:
        {
             TeamBuild1VC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"TeamBuild1VC"];
             [self.navigationController pushViewController:next animated:NO];
        }
            break;
            
            break;
        default:
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)btn_NEWMENU:(id)sender
{
    [self playAlertSoundPressed:nil];
    if (isShow==false) {
        _collection_view.hidden=NO;
        _img_colle_back.hidden=NO;
        isShow=true;
    }
    else
    {
        _collection_view.hidden=YES;
        _img_colle_back.hidden=YES;
        isShow=false;
    }

}

- (IBAction)playAlertSoundPressed:(UIButton *)sender
{
    [[JSQSystemSoundPlayer sharedPlayer] playAlertSoundWithFilename:@"Click2-Sebastian"
                                                      fileExtension:kJSQSystemSoundTypeAIFF
                                                         completion:nil];
}



- (IBAction)btn_NOTIFY:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}
- (IBAction)btn_HOME:(id)sender {
    
        NewHomeVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
        [self.navigationController pushViewController:next animated:YES];
    
}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}

- (IBAction)btn_ADDFUND:(id)sender {
   
    /*AddfundVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AddfundVC"];
    [self.navigationController pushViewController:next animated:YES];*/
    
//    UIViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"GlobeFundPop"];
//    vc.view.frame = CGRectMake(0, 0, 233.0f, 236.0f);
//    [self presentPopUpViewController:vc];
    GlobeFundPop *homescreen = [self.storyboard instantiateViewControllerWithIdentifier:@"GlobeFundPop"];
   
    homescreen.view.frame = self.view.bounds;
    [homescreen.view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5f]];
    [self.view addSubview:homescreen.view];
    [self addChildViewController:homescreen];
    [homescreen didMoveToParentViewController:self];


}


@end
