//
//  HclassiCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 2/3/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShadowView.h"


@interface HclassiCell : UITableViewCell

//Outlet

@property (strong, nonatomic) IBOutlet UIImageView *img_prof;
@property (strong, nonatomic) IBOutlet UILabel *lbl_usernm;
@property (strong, nonatomic) IBOutlet UIImageView *img_image;
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;
@property (strong, nonatomic) IBOutlet UILabel *lbl_disc;
@property (strong, nonatomic) IBOutlet UILabel *lbl_location;
@property (strong, nonatomic) IBOutlet UILabel *lbl_postdt;
@property (weak, nonatomic) IBOutlet ShadowView *shadow_vw;
@property (weak, nonatomic) IBOutlet UILabel *lbl_sell_price;



@end
