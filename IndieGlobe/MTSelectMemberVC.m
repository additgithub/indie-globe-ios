//
//  MTSelectMemberVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 4/23/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "MTSelectMemberVC.h"
#import "MTSelectMemberCell.h"
#import "NotificationVC.h"

@interface MTSelectMemberVC ()

@end

@implementation MTSelectMemberVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSLog(@"jjhjudhus  %@",_Dict);
    selected=0;
    
    chkArr=[[NSMutableArray alloc]init];
    
    
    for (int i=0; i<[_Dict count]; i++) {
        [chkArr addObject:@"0"];
    }
    
    NSLog(@"%@",chkArr);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _Dict.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Cell";
    tableView.separatorColor = [UIColor clearColor];
    MTSelectMemberCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[MTSelectMemberCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.lbl_usernm.text=[NSString stringWithFormat:@"%@ %@",[[_Dict objectAtIndex:indexPath.row] valueForKey:@"FName"],[[_Dict objectAtIndex:indexPath.row] valueForKey:@"LName"]];
    
    
    [cell.btn_check setTag:indexPath.row];
    [cell.btn_check addTarget:self action:@selector(checkBoxClicked:)forControlEvents:UIControlEventTouchUpInside];
    
    
    
    if ([[chkArr objectAtIndex:indexPath.row] intValue]==0) {
        cell.btn_check.selected=NO;
    }
    else
    {
        cell.btn_check.selected=YES;
    }
    
    if (chkArr.count == selected ) {
        _btn_select_all.selected=YES;
    }
    else
    {
        _btn_select_all.selected=NO;
    }
  
    
    

    return cell;
}
- (void)checkBoxClicked:(id)sender
{
    UIButton *btn=sender;
    NSLog(@"button was clicked for the following row %li", (long)btn.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[btn tag]  inSection:0];
    NSLog(@"indexpath.row %ld", (long)indexPath.row);
    NSLog(@"indexpath.row %ld", (long)indexPath.section);
    
    
    if ([[chkArr objectAtIndex:indexPath.row] intValue]==0) {
        [chkArr replaceObjectAtIndex:indexPath.row withObject:@"1"];
        selected++;
    }
    else
    {
        [chkArr replaceObjectAtIndex:indexPath.row withObject:@"0"];
         selected--;
    }
    
    [_tbl_seelct reloadData];
   
}
- (IBAction)btn_BACK:(id)sender {
     [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)btn_SELECTALL:(id)sender {
  selected=0;
    
    if (_btn_select_all.isSelected==YES) {
        _btn_select_all.selected=NO;
        for (int i=0; i<[chkArr count]; i++) {
            [chkArr replaceObjectAtIndex:i withObject:@"0"];
            selected--;
        }
    }
    else
    {
        _btn_select_all.selected=YES;
        for (int i=0; i<[chkArr count]; i++) {
            [chkArr replaceObjectAtIndex:i withObject:@"1"];
            selected++;
        }
    }

    [_tbl_seelct reloadData];
    
}

- (IBAction)btn_NEXT:(id)sender {
    NotificationVC *homescreen = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationVC"];
    //homescreen.Uid=[_mutDict valueForKey:@"UserID"];
    arrSelected=[[NSMutableArray alloc]init];
    NSString *joinedComponents=[[NSString alloc]init];
    for (int i=0; i<[_Dict count]; i++) {
        if ([[chkArr objectAtIndex:i] intValue]==1) {
            
           
            [arrSelected addObject:[[_Dict objectAtIndex:i] valueForKey:@"UserID"]];
            
            joinedComponents = [arrSelected componentsJoinedByString:@","];
            NSLog(@"joinedComponents  %@",joinedComponents);
            
        }
    }
    
    homescreen.strUserID=joinedComponents;
    homescreen.view.frame = self.view.bounds;
    [homescreen.view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5f]];
    [self.view addSubview:homescreen.view];
    [self addChildViewController:homescreen];
    [homescreen didMoveToParentViewController:self];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    // the user clicked OK
    if (buttonIndex == 0) {
        // do something here...
        [self.navigationController popViewControllerAnimated:YES];
    }
}
@end
