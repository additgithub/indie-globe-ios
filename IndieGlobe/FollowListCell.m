//
//  FollowListCell.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 3/10/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "FollowListCell.h"

@implementation FollowListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _img_profile.clipsToBounds=YES;
    _img_profile.layer.cornerRadius=_img_profile.frame.size.height/2;
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
