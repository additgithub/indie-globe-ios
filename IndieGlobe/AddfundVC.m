//
//  AddfundVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/1/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "AddfundVC.h"
#import "SWRevealViewController.h"
#import "DeshboardVC.h"
#import "WYPopoverController.h"
#import "Constants.h"
#import "NewHomeVC.h"
#import "AllNotificationVC.h"
#import "UIImageView+WebCache.h"
#import "objc/runtime.h"
#import "UIView+WebCacheOperation.h"
#import "MyFundingVC.h"
#import "TextFieldValidator.h"


#define REGEX_EMAIL @"[A-Za-z://]{3,}+\\.[A-Za-z0-9]+\\.[A-Za-z]{2,8}"

//#define REGEX_EMAIL @"^(http(s)?://)?((www)?\.)?[\w]+\.[\w]+"


@interface AddfundVC ()<WYPopoverControllerDelegate>{
    WYPopoverController* popoverController;
}

@end

@implementation AddfundVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
    [self customSetup];
    
    
    
    if ([_isFrom isEqualToString:@"MyFundingVC"]) {
        // Do any additional setup after loading the view.
        [self CustomizeTextField:_txt_title];
        _txt_title.text=[NSString stringWithFormat:@"%@",[_arrDetails valueForKey:@"Title"]];
        //[self commentview:_txt_aboutwork];
        //_txt_aboutwork.placeholder=@"Mention Your Words about how it works?";
        [self CustomizeTextField:_txt_link];
        _txt_link.text=[NSString stringWithFormat:@"%@",[_arrDetails valueForKey:@"GFMKSLink"]];
        [self CustomizeTextField:_txt_goal];
        _txt_goal.text=[NSString stringWithFormat:@"%@",[_arrDetails valueForKey:@"GoalAmount"]];
        [self CustomizeTextField:txt_days];
        txt_days.text=[NSString stringWithFormat:@"%@",[_arrDetails valueForKey:@"Duration"]];
        
        [_img_selectlogo sd_setImageWithURL:[NSURL URLWithString:[_arrDetails valueForKey:@"FRImage"]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
        
        _img_extra.hidden=YES;
        _lbl_extra_lab.hidden=YES;
        _btn_submit.titleLabel.text=@"DONE";
        
        
        ISimage =_img_selectlogo.image;
        
        _btn_submit.layer.cornerRadius=_btn_submit.frame.size.height/2;
        _img_selectlogo.layer.cornerRadius=10;
        
        _txt_aboutwork.delegate=self;
        _txt_aboutwork.text = [NSString stringWithFormat:@"%@",[_arrDetails valueForKey:@"ShortDesc"]];
        _txt_aboutwork.textColor = [UIColor blackColor];
        _txt_aboutwork.layer.borderWidth=1.0;
        _txt_aboutwork.layer.cornerRadius=6;
        _txt_aboutwork.layer.borderColor=[UIColor colorWithRed:(160.0f/255.0) green:(160.0f/255.0) blue:(160.0f/255.0) alpha:1].CGColor;
        _txt_aboutwork.textContainerInset = UIEdgeInsetsMake(5, 7, 0, 0);
        
        [txt_days setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        txt_days.leftView = paddingView;
        txt_days.leftViewMode = UITextFieldViewModeAlways;
        
        
        [_txt_goal setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
        UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        _txt_goal.leftView = paddingView1;
        _txt_goal.leftViewMode = UITextFieldViewModeAlways;
        
        
        ///ADD TAP
        picker = [[UIView alloc] initWithFrame:self.view.bounds];
        picker.backgroundColor=[UIColor blackColor];
        picker.alpha=0.4;
        [self.view addSubview: picker];
        picker.hidden=YES;
        UITapGestureRecognizer *singleFingerTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                                action:@selector(revealToggle:)];
        [picker addGestureRecognizer:singleFingerTap];
        
        //Choose image
        UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
        [singleTap setNumberOfTapsRequired:1];
        [_img_selectlogo addGestureRecognizer:singleTap];
        
        
        _vw_scroll.delegate=self;
        self.vw_scroll.contentSize =CGSizeMake(320, 220);
    }
    else
    {
        // Do any additional setup after loading the view.
        [self CustomizeTextField:_txt_title];
        _txt_title.placeholder=@"Campaign Title";
        //[self commentview:_txt_aboutwork];
        //_txt_aboutwork.placeholder=@"Mention Your Words about how it works?";
        [self CustomizeTextField:_txt_link];
        _txt_link.placeholder=@"Link of Campaign";
        [self CustomizeTextField:_txt_goal];
        _txt_goal.placeholder=@"Goal to achieve";
        [self CustomizeTextField:txt_days];
        txt_days.placeholder=@"30 Days";
        
        _btn_submit.layer.cornerRadius=_btn_submit.frame.size.height/2;
        _img_selectlogo.layer.cornerRadius=10;
        
        _txt_aboutwork.delegate=self;
        _txt_aboutwork.text = @"Add short description";
        _txt_aboutwork.textColor = [UIColor lightGrayColor];
        _txt_aboutwork.layer.borderWidth=1.0;
        _txt_aboutwork.layer.cornerRadius=6;
        _txt_aboutwork.layer.borderColor=[UIColor colorWithRed:(160.0f/255.0) green:(160.0f/255.0) blue:(160.0f/255.0) alpha:1].CGColor;
        _txt_aboutwork.textContainerInset = UIEdgeInsetsMake(5, 7, 0, 0);
        
        [txt_days setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        txt_days.leftView = paddingView;
        txt_days.leftViewMode = UITextFieldViewModeAlways;
        
        
        [_txt_goal setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
        UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        _txt_goal.leftView = paddingView1;
        _txt_goal.leftViewMode = UITextFieldViewModeAlways;
        
        
        ///ADD TAP
        picker = [[UIView alloc] initWithFrame:self.view.bounds];
        picker.backgroundColor=[UIColor blackColor];
        picker.alpha=0.4;
        [self.view addSubview: picker];
        picker.hidden=YES;
        UITapGestureRecognizer *singleFingerTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                                action:@selector(revealToggle:)];
        [picker addGestureRecognizer:singleFingerTap];
        
        //Choose image
        UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
        [singleTap setNumberOfTapsRequired:1];
        [_img_selectlogo addGestureRecognizer:singleTap];
        
        
        _vw_scroll.delegate=self;
        self.vw_scroll.contentSize =CGSizeMake(320, 220);
    }
    
    
    
    
    [self makeDays];
    [self setupAlerts];
   
    
    _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
    _lbl_notifi_count.clipsToBounds=YES;
    _lbl_notifi_count.text=theAppDelegate.n_Count;
    
    if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
        _lbl_notifi_count.hidden=YES;
    }
    else
    {
        _lbl_notifi_count.hidden=NO;
    }
}

-(void)setupAlerts{
    
   
    
    [_txt_link addRegx:REGEX_EMAIL withMsg:@"Enter Valid Link"];
    
   
    
}
- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if (IsIphone7 || IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = 145;
    }
    else{
        self.revealViewController.rearViewRevealWidth = 145;
    }
}
-(void)makeDays
{
    days= [[NSMutableArray alloc]initWithObjects:@"30 Days",@"60 Days",@"90 Days",nil];
    
    NSLog(@"ARR --- %@",days);
}
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}

-(void)singleTapping:(UIGestureRecognizer *)recognizer {
    NSLog(@"image clicked");
    ci= [[UIImagePickerController alloc] init];
    ci.delegate = self;
    ci.allowsEditing=YES;
    ci.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
        [self presentViewController:ci animated:YES completion:nil];
    else
    {
        popover=[[UIPopoverController alloc]initWithContentViewController:ci];
        [popover presentPopoverFromRect:_img_selectlogo.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}

#pragma mark - GetAllPosts
-(void)CallAddFunding
{
    [APP_DELEGATE showLoadingView:@""];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:_txt_title.text forKey:@"title"];
    [AddPost setValue:_txt_aboutwork.text forKey:@"description"];
    [AddPost setValue:_txt_goal.text forKey:@"goal_anmount"];
    [AddPost setValue:_txt_link.text forKey:@"link"];
    [AddPost setValue:txt_days.text forKey:@"duration"];
    
    if (ISimage>0) {
        //Create manager
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
        
        //Now post
        NSLog(@"AddPost %@",AddPost);
        NSString *url = [NSString stringWithFormat:@"%@add_funding",JsonUrlConstant];
        [manager POST:url parameters:[AddPost copy] constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            //add img data one by one
            
            NSData *imageData = UIImageJPEGRepresentation(ISimage,0.5);
            [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"fund_image"] fileName:[NSString stringWithFormat:@"user_image.jpg"] mimeType:@"image/jpeg"];
            
        } success:^(AFHTTPRequestOperation *operation, id dicsResponse) {
            // [(AppDelegate *)[[UIApplication sharedApplication] delegate] hideProgress];
            NSLog(@"Success: %@", dicsResponse);
            NSLog(@"Edit profile---%@",dicsResponse);
            NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            
            if ([[dicsResponse valueForKey:@"status"] integerValue] == 0) {
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Add Data not match" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                //[alert show];
                
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Add Successfull" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                //[alert show];
                [self.navigationController popViewControllerAnimated:YES];
                
            }
            [APP_DELEGATE hideLoadingView];
        }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  NSLog(@"Error: %@", error);
                  [APP_DELEGATE hideLoadingView];
              }];
    }
    else
    {
        [AddPost setValue:@"" forKey:@"fund_image"];
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
            httpaddfund  = [[HttpWrapper alloc] init];
            httpaddfund.delegate=self;
            httpaddfund.getbool=NO;
            [httpaddfund requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@add_funding",JsonUrlConstant] param:[AddPost copy]];
            [APP_DELEGATE hideLoadingView];
        });
        
    }
    
    
    
    
    
}

-(void)CallEditFunding
{
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:_txt_title.text forKey:@"title"];
    [AddPost setValue:_txt_aboutwork.text forKey:@"description"];
    [AddPost setValue:_txt_goal.text forKey:@"goal_anmount"];
    [AddPost setValue:_txt_link.text forKey:@"link"];
    [AddPost setValue:txt_days.text forKey:@"duration"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",[_arrDetails valueForKey:@"FundRaisingID"]] forKey:@"fund_raising_id"];
    
    
    if (ISimage>0) {
        //Create manager
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
        //Now post
        NSLog(@"AddPost %@",AddPost);
        NSString *url = [NSString stringWithFormat:@"%@edit_funding",JsonUrlConstant];
        [manager POST:url parameters:[AddPost copy] constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            //add img data one by one
            
            NSData *imageData = UIImageJPEGRepresentation(ISimage,0.5);
            [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"fund_image"] fileName:[NSString stringWithFormat:@"user_image.jpg"] mimeType:@"image/jpeg"];
            
        } success:^(AFHTTPRequestOperation *operation, id dicsResponse) {
            // [(AppDelegate *)[[UIApplication sharedApplication] delegate] hideProgress];
            NSLog(@"Success: %@", dicsResponse);
            NSLog(@"Edit profile---%@",dicsResponse);
            NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            
            if ([[dicsResponse valueForKey:@"status"] integerValue] == 0) {
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Add Data not match" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                //[alert show];
                
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Add Successfull" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                //[alert show];
                [self.navigationController popViewControllerAnimated:YES];
                
            }
            
        }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  NSLog(@"Error: %@", error);
              }];
    }
    else
    {
        [AddPost setValue:@"" forKey:@"fund_image"];
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
            httpeditfund  = [[HttpWrapper alloc] init];
            httpeditfund.delegate=self;
            httpeditfund.getbool=NO;
            [httpeditfund requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@add_funding",JsonUrlConstant] param:[AddPost copy]];
        });
        
    }
    
}

#pragma mark- Wrapper Class Delegate Methods
//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary* )dicsResponse
{
    if(wrapper == httpaddfund && httpaddfund != nil)
    {
        NSLog(@"---%@",dicsResponse);
        [APP_DELEGATE hideLoadingView];
    }
    else if(wrapper == httpeditfund && httpeditfund != nil)
    {
        NSLog(@"---%@",dicsResponse);
        [APP_DELEGATE hideLoadingView];
    }
}


//fetchDataFail
- (void) HttpWrapper:(HttpWrapper* )wrapper fetchDataFail:(NSError* )error
{
    [APP_DELEGATE hideLoadingView];
    NSLog(@"Fetch Data Fail Error:%@",error);
}
- (void)touchesBegan:(NSSet* )touches withEvent:(UIEvent* )event {
    
    [self.view endEditing:YES];
    //[_vw_scroll endEditing:YES];
    
}

#pragma mark - ImagePickerController Delegate
-(void)imagePickerController:(UIImagePickerController *)pick didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        //[pick dismissViewControllerAnimated:YES completion:nil];
    } else {
        [popover dismissPopoverAnimated:YES];
    }
    //_img_selectlogo.image = [info objectForKey:UIImagePickerControllerOriginalImage];
    _img_extra.hidden=YES;
    _lbl_extra_lab.hidden=YES;
    //ISimage=[info objectForKey:UIImagePickerControllerEditedImage];
    
    UIImageView *tempImg=[[UIImageView alloc]init];
    tempImg.image=[info objectForKey:UIImagePickerControllerEditedImage];
  /*
    _imgCropperViewController = [[UzysImageCropperViewController alloc] initWithImage:tempImg.image andframeSize:ci.view.frame.size andcropSize:CGSizeMake(400, 240)];
    _imgCropperViewController.delegate = self;
    [ci presentViewController:_imgCropperViewController animated:YES completion:nil];
    [_imgCropperViewController release];
   */
    
    ISimage=[info objectForKey:UIImagePickerControllerEditedImage];
    _img_selectlogo.image=tempImg.image;
    //_img_chooseimg.image=image;
    _img_extra.hidden=YES;
    //_lbl_extra_lab.hidden=YES;
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}

- (void)imageCropper:(UzysImageCropperViewController *)cropper didFinishCroppingWithImage:(UIImage *)image
{
    ISimage=image;
    _img_selectlogo.image=image;
    //_img_chooseimg.image=image;
    _img_extra.hidden=YES;
    //_lbl_extra_lab.hidden=YES;
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imageCropperDidCancel:(UzysImageCropperViewController *)cropper
{
    [self dismissViewControllerAnimated:YES completion:nil];
    // [_picker dismissViewControllerAnimated:YES completion:nil];
    
    
    //ISimage=image;
    //_img_selectlogo.image=nil;
    //_img_chooseimg.image=image;
    _img_extra.hidden=NO;
    _lbl_extra_lab.hidden=NO;
    
}


-(void)imagePickerControllerDidCancel:(UIImagePickerController *)pick
{
    [pick dismissViewControllerAnimated:YES completion:nil];
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self customSetup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if([textView.text isEqual: @"Add short description"])
    {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    return YES;
}



-(void)CustomizeTextField:(UITextField *)myTextField
{
    myTextField.layer.borderWidth=1.0;
    myTextField.layer.borderColor=[UIColor colorWithRed:(160.0f/255.0) green:(160.0f/255.0) blue:(160.0f/255.0) alpha:1].CGColor;
    CGRect frameRect = myTextField.frame;
    frameRect.size.height = 24;
    myTextField.frame = frameRect;
    myTextField.layer.cornerRadius=myTextField.frame.size.height/2;
    
    [myTextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
    myTextField.leftView = paddingView;
    myTextField.leftViewMode = UITextFieldViewModeAlways;
    
    
    
    
    
}
-(void)commentview:(UITextField *)myTextField
{
    myTextField.layer.borderWidth=1.0;
    myTextField.layer.borderColor=[UIColor colorWithRed:(160.0f/255.0) green:(160.0f/255.0) blue:(160.0f/255.0) alpha:1].CGColor;
    CGRect frameRect = myTextField.frame;
    frameRect.size.height = 140;
    myTextField.frame = frameRect;
    myTextField.layer.cornerRadius=15;
    
    [myTextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
    myTextField.leftView = paddingView;
    myTextField.leftViewMode = UITextFieldViewModeAlways;
    
    /*UIColor *color = [UIColor lightGrayColor];
     myTextField.attributedPlaceholder =
     [[NSAttributedString alloc] initWithString:@"Placeholder Text"
     attributes:@{
     NSForegroundColorAttributeName: color,
     NSFontAttributeName : [UIFont fontWithName:@"Roboto-Bold" size:10.0]
     }
     ];
     */
    
    
}




- (IBAction)btn_SUBMIT_ACTION:(id)sender {
    
    
    if ([_isFrom isEqualToString:@"MyFundingVC"]) {
        
        if (![_txt_title.text isEqualToString:@""]) {
            if (![_txt_aboutwork.text isEqualToString:@""]) {
                if (![txt_days.text isEqualToString:@""]) {
                    if (![_txt_link.text isEqualToString:@""]&&[_txt_link validate]) {
                        if ([self validateUrl:_txt_link.text]) {
                            if (![_txt_goal.text isEqualToString:@""]) {
                                if ((ISimage!=0)) {
                                    [self CallEditFunding];
                                }
                                else
                                {
                                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please choose Image." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                                    [alert show];
                                }
                            }
                            else
                            {
                                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Add Goal." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                                [alert show];
                            }
                        }
                        else
                        {
                            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Enter Valid Link." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                            [alert show];
                        }
                    }
                    else
                    {
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Add Link" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                        [alert show];
                    }
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Select Days. " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                    [alert show];
                }
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Add your Description. " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Add your Title." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
    }
    else
    {
        if (![_txt_title.text isEqualToString:@""]) {
            if (![_txt_aboutwork.text isEqualToString:@""]&& ![_txt_aboutwork.text isEqualToString:@"Add short description"]) {
                if ((ISimage!=0)) {
                    if (![_txt_link.text isEqualToString:@""]&&[_txt_link validate]) {
                        if ([self validateUrl:_txt_link.text]) {
                            if (![_txt_goal.text isEqualToString:@""]) {
                                if (![txt_days.text isEqualToString:@""]) {
                                    [self CallAddFunding];
                                }
                                else
                                {
                                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Select Days" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                                    [alert show];
                                }
                            }
                            else
                            {
                                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Add Goal" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                                [alert show];
                            }
                        }
                        else
                        {
                            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Enter Valid Link." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                            [alert show];
                        }
                    }
                    else
                    {
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Enter Valid Link" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                        [alert show];
                    }
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Select Image " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                    [alert show];
                }
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Add your Description " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Add your Title" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
    }
}

- (IBAction)btn_HOME:(id)sender {
    NewHomeVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:YES];
    
}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}


- (IBAction)btn_SDAY:(id)sender {
    [self.view endEditing:YES];
    isCheck=4;
    [txt_days setTag:4];
    currentTextField = txt_days;
    [self showPopover:sender];
    
}

- (IBAction)btn_BACK:(id)sender {
    
    if ([_isFrom isEqualToString:@"MyFundingVC"]) {
        MyFundingVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"MyFundingVC"];
        [self.navigationController pushViewController:next animated:YES];
    }
    else
    {
        FundingVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"FundingVC"];
        [self.navigationController pushViewController:next animated:YES];
    }
    
    
    
}

- (IBAction)btn_NOTIFY:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}


- (void)showPopover:(UIView*)btn;
{
    
    if (popoverController == nil)
    {
        PopOverView *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PopOverView"];
        
        settingsViewController.index = currentTextField.tag;
        
        if (isCheck == 4) {
            settingsViewController.marrdays = days;
        }
        
        
        
        if ([settingsViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
            settingsViewController.preferredContentSize = CGSizeMake(200, 170);             // iOS 7
        }
        else {
#pragma clang diagnostic pushc
#pragma GCC diagnostic ignored "-Wdeprecated"
            settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 170);      // iOS < 7
#pragma clang diagnostic pop
        }
        settingsViewController.delegates = self;
        
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        
        settingsViewController.modalInPopover = NO;
        popoverController = [[WYPopoverController alloc] initWithContentViewController:settingsViewController];
        
        popoverController.delegate = self;
        
        popoverController.passthroughViews = @[btn];
        popoverController.popoverLayoutMargins = UIEdgeInsetsMake(0, 10, 10 , 10);
        popoverController.wantsDefaultContentAppearance = YES;
        
        [popoverController presentPopoverFromRect:btn.bounds
                                           inView:btn
                         permittedArrowDirections:WYPopoverArrowDirectionAny
                                         animated:YES
                                          options:WYPopoverAnimationOptionFadeWithScale];
    }
    
}

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    popoverController.delegate = nil;
    popoverController = nil;
}

-(void)dismissPopUpViewController
{
    [popoverController dismissPopoverAnimated:YES];
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)Popvalueselected:(NSString*)strValue{
    NSLog(@"POP:%@",strValue);
    
    currentTextField.text = strValue;
    
}

- (BOOL) validateUrl: (NSString *) candidate {
    NSString *urlRegEx =
    @"((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:candidate];
}

@end
