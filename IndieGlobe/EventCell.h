//
//  EventCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/1/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShadowView.h"


@interface EventCell : UITableViewCell<UITableViewDelegate,UITableViewDataSource>
//Outlet
@property (strong, nonatomic) IBOutlet UIButton *btn_rsvp;
@property (strong, nonatomic) IBOutlet UIButton *btn_scmi;
@property (strong, nonatomic) IBOutlet UIImageView *img_date;
@property (strong, nonatomic) IBOutlet UIImageView *img_main_image;
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;

@property (strong, nonatomic) IBOutlet UILabel *lbl_rsvp;
@property (strong, nonatomic) IBOutlet UILabel *lbl_postdate;
@property (strong, nonatomic) IBOutlet UILabel *lbl_address;

@property (strong, nonatomic) IBOutlet UILabel *lbl_short_mon;
@property (strong, nonatomic) IBOutlet UILabel *lbl_short_dt;
@property (strong, nonatomic) IBOutlet UILabel *lbl_usernm;
@property (strong, nonatomic) IBOutlet UIImageView *img_user_prf_img;


@property (weak, nonatomic) IBOutlet ShadowView *vw_shadow;
@property (weak, nonatomic) IBOutlet UIButton *btn_like;
@property (weak, nonatomic) IBOutlet UIButton *btn_dislike;
@property (weak, nonatomic) IBOutlet UIButton *btn_comment;
@property (weak, nonatomic) IBOutlet UILabel *lbl_like;
@property (weak, nonatomic) IBOutlet UILabel *lbl_dislike;
@property (weak, nonatomic) IBOutlet UILabel *lbl_comment;

@property (weak, nonatomic) IBOutlet UIView *vw_1;
@property (weak, nonatomic) IBOutlet UIImageView *img_vw1;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title_vw1;
@property (weak, nonatomic) IBOutlet UILabel *lbl_desc_vw1;

@property (weak, nonatomic) IBOutlet UIView *vw_2;
@property (weak, nonatomic) IBOutlet UIImageView *img_vw2;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title_vw2;
@property (weak, nonatomic) IBOutlet UILabel *lbl_desc_vw2;
@property (weak, nonatomic) IBOutlet UIScrollView *scroll_imgs;




//Action


@end
