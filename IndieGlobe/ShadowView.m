//
//  ShadowView.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 10/17/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "ShadowView.h"

@implementation ShadowView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)init {
    self = [super init];
    if (self) {
        // [self setupView];
    }
    return self;
}

-(void)setShadow
{
    [self.layer setCornerRadius:5.0f];
    
    // border
    [self.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.layer setBorderWidth:1.5f];
    
    // drop shadow
    [self.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.layer setShadowOpacity:0.8];
    [self.layer setShadowRadius:3.0];
    [self.layer setShadowOffset:CGSizeMake(5.0, 5.0)];
}


@end
