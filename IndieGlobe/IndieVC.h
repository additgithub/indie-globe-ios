//
//  IndieVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/1/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGCMenu.h"
#import "DeshboardVC.h"
#import "ProfileVC.h"
#import "CritiquesVC.h"
#import "ClassifiedVC.h"
#import "PackageList.h"
#import "LearningVC.h"
#import "EventVC.h"
#import "TeamBuildVC.h"
#import "UIViewController+ENPopUp.h"
#import "MyEventVC.h"
#import "SettingVC.h"
#import "NewProfileVC.h"
#import "IndieVC.h"
#import "FundingVC.h"
#import "NewsVC.h"
#import "HttpWrapper.h"
#import "ApplicationConstant.h"
#import "NewHomeSearchVC.h"


@interface IndieVC : UIViewController<UITableViewDelegate,UITableViewDataSource,IGCMenuDelegate,HttpWrapperDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
{
    UIView *picker;
    NSMutableArray *arrimg;
    
    HttpWrapper *httpindie,*httplike,*httpdislike;
    
    NSMutableArray *mutDict;
    NSString *page_no;
    
    UIImage *thumbnail ;
}


@property (strong, nonatomic) IBOutlet UITableView *tbl_indie;

@property (strong, nonatomic) IBOutlet UIButton *btn_menu;
@property (strong, nonatomic) IBOutlet UITextField *txt_search;
@property (strong, nonatomic) IBOutlet UIButton *btn_search;
@property (strong, nonatomic) IBOutlet UIButton *btn_new_menu;
@property (strong, nonatomic) IBOutlet UICollectionView *collection_view;
@property (strong, nonatomic) IBOutlet UIImageView *img_colle_back;

@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;




- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;
- (IBAction)btn_NEWMENU:(id)sender;
- (IBAction)btn_NOTIFY:(id)sender;

@end
