//
//  PDCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/2/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDCell : UITableViewCell
//Outlets
@property (strong, nonatomic) IBOutlet UIImageView *img_profile;
@property (strong, nonatomic) IBOutlet UILabel *lbl_usernm;
@property (strong, nonatomic) IBOutlet UILabel *lbl_prof;
@property (strong, nonatomic) IBOutlet UILabel *lbl_status;
@property (strong, nonatomic) IBOutlet UILabel *lbl_datetime;


//Action


@end
