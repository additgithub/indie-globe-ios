//
//  AppDelegate.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/24/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <OneSignal/OneSignal.h>

#import "AppDelegate.h"
#import "DGActivityIndicatorView.h"
#import "UIColor+CL.h"
#import "iCarousel.h"

#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

#import "MessagesVC.h"
#import "CChatVC.h"
#import "CritiInfoVC.h"
#import "EventDetailsVC.h"

#import "Firebase.h"



@import Firebase;





@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [application setStatusBarHidden:NO];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [FIRApp configure];
    [Fabric with:@[[Crashlytics class]]];
    standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    
    
#warning "Enter your credentials"
    //PayPal Integration
    [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction :PayPalEnvironmentProduction_Key,
                                                           PayPalEnvironmentSandbox :@"ATqUrr5VkejxbFLie-nb15jqYcAVdf4VzQIG9ouC_kHuN_O3ePRvzzFX49VpOaqUmD6vMHBTicuQ11Gk"}];
    ///AVTu4D2bUNkuUffAO3A6rX7d5B7QXSafv1Fg34BCAAytod9xdBRZm9_xyTaVN9pYmspZcBw1zbDBE9Ci
    //--------------------------------------------
    //=============Notofication===================
    //--------------------------------------------
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    [OneSignal setLogLevel:ONE_S_LL_VERBOSE visualLevel:ONE_S_LL_WARN];
    
    // (Optional) - Create block the will fire when a notification is recieved while the app is in focus.
    id notificationRecievedBlock = ^(OSNotification *notification) {
        NSLog(@"Received Notification - %@", notification.payload.notificationID);
        
        
        
        if ([notification.payload.title isEqualToString:@"Message"] || [notification.payload.title isEqualToString:@"Join Team Request"]) {
            
          
            int val=[[standardUserDefaults valueForKey:@"msg_req_count"] intValue];
            [standardUserDefaults setObject:[NSString stringWithFormat:@"%d",val+1] forKey:@"msg_req_count"];
        }
        
    };
    
    // (Optional) - Create block that will fire when a notification is tapped on.
    id notificationReceiverBlock = ^(OSNotification *notification) {
        NSLog(@"Received Notification - %@", notification);
    };
    
    id notificationOpenedBlock = ^(OSNotificationOpenedResult *result) {
        // This block gets called when the user reacts to a notification received
        OSNotificationPayload* payload = result.notification.payload;
        
        NSString* messageTitle = @"OneSignal Example";
        NSString* fullMessage = [payload.body copy];
        NSLog(@"fullMessage - %@", fullMessage);
        
        if (payload.additionalData) {
            
            if(payload.title)
                messageTitle = payload.title;
            
            _additionalData = [payload.additionalData copy];
            
            NSLog(@"hello %@",_additionalData);
        }
        
        if ([payload.title isEqualToString:@"Unfollow"] || [payload.title isEqualToString:@"Follow"]){
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            NewHomeSearchVC *frontViewController = [storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
            MenuViewController *rearViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
            UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:rearViewController];
            
            [self SideBarAdded:rearNavigationController :frontViewController];
        }
        else if ([payload.title isEqualToString:@"Classified Chat"] || [payload.title isEqualToString:@"Classified Chat"]){
            
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            CChatVC *frontViewController = [storyboard instantiateViewControllerWithIdentifier:@"CChatVC"];
            frontViewController.critiID=[_additionalData valueForKey:@"classified_id"];
            frontViewController.isFrom=@"AppDelegate";
            MenuViewController *rearViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
            UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:rearViewController];
            
            [self SideBarAdded:rearNavigationController :frontViewController];
            
            
            
            
        }else if ([payload.title isEqualToString:@"RSVP Event"] || [payload.title isEqualToString:@"SCMI Event"]){
            
        }else if ([payload.title isEqualToString:@"Join Team Request"] || [payload.title isEqualToString:@"Team Invitation"]){
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            MessagesVC *frontViewController = [storyboard instantiateViewControllerWithIdentifier:@"MessagesVC"];
            frontViewController.isfronN=@"1";
            MenuViewController *rearViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
            UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:rearViewController];
            
            [self SideBarAdded:rearNavigationController :frontViewController];
            
        }else if ([payload.title isEqualToString:@"Message"] || [payload.title isEqualToString:@"Message"]){
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            MessagesVC *frontViewController = [storyboard instantiateViewControllerWithIdentifier:@"MessagesVC"];
            frontViewController.isfronN=@"2";
            MenuViewController *rearViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
            UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:rearViewController];
            
            [self SideBarAdded:rearNavigationController :frontViewController];
            
            
        }
        else if ([payload.title isEqualToString:@"Critique Comment Reply"] || [payload.title isEqualToString:@"Critique Comment"]){
            
            [self CallCritiData:[_additionalData valueForKey:@"critique_id"]];
            
        }else if ([payload.title isEqualToString:@"SCMI Event"] || [payload.title isEqualToString:@"RSVP Event"]){
            [self CallEvent:[payload.title valueForKey:@"EventID"]];
        }
        
        
         };
    
    // (Optional) - Configuration options for OneSignal settings.
    id oneSignalSetting = @{kOSSettingsKeyInFocusDisplayOption : @(OSNotificationDisplayTypeNotification), kOSSettingsKeyAutoPrompt : @YES};
    
    
    
    // (REQUIRED) - Initializes OneSignal
    [OneSignal initWithLaunchOptions:launchOptions
                               appId:@"fa821e47-4371-4a23-bf12-1a8c7dab35f0"
          handleNotificationReceived:notificationRecievedBlock
            handleNotificationAction:notificationOpenedBlock
                            settings:oneSignalSetting];
    
     // check for logged in and user id
    
    if ([standardUserDefaults objectForKey:@"UserID"]) {
        
        newRefrence =  [[FIRDatabase database] reference];
        newRefrence =[[newRefrence child:[standardUserDefaults valueForKey:@"UserID"]] child:@"isOnline"];
        [newRefrence setValue:@"1"];
    }
    else
    {
        NSLog(@"user is null");
    }
    
    
    /// else set in login and logout
    
    
    return YES;
}

-(void)SideBarAdded : (UIViewController *)rearNavigationController :(UIViewController *)frontViewController{
    SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:rearNavigationController frontViewController:frontViewController];
    revealController.delegate = self;
    
    self.viewController = revealController;
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    
}

-(void)CallEvent : (NSString*)strID
{
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];//critique_id
    [AddPost setValue:strID forKey:@"event_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@get_single_event",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSMutableDictionary *arrres=[[NSMutableDictionary alloc]init];
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            arrres=[responseObject valueForKey:@"data"];
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            EventDetailsVC *frontViewController = [storyboard instantiateViewControllerWithIdentifier:@"EventDetailsVC"];
            frontViewController.FromFlag=@"1";
            frontViewController.mutDict=arrres;
            
            MenuViewController *rearViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
            UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:rearViewController];
            
            SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:rearNavigationController frontViewController:frontViewController];
            revealController.delegate = self;
            
            self.viewController = revealController;
            self.window.rootViewController = self.viewController;
            [self.window makeKeyAndVisible];
        }
        
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];
}


-(void)CallCritiData : (NSString*)strID
{
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];//critique_id
    [AddPost setValue:strID forKey:@"critique_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@get_single_critique",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSMutableArray *arrres=[[NSMutableArray alloc]init];
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            arrres=[responseObject valueForKey:@"data"];
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            CritiInfoVC *frontViewController = [storyboard instantiateViewControllerWithIdentifier:@"CritiInfoVC"];
            frontViewController.FromFlag=@"1";
            frontViewController.mutDict=arrres;
            
            MenuViewController *rearViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
            UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:rearViewController];
            
            SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:rearNavigationController frontViewController:frontViewController];
            revealController.delegate = self;
            
            self.viewController = revealController;
            self.window.rootViewController = self.viewController;
            [self.window makeKeyAndVisible];
            
            
        }
        
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];
}


-(void)showLoadingView:(NSString *)strMSG
{
    [self hideLoadingView];
    
    UIView *view = [[UIView alloc] initWithFrame:self.window.bounds];
    [view setBackgroundColor:[UIColor blackColor]];
    view.tag = 4444;
    [view setAlpha:0.6];
    
    
    DGActivityIndicatorView *act = [[DGActivityIndicatorView alloc] initWithType:(DGActivityIndicatorAnimationType)DGActivityIndicatorAnimationTypeBallScaleRippleMultiple tintColor:[UIColor colorWithHex:0x01B5A9]];
    [act startAnimating];
    [view addSubview:act];
    act.center = view.center;
    [self.window addSubview:view];
    
    [self performSelector:@selector(hideLoadingView)
               withObject:nil
               afterDelay:11.0];
    
    
}
-(void)hideLoadingView
{
    for(UIView *view in self.window.subviews)
    {
        if(view.tag==4444)
        {
            [view removeFromSuperview];
        }
    }
}


+(AppDelegate *)sharedDelegate
{
    return  (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
#pragma mark - SharedAppDelegate

//SharedAppDelegate
+(AppDelegate *)sharedAppDelegate
{
    
    
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
    
}

#pragma mark -External Property

//applicationDocumentDirectoryString
-(NSString *)applicationDocumentDirectoryString
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return documentsDirectory;
}

- (BOOL)connectedToInternet
{
    NSString *urlString = @"http://www.google.com/";
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    [request setTimeoutInterval:10];
    NSHTTPURLResponse *response;
    
    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error: NULL];
    
    return ([response statusCode] == 200) ? YES : NO;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
      [newRefrence setValue:@"0"];
}



- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
     [newRefrence setValue:@"1"];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
     [newRefrence setValue:@"1"];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
     [newRefrence setValue:@"0"];
    
}

//DateConvert
-(NSString*)DateCovertforClassified:(NSString *)strDate
{
    NSString * yourJSONString = strDate;
    NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
    [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateFromString = [currentDTFormatter dateFromString:yourJSONString];
    [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *eventDateStr = [currentDTFormatter stringFromDate:dateFromString];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *currentDate = [NSDate date];
    NSString *start = [formatter stringFromDate:dateFromString];
    NSString *end = [formatter stringFromDate:currentDate];
    //NSString *end = @"2018-01-11";
    
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd"];
    NSDate *startDate = [f dateFromString:start];
    NSDate *endDate = [f dateFromString:end];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:(NSCalendarUnitDay |NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitWeekOfYear)
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    
    
    NSLog(@"%ld", [components day]);
    
    
    NSLog(@"Day %ld", [components day]);
    NSLog(@"Hour %ld", [components hour]);
    NSLog(@"Minute %ld", [components minute]);
    
    
    if ([components weekOfYear]>0) {
        return [NSString stringWithFormat:@"%ld week ago",(long)[components weekOfYear]];
    }
    else if ([components hour]>0) {
        return [NSString stringWithFormat:@"%ld hours ago",(long)[components hour]];
    }
    else if ([components minute]>0)
    {
        return [NSString stringWithFormat:@"%ld min ago",(long)[components minute]];
    }
    else{
        return [NSString stringWithFormat:@"%ld day ago",(long)[components day]];
    }
    
}

-(NSString*)DateCovertforEvent:(NSString *)strDate
{
    NSString * yourJSONString = strDate;//EndDate
    NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
    [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateFromString = [currentDTFormatter dateFromString:yourJSONString];
    [currentDTFormatter setDateFormat:@"MMM"];
    NSString *eventDateStr = [currentDTFormatter stringFromDate:dateFromString];
    [currentDTFormatter setDateFormat:@"dd"];
    NSString *onlydt = [currentDTFormatter stringFromDate:dateFromString];
    [currentDTFormatter setDateFormat:@"MMMM"];
    NSString *onlymon = [currentDTFormatter stringFromDate:dateFromString];
    
    return [NSString stringWithFormat:@"%@ %@",eventDateStr,onlydt];
}


@end
