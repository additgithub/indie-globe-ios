//
//  MyEventDetailsVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/7/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "MyEventDetailsVC.h"
#import "SWRevealViewController.h"
#import "DeshboardVC.h"
#import "MyEventDetailsCell.h"
#import "Constants.h"
#import "NewsVC.h"
#import "EventVC.h"
#import "NewHomeVC.h"
#import "AllNotificationVC.h"
#import "AFTPagingBaseViewController.h"



@interface MyEventDetailsVC ()

@end

@implementation MyEventDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customSetup];
    self.navigationController.navigationBarHidden=YES;
    // Do any additional setup after loading the view.
    
    _tbl_main.tableHeaderView = _vw_main;
    
    NSLog(@"---%@",_mutdict);
    
    [_img_image sd_setImageWithURL:[NSURL URLWithString:[_mutdict valueForKey:@"EventImageURL"]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
    _lbl_title.text=[NSString stringWithFormat:@"%@",[_mutdict valueForKey:@"Title"]];
    _lbl_rsvp_count.text=[NSString stringWithFormat:@"%@ People RSVP",[_mutdict valueForKey:@"TotalIsRSVP"]];
    _lbl_address.text=[NSString stringWithFormat:@"%@",[_mutdict valueForKey:@"Address"]];
    
    _lbl_desc.text=[NSString stringWithFormat:@"%@",[_mutdict valueForKey:@"EventDetail"]];
    
    
    NSString *startDt=[APP_DELEGATE DateCovertforEvent:[_mutdict valueForKey:@"StartDate"]];
    NSString *endDt=[APP_DELEGATE DateCovertforEvent:[_mutdict valueForKey:@"EndDate"]];
    _lbl_post_date.text=[NSString stringWithFormat:@"%@",startDt,endDt];
    
    
    ////---------------------------
    
    NSMutableArray *image_arr=[[NSMutableArray alloc]init];
    image_arr=[_mutdict valueForKey:@"event_images"];
    
    _scroll_event_img.pagingEnabled=YES;
    int x=0;
    //Load images on ImageView
    NSLog(@"arr  :: %@", image_arr);
    
    if (image_arr.count>0) {
        for (int k=0; k<[image_arr count]; k++)
        {
            
            NSString * strImage = [[image_arr objectAtIndex:k] valueForKey:@"ImageURL"];
            
            strImage = [strImage stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
            UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(x, 0,_scroll_event_img.frame.size.width, _scroll_event_img.frame.size.height)];
            
            
            [img sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:@"avatar"]];
            
            [img setContentMode:UIViewContentModeScaleAspectFit];
            
            x=x+_scroll_event_img.frame.size.width;
            
            [_scroll_event_img addSubview:img];
            
        }
    }
    else
    {
        //https://indieglobe.blenzabi.com/uploads/events/
        
        
        
        NSString * strImage = @"https://indieglobe.blenzabi.com/uploads/events/";
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(x, 0,_scroll_event_img.frame.size.width, _scroll_event_img.frame.size.height)];
        
        
        [img sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
        
        
        [img setContentMode:UIViewContentModeScaleAspectFit];
        
        x=x+_scroll_event_img.frame.size.width;
        
        [_scroll_event_img addSubview:img];
    }
    
    _scroll_event_img.contentSize=CGSizeMake(x, _scroll_event_img.frame.size.height);
    _scroll_event_img.contentOffset=CGPointMake(0, 0);
    
    //-----------------------------
    
    UITapGestureRecognizer *letterTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(highlightLetter:)];
    letterTapRecognizer.numberOfTapsRequired = 1;
    [_scroll_event_img addGestureRecognizer:letterTapRecognizer];
    
    
    
    [self CallMyEventDetils];
    _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
    _lbl_notifi_count.clipsToBounds=YES;
    _lbl_notifi_count.text=theAppDelegate.n_Count;
    
    if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
        _lbl_notifi_count.hidden=YES;
    }
    else
    {
        _lbl_notifi_count.hidden=NO;
    }
    
}

-(void)highlightLetter:(UITapGestureRecognizer*)sender
{
    [APP_DELEGATE showLoadingView:@""];
    NSLog(@"click on image");
    
    [APP_DELEGATE showLoadingView:@""];
    
    
        
        
        NSMutableArray *lImages=[[NSMutableArray alloc]init];
        
        NSMutableArray *image_arr=[[NSMutableArray alloc]init];
        image_arr=[_mutdict valueForKey:@"event_images"];
        
        
        for (int k=0; k<[image_arr count]; k++)
        {
            
            NSString * strImage = [[image_arr objectAtIndex:k] valueForKey:@"ImageURL"];
            
            strImage = [strImage stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
            
            NSURL *url = [NSURL URLWithString:strImage];
            NSData *data = [NSData dataWithContentsOfURL:url];
            UIImage* immg = [[UIImage alloc]initWithData:data];
            
            [lImages addObject:immg];
            
        }
        
        NSArray *landscapeImages = [NSArray arrayWithArray:lImages];
        
        
        Class cls = NSClassFromString(@"AFTNormalPagingViewController");
        AFTPagingBaseViewController *vc = [cls new];
        AFTPagingBaseViewController *bvc = (AFTPagingBaseViewController *)vc;
        
        bvc.title = @"Photo";
        bvc.images = landscapeImages;
        [APP_DELEGATE hideLoadingView];
        [self presentViewController:bvc animated:YES completion:nil];
        
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self customSetup];
}

#pragma mark - GetAllPosts
-(void)CallMyEventDetils
{
    [APP_DELEGATE showLoadingView:@""];
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[_mutdict valueForKey:@"EventID"] forKey:@"event_id"];
    
    
    NSLog(@"---%@",AddPost);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@get_rsvp_scmi_list",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        
        
        _resdict=[[NSMutableArray alloc]init];
        _resdict=[responseObject valueForKey:@"data"];
        
        [_tbl_main reloadData];
        
        [APP_DELEGATE hideLoadingView];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
}

#pragma mark- Wrapper Class Delegate Methods
//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary* )dicsResponse
{
    [APP_DELEGATE hideLoadingView];
    if(wrapper == httpmyevetdetails && httpmyevetdetails != nil)
    {
        
    }
}


//fetchDataFail
- (void) HttpWrapper:(HttpWrapper* )wrapper fetchDataFail:(NSError* )error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}


- (void)customSetup
{
    
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        //[picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if (IsIphone7 || IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = 145;
    }
    else{
        self.revealViewController.rearViewRevealWidth = 145;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _resdict.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Cell";
    
    //tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.separatorColor = [UIColor clearColor];
    
    
    MyEventDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[MyEventDetailsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.lbl_name.text=[NSString stringWithFormat:@"%@",[[_resdict  valueForKey:@"FName"] objectAtIndex:indexPath.row]];
    
    [cell.img_profileimg sd_setImageWithURL:[NSURL URLWithString:[_resdict valueForKey:@"ImageURL"][indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
    
    int r=[[[_resdict valueForKey:@"IsRSVP"] objectAtIndex:indexPath.row] intValue];
    int s=[[[_resdict valueForKey:@"IsSCMI"] objectAtIndex:indexPath.row] intValue];
    
    
    if (r==1) {
        cell.vw_rsvp.hidden=NO;
    }
    else
    {
        cell.vw_rsvp.hidden=YES;
    }
    
    if (s==1) {
        cell.vw_scmi.hidden=NO;
    }
    else
    {
        cell.vw_scmi.hidden=YES;
    }
    
    
    //cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.selected) {
        // ... Uncheck
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    
    
}



- (IBAction)btn_HOME:(id)sender {
    NewHomeVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:YES];
    
}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}


- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_NOTIFY:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}
@end
