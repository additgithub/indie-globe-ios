//
//  MessageChatCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 9/11/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageChatCell : UITableViewCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;


@property (weak, nonatomic) IBOutlet UIView *vw_cell;

@property (weak, nonatomic) IBOutlet UIView *vw_me;
@property (weak, nonatomic) IBOutlet UIView *vw_sender;

@property (weak, nonatomic) IBOutlet UILabel *lbl_me_msg;
@property (weak, nonatomic) IBOutlet UILabel *lbl_me_time;
@property (weak, nonatomic) IBOutlet UILabel *lbl_sender_msg;
@property (weak, nonatomic) IBOutlet UILabel *lbl_sender_time;


@end
