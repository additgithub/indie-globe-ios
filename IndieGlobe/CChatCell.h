//
//  CChatCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 6/29/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CChatCell : UITableViewCell


//Outlet
@property (weak, nonatomic) IBOutlet UIImageView *img_user;
@property (weak, nonatomic) IBOutlet UILabel *lbl_usernm;
@property (weak, nonatomic) IBOutlet UILabel *lbl_msg;
@property (weak, nonatomic) IBOutlet UILabel *lbl_lastmsg;

@property (weak, nonatomic) IBOutlet UILabel *lbl_date;



@end
