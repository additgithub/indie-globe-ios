//
//  ApplicationData.h
//  Towing Service
//
//  Created by RAHUL MEHTA on 03/03/17.
//  Copyright © 2017 RAHUL MEHTA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "AppDelegate.h" //AppDelegate File
#import "ApplicationConstant.h" // Constant File
#include <sys/utsname.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#import <Crashlytics/Crashlytics.h>


@import MobileCoreServices;    // only needed in iOS

@interface ApplicationData : NSObject{
}

@property (retain, nonatomic) NSString *ConsultId;
@property (retain, nonatomic) NSString *DoctorId;
@property (retain, nonatomic) NSString *DoctorCharge;
@property (retain, nonatomic) NSString *DoctorName;

+ (ApplicationData*)sharedInstance;//Create a Delegate

-(BOOL)ValidEmail:(NSString *)checkString; //Check Valid Email Or Not

-(UIView *)GradiantColorUsing :(UIView*)View;

- (IBAction)crashButtonTapped:(id)sender;
+ (id)cleanJsonToObject:(id)data;
@end
