//
//  InviteTCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/4/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InviteTCell : UICollectionViewCell

//Outlet
@property (strong, nonatomic) IBOutlet UIButton *btn_invite;
@property (strong, nonatomic) IBOutlet UILabel *lbl_name;
@property (strong, nonatomic) IBOutlet UIImageView *img_profile;
@property (strong, nonatomic) IBOutlet UILabel *lbl_prof;

@property (weak, nonatomic) IBOutlet UIImageView *img_on_offline;
@property (weak, nonatomic) IBOutlet UILabel *lbl_on_offline;



//Action

- (IBAction)btn_INVITE_ACTION:(id)sender;


@end
