//
//  NewsVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/28/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "HttpWrapper.h"
#import "ApplicationConstant.h"
#import "NewHomeSearchVC.h"

@interface NewsVC : UIViewController<UITableViewDelegate,UITableViewDataSource,HttpWrapperDelegate,MPMediaPickerControllerDelegate,UIScrollViewDelegate>
{
    UIView *picker;
    NSMutableArray *arrimg;
    
    NSMutableArray *teamimg;

    //HTTP
    HttpWrapper* httpnews,*httplike,*httpdislike;
    
    NSURL *url;
    UIImage *thumbnail ;
    NSMutableDictionary *mutDict;
    
    
  //  SoundEffect *soundEffect;
    
}
- (NSMutableDictionary *)dictionaryForQueryString;
@property (nullable, readonly, copy) NSString *query;
@property (strong, nonatomic) AVPlayerViewController *playerViewController;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (strong, nonatomic) IBOutlet UIImageView *img_colle_back;

//Outlets


@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;

@property (strong, nonatomic) IBOutlet UITableView *tbl_news;

@property (strong, nonatomic) IBOutlet UIButton *btn_menu;
@property (strong, nonatomic) IBOutlet UIButton *btn_search;
@property (strong, nonatomic) IBOutlet UITextField *txt_search;

@property (strong, nonatomic) IBOutlet UICollectionView *collectionview;





//Action
- (IBAction)btn_DISLIKE:(id)sender;
- (IBAction)btn_LIKE:(id)sender;


- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;
- (IBAction)btn_SHOW:(id)sender;
- (IBAction)btn_SCROLL:(id)sender;
- (IBAction)ButtonMenu:(id)sender;
- (IBAction)btn_NOTIFY:(id)sender;


- (void)moveObjectAtIndex:(NSUInteger)fromIndex toIndex:(NSUInteger)toIndex;

@end
