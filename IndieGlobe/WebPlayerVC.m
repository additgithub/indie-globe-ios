//
//  WebPlayerVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 3/26/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "WebPlayerVC.h"

@interface WebPlayerVC ()

@end

@implementation WebPlayerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [_web_view loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_strurl]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_BACK:(id)sender {
     [self.navigationController popViewControllerAnimated:YES];
}
@end
