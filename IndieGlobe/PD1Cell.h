//
//  PD1Cell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/26/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PD1Cell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *img_prof_image;
@property (strong, nonatomic) IBOutlet UILabel *lbl_usernm;
@property (strong, nonatomic) IBOutlet UILabel *lbl_prof;
@property (strong, nonatomic) IBOutlet UILabel *lbl_invited;
@property (strong, nonatomic) IBOutlet UILabel *lbl_datetime;



@end
