//
//  ClassiCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/29/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShadowView.h"


@interface ClassiCell : UITableViewCell

//Outlet
@property (strong, nonatomic) IBOutlet UIImageView *img_profile;
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;
@property (strong, nonatomic) IBOutlet UILabel *lbl_short_desc;


@property (strong, nonatomic) IBOutlet UILabel *lbl_address;
@property (strong, nonatomic) IBOutlet UILabel *lbl_days;

@property (strong, nonatomic) IBOutlet UIImageView *img_user_prof;
@property (strong, nonatomic) IBOutlet UILabel *lbl_usernm;
@property (strong, nonatomic) IBOutlet UILabel *cat_nm;
@property (weak, nonatomic) IBOutlet UILabel *lbl_sell_price;

@property (weak, nonatomic) IBOutlet ShadowView *shadow_vw;
@property (weak, nonatomic) IBOutlet UIScrollView *scroll_classi;

//Action



@end
