//
//  LearningUploadVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/30/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopOverView.h"
#import "HttpWrapper.h"
#import "ApplicationConstant.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "NewHomeSearchVC.h"
#import "UzysImageCropperViewController.h"

@interface LearningUploadVC : UIViewController<UzysImageCropperDelegate,UITabBarDelegate,UIImagePickerControllerDelegate,HttpWrapperDelegate,UINavigationControllerDelegate>
{
    UIView *picker;
    UITextField *currentTextField;
    
    UIImagePickerController *ci;
    UIPopoverController *popover;
    
    int isCheck;
    int isVideo;
    NSString* filePath;
    
    NSMutableArray* categor;
    NSString *categorid;
    IBOutlet UITextField *txt_select_cate;
    
    HttpWrapper* httplearningadd;
    
}
@property (nonatomic,retain) UzysImageCropperViewController *imgCropperViewController;
@property (nonatomic,retain) UIImagePickerController *imgpicker;
//Outlet
//@property (strong, nonatomic) IBOutlet UITextField *txt_select_cate;
@property (strong, nonatomic) IBOutlet UITextField *txt_topic;

@property (strong, nonatomic) IBOutlet UITextField *txt_link;
@property (strong, nonatomic) IBOutlet UIButton *btn_submitpost;
@property (strong, nonatomic) IBOutlet UIButton *btn_submitpost1;

@property (strong, nonatomic) IBOutlet UIButton *btn_menu;
@property (strong, nonatomic) IBOutlet UIImageView *img_select_img;
@property (strong, nonatomic) IBOutlet UITextView *txt_workVW;
@property (strong, nonatomic) IBOutlet UIImageView *img_picture;
@property (strong, nonatomic) IBOutlet UIImageView *img_video;
@property (strong, nonatomic) IBOutlet UIView *vw_picture;
@property (strong, nonatomic) IBOutlet UIView *vw_video;
@property (strong, nonatomic) IBOutlet UIImageView *img_extra;

@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;



//Action

- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;
- (IBAction)btn_PICTURE:(id)sender;
- (IBAction)btn_VIDEO:(id)sender;
- (IBAction)btn_SCATEG:(id)sender;
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_SUBMIT_POST:(id)sender;
- (IBAction)btn_NOTIFY:(id)sender;


-(void)Popvalueselected:(NSString*)strValue;
-(void)Popvalveselected:(NSString*)strValve;
-(void)dismissPopUpViewController;


@end
