//
//  circleVW.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/15/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "circleVW.h"
#import "DSCircularLayout.h"
#import "DSCollectionViewCell.h"

#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height
#define ITEM_WIDTH 50
#define ITEM_HEIGHT 50

@interface circleVW (){
    int count;
    BOOL isShow;
}

@end

@implementation circleVW

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    count = 0;
    isShow=false;
    
    [self setCircularLayout];
    arrimg=[[NSMutableArray alloc]initWithObjects:@"event-icon.png",@"critiques.png",@"myteam.png",@"message-icon.png",@"upload-icon.png",@"myteam.png",@"myteam.png", nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self.collectionView performBatchUpdates:^{
        [self.collectionView insertItemsAtIndexPaths:[NSArray arrayWithObjects:
                                                      [NSIndexPath indexPathForRow:0 inSection:0],
                                                      [NSIndexPath indexPathForRow:1 inSection:0],
                                                      [NSIndexPath indexPathForRow:2 inSection:0],
                                                      [NSIndexPath indexPathForRow:3 inSection:0],
                                                      [NSIndexPath indexPathForRow:4 inSection:0],
                                                      [NSIndexPath indexPathForRow:5 inSection:0],
                                                      [NSIndexPath indexPathForRow:6 inSection:0],
                                                       /*[NSIndexPath indexPathForRow:7 inSection:0],
                                                       [NSIndexPath indexPathForRow:8 inSection:0],
                                                       [NSIndexPath indexPathForRow:9 inSection:0],
                                                       [NSIndexPath indexPathForRow:10 inSection:0],
                                                       [NSIndexPath indexPathForRow:11 inSection:0],
                                                       [NSIndexPath indexPathForRow:12 inSection:0],
                                                       [NSIndexPath indexPathForRow:13 inSection:0],
                                                       [NSIndexPath indexPathForRow:14 inSection:0],
                                                       [NSIndexPath indexPathForRow:15 inSection:0],
                                                       [NSIndexPath indexPathForRow:16 inSection:0],
                                                       [NSIndexPath indexPathForRow:17 inSection:0],
                                                       [NSIndexPath indexPathForRow:18 inSection:0],
                                                       [NSIndexPath indexPathForRow:19 inSection:0],*/
                                                      nil]];
        count = 7;
    } completion:^(BOOL finished) {
        [self.collectionView reloadData];
    }];
}
-(void)setCircularLayout{
   /* DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
    [circularLayout initWithCentre:CGPointMake(190, 235)
                            radius:180
                          itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                 andAngularSpacing:90];
    [circularLayout setStartAngle:160 endAngle:0];
    circularLayout.mirrorX = NO;
    circularLayout.mirrorY = NO;
    circularLayout.rotateItems = YES;
    
    circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    [self.collectionView setCollectionViewLayout:circularLayout];*/
    
   /* DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
    [circularLayout initWithCentre:CGPointMake(180, 180)
                            radius:120
                          itemSize:CGSizeMake(50, 50)
                 andAngularSpacing:30];
    [circularLayout setStartAngle:90 endAngle:220];
    circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    [_collectionView setCollectionViewLayout:circularLayout];*/
    
    DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
    [circularLayout initWithCentre:CGPointMake(190, 240)
                            radius:175
                          itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                 andAngularSpacing:90];
    [circularLayout setStartAngle:M_PI endAngle:0];
    
    //circularLayout.mirrorX = NO;
    //circularLayout.mirrorY = NO;
    circularLayout.rotateItems = YES;
    circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    [self.collectionView setCollectionViewLayout:circularLayout];
    
    

     // your collectionView
    
    
    
    
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    DSCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    //cell.lbl.text = [NSString stringWithFormat:@"%d",(int)indexPath.item + 1];
    cell.img_icon.image=[UIImage imageNamed:@"myteam.png"];
    //cell.img_icon.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",[arrimg objectAtIndex:indexPath.row]]];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat width = 320/2-20;
    return CGSizeMake(width, 50.0);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    CGFloat totalCellWidth = 50;
    CGFloat totalSpacingWidth = 7;
    CGFloat leftInset = (50 - (totalCellWidth + totalSpacingWidth)) / 2;
    CGFloat rightInset = leftInset;
    UIEdgeInsets sectionInset = UIEdgeInsetsMake(0, leftInset, 0, rightInset);
    return sectionInset;

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)btn_BACK_BTN:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_SHOWMENU:(id)sender {
    
    if (isShow==false) {
        _collectionView.hidden=NO;
        isShow=true;
    }
    else
    {
        _collectionView.hidden=YES;
        isShow=false;

    }
    
    
    
}
@end
