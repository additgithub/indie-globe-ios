//
//  EventDetailsVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/28/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "EventDetailsVC.h"
#import "NewsVC.h"
#import "SWRevealViewController.h"
#import "ProfileCell.h"
#import "DeshboardVC.h"
#import "Constants.h"
#import "UIColor+CL.h"
#import "EditProfileVC.h"
#import "MessagePopVC.h"
#import "NewHomeVC.h"
#import "AllNotificationVC.h"
#import "AFTPagingBaseViewController.h"




@interface EventDetailsVC ()

@end

@implementation EventDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden=YES;
    
    [self customSetup];
    
    _btn_rsvp.layer.cornerRadius=_btn_rsvp.frame.size.height/2;
    _btn_scmi.layer.cornerRadius=_btn_scmi.frame.size.height/2;
    
    _img_prof_image.clipsToBounds=YES;
    _img_prof_image.layer.cornerRadius=_img_prof_image.frame.size.height/2;
    
    
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];
    
    
    NSLog(@"----%@",_mutDict);
    
    if ([_FromFlag isEqualToString:@"AllNotificationVC"]) {
        [_img_profile sd_setImageWithURL:[NSURL URLWithString:[_mutDict valueForKey:@"UserImageURL"]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
    }
    else if ([_FromFlag integerValue]==1) {
        [_img_profile sd_setImageWithURL:[NSURL URLWithString:[_mutDict valueForKey:@"EventImageURL"]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
    }else
    {
        [_img_profile sd_setImageWithURL:[NSURL URLWithString:[_mutDict valueForKey:@"Image"]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
    }
    
    
    _lbl_title.text=[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"Title"]];
    _lbl_address.text=[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"Address"]];
    _txt_text_info.text=[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"EventDetail"]];
    _lbl_rsvp_count.text=[NSString stringWithFormat:@"%@ People RSVP",[_mutDict  valueForKey:@"TotalIsRSVP"]];
    
    NSLog(@"----%@",_mutDict);
    
    
    //------------------------
    
    if ([_FromFlag integerValue]==1) {
        
        
        NSMutableArray *image_arr=[[NSMutableArray alloc]init];
        image_arr=[_mutDict valueForKey:@"event_images"];
        
        _scroll_event_img.pagingEnabled=YES;
        
        int x=0;
        
        //Load images on ImageView
        NSLog(@"arr  :: %@", image_arr);
        
        
        for (int k=0; k<[image_arr count]; k++)
        {
            
            NSString * strImage = [[image_arr objectAtIndex:k] valueForKey:@"ImageURL"];
            
            strImage = [strImage stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
            UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(x, 0,_scroll_event_img.frame.size.width, _scroll_event_img.frame.size.height)];
            
            
            [img sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:@"avatar"]];
            
            [img setContentMode:UIViewContentModeScaleAspectFit];
            
            
            
            x=x+_scroll_event_img.frame.size.width;
            
            [_scroll_event_img addSubview:img];
            
        }
        
        
        _scroll_event_img.contentSize=CGSizeMake(x, _scroll_event_img.frame.size.height);
        _scroll_event_img.contentOffset=CGPointMake(0, 0);
        
        
    }
    else
    {
        NSMutableArray *image_arr=[[NSMutableArray alloc]init];
        image_arr=[_mutDict valueForKey:@"event_images"];
        
        _scroll_event_img.pagingEnabled=YES;
        
        int x=0;
        
        //Load images on ImageView
        NSLog(@"arr  :: %@", image_arr);
        
        
        for (int k=0; k<[image_arr count]; k++)
        {
            
            NSString * strImage = [[image_arr objectAtIndex:k] valueForKey:@"ImageURL"];
            
            strImage = [strImage stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
            UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(x, 0,_scroll_event_img.frame.size.width, _scroll_event_img.frame.size.height)];
            
            
            [img sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:@"avatar"]];
            
            [img setContentMode:UIViewContentModeScaleAspectFit];
            
            
            
            x=x+_scroll_event_img.frame.size.width;
            
            [_scroll_event_img addSubview:img];
            
        }
        _scroll_event_img.contentSize=CGSizeMake(x, _scroll_event_img.frame.size.height);
        _scroll_event_img.contentOffset=CGPointMake(0, 0);
    }
    
    
    //------------------------
    
    
    
    _lbl_usernm.text=[NSString stringWithFormat:@"%@ %@",[_mutDict valueForKey:@"FName"],[_mutDict valueForKey:@"LName"]];
    [_img_prof_image sd_setImageWithURL:[NSURL URLWithString:[_mutDict valueForKey:@"UserImageURL"]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
    
    /////////////////////////
    
    NSString *startDt=[APP_DELEGATE DateCovertforEvent:[_mutDict valueForKey:@"StartDate"]];
    NSString *endDt=[APP_DELEGATE DateCovertforEvent:[_mutDict valueForKey:@"EndDate"]];
    
    _lbl_posted_on.text=[NSString stringWithFormat:@"%@ - %@",startDt,endDt];
    
    
    
    if ([[_mutDict valueForKey:@"IsRSVP"] isEqualToString:@"1"]) {
        //cell.btn_rsvp.layer.backgroundColor=[UIColor colorWithHex:0xAAAAAA].CGColor;
        _btn_rsvp.alpha=0.40;
        _btn_rsvp.enabled=NO;
    }
    else
    {
        _btn_rsvp.alpha=1.00;
        _btn_rsvp.enabled=YES;
    }
    
    if ([[_mutDict valueForKey:@"IsSCMI"] isEqualToString:@"1"]) {
        //cell.btn_rsvp.layer.backgroundColor=[UIColor colorWithHex:0xAAAAAA].CGColor;
        _btn_scmi.alpha=0.40;
        _btn_scmi.enabled=NO;
    }
    else
    {
        _btn_scmi.alpha=1.00;
        _btn_scmi.enabled=YES;
    }
    
    
    ///////////Zoom view
    UITapGestureRecognizer *letterTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(highlightLetter:)];
    letterTapRecognizer.numberOfTapsRequired = 1;
    [_scroll_event_img addGestureRecognizer:letterTapRecognizer];
    
    
    _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
    _lbl_notifi_count.clipsToBounds=YES;
    _lbl_notifi_count.text=theAppDelegate.n_Count;
    
    if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
        _lbl_notifi_count.hidden=YES;
    }
    else
    {
        _lbl_notifi_count.hidden=NO;
    }
}


-(void)highlightLetter:(UITapGestureRecognizer*)sender
{

    
    
    [APP_DELEGATE showLoadingView:@""];
    
    NSLog(@"click on image");
    
    
    
    if ([_FromFlag isEqualToString:@"AllNotificationVC"]) {
        
        
        NSMutableArray *lImages=[[NSMutableArray alloc]init];
        
        NSMutableArray *image_arr=[[NSMutableArray alloc]init];
        image_arr=[_mutDict valueForKey:@"event_images"];
        
        
        for (int k=0; k<[image_arr count]; k++)
        {
            
            NSString * strImage = [[image_arr objectAtIndex:k] valueForKey:@"ImageURL"];
            
            strImage = [strImage stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
            
            NSURL *url = [NSURL URLWithString:strImage];
            NSData *data = [NSData dataWithContentsOfURL:url];
            UIImage* immg = [[UIImage alloc]initWithData:data];
            
            [lImages addObject:immg];
            
        }
        
        NSArray *landscapeImages = [NSArray arrayWithArray:lImages];
        
        
        Class cls = NSClassFromString(@"AFTNormalPagingViewController");
        AFTPagingBaseViewController *vc = [cls new];
        AFTPagingBaseViewController *bvc = (AFTPagingBaseViewController *)vc;
        
        bvc.title = @"Photo";
        bvc.images = landscapeImages;
        [APP_DELEGATE hideLoadingView];
        [self presentViewController:bvc animated:YES completion:nil];
        
    }
    else if ([_FromFlag integerValue]==1) {
        NSMutableArray *lImages=[[NSMutableArray alloc]init];
        
        NSMutableArray *image_arr=[[NSMutableArray alloc]init];
        image_arr=[_mutDict valueForKey:@"event_images"];
        
        
        for (int k=0; k<[image_arr count]; k++)
        {
            
            NSString * strImage = [[image_arr objectAtIndex:k] valueForKey:@"ImageURL"];
            
            strImage = [strImage stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
            
            NSURL *url = [NSURL URLWithString:strImage];
            NSData *data = [NSData dataWithContentsOfURL:url];
            UIImage* immg = [[UIImage alloc]initWithData:data];
            
            [lImages addObject:immg];
            
        }
        
        NSArray *landscapeImages = [NSArray arrayWithArray:lImages];
        
        
        Class cls = NSClassFromString(@"AFTNormalPagingViewController");
        AFTPagingBaseViewController *vc = [cls new];
        AFTPagingBaseViewController *bvc = (AFTPagingBaseViewController *)vc;
        
        bvc.title = @"Photo";
        bvc.images = landscapeImages;
        [APP_DELEGATE hideLoadingView];
        [self presentViewController:bvc animated:YES completion:nil];
        
    }else
    {
        NSMutableArray *lImages=[[NSMutableArray alloc]init];
        
        NSMutableArray *image_arr=[[NSMutableArray alloc]init];
        image_arr=[_mutDict valueForKey:@"event_images"];
        
        
        for (int k=0; k<[image_arr count]; k++)
        {
            
            NSString * strImage = [[image_arr objectAtIndex:k] valueForKey:@"ImageURL"];
            
            strImage = [strImage stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
            NSURL *imageURL = [NSURL URLWithString:strImage];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    // Update the UI
                   // self.imageView.image = [UIImage imageWithData:imageData];
                    
                    UIImage* immg = [[UIImage alloc]initWithData:imageData];
                    [lImages addObject:immg];
                });
            });
            
           
            
        }
        
        NSArray *landscapeImages = [NSArray arrayWithArray:lImages];
        
        
        Class cls = NSClassFromString(@"AFTNormalPagingViewController");
        AFTPagingBaseViewController *vc = [cls new];
        AFTPagingBaseViewController *bvc = (AFTPagingBaseViewController *)vc;
        
        bvc.title = @"Photo";
        bvc.images = landscapeImages;
        [APP_DELEGATE hideLoadingView];
        [self presentViewController:bvc animated:YES completion:nil];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self customSetup];
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if (IsIphone7 || IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = 145;
    }
    else{
        self.revealViewController.rearViewRevealWidth = 145;
    }
}
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}

#pragma mark - GetAllPosts
-(void)CallMyMethod
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    [AddPost setValue:@"Admin" forKey:@"email"];
    [AddPost setValue:@"Admin" forKey:@"password"];
    
    NSLog(@" url is %@login",JsonUrlConstant);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpeventdetails  = [[HttpWrapper alloc] init];
        httpeventdetails.delegate=self;
        httpeventdetails.getbool=NO;
        [httpeventdetails requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@login",JsonUrlConstant] param:[AddPost copy]];
    });
}

#pragma mark- Wrapper Class Delegate Methods
//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary* )dicsResponse
{
    [APP_DELEGATE hideLoadingView];
    if(wrapper == httpeventdetails && httpeventdetails != nil)
    {
        
    }
}


//fetchDataFail
- (void) HttpWrapper:(HttpWrapper* )wrapper fetchDataFail:(NSError* )error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)btn_HOME:(id)sender {
    NewHomeVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:YES];
    
}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}


- (IBAction)btn_RSVP_ACTION:(id)sender {
    
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    if ([_FromFlag integerValue]==2) {
        [AddPost setValue:[_mutDict valueForKey:@"ID"] forKey:@"event_id"];
    }else
    {
        [AddPost setValue:[_mutDict valueForKey:@"EventID"] forKey:@"event_id"];
    }
    
    
    
    NSString *strRSVP=[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"IsRSVP"]];
    
    if ([strRSVP isEqualToString:@"0"]) {
        [AddPost setValue:@"1" forKey:@"is_rsvp"];
    }
    else
    {
        [AddPost setValue:@"0" forKey:@"is_rsvp"];
    }
    
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@rsvp_event",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        
        _lbl_rsvp_count.text=[NSString stringWithFormat:@"%@ People RSVP",[responseObject valueForKey:@"total_rsvp"]];
        
        
        _btn_rsvp.alpha=0.40;
        _btn_rsvp.enabled=NO;
        _btn_rsvp.userInteractionEnabled=YES;
        _btn_scmi.alpha=1.00;
        _btn_scmi.enabled=YES;
        
        [APP_DELEGATE hideLoadingView];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
    
}

- (IBAction)btn_SCMI_ACTION:(id)sender {
    
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    if ([_FromFlag integerValue]==2) {
        [AddPost setValue:[_mutDict valueForKey:@"ID"] forKey:@"event_id"];
    }else
    {
        [AddPost setValue:[_mutDict valueForKey:@"EventID"] forKey:@"event_id"];
    }
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@scmi_event",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        _lbl_rsvp_count.text=[NSString stringWithFormat:@"%@ People RSVP",[responseObject valueForKey:@"total_rsvp"]];
        
        _btn_rsvp.alpha=1.00;
        _btn_rsvp.enabled=YES;
        _btn_scmi.userInteractionEnabled=YES;
        _btn_scmi.alpha=0.40;
        _btn_scmi.enabled=NO;
        
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
    
}

- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)btn_NOTIFY:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}


@end
