//
//  AllNotificationVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 4/25/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "AllNotificationVC.h"
#import "AllNotifCell.h"
#import "HttpWrapper.h"
#import "ApplicationData.h"
#import "ApplicationConstant.h"
#import "UIImageView+WebCache.h"
#import "MessagesVC.h"
#import "NewHomeSearchVC.h"
#import "CChatVC.h"
#import "CritiInfoVC.h"
#import "EventDetailsVC.h"
#import "FundingVC.h"
#import "TeamBuild1VC.h"
#import "AllCommentView.h"







@interface AllNotificationVC ()

@end

@implementation AllNotificationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    Dict=[[NSMutableDictionary alloc]init];
    
    [self GetNotificationList];
    
    
    theAppDelegate.n_Count=@"0";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return Dict.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Cell";
    //tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.separatorColor = [UIColor clearColor];
    AllNotifCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[AllNotifCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.txt_title.text=[Dict valueForKey:@"NotificationName"][indexPath.row];
    cell.txt_desc.text=[Dict valueForKey:@"NotificationDesc"][indexPath.row];
    
    NSString *strurl=[NSString stringWithFormat:@"%@",[Dict valueForKey:@"UserImageURL"][indexPath.row]];
    [cell.img_prof sd_setImageWithURL:[NSURL URLWithString:strurl]
                      placeholderImage:[UIImage imageNamed:@"Avatar"]];
    
    
    
    if ([[Dict valueForKey:@"is_read"][indexPath.row] intValue]==1) {
        cell.vw.backgroundColor=[UIColor colorWithHex:0xFFFFFF];
    }
    else
    {
        cell.vw.backgroundColor=[UIColor colorWithHex:0xf9f9f9];
    }
    
    
    
    
    
    //////////AUTO SPACING
    //[cell.txt_desc setNumberOfLines:0];
    //[cell.txt_desc sizeToFit];
    
    /*CGRect autosize4 = cell.txt_desc.frame;
    autosize4.size.height = [self heightForText:[NSString stringWithFormat:@"%@",[Dict valueForKey:@"NotificationDesc"][indexPath.row]]];
    cell.txt_desc.frame = autosize4;
    hei=cell.txt_desc.frame.size.height;*/
    
    hei=[self getLabelHeight:cell.txt_desc];
    CGRect autosize4 = cell.txt_desc.frame;
    autosize4.size.height = hei;
    cell.txt_desc.frame = autosize4;
    
    
    //================DATE TIME CALCULATION------------
    
    NSString * yourJSONString = [NSString stringWithFormat:@"%@",[[Dict  valueForKey:@"CreatedOn"] objectAtIndex:indexPath.row]];
    
    NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
    [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateFromString = [currentDTFormatter dateFromString:yourJSONString];
    [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *eventDateStr = [currentDTFormatter stringFromDate:dateFromString];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *currentDate = [NSDate date];
    NSString *start = [formatter stringFromDate:dateFromString];
    NSString *end = [formatter stringFromDate:currentDate];
    //NSString *end = @"2018-01-11";
    
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *startDate = [f dateFromString:start];
    NSDate *endDate = [f dateFromString:end];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:(NSCalendarUnitDay |NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitWeekOfYear)
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    
    NSLog(@"Day %ld", [components day]);
    NSLog(@"Hour %ld", [components hour]);
    NSLog(@"Minute %ld", [components minute]);
    
    if ([components weekOfYear]>0) {
        cell.txt_postdt.text=[NSString stringWithFormat:@"%ld week %ld day %ld hour %ld min ago",(long)[components weekOfYear],(long)[components hour],(long)[components minute]];
    }
    else if ([components day]>0) {
        cell.txt_postdt.text=[NSString stringWithFormat:@"%ld day %ld hour %ld min ago",(long)[components day],(long)[components hour],(long)[components minute]];
    }
    else if ([components hour]>0)
    {
        cell.txt_postdt.text=[NSString stringWithFormat:@"%ld hour %ld min ago",(long)[components hour],(long)[components minute]];
    }
    else{
        cell.txt_postdt.text=[NSString stringWithFormat:@"%ld day ago",(long)[components minute]];
    }
    
    
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    static NSString *simpleTableIdentifier = @"Cell";

    tableView.separatorColor = [UIColor clearColor];
    AllNotifCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[AllNotifCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    if ([[Dict valueForKey:@"NotificationName"][indexPath.row]isEqualToString:@"Team Invitation"]) {
        NSLog(@"GOTO Team Invitation");

        MessagesVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"MessagesVC"];
        next.isfronN=@"1";
        [self.navigationController pushViewController:next animated:NO];
        
    }else if ([[Dict valueForKey:@"NotificationName"][indexPath.row]isEqualToString:@"Message"]) {
        NSLog(@"Message");
        
        MessagesVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"MessagesVC"];
        next.isfronN=@"2";
        [self.navigationController pushViewController:next animated:NO];
        
    }
    else if ([[Dict valueForKey:@"NotificationName"][indexPath.row]isEqualToString:@"Unfollow"]||[[Dict valueForKey:@"NotificationName"][indexPath.row]isEqualToString:@"Follow"])
    {
        NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
        [self.navigationController pushViewController:next animated:NO];
       
    }
    else if ([[Dict valueForKey:@"NotificationName"][indexPath.row]isEqualToString:@"Classified Chat"])
    {
        CChatVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"CChatVC"];
        next.critiID=[Dict valueForKey:@"ClassfiedID"][indexPath.row];
        next.isFrom=@"AllNotificationVC";
        [self.navigationController pushViewController:next animated:NO];

    }
    else if([[Dict valueForKey:@"NotificationName"][indexPath.row]isEqualToString:@"Critique Comment Reply"]||[[Dict valueForKey:@"NotificationName"][indexPath.row]isEqualToString:@"Critique Comment"])//Critique Comment
    {
        [self CallCritiData:[Dict valueForKey:@"CritiquesID"][indexPath.row]];
       // CritiInfoVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"CritiInfoVC"];
        //next.isFromNoti=@"1";
        //[self.navigationController pushViewController:next animated:NO];
    }else if([[Dict valueForKey:@"NotificationName"][indexPath.row]isEqualToString:@"SCMI Event"]||[[Dict valueForKey:@"NotificationName"][indexPath.row]isEqualToString:@"RSVP Event"])//Critique Comment
    {
        [self CallEvent:[Dict valueForKey:@"EventID"][indexPath.row]];
        
    }else if([[Dict valueForKey:@"NotificationName"][indexPath.row]isEqualToString:@"Event Comment"])//Critique Comment
    {
        NSLog(@"Event Comment");
         [self CallEvent:[Dict valueForKey:@"EventID"][indexPath.row]];
        // CritiInfoVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"CritiInfoVC"];
        //next.isFromNoti=@"1";
        //[self.navigationController pushViewController:next animated:NO];
        //Follow Your Fund Raising
    }else if([[Dict valueForKey:@"NotificationName"][indexPath.row]isEqualToString:@"Follow Your Fund Raising"]||[[Dict valueForKey:@"NotificationName"][indexPath.row]isEqualToString:@"UnFollow Your Fund Raising"])
    {
        
        FundingVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"FundingVC"];
        [self.navigationController pushViewController:next animated:NO];
        
    }else if([[Dict valueForKey:@"NotificationName"][indexPath.row]isEqualToString:@"Join Team Request"])
    {
        
        TeamBuild1VC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"TeamBuild1VC"];
        [self.navigationController pushViewController:next animated:NO];
       
    }else if([[Dict valueForKey:@"NotificationName"][indexPath.row]isEqualToString:@"Portfolio Comment"])
    {
        AllCommentView *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllCommentView"];
        next.user_portfolio_category_id=[NSString stringWithFormat:@"%@",[[Dict valueForKey:@"AppUserPortdolioID"] objectAtIndex:indexPath.row]];
        next.category_nm=@"Portfolio";
        [self.navigationController pushViewController:next animated:NO];
    }
    
    
}

-(void)CallCritiData : (NSString*)strID
{
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];//critique_id
    [AddPost setValue:strID forKey:@"critique_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@get_single_critique",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSMutableArray *arrres=[[NSMutableArray alloc]init];
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            arrres=[responseObject valueForKey:@"data"];
            
             CritiInfoVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"CritiInfoVC"];
            next.FromFlag=@"AllNotificationVC";
            next.mutDict=arrres;
            [self.navigationController pushViewController:next animated:NO];
            
            
        }
        
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];
}


-(void)CallEvent : (NSString*)strID
{
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];//critique_id
    [AddPost setValue:strID forKey:@"event_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@get_single_event",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSMutableDictionary *arrres=[[NSMutableDictionary alloc]init];
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            arrres=[[responseObject valueForKey:@"data"] objectAtIndex:0];
            
            EventDetailsVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetailsVC"];
            next.FromFlag=@"AllNotificationVC";
            next.mutDict=arrres;
            [self.navigationController pushViewController:next animated:NO];
            
            
        }
        
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];
}


- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}

-(void)GetNotificationList
{
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];//
   
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@get_notification_list",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            Dict=[responseObject valueForKey:@"data"];
            [self CallUpdateNotitfication];
            [_tbl_notif reloadData];
            
        }
        else
        {
            _tbl_notif.hidden=YES;
        }
       
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        _tbl_notif.hidden=YES;
    }];
}

-(void)CallUpdateNotitfication
{
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];//
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@update_view_notification",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
           
        }
        else
        {
            
        }
        
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        _tbl_notif.hidden=YES;
    }];
}




- (IBAction)btn_BACK:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_HOME:(id)sender {
}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}

@end
