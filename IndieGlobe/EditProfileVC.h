//
//  EditProfileVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/16/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "PopOverView.h"
#import "TextFieldValidator.h"
#import "DropDownListView.h"
#import "HttpWrapper.h"
#import "ApplicationConstant.h"
#import "UzysImageCropperViewController.h"

@interface EditProfileVC : UIViewController<UzysImageCropperDelegate,UITextFieldDelegate,kDropDownListViewDelegate,HttpWrapperDelegate>
{
    //Image Picker Using
    UIImagePickerController *ipc;
    
    NSArray *arryList;
    DropDownListView * Dropobj;
    
    //HTTP
    HttpWrapper *httpeditprof,*httpprofes,*httcontry,*httpstate,*httpcity;
    
    
    UIImage *select_image;
    
    //VAR
    UITextField *currentTextField;
    int Default;
    NSMutableDictionary *dictres;
    
    int isCheck;
    NSMutableArray* country;
    IBOutlet UITextField *txt_country;
    NSString *countryid;
    
    NSMutableArray* state;
    IBOutlet UITextField *txt_state;
    NSString * stateid;
    
    NSMutableArray* arrcity;
    IBOutlet UITextField *txt_city;
    NSString * cityid;

    NSMutableArray* arrgender;
    IBOutlet UITextField *txt_gender;
    NSString * genderid;
    
    NSString *ProfessionalID;
    NSString *dateofb;
}
@property (nonatomic,retain)NSMutableDictionary *responcedic;

@property (nonatomic,retain) UzysImageCropperViewController *imgCropperViewController;
////////crop
@property (nonatomic,retain) UIImageView *resultImgView;
@property (nonatomic,retain) UIImagePickerController *picker;
//////////



//Outlet
@property (strong, nonatomic) IBOutlet UITextField *txt_bod;
@property (strong, nonatomic) IBOutlet UIImageView *img_profile;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_fname;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_lname;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_email;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_cate;
//@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_country;
//@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_state;
//@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_city;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_short_bio;

@property (strong, nonatomic) IBOutlet UIButton *btn_save_prof;
@property (strong, nonatomic) IBOutlet UIButton *btn_cancel_prof;




//Action
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_SAVEPROF:(id)sender;
- (IBAction)btn_CHOOSE_IMG:(id)sender;
- (IBAction)btn_SPROFESSION:(id)sender;
- (IBAction)btn_SCOUNTRY:(id)sender;
- (IBAction)btn_SSTATE:(id)sender;
- (IBAction)btn_SCITY:(id)sender;
- (IBAction)btn_CANCLE:(id)sender;
- (IBAction)btn_SGENDER:(id)sender;
- (IBAction)btn_SBOD:(id)sender;









@end
