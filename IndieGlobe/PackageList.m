	//
//  PackageList.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/29/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "PackageList.h"
#import "UIViewController+ENPopUp.h"
#import "JWBlurView.h"
#import "MenuViewController.h"
#import "MenuCell.h"
#import "UIColor+CL.h"
#import "PaymentVC.h"

#define kPayPalEnvironment PayPalEnvironmentSandbox

@interface PackageList ()
@property(nonatomic, strong, readwrite) PayPalConfiguration *payPalConfig;
@end

@implementation PackageList

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    // Do any additional setup after loading the view.
    
    ButtonFlage=1;
    PackageFlage=1;
    
    // Set up payPalConfig
    _payPalConfig = [[PayPalConfiguration alloc] init];
    _payPalConfig.acceptCreditCards = YES;
    _payPalConfig.merchantName = @"TechMishty";
    _payPalConfig.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/privacy-full"];
    _payPalConfig.merchantUserAgreementURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/useragreement-full"];

     _payPalConfig.languageOrLocale = [NSLocale preferredLanguages][0];
    
    // See PayPalConfiguration.h for details.
    
    _payPalConfig.payPalShippingAddressOption = PayPalShippingAddressOptionPayPal;
    //self.successView.hidden = YES;
    
    // use default environment, should be Production in real life
    self.environment = kPayPalEnvironment;
    [PayPalMobile preconnectWithEnvironment:_environment];
    

    
    NSLog(@"PayPal iOS SDK version: %@", [PayPalMobile libraryVersion]);
    
    CGRect frameRect3 = _btn_act_glod.frame;
    frameRect3.size.height = 22; // <-- Specify the height you want here.
    _btn_act_glod.frame = frameRect3;
    _btn_act_glod.layer.borderWidth=1;
    _btn_act_glod.layer.borderColor=[UIColor colorWithRed:(23.0f/255.0) green:(196.0f/255.0) blue:(199.0f/255.0) alpha:1].CGColor;
    _btn_act_glod.layer.cornerRadius=_btn_act_glod.frame.size.height/2;
    //_btn_pay1.userInteractionEnabled=NO;
    

    
    CGRect frameRect = _btn_pay1.frame;
    frameRect.size.height = 22; // <-- Specify the height you want here.
    _btn_pay1.frame = frameRect;
    _btn_pay1.layer.borderWidth=1;
    _btn_pay1.layer.borderColor=[UIColor colorWithRed:(23.0f/255.0) green:(196.0f/255.0) blue:(199.0f/255.0) alpha:1].CGColor;
    _btn_pay1.layer.cornerRadius=_btn_pay1.frame.size.height/2;
    //_btn_pay1.userInteractionEnabled=NO;
    
    CGRect frameRect1 = _btn_pay2.frame;
    frameRect1.size.height = 22; // <-- Specify the height you want here.
    _btn_pay2.frame = frameRect1;
    _btn_pay2.layer.borderWidth=1;
    _btn_pay2.layer.borderColor=[UIColor colorWithRed:(23.0f/255.0) green:(196.0f/255.0) blue:(199.0f/255.0) alpha:1].CGColor;
    _btn_pay2.layer.cornerRadius=_btn_pay2.frame.size.height/2;
    //_btn_pay2.userInteractionEnabled=NO;
    
    _vw_package.layer.cornerRadius=5;
    
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [_vw_alpha addGestureRecognizer:singleFingerTap];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *planid=[standardUserDefaults valueForKey:@"PlanID"];
    NSString *planTerms=[standardUserDefaults valueForKey:@"PlanTerms"];
    
    if ([planTerms isEqualToString:@"yearly"]) {
        PackageFlage=2;
    }
    else
    {
        PackageFlage=1;
    }
    
    
    if ([planid intValue]==1) {
        NSLog(@"GOLD");
        [self Goldplan];
    }
    else if ([planid intValue]==2)
    {
        NSLog(@"PLATINUM");
        [self Platinumplan];
    }
    else
    {
        NSLog(@"DIAMOND");
        [self Diamondplan];
    }
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self GetPackagePlan];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *planid=[standardUserDefaults valueForKey:@"PlanID"];
    NSString *planTerms=[standardUserDefaults valueForKey:@"PlanTerms"];
    
    if ([planTerms isEqualToString:@"yearly"]) {
        PackageFlage=2;
    }
    else
    {
        PackageFlage=1;
    }
    
    
    if ([planid intValue]==1) {
        NSLog(@"GOLD");
        [self Goldplan];
    }
    else if ([planid intValue]==2)
    {
        NSLog(@"PLATINUM");
        [self Platinumplan];
    }
    else
    {
        NSLog(@"DIAMOND");
        [self Diamondplan];
    }
}

-(void)GetPackagePlan
{
    [APP_DELEGATE showLoadingView:@""];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@get_plan",BaseURLAPI] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        arrplan= [responseObject valueForKey:@"data"];
        NSLog(@"%@",arrplan);
        
        _lbl_plat_plan.text=[NSString stringWithFormat:@"($%@/Mo & $%@/Yr)",[[arrplan valueForKey:@"MonthlyFee"] objectAtIndex:1],[[arrplan valueForKey:@"yearlyFees"] objectAtIndex:1]];
        
        _lbl_daim_plan.text=[NSString stringWithFormat:@"($%@/Mo & $%@/Yr)",[[arrplan valueForKey:@"MonthlyFee"] objectAtIndex:2],[[arrplan valueForKey:@"yearlyFees"] objectAtIndex:2]];
        
        _lbl_gold.text=[NSString stringWithFormat:@"%@",[[arrplan valueForKey:@"PlanName"] objectAtIndex:0]];
        
        _lbl_platinum.text=[NSString stringWithFormat:@"%@",[[arrplan valueForKey:@"PlanName"] objectAtIndex:1]];
        
        _lbl_diamond.text=[NSString stringWithFormat:@"%@",[[arrplan valueForKey:@"PlanName"] objectAtIndex:2]];
        
        

        
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];

}


//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    
    SettingVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingVC"];
    [self.navigationController pushViewController:next animated:NO];

}

- (void)backToSetting
{
    
    SettingVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingVC"];
    [self.navigationController pushViewController:next animated:NO];
    
}

-(void)Goldplan
{
    ButtonFlage=1;
    NSData *imgData1 = UIImagePNGRepresentation([UIImage imageNamed:@"radio-unselect.png"]);
    NSData *imgData2 = UIImagePNGRepresentation(_img_gold.image);
    BOOL isCompare =  [imgData1 isEqualToData:imgData2];
    if(isCompare)
    {
        NSLog(@"Image View contains image.png");
        _img_gold.image=[UIImage imageNamed:@"radio-select.png"];
        _img_platinum.image=[UIImage imageNamed:@"radio-unselect.png"];
        _img_diamond.image=[UIImage imageNamed:@"radio-unselect.png"];
        _lbl_gold.textColor=[UIColor colorWithHex:0x17c4c7];
        _lbl_platinum.textColor=[UIColor colorWithHex:0x000000];
        _lbl_diamond.textColor=[UIColor colorWithHex:0x000000];
        
        
    }
}

-(void)Platinumplan
{
    ButtonFlage=2;
    NSData *imgData1 = UIImagePNGRepresentation([UIImage imageNamed:@"radio-unselect.png"]);
    NSData *imgData2 = UIImagePNGRepresentation(_img_platinum.image);
    BOOL isCompare =  [imgData1 isEqualToData:imgData2];
    if(isCompare)
    {
        NSLog(@"Image View contains image.png");
        _img_gold.image=[UIImage imageNamed:@"radio-unselect.png"];
        _img_platinum.image=[UIImage imageNamed:@"radio-select.png"];
        _img_diamond.image=[UIImage imageNamed:@"radio-unselect.png"];
        _lbl_gold.textColor=[UIColor colorWithHex:0x000000];
        _lbl_platinum.textColor=[UIColor colorWithHex:0x17c4c7];
        _lbl_diamond.textColor=[UIColor colorWithHex:0x000000];

        //_btn_pay1.userInteractionEnabled=YES;
        //_btn_pay2.userInteractionEnabled=NO;
        
    }
    
    if (PackageFlage==1) {
        //[self btn_SP_MO:];
        [self performSelector:@selector(btn_SP_MO:) withObject:self afterDelay:0.0f];

    }
    else
    {
        [self performSelector:@selector(btn_SP_YR:) withObject:self afterDelay:0.0f];
    }
    
}
-(void)Diamondplan
{
    ButtonFlage=3;
    NSData *imgData1 = UIImagePNGRepresentation([UIImage imageNamed:@"radio-unselect.png"]);
    NSData *imgData2 = UIImagePNGRepresentation(_img_diamond.image);
    BOOL isCompare =  [imgData1 isEqualToData:imgData2];
    if(isCompare)
    {
        NSLog(@"Image View contains image.png");
        _img_gold.image=[UIImage imageNamed:@"radio-unselect.png"];
        _img_platinum.image=[UIImage imageNamed:@"radio-unselect.png"];
        _img_diamond.image=[UIImage imageNamed:@"radio-select.png"];
        _lbl_gold.textColor=[UIColor colorWithHex:0x000000];
        _lbl_platinum.textColor=[UIColor colorWithHex:0x000000];
        _lbl_diamond.textColor=[UIColor colorWithHex:0x17c4c7];
        
       // _btn_pay1.userInteractionEnabled=NO;
       // _btn_pay2.userInteractionEnabled=YES;
    }
    
    if (PackageFlage==1) {
        //[self btn_SP_MO:];
        [self performSelector:@selector(btn_SD_MO:) withObject:self afterDelay:0.0f];
        
    }
    else
    {
        [self performSelector:@selector(btn_SD_YR:) withObject:self afterDelay:0.0f];
    }
}
- (IBAction)btn_SP_MO:(id)sender {
    PackageFlage=1;
    NSData *imgData1 = UIImagePNGRepresentation([UIImage imageNamed:@"radio-unselect.png"]);
    NSData *imgData2 = UIImagePNGRepresentation(_img_plat_mo.image);
    BOOL isCompare =  [imgData1 isEqualToData:imgData2];
    if(isCompare)
    {
        NSLog(@"Image View contains image.png");
        _img_plat_mo.image=[UIImage imageNamed:@"radio-select.png"];
        _img_plat_yr.image=[UIImage imageNamed:@"radio-unselect.png"];
    }
}

- (IBAction)btn_SP_YR:(id)sender {
    PackageFlage=2;
    NSData *imgData1 = UIImagePNGRepresentation([UIImage imageNamed:@"radio-unselect.png"]);
    NSData *imgData2 = UIImagePNGRepresentation(_img_plat_yr.image);
    BOOL isCompare =  [imgData1 isEqualToData:imgData2];
    if(isCompare)
    {
        NSLog(@"Image View contains image.png");
        _img_plat_yr.image=[UIImage imageNamed:@"radio-select.png"];
        _img_plat_mo.image=[UIImage imageNamed:@"radio-unselect.png"];
    }
}

- (IBAction)btn_SD_MO:(id)sender {
    
    PackageFlage=1;
    NSData *imgData1 = UIImagePNGRepresentation([UIImage imageNamed:@"radio-unselect.png"]);
    NSData *imgData2 = UIImagePNGRepresentation(_img_diam_mo.image);
    BOOL isCompare =  [imgData1 isEqualToData:imgData2];
    if(isCompare)
    {
        NSLog(@"Image View contains image.png");
        _img_diam_mo.image=[UIImage imageNamed:@"radio-select.png"];
        _img_diam_yr.image=[UIImage imageNamed:@"radio-unselect.png"];
    }
}

- (IBAction)btn_SD_YR:(id)sender {
    
    PackageFlage=2;
    NSData *imgData1 = UIImagePNGRepresentation([UIImage imageNamed:@"radio-unselect.png"]);
    NSData *imgData2 = UIImagePNGRepresentation(_img_diam_yr.image);
    BOOL isCompare =  [imgData1 isEqualToData:imgData2];
    if(isCompare)
    {
        NSLog(@"Image View contains image.png");
        _img_diam_yr.image=[UIImage imageNamed:@"radio-select.png"];
        _img_diam_mo.image=[UIImage imageNamed:@"radio-unselect.png"];
    }
    
}

- (IBAction)btn_SGOLD:(id)sender {
    [self Goldplan];
}
- (IBAction)btn_SPLATINUM:(id)sender {
    [self Platinumplan];
}
- (IBAction)btn_SDIAMOND:(id)sender {
    [self Diamondplan];
}


- (IBAction)btn_PAY_ACTION:(id)sender {
    
   // [self CallEditPlan];
 
    if (ButtonFlage==2) {
        NSString *amt=[[NSString alloc]init];
        NSData *imgData1 = UIImagePNGRepresentation([UIImage imageNamed:@"radio-select.png"]);
        NSData *imgData2 = UIImagePNGRepresentation(_img_plat_mo.image);
        BOOL isCompare =  [imgData1 isEqualToData:imgData2];
        if(isCompare)
        {
            PackageFlage=1;
            amt=[[arrplan  valueForKey:@"MonthlyFee"] objectAtIndex:1];
        }
        else
        {
            PackageFlage=2;
            amt=[[arrplan  valueForKey:@"yearlyFees"] objectAtIndex:1];
        }
        
        PayPalItem *item1 = [PayPalItem itemWithName:[NSString stringWithFormat:@"%@",[[arrplan valueForKey:@"PlanName"]objectAtIndex:1 ]]
                                        withQuantity:1
                                           withPrice:[NSDecimalNumber decimalNumberWithString:amt]
                                        withCurrency:@"USD"
                                             withSku:@"SK-Platinum"];
        
        
        NSArray*items=@[item1];
        NSDecimalNumber *subtotal = [PayPalItem totalPriceForItems:items];
        
        // Optional: include payment details
        NSDecimalNumber *shipping = [[NSDecimalNumber alloc] initWithString:@"0.00"];
        NSDecimalNumber *tax = [[NSDecimalNumber alloc] initWithString:@"0.00"];
        PayPalPaymentDetails *paymentDetails = [PayPalPaymentDetails paymentDetailsWithSubtotal:subtotal
                                                                                   withShipping:shipping
                                                                                        withTax:tax];
        NSDecimalNumber *total = [[subtotal decimalNumberByAdding:shipping] decimalNumberByAdding:tax];
        
        PayPalPayment *payment = [[PayPalPayment alloc] init];
        payment.amount = total;
        payment.currencyCode = @"USD";
        payment.shortDescription = [NSString stringWithFormat:@"%@",[[arrplan valueForKey:@"PlanName"] objectAtIndex:1]];
        payment.items = items;  // if not including multiple items, then leave payment.items as nil
        payment.paymentDetails = paymentDetails;
        
        if (!payment.processable) {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn't be processable, and you'd want
            // to handle that here.
        }
        
        PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                                                    configuration:self.payPalConfig
                                                                                                         delegate:self];
        [self presentViewController:paymentViewController animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Please Select Platinum Plan"
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"OK",nil];
        [alert show];
    }
    
    
    

    
 
    
  /*  PaymentVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentVC"];
    [self.navigationController pushViewController:next animated:YES];*/
}

- (IBAction)btn_PAY_ACTION1:(id)sender {
    
    //[self CallEditPlan];
  
    if (ButtonFlage==3) {
        NSString *amt=[[NSString alloc]init];
        NSData *imgData1 = UIImagePNGRepresentation([UIImage imageNamed:@"radio-select.png"]);
        NSData *imgData2 = UIImagePNGRepresentation(_img_diam_mo.image);
        BOOL isCompare =  [imgData1 isEqualToData:imgData2];
        if(isCompare)
        {
            PackageFlage=1;
            amt=[[arrplan  valueForKey:@"MonthlyFee"] objectAtIndex:2];
        }
        else
        {
            PackageFlage=2;
            amt=[[arrplan  valueForKey:@"yearlyFees"] objectAtIndex:2];
        }
        
        PayPalItem *item1 = [PayPalItem itemWithName:[NSString stringWithFormat:@"%@",[[arrplan valueForKey:@"PlanName"]objectAtIndex:2 ]]
                                        withQuantity:1
                                           withPrice:[NSDecimalNumber decimalNumberWithString:amt]
                                        withCurrency:@"USD"
                                             withSku:@"SK-Platinum"];
        
        
        NSArray*items=@[item1];
        NSDecimalNumber *subtotal = [PayPalItem totalPriceForItems:items];
        
        // Optional: include payment details
        NSDecimalNumber *shipping = [[NSDecimalNumber alloc] initWithString:@"0.00"];
        NSDecimalNumber *tax = [[NSDecimalNumber alloc] initWithString:@"0.00"];
        PayPalPaymentDetails *paymentDetails = [PayPalPaymentDetails paymentDetailsWithSubtotal:subtotal
                                                                                   withShipping:shipping
                                                                                        withTax:tax];
        NSDecimalNumber *total = [[subtotal decimalNumberByAdding:shipping] decimalNumberByAdding:tax];
        
        PayPalPayment *payment = [[PayPalPayment alloc] init];
        payment.amount = total;
        payment.currencyCode = @"USD";
        payment.shortDescription = [NSString stringWithFormat:@"%@",[[arrplan valueForKey:@"PlanName"] objectAtIndex:2]];
        payment.items = items;  // if not including multiple items, then leave payment.items as nil
        payment.paymentDetails = paymentDetails;
        
        if (!payment.processable) {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn't be processable, and you'd want
            // to handle that here.
        }
        
        PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                                                    configuration:self.payPalConfig
                                                                                                         delegate:self];
        [self presentViewController:paymentViewController animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Please Select Diamond Plan"
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"OK",nil];
        [alert show];
    }
    
    
    
   
    /*PaymentVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentVC"];
    [self.navigationController pushViewController:next animated:YES];*/

}

- (IBAction)btn_ACTIVE_GLOD:(id)sender {
   
    if (ButtonFlage==1) {
        
        [APP_DELEGATE showLoadingView:@""];
        
        NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
        NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
        [AddPost setValue:[NSString stringWithFormat:@"%d",ButtonFlage] forKey:@"plan_id"];
        [AddPost setValue:@" " forKey:@"plan_terms"];
       
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
        NSLog(@"PARAM---%@",AddPost);
        
        [manager POST:[NSString stringWithFormat:@"%@edit_user_plan",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            
            if ([[responseObject valueForKey:@"status"] integerValue]==1) {
                responseObject=[responseObject valueForKey:@"data"];
                
                NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
                [standardUserDefaults setValue:[responseObject valueForKey:@"PlanID"] forKey:@"PlanID"];
                [standardUserDefaults setValue:[responseObject valueForKey:@"PlanTerms"] forKey:@"PlanTerms"];
                [standardUserDefaults setValue:[responseObject valueForKey:@"PlanName"] forKey:@"PlanName"];
                
            }
            
            [self performSelector:@selector(backToSetting) withObject:self afterDelay:0.0f];
            
            
            [APP_DELEGATE hideLoadingView];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            NSLog(@"Error: %@", error);
            [APP_DELEGATE hideLoadingView];
            
        }];
    }
}


- (BOOL)isCompare:(UIImage *)image1 isEqualTo:(UIImage *)image2
{
    NSData *data1 = UIImagePNGRepresentation(image1);
    NSData *data2 = UIImagePNGRepresentation(image2);
    return [data1 isEqual:data2];
}



#pragma mark - Paypal Config

//acceptCreditCards
- (BOOL)acceptCreditCards {
    return self.payPalConfig.acceptCreditCards;
}

//setAcceptCreditCards
- (void)setAcceptCreditCards:(BOOL)acceptCreditCards {
    self.payPalConfig.acceptCreditCards = acceptCreditCards;
}
- (void)setPayPalEnvironment:(NSString *)environment {
    self.environment = kPayPalEnvironment;
    [PayPalMobile preconnectWithEnvironment:environment];
}


- (void)payPalFuturePaymentDidCancel:(nonnull PayPalFuturePaymentViewController *)futurePaymentViewController
{
    
}
- (void)payPalFuturePaymentViewController:(nonnull PayPalFuturePaymentViewController *)futurePaymentViewController
                didAuthorizeFuturePayment:(nonnull NSDictionary *)futurePaymentAuthorization
{
    NSLog(@"log printed");
}

- (void)userDidCancelPayPalProfileSharingViewController:(nonnull PayPalProfileSharingViewController *)profileSharingViewController
{
    NSLog(@"log printed 1");
}
- (void)payPalProfileSharingViewController:(nonnull PayPalProfileSharingViewController *)profileSharingViewController
             userDidLogInWithAuthorization:(nonnull NSDictionary *)profileSharingAuthorization
{
    NSLog(@"log printed 2");
}

#pragma mark PayPalPaymentDelegate methods

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment {
    NSLog(@"PayPal Payment Success! %@",completedPayment);
    
    NSLog(@"PayPal Payment Success!");
    self.resultText = [completedPayment description];
    //[self showSuccess];
    NSLog(@"completedPayment ---%@",completedPayment);
    [self sendCompletedPaymentToServer:completedPayment]; // Payment was processed successfully; send to server for verification and fulfillment
    
    [self CallEditPlan];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)CallEditPlan
{
    
    [APP_DELEGATE showLoadingView:@""];
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[NSString stringWithFormat:@"%d",ButtonFlage] forKey:@"plan_id"];
    if (PackageFlage==1) {
        [AddPost setValue:@"monthly" forKey:@"plan_terms"];
    }
    else
    {
        [AddPost setValue:@"yearly" forKey:@"plan_terms"];
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"PARAM---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@edit_user_plan",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        if ([[responseObject valueForKey:@"status"] integerValue]==1) {
            responseObject=[responseObject valueForKey:@"data"];
            
            NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
            [standardUserDefaults setValue:[responseObject valueForKey:@"PlanID"] forKey:@"PlanID"];
            [standardUserDefaults setValue:[responseObject valueForKey:@"PlanTerms"] forKey:@"PlanTerms"];
            [standardUserDefaults setValue:[responseObject valueForKey:@"PlanName"] forKey:@"PlanName"];
            
        }
        
        [self performSelector:@selector(backToSetting) withObject:self afterDelay:0.0f];
        
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
        
    }];
}


- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    NSLog(@"PayPal Payment Canceled");
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)paymentFailedWithCorrelationID:
(NSString *)correlationID andErrorCode:
(NSString *)errorCode andErrorMessage:
(NSString *)errorMessage {
    NSLog(@"Failed");
    
}

#pragma mark Proof of payment validation

- (void)sendCompletedPaymentToServer:(PayPalPayment *)completedPayment
{
    NSLog(@"completedPayment %@",completedPayment);
    // TODO: Send completedPayment.confirmation to server
    NSLog(@"Here is your proof of payment:\n\n%@\n\nSend this to your server for confirmation and fulfillment.", completedPayment.confirmation);
    
    NSLog(@"completedPayment %@",[completedPayment.confirmation valueForKeyPath:@"response.id"]);
    
    
}

- (void)showSuccess
{
   
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelay:2.0];
    [UIView commitAnimations];
}


/*
- (void)sendCompletedPaymentToServer:(PayPalPayment *)completedPayment {
    // TODO: Send completedPayment.confirmation to server
    NSLog(@"Here is your proof of payment:\n\n%@\n\nSend this to your server for confirmation and fulfillment.", completedPayment.confirmation);
}

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    NSLog(@"PayPal Payment Canceled");
    self.resultText = nil;
    //self.successView.hidden = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
}
*/


@end
