//
//  RequestCell.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/26/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "RequestCell.h"

@implementation RequestCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _img_profileimg.clipsToBounds=YES;
    _img_profileimg.layer.cornerRadius=_img_profileimg.frame.size.height/2;
    
    _btn_accept.layer.cornerRadius=_btn_accept.frame.size.height/2;
    _btn_denied.layer.cornerRadius=_btn_denied.frame.size.height/2;
    
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
