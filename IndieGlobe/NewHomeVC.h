//
//  NewHomeVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 2/3/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "HttpWrapper.h"
#import "ApplicationConstant.h"
#import "NewHomeSearchVC.h"
#import "HttpWrapper.h"
#import "ApplicationConstant.h"
#import "NewHomeSearchVC.h"
#import "Firebase.h"
#import "JhtFloatingBall.h"

/** 颜色转换  例：#000000 UIColorFromRGB(0x000000) */
#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


@interface NewHomeVC : UIViewController<JhtFloatingBallDelegate,UICollectionViewDelegate,HttpWrapperDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource>
{
    UIView *picker;
    NSMutableArray *arrimg;
    
    NSMutableArray *arrresp;
    
    UIImage *thumbnail;
    
    
    
    FIRDatabaseReference  *newRefrence;
    
    HttpWrapper* httplike,*httpdislike,*httpfollow,*httpunfollow;
    
    
    NSMutableArray *arrevent;
    NSMutableArray *arrclass;
    NSMutableArray *arrcriti;
    NSMutableArray *arrlearn;
    NSMutableArray *arrfunding;
    NSMutableArray *arrindie;
    NSMutableArray *arrport;
    

    SystemSoundID mBeep;
}

/** folatingball */
@property (nonatomic, strong) JhtFloatingBall *folatingball;

//Outlets
@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;

@property (strong, nonatomic) IBOutlet UIButton *btn_menu;
@property (strong, nonatomic) IBOutlet UICollectionView *collection_home;
@property (strong, nonatomic) IBOutlet UIButton *menu_Button;
@property (strong, nonatomic) IBOutlet UITableView *tbl_newhome;
@property (strong, nonatomic) IBOutlet UIImageView *img_colle_back;





//Action
- (IBAction) playSound;
- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;
- (IBAction)btn_GLOBE:(id)sender;
- (IBAction)btn_NOTIF:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;

- (IBAction)playLongSoundPressed:(UIButton *)sender;


@end
