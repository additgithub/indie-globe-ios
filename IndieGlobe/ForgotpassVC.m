//
//  ForgotpassVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/11/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "ForgotpassVC.h"
#import "LoginVC.h"
#import "TextFieldValidator.h"


#define REGEX_EMAIL @"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"


@interface ForgotpassVC ()<UITextFieldDelegate>

@end

@implementation ForgotpassVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     _txt_forgotpass.placeholder=@"email";
    [self textlayers:_txt_forgotpass];
   
    _btn_send.layer.cornerRadius=16.0;

     [_txt_forgotpass addRegx:REGEX_EMAIL withMsg:@"Enter valid email."];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)textlayers :(UITextField *)UITextField
{
    
    UITextField.layer.borderColor=[[UIColor clearColor]CGColor];
    UITextField.layer.borderWidth=1.5;
    UITextField.layer.borderColor=[[UIColor whiteColor] CGColor];
    UITextField.layer.cornerRadius=15.0;
    [UITextField setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
    UITextField.leftView = paddingView;
    UITextField.leftViewMode = UITextFieldViewModeAlways;
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch * touch = [touches anyObject];
    if(touch.phase == UITouchPhaseBegan) {
        [_txt_forgotpass resignFirstResponder];
    }
}
#pragma mark - GetAllPosts
-(void)CallForgot
{
    
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue:_txt_forgotpass.text forKey:@"email"];
   
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@forgot_password",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        
        [APP_DELEGATE hideLoadingView];
       /* LoginVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        [self.navigationController pushViewController:next animated:YES];*/
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Send to your registered mailid." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];

        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_SENDPASS:(id)sender {
    if([_txt_forgotpass validate]){
        
                [self CallForgot];
    }
    

}

- (IBAction)btn_GOLOGIN:(id)sender {
    if([_txt_forgotpass validate]){
        
        LoginVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        [self.navigationController pushViewController:next animated:YES];
        
    }
    LoginVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    [self.navigationController pushViewController:next animated:YES];

    

}
@end
