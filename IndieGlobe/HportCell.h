//
//  HportCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 3/26/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShadowView.h"


@interface HportCell : UITableViewCell

//Outlet
@property (strong, nonatomic) IBOutlet UIImageView *img_prof;
@property (strong, nonatomic) IBOutlet UILabel *lbl_usernm;
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;
@property (strong, nonatomic) IBOutlet UIImageView *img_image;
@property (strong, nonatomic) IBOutlet UIButton *btn_play;
@property (strong, nonatomic) IBOutlet UILabel *lbl_postdt;

@property (weak, nonatomic) IBOutlet UIButton *btn_like;
@property (weak, nonatomic) IBOutlet UIButton *btn_dislike;

@property (weak, nonatomic) IBOutlet UILabel *lbl_likecount;
@property (weak, nonatomic) IBOutlet UILabel *lbl_dislikecount;

@property (weak, nonatomic) IBOutlet ShadowView *shadow_vw;

@property (weak, nonatomic) IBOutlet UILabel *lbl_comment_count;
@property (weak, nonatomic) IBOutlet UIButton *btn_commnent;


@property (weak, nonatomic) IBOutlet UIImageView *img_vw1;
@property (weak, nonatomic) IBOutlet UILabel *lbl_nm_vw1;
@property (weak, nonatomic) IBOutlet UILabel *lbl_desc_vw1;

@property (weak, nonatomic) IBOutlet UIImageView *img_vw2;
@property (weak, nonatomic) IBOutlet UILabel *lbl_nm_vw2;
@property (weak, nonatomic) IBOutlet UILabel *lbl_desc_vw2;

@property (weak, nonatomic) IBOutlet UIView *vw1;
@property (weak, nonatomic) IBOutlet UIView *vw2;



//Action


@end
