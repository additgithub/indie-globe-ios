//
//  DeshboardVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/27/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

@interface DeshboardVC : UIViewController<SWRevealViewControllerDelegate>
{
    UIView *picker;
}
//Outlets
@property (strong, nonatomic) IBOutlet UIButton *btn_menu;
@property (strong, nonatomic) IBOutlet UIView *vw_side;
@property (strong, nonatomic) IBOutlet UIScrollView *vw_scroll;



//Actions
- (IBAction)btn_MENU_ACTION:(id)sender;
- (IBAction)btn_EVENT:(id)sender;

- (IBAction)btn_SKIP:(id)sender;
- (IBAction)btn_NEWS:(id)sender;
- (IBAction)btn_CRITIQUES:(id)sender;
- (IBAction)btn_CLASSIFIED:(id)sender;
- (IBAction)btn_LEARNING:(id)sender;
- (IBAction)btn_INDIE:(id)sender;
- (IBAction)btn_FUNDING:(id)sender;
- (IBAction)BTN_2:(id)sender;
- (IBAction)btn_3:(id)sender;
- (IBAction)btn_4:(id)sender;


@end
