//
//  ProjectVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/2/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
#import "PopOverView.h"

#import "ApplicationConstant.h"
#import "UzysImageCropperViewController.h"

@interface ProjectVC : UIViewController<UzysImageCropperDelegate,HttpWrapperDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
{
    UIView *picker;
    
    HttpWrapper* httpaddproj,*httpcountry,*httpstate,*httpcity;
    
    UIImagePickerController *ci;
    UIPopoverController *popover;
    UIImage *select_image;
    
    
    UITextField *currentTextField;
    int isCheck;
    NSMutableArray* country;
    IBOutlet UITextField *txt_country;
    NSString *countryid;

    
    NSMutableArray* state;
    IBOutlet UITextField *txt_state;
    NSString * stateid;
    
    NSMutableArray* arrcity;
    IBOutlet UITextField *txt_city;
    NSString * cityid;

}

@property (nonatomic,retain) UzysImageCropperViewController *imgCropperViewController;
@property (nonatomic,retain) UIImagePickerController *imgpicker;


//Outlet
@property (strong, nonatomic) IBOutlet UIButton *btn_menu;
@property (strong, nonatomic) IBOutlet UITextField *txt_title;
@property (strong, nonatomic) IBOutlet UITextView *txt_desc;

@property (strong, nonatomic) IBOutlet UITextField *txt_teamsize;
@property (strong, nonatomic) IBOutlet UIButton *btn_submit;
@property (strong, nonatomic) IBOutlet UIImageView *img_select_image;
@property (strong, nonatomic) IBOutlet UIImageView *img_extra;
@property (strong, nonatomic) IBOutlet UILabel *lbl_extra;
@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;




//Action


- (IBAction)btn_HOMEs:(id)sender;
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_SCITY:(id)sender;
- (IBAction)btn_SSTATE:(id)sender;
- (IBAction)btn_SCOUNTRY:(id)sender;

- (IBAction)btn_ADD_PROJ:(id)sender;
- (IBAction)btn_NOTIFY:(id)sender;


@end
