//
//  HfundCell.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 3/23/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "HfundCell.h"
#import "UIColor+CL.h"

@implementation HfundCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _btn_donet.layer.cornerRadius=_btn_donet.frame.size.height/2;
    _btn_donet.layer.borderWidth=1;
    _btn_donet.layer.borderColor=[UIColor colorWithHex:0x196a84].CGColor;
    
    _img_image.clipsToBounds=YES;
    _img_image.layer.borderWidth=1;
    _img_image.layer.borderColor=[UIColor lightGrayColor].CGColor;
    
    
    [self.shadow_vw setShadow];
    
    _btn_follow.layer.cornerRadius=_btn_follow.frame.size.height/2;
    _btn_follow.layer.borderWidth=1;
    _btn_follow.layer.borderColor=[UIColor colorWithHex:0x196a84].CGColor;
    
    
    _img_prof.layer.cornerRadius=_img_prof.layer.frame.size.height/2;
    _img_prof.clipsToBounds=YES;
    
    
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
