//
//  MessageChat.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 9/11/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageChat : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *arrList;
    float CellHeight;
    
}

@property (weak, nonatomic) IBOutlet UITableView *tbl_chat;

@property (weak, nonatomic) IBOutlet UILabel *lbl_headernm;
@property (weak, nonatomic) IBOutlet UIImageView *img_header;
@property (weak, nonatomic) IBOutlet UITextField *txt_msg;


@property (retain , nonatomic)NSMutableDictionary *dictFrom;

- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_SENDMSG:(id)sender;


@end
