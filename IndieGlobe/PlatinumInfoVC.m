//
//  PlatinumInfoVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/25/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "PlatinumInfoVC.h"
#import "UIColor+CL.h"
#import "AllNotificationVC.h"


@interface PlatinumInfoVC ()

@end

@implementation PlatinumInfoVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Do any additional setup after loading the vie
    //_btn_monthly.layer.cornerRadius=_btn_monthly.frame.size.height/2;
    
    
    //_btn_yearly.layer.cornerRadius=_btn_yearly.frame.size.height/2;
    _btn_yearly.layer.backgroundColor=[UIColor clearColor].CGColor;
    
    _vw_buttons.layer.cornerRadius=_vw_buttons.frame.size.height/2;
    _vw_buttons.layer.borderWidth=1;
    _vw_buttons.layer.borderColor=[UIColor colorWithHex:0xAFAFAF].CGColor;
    
    if (_ButtonFlag==2) {
        _lbl_header_title.text=@"PLATINUM";
        [_btn_monthly setTitle:[NSString stringWithFormat:@"%@",[[_arrplan valueForKey:@"MonthlyFee"] objectAtIndex:1]] forState:UIControlStateNormal];
        [_btn_yearly setTitle:[NSString stringWithFormat:@"%@",[[_arrplan valueForKey:@"yearlyFees"] objectAtIndex:1]] forState:UIControlStateNormal];
    }
    else if (_ButtonFlag==3)
    {
        _lbl_header_title.text=@"DIAMOND";
        [_btn_monthly setTitle:[NSString stringWithFormat:@"%@",[[_arrplan valueForKey:@"MonthlyFee"] objectAtIndex:2]] forState:UIControlStateNormal];
        [_btn_yearly setTitle:[NSString stringWithFormat:@"%@",[[_arrplan valueForKey:@"yearlyFees"] objectAtIndex:2]] forState:UIControlStateNormal];
    }
    
    
    if (_PackageFlag==1) {
        //_btn_monthly.layer.cornerRadius=_btn_monthly.frame.size.height/2;
        _btn_monthly.layer.backgroundColor=[UIColor colorWithHex:0x01B5A9].CGColor;
        [_btn_monthly setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _lbl_monthly.textColor=[UIColor whiteColor];
        
        //_btn_yearly.layer.cornerRadius=_btn_yearly.frame.size.height/2;
        _btn_yearly.layer.backgroundColor=[UIColor clearColor].CGColor;
        [_btn_yearly setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _lbl_yearly.textColor=[UIColor blackColor];
        
        // Drawing code
        CAShapeLayer * maskLayer = [CAShapeLayer layer];
        maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: _btn_monthly.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerBottomLeft  cornerRadii: (CGSize){20.0, 20.0}].CGPath;
        _btn_monthly.layer.mask = maskLayer;
        
        
       
        
        
    }
    else if (_PackageFlag==2)
    {
        //_btn_yearly.selected=true;
        //_btn_yearly.layer.cornerRadius=_btn_yearly.frame.size.height/2;
        _btn_yearly.layer.backgroundColor=[UIColor colorWithHex:0x01B5A9].CGColor;
        [_btn_yearly setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _lbl_yearly.textColor=[UIColor whiteColor];
        
        
        //_btn_monthly.layer.cornerRadius=_btn_monthly.frame.size.height/2;
        _btn_monthly.layer.backgroundColor=[UIColor clearColor].CGColor;
        [_btn_monthly setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _lbl_monthly.textColor=[UIColor blackColor];
        
        
        // Drawing code
        CAShapeLayer * maskLayer = [CAShapeLayer layer];
        maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: _btn_yearly.bounds byRoundingCorners: UIRectCornerTopRight | UIRectCornerBottomRight  cornerRadii: (CGSize){20.0, 20.0}].CGPath;
        _btn_yearly.layer.mask = maskLayer;
        
        
    }
    
    
    
    // Drawing code
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: _btn_monthly.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerBottomLeft  cornerRadii: (CGSize){20.0, 20.0}].CGPath;
    _btn_monthly.layer.mask = maskLayer;
    
    
    _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
    _lbl_notifi_count.clipsToBounds=YES;
    _lbl_notifi_count.text=theAppDelegate.n_Count;
    
    if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
        _lbl_notifi_count.hidden=YES;
    }
    else
    {
        _lbl_notifi_count.hidden=NO;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_MONTHLY_ACT:(id)sender {
    
    //_btn_monthly.layer.cornerRadius=_btn_monthly.frame.size.height/2;
    _btn_monthly.layer.backgroundColor=[UIColor colorWithHex:0x01B5A9].CGColor;
    [_btn_monthly setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _lbl_monthly.textColor=[UIColor whiteColor];
    
    //_btn_yearly.layer.cornerRadius=_btn_yearly.frame.size.height/2;
    _btn_yearly.layer.backgroundColor=[UIColor clearColor].CGColor;
    [_btn_yearly setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _lbl_yearly.textColor=[UIColor blackColor];
    
    // Drawing code
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: _btn_monthly.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerBottomLeft  cornerRadii: (CGSize){20.0, 20.0}].CGPath;
    _btn_monthly.layer.mask = maskLayer;

}

- (IBAction)btn_YEARLY_ACT:(id)sender {
    
    //_btn_yearly.layer.cornerRadius=_btn_yearly.frame.size.height/2;
    _btn_yearly.layer.backgroundColor=[UIColor colorWithHex:0x01B5A9].CGColor;
    [_btn_yearly setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _lbl_yearly.textColor=[UIColor whiteColor];
    
    
    //_btn_monthly.layer.cornerRadius=_btn_monthly.frame.size.height/2;
    _btn_monthly.layer.backgroundColor=[UIColor clearColor].CGColor;
    [_btn_monthly setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _lbl_monthly.textColor=[UIColor blackColor];
    
    
    // Drawing code
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: _btn_yearly.bounds byRoundingCorners: UIRectCornerTopRight | UIRectCornerBottomRight  cornerRadii: (CGSize){20.0, 20.0}].CGPath;
    _btn_yearly.layer.mask = maskLayer;


}

- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)btn_NOTIFY:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}

@end
