//
//  HnewsCell.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 2/3/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "HnewsCell.h"

@implementation HnewsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _img_prof.clipsToBounds=YES;
    _img_prof.layer.cornerRadius=_img_prof.frame.size.height/2;
    
    [self.shadow_vw setShadow];
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
