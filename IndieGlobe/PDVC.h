//
//  PDVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/2/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
#import "ApplicationConstant.h"
#import "PDVC.h"
#import "NewHomeSearchVC.h"

@interface PDVC : UIViewController<UITableViewDataSource,UITableViewDelegate,HttpWrapperDelegate>
{
    UIView *picker;
    NSString *page_no;
    NSMutableArray *inviteList;
    
}

@property (strong, nonatomic) IBOutlet UIButton *btn_notifyuser;
@property (retain ,nonatomic)NSMutableArray *mutDict;
//OUTLETS
@property (strong, nonatomic) IBOutlet UITableView *tbl_invite_list;

@property (strong, nonatomic) IBOutlet UIButton *btn_menu;
@property (strong, nonatomic) IBOutlet UIButton *btn_add_memb;
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;
@property (strong, nonatomic) IBOutlet UITextView *lbl_desc;
@property (strong, nonatomic) IBOutlet UIView *vw_header;
@property (strong, nonatomic) IBOutlet UIImageView *joinimage;
@property (strong, nonatomic) IBOutlet UILabel *lblborder;
@property (strong, nonatomic) IBOutlet UIImageView *img_proj_image;

@property (strong, nonatomic) IBOutlet UIImageView *img_user;
@property (strong, nonatomic) IBOutlet UILabel *lbl_usernm;
@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;


//ACTION
- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;
- (IBAction)btn_ADDMEMB:(id)sender;
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_NOTIFI:(id)sender;
- (IBAction)btn_NOTIFY:(id)sender;


@end
