//
//  EventVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/1/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "EventVC.h"
#import "DeshboardVC.h"
#import "SWRevealViewController.h"
#import "UIColor+CL.h"
#import "WYPopoverController.h"
#import "AddEventVC.h"
#import "Constants.h"
#import "DSCircularLayout.h"
#import "DSCollectionViewCell.h"
#import "TeamBuild1VC.h"
#import "EventCell.h"
#import "EventDetailsVC.h"
#import "SVPullToRefresh.h"
#import "NewHomeVC.h"
#import "AllNotificationVC.h"
#import <JSQSystemSoundPlayer/JSQSystemSoundPlayer.h>
#import <Crashlytics/Crashlytics.h>
#import "AllCommentView.h"
#import "UIImageView+Haneke.h"





#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height
#define ITEM_WIDTH 75
#define ITEM_HEIGHT 75

@interface EventVC ()<WYPopoverControllerDelegate>{
    WYPopoverController* popoverController;
}

@end

@implementation EventVC
{
    BOOL isMenuActive;
    IGCMenu *igcMenu;
    int count;
    BOOL isShow;
}

- (void)viewDidLoad {
    
    
    //[super viewDidLoad];
    
    [self customSetup];
    [self setCircularLayout];
    self.navigationController.navigationBarHidden=YES;
    
    
    // Do any additional setup after loading the view.
    
    
    
    
    //NEW MENU
    btn_enable=@"0";
    page_no=@"0";
    isSearch=@"0";
    last_eventid=@"0";
    count = 0;
    isShow=false;
    
    
    arrimg= [[NSMutableArray alloc]initWithObjects:@"evets4553.png",@"news4557.png",@"critique4552.png",@"classified4551.png",@"learning4556.png",@"indie-pic4555.png",@"global4554.png",@"team4558.png",nil];
    
    
    
    
    [self CustomizeTextField:txt_city];
    [self CustomizeTextField:txt_country];
    [self CustomizeTextField:txt_state];
    [self CustomizeTextField:_txt_search];
    txt_city.placeholder=@"City";
    txt_country.placeholder=@"Country";
    txt_state.placeholder=@"State";
    
    _txt_search.placeholder=@"Search Events";
    
    
    _txt_search.layer.borderWidth=1.0;
    _txt_search.layer.borderColor=[UIColor colorWithRed:(180.0f/255.0) green:(180.0f/255.0) blue:(180.0f/255.0) alpha:1].CGColor;
    
    _btn_addevent.layer.cornerRadius=_btn_addevent.frame.size.height/2;
    
    //btn_search
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.btn_search.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(10.0, 10.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    self.btn_search.layer.mask = maskLayer;
    
    
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];
    
    [_btn_search addTarget:self
                    action:@selector(CallSearchMethod)
          forControlEvents:UIControlEventTouchUpInside];
    
    
    //[self CallGetEvent];
    [self CallCountry];
    
    
    

    
    
    
}
-(void)CallSearchMethod
{
    [self.view endEditing:YES];
    [APP_DELEGATE showLoadingView:@""];
    page_no=@"0";
    mutDict=[[NSMutableArray alloc] init];
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    //Optional
    [AddPost setValue:countryid forKey:@"country"];
    [AddPost setValue:stateid forKey:@"state"];
    [AddPost setValue:cityid forKey:@"city"];
    
    [AddPost setValue:[NSString stringWithFormat:@"%@",page_no] forKey:@"page_no"];
    if (![_txt_search.text isEqualToString:@""]) {
        [AddPost setValue:_txt_search.text forKey:@"search_text"];
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    [manager POST:[NSString stringWithFormat:@"%@get_events",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        page_no=[responseObject valueForKey:@"page_no"];
        
        if ([[responseObject valueForKey:@"data"] count] < 0) {
            //mutDict=[[NSMutableArray alloc] init];
            _tbl_event.showsInfiniteScrolling = NO;
        }
        else
        {
            int64_t delayInSeconds = 2.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                //[_tbl_event beginUpdates];
                NSArray *arr = [responseObject valueForKey:@"data"];
                NSMutableArray *newarr = [[NSMutableArray alloc] init];
                NSLog(@"---%@",newarr);
                for (NSString *str in mutDict) {
                    [newarr addObject:str];
                }
                for (int i=0; i<arr.count; i++) {
                    //[mutDict addObject:arr[i]];
                    [newarr addObject:arr[i]];
                }
                NSLog(@"---%@",newarr);
                mutDict = [[NSMutableArray alloc] init];
                for (NSString*str  in newarr) {
                    [mutDict addObject:str];
                }
                
                
                NSLog(@"--%@",mutDict);
                [_tbl_event reloadData];
                [APP_DELEGATE hideLoadingView];
            });
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
    
    
}
-(void)CallSearchMethod1
{
    [self.view endEditing:YES];
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    //Optional
    [AddPost setValue:countryid forKey:@"country"];
    [AddPost setValue:stateid forKey:@"state"];
    [AddPost setValue:cityid forKey:@"city"];
    
    [AddPost setValue:[NSString stringWithFormat:@"%@",page_no] forKey:@"page_no"];
    if (![_txt_search.text isEqualToString:@""]) {
        [AddPost setValue:_txt_search.text forKey:@"search_text"];
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    
    NSLog(@"---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@get_events",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        page_no=[responseObject valueForKey:@"page_no"];
        
        if ([[responseObject valueForKey:@"data"] isEqual:[NSNull null]]) {
            //mutDict=[[NSMutableArray alloc] init];
            [_tbl_event.infiniteScrollingView stopAnimating];
        }
        else
        {
            int64_t delayInSeconds = 2.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                //[_tbl_event beginUpdates];
                
                NSArray *arr = [responseObject valueForKey:@"data"];
                if (arr.count<1) {
                    _tbl_event.showsInfiniteScrolling = NO;
                }
                NSMutableArray *newarr = [[NSMutableArray alloc] init];
                NSLog(@"---%@",newarr);
                for (NSString *str in mutDict) {
                    [newarr addObject:str];
                }
                for (int i=0; i<arr.count; i++) {
                    //[mutDict addObject:arr[i]];
                    [newarr addObject:arr[i]];
                }
                NSLog(@"---%@",newarr);
                mutDict = [[NSMutableArray alloc] init];
                for (NSString*str  in newarr) {
                    [mutDict addObject:str];
                }
                [_tbl_event.infiniteScrollingView stopAnimating];
                [_tbl_event reloadData];
                [APP_DELEGATE hideLoadingView];
                
            });
            
        }
        
        //[_tbl_event reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
    
    
}

-(void)CallRSVP:(NSInteger)intval
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[[mutDict valueForKey:@"EventID"] objectAtIndex:intval] forKey:@"event_id"];
    
    if ([[[mutDict valueForKey:@"IsRSVP"] objectAtIndex:intval] boolValue]) {
        [AddPost setValue:@"0" forKey:@"is_rsvp"];
    }
    else
    {
        [AddPost setValue:@"1" forKey:@"is_rsvp"];
    }
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@rsvp_event",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        EventCell *cell=(EventCell *)[_tbl_event cellForRowAtIndexPath:index_event];
        cell.lbl_rsvp.text=[NSString stringWithFormat:@"%@ People RSVP",[responseObject valueForKey:@"total_rsvp"]];
        
        
        [APP_DELEGATE hideLoadingView];
        
        [self viewDidAppear:NO];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
}

-(void)CallSCMI:(NSInteger)intval
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[[mutDict valueForKey:@"EventID"] objectAtIndex:intval] forKey:@"event_id"];
    
    if ([[[mutDict valueForKey:@"IsSCMI"] objectAtIndex:intval] boolValue]) {
        [AddPost setValue:@"0" forKey:@"is_scmi"];
    }
    else
    {
        [AddPost setValue:@"1" forKey:@"is_scmi"];
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@scmi_event",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        EventCell *cell=(EventCell *)[_tbl_event cellForRowAtIndexPath:index_event];
        
        cell.lbl_rsvp.text=[NSString stringWithFormat:@"%@ People RSVP",[responseObject valueForKey:@"total_rsvp"]];
        
        /*   cell.btn_rsvp.alpha=1.00;
         cell.btn_rsvp.enabled=YES;
         cell.btn_scmi.userInteractionEnabled=YES;
         cell.btn_scmi.alpha=0.40;
         cell.btn_scmi.enabled=NO;
         */
        
        [APP_DELEGATE hideLoadingView];
        
        [self viewDidAppear:NO];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    if (IsIphone7 || IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = 145;
    }
    else{
        self.revealViewController.rearViewRevealWidth = 145;
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}


#pragma mark - GetAllPosts
-(void)CallGetEvent
{
    [self.view endEditing:YES];
    [APP_DELEGATE showLoadingView:@""];
    
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    [AddPost setValue:[NSString stringWithFormat:@"%@",page_no] forKey:@"page_no"];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@get_events",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        mutDict=[[NSMutableArray alloc] init];
        mutDict=[responseObject valueForKey:@"data"];
        page_no=[responseObject valueForKey:@"page_no"];
        
        theAppDelegate.n_Count = [NSString stringWithFormat:@"%@",[responseObject valueForKey:@"notification_count"]];
        
        _lbl_notifi_count.text=theAppDelegate.n_Count;
        
        if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
            _lbl_notifi_count.hidden=YES;
            
           
        }
        else
        {
            _lbl_notifi_count.hidden=NO;
            
            _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
            _lbl_notifi_count.clipsToBounds=YES;
        }

        
        if (!(mutDict.count>0)) {
            _tbl_event.hidden=YES;
        }
        
        [_tbl_event reloadData];
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
}

-(void)CallCountry
{
    [APP_DELEGATE showLoadingView:@""];
    [self.view endEditing:YES];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    NSLog(@" PARAM --- %@",AddPost);
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpcountry  = [[HttpWrapper alloc] init];
        httpcountry.delegate=self;
        httpcountry.getbool=NO;
        [httpcountry requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_country",JsonUrlConstant] param:[AddPost copy]];
        
        
        
        
    });
}
-(void)Callstate:(NSString *)str
{
    [APP_DELEGATE showLoadingView:@""];
    [self.view endEditing:YES];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    [AddPost setValue:str forKey:@"country_id"];
    
    NSLog(@" PARAM --- %@",AddPost);
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpstate  = [[HttpWrapper alloc] init];
        httpstate.delegate=self;
        httpstate.getbool=NO;
        [httpstate requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_state"
                                                  ,JsonUrlConstant] param:[AddPost copy]];
    });
    
}
-(void)CallCity:(NSString *)str
{
    [self.view endEditing:YES];
    [APP_DELEGATE showLoadingView:@""];
    [self.view endEditing:YES];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    [AddPost setValue:str forKey:@"state_id"];
    
    NSLog(@" PARAM --- %@",AddPost);
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpcity  = [[HttpWrapper alloc] init];
        httpcity.delegate=self;
        httpcity.getbool=NO;
        [httpcity requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_city"
                                                 ,JsonUrlConstant] param:[AddPost copy]];
    });
    
}

#pragma mark- Wrapper Class Delegate Methods
//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary* )dicsResponse
{
    if(wrapper == httpevent && httpevent != nil)
    {
        NSLog(@"---%@",dicsResponse);
        NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
        mutDict=[[NSMutableArray alloc] init];
        mutDict=[dicsResponse valueForKey:@"data"];
        
        if ([[dicsResponse valueForKey:@"status"] integerValue] == 0) {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Success" message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //[alert show];
            [APP_DELEGATE hideLoadingView];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Success" message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //[alert show];
            
            [_tbl_event reloadData];
            [APP_DELEGATE hideLoadingView];
        }
        [APP_DELEGATE hideLoadingView];
        // last_eventid=[[mutDict valueForKey:@"EventID"]firstObject];
        
        
    }
    else if(wrapper == httpcountry && httpcountry != nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        dict=[dicsResponse valueForKey:@"data"];
        NSMutableDictionary *dictval=[[NSMutableDictionary alloc]init];
        [dictval setValue:@"SelectAll" forKey:@"LocationName"];
        [dictval setValue:@"0" forKey:@"LocationID"];
        country=[[NSMutableArray alloc]init];
        
        //country= [dicsResponse valueForKey:@"data"];
        [country insertObject:dictval atIndex:0];
        for (int i=0; i<[[dicsResponse valueForKey:@"data"] count]; i++) {
            [country insertObject:[[dicsResponse valueForKey:@"data"]objectAtIndex:i] atIndex:i+1];
        }
        
        
        
        
        
        
        
        
        NSLog(@"%@",country);
        [APP_DELEGATE hideLoadingView];
        
    }
    else if (wrapper==httpstate && httpstate !=nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        if ([dicsResponse valueForKey:@"status"]!=0) {
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[dicsResponse valueForKey:@"data"];
            state= [dicsResponse valueForKey:@"data"];
            NSLog(@"%@",state);
            
        }
        [APP_DELEGATE hideLoadingView];
    }
    else if (wrapper==httpcity && httpcity !=nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        if ([dicsResponse valueForKey:@"status"]!=0) {
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[dicsResponse valueForKey:@"data"];
            arrcity= [dicsResponse valueForKey:@"data"];
            NSLog(@"%@",arrcity);
        }
        [APP_DELEGATE hideLoadingView];
    }
    
}


//fetchDataFail
- (void) HttpWrapper:(HttpWrapper* )wrapper fetchDataFail:(NSError* )error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}
-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    //[self CallGetEvent];
    
    _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
    _lbl_notifi_count.clipsToBounds=YES;
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self customSetup];
    page_no=@"0";
    [self CallGetEvent];
    
    
    /*  [self.collection_view performBatchUpdates:^{
     [self.collection_view insertItemsAtIndexPaths:[NSArray arrayWithObjects:
     [NSIndexPath indexPathForRow:0 inSection:0],
     [NSIndexPath indexPathForRow:1 inSection:0],
     [NSIndexPath indexPathForRow:2 inSection:0],
     [NSIndexPath indexPathForRow:3 inSection:0],
     [NSIndexPath indexPathForRow:4 inSection:0],
     [NSIndexPath indexPathForRow:5 inSection:0],
     [NSIndexPath indexPathForRow:6 inSection:0],
     [NSIndexPath indexPathForRow:7 inSection:0],
     
     
     nil]];
     count = 8;
     } completion:^(BOOL finished) {
     [self.collection_view reloadData];
     }];*/
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)CustomizeTextField:(UITextField *)myTextField
{
    myTextField.layer.borderWidth=1;
    myTextField.layer.borderColor=[UIColor colorWithHex:0x196A84].CGColor;
    CGRect frameRect = myTextField.frame;
    frameRect.size.height = 28;
    myTextField.frame = frameRect;
    myTextField.layer.cornerRadius=myTextField.frame.size.height/2;
    
    [myTextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
    myTextField.leftView = paddingView;
    myTextField.leftViewMode = UITextFieldViewModeAlways;
    
}

-(void)setCircularLayout{
    if ([[UIScreen mainScreen] bounds].size.height == 736.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(190, 270)
                                radius:180
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:72];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }
    else  if ([[UIScreen mainScreen] bounds].size.height == 812.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(200, 260)
                                radius:180
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:72];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 667.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(190, 270)
                                radius:180
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:72];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 568.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(158, 230)
                                radius:150
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:58];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 480.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(158, 230)
                                radius:160
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:58];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return mutDict.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Cell";
    //tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.separatorColor = [UIColor clearColor];
    
    EventCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[EventCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    [cell.img_user_prf_img sd_setImageWithURL:[NSURL URLWithString:[mutDict valueForKey:@"UserImageURL"][indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
    
    cell.lbl_usernm.text=[NSString stringWithFormat:@"%@ %@",[[mutDict  valueForKey:@"FName"] objectAtIndex:indexPath.row],[[mutDict  valueForKey:@"LName"] objectAtIndex:indexPath.row]];
    
    
    ///=============================================
    NSString *startDt=[APP_DELEGATE DateCovertforEvent:[mutDict valueForKey:@"StartDate"][indexPath.row]];
    NSString *endDt=[APP_DELEGATE DateCovertforEvent:[mutDict valueForKey:@"EndDate"][indexPath.row]];
    
    cell.lbl_postdate.text=[NSString stringWithFormat:@"%@ - %@",startDt,endDt];
    NSArray *arr = [startDt componentsSeparatedByString:@" "];
    
    cell.lbl_short_mon.text=[arr objectAtIndex:0];
    cell.lbl_short_dt.text=[arr objectAtIndex:1];
    
    
    NSMutableArray *image_arr=[[NSMutableArray alloc]init];
    image_arr=[mutDict valueForKey:@"event_images"][indexPath.row];
    
    cell.scroll_imgs.pagingEnabled=YES;
    
    int x=0;
    
    //Load images on ImageView
    NSLog(@"arr  :: %@", image_arr);
    
    if (image_arr.count>0) {
        for (int k=0; k<[image_arr count]; k++)
        {
            
            NSString * strImage = [[image_arr objectAtIndex:k] valueForKey:@"ImageURL"];
            
            strImage = [strImage stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
            UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(x, 0,cell.scroll_imgs.frame.size.width, cell.scroll_imgs.frame.size.height)];
            
            
            [img sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:@"avatar"]];
            
            
            
            x=x+cell.scroll_imgs.frame.size.width;
            
            [img setContentMode:UIViewContentModeScaleAspectFill];
            [img setClipsToBounds:YES];
            
            [cell.scroll_imgs addSubview:img];
            
        }
    }
    else
    {
        //https://indieglobe.blenzabi.com/uploads/events/
        
        
        
        NSString * strImage = @"https://indieglobe.blenzabi.com/uploads/events/";
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(x, 0,cell.scroll_imgs.frame.size.width, cell.scroll_imgs.frame.size.height)];
        
        
        [img sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
        
        x=x+cell.scroll_imgs.frame.size.width;
        [img setContentMode:UIViewContentModeScaleAspectFill];
        [img setClipsToBounds:YES];//btn_replay.hidden
        
        [cell.scroll_imgs addSubview:img];
    }
    
    
    cell.scroll_imgs.contentSize=CGSizeMake(x, cell.scroll_imgs.frame.size.height);
    cell.scroll_imgs.contentOffset=CGPointMake(0, 0);
    
    
    //==============================================
    
    ///-----------------------------
    
    if ([[[mutDict valueForKey:@"IsLike"] objectAtIndex:indexPath.row] intValue]==1) {
        cell.btn_like.selected=YES;
    }
    else
    {
        cell.btn_like.selected=NO;
    }
    
    
    if ([[[mutDict valueForKey:@"IsDisike"] objectAtIndex:indexPath.row] intValue]==1) {
        cell.btn_dislike.selected=YES;
    }
    else
    {
        cell.btn_dislike.selected=NO;
    }
    
    
    cell.lbl_like.text=[NSString stringWithFormat:@"%@",[[mutDict valueForKey:@"TotalLikes"] objectAtIndex:indexPath.row]];
    
    cell.lbl_dislike.text=[NSString stringWithFormat:@"%@",[[mutDict valueForKey:@"TotalDislikes"] objectAtIndex:indexPath.row]];
    
    
    ////Comment  TotalComment
    
    cell.lbl_comment.text=[NSString stringWithFormat:@"%@",[mutDict valueForKey:@"TotalComments"][indexPath.row]];
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]initWithArray:[[mutDict valueForKey:@"last_comments"] objectAtIndex:indexPath.row]];
    
    //NSLog(@"arrTemp %@",arrTemp);
    
    
    for (int i=0; i<arrTemp.count; i++) {
        switch (i) {
            case 0:
            {
                [cell.img_vw1 sd_setImageWithURL:[NSURL URLWithString:[arrTemp valueForKey:@"ImageURL"][0]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
                
                cell.lbl_title_vw1.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"FName"][0]];
                
                cell.lbl_desc_vw1.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"Comments"][0]];
                
            }
                break;
            case 1:
            {
                [cell.img_vw2 sd_setImageWithURL:[NSURL URLWithString:[arrTemp valueForKey:@"ImageURL"][1]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
                
                cell.lbl_title_vw2.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"FName"][1]];
                
                cell.lbl_desc_vw2.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"Comments"][1]];
            }
                break;
                
            default:
                break;
        }
    }
    
    int C=[[[mutDict valueForKey:@"last_comments"] objectAtIndex:indexPath.row] count];
    
    if (C==2) {
        cell.vw_2.hidden=NO;
        cell.vw_1.hidden=NO;
    }
    else if (C==1)
    {
        cell.vw_1.hidden=NO;
        cell.vw_2.hidden=YES;
        
    }
    else
    {
        
        cell.vw_2.hidden=YES;
        cell.vw_1.hidden=YES;
    }
    
    //////LIKE DISLIKE
    [cell.btn_like setTag:indexPath.row];
    [cell.btn_like addTarget:self action:@selector(CallLike:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.btn_dislike setTag:indexPath.row];
    [cell.btn_dislike addTarget:self action:@selector(CallDisLike:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.btn_comment setTag:indexPath.row];
    [cell.btn_comment addTarget:self action:@selector(ViewComments:) forControlEvents:UIControlEventTouchUpInside];
    
    
    //----------------------------
    
    
    
    
    if ([[[mutDict valueForKey:@"IsRSVP"] objectAtIndex:indexPath.row]isEqualToString:@"1"]) {
        //cell.btn_rsvp.layer.backgroundColor=[UIColor colorWithHex:0xAAAAAA].CGColor;
        cell.btn_rsvp.alpha=0.40;
        //cell.btn_rsvp.enabled=NO;
    }
    else
    {
        cell.btn_rsvp.alpha=1.00;
        //cell.btn_rsvp.enabled=YES;
    }
    
    if ([[[mutDict valueForKey:@"IsSCMI"] objectAtIndex:indexPath.row]isEqualToString:@"1"]) {
        //cell.btn_rsvp.layer.backgroundColor=[UIColor colorWithHex:0xAAAAAA].CGColor;
        cell.btn_scmi.alpha=0.40;
        //cell.btn_scmi.enabled=NO;
    }
    else
    {
        cell.btn_scmi.alpha=1.00;
        //cell.btn_scmi.enabled=YES;
    }
    
    
    
    
    
    cell.lbl_rsvp.text=[NSString stringWithFormat:@"%@ People RSVP",[[mutDict  valueForKey:@"TotalIsRSVP"] objectAtIndex:indexPath.row]];
    
    cell.lbl_title.text=[NSString stringWithFormat:@"%@",[[mutDict  valueForKey:@"Title"] objectAtIndex:indexPath.row]];
    
    cell.lbl_address.text=[NSString stringWithFormat:@"%@",[[mutDict  valueForKey:@"Address"] objectAtIndex:indexPath.row]];
    
    [cell.img_main_image sd_setImageWithURL:[NSURL URLWithString:[mutDict valueForKey:@"EventImageURL"][indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
    
    
    [cell.btn_rsvp addTarget:self action:@selector(rsvpEvent:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btn_scmi addTarget:self action:@selector(scmiEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    
    return cell;
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    int lastRow=[mutDict count]-1;
    if(([indexPath row] == lastRow)&&(lastRow<[mutDict count]))
    {
        //[self Getnewdata];
        NSLog(@"i m call");
        
        if ([mutDict count]>=2) {
            __weak EventVC *weakSelf = self;
            // setup infinite scrolling
            [_tbl_event addInfiniteScrollingWithActionHandler:^{
                [weakSelf CallSearchMethod1];
            }];
        }
        
        last_eventid=[[mutDict valueForKey:@"EventID"]lastObject];
        if ([last_eventid integerValue]<1) {
            _tbl_event.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        }
        //[self CallSearchMethod];
        
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *simpleTableIdentifier = @"CellEvent";
    //tableView.separatorColor = [UIColor clearColor];
    EventCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[EventCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    
    int C=[[[mutDict valueForKey:@"last_comments"] objectAtIndex:indexPath.row] count];
    
    if (C==2) {
        
        return 525;
        
    }
    else if (C==1)
    {
        cell.vw_2.hidden=YES;
        return 525-50;
        
        
        
    }
    else
    {
        cell.vw_2.hidden=YES;
        cell.vw_1.hidden=YES;
        
        return 525-100;
        
        
    }
}

-(void)ViewComments:(id)sender
{
    NSLog(@"ViewComments %@",sender);
    //NSInteger ival=[sender integerValue];
    
    UIButton *btn = (UIButton *)sender;
    CGPoint origin = btn.frame.origin;
    CGPoint point = [btn.superview convertPoint:origin toView:_tbl_event];
    NSIndexPath * indexPath = [_tbl_event indexPathForRowAtPoint:point];
    
    
    
    AllCommentView *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllCommentView"];
    next.user_portfolio_category_id=[mutDict valueForKey:@"EventID"][indexPath.row];
    next.category_nm=@"Events";
    [self.navigationController pushViewController:next animated:NO];
    
    
    
}


-(void)CallLike:(UIButton*)sender
{
    UIButton *btn=sender;
    
    NSLog(@"button was clicked for the following row %li", (long)btn.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[btn tag]  inSection:0];
    NSLog(@"indexpath.row %ld", (long)indexPath.row);
    NSLog(@"indexpath.row %ld", (long)indexPath.section);
    
    
    
    
    /*user_id
     indie_pick_id
     is_like */
    
    NSString *strID=[[mutDict valueForKey:@"EventID"] objectAtIndex:indexPath.row];
    NSString *strISLike=[[mutDict valueForKey:@"IsLike"] objectAtIndex:indexPath.row];
    
    if ([strISLike isEqualToString:@"0"]) {
        strISLike=@"1";
    }
    else
    {
        strISLike=@"0";
    }
    
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:strID forKey:@"event_id"];
    [AddPost setValue:strISLike forKey:@"is_like"];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@events_like",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        
        page_no=@"0";
        [self CallGetEvent];
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
}

-(void)CallDisLike:(UIButton*)sender
{
    UIButton *btn=sender;
    
    NSLog(@"button was clicked for the following row %li", (long)btn.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[btn tag]  inSection:0];
    NSLog(@"indexpath.row %ld", (long)indexPath.row);
    NSLog(@"indexpath.row %ld", (long)indexPath.section);
    
    
    
    
    /*user_id
     indie_pick_id
     is_like */
    
    NSString *strID=[[mutDict valueForKey:@"EventID"] objectAtIndex:indexPath.row];
    NSString *strISLike=[[mutDict valueForKey:@"IsDisike"] objectAtIndex:indexPath.row];
    
    if ([strISLike isEqualToString:@"0"]) {
        strISLike=@"1";
    }
    else
    {
        strISLike=@"0";
    }
    
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:strID forKey:@"event_id"];
    [AddPost setValue:strISLike forKey:@"is_dislike"];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@events_dislike",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        page_no=@"0";
        [self CallGetEvent];
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
}

-(void)rsvpEvent:(id)sender
{
    [self.view endEditing:YES];
    NSLog(@"User Like %@",sender);
    
    //NSInteger ival=[sender integerValue];
    
    UIButton *btn = (UIButton *)sender;
    
    CGPoint origin = btn.frame.origin;
    CGPoint point = [btn.superview convertPoint:origin toView:_tbl_event];
    index_event = [_tbl_event indexPathForRowAtPoint:point];
    
    
    
    [self CallRSVP:index_event.row];
    
}

-(void)scmiEvent:(id)sender
{
    
    [self.view endEditing:YES];
    NSLog(@"Dislike NEWS  %@",sender);
    UIButton *btn = (UIButton *)sender;
    
    CGPoint origin = btn.frame.origin;
    CGPoint point = [btn.superview convertPoint:origin toView:_tbl_event];
    NSIndexPath * indexPath = [_tbl_event indexPathForRowAtPoint:point];
    EventCell *cell=(EventCell *)[_tbl_event cellForRowAtIndexPath:indexPath];
    
    [self CallSCMI:indexPath.row];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.selected) {
        // ... Uncheck
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    
    EventDetailsVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetailsVC"];
    NSArray *arr = [[NSArray alloc] initWithObjects:mutDict, nil][0];
    NSLog(@"arr %@",arr);
    next.mutDict=[arr objectAtIndex:indexPath.row];
    next.FromFlag=@"1";
    
    [self.navigationController pushViewController:next animated:YES];
    
}

#pragma NEW MENU
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 8;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    DSCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    //cell.layer.cornerRadius=cell.frame.size.height/2;
    //cell.layer.borderWidth=1.5;
    //cell.layer.borderColor=[UIColor whiteColor].CGColor;
    cell.img_icon.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",[arrimg objectAtIndex:indexPath.row]]];
    [self playAlertSoundPressed:nil];
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self playAlertSoundPressed:nil];
    [self SelectedMenu:indexPath.row];
}
- (void)SelectedMenu:(NSInteger)index{
    
    NSLog(@"%ld",(long)index);
    
    switch (index) {
        case 0:
        {
            //Perform any action that u want on menu selection
            /* EventVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"EventVC"];
             [self.navigationController pushViewController:next animated:NO];*/
        }
            break;
            
        case 1:{
            NewsVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsVC"];
            [self.navigationController pushViewController:next animated:NO];
            
        }
            
            break;
        case 2:
        {
            CritiquesVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"CritiquesVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 3:
        {
            ClassifiedVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"ClassifiedVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 4:
        {
            LearningVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"LearningVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 5:
        {
            IndieVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"IndieVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 6:
        {
            FundingVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"FundingVC"];
            [self.navigationController pushViewController:next animated:NO];
            
        }
            break;
        case 7:
        {
            TeamBuild1VC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"TeamBuild1VC"];
            [self.navigationController pushViewController:next animated:NO];
            
        }
            break;
            
            
            break;
        default:
            break;
    }
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)btn_HOME:(id)sender {
    NewHomeVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:YES];
}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}


- (IBAction)btn_ADD_EVENT:(id)sender {
    
    AddEventVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"AddEventVC"];
    [self.navigationController pushViewController:next animated:YES];
}



- (IBAction)btn_SCOUNTRY:(id)sender {
    [self.view endEditing:YES];
    isCheck=0;
    [txt_country setTag:0];
    currentTextField = txt_country;
    [self showPopover:sender];
}

- (IBAction)btn_SSTATE:(id)sender {
    [self.view endEditing:YES];
    isCheck=1;
    [txt_state setTag:1];
    currentTextField = txt_state;
    [self showPopover:sender];
}

- (IBAction)btn_NOTIFY:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}


- (IBAction)btncity:(id)sender {
    [self.view endEditing:YES];
    isCheck=2;
    [txt_city setTag:2];
    currentTextField = txt_city;
    [self showPopover:sender];
}

- (void)touchesBegan:(NSSet* )touches withEvent:(UIEvent* )event {
    [self.view endEditing:YES];
    
}
- (void)showPopover:(UIView*)btn;
{
    
    if (popoverController == nil)
    {
        PopOverView *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PopOverView"];
        
        settingsViewController.index = currentTextField.tag;
        if (isCheck == 0) {
            settingsViewController.marrcountry = country;
        }
        else if (isCheck==1)
        {
            settingsViewController.marrstate = state;
        }
        else if (isCheck==2)
        {
            settingsViewController.marrcity = arrcity;
        }
        else{
            NSLog(@"NO DATA FROM API");
        }
        
        
        if ([settingsViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
            settingsViewController.preferredContentSize = CGSizeMake(200, 170);             // iOS 7
        }
        else {
#pragma clang diagnostic pushc
#pragma GCC diagnostic ignored "-Wdeprecated"
            settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 170);      // iOS < 7
#pragma clang diagnostic pop
        }
        settingsViewController.delegates = self;
        
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        
        settingsViewController.modalInPopover = NO;
        popoverController = [[WYPopoverController alloc] initWithContentViewController:settingsViewController];
        
        popoverController.delegate = self;
        
        popoverController.passthroughViews = @[btn];
        popoverController.popoverLayoutMargins = UIEdgeInsetsMake(0, 10, 10 , 10);
        popoverController.wantsDefaultContentAppearance = YES;
        
        [popoverController presentPopoverFromRect:btn.bounds
                                           inView:btn
                         permittedArrowDirections:WYPopoverArrowDirectionAny
                                         animated:YES
                                          options:WYPopoverAnimationOptionFadeWithScale];
    }
    
}

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    popoverController.delegate = nil;
    popoverController = nil;
}

-(void)dismissPopUpViewController
{
    [popoverController dismissPopoverAnimated:YES];
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)Popvalueselected:(NSString*)strValue{
    NSLog(@"POP:%@",strValue);
    
    currentTextField.text = strValue;
    if (isCheck==0) {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [country filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        countryid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        
        txt_state.text=@"";
        txt_city.text=@"";
        
        
        
        if (countryid==0) {
            page_no=@"0";
            [self CallGetEvent];
            _btn_city.userInteractionEnabled=NO;
            _btn_state.userInteractionEnabled=NO;
        }
        else
        {
            _btn_city.userInteractionEnabled=YES;
            _btn_state.userInteractionEnabled=YES;
            [self Callstate:countryid];
            [self CallSearchMethod];
        }
        
        
    }
    else if (isCheck==1)
    {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [state filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        stateid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        txt_city.text=@"";
        [self CallCity:stateid];
        [self CallSearchMethod];
    }
    else if (isCheck==2)
    {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [arrcity filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        cityid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        
        [self CallSearchMethod];
        
    }
    else
    {
        currentTextField.text = strValue;
    }
    
    
}


- (IBAction)btn_NEWMENU:(id)sender {
    [self playAlertSoundPressed:nil];
    if (isShow==false) {
        _collection_view.hidden=NO;
        _img_colle_back.hidden=NO;
        isShow=true;
    }
    else
    {
        _collection_view.hidden=YES;
        _img_colle_back.hidden=YES;
        isShow=false;
    }
    
}
- (IBAction)playAlertSoundPressed:(UIButton *)sender
{
    [[JSQSystemSoundPlayer sharedPlayer] playAlertSoundWithFilename:@"Click2-Sebastian"
                                                      fileExtension:kJSQSystemSoundTypeAIFF
                                                         completion:nil];
}




@end
