//
//  PlatinumInfoVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/25/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlatinumInfoVC : UIViewController

//Outlet
@property (strong, nonatomic) IBOutlet UIButton *btn_monthly;
@property (strong, nonatomic) IBOutlet UIButton *btn_yearly;
@property (strong, nonatomic) IBOutlet UIView *vw_buttons;
@property (strong, nonatomic) IBOutlet UILabel *lbl_monthly;
@property (strong, nonatomic) IBOutlet UILabel *lbl_yearly;
@property (strong, nonatomic) IBOutlet UILabel *lbl_header_title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;

@property int ButtonFlag;
@property int PackageFlag;
@property (nonatomic,retain)NSMutableArray *arrplan;

//Action
- (IBAction)btn_MONTHLY_ACT:(id)sender;
- (IBAction)btn_YEARLY_ACT:(id)sender;
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_NOTIFY:(id)sender;


@end
