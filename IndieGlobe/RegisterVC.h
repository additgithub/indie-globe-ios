//
//  RegisterVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/24/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopOverView.h"
#import "TextFieldValidator.h"
#import "DropDownListView.h"
#import "HttpWrapper.h"
#import "AppDelegate.h"


@interface RegisterVC : UIViewController<UIScrollViewDelegate,UITextFieldDelegate,kDropDownListViewDelegate,HttpWrapperDelegate>
{
    UITextField *currentTextField;
    
    AppDelegate *appdelegate;
    
    int ButtonFlage;
    int PackageFlage;
    
    
    NSMutableArray *arryList;
    DropDownListView * Dropobj;
    
    
    NSMutableArray *arrplan;
    
    
    //HTTP
    HttpWrapper* httpreg,*httcontry,*httpstate,*httpcity,*httpgetplan,*httpprofes,*httpemail;
    
    
    //VAR
    int isCheck;
    NSMutableArray* country;
    IBOutlet UITextField *txt_country;
    NSString *countryid;
    
    NSMutableArray* state;
    IBOutlet UITextField *txt_state;
    NSString * stateid;
    
    NSMutableArray* arrcity;
    IBOutlet UITextField *txt_city;
    NSString * cityid;
    

    NSString *ProfessionalID;
}

@property(nonatomic, strong, readwrite) NSString *environment;
@property(nonatomic, strong, readwrite) NSString *resultText;


//Outlet
@property (strong, nonatomic) IBOutlet UIScrollView *scrollV;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_fname;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_lname;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_email;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_pass;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_confpass;

@property (strong, nonatomic) IBOutlet UITextField *txt_profession;


@property (strong, nonatomic) IBOutlet UISegmentedControl *seg_select_plan;
@property (strong, nonatomic) IBOutlet UIButton *btn_join_now;
@property (strong, nonatomic) IBOutlet UILabel *lbl_info;
@property (strong, nonatomic) IBOutlet UIView *vw_info;
@property (strong, nonatomic) IBOutlet UIButton *btn_gold;
@property (strong, nonatomic) IBOutlet UIButton *btn_diamond;
@property (strong, nonatomic) IBOutlet UIButton *btn_platinum;
@property (strong, nonatomic) IBOutlet UIView *vw_buttons;

@property (strong, nonatomic) IBOutlet UILabel *lbl_mfee;
@property (strong, nonatomic) IBOutlet UILabel *lbl_yfee;



@property (strong, nonatomic) IBOutlet UIImageView *img_monthly;
@property (strong, nonatomic) IBOutlet UIImageView *img_yearly;

@property (strong, nonatomic) IBOutlet UIView *vw_monthy;
@property (strong, nonatomic) IBOutlet UIView *vw_yearly;

@property(nonatomic,assign) NSMutableArray *marrcount;





//Action
- (IBAction)btnplatinum:(id)sender;
- (IBAction)btndiamond:(id)sender;
- (IBAction)btngold:(id)sender;


- (IBAction)btn_BACK:(id)sender;

- (IBAction)btn_LOGIN:(id)sender;
- (IBAction)btn_JOIN_NOW:(id)sender;

- (IBAction)btn_SPROFESSION:(id)sender;
- (IBAction)btn_SCOUNTRY:(id)sender;
- (IBAction)btn_SSTATE:(id)sender;
- (IBAction)btn_SCITY:(id)sender;

- (IBAction)btn_PLANINFO:(id)sender;




-(void)Popvalueselected:(NSString*)strValue;
-(void)Popvalveselected:(NSString*)strValve;
-(void)dismissPopUpViewController;



@end
