//
//  AddClassi.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/22/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "PopOverView.h"
#import "HttpWrapper.h"
#import "ApplicationConstant.h"
#import "NewHomeSearchVC.h"
#import "UzysImageCropperViewController.h"



@interface AddClassi : UIViewController<UzysImageCropperDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPickerViewDelegate,HttpWrapperDelegate,CLLocationManagerDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
{
    UITextField *currentTextField;
    UIView *picker;
    
     NSMutableArray *AddNew;
    
    UIImagePickerController *ci;
    UIPopoverController *popover;
    
    UITextField *textField;
    UIDatePicker *dpDatePicker;


    //HTTP
    HttpWrapper* httpaddclassi,*httpcountry,*httpstate,*httpcity;
    
    //VAR
    UIImage *Isimage;
    
    int isCheck;
    NSMutableArray* country;
    IBOutlet UITextField *txt_country;
    NSString *countryid;
    
    NSMutableArray* state;
    IBOutlet UITextField *txt_state;
    NSString * stateid;
    
    NSMutableArray* arrcity;
    IBOutlet UITextField *txt_city;
    NSString * cityid;
    
    NSMutableArray* categor;
    IBOutlet UITextField *txt_category;
    NSString *categorid;
    
    
    

}


@property (weak, nonatomic) IBOutlet UICollectionView *coll_addClassi;


@property (nonatomic,retain) UzysImageCropperViewController *imgCropperViewController;
@property (nonatomic,retain) UIImagePickerController *imgpicker;

//Outlet
@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;

@property (strong, nonatomic) IBOutlet UIImageView *img_newimg;
@property (strong, nonatomic) IBOutlet UITextField *txt_title;
@property (strong, nonatomic) IBOutlet UITextField *txt_date;
@property (strong, nonatomic) IBOutlet UITextField *txt_contact;
@property (strong, nonatomic) IBOutlet UITextView *txt_location;
@property (strong, nonatomic) IBOutlet UITextView *txt_discr;
@property (strong, nonatomic) IBOutlet UIButton *btn_submit;
@property (strong, nonatomic) IBOutlet UIButton *btn_menu;
@property (strong, nonatomic) IBOutlet UITextField *txt_lat;
@property (strong, nonatomic) IBOutlet UITextField *txt_longi;
@property (strong, nonatomic) IBOutlet UIImageView *img_extra;
@property (weak, nonatomic) IBOutlet UITextField *txt_classi_price;




//Action
- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;
- (IBAction)btn_SCOUNTRY:(id)sender;
- (IBAction)btn_SSTATE:(id)sender;
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_SCITY:(id)sender;
- (IBAction)btn_ADD_CLASSI:(id)sender;
- (IBAction)btn_SCATEGORY:(id)sender;
- (IBAction)btn_NOTIFY:(id)sender;






-(void)Popvalueselected:(NSString*)strValue;
-(void)Popvalveselected:(NSString*)strValve;
-(void)dismissPopUpViewController;

@end
