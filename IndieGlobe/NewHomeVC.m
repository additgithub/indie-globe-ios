//
//  NewHomeVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 2/3/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "NewHomeVC.h"
#import "Constants.h"
#import "DSCircularLayout.h"
#import "DSCollectionViewCell.h"
#import <JSQSystemSoundPlayer/JSQSystemSoundPlayer.h>

//---------------------
#import "DeshboardVC.h"
#import "ProfileVC.h"
#import "CritiquesVC.h"
#import "ClassifiedVC.h"
#import "PackageList.h"
#import "LearningVC.h"
#import "EventVC.h"
#import "TeamBuildVC.h"
#import "UIViewController+ENPopUp.h"
#import "MyEventVC.h"
#import "SettingVC.h"
#import "NewProfileVC.h"
#import "IndieVC.h"
#import "FundingVC.h"
#import "PlatinumInfoVC.h"
#import "TeamBuildVC.h"
#import "TeamBuild1VC.h"
#import "NewsCell.h"
#import "HnewsCell.h"
#import "HcritiCell.h"
#import "HeventCell.h"
#import "HclassiCell.h"
#import "EventDetailsVC.h"
#import "HfundCell.h"
#import "ClassInfoVC.h"
#import "CritiInfoVC.h"
#import "HindiCell.h"
#import "HportCell.h"
#import "ProfilePhotoViewVC.h"
#import "NewHomeSearchVC.h"
#import "AllCommentView.h"

#import "YKMediaPlayerKit.h"
#import "YKVimeoVideo.h"
#import "YKYouTubeVideo.h"
#import "YKDirectVideo.h"
#import "WebPlayerVC.h"
#import "AllNotificationVC.h"
#import <Crashlytics/Crashlytics.h>

#import "ProfilePhotoForHome.h"
#import "AFTPagingBaseViewController.h"
#import "JhtFloatingBall.h"
#import "ProfileVC.h"



#define TRANSFORM_CELL_VALUE CGAffineTransformMakeScale(0.1, 0.1)
#define ANIMATION_SPEED 0.2



#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height
#define ITEM_WIDTH 75
#define ITEM_HEIGHT 75

@interface NewHomeVC ()

@end

@implementation NewHomeVC
{
    int count;
    BOOL isShow;
    
    YKYouTubeVideo  *_youTubeVideo;
    YKVimeoVideo    *_vimeoVideo;
    YKDirectVideo   *_directVideo;
    YKDirectVideo   *_otherVideo;
    YKUnKnownVideo  *_unknownVideo;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    // Do any additional setup after loading the view.
    
    
    
    [self customSetup];
    //NEW MENU
    count = 0;
    isShow=false;
    [self setCircularLayout];
    arrimg= [[NSMutableArray alloc]initWithObjects:@"evets4553.png",@"news4557.png",@"critique4552.png",@"classified4551.png",@"learning4556.png",@"indie-pic4555.png",@"global4554.png",@"team4558.png",nil];
    
    
    
    [self CallNewHome];
    
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];
    
    
    NSString *strPath=[self getFilePath];
    NSLog(@"----File Path %@",strPath);
    
    
    _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
    _lbl_notifi_count.clipsToBounds=YES;
    
    
    
    
    //[self createUI];
    
}



-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [newRefrence setValue:@"1"];
    
    [self customSetup];
    [self CallNewHome];
}

- (NSString *) getFilePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [paths objectAtIndex:0];;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if (IsIphone7 || IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = 145;
    }
    else{
        self.revealViewController.rearViewRevealWidth = 145;
    }
}
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}

-(void)CallNewHome
{
    [APP_DELEGATE showLoadingView:@""];
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"PARAM---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@get_home_screen_list",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        arrresp=[responseObject valueForKey:@"data"];
        
        if (!(arrresp.count>0)) {
            _tbl_newhome.hidden=YES;
        }
        
        
        
        theAppDelegate.n_Count = [NSString stringWithFormat:@"%@",[responseObject valueForKey:@"notification_count"]];
        
        _lbl_notifi_count.text=theAppDelegate.n_Count;
        
        if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
            _lbl_notifi_count.hidden=YES;
        }
        else
        {
            _lbl_notifi_count.hidden=NO;
        }
        
        
        arrcriti=[[NSMutableArray alloc]init];
        arrevent=[[NSMutableArray alloc]init];
        arrclass=[[NSMutableArray alloc]init];
        arrlearn=[[NSMutableArray alloc]init];
        arrfunding=[[NSMutableArray alloc]init];
        arrindie=[[NSMutableArray alloc]init];
        arrport=[[NSMutableArray alloc]init];
        
        
        
        
        for (int i=0; i<arrresp.count; i++) {
            if ([[[arrresp valueForKey:@"Type"]objectAtIndex:i]isEqualToString:@"Critiques"]) {
                [arrcriti addObject:arrresp[i]];
            }
            else if ([[[arrresp valueForKey:@"Type"]objectAtIndex:i]isEqualToString:@"Events"])
            {
                [arrevent addObject:arrresp[i]];
            }
            else if ([[[arrresp valueForKey:@"Type"]objectAtIndex:i]isEqualToString:@"Classified"])
            {
                [arrclass addObject:arrresp[i]];
            }
            else if ([[[arrresp valueForKey:@"Type"]objectAtIndex:i]isEqualToString:@"LearningCenter"])
            {
                [arrlearn addObject:arrresp[i]];
            }
            else if ([[[arrresp valueForKey:@"Type"]objectAtIndex:i]isEqualToString:@"FundRaising"])
            {
                [arrfunding addObject:arrresp[i]];
            }
            else if ([[[arrresp valueForKey:@"Type"]objectAtIndex:i]isEqualToString:@"IndiePick"])
            {
                [arrindie addObject:arrresp[i]];
            }
            else if ([[[arrresp valueForKey:@"Type"]objectAtIndex:i]isEqualToString:@"Portfolio"])
            {
                [arrport addObject:arrresp[i]];
            }
        }
        
        
        NSLog(@" arrcriti %@",arrcriti );
        NSLog(@" arrevent %@",arrevent );
        NSLog(@" arrclass %@",arrclass );
        NSLog(@" arrlearn %@",arrlearn );
        NSLog(@" arrfunding %@",arrfunding);
        NSLog(@" arrport %@",arrport);
        
        [_tbl_newhome reloadData];
        [APP_DELEGATE hideLoadingView];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
        
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //return 4;
    return arrresp.count;
}
-(void)gestureHandlerMethod:(UITapGestureRecognizer*)sender {
    
    [self CallTeamList:[[arrresp valueForKey:@"PostedBy"] objectAtIndex:sender.view.tag]];
 
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([[[arrresp valueForKey:@"Type"] objectAtIndex:indexPath.row]isEqualToString:@"Critiques"]) {
        
        
        static NSString *simpleTableIdentifier = @"CellCriti";
        //tableView.separatorColor = [UIColor clearColor];
        HcritiCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[HcritiCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        cell.lbl_usernm.text=[NSString stringWithFormat:@"%@ Posted a CRITIQUES",[[arrresp  valueForKey:@"FName"] objectAtIndex:indexPath.row]];
        [cell.img_prof sd_setImageWithURL:[NSURL URLWithString:[[arrresp valueForKey:@"UserImageURL"] objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
        cell.img_prof.tag=indexPath.row;
        cell.img_prof.userInteractionEnabled = YES;
        [cell.img_prof addGestureRecognizer:tapRecognizer];
        
        [cell.img_image sd_setImageWithURL:[NSURL URLWithString:[[arrresp valueForKey:@"Image"] objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
        
        [cell.img_image setContentMode:UIViewContentModeScaleAspectFill];
        [cell.img_image setClipsToBounds:YES];
        
        cell.lbl_disc.text=[NSString stringWithFormat:@"%@",[[arrresp  valueForKey:@"Title"] objectAtIndex:indexPath.row]];
        
        cell.lbl_comment.text=[NSString stringWithFormat:@"%@ Comments",[[arrresp  valueForKey:@"TotalComment"] objectAtIndex:indexPath.row]];
        
        
        /*   //DATE CONVERT
         NSString *strdt=[arrresp valueForKey:@"CreatedOn"][indexPath.row];
         NSDateFormatter * formatter =  [[NSDateFormatter alloc] init];
         [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
         NSDate * convrtedDate = [formatter dateFromString:strdt];
         [formatter setDateFormat:@"MMMM dd"];
         NSString *dateString = [formatter stringFromDate:convrtedDate];
         
         cell.lbl_postdt.text=[NSString stringWithFormat:@"%@",dateString];
         //NSURL *movieURL = [NSURL URLWithString:[[arrcriti  valueForKey:@"Image"] objectAtIndex:indexPath.row]];
         //cell.img_image.image= [self imageFromMovie:movieURL atTime:5.0];
         NSLog(@"index path %@",[arrresp objectAtIndex:indexPath.row]);*/
        
        
        
        //================DATE TIME CALCULATION------------
        NSString * yourJSONString = [NSString stringWithFormat:@"%@",[[arrresp  valueForKey:@"CreatedOn"] objectAtIndex:indexPath.row]];
        
        NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
        [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *dateFromString = [currentDTFormatter dateFromString:yourJSONString];
        [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *eventDateStr = [currentDTFormatter stringFromDate:dateFromString];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSDate *currentDate = [NSDate date];
        NSString *start = [formatter stringFromDate:dateFromString];
        NSString *end = [formatter stringFromDate:currentDate];
        //NSString *end = @"2018-01-11";
        
        
        NSDateFormatter *f = [[NSDateFormatter alloc] init];
        [f setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *startDate = [f dateFromString:start];
        NSDate *endDate = [f dateFromString:end];
        
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [gregorianCalendar components:(NSCalendarUnitDay |NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitWeekOfYear)
                                                            fromDate:startDate
                                                              toDate:endDate
                                                             options:0];
        
        
        NSLog(@"Month %ld", [components weekOfYear]);
        NSLog(@"Day %ld", [components day]);
        NSLog(@"Hour %ld", [components hour]);
        NSLog(@"Minute %ld", [components minute]);
        
        if ([components month]>0) {
            cell.lbl_postdt.text=[NSString stringWithFormat:@"%ld week ago",(long)[components weekOfYear]];
        }
        else if ([components day]>0) {
            cell.lbl_postdt.text=[NSString stringWithFormat:@"%ld Day ago",(long)[components day]];
        }
        else if ([components hour]>0) {
            cell.lbl_postdt.text=[NSString stringWithFormat:@"%ld hours ago",(long)[components hour]];
        }
        else if ([components minute]>0)
        {
            cell.lbl_postdt.text=[NSString stringWithFormat:@"%ld min ago",(long)[components minute]];
        }
        
        
        
        if ([[[arrresp valueForKey:@"IsLike"] objectAtIndex:indexPath.row] intValue]==1) {
            cell.btn_like.selected=YES;
        }
        else
        {
            cell.btn_like.selected=NO;
        }
        
        
        if ([[[arrresp valueForKey:@"IsDislike"] objectAtIndex:indexPath.row] intValue]==1) {
            cell.btn_dislike.selected=YES;
        }
        else
        {
            cell.btn_dislike.selected=NO;
        }
        
        
        cell.lbl_likecount.text=[NSString stringWithFormat:@"%@",[[arrresp valueForKey:@"TotalLike"] objectAtIndex:indexPath.row]];
        
        cell.lbl_dislikecount.text=[NSString stringWithFormat:@"%@",[[arrresp valueForKey:@"TotalDislike"] objectAtIndex:indexPath.row]];
        
        
        ////Comment  TotalComment
        
        cell.lbl_comment_count.text=[NSString stringWithFormat:@"%@",[arrresp valueForKey:@"TotalComment"][indexPath.row]];
        
        //////LIKE DISLIKE
        [cell.btn_like setTag:indexPath.row];
        [cell.btn_like addTarget:self action:@selector(likeNews:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.btn_dislike setTag:indexPath.row];
        [cell.btn_dislike addTarget:self action:@selector(DislikeNews:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.btn_commnent setTag:indexPath.row];
        [cell.btn_commnent addTarget:self action:@selector(ViewComments:) forControlEvents:UIControlEventTouchUpInside];
        
        
        NSMutableArray *arrTemp=[[NSMutableArray alloc]initWithArray:[[arrresp valueForKey:@"last_comments"] objectAtIndex:indexPath.row]];
        
        //NSLog(@"arrTemp %@",arrTemp);
        
        
        for (int i=0; i<arrTemp.count; i++) {
            switch (i) {
                case 0:
                {
                    [cell.img_vw1 sd_setImageWithURL:[NSURL URLWithString:[arrTemp valueForKey:@"ImageURL"][0]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
                    
                    cell.lbl_nm_vw1.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"FName"][0]];
                    
                    cell.lbl_desc_vw1.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"Comments"][0]];
                    
                }
                    break;
                case 1:
                {
                    [cell.img_vw2 sd_setImageWithURL:[NSURL URLWithString:[arrTemp valueForKey:@"ImageURL"][1]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
                    
                    cell.lbl_nm_vw2.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"FName"][1]];
                    
                    cell.lbl_desc_vw2.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"Comments"][1]];
                }
                    break;
                    
                default:
                    break;
            }
        }
        
        int C=[[[arrresp valueForKey:@"last_comments"] objectAtIndex:indexPath.row] count];
        
        if (C==2) {
            cell.vw2.hidden=NO;
            cell.vw1.hidden=NO;
        }
        else if (C==1)
        {
            cell.vw1.hidden=NO;
            cell.vw2.hidden=YES;
            
        }
        else
        {
            
            cell.vw2.hidden=YES;
            cell.vw1.hidden=YES;
        }
        
        
        return  cell;
    }
    else if ([[[arrresp valueForKey:@"Type"] objectAtIndex:indexPath.row]isEqualToString:@"Events"])
    {
        static NSString *simpleTableIdentifier = @"CellEvent";
        //tableView.separatorColor = [UIColor clearColor];
        HeventCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[HeventCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        cell.lbl_usernm.text=[NSString stringWithFormat:@"%@ Posted an EVENT",[[arrresp  valueForKey:@"FName"] objectAtIndex:indexPath.row]];
        
        [cell.img_prof sd_setImageWithURL:[NSURL URLWithString:[[arrresp valueForKey:@"UserImageURL"] objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
        cell.img_prof.tag=indexPath.row;
        cell.img_prof.userInteractionEnabled = YES;
        [cell.img_prof addGestureRecognizer:tapRecognizer];
        
        [cell.img_image setContentMode:UIViewContentModeScaleAspectFill];
        [cell.img_image setClipsToBounds:YES];
        
        [cell.img_image sd_setImageWithURL:[NSURL URLWithString:[[arrresp valueForKey:@"Image"] objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
        
        
        
        
        cell.lbl_title.text=[NSString stringWithFormat:@"%@",[[arrresp  valueForKey:@"Title"] objectAtIndex:indexPath.row]];
        cell.lbl_totrsvp.text=[NSString stringWithFormat:@"%@ People RSVP",[[arrresp  valueForKey:@"TotalIsRSVP"] objectAtIndex:indexPath.row]];
        
        //================DATE TIME CALCULATION------------
        
        NSString * yourJSONString = [NSString stringWithFormat:@"%@",[[arrresp  valueForKey:@"CreatedOn"] objectAtIndex:indexPath.row]];
        
        NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
        [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *dateFromString = [currentDTFormatter dateFromString:yourJSONString];
        [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *eventDateStr = [currentDTFormatter stringFromDate:dateFromString];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSDate *currentDate = [NSDate date];
        NSString *start = [formatter stringFromDate:dateFromString];
        NSString *end = [formatter stringFromDate:currentDate];
        //NSString *end = @"2018-01-11";
        
        NSDateFormatter *f = [[NSDateFormatter alloc] init];
        [f setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *startDate = [f dateFromString:start];
        NSDate *endDate = [f dateFromString:end];
        
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [gregorianCalendar components:(NSCalendarUnitDay |NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitWeekOfYear)
                                                            fromDate:startDate
                                                              toDate:endDate
                                                             options:0];
        
        
        NSLog(@"Day %ld", [components weekOfYear]);
        NSLog(@"Day %ld", [components day]);
        NSLog(@"Hour %ld", [components hour]);
        NSLog(@"Minute %ld", [components minute]);
        
        if ([components month]>0) {
            cell.lbl_createon.text=[NSString stringWithFormat:@"%ld week ago",(long)[components weekOfYear]];
        }
        else if ([components day]>0) {
            cell.lbl_createon.text=[NSString stringWithFormat:@"%ld Day ago",(long)[components day]];
        }
        else if ([components hour]>0) {
            cell.lbl_createon.text=[NSString stringWithFormat:@"%ld hours ago",(long)[components hour]];
        }
        else if ([components minute]>0)
        {
            cell.lbl_createon.text=[NSString stringWithFormat:@"%ld min ago",(long)[components minute]];
        }
        
        
        //-------------------------
        
        cell.lbl_like.text=[NSString stringWithFormat:@"%@",[[arrresp  valueForKey:@"TotalLike"] objectAtIndex:indexPath.row]];
        cell.lbl_dislike.text=[NSString stringWithFormat:@"%@",[[arrresp  valueForKey:@"TotalDislike"] objectAtIndex:indexPath.row]];
        cell.lbl_comment.text=[NSString stringWithFormat:@"%@",[[arrresp  valueForKey:@"TotalComment"] objectAtIndex:indexPath.row]];
        
        //////LIKE DISLIKE
        [cell.btn_like setTag:indexPath.row];
        [cell.btn_like addTarget:self action:@selector(likeNews:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.btn_dislike setTag:indexPath.row];
        [cell.btn_dislike addTarget:self action:@selector(DislikeNews:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.btn_comment setTag:indexPath.row];
        [cell.btn_comment addTarget:self action:@selector(ViewComments:) forControlEvents:UIControlEventTouchUpInside];
        
        if ([[[arrresp valueForKey:@"IsLike"] objectAtIndex:indexPath.row] intValue]==1) {
            cell.btn_like.selected=YES;
        }
        else
        {
            cell.btn_like.selected=NO;
        }
        
        
        if ([[[arrresp valueForKey:@"IsDislike"] objectAtIndex:indexPath.row] intValue]==1) {
            cell.btn_dislike.selected=YES;
        }
        else
        {
            cell.btn_dislike.selected=NO;
        }
        
        
        
        
        
        
        NSMutableArray *arrTemp=[[NSMutableArray alloc]initWithArray:[[arrresp valueForKey:@"last_comments"] objectAtIndex:indexPath.row]];
        
        //NSLog(@"arrTemp %@",arrTemp);
        
        
        for (int i=0; i<arrTemp.count; i++) {
            switch (i) {
                case 0:
                {
                    [cell.img_vw1 sd_setImageWithURL:[NSURL URLWithString:[arrTemp valueForKey:@"ImageURL"][0]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
                    
                    cell.lbl_nm_vw1.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"FName"][0]];
                    
                    cell.lbl_desc_vw1.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"Comments"][0]];
                    
                }
                    break;
                case 1:
                {
                    [cell.img_vw2 sd_setImageWithURL:[NSURL URLWithString:[arrTemp valueForKey:@"ImageURL"][1]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
                    
                    cell.lbl_nm_vw2.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"FName"][1]];
                    
                    cell.lbl_desc_vw2.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"Comments"][1]];
                }
                    break;
                    
                default:
                    break;
            }
        }
        
        int C=[[[arrresp valueForKey:@"last_comments"] objectAtIndex:indexPath.row] count];
        
        if (C==2) {
            cell.vw2.hidden=NO;
            cell.vw1.hidden=NO;
        }
        else if (C==1)
        {
            cell.vw1.hidden=NO;
            cell.vw2.hidden=YES;
            
        }
        else
        {
            
            cell.vw2.hidden=YES;
            cell.vw1.hidden=YES;
        }
        //--------------------------
        
        //DATE CONVERT
        NSString *strdt=[arrresp valueForKey:@"StartDate"][indexPath.row];
        NSDateFormatter * formatter2 =  [[NSDateFormatter alloc] init];
        [formatter2 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate * convrtedDate = [formatter2 dateFromString:strdt];
        [formatter2 setDateFormat:@"MMMM dd"];
        NSString *dateString = [formatter2 stringFromDate:convrtedDate];
        
        
        
        //DATE CONVERT
        NSString *strdt1=[arrresp valueForKey:@"EndDate"][indexPath.row];
        NSDateFormatter * formatter1 =  [[NSDateFormatter alloc] init];
        [formatter1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate * convrtedDate1 = [formatter1 dateFromString:strdt1];
        [formatter1 setDateFormat:@"MMMM dd"];
        NSString *dateString1 = [formatter1 stringFromDate:convrtedDate1];
        cell.lbl_postdt1.text=[NSString stringWithFormat:@"%@",dateString1];
        //NSURL *movieURL = [NSURL URLWithString:[[arrcriti  valueForKey:@"Image"] objectAtIndex:indexPath.row]];
        //cell.img_image.image= [self imageFromMovie:movieURL atTime:5.0];
        cell.lbl_postdt.text=[NSString stringWithFormat:@"%@-%@",dateString,dateString1];
        
        NSLog(@"index path %@",[arrresp objectAtIndex:indexPath.row]);
        
        return  cell;
    }
    else if ([[[arrresp valueForKey:@"Type"] objectAtIndex:indexPath.row]isEqualToString:@"Classified"])
    {
        static NSString *simpleTableIdentifier = @"CellClassi";
        //tableView.separatorColor = [UIColor clearColor];
        HclassiCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[HclassiCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        cell.lbl_usernm.text=[NSString stringWithFormat:@"%@ Posted a CLASSIFIED",[[arrresp  valueForKey:@"FName"] objectAtIndex:indexPath.row]];
        
        cell.lbl_sell_price.text=[NSString stringWithFormat:@"$ %@",[[arrresp  valueForKey:@"Price"] objectAtIndex:indexPath.row]];
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
        cell.img_prof.tag=indexPath.row;
        cell.img_prof.userInteractionEnabled = YES;
        [cell.img_prof addGestureRecognizer:tapRecognizer];
        
        [cell.img_prof sd_setImageWithURL:[NSURL URLWithString:[[arrresp valueForKey:@"UserImageURL"] objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
        
        [cell.img_image sd_setImageWithURL:[NSURL URLWithString:[[arrresp valueForKey:@"Image"] objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
        
        [cell.img_image setContentMode:UIViewContentModeScaleAspectFill];
        [cell.img_image setClipsToBounds:YES];
        
        cell.lbl_title.text=[NSString stringWithFormat:@"%@",[[arrresp  valueForKey:@"Title"] objectAtIndex:indexPath.row]];
        cell.lbl_disc.text=[NSString stringWithFormat:@"%@",[[arrresp  valueForKey:@"ShortDesc"] objectAtIndex:indexPath.row]];
        cell.lbl_location.text=[NSString stringWithFormat:@"%@",[[arrresp  valueForKey:@"CityName"] objectAtIndex:indexPath.row]];
        
        //////////AUTO SPACING
        
        CGRect autosize4 = cell.lbl_disc.frame;
        autosize4.size.height = [self heightForText:[NSString stringWithFormat:@"%@",[arrresp valueForKey:@"Description"][indexPath.row]]];
        cell.lbl_disc.frame = autosize4;
        
        [cell.lbl_disc setNumberOfLines:0];
        [cell.lbl_disc sizeToFit];
        
        //NSURL *movieURL = [NSURL URLWithString:[[arrcriti  valueForKey:@"Image"] objectAtIndex:indexPath.row]];
        //cell.img_image.image= [self imageFromMovie:movieURL atTime:5.0];
        
        //================DATE TIME CALCULATION------------
        
        NSString * yourJSONString = [NSString stringWithFormat:@"%@",[[arrresp  valueForKey:@"CreatedOn"] objectAtIndex:indexPath.row]];
        
        NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
        [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *dateFromString = [currentDTFormatter dateFromString:yourJSONString];
        [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *eventDateStr = [currentDTFormatter stringFromDate:dateFromString];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSDate *currentDate = [NSDate date];
        NSString *start = [formatter stringFromDate:dateFromString];
        NSString *end = [formatter stringFromDate:currentDate];
        //NSString *end = @"2018-01-11";
        
        NSDateFormatter *f = [[NSDateFormatter alloc] init];
        [f setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *startDate = [f dateFromString:start];
        NSDate *endDate = [f dateFromString:end];
        
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [gregorianCalendar components:(NSCalendarUnitDay |NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitWeekOfYear)
                                                            fromDate:startDate
                                                              toDate:endDate
                                                             options:0];
        
        NSLog(@"Day %ld", [components day]);
        NSLog(@"Hour %ld", [components hour]);
        NSLog(@"Minute %ld", [components minute]);
        
        
        
        if ([components weekOfYear]>0) {
            cell.lbl_postdt.text=[NSString stringWithFormat:@"%ld week ago",(long)[components weekOfYear]];
        }
        else if ([components day]>0) {
            cell.lbl_postdt.text=[NSString stringWithFormat:@"%ld Day ago",(long)[components day]];
        }
        else if ([components hour]>0) {
            cell.lbl_postdt.text=[NSString stringWithFormat:@"%ld hours ago",(long)[components hour]];
        }
        else if ([components minute]>0)
        {
            cell.lbl_postdt.text=[NSString stringWithFormat:@"%ld min ago",(long)[components minute]];
        }
        
        NSLog(@"index path %@",[arrresp objectAtIndex:indexPath.row]);
        return  cell;
    }
    else if ([[[arrresp valueForKey:@"Type"] objectAtIndex:indexPath.row]isEqualToString:@"LearningCenter"])
    {
        static NSString *simpleTableIdentifier = @"CellNews";
        //tableView.separatorColor = [UIColor clearColor];
        HnewsCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[HnewsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        [cell.btn_play_v setTag:indexPath.row];
        [cell.btn_play_v addTarget:self action:@selector(yourButtonClicked:)forControlEvents:UIControlEventTouchUpInside];
        
        cell.lbl_usernm.text=[NSString stringWithFormat:@"%@ Shared Learning",[[arrresp  valueForKey:@"FName"] objectAtIndex:indexPath.row]];
        
        [cell.img_prof sd_setImageWithURL:[NSURL URLWithString:[[arrresp valueForKey:@"UserImageURL"] objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
        cell.img_prof.tag=indexPath.row;
        cell.img_prof.userInteractionEnabled = YES;
        [cell.img_prof addGestureRecognizer:tapRecognizer];
        //[cell.img_image sd_setImageWithURL:[NSURL URLWithString:[[arrresp valueForKey:@"Image"] objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
        cell.lbl_title.text=[NSString stringWithFormat:@"%@",[[arrresp  valueForKey:@"Title"] objectAtIndex:indexPath.row]];
        
        NSString *str=[NSString stringWithFormat:@"%@", [[arrresp  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];  //is your str
        if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
            
            //NSURL *movieURL = [NSURL URLWithString:[[arrresp  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
            
            [cell.img_image sd_setImageWithURL:[NSString stringWithFormat:@"%@",[self extractYoutubeIdFromLink:str]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
            
            //Thard party thambanil
            /*  _youTubeVideo = [[YKYouTubeVideo alloc] initWithContent:[NSURL URLWithString:[[arrresp  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]]];
             [_youTubeVideo parseWithCompletion:^(NSError *error) {
             [_youTubeVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
             cell.img_image.image = thumbImage;
             }];
             }];
             */
        }
        else
        {
            //NSURL *movieURL = [NSURL URLWithString:[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
            _directVideo = [[YKDirectVideo alloc] initWithContent:[NSURL URLWithString:[[arrresp  valueForKey:@"Image"] objectAtIndex:indexPath.row]]];
            [_directVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                cell.img_image.image = thumbImage;
            }];
        }
        
        
        
        
        
        /*
         //Thard party thambanil
         _youTubeVideo = [[YKYouTubeVideo alloc] initWithContent:[NSURL URLWithString:[[arrresp  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]]];
         [_youTubeVideo parseWithCompletion:^(NSError *error) {
         [_youTubeVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
         cell.img_image.image = thumbImage;
         }];
         }];
         */
        //NSURL *movieURL = [NSURL URLWithString:[[arrcriti  valueForKey:@"Image"] objectAtIndex:indexPath.row]];
        //cell.img_image.image= [self imageFromMovie:movieURL atTime:5.0];
        
        
        
        //================DATE TIME CALCULATION------------
        
        NSString * yourJSONString = [NSString stringWithFormat:@"%@",[[arrresp  valueForKey:@"CreatedOn"] objectAtIndex:indexPath.row]];
        
        NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
        [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *dateFromString = [currentDTFormatter dateFromString:yourJSONString];
        [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *eventDateStr = [currentDTFormatter stringFromDate:dateFromString];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSDate *currentDate = [NSDate date];
        NSString *start = [formatter stringFromDate:dateFromString];
        NSString *end = [formatter stringFromDate:currentDate];
        //NSString *end = @"2018-01-11";
        
        NSDateFormatter *f = [[NSDateFormatter alloc] init];
        [f setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *startDate = [f dateFromString:start];
        NSDate *endDate = [f dateFromString:end];
        
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [gregorianCalendar components:(NSCalendarUnitDay |NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitWeekOfYear)
                                                            fromDate:startDate
                                                              toDate:endDate
                                                             options:0];
        
        NSLog(@"Day %ld", [components day]);
        NSLog(@"Hour %ld", [components hour]);
        NSLog(@"Minute %ld", [components minute]);
        
        if ([components weekOfYear]>0) {
            cell.lbl_postdt.text=[NSString stringWithFormat:@"%ld week ago",(long)[components weekOfYear]];
        }
        else if ([components day]>0) {
            cell.lbl_postdt.text=[NSString stringWithFormat:@"%ld Day ago",(long)[components day]];
        }
        else if ([components hour]>0) {
            cell.lbl_postdt.text=[NSString stringWithFormat:@"%ld hours ago",(long)[components hour]];
        }
        else if ([components minute]>0)
        {
            cell.lbl_postdt.text=[NSString stringWithFormat:@"%ld min ago",(long)[components minute]];
        }
        
        
        
        NSLog(@"index path %@",[arrresp objectAtIndex:indexPath.row]);
        
        return  cell;
    }
    else if ([[[arrresp valueForKey:@"Type"] objectAtIndex:indexPath.row]isEqualToString:@"FundRaising"])
    {
        static NSString *simpleTableIdentifier = @"CellFunding";
        //tableView.separatorColor = [UIColor clearColor];
        HfundCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[HfundCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        
        cell.btn_follow.tag=indexPath.row;
        [cell.btn_follow addTarget:self action:@selector(yourButtonClicked1:) forControlEvents:UIControlEventTouchUpInside];
        
        
        cell.btn_donet.tag = indexPath.row;
        [cell.btn_donet addTarget:self action:@selector(yourButtonFunding:) forControlEvents:UIControlEventTouchUpInside];
        
        
        cell.lbl_usernm.text=[NSString stringWithFormat:@"%@ Posted Funding",[[arrresp  valueForKey:@"FName"] objectAtIndex:indexPath.row]];
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
        cell.img_prof.tag=indexPath.row;
        cell.img_prof.userInteractionEnabled = YES;
        [cell.img_prof addGestureRecognizer:tapRecognizer];
        
        [cell.img_prof sd_setImageWithURL:[NSURL URLWithString:[[arrresp valueForKey:@"UserImageURL"] objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
        
        [cell.img_image sd_setImageWithURL:[NSURL URLWithString:[[arrresp valueForKey:@"Image"] objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
        
        [cell.img_image setContentMode:UIViewContentModeScaleAspectFill];
        [cell.img_image setClipsToBounds:YES];
        
        cell.lbl_title.text=[NSString stringWithFormat:@"%@",[[arrresp  valueForKey:@"Title"] objectAtIndex:indexPath.row]];
        
        cell.lbl_desc.text=[NSString stringWithFormat:@"%@",[[arrresp valueForKey:@"ShortDesc"]objectAtIndex:indexPath.row]];
        
        cell.lbl_amt.text=[NSString stringWithFormat:@"GOAL:%@",[[arrresp  valueForKey:@"GoalAmount"] objectAtIndex:indexPath.row]];
        
        cell.btn_donet.tag = indexPath.row;
        [cell.btn_donet addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        //NSURL *movieURL = [NSURL URLWithString:[[arrcriti  valueForKey:@"Image"] objectAtIndex:indexPath.row]];
        //cell.img_image.image= [self imageFromMovie:movieURL atTime:5.0];
        
        
        if ([[[arrresp valueForKey:@"IsFollow"] objectAtIndex:indexPath.row]isEqualToString:@"0"]) {
            [cell.btn_follow setTitle: @"FOLLOW" forState: UIControlStateNormal];
        }
        else
        {
            [cell.btn_follow setTitle: @"UNFOLLOW" forState: UIControlStateNormal];
        }
        
        //================DATE TIME CALCULATION------------
        
        NSString * yourJSONString = [NSString stringWithFormat:@"%@",[[arrresp  valueForKey:@"CreatedOn"] objectAtIndex:indexPath.row]];
        
        NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
        [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *dateFromString = [currentDTFormatter dateFromString:yourJSONString];
        [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *eventDateStr = [currentDTFormatter stringFromDate:dateFromString];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSDate *currentDate = [NSDate date];
        NSString *start = [formatter stringFromDate:dateFromString];
        NSString *end = [formatter stringFromDate:currentDate];
        //NSString *end = @"2018-01-11";
        
        NSDateFormatter *f = [[NSDateFormatter alloc] init];
        [f setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *startDate = [f dateFromString:start];
        NSDate *endDate = [f dateFromString:end];
        
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [gregorianCalendar components:(NSCalendarUnitDay |NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitWeekOfYear)
                                                            fromDate:startDate
                                                              toDate:endDate
                                                             options:0];
        
        NSLog(@"Day %ld", [components day]);
        NSLog(@"Hour %ld", [components hour]);
        NSLog(@"Minute %ld", [components minute]);
        
        if ([components weekOfYear]>0) {
            cell.lbl_postdt.text=[NSString stringWithFormat:@"%ld Week ago",(long)[components weekOfYear]];
        }
        else if ([components day]>0) {
            cell.lbl_postdt.text=[NSString stringWithFormat:@"%ld Day ago",(long)[components day]];
        }
        else if ([components hour]>0) {
            cell.lbl_postdt.text=[NSString stringWithFormat:@"%ld hours ago",(long)[components hour]];
        }
        else if ([components minute]>0)
        {
            cell.lbl_postdt.text=[NSString stringWithFormat:@"%ld min ago",(long)[components minute]];
        }
        
        
        
        NSLog(@"index path %@",[arrresp objectAtIndex:indexPath.row]);
        
        return  cell;
    }
    else if ([[[arrresp valueForKey:@"Type"]objectAtIndex:indexPath.row]isEqualToString:@"IndiePick"])
    {
        static NSString *simpleTableIdentifier = @"CellIndie";
        //tableView.separatorColor = [UIColor clearColor];
        HindiCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[HindiCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        [cell.img_image sd_setImageWithURL:[NSURL URLWithString:[[arrresp valueForKey:@"Image"] objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
        [cell.img_image setContentMode:UIViewContentModeScaleAspectFill];
        [cell.img_image setClipsToBounds:YES];
        
        
        cell.lbl_title.text=[NSString stringWithFormat:@"%@",[[arrresp  valueForKey:@"Title"] objectAtIndex:indexPath.row]];
        
        
        cell.btn_indi_play.tag = indexPath.row;
        [cell.btn_indi_play addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        NSString *str=[NSString stringWithFormat:@"%@", [[arrresp  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];  //is your str
        if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
            
            NSURL *movieURL = [NSURL URLWithString:[[arrresp  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
            //Thard party thambanil
            _youTubeVideo = [[YKYouTubeVideo alloc] initWithContent:[NSURL URLWithString:[[arrresp  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]]];
            [_youTubeVideo parseWithCompletion:^(NSError *error) {
                [_youTubeVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                    cell.img_image.image = thumbImage;
                }];
            }];
        }
        else
        {
            //NSURL *movieURL = [NSURL URLWithString:[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
            _directVideo = [[YKDirectVideo alloc] initWithContent:[NSURL URLWithString:[[arrresp  valueForKey:@"Image"] objectAtIndex:indexPath.row]]];
            [_directVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                cell.img_image.image = thumbImage;
            }];
        }
        
        
        UITapGestureRecognizer *letterTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(highlightLetter:)];
        letterTapRecognizer.numberOfTapsRequired = 1;
        [cell.img_image addGestureRecognizer:letterTapRecognizer];
        
        
        if ([[[arrresp valueForKey:@"IsLike"] objectAtIndex:indexPath.row] intValue]==1) {
            cell.btn_like.selected=YES;
        }
        else
        {
            cell.btn_like.selected=NO;
        }
        
        
        if ([[[arrresp valueForKey:@"IsDislike"] objectAtIndex:indexPath.row] intValue]==1) {
            cell.btn_dislike.selected=YES;
        }
        else
        {
            cell.btn_dislike.selected=NO;
        }
        
        
        cell.lbl_like.text=[NSString stringWithFormat:@"%@",[[arrresp valueForKey:@"TotalLike"] objectAtIndex:indexPath.row]];
        
        cell.lbl_dislike.text=[NSString stringWithFormat:@"%@",[[arrresp valueForKey:@"TotalDislike"] objectAtIndex:indexPath.row]];
        
        
        ////Comment  TotalComment
        
        cell.lbl_comment.text=[NSString stringWithFormat:@"%@",[arrresp valueForKey:@"TotalComment"][indexPath.row]];
        
        NSMutableArray *arrTemp=[[NSMutableArray alloc]initWithArray:[[arrresp valueForKey:@"last_comments"] objectAtIndex:indexPath.row]];
        
        //NSLog(@"arrTemp %@",arrTemp);
        
        
        for (int i=0; i<arrTemp.count; i++) {
            switch (i) {
                case 0:
                {
                    [cell.img_vw1 sd_setImageWithURL:[NSURL URLWithString:[arrTemp valueForKey:@"ImageURL"][0]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
                    
                    cell.lbl_nm_vw1.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"FName"][0]];
                    
                    cell.lbl_desc_vw1.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"Comments"][0]];
                    
                }
                    break;
                case 1:
                {
                    [cell.img_vw2 sd_setImageWithURL:[NSURL URLWithString:[arrTemp valueForKey:@"ImageURL"][1]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
                    
                    cell.lbl_nm_vw2.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"FName"][1]];
                    
                    cell.lbl_desc_vw2.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"Comments"][1]];
                }
                    break;
                    
                default:
                    break;
            }
        }
        
        int C=[[[arrresp valueForKey:@"last_comments"] objectAtIndex:indexPath.row] count];
        
        if (C==2) {
            cell.vw2.hidden=NO;
            cell.vw1.hidden=NO;
        }
        else if (C==1)
        {
            cell.vw1.hidden=NO;
            cell.vw2.hidden=YES;
            
        }
        else
        {
            
            cell.vw2.hidden=YES;
            cell.vw1.hidden=YES;
        }
        
        
        
        //////LIKE DISLIKE
        [cell.btn_like setTag:indexPath.row];
        [cell.btn_like addTarget:self action:@selector(likeNews:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.btn_dislike setTag:indexPath.row];
        [cell.btn_dislike addTarget:self action:@selector(DislikeNews:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.btn_comment setTag:indexPath.row];
        [cell.btn_comment addTarget:self action:@selector(ViewComments:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        
        
        //================DATE TIME CALCULATION------------
        
        NSString * yourJSONString = [NSString stringWithFormat:@"%@",[[arrresp  valueForKey:@"CreatedOn"] objectAtIndex:indexPath.row]];
        
        NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
        [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *dateFromString = [currentDTFormatter dateFromString:yourJSONString];
        [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *eventDateStr = [currentDTFormatter stringFromDate:dateFromString];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSDate *currentDate = [NSDate date];
        NSString *start = [formatter stringFromDate:dateFromString];
        NSString *end = [formatter stringFromDate:currentDate];
        //NSString *end = @"2018-01-11";
        
        NSDateFormatter *f = [[NSDateFormatter alloc] init];
        [f setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *startDate = [f dateFromString:start];
        NSDate *endDate = [f dateFromString:end];
        
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [gregorianCalendar components:(NSCalendarUnitDay |NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitWeekOfYear)
                                                            fromDate:startDate
                                                              toDate:endDate
                                                             options:0];
        
        NSLog(@"Day %ld", [components day]);
        NSLog(@"Hour %ld", [components hour]);
        NSLog(@"Minute %ld", [components minute]);
        
        
        
        if ([components weekOfYear]>0) {
            cell.lbl_postdt.text=[NSString stringWithFormat:@"%ld week ago",(long)[components weekOfYear]];
        }
        else if ([components day]>0) {
            cell.lbl_postdt.text=[NSString stringWithFormat:@"%ld Day ago",(long)[components day]];
        }
        else if ([components hour]>0) {
            cell.lbl_postdt.text=[NSString stringWithFormat:@"%ld hours ago",(long)[components hour]];
        }
        else if ([components minute]>0)
        {
            cell.lbl_postdt.text=[NSString stringWithFormat:@"%ld min ago",(long)[components minute]];
        }
        
        
        
        
        NSLog(@"index path %@",[arrresp objectAtIndex:indexPath.row]);
        
        return  cell;
    }
    else if ([[[arrresp valueForKey:@"Type"]objectAtIndex:indexPath.row]isEqualToString:@"Portfolio"])
    {
        static NSString *simpleTableIdentifier = @"CellPort";
        //tableView.separatorColor = [UIColor clearColor];
        HportCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[HportCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        cell.lbl_usernm.text=[NSString stringWithFormat:@"%@ Posted a %@ ",[[arrresp  valueForKey:@"FName"] objectAtIndex:indexPath.row],[[arrresp  valueForKey:@"Category"] objectAtIndex:indexPath.row]];
        
        [cell.img_prof sd_setImageWithURL:[NSURL URLWithString:[[arrresp valueForKey:@"UserImageURL"] objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
        cell.img_prof.tag=indexPath.row;
        cell.img_prof.userInteractionEnabled = YES;
        [cell.img_prof addGestureRecognizer:tapRecognizer];
        [cell.btn_play setTag:indexPath.row];
        [cell.btn_play addTarget:self action:@selector(yourButtonClicked:)forControlEvents:UIControlEventTouchUpInside];
        
        [cell.img_image setContentMode:UIViewContentModeScaleAspectFill];
        [cell.img_image setClipsToBounds:YES];
        
        //[cell.img_image sd_setImageWithURL:[NSURL URLWithString:[[arrresp valueForKey:@"Image"] objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
        cell.lbl_title.text=[NSString stringWithFormat:@"%@",[[arrresp  valueForKey:@"Title"] objectAtIndex:indexPath.row]];
        
        //IsPortfolioLike
        
        
        
        if ([[[arrresp valueForKey:@"IsLike"] objectAtIndex:indexPath.row] intValue]==1) {
            cell.btn_like.selected=YES;
        }
        else
        {
            cell.btn_like.selected=NO;
        }
        
        
        if ([[[arrresp valueForKey:@"IsDislike"] objectAtIndex:indexPath.row] intValue]==1) {
            cell.btn_dislike.selected=YES;
        }
        else
        {
            cell.btn_dislike.selected=NO;
        }
        
        
        cell.lbl_likecount.text=[NSString stringWithFormat:@"%@",[[arrresp valueForKey:@"TotalLike"] objectAtIndex:indexPath.row]];
        
        cell.lbl_dislikecount.text=[NSString stringWithFormat:@"%@",[[arrresp valueForKey:@"TotalDislike"] objectAtIndex:indexPath.row]];
        
        
        ////Comment  TotalComment
        
        cell.lbl_comment_count.text=[NSString stringWithFormat:@"%@",[arrresp valueForKey:@"TotalComment"][indexPath.row]];
        
        //////LIKE DISLIKE
        [cell.btn_like setTag:indexPath.row];
        [cell.btn_like addTarget:self action:@selector(likeNews:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.btn_dislike setTag:indexPath.row];
        [cell.btn_dislike addTarget:self action:@selector(DislikeNews:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.btn_commnent setTag:indexPath.row];
        [cell.btn_commnent addTarget:self action:@selector(ViewComments:) forControlEvents:UIControlEventTouchUpInside];
        
        NSMutableArray *arrTemp=[[NSMutableArray alloc]initWithArray:[[arrresp valueForKey:@"last_comments"] objectAtIndex:indexPath.row]];
        
        //NSLog(@"arrTemp %@",arrTemp);
        
        
        for (int i=0; i<arrTemp.count; i++) {
            switch (i) {
                case 0:
                {
                    [cell.img_vw1 sd_setImageWithURL:[NSURL URLWithString:[arrTemp valueForKey:@"ImageURL"][0]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
                    
                    cell.lbl_nm_vw1.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"FName"][0]];
                    
                    cell.lbl_desc_vw1.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"Comments"][0]];
                    
                }
                    break;
                case 1:
                {
                    [cell.img_vw2 sd_setImageWithURL:[NSURL URLWithString:[arrTemp valueForKey:@"ImageURL"][1]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
                    
                    cell.lbl_nm_vw2.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"FName"][1]];
                    
                    cell.lbl_desc_vw2.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"Comments"][1]];
                }
                    break;
                    
                default:
                    break;
            }
        }
        
        int C=[[[arrresp valueForKey:@"last_comments"] objectAtIndex:indexPath.row] count];
        
        if (C==2) {
            cell.vw2.hidden=NO;
            cell.vw1.hidden=NO;
        }
        else if (C==1)
        {
            cell.vw1.hidden=NO;
            cell.vw2.hidden=YES;
            
        }
        else
        {
            
            cell.vw2.hidden=YES;
            cell.vw1.hidden=YES;
        }
        
        
        
        
        if ([[[arrresp valueForKey:@"Category"] objectAtIndex:indexPath.row] isEqualToString:@"Photos"])
        {
            [cell.img_image sd_setImageWithURL:[NSURL URLWithString:[[arrresp valueForKey:@"Image"] objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
            
            UIImage *immg=cell.img_image.image;
            NSLog(@"Image %@",immg);
            cell.btn_play.hidden=YES;
        }
        else if ([[[arrresp valueForKey:@"Category"] objectAtIndex:indexPath.row] isEqualToString:@"Feature Films"])
        {
            cell.btn_play.hidden=NO;
            NSString *str=[NSString stringWithFormat:@"%@",[[arrresp  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
            
            [cell.img_image sd_setImageWithURL:[NSString stringWithFormat:@"%@",[self extractYoutubeIdFromLink:str]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
            
        }
        else if ([[[arrresp valueForKey:@"Category"] objectAtIndex:indexPath.row] isEqualToString:@"Short Films"])
        {
            cell.btn_play.hidden=NO;
            
            
            NSString *str=[NSString stringWithFormat:@"%@",[[arrresp  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
            
            [cell.img_image sd_setImageWithURL:[NSString stringWithFormat:@"%@",[self extractYoutubeIdFromLink:str]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
        }
        else if ([[[arrresp valueForKey:@"Category"] objectAtIndex:indexPath.row] isEqualToString:@"Reel"])
        {
            cell.btn_play.hidden=NO;
            cell.img_image.userInteractionEnabled=NO;
            NSString *str=[NSString stringWithFormat:@"%@",[[arrresp  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
            
            [cell.img_image sd_setImageWithURL:[NSString stringWithFormat:@"%@",[self extractYoutubeIdFromLink:str]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
        }
        else if ([[[arrresp valueForKey:@"Category"] objectAtIndex:indexPath.row] isEqualToString:@"BTS"])
        {
            
            NSString *str=[NSString stringWithFormat:@"%@", [[arrresp  valueForKey:@"Image"] objectAtIndex:indexPath.row]];  //is your str
            NSString *extension = [str pathExtension];
            
            if ([extension isEqualToString:@"jpg"])
            {
                cell.btn_play.hidden=YES;
                
                [cell.img_image sd_setImageWithURL:[NSURL URLWithString:[[arrresp valueForKey:@"Image"] objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
                
                UIImage *immg=cell.img_image.image;
                NSLog(@"Image %@",immg);
                
            }
            else
            {
                NSLog(@"Video Play");
                cell.btn_play.hidden=NO;
                
                NSString *str=[NSString stringWithFormat:@"%@",[[arrresp  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
                
                [cell.img_image sd_setImageWithURL:[NSString stringWithFormat:@"%@",[self extractYoutubeIdFromLink:str]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
            }
            
            
        }
        else if ([[[arrresp valueForKey:@"Category"] objectAtIndex:indexPath.row] isEqualToString:@"Videos"])
        {
            cell.btn_play.hidden=NO;
            NSString *str=[NSString stringWithFormat:@"%@",[[arrresp  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
            
            [cell.img_image sd_setImageWithURL:[NSString stringWithFormat:@"%@",[self extractYoutubeIdFromLink:str]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
        }
        else if ([[[arrresp valueForKey:@"Category"] objectAtIndex:indexPath.row] isEqualToString:@"Head Shots"])
        {
            [cell.img_image sd_setImageWithURL:[NSURL URLWithString:[[arrresp valueForKey:@"Image"] objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
            
            UIImage *immg=cell.img_image.image;
            NSLog(@"Image %@",immg);
            cell.btn_play.hidden=YES;
        }
        
        
        /*
         if ([[[arrresp valueForKey:@"Category"] objectAtIndex:indexPath.row]isEqualToString:@"Photos"]) {
         
         //cell.img_image.frame.size.height = 240.0f;
         // [cell.img_image setFrame:CGRectMake(0,60,cell.img_image.frame.size.width,160)];
         
         
         
         
         }
         else
         {
         cell.btn_play.hidden=NO;
         
         /*  _youTubeVideo = [[YKYouTubeVideo alloc] initWithContent:[NSURL URLWithString:[[arrresp  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]]];
         [_youTubeVideo parseWithCompletion:^(NSError *error) {
         [_youTubeVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
         cell.img_image.image = thumbImage;
         }];
         }];
         
         NSString *str=[NSString stringWithFormat:@"%@",[[arrresp  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
         
         [cell.img_image sd_setImageWithURL:[NSString stringWithFormat:@"%@",[self extractYoutubeIdFromLink:str]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
         
         
         //[cell.img_image sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:[UIImage imageNamed:@"No"]];
         }
         */
        
        //================DATE TIME CALCULATION------------
        
        NSString * yourJSONString = [NSString stringWithFormat:@"%@",[[arrresp  valueForKey:@"CreatedOn"] objectAtIndex:indexPath.row]];
        
        NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
        [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *dateFromString = [currentDTFormatter dateFromString:yourJSONString];
        [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *eventDateStr = [currentDTFormatter stringFromDate:dateFromString];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSDate *currentDate = [NSDate date];
        NSString *start = [formatter stringFromDate:dateFromString];
        NSString *end = [formatter stringFromDate:currentDate];
        //NSString *end = @"2018-01-11";
        
        NSDateFormatter *f = [[NSDateFormatter alloc] init];
        [f setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *startDate = [f dateFromString:start];
        NSDate *endDate = [f dateFromString:end];
        
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [gregorianCalendar components:(NSCalendarUnitDay |NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitWeekOfYear)
                                                            fromDate:startDate
                                                              toDate:endDate
                                                             options:0];
        
        NSLog(@"Day %ld", [components day]);
        NSLog(@"Hour %ld", [components hour]);
        NSLog(@"Minute %ld", [components minute]);
        
        if ([components weekOfYear]>0) {
            cell.lbl_postdt.text=[NSString stringWithFormat:@"%ld week ago",(long)[components weekOfYear]];
        }
        else if ([components day]>0) {
            cell.lbl_postdt.text=[NSString stringWithFormat:@"%ld Day ago",(long)[components day]];
        }
        else if ([components hour]>0) {
            cell.lbl_postdt.text=[NSString stringWithFormat:@"%ld hours ago",(long)[components hour]];
        }
        else if ([components minute]>0)
        {
            cell.lbl_postdt.text=[NSString stringWithFormat:@"%ld min ago",(long)[components minute]];
        }
        
        
        
        NSLog(@"index path %@",[arrresp objectAtIndex:indexPath.row]);
        
        return  cell;
    }
    else
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }
        return  cell;
    }
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20.0f;// set this according to that you want...
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([[[arrresp valueForKey:@"Type"] objectAtIndex:indexPath.row]isEqualToString:@"Critiques"]) {
        
        
        static NSString *simpleTableIdentifier = @"CellCriti";
        //tableView.separatorColor = [UIColor clearColor];
        HcritiCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[HcritiCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        
        if ([[arrresp valueForKey:@"accessible"][indexPath.row] intValue]==1) {
            CritiInfoVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"CritiInfoVC"];
            NSArray *arr = [[NSArray alloc] initWithObjects:arrresp, nil][0];
            NSLog(@"arr %@",arr);
            next.mutDict=[arr objectAtIndex:indexPath.row];
            next.FromFlag=@"2";
            
            [self.navigationController pushViewController:next animated:YES];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"You have to upgrade your plan for detail view." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
        
        
        
        
    }
    else if ([[[arrresp valueForKey:@"Type"] objectAtIndex:indexPath.row]isEqualToString:@"Events"])
    {
        static NSString *simpleTableIdentifier = @"CellEvent";
        //tableView.separatorColor = [UIColor clearColor];
        HeventCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[HeventCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        
        if ([[arrresp valueForKey:@"accessible"][indexPath.row] intValue]==1) {
            EventDetailsVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetailsVC"];
            NSArray *arr = [[NSArray alloc] initWithObjects:arrresp, nil][0];
            NSLog(@"arr %@",arr);
            next.FromFlag=@"2";
            next.mutDict=[arr objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:next animated:YES];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"You have to upgrade your plan for detail view." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
        
        
    }
    else if ([[[arrresp valueForKey:@"Type"] objectAtIndex:indexPath.row]isEqualToString:@"Classified"])
    {
        static NSString *simpleTableIdentifier = @"CellClassi";
        //tableView.separatorColor = [UIColor clearColor];
        HclassiCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[HclassiCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        if ([[arrresp valueForKey:@"accessible"][indexPath.row] intValue]==1) {
            ClassInfoVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"ClassInfoVC"];
            NSArray *arr = [[NSArray alloc] initWithObjects:arrresp, nil][0];
            NSLog(@"arr %@",arr);
            next.mutDict=[arr objectAtIndex:indexPath.row];
            next.isFrom=@"NewHomeVC";
            [self.navigationController pushViewController:next animated:YES];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"You have to upgrade your plan for detail view." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
        
    }
    else if ([[[arrresp valueForKey:@"Type"] objectAtIndex:indexPath.row]isEqualToString:@"LearningCenter"])
    {
        static NSString *simpleTableIdentifier = @"CellNews";
        //tableView.separatorColor = [UIColor clearColor];
        HnewsCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[HnewsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
    }
    else if ([[[arrresp valueForKey:@"Type"] objectAtIndex:indexPath.row]isEqualToString:@"FundRaising"])
    {
        static NSString *simpleTableIdentifier = @"CellFunding";
        //tableView.separatorColor = [UIColor clearColor];
        HfundCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[HfundCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
       
    }
    else if ([[[arrresp valueForKey:@"Type"]objectAtIndex:indexPath.row]isEqualToString:@"IndiePick"])
    {
         static NSString *simpleTableIdentifier = @"CellIndie";
         //tableView.separatorColor = [UIColor clearColor];
         HindiCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
         
         if (cell == nil) {
         cell = [[HindiCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
         }
         
   
        [cell.img_image sd_setImageWithURL:[NSURL URLWithString:[[arrresp valueForKey:@"Image"] objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
        
        
        UIImage *immg=cell.img_image.image;
        NSArray *landscapeImages=[[NSArray alloc]initWithObjects:immg, nil];
        
        
        Class cls = NSClassFromString(@"AFTNormalPagingViewController");
        AFTPagingBaseViewController *vc = [cls new];
        AFTPagingBaseViewController *bvc = (AFTPagingBaseViewController *)vc;
        
        bvc.title = @"Photo";
        bvc.images = landscapeImages;
        [self presentViewController:bvc animated:YES completion:nil];
        
        
    }
    else if ([[[arrresp valueForKey:@"Type"]objectAtIndex:indexPath.row]isEqualToString:@"Portfolio"])
    {
        static NSString *simpleTableIdentifier = @"CellPort";
        //tableView.separatorColor = [UIColor clearColor];
        HportCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[HportCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        if ([[[arrresp valueForKey:@"Category"] objectAtIndex:indexPath.row] isEqualToString:@"Photos"])
        {
            [cell.img_image sd_setImageWithURL:[NSURL URLWithString:[[arrresp valueForKey:@"Image"] objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
            
            UIImage *immg=cell.img_image.image;
            NSArray *landscapeImages=[[NSArray alloc]initWithObjects:immg, nil];
            
            
            Class cls = NSClassFromString(@"AFTNormalPagingViewController");
            AFTPagingBaseViewController *vc = [cls new];
            AFTPagingBaseViewController *bvc = (AFTPagingBaseViewController *)vc;
            
            bvc.title = @"Photo";
            bvc.images = landscapeImages;
            [self presentViewController:bvc animated:YES completion:nil];
        }
        else if ([[[arrresp valueForKey:@"Category"] objectAtIndex:indexPath.row] isEqualToString:@"Feature Films"])
        {
            
            
        }
        else if ([[[arrresp valueForKey:@"Category"] objectAtIndex:indexPath.row] isEqualToString:@"Short Films"])
        {
            
        }
        else if ([[[arrresp valueForKey:@"Category"] objectAtIndex:indexPath.row] isEqualToString:@"Reel"])
        {
            
        }
        else if ([[[arrresp valueForKey:@"Category"] objectAtIndex:indexPath.row] isEqualToString:@"BTS"])
        {
            
            NSString *str=[NSString stringWithFormat:@"%@", [[arrresp  valueForKey:@"Image"] objectAtIndex:indexPath.row]];  //is your str
            NSString *extension = [str pathExtension];
            
            if ([extension isEqualToString:@"jpg"])
            {
                [cell.img_image sd_setImageWithURL:[NSURL URLWithString:[[arrresp valueForKey:@"Image"] objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
                
                UIImage *immg=cell.img_image.image;
                NSArray *landscapeImages=[[NSArray alloc]initWithObjects:immg, nil];
                
                
                Class cls = NSClassFromString(@"AFTNormalPagingViewController");
                AFTPagingBaseViewController *vc = [cls new];
                AFTPagingBaseViewController *bvc = (AFTPagingBaseViewController *)vc;
                
                bvc.title = @"Photo";
                bvc.images = landscapeImages;
                [self presentViewController:bvc animated:YES completion:nil];
                
            }
            else
            {
                
            }
            
            
        }
        else if ([[[arrresp valueForKey:@"Category"] objectAtIndex:indexPath.row] isEqualToString:@"Videos"])
        {
            
        }
        else if ([[[arrresp valueForKey:@"Category"] objectAtIndex:indexPath.row] isEqualToString:@"Head Shots"])
        {
            
        }
        
    }
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.selected) {
        // ... Uncheck
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    
    [self.view endEditing:YES];
}


-(void)ViewComments:(id)sender
{
    NSLog(@"ViewComments %@",sender);
    //NSInteger ival=[sender integerValue];
    
    UIButton *btn = (UIButton *)sender;
    CGPoint origin = btn.frame.origin;
    CGPoint point = [btn.superview convertPoint:origin toView:_tbl_newhome];
    NSIndexPath * indexPath = [_tbl_newhome indexPathForRowAtPoint:point];
    
    
    
    if ([[[arrresp valueForKey:@"Type"] objectAtIndex:indexPath.row]isEqualToString:@"Portfolio"])
    {
        AllCommentView *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllCommentView"];
        next.user_portfolio_category_id=[NSString stringWithFormat:@"%@",[[arrresp valueForKey:@"ID"] objectAtIndex:indexPath.row]];
        next.category_nm=@"Portfolio";
        [self.navigationController pushViewController:next animated:NO];
    }
    else if ([[[arrresp valueForKey:@"Type"] objectAtIndex:indexPath.row]isEqualToString:@"IndiePick"])
    {
        AllCommentView *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllCommentView"];
        next.user_portfolio_category_id=[NSString stringWithFormat:@"%@",[[arrresp valueForKey:@"ID"] objectAtIndex:indexPath.row]];
        next.category_nm=@"IndiePick";
        [self.navigationController pushViewController:next animated:NO];
    }
    else if ([[[arrresp valueForKey:@"Type"] objectAtIndex:indexPath.row]isEqualToString:@"Classified"])
    {
        
    }
    else if ([[[arrresp valueForKey:@"Type"] objectAtIndex:indexPath.row]isEqualToString:@"Events"])
    {
        AllCommentView *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllCommentView"];
        next.user_portfolio_category_id=[NSString stringWithFormat:@"%@",[[arrresp valueForKey:@"ID"] objectAtIndex:indexPath.row]];
        next.category_nm=@"Events";
        [self.navigationController pushViewController:next animated:NO];
    }
    else if ([[[arrresp valueForKey:@"Type"] objectAtIndex:indexPath.row]isEqualToString:@"Critiques"])
    {
        AllCommentView *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllCommentView"];
        next.user_portfolio_category_id=[NSString stringWithFormat:@"%@",[[arrresp valueForKey:@"ID"] objectAtIndex:indexPath.row]];
        next.category_nm=@"Critiques";
        [self.navigationController pushViewController:next animated:NO];
    }
    
    
}

-(void)likeNews:(id)sender
{
    NSLog(@"User Like %@",sender);
    //NSInteger ival=[sender integerValue];
    
    UIButton *btn = (UIButton *)sender;
    CGPoint origin = btn.frame.origin;
    CGPoint point = [btn.superview convertPoint:origin toView:_tbl_newhome];
    NSIndexPath * indexPath = [_tbl_newhome indexPathForRowAtPoint:point];
    
    
    
    if ([[[arrresp valueForKey:@"Type"]objectAtIndex:indexPath.row]isEqualToString:@"IndiePick"])
    {
        static NSString *simpleTableIdentifier = @"CellIndie";
        //tableView.separatorColor = [UIColor clearColor];
        HindiCell *cell = [_tbl_newhome dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[HindiCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        
        [APP_DELEGATE showLoadingView:@""];
        //Post Method Request
        NSString *strID=[[arrresp valueForKey:@"ID"] objectAtIndex:indexPath.row];
        NSString *strISLike=[[arrresp valueForKey:@"IsLike"] objectAtIndex:indexPath.row];
        
        if ([strISLike isEqualToString:@"0"]) {
            strISLike=@"1";
        }
        else
        {
            strISLike=@"0";
        }
        
        
        //Post Method Request
        NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
        
        NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
        [AddPost setValue:strID forKey:@"indie_pick_id"];
        [AddPost setValue:strISLike forKey:@"is_like"];
        
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
            httplike  = [[HttpWrapper alloc] init];
            httplike.delegate=self;
            httplike.getbool=NO;
            [httplike requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@indie_picks_like",JsonUrlConstant] param:[AddPost copy]];
        });
        
        
    }
    else if ([[[arrresp valueForKey:@"Type"]objectAtIndex:indexPath.row]isEqualToString:@"Portfolio"])
    {
        static NSString *simpleTableIdentifier = @"CellPort";
        //tableView.separatorColor = [UIColor clearColor];
        //HportCell *cell = [_tbl_newhome dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        HportCell *cell=(HportCell *)[_tbl_newhome cellForRowAtIndexPath:indexPath];
        if (cell == nil) {
            cell = [[HportCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        [APP_DELEGATE showLoadingView:@""];
        //Post Method Request
        NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
        
        NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
        [AddPost setValue:[[arrresp objectAtIndex:indexPath.row] valueForKey:@"ID"] forKey:@"user_portfolio_category_id"];
        
        if ([[[arrresp valueForKey:@"IsLike"] objectAtIndex:indexPath.row] boolValue]) {
            [AddPost setValue:@"0" forKey:@"is_like"];
        }
        else
        {
            [AddPost setValue:@"1" forKey:@"is_like"];
        }
        
        
        
        NSLog(@"user_portfolio_category_id %@",AddPost);
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
            httplike  = [[HttpWrapper alloc] init];
            httplike.delegate=self;
            httplike.getbool=NO;
            [httplike requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@portfolio_like",JsonUrlConstant] param:[AddPost copy]];
        });
        
        
    }
    else if ([[[arrresp valueForKey:@"Type"]objectAtIndex:indexPath.row]isEqualToString:@"Events"])
    {
        static NSString *simpleTableIdentifier = @"CellEvent";
        //tableView.separatorColor = [UIColor clearColor];
        
        HeventCell *cell=(HeventCell *)[_tbl_newhome cellForRowAtIndexPath:indexPath];
        if (cell == nil) {
            cell = [[HeventCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        
        [APP_DELEGATE showLoadingView:@""];
        //Post Method Request
        NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
        
        NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
        [AddPost setValue:[[arrresp objectAtIndex:indexPath.row] valueForKey:@"ID"] forKey:@"event_id"];
        
        if ([[[arrresp valueForKey:@"IsLike"] objectAtIndex:indexPath.row] boolValue]) {
            [AddPost setValue:@"0" forKey:@"is_like"];
        }
        else
        {
            [AddPost setValue:@"1" forKey:@"is_like"];
        }
        
        
        
        NSLog(@"user_portfolio_category_id %@",AddPost);
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
            httplike  = [[HttpWrapper alloc] init];
            httplike.delegate=self;
            httplike.getbool=NO;
            [httplike requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@events_like",JsonUrlConstant] param:[AddPost copy]];
        });
        
        
    }
    else if ([[[arrresp valueForKey:@"Type"]objectAtIndex:indexPath.row]isEqualToString:@"Critiques"])
    {
        static NSString *simpleTableIdentifier = @"CellEvent";
        //tableView.separatorColor = [UIColor clearColor];
        
        HeventCell *cell=(HeventCell *)[_tbl_newhome cellForRowAtIndexPath:indexPath];
        if (cell == nil) {
            cell = [[HeventCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        
        [APP_DELEGATE showLoadingView:@""];
        //Post Method Request
        NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
        
        NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
        [AddPost setValue:[[arrresp objectAtIndex:indexPath.row] valueForKey:@"ID"] forKey:@"critiques_id"];
        
        if ([[[arrresp valueForKey:@"IsLike"] objectAtIndex:indexPath.row] boolValue]) {
            [AddPost setValue:@"0" forKey:@"is_like"];
        }
        else
        {
            [AddPost setValue:@"1" forKey:@"is_like"];
        }
        
        
        
        NSLog(@"user_portfolio_category_id %@",AddPost);
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
            httplike  = [[HttpWrapper alloc] init];
            httplike.delegate=self;
            httplike.getbool=NO;
            [httplike requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@critiques_like",JsonUrlConstant] param:[AddPost copy]];
        });
    }
    
    
}
-(void)DislikeNews:(id)sender
{
    /*
     
     
     */
    NSLog(@"Dislike NEWS  %@",sender);
    UIButton *btn = (UIButton *)sender;
    
    CGPoint origin = btn.frame.origin;
    CGPoint point = [btn.superview convertPoint:origin toView:_tbl_newhome];
    NSIndexPath * indexPath = [_tbl_newhome indexPathForRowAtPoint:point];
    
    
    
    if ([[[arrresp valueForKey:@"Type"]objectAtIndex:indexPath.row]isEqualToString:@"IndiePick"])
    {
        static NSString *simpleTableIdentifier = @"CellIndie";
        //tableView.separatorColor = [UIColor clearColor];
        //HindiCell *cell = [_tbl_newhome dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        HindiCell *cell=(HindiCell *)[_tbl_newhome cellForRowAtIndexPath:indexPath];
        
        if (cell == nil) {
            cell = [[HindiCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        
        [APP_DELEGATE showLoadingView:@""];
        
        
        
        NSString *strID=[[arrresp valueForKey:@"ID"] objectAtIndex:indexPath.row];
        NSString *strISLike=[[arrresp valueForKey:@"IsDislike"] objectAtIndex:indexPath.row];
        
        if ([strISLike isEqualToString:@"0"]) {
            strISLike=@"1";
        }
        else
        {
            strISLike=@"0";
        }
        
        
        //Post Method Request
        NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
        
        NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
        [AddPost setValue:strID forKey:@"indie_pick_id"];
        [AddPost setValue:strISLike forKey:@"is_dislike"];
        
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
            httpdislike  = [[HttpWrapper alloc] init];
            httpdislike.delegate=self;
            httpdislike.getbool=NO;
            [httpdislike requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@indie_picks_dislike",JsonUrlConstant] param:[AddPost copy]];
        });
        
        
    }
    else if ([[[arrresp valueForKey:@"Type"]objectAtIndex:indexPath.row]isEqualToString:@"Portfolio"])
    {
        static NSString *simpleTableIdentifier = @"CellPort";
        //tableView.separatorColor = [UIColor clearColor];
        //HportCell *cell = [_tbl_newhome dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        HportCell *cell=(HportCell *)[_tbl_newhome cellForRowAtIndexPath:indexPath];
        if (cell == nil) {
            cell = [[HportCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        [APP_DELEGATE showLoadingView:@""];
        //Post Method Request
        NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
        
        NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
        [AddPost setValue:[[arrresp objectAtIndex:indexPath.row] valueForKey:@"ID"] forKey:@"user_portfolio_category_id"];
        if ([[[arrresp valueForKey:@"IsDislike"] objectAtIndex:indexPath.row] boolValue]) {
            [AddPost setValue:@"0" forKey:@"is_dislike"];
        }
        else
        {
            [AddPost setValue:@"1" forKey:@"is_dislike"];
        }
        
        NSLog(@"user_portfolio_category_id %@",AddPost);
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
            httpdislike  = [[HttpWrapper alloc] init];
            httpdislike.delegate=self;
            httpdislike.getbool=NO;
            [httpdislike requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@portfolio_dislike",JsonUrlConstant] param:[AddPost copy]];
        });
        
        
    }
    else if ([[[arrresp valueForKey:@"Type"]objectAtIndex:indexPath.row]isEqualToString:@"Events"])
    {
        static NSString *simpleTableIdentifier = @"CellEvent";
        //tableView.separatorColor = [UIColor clearColor];
        
        HeventCell *cell=(HeventCell *)[_tbl_newhome cellForRowAtIndexPath:indexPath];
        if (cell == nil) {
            cell = [[HeventCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        
        [APP_DELEGATE showLoadingView:@""];
        //Post Method Request
        NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
        
        NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
        [AddPost setValue:[[arrresp objectAtIndex:indexPath.row] valueForKey:@"ID"] forKey:@"event_id"];
        
        if ([[[arrresp valueForKey:@"IsDislike"] objectAtIndex:indexPath.row] boolValue]) {
            [AddPost setValue:@"0" forKey:@"is_dislike"];
        }
        else
        {
            [AddPost setValue:@"1" forKey:@"is_dislike"];
        }
        
        
        
        NSLog(@"user_portfolio_category_id %@",AddPost);
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
            httplike  = [[HttpWrapper alloc] init];
            httplike.delegate=self;
            httplike.getbool=NO;
            [httplike requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@events_dislike",JsonUrlConstant] param:[AddPost copy]];
        });
        
        
    }
    else if ([[[arrresp valueForKey:@"Type"]objectAtIndex:indexPath.row]isEqualToString:@"Critiques"])
    {
        static NSString *simpleTableIdentifier = @"CellEvent";
        //tableView.separatorColor = [UIColor clearColor];
        
        HeventCell *cell=(HeventCell *)[_tbl_newhome cellForRowAtIndexPath:indexPath];
        if (cell == nil) {
            cell = [[HeventCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        
        [APP_DELEGATE showLoadingView:@""];
        //Post Method Request
        NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
        
        NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
        [AddPost setValue:[[arrresp objectAtIndex:indexPath.row] valueForKey:@"ID"] forKey:@"critiques_id"];
        
        if ([[[arrresp valueForKey:@"IsDislike"] objectAtIndex:indexPath.row] boolValue]) {
            [AddPost setValue:@"0" forKey:@"is_dislike"];
        }
        else
        {
            [AddPost setValue:@"1" forKey:@"is_dislike"];
        }
        
        
        
        NSLog(@"user_portfolio_category_id %@",AddPost);
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
            httplike  = [[HttpWrapper alloc] init];
            httplike.delegate=self;
            httplike.getbool=NO;
            [httplike requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@critiques_dislike",JsonUrlConstant] param:[AddPost copy]];
        });
    }
    
}



-(void)CallLike:(NSInteger)intval
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[[arrresp objectAtIndex:intval] valueForKey:@"ID"] forKey:@"user_portfolio_category_id"];
    
    if ([[[arrresp valueForKey:@"IsLike"] objectAtIndex:intval] boolValue]) {
        [AddPost setValue:@"0" forKey:@"is_like"];
    }
    else
    {
        [AddPost setValue:@"1" forKey:@"is_like"];
    }
    
    
    
    NSLog(@"user_portfolio_category_id %@",AddPost);
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httplike  = [[HttpWrapper alloc] init];
        httplike.delegate=self;
        httplike.getbool=NO;
        [httplike requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@portfolio_like",JsonUrlConstant] param:[AddPost copy]];
    });
}

-(void)CallDislike:(NSInteger)intval
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[[arrresp objectAtIndex:intval] valueForKey:@"ID"] forKey:@"user_portfolio_category_id"];
    if ([[[arrresp valueForKey:@"IsDislike"] objectAtIndex:intval] boolValue]) {
        [AddPost setValue:@"0" forKey:@"is_dislike"];
    }
    else
    {
        [AddPost setValue:@"1" forKey:@"is_dislike"];
    }
    
    NSLog(@"user_portfolio_category_id %@",AddPost);
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpdislike  = [[HttpWrapper alloc] init];
        httpdislike.delegate=self;
        httpdislike.getbool=NO;
        [httpdislike requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@portfolio_dislike",JsonUrlConstant] param:[AddPost copy]];
    });
}

-(void)yourButtonFunding:(UIButton*)sender
{
    UIButton *btn=sender;
    
    NSLog(@"button was clicked for the following row %li", (long)btn.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[btn tag]  inSection:0];
    NSLog(@"indexpath.row %ld", (long)indexPath.row);
    NSLog(@"indexpath.row %ld", (long)indexPath.section);
    
    NSString *strurl=[[arrresp  valueForKey:@"GFMKSLink"] objectAtIndex:indexPath.row];//FundRaisingID
    
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://www.google.com"]];
    
    // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strurl ]];
    
    
}

-(void)yourButtonClicked1:(UIButton*)sender
{
    UIButton *btn=sender;
    
    NSLog(@"button was clicked for the following row %li", (long)btn.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[btn tag]  inSection:0];
    NSLog(@"indexpath.row %ld", (long)indexPath.row);
    NSLog(@"indexpath.row %ld", (long)indexPath.section);
    
    NSString *strurl=[[arrresp valueForKey:@"IsFollow"] objectAtIndex:indexPath.row];
    NSString *FRId=[[arrresp valueForKey:@"ID"] objectAtIndex:indexPath.row];
    
    if ([strurl isEqualToString:@"0"]) {
        [self CallFollow :FRId];
    }
    else
    {
        [self CallUnFollow :FRId];
    }
    
    
}

-(void)CallFollow :(NSString *)FRId
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:FRId forKey:@"fund_raising_id"];//
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpfollow  = [[HttpWrapper alloc] init];
        httpfollow.delegate=self;
        httpfollow.getbool=NO;
        [httpfollow requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@fund_raising_follow",JsonUrlConstant] param:[AddPost copy]];
    });
    
}
-(void)CallUnFollow :(NSString *)FRId
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:FRId forKey:@"fund_raising_id"];//
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpunfollow  = [[HttpWrapper alloc] init];
        httpunfollow.delegate=self;
        httpunfollow.getbool=NO;
        [httpunfollow requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@fund_raising_unfollow",JsonUrlConstant] param:[AddPost copy]];
    });
    
}


#pragma mark- Wrapper Class Delegate Methods
//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary* )dicsResponse
{
    if(wrapper == httplike && httplike != nil)
    {
        NSLog(@"---%@",dicsResponse);
        [self CallNewHome];
        [APP_DELEGATE hideLoadingView];
        
    }
    else if(wrapper == httpdislike && httpdislike != nil)
    {
        NSLog(@"---%@",dicsResponse);
        [self CallNewHome];
        [APP_DELEGATE hideLoadingView];
    }
    else if (wrapper == httpfollow && httpfollow != nil)
    {
        NSLog(@"DicsResponse ---%@",dicsResponse);
        //NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
        
        if ([[dicsResponse valueForKey:@"status"] intValue]==1) {
            //[self CallGetFundList];
            [self CallNewHome];
        }
        
        [APP_DELEGATE hideLoadingView];
    }
    else if (wrapper == httpunfollow && httpunfollow != nil)
    {
        NSLog(@"DicsResponse ---%@",dicsResponse);
        if ([[dicsResponse valueForKey:@"status"] intValue]==1) {
            //[self CallGetFundList];
            
            [self CallNewHome];
        }
        
        [APP_DELEGATE hideLoadingView];
    }
}


//fetchDataFail
- (void) HttpWrapper:(HttpWrapper* )wrapper fetchDataFail:(NSError* )error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}



- (void)exchangeKey:(NSString *)aKey withKey:(NSString *)aNewKey inMutableDictionary:(NSMutableDictionary *)aDict
{
    if (![aKey isEqualToString:aNewKey]) {
        id objectToPreserve = [aDict objectForKey:aKey];
        [aDict setObject:objectToPreserve forKey:aNewKey];
        [aDict removeObjectForKey:aKey];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    if ([[[arrresp valueForKey:@"Type"] objectAtIndex:indexPath.row]isEqualToString:@"Classified"])
    {
        static NSString *simpleTableIdentifier = @"CellClassi";
        //tableView.separatorColor = [UIColor clearColor];
        HclassiCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[HclassiCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        return  313;
    }
    else if ([[[arrresp valueForKey:@"Type"] objectAtIndex:indexPath.row]isEqualToString:@"FundRaising"])
    {
        static NSString *simpleTableIdentifier = @"CellFunding";
        //tableView.separatorColor = [UIColor clearColor];
        HfundCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[HfundCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        return  367;
    }
    else if ([[[arrresp valueForKey:@"Type"] objectAtIndex:indexPath.row]isEqualToString:@"IndiePick"])
    {
        static NSString *simpleTableIdentifier = @"CellIndie";
        //tableView.separatorColor = [UIColor clearColor];
        HindiCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[HindiCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        int C=[[[arrresp valueForKey:@"last_comments"] objectAtIndex:indexPath.row] count];
        
        if (C==2) {
            if ([[[arrresp valueForKey:@"Category"] objectAtIndex:indexPath.row]isEqualToString:@"Photos"]) {
                return 410;
            }
            else
            {
                return 410;
            }
        }
        else if (C==1)
        {
            if ([[[arrresp valueForKey:@"Category"] objectAtIndex:indexPath.row]isEqualToString:@"Photos"]) {
                return 410-50;
            }
            else
            {
                return 410-50;
            }
            cell.vw2.hidden=YES;
            
        }
        else
        {
            if ([[[arrresp valueForKey:@"Category"] objectAtIndex:indexPath.row]isEqualToString:@"Photos"]) {
                return 410-100;
            }
            else
            {
                return 410-100;
            }
            cell.vw2.hidden=YES;
            cell.vw1.hidden=YES;
        }
        //return  310;
    }
    else if ([[[arrresp valueForKey:@"Type"] objectAtIndex:indexPath.row]isEqualToString:@"Events"])
    {
        static NSString *simpleTableIdentifier = @"CellEvent";
        //tableView.separatorColor = [UIColor clearColor];
        HeventCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[HeventCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        
        int C=[[[arrresp valueForKey:@"last_comments"] objectAtIndex:indexPath.row] count];
        
        if (C==2) {
            if ([[[arrresp valueForKey:@"Category"] objectAtIndex:indexPath.row]isEqualToString:@"Photos"]) {
                return 450;
            }
            else
            {
                return 450;
            }
        }
        else if (C==1)
        {
            if ([[[arrresp valueForKey:@"Category"] objectAtIndex:indexPath.row]isEqualToString:@"Photos"]) {
                return 450-50;
            }
            else
            {
                return 450-50;
            }
            cell.vw2.hidden=YES;
            
        }
        else
        {
            if ([[[arrresp valueForKey:@"Category"] objectAtIndex:indexPath.row]isEqualToString:@"Photos"]) {
                return 450-100;
            }
            else
            {
                return 450-100;
            }
            cell.vw2.hidden=YES;
            cell.vw1.hidden=YES;
        }
        
        
        //return 350;
    }
    else if ([[[arrresp valueForKey:@"Type"]objectAtIndex:indexPath.row]isEqualToString:@"Portfolio"])
    {
        
        static NSString *simpleTableIdentifier = @"CellPort";
        //tableView.separatorColor = [UIColor clearColor];
        HportCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[HportCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        
        
        
        
        
        int C=[[[arrresp valueForKey:@"last_comments"] objectAtIndex:indexPath.row] count];
        
        if (C==2) {
            if ([[[arrresp valueForKey:@"Category"] objectAtIndex:indexPath.row]isEqualToString:@"Photos"]) {
                return 438;
            }
            else
            {
                return 438;
            }
        }
        else if (C==1)
        {
            if ([[[arrresp valueForKey:@"Category"] objectAtIndex:indexPath.row]isEqualToString:@"Photos"]) {
                return 438-50;
            }
            else
            {
                return 438-50;
            }
            cell.vw2.hidden=YES;
            
        }
        else
        {
            if ([[[arrresp valueForKey:@"Category"] objectAtIndex:indexPath.row]isEqualToString:@"Photos"]) {
                return 438-100;
            }
            else
            {
                return 438-100;
            }
            cell.vw2.hidden=YES;
            cell.vw1.hidden=YES;
        }
        
        
    }
    else if ([[[arrresp valueForKey:@"Type"] objectAtIndex:indexPath.row]isEqualToString:@"LearningCenter"])
    {
        static NSString *simpleTableIdentifier = @"CellNews";
        //tableView.separatorColor = [UIColor clearColor];
        HnewsCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[HnewsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        return 285;
    }
    else if ([[[arrresp valueForKey:@"Type"] objectAtIndex:indexPath.row]isEqualToString:@"Critiques"]) {
        
        
        static NSString *simpleTableIdentifier = @"CellCriti";
        //tableView.separatorColor = [UIColor clearColor];
        HcritiCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[HcritiCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        
        int C=[[[arrresp valueForKey:@"last_comments"] objectAtIndex:indexPath.row] count];
        
        if (C==2) {
            if ([[[arrresp valueForKey:@"Category"] objectAtIndex:indexPath.row]isEqualToString:@"Critiques"]) {
                return 410;
            }
            else
            {
                return 410;
            }
        }
        else if (C==1)
        {
            if ([[[arrresp valueForKey:@"Category"] objectAtIndex:indexPath.row]isEqualToString:@"Critiques"]) {
                return 410-50;
            }
            else
            {
                return 410-50;
            }
            cell.vw2.hidden=YES;
            
        }
        else
        {
            if ([[[arrresp valueForKey:@"Category"] objectAtIndex:indexPath.row]isEqualToString:@"Critiques"]) {
                return 410-100;
            }
            else
            {
                return 410-100;
            }
            cell.vw2.hidden=YES;
            cell.vw1.hidden=YES;
        }
        
        
    }
    
    return 230;
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

- (UIImage* )imageFromMovie:(NSURL* )movieURL atTime:(NSTimeInterval)time {
    // set up the movie player
    MPMoviePlayerController *mp = [[MPMoviePlayerController alloc]
                                   initWithContentURL:movieURL];
    mp.shouldAutoplay = NO;
    mp.initialPlaybackTime = time;
    mp.currentPlaybackTime = time;
    // get the thumbnail
    thumbnail = [mp thumbnailImageAtTime:time
                              timeOption:MPMovieTimeOptionNearestKeyFrame];
    // clean up the movie player
    [mp stop];
    return(thumbnail);
}
-(void)yourButtonClicked:(UIButton*)sender
{
    UIButton *btn=sender;
    
    NSLog(@"button was clicked for the following row %li", (long)btn.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[btn tag]  inSection:0];
    NSLog(@"indexpath.row %ld", (long)indexPath.row);
    NSLog(@"indexpath.row %ld", (long)indexPath.section);
    
    
    if ([[[arrresp valueForKey:@"Type"] objectAtIndex:indexPath.row]isEqualToString:@"FundRaising"]) {
        NSString *strurl=[[arrresp  valueForKey:@"GFMKSLink"] objectAtIndex:indexPath.row];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strurl ]];
        
        
    }
    else if ([[[arrresp valueForKey:@"Type"] objectAtIndex:indexPath.row]isEqualToString:@"LearningCenter"])
    {
        NSString *strurl=[[arrresp  valueForKey:@"Image"] objectAtIndex:indexPath.row];
        //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:strurl ]];
        
        ////////////
        NSString *str=[NSString stringWithFormat:@"%@", [[arrresp  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];  //is your str
        if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
            
            /*  UIWebView *webView = [[UIWebView alloc] initWithFrame:self.view.bounds];  //Change self.view.bounds to a smaller CGRect if you don't want it to take up the whole screen
             [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]]]];
             [self.view addSubview:webView];*/
            
            
            WebPlayerVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"WebPlayerVC"];
            next.strurl=[[arrresp  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:next animated:NO];
            
        }
        else
        {
            NSURL *videoURL = [NSURL URLWithString:[[arrresp  valueForKey:@"Image"] objectAtIndex:indexPath.row]];
            AVPlayer *player = [AVPlayer playerWithURL:videoURL];
            AVPlayerViewController *avplayerViewController = [AVPlayerViewController new];
            avplayerViewController.player = player;
            //[self.view addSubview:avplayerViewController.view];
            [self presentViewController:avplayerViewController animated:YES completion:nil];
        }
        
        
        
        
    }
    else if ([[[arrresp valueForKey:@"Type"] objectAtIndex:indexPath.row]isEqualToString:@"IndiePick"])
    {
        
        NSString *str=[NSString stringWithFormat:@"%@", [[arrresp  valueForKey:@"VIURL"] objectAtIndex:indexPath.row]];  //is your str
        if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
            
            /*  UIWebView *webView = [[UIWebView alloc] initWithFrame:self.view.bounds];  //Change self.view.bounds to a smaller CGRect if you don't want it to take up the whole screen
             [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]]]];
             [self.view addSubview:webView];*/
            
            WebPlayerVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"WebPlayerVC"];
            next.strurl=[[arrresp  valueForKey:@"VIURL"] objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:next animated:NO];
            
        }
        else
        {
            NSURL *videoURL = [NSURL URLWithString:[[arrresp  valueForKey:@"Image"] objectAtIndex:indexPath.row]];
            AVPlayer *player = [AVPlayer playerWithURL:videoURL];
            AVPlayerViewController *avplayerViewController = [AVPlayerViewController new];
            avplayerViewController.player = player;
            //[self.view addSubview:avplayerViewController.view];
            [self presentViewController:avplayerViewController animated:YES completion:nil];
        }
        
    }
    else if ([[[arrresp valueForKey:@"Type"] objectAtIndex:indexPath.row]isEqualToString:@"Portfolio"])
    {
        
        NSString *str=[NSString stringWithFormat:@"%@", [[arrresp  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];  //is your str
        if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
            
            WebPlayerVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"WebPlayerVC"];
            next.strurl=[[arrresp  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:next animated:NO];
            
        }
        else
        {
            NSURL *videoURL = [NSURL URLWithString:[[arrresp  valueForKey:@"Image"] objectAtIndex:indexPath.row]];
            AVPlayer *player = [AVPlayer playerWithURL:videoURL];
            AVPlayerViewController *avplayerViewController = [AVPlayerViewController new];
            avplayerViewController.player = player;
            //[self.view addSubview:avplayerViewController.view];
            [self presentViewController:avplayerViewController animated:YES completion:nil];
        }
        
    }
    
}


-(void)setCircularLayout{
    // your collectionView
    if ([[UIScreen mainScreen] bounds].size.height == 736.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(190, 270)
                                radius:180
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:72];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_home setCollectionViewLayout:circularLayout];
    }
    else  if ([[UIScreen mainScreen] bounds].size.height == 812.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(200, 260)
                                radius:180
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:72];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_home setCollectionViewLayout:circularLayout];
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 667.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(200, 270)
                                radius:180
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:72];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        
        
        
        [self.collection_home setCollectionViewLayout:circularLayout];
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 568.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(158, 230)
                                radius:150
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:58];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_home setCollectionViewLayout:circularLayout];
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 480.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(158, 230)
                                radius:160
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:58];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_home setCollectionViewLayout:circularLayout];
    }
    
}

-(CGFloat)heightForText:(NSString *)text
{
    NSInteger MAX_HEIGHT = 2000;
    UITextView * textView = [[UITextView alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width-80, MAX_HEIGHT+150)];
    textView.text = text;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        textView.font = [UIFont fontWithName:@"Roboto-Light" size:12.0];
    }
    else
    {
        textView.font = [UIFont fontWithName:@"Roboto-Light" size:12.0];
    }
    
    [textView sizeToFit];
    
    return textView.frame.size.height;
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 8;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    DSCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    
    cell.img_icon.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",[arrimg objectAtIndex:indexPath.row]]];
    [self playAlertSoundPressed:nil];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self playAlertSoundPressed:nil];
    [self SelectedMenu:indexPath.row];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSLog(@"Scroll...!");
    //[[NSSound soundNamed:@"Tink"] play];
    
    //[self playAlertSoundPressed:nil];
}


- (IBAction)playAlertSoundPressed:(UIButton *)sender
{
    [[JSQSystemSoundPlayer sharedPlayer] playAlertSoundWithFilename:@"Click2-Sebastian"
                                                      fileExtension:kJSQSystemSoundTypeAIFF
                                                         completion:nil];
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 100, 0, 0);
}
- (void)SelectedMenu:(NSInteger)index{
    
    NSLog(@"%ld",(long)index);
    
    switch (index) {
        case 0:
        {
            //Perform any action that u want on menu selection
            EventVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"EventVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            break;
            
        case 1:{
            NewsVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsVC"];
            [self.navigationController pushViewController:next animated:NO];
            
        }
            
            break;
        case 2:
        {
            CritiquesVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"CritiquesVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 3:
        {
            ClassifiedVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"ClassifiedVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 4:
        {
            LearningVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"LearningVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 5:
        {
            IndieVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"IndieVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 6:
        {
            FundingVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"FundingVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            break;
        case 7:
        {
            TeamBuild1VC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"TeamBuild1VC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            break;
            
            
            break;
        default:
            break;
    }
}



- (IBAction)btn_HOME:(id)sender {
    //NewsVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsVC"];
    //[self.navigationController pushViewController:next animated:NO];
    
}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}


- (IBAction)btn_GLOBE:(id)sender {
    [self playAlertSoundPressed:nil];
    if (isShow==false) {
        _collection_home.hidden=NO;
        _img_colle_back.hidden=NO;
        isShow=true;
        
    }
    else
    {
        _collection_home.hidden=YES;
        _img_colle_back.hidden=YES;
        isShow=false;
    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 5;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIView *view = [[UIView alloc]init];
    [view setAlpha:0.6F];
    return view;
    
}

- (IBAction)btn_NOTIF:(id)sender {
    
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
    
}

- (NSString *)extractYoutubeIdFromLink:(NSString *)link {
    NSString *regexString = @"((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)";
    NSRegularExpression *regExp = [NSRegularExpression regularExpressionWithPattern:regexString
                                                                            options:NSRegularExpressionCaseInsensitive
                                                                              error:nil];
    
    NSArray *array = [regExp matchesInString:link options:0 range:NSMakeRange(0,link.length)];
    if (array.count > 0) {
        NSTextCheckingResult *result = array.firstObject;
        NSString* thumbImageUrl = [NSString stringWithFormat:@"https://img.youtube.com/vi/%@/default.jpg",[link substringWithRange:result.range]];
        return thumbImageUrl;
    }
    return nil;
}

#pragma mark - UI
/** CreateUI */
- (void)createUI {
    self.view.backgroundColor = UIColorFromRGB(0xCCBBAA);
    
    self.navigationController.navigationBar.translucent = NO;
    self.title = @"JhtFloatingBall";
    
    // 添加folatingball
    [self.view addSubview:self.folatingball];
}



#pragma mark - Get
/** folatingball */
- (JhtFloatingBall *)folatingball {
    if (!_folatingball) {
        UIImage *suspendedBallImage = [UIImage imageNamed:@"notification_red"];
        _folatingball = [[JhtFloatingBall alloc] initWithFrame:CGRectMake(self.view.frame.size.width-50, 100, 45, 45)];
        
        _folatingball.image = suspendedBallImage;
        _folatingball.stayAlpha = 0.6;
        _folatingball.delegate = self;
        
        _folatingball.stayMode = (StayMode_OnlyRight);
    }
    
    return _folatingball;
}



#pragma mark - JhtFloatingBallDelegate
/** folatingball点击 事件 */
- (void)tapFloatingBall {
    NSLog(@"folatingball Click ");
}

#pragma mark - GetAllPosts
-(void)CallTeamList:(NSString *)userid
{
    [APP_DELEGATE showLoadingView:@""];
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
   // NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:userid forKey:@"user_id"];
//    [AddPost setValue:[NSString stringWithFormat:@"%@",page_no] forKey:@"page_no"];
//    [AddPost setValue:[NSString stringWithFormat:@"%@",_projID] forKey:@"project_id"];
    //https://indieglobe.blenzabi.com/web_services/version_1/Web_services/get_single_people_list
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@get_single_people_list",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        //page_no=[responseObject valueForKey:@"page_no"];
        
    //   NSMutableArray *mutDictarr=[responseObject valueForKey:@"data"];
        NSMutableArray *mutDictarr = [ApplicationData cleanJsonToObject:[responseObject valueForKey:@"data"]];
        
        if (mutDictarr.count>0) {
            //[_lbl_myevent reloadData];
            ProfileVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileVC"];
            next.mutDict= [mutDictarr objectAtIndex:0];
            [self.navigationController pushViewController:next animated:YES];
              [APP_DELEGATE hideLoadingView];
        }
        else
        {
            //_lbl_myevent.showsInfiniteScrolling = NO;
        }
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
}

@end
