//
//  IndieVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/1/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "IndieVC.h"
#import "SWRevealViewController.h"
#import <JSQSystemSoundPlayer/JSQSystemSoundPlayer.h>
#import "DeshboardVC.h"
#import "UIColor+CL.h"
#import "Constants.h"
#import "DSCircularLayout.h"
#import "DSCollectionViewCell.h"
#import "TeamBuild1VC.h"
#import "IndieCell.h"
#import "SVPullToRefresh.h"
#import "NewHomeVC.h"
#import "ProfilePhotoViewVC.h"

#import "YKMediaPlayerKit.h"
#import "YKVimeoVideo.h"
#import "YKYouTubeVideo.h"
#import "YKDirectVideo.h"
#import "WebPlayerVC.h"
#import "AllNotificationVC.h"
#import "AllCommentView.h"
#import "AFTPagingBaseViewController.h"




#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height
#define ITEM_WIDTH 75
#define ITEM_HEIGHT 75


@interface IndieVC ()

@end

@implementation IndieVC
{
    BOOL isMenuActive;
    IGCMenu *igcMenu;
    int count;
    BOOL isShow;
    
    YKYouTubeVideo  *_youTubeVideo;
    YKVimeoVideo    *_vimeoVideo;
    YKDirectVideo   *_directVideo;
    YKDirectVideo   *_otherVideo;
    YKUnKnownVideo  *_unknownVideo;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    [self customSetup];
    // Do any additional setup after loading the view.
    
    [self CustomizeTextField:_txt_search];
    _txt_search.placeholder=@"Search your stuff here";
    
    //NEW MENU
    count = 0;
    isShow=false;
    [self setCircularLayout];

    arrimg= [[NSMutableArray alloc]initWithObjects:@"evets4553.png",@"news4557.png",@"critique4552.png",@"classified4551.png",@"learning4556.png",@"indie-pic4555.png",@"global4554.png",@"team4558.png",nil];

    
   
    //btn_search
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.btn_search.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(10.0, 10.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    self.btn_search.layer.mask = maskLayer;
    
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];

    [_btn_search addTarget:self
                    action:@selector(CallSearchMethod)
          forControlEvents:UIControlEventTouchUpInside];
    
    
}

- (void)customSetup
{
    
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if (IsIphone7 || IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = 145;
    }
    else{
        self.revealViewController.rearViewRevealWidth = 145;
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}
-(void)setCircularLayout{
    if ([[UIScreen mainScreen] bounds].size.height == 736.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(190, 270)
                                radius:180
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:72];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }
    else  if ([[UIScreen mainScreen] bounds].size.height == 812.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(200, 260)
                                radius:180
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:72];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 667.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(190, 270)
                                radius:180
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:72];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 568.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(158, 230)
                                radius:150
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:58];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 480.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(158, 230)
                                radius:160
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:58];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }
}


-(void)CustomizeTextField:(UITextField *)myTextField
{
    myTextField.layer.borderWidth=1.0;
    //myTextField.layer.borderColor=[UIColor colorWithRed:(180.0f/255.0) green:(180.0f/255.0) blue:(180.0f/255.0) alpha:1].CGColor;
    myTextField.layer.borderColor=[UIColor colorWithHex:0x196A84].CGColor;
    CGRect frameRect = myTextField.frame;
    frameRect.size.height = 24;
    myTextField.frame = frameRect;
    myTextField.layer.cornerRadius=myTextField.frame.size.height/2;
    
    [myTextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
    myTextField.leftView = paddingView;
    myTextField.leftViewMode = UITextFieldViewModeAlways;
    
    
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self customSetup];
    page_no=@"0";
    [self CallIndie];
    
    
    
    [self.collection_view performBatchUpdates:^{
        [self.collection_view insertItemsAtIndexPaths:[NSArray arrayWithObjects:
                                                      [NSIndexPath indexPathForRow:0 inSection:0],
                                                      [NSIndexPath indexPathForRow:1 inSection:0],
                                                      [NSIndexPath indexPathForRow:2 inSection:0],
                                                      [NSIndexPath indexPathForRow:3 inSection:0],
                                                      [NSIndexPath indexPathForRow:4 inSection:0],
                                                      [NSIndexPath indexPathForRow:5 inSection:0],
                                                      [NSIndexPath indexPathForRow:6 inSection:0],
                                                        [NSIndexPath indexPathForRow:7 inSection:0],
                                                      
                                                      nil]];
        count = 8;
    } completion:^(BOOL finished) {
        [self.collection_view reloadData];
    }];
     
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)CallSearchMethod
{
    [self.view endEditing:YES];
    [APP_DELEGATE showLoadingView:@""];
    page_no=@"0";
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    
    [AddPost setValue:[NSString stringWithFormat:@"%@",page_no] forKey:@"page_no"];
    
    if (![_txt_search.text isEqualToString:@""]) {
        [AddPost setValue:_txt_search.text forKey:@"search_text"];
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    [manager POST:[NSString stringWithFormat:@"%@get_indie_picks",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        page_no=[responseObject valueForKey:@"page_no"];
        
        mutDict=[responseObject valueForKey:@"data"];
        
        if (mutDict.count>0) {
            [_tbl_indie reloadData];
        }
        else
        {
            _tbl_indie.showsInfiniteScrolling = NO;
        }
        [_tbl_indie reloadData];
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
}
-(void)CallSearchMethod1
{
    [APP_DELEGATE showLoadingView:@""];
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    //Optional
    
    [AddPost setValue:[NSString stringWithFormat:@"%@",page_no] forKey:@"page_no"];
    if (![_txt_search.text isEqualToString:@""]) {
        [AddPost setValue:_txt_search.text forKey:@"search_text"];
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@get_indie_picks",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        page_no=[responseObject valueForKey:@"page_no"];
        
        if ([[responseObject valueForKey:@"data"] count]>0) {
            
            int64_t delayInSeconds = 2.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                //[_tbl_event beginUpdates];
                NSArray *arr = [responseObject valueForKey:@"data"];
                NSMutableArray *newarr = [[NSMutableArray alloc] init];
                NSLog(@"---%@",newarr);
                for (NSString *str in mutDict) {
                    [newarr addObject:str];
                }
                for (int i=0; i<arr.count; i++) {
                    //[mutDict addObject:arr[i]];
                    [newarr addObject:arr[i]];
                }
                NSLog(@"---%@",newarr);
                mutDict = [[NSMutableArray alloc] init];
                for (NSString*str  in newarr) {
                    [mutDict addObject:str];
                }
                
                
                NSLog(@"--%@",mutDict);
                [_tbl_indie.infiniteScrollingView stopAnimating];
                [_tbl_indie reloadData];
            });
            
        }
        else
        {
            [_tbl_indie.infiniteScrollingView stopAnimating];
        }
        
        //[_lbl_myevent reloadData];
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
    
    
}

#pragma mark - GetAllPosts
-(void)CallIndie
{
    [APP_DELEGATE showLoadingView:@""];
   
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];

    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpindie  = [[HttpWrapper alloc] init];
        httpindie.delegate=self;
        httpindie.getbool=NO;
        [httpindie requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_indie_picks",JsonUrlConstant] param:[AddPost copy]];
    });
}

#pragma mark- Wrapper Class Delegate Methods
//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary* )dicsResponse
{
    if(wrapper == httpindie && httpindie != nil)
    {
        NSLog(@"---%@",dicsResponse);
        NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
        mutDict=[[NSMutableArray alloc] init];
        mutDict=[dicsResponse valueForKey:@"data"];
        page_no=[dicsResponse valueForKey:@"page_no"];
        
        theAppDelegate.n_Count = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"notification_count"]];
        
        _lbl_notifi_count.text=theAppDelegate.n_Count;
        
        if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
            _lbl_notifi_count.hidden=YES;
            
           
            
        }
        else
        {
            _lbl_notifi_count.hidden=NO;
            
            _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
            _lbl_notifi_count.clipsToBounds=YES;
        }
        
        if (mutDict <=0) {
            _tbl_indie.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
            _tbl_indie.hidden=YES;
        }
        else
        {
            [_tbl_indie reloadData];
        }
        [APP_DELEGATE hideLoadingView];
    }
    
    if(wrapper == httplike && httplike != nil)
    {
        NSLog(@"---%@",dicsResponse);
        NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
        
        [self CallIndie];
        
        //[_tbl_indie reloadData];
        
        [APP_DELEGATE hideLoadingView];
    }
    
    if(wrapper == httpdislike && httpdislike != nil)
    {
        NSLog(@"---%@",dicsResponse);
        NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
        
        [self CallIndie];
        
        //[_tbl_indie reloadData];
        
        [APP_DELEGATE hideLoadingView];
    }
}


//fetchDataFail
- (void) HttpWrapper:(HttpWrapper* )wrapper fetchDataFail:(NSError* )error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
     [APP_DELEGATE hideLoadingView];
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return mutDict.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Cell";
    
    //tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.separatorColor = [UIColor clearColor];
    
    
    
    IndieCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[IndieCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.lbl_title.text=[NSString stringWithFormat:@"%@",[[mutDict  valueForKey:@"IndiePickTitle"] objectAtIndex:indexPath.row]];
    NSString *strVI=[NSString stringWithFormat:@"%@",[[mutDict  valueForKey:@"VideoOrImage"] objectAtIndex:indexPath.row]];
   
    [cell.btn_play_v setTag:indexPath.row];
    [cell.btn_play_v addTarget:self action:@selector(checkBoxClicked:event:)forControlEvents:UIControlEventTouchUpInside];
    
    
    cell.lbl_commont_count.text=[NSString stringWithFormat:@"%@",[mutDict valueForKey:@"TotalComments"][indexPath.row]];
    
    
    cell.lbl_like_count.text=[NSString stringWithFormat:@"%@",[[mutDict valueForKey:@"TotalLikes"] objectAtIndex:indexPath.row]];
    
    cell.lbl_dislike_count.text=[NSString stringWithFormat:@"%@",[[mutDict valueForKey:@"TotalDislikes"] objectAtIndex:indexPath.row]];
    
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]initWithArray:[[mutDict valueForKey:@"last_comments"] objectAtIndex:indexPath.row]];
    
    for (int i=0; i<arrTemp.count; i++) {
        switch (i) {
            case 0:
            {
                [cell.img_vw1 sd_setImageWithURL:[NSURL URLWithString:[arrTemp valueForKey:@"ImageURL"][0]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
                
                cell.lbl_title_vw1.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"FName"][0]];
                
                cell.lbl_desc_vw1.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"Comments"][0]];
                
            }
                break;
            case 1:
            {
                [cell.img_vw2 sd_setImageWithURL:[NSURL URLWithString:[arrTemp valueForKey:@"ImageURL"][1]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
                
                cell.lbl_title_vw2.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"FName"][1]];
                
                cell.lbl_desc_vw2.text=[NSString stringWithFormat:@"%@",[arrTemp valueForKey:@"Comments"][1]];
            }
                break;
                
            default:
                break;
        }
    }
    
    int C=[[[mutDict valueForKey:@"last_comments"] objectAtIndex:indexPath.row] count];
    
    if (C==2) {
        cell.vw_2.hidden=NO;
        cell.vw_1.hidden=NO;
    }
    else if (C==1)
    {
        cell.vw_1.hidden=NO;
        cell.vw_2.hidden=YES;
        
    }
    else
    {
        
        cell.vw_2.hidden=YES;
        cell.vw_1.hidden=YES;
    }
    
    
   
    
    if ([[mutDict valueForKey:@"IsLike"][indexPath.row] intValue]==0) {
        cell.btn_like.selected=NO;
    }
    else
    {
        cell.btn_like.selected=YES;
    }
    
    if ([[mutDict valueForKey:@"IsDisike"][indexPath.row] intValue]==0) {
        cell.btn_dislike.selected=NO;
    }
    else
    {
        cell.btn_dislike.selected=YES;
    }
    
    
    
    if ([strVI isEqualToString:@"V"]) {
        cell.btn_play_v.hidden=NO;
        /*NSURL *movieURL = [NSURL URLWithString:[[mutDict  valueForKey:@"VIURL"] objectAtIndex:indexPath.row]];
        cell.img_profile.image= [self imageFromMovie:movieURL atTime:15.0];*/
        
        NSString *str=[NSString stringWithFormat:@"%@", [[mutDict  valueForKey:@"VIURL"] objectAtIndex:indexPath.row]];  //is your str
        if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
            //Thard party thambanil
            _youTubeVideo = [[YKYouTubeVideo alloc] initWithContent:[NSURL URLWithString:[[mutDict  valueForKey:@"VIURL"] objectAtIndex:indexPath.row]]];
            [_youTubeVideo parseWithCompletion:^(NSError *error) {
                [_youTubeVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                    cell.img_profile.image = thumbImage;
                }];
            }];
        }
        else
        {
            //NSURL *movieURL = [NSURL URLWithString:[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
            _directVideo = [[YKDirectVideo alloc] initWithContent:[NSURL URLWithString:[[mutDict  valueForKey:@"VIURL"] objectAtIndex:indexPath.row]]];
            [_directVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                cell.img_profile.image = thumbImage;
            }];
        }
        
        
        
        
        
    }
    else
    {
        cell.btn_play_v.hidden=YES;
        [cell.img_profile sd_setImageWithURL:[NSURL URLWithString:[mutDict valueForKey:@"VIURL"][indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
        
    }
    
    [cell.img_profile setContentMode:UIViewContentModeScaleAspectFill];
    [cell.img_profile setClipsToBounds:YES];
    
    
    //////LIKE DISLIKE
    [cell.btn_like setTag:indexPath.row];
    [cell.btn_like addTarget:self action:@selector(CallLike:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.btn_dislike setTag:indexPath.row];
    [cell.btn_dislike addTarget:self action:@selector(CallDisLike:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.btn_comment setTag:indexPath.row];
    [cell.btn_comment addTarget:self action:@selector(ViewComments:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
   
  
    //================DATE TIME CALCULATION------------
    
    NSString * yourJSONString = [mutDict valueForKey:@"CreatedOn"][indexPath.row];
    
    NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
    [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateFromString = [currentDTFormatter dateFromString:yourJSONString];
    [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *eventDateStr = [currentDTFormatter stringFromDate:dateFromString];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *currentDate = [NSDate date];
    NSString *start = [formatter stringFromDate:dateFromString];
    NSString *end = [formatter stringFromDate:currentDate];
    //NSString *end = @"2018-01-11";
    
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *startDate = [f dateFromString:start];
    NSDate *endDate = [f dateFromString:end];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:(NSCalendarUnitDay |NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitWeekOfYear)
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    
    NSLog(@"Day %ld", [components day]);
    NSLog(@"Hour %ld", [components hour]);
    NSLog(@"Minute %ld", [components minute]);
    
    
    
    if ([components weekOfYear]>0) {
        cell.lbl_time.text=[NSString stringWithFormat:@"%ld week ago",(long)[components weekOfYear]];
    }
    else if ([components day]>0) {
        cell.lbl_time.text=[NSString stringWithFormat:@"%ld Day ago",(long)[components day]];
    }
    else if ([components hour]>0) {
        cell.lbl_time.text=[NSString stringWithFormat:@"%ld hours ago",(long)[components hour]];
    }
    else if ([components minute]>0)
    {
        cell.lbl_time.text=[NSString stringWithFormat:@"%ld min ago",(long)[components minute]];
    }
    
    
 
    
    //cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *simpleTableIdentifier = @"Cell";
    
    //tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.separatorColor = [UIColor clearColor];
    
    
    
    IndieCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[IndieCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    int C=[[[mutDict valueForKey:@"last_comments"] objectAtIndex:indexPath.row] count];
    
    if (C==2) {
        
        return 410;
        
    }
    else if (C==1)
    {
        cell.vw_2.hidden=YES;
        return 410-50;
        
        
        
    }
    else
    {
        cell.vw_2.hidden=YES;
        cell.vw_1.hidden=YES;
        
        return 410-100;
        
        
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    IndieCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (cell == nil) {
        cell = [[IndieCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    if (cell.selected) {
        // ... Uncheck
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
     NSString *strVI=[NSString stringWithFormat:@"%@",[[mutDict  valueForKey:@"VideoOrImage"] objectAtIndex:indexPath.row]];
    
    if (![strVI isEqualToString:@"V"]) {
        
     
        NSURL *url = [NSURL URLWithString:[mutDict valueForKey:@"VIURL"][indexPath.row]];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage* immg = [[UIImage alloc]initWithData:data];
        NSArray *landscapeImages = [[NSArray alloc]initWithObjects:immg, nil];
        
        Class cls = NSClassFromString(@"AFTNormalPagingViewController");
        AFTPagingBaseViewController *vc1 = [cls new];
        AFTPagingBaseViewController *bvc = (AFTPagingBaseViewController *)vc1;
        
        bvc.title = @"Photo";
        bvc.images = landscapeImages;
        [APP_DELEGATE hideLoadingView];
        [self presentViewController:bvc animated:YES completion:nil];
        
        
    
    }
    
    [self.view endEditing:YES];
}

-(void)ViewComments:(id)sender
{
    NSLog(@"ViewComments %@",sender);
    //NSInteger ival=[sender integerValue];
    
    UIButton *btn = (UIButton *)sender;
    CGPoint origin = btn.frame.origin;
    CGPoint point = [btn.superview convertPoint:origin toView:_tbl_indie];
    NSIndexPath * indexPath = [_tbl_indie indexPathForRowAtPoint:point];
    
    
    
        AllCommentView *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllCommentView"];
        next.user_portfolio_category_id=[mutDict valueForKey:@"IndiePickID"][indexPath.row];
        next.category_nm=@"IndiePick";
        [self.navigationController pushViewController:next animated:NO];
   
    
    
}


-(void)CallLike:(UIButton*)sender
{
    UIButton *btn=sender;
    
    NSLog(@"button was clicked for the following row %li", (long)btn.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[btn tag]  inSection:0];
    NSLog(@"indexpath.row %ld", (long)indexPath.row);
    NSLog(@"indexpath.row %ld", (long)indexPath.section);
    
    
    [APP_DELEGATE showLoadingView:@""];
    
    /*user_id
     indie_pick_id
     is_like */
    
    NSString *strID=[[mutDict valueForKey:@"IndiePickID"] objectAtIndex:indexPath.row];
    NSString *strISLike=[[mutDict valueForKey:@"IsLike"] objectAtIndex:indexPath.row];
    
    if ([strISLike isEqualToString:@"0"]) {
        strISLike=@"1";
    }
    else
    {
        strISLike=@"0";
    }
    
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:strID forKey:@"indie_pick_id"];
    [AddPost setValue:strISLike forKey:@"is_like"];
    
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httplike  = [[HttpWrapper alloc] init];
        httplike.delegate=self;
        httplike.getbool=NO;
        [httplike requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@indie_picks_like",JsonUrlConstant] param:[AddPost copy]];
    });
   
}

-(void)CallDisLike:(UIButton*)sender
{
    UIButton *btn=sender;
    
    NSLog(@"button was clicked for the following row %li", (long)btn.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[btn tag]  inSection:0];
    NSLog(@"indexpath.row %ld", (long)indexPath.row);
    NSLog(@"indexpath.row %ld", (long)indexPath.section);
    
    
    [APP_DELEGATE showLoadingView:@""];
    
    /*user_id
     indie_pick_id
     is_like */
    
    NSString *strID=[[mutDict valueForKey:@"IndiePickID"] objectAtIndex:indexPath.row];
    NSString *strISLike=[[mutDict valueForKey:@"IsDisike"] objectAtIndex:indexPath.row];
    
    if ([strISLike isEqualToString:@"0"]) {
        strISLike=@"1";
    }
    else
    {
        strISLike=@"0";
    }
    
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:strID forKey:@"indie_pick_id"];
    [AddPost setValue:strISLike forKey:@"is_dislike"];
    
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpdislike  = [[HttpWrapper alloc] init];
        httpdislike.delegate=self;
        httpdislike.getbool=NO;
        [httpdislike requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@indie_picks_dislike",JsonUrlConstant] param:[AddPost copy]];
    });
    
}


#pragma  VIDEO THAMNIL
- (UIImage* )imageFromMovie:(NSURL* )movieURL atTime:(NSTimeInterval)time {
    // set up the movie player
    MPMoviePlayerController *mp = [[MPMoviePlayerController alloc]
                                   initWithContentURL:movieURL];
    mp.shouldAutoplay = NO;
    mp.initialPlaybackTime = time;
    mp.currentPlaybackTime = time;
    // get the thumbnail
    thumbnail = [mp thumbnailImageAtTime:time
                              timeOption:MPMovieTimeOptionNearestKeyFrame];
    // clean up the movie player
    [mp stop];
    return(thumbnail);
}

- (void)checkBoxClicked:(id)sender event:(id)event
{
    UIButton *btn=sender;
    
    NSLog(@"button was clicked for the following row %li", (long)btn.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[btn tag]  inSection:0];
    NSLog(@"indexpath.row %ld", (long)indexPath.row);
    NSLog(@"indexpath.row %ld", (long)indexPath.section);
    
   /* NSURL *movieURL = [NSURL URLWithString:[[mutDict  valueForKey:@"VIURL"] objectAtIndex:indexPath.row]];
    [[UIApplication sharedApplication] openURL:movieURL];*/
    
    
    NSString *str=[NSString stringWithFormat:@"%@", [[mutDict  valueForKey:@"VIURL"] objectAtIndex:indexPath.row]];  //is your str
    if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
        
        /*  UIWebView *webView = [[UIWebView alloc] initWithFrame:self.view.bounds];  //Change self.view.bounds to a smaller CGRect if you don't want it to take up the whole screen
         [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]]]];
         [self.view addSubview:webView];*/
        
        WebPlayerVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"WebPlayerVC"];
        next.strurl=[[mutDict  valueForKey:@"VIURL"] objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:next animated:NO];
        
    }
    else
    {
        NSURL *videoURL = [NSURL URLWithString:[[mutDict  valueForKey:@"VIURL"] objectAtIndex:indexPath.row]];
        AVPlayer *player = [AVPlayer playerWithURL:videoURL];
        AVPlayerViewController *avplayerViewController = [AVPlayerViewController new];
        avplayerViewController.player = player;
        //[self.view addSubview:avplayerViewController.view];
        [self presentViewController:avplayerViewController animated:YES completion:nil];
    }
    
}
#pragma video image
/*
-(UIImage* )generateThumbImage : (NSURL* )filepath
{
    
    
    
    NSString *str=[NSString stringWithFormat:@"%@", filepath];  //is your str
    
    NSLog(@"%@",str);
    
    if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
        
        url = filepath;
        NSLog(@"string  contain youtube");
        
        AVAsset *asset = [AVAsset assetWithURL:url];
        AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
        imageGenerator.appliesPreferredTrackTransform = YES;
        
        CMTime time = [asset duration];
        time.value = 0;
        CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
        thumbnail = [UIImage imageWithCGImage:imageRef];
        CGImageRelease(imageRef);
        
        
    }
    else if(([str rangeOfString:@"mp4"].location == NSNotFound)){
        
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",str]];
        
        MPMoviePlayerController *mp = [[MPMoviePlayerController alloc]
                                       initWithContentURL:url];
        mp.shouldAutoplay = NO;
        mp.initialPlaybackTime = 5;
        mp.currentPlaybackTime = 5;
        // get the thumbnail
        thumbnail = [mp thumbnailImageAtTime:5
                                  timeOption:MPMovieTimeOptionNearestKeyFrame];
        // clean up the movie player
        [mp stop];
        
    } else {
        NSLog(@"string contains mp4!");
        
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",str]];
        
        MPMoviePlayerController *mp = [[MPMoviePlayerController alloc]
                                       initWithContentURL:url];
        mp.shouldAutoplay = NO;
        mp.initialPlaybackTime = 5;
        mp.currentPlaybackTime = 5;
        // get the thumbnail
        thumbnail = [mp thumbnailImageAtTime:5
                                  timeOption:MPMovieTimeOptionNearestKeyFrame];
        // clean up the movie player
        [mp stop];
        
    }
    
    return thumbnail;
}
*/



-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    int lastRow=[mutDict count]-1;
    if(([indexPath row] == lastRow)&&(lastRow<[mutDict count]))
    {
        //[self Getnewdata];
        NSLog(@"i m call");
        
        if ([mutDict count]>=2) {
            __weak IndieVC *weakSelf = self;
            // setup infinite scrolling
            [_tbl_indie addInfiniteScrollingWithActionHandler:^{
                [weakSelf CallSearchMethod1];
            }];
        }
        
        /* last_eventid=[[mutDict valueForKey:@"EventID"]lastObject];
         if ([last_eventid integerValue]<1) {
         _tbl_event.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
         }*/
        //[self CallSearchMethod];
        
    }
}
- (void)touchesBegan:(NSSet* )touches withEvent:(UIEvent* )event {
    [self.view endEditing:YES];
    
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    DSCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    //cell.layer.cornerRadius=cell.frame.size.height/2;
    //cell.layer.borderWidth=1.5;
    //cell.layer.borderColor=[UIColor whiteColor].CGColor;
    cell.img_icon.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",[arrimg objectAtIndex:indexPath.row]]];
    [self playAlertSoundPressed:nil];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self playAlertSoundPressed:nil];
    [self SelectedMenu:indexPath.row];
    
    
    
    
}
- (void)SelectedMenu:(NSInteger)index{
    
    NSLog(@"%ld",(long)index);
    
    switch (index) {
        case 0:
        {
            //Perform any action that u want on menu selection
            EventVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"EventVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            break;
            
        case 1:{
              NewsVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsVC"];
             [self.navigationController pushViewController:next animated:NO];
            
        }
            
            break;
        case 2:
        {
            CritiquesVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"CritiquesVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 3:
        {
            ClassifiedVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"ClassifiedVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 4:
        {
            LearningVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"LearningVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 5:
        {
           /* IndieVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"IndieVC"];
            [self.navigationController pushViewController:next animated:NO];*/
        }
            
            break;
        case 6:
        {
            FundingVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"FundingVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            break;
        case 7:
        {
            TeamBuild1VC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"TeamBuild1VC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            break;

            
            break;
        default:
            break;
    }
}

- (IBAction)btn_NEWMENU:(id)sender {
    [self playAlertSoundPressed:nil];
    if (isShow==false) {
        _collection_view.hidden=NO;
        _img_colle_back.hidden=NO;
        isShow=true;
    }
    else
    {
        _collection_view.hidden=YES;
        _img_colle_back.hidden=YES;
        isShow=false;
    }
    
}

- (IBAction)playAlertSoundPressed:(UIButton *)sender
{
    [[JSQSystemSoundPlayer sharedPlayer] playAlertSoundWithFilename:@"Click2-Sebastian"
                                                      fileExtension:kJSQSystemSoundTypeAIFF
                                                         completion:nil];
}


- (IBAction)btn_NOTIFY:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_HOME:(id)sender {
    
           NewHomeVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
        [self.navigationController pushViewController:next animated:YES];
    
}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}

@end
