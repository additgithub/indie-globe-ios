//
//  MyEventDetailsCell.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/7/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "MyEventDetailsCell.h"

@implementation MyEventDetailsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _img_profileimg.layer.masksToBounds=YES;
    _img_profileimg.layer.cornerRadius=_img_profileimg.frame.size.height/2;
    //_btn_rsvp.layer.cornerRadius=_btn_rsvp.frame.size.height/2;
    _vw_rsvp.layer.masksToBounds=YES;
    _vw_rsvp.layer.cornerRadius=_vw_rsvp.frame.size.height/2;
    
    _vw_scmi.layer.masksToBounds=YES;
    _vw_scmi.layer.cornerRadius=_vw_scmi.frame.size.height/2;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
