//
//  EventDetailsVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/28/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
#import "ApplicationConstant.h"
#import "NewHomeSearchVC.h"

@interface EventDetailsVC : UIViewController<HttpWrapperDelegate>
{
    UIView *picker;
    
    HttpWrapper* httpeventdetails;
}

//Outlet
@property (nonatomic,retain)NSMutableDictionary *mutDict;
@property (nonatomic,retain) NSString *FromFlag;
@property (nonatomic,retain)NSString *btn_enable;

@property (strong, nonatomic) IBOutlet UILabel *lbl_rsvp_count;

@property (strong, nonatomic) IBOutlet UIButton *btn_menu;
@property (strong, nonatomic) IBOutlet UIImageView *img_profile;
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;
@property (strong, nonatomic) IBOutlet UILabel *lbl_posted_on;
@property (strong, nonatomic) IBOutlet UILabel *lbl_address;
@property (strong, nonatomic) IBOutlet UIButton *btn_scmi;
@property (strong, nonatomic) IBOutlet UIButton *btn_rsvp;
@property (strong, nonatomic) IBOutlet UITextView *txt_text_info;
@property (strong, nonatomic) IBOutlet UIImageView *img_prof_image;
@property (strong, nonatomic) IBOutlet UILabel *lbl_usernm;

@property (weak, nonatomic) IBOutlet UIScrollView *scroll_event_img;
@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;

//Action
- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;
- (IBAction)btn_RSVP_ACTION:(id)sender;
- (IBAction)btn_SCMI_ACTION:(id)sender;
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_NOTIFY:(id)sender;




@end
