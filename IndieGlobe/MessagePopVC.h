//
//  MessagePopVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/28/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
#import "ApplicationConstant.h"
#import "UIViewController+ENPopUp.h"

@interface MessagePopVC : UIViewController<HttpWrapperDelegate>
{
    HttpWrapper *httpsendmsg;
}
@property (retain , nonatomic)NSString *Uid;
@property (strong, nonatomic) IBOutlet UIView *vw_backgroung;
@property (strong, nonatomic) IBOutlet UIView *vw_msg;

//Outlet
@property (strong, nonatomic) IBOutlet UITextView *txt_msg;
@property (strong, nonatomic) IBOutlet UIButton *btn_send;





//Action
- (IBAction)btn_SEND:(id)sender;



@end
