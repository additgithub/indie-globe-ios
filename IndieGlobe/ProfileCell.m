//
//  ProfileCell.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/29/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "ProfileCell.h"

@implementation ProfileCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _img_photo.layer.masksToBounds=YES;
    _img_movies.layer.masksToBounds=YES;
    _img_sfilm.layer.masksToBounds=YES;

    _img_photo.layer.cornerRadius=6;
    _img_movies.layer.cornerRadius=6;
    _img_sfilm.layer.cornerRadius=6;
    
    
}


@end
