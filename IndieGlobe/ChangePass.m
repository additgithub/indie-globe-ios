//
//  ChangePass.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 1/4/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "ChangePass.h"
#import "TextFieldValidator.h"
#import "UIColor+CL.h"

#define REGEX_USER_NAME_LIMIT @"^.{3,10}$"
#define REGEX_USER_NAME @"[A-Za-z0-9]{3,10}"
#define REGEX_EMAIL @"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
#define REGEX_PASSWORD_LIMIT @"^.{6,20}$"
#define REGEX_PASSWORD @"[A-Za-z0-9]{6,20}"
#define REGEX_PHONE_DEFAULT @"[0-9]{3}\\-[0-9]{3}\\-[0-9]{4}"

@interface ChangePass ()

@end

@implementation ChangePass

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden=YES;
    
    _txt_currpass.placeholder=@"Enter your current password";
    _txt_newpass.placeholder=@"Enter new password";
    _txt_confpass.placeholder=@"Re-enter your new password";
    
    
    [self textlayers:_txt_currpass];
    [self textlayers:_txt_newpass];
    [self textlayers:_txt_confpass];
    
    _btn_save.layer.cornerRadius=_btn_save.frame.size.height/2;
    _btn_cancle.layer.cornerRadius=_btn_cancle.frame.size.height/2;

    [self setupAlerts];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setupAlerts{
    
    [_txt_newpass addRegx:REGEX_PASSWORD_LIMIT withMsg:@"Password charaters limit should be come between 3-10"];
    
    [_txt_newpass addRegx:REGEX_PASSWORD withMsg:@"Enter Valid Password"];
    
   [_txt_confpass addConfirmValidationTo:_txt_newpass withMsg:@"Confirm password didn't match."];
    
}
#pragma mark - GetAllPosts
-(void)CallChangePass
{
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];

    [AddPost setValue:_txt_currpass.text forKey:@"old_password"];
    [AddPost setValue:_txt_newpass.text forKey:@"new_password"];
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpchangepass  = [[HttpWrapper alloc] init];
        httpchangepass.delegate=self;
        httpchangepass.getbool=NO;
        [httpchangepass requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@change_password",JsonUrlConstant] param:[AddPost copy]];
    });
}

#pragma mark- Wrapper Class Delegate Methods
//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary* )dicsResponse
{
    if(wrapper == httpchangepass && httpchangepass != nil)
    {
        NSLog(@"Pass change success..");
        NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
        if ([[dicsResponse valueForKey:@"status"] integerValue] == 0) {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //[alert show];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //[alert show];
            [self.navigationController popViewControllerAnimated:YES];
            
        }

    }
}


//fetchDataFail
- (void) HttpWrapper:(HttpWrapper* )wrapper fetchDataFail:(NSError* )error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
}

-(void)textlayers :(UITextField *)UITextField
{
    
    UITextField.layer.borderWidth=1.5;
    UITextField.layer.borderColor=[UIColor colorWithHex:0x17C4C7].CGColor;
    CGRect frameRect = UITextField.frame;
    frameRect.size.height = 30;
    UITextField.frame = frameRect;
    UITextField.layer.cornerRadius=UITextField.frame.size.height/2;
    
    [UITextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
    UITextField.leftView = paddingView;
    UITextField.leftViewMode = UITextFieldViewModeAlways;
    
    
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (void)touchesBegan:(NSSet* )touches withEvent:(UIEvent* )event {
    
    [self.view endEditing:YES];
    //[_vw_scroll endEditing:YES];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_SAVE_ACTION:(id)sender {
    
    if ([_txt_newpass validate] && [_txt_confpass validate] && [_txt_currpass validate]) {
        NSLog(@"Password Change Successfully.");
        [self CallChangePass];
    }
    else
    {
        NSLog(@"Error");
    }
    
    
}

- (IBAction)btn_CANCLE_ACTION:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

}
@end
