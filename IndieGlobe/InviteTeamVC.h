//
//  InviteTeamVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/4/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopOverView.h"
#import "HttpWrapper.h"
#import "ApplicationConstant.h"
#import "NewHomeSearchVC.h"
#import "Firebase.h"


//@import Firebase;

@interface InviteTeamVC : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,HttpWrapperDelegate>
{
    UIView *picker;
    
    UITextField *currentTextField;
    int isCheck;
    NSMutableArray* categor;
    IBOutlet UITextField *txt_categ;
    NSString *categorid;


    HttpWrapper* httpinviteam;
    
    NSMutableArray *mutDict;
    NSString *page_no;
}

@property (strong, nonatomic) FIRDatabaseReference *refOnline;



@property (retain,nonatomic)NSString *projID;

//Outlet
@property (strong, nonatomic) IBOutlet UIButton *btn_menu;
@property (strong, nonatomic) IBOutlet UIButton *btn_search;
@property (strong, nonatomic) IBOutlet UITextField *txt_search;
@property (strong, nonatomic) IBOutlet UICollectionView *coll_invite_team;
@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;


//Action
- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;
- (IBAction)btn_SCATEG:(id)sender;
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_NOTIFY:(id)sender;

-(void)Popvalueselected:(NSString*)strValue;
-(void)Popvalveselected:(NSString*)strValve;
-(void)dismissPopUpViewController;




@end
