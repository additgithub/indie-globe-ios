//
//  CritiInfoVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/30/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "CritiInfoVC.h"
#import "SWRevealViewController.h"
#import "DeshboardVC.h"
#import "Constants.h"
#import "UIColor+CL.h"
#import "NewsVC.h"
#import "EventVC.h"
#import "CritiInfoCell.h"
#import "NewHomeVC.h"
#import "MessagePopVC.h"
#import "ReplayCritiques.h"
#import "Commentsview.h"
#import "AllNotificationVC.h"
#import "ReplayVC.h"

#import "AllCommentView.h"
#import "AllCommentViewCell.h"
#import "AFTPagingBaseViewController.h"








@interface CritiInfoVC ()

@end

@implementation CritiInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    [self customSetup];
    // Do any additional setup after loading the view.
    
    
    //[self.vw_critiVW setShadow];
    [self.shadow_vw setShadow];
    
    _tbl_criritab.tableHeaderView = _header_vw;
    
    page_no=@"0";
    _img_image.userInteractionEnabled=YES;
    
 
    if ([_fromMyCriti intValue]==1) {
        _lbl_header_title.text=@"My Critiques";
    }
    
    //------------
    _btn_post_comm.layer.cornerRadius=_btn_post_comm.frame.size.height/2;
    
    _txt_comment.delegate=self;
    _txt_comment.text = @"Your Comment";
    _txt_comment.textColor = [UIColor lightGrayColor];
    _txt_comment.layer.borderWidth=1.0;
    _txt_comment.layer.cornerRadius=11;
    _txt_comment.layer.borderColor=[UIColor colorWithRed:(160.0f/255.0) green:(160.0f/255.0) blue:(160.0f/255.0) alpha:1].CGColor;
    
    [self CustomizeTextField:_txt_opinion];
    [self CustomizeTextField:_txt_solution];
    _txt_solution.placeholder=@"Enter your solutions";
    _txt_opinion.placeholder=@"Enter your opinions";
    
    
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];
    
    _txt_comment.delegate=self;
    _txt_comment.text = @"Enter your solutions";
    _txt_comment.textColor = [UIColor lightGrayColor];
    _txt_comment.layer.borderWidth=1.0;
    _txt_comment.layer.cornerRadius=6;
    _txt_comment.layer.borderColor=[UIColor colorWithRed:(160.0f/255.0) green:(160.0f/255.0) blue:(160.0f/255.0) alpha:1].CGColor;
    
    
    //[self CallMyMethod];
    
    
    NSLog(@"----%@",_mutDict);
    if ([_FromFlag integerValue]==2) {
        [_img_image sd_setImageWithURL:[NSURL URLWithString:[_mutDict valueForKey:@"Image"]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
    }
    else
    {
        [_img_image sd_setImageWithURL:[NSURL URLWithString:[_mutDict valueForKey:@"PhotoURL"]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
        
        
        
    }
    
    _img_image.contentMode = UIViewContentModeScaleAspectFit;
    
    
    
    
    if ([_FromFlag integerValue]==2) {
        
        _lbl_like.text=[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"TotalLike"]];
        _lbl_dislike.text=[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"TotalDislike"]];
        _lbl_comment.text=[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"TotalComment"]];
        
        
        [_btn_like addTarget:self action:@selector(CallLike:) forControlEvents:UIControlEventTouchUpInside];
        [_btn_dislike addTarget:self action:@selector(CallDisLike:) forControlEvents:UIControlEventTouchUpInside];
        // [_btn_comment addTarget:self action:@selector(ViewComments:) forControlEvents:UIControlEventTouchUpInside];
        
        
        if ([[_mutDict valueForKey:@"IsLike"] intValue]==0) {
            _btn_like.selected=NO;
        }
        else
        {
            _btn_like.selected=YES;
        }
        
        if ([[_mutDict valueForKey:@"IsDislike"] intValue]==0) {
            _btn_dislike.selected=NO;
        }
        else
        {
            _btn_dislike.selected=YES;
        }
        
    }
    else
    {
        _lbl_like.text=[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"TotalLikes"]];
        _lbl_dislike.text=[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"TotalDislikes"]];
        _lbl_comment.text=[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"TotalComment"]];
        
        
        [_btn_like addTarget:self action:@selector(CallLike:) forControlEvents:UIControlEventTouchUpInside];
        [_btn_dislike addTarget:self action:@selector(CallDisLike:) forControlEvents:UIControlEventTouchUpInside];
        // [_btn_comment addTarget:self action:@selector(ViewComments:) forControlEvents:UIControlEventTouchUpInside];
        
        
        if ([[_mutDict valueForKey:@"IsLike"] intValue]==0) {
            _btn_like.selected=NO;
        }
        else
        {
            _btn_like.selected=YES;
        }
        
        if ([[_mutDict valueForKey:@"IsDisike"] intValue]==0) {
            _btn_dislike.selected=NO;
        }
        else
        {
            _btn_dislike.selected=YES;
        }
        
    }
    
    
    
    
    ////=========DATE CONVERT=====================
    NSString * yourJSONString = [_mutDict valueForKey:@"PostedDate"];
    
    NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
    [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateFromString = [currentDTFormatter dateFromString:yourJSONString];
    [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *eventDateStr = [currentDTFormatter stringFromDate:dateFromString];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *currentDate = [NSDate date];
    NSString *start = [formatter stringFromDate:dateFromString];
    NSString *end = [formatter stringFromDate:currentDate];
    //NSString *end = @"2018-01-11";
    
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *startDate = [f dateFromString:start];
    NSDate *endDate = [f dateFromString:end];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:(NSCalendarUnitDay |NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitWeekOfYear)
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    
    NSLog(@"Day %ld", [components day]);
    NSLog(@"Hour %ld", [components hour]);
    NSLog(@"Minute %ld", [components minute]);
    
    
    
    if ([components weekOfYear]>0) {
        _lbl_postdt.text=[NSString stringWithFormat:@"%ld week ago",(long)[components weekOfYear]];
    }
    else if ([components hour]>0) {
        _lbl_postdt.text=[NSString stringWithFormat:@"%ld hours ago",(long)[components hour]];
    }
    else if ([components minute]>0)
    {
        _lbl_postdt.text=[NSString stringWithFormat:@"%ld min ago",(long)[components minute]];
    }
    else{
        _lbl_postdt.text=[NSString stringWithFormat:@"%ld day ago",(long)[components day]];
    }
    
    ////================
    _img_user_image.clipsToBounds=YES;
    _img_user_image.layer.cornerRadius=_img_user_image.layer.frame.size.height/2;
    
    [_img_user_image sd_setImageWithURL:[NSURL URLWithString:[_mutDict valueForKey:@"UserImageURL"]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
    
    
    
    _lbl_post_name.text=[NSString stringWithFormat:@"%@ %@",[_mutDict valueForKey:@"FName"],[_mutDict valueForKey:@"LName"]];
    _lbl_title.text=[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"Title"]];
    //_lbl_postdt.text=[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"PostedDate"]];
    //_lbl_title.text=[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"Questions"]];
    _lbl_desc.text=[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"Questions"]];
    
    [_btn_post_comm addTarget:self action:@selector(CallAddComment) forControlEvents:UIControlEventTouchUpInside];
    /*  if (![_txt_comment.text isEqualToString:@""]&&![_txt_comment.text isEqualToString:@"Your Comment"]) {
     
     }
     else
     {
     NSLog(@"Add Comment First!!!");
     }*/
    
    
    ///////AUTO SPACING
    
    _lbl_desc.numberOfLines = 0; // allows label to have as many lines as needed
    [_lbl_desc sizeToFit];
    
    CGRect mainframe = _lbl_desc.frame;
    mainframe.size.height =  [self heightForText:[_mutDict valueForKey:@"Questions"]];
    _lbl_desc.frame = mainframe;
    
    CGRect frame1 = _header_vw.frame;
    frame1.size.height =  288+_lbl_desc.frame.size.height;
    _header_vw.frame = frame1;
    
    
    /*  CGRect buttonframe1 = _lbl_desc.frame;
     buttonframe1.origin.y = _img_image.frame.origin.y+_lbl_title.frame.origin.y+ 35;
     _lbl_desc.frame = buttonframe1;
     
     CGRect autosize1 = _lbl_solution_lab.frame;
     autosize1.origin.y = _lbl_desc.frame.origin.y + [self heightForText:[_mutDict valueForKey:@"Questions"]] + 10;
     _lbl_solution_lab.frame = autosize1;
     
     
     CGRect autosize2 = _txt_comment.frame;
     autosize2.origin.y = _lbl_solution_lab.frame.origin.y + _lbl_solution_lab.frame.size.height + 4;
     _txt_comment.frame = autosize2;
     
     
     CGRect autosize3 = _btn_post_comm.frame;
     autosize3.origin.y = _txt_comment.frame.origin.y + _txt_comment.frame.size.height + 10;
     _btn_post_comm.frame = autosize3;
     
     
     CGRect autosize4 = _lbl_other_lab.frame;
     autosize4.origin.y = _btn_post_comm.frame.origin.y + _btn_post_comm.frame.size.height + 10;
     _lbl_other_lab.frame = autosize4;
     
     
     
     CGSize imageSize = _img_image.frame.size;
     CGRect imagefram = _vw_critiVW1.frame;
     imagefram.origin.y = imageSize.height;
     _vw_critiVW1.frame = imagefram;
     
     
     CGRect autosize5 = _vw_critiVW.frame;
     autosize5.size.height = _vw_critiVW1.frame.origin.y + _vw_critiVW1.frame.size.height;
     _vw_critiVW.frame = autosize5;
     */
    
    [self CallCritiInfo];
    
    
    UITapGestureRecognizer *letterTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(highlightLetter:)];
    letterTapRecognizer.numberOfTapsRequired = 1;
    [_img_image addGestureRecognizer:letterTapRecognizer];
    
    _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
    _lbl_notifi_count.clipsToBounds=YES;
    _lbl_notifi_count.text=theAppDelegate.n_Count;
    
    if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
        _lbl_notifi_count.hidden=YES;
    }
    else
    {
        _lbl_notifi_count.hidden=NO;
    }
}


-(void)highlightLetter:(UITapGestureRecognizer*)sender
{
    
    [APP_DELEGATE showLoadingView:@""];
    NSLog(@"click on image");
    
    [APP_DELEGATE showLoadingView:@""];
    
    if ([_FromFlag integerValue]==2) {
        
        NSURL *url = [NSURL URLWithString:[_mutDict valueForKey:@"Image"]];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage* immg = [[UIImage alloc]initWithData:data];
        NSArray *landscapeImages = [[NSArray alloc]initWithObjects:immg, nil];
        
        Class cls = NSClassFromString(@"AFTNormalPagingViewController");
        AFTPagingBaseViewController *vc1 = [cls new];
        AFTPagingBaseViewController *bvc = (AFTPagingBaseViewController *)vc1;
        
        bvc.title = @"Photo";
        bvc.images = landscapeImages;
        [APP_DELEGATE hideLoadingView];
        [self presentViewController:bvc animated:YES completion:nil];
        
        
    }else
    {
        
        NSURL *url = [NSURL URLWithString:[_mutDict valueForKey:@"PhotoURL"]];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage* immg = [[UIImage alloc]initWithData:data];
        NSArray *landscapeImages = [[NSArray alloc]initWithObjects:immg, nil];
        
        Class cls = NSClassFromString(@"AFTNormalPagingViewController");
        AFTPagingBaseViewController *vc1 = [cls new];
        AFTPagingBaseViewController *bvc = (AFTPagingBaseViewController *)vc1;
        
        bvc.title = @"Photo";
        bvc.images = landscapeImages;
        [APP_DELEGATE hideLoadingView];
        [self presentViewController:bvc animated:YES completion:nil];
        
    }
    
}



-(CGFloat)heightForText:(NSString *)text
{
    NSInteger MAX_HEIGHT = 2000;
    UITextView * textView = [[UITextView alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width-80, MAX_HEIGHT+150)];
    textView.text = text;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        textView.font = [UIFont fontWithName:@"Roboto-Light" size:12.0];
    }
    else
    {
        textView.font = [UIFont fontWithName:@"Roboto-Light" size:12.0];
    }
    
    [textView sizeToFit];
    
    return textView.frame.size.height;
}

-(void)ViewComments:(id)sender
{
    NSLog(@"ViewComments %@",sender);
    //NSInteger ival=[sender integerValue];
    
    UIButton *btn = (UIButton *)sender;
    CGPoint origin = btn.frame.origin;
    // point = [btn.superview convertPoint:origin toView:_tbl_indie];
    //NSIndexPath * indexPath = [_tbl_indie indexPathForRowAtPoint:point];
    
    
    
    AllCommentView *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllCommentView"];
    next.user_portfolio_category_id=[_mutDict valueForKey:@"CritiquesID"];
    next.category_nm=@"Critiques";
    [self.navigationController pushViewController:next animated:NO];
    
    
    
}



-(void)CallLike:(UIButton*)sender
{
    UIButton *btn=sender;
    
    NSLog(@"button was clicked for the following row %li", (long)btn.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[btn tag]  inSection:0];
    NSLog(@"indexpath.row %ld", (long)indexPath.row);
    NSLog(@"indexpath.row %ld", (long)indexPath.section);
    
    
    [APP_DELEGATE showLoadingView:@""];
    
    /*user_id
     indie_pick_id
     is_like */
    NSString *strID=[[NSString alloc]init];
    
    if ([_FromFlag integerValue]==2) {
        strID=[_mutDict valueForKey:@"ID"];
    }
    else
    {
        strID=[_mutDict valueForKey:@"CritiquesID"];
    }
    
    
    
    NSString *strISLike=[_mutDict valueForKey:@"IsLike"];
    
    if ([strISLike isEqualToString:@"0"]) {
        strISLike=@"1";
    }
    else
    {
        strISLike=@"0";
    }
    
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:strID forKey:@"critiques_id"];
    [AddPost setValue:strISLike forKey:@"is_like"];
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"PARAM---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@critiques_like",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        
        NSMutableArray *tempData=[responseObject valueForKey:@"data"];
        
        _lbl_like.text=[NSString stringWithFormat:@"%@",[tempData valueForKey:@"TotalLikes"]];
        _lbl_dislike.text=[NSString stringWithFormat:@"%@",[tempData valueForKey:@"TotalDislikes"]];
        _lbl_comment.text=[NSString stringWithFormat:@"%@",[tempData valueForKey:@"TotalComment"]];
        
        
        
        if ([[tempData valueForKey:@"IsLike"] intValue]==0) {
            _btn_like.selected=NO;
        }
        else
        {
            _btn_like.selected=YES;
        }
        
        if ([[tempData valueForKey:@"IsDisike"] intValue]==0) {
            _btn_dislike.selected=NO;
        }
        else
        {
            _btn_dislike.selected=YES;
        }
        
        //[self CallCritiInfo];
        
        [APP_DELEGATE hideLoadingView];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
}

-(void)CallDisLike:(UIButton*)sender
{
    UIButton *btn=sender;
    
    NSLog(@"button was clicked for the following row %li", (long)btn.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[btn tag]  inSection:0];
    NSLog(@"indexpath.row %ld", (long)indexPath.row);
    NSLog(@"indexpath.row %ld", (long)indexPath.section);
    
    
    [APP_DELEGATE showLoadingView:@""];
    
    
    
    NSString *strID=[[NSString alloc]init];
    
    if ([_FromFlag integerValue]==2) {
        strID=[_mutDict valueForKey:@"ID"];
    }
    else
    {
        strID=[_mutDict valueForKey:@"CritiquesID"];
    }
    
    NSString *strISLike=[_mutDict valueForKey:@"IsDisike"];
    
    if ([strISLike isEqualToString:@"0"]) {
        strISLike=@"1";
    }
    else
    {
        strISLike=@"0";
    }
    
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:strID forKey:@"critiques_id"];
    [AddPost setValue:strISLike forKey:@"is_dislike"];
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"PARAM---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@critiques_dislike",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        
        NSMutableArray *tempData=[responseObject valueForKey:@"data"];
        
        _lbl_like.text=[NSString stringWithFormat:@"%@",[tempData valueForKey:@"TotalLikes"]];
        _lbl_dislike.text=[NSString stringWithFormat:@"%@",[tempData valueForKey:@"TotalDislikes"]];
        _lbl_comment.text=[NSString stringWithFormat:@"%@",[tempData valueForKey:@"TotalComment"]];
        
        
        if ([[tempData valueForKey:@"IsLike"] intValue]==0) {
            _btn_like.selected=NO;
        }
        else
        {
            _btn_like.selected=YES;
        }
        
        if ([[tempData valueForKey:@"IsDisike"] intValue]==0) {
            _btn_dislike.selected=NO;
        }
        else
        {
            _btn_dislike.selected=YES;
        }
        
        
        
        //[self CallCritiInfo];
        
        [APP_DELEGATE hideLoadingView];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
    
    
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self customSetup];
}


- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if (IsIphone7 || IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = 145;
    }
    else{
        self.revealViewController.rearViewRevealWidth = 145;
    }
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if([textView.text isEqual: @"Enter your solutions"])
    {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    return YES;
}

-(void)CustomizeTextField:(UITextField *)myTextField
{
    myTextField.layer.borderWidth=1.0;
    myTextField.layer.borderColor=[UIColor colorWithRed:(160.0f/255.0) green:(160.0f/255.0) blue:(160.0f/255.0) alpha:1].CGColor;
    CGRect frameRect = myTextField.frame;
    //frameRect.size.height = 28;
    myTextField.frame = frameRect;
    myTextField.layer.cornerRadius=myTextField.frame.size.height/2;
    
    [myTextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,6, 20)];
    myTextField.leftView = paddingView;
    myTextField.leftViewMode = UITextFieldViewModeAlways;
    
    if (myTextField==_txt_solution) {
        frameRect.size.height = 45;
    }
    else
    {
        frameRect.size.height = 28;
    }
    
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}
/*
 -(void)CallAddComment
 {
 
 
 [APP_DELEGATE showLoadingView:@""];
 NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
 NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
 [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
 
 if ([_FromFlag integerValue]==2) {
 [AddPost setValue:[_mutDict valueForKey:@"ID"] forKey:@"critique_id"];
 }else
 {
 [AddPost setValue:[_mutDict valueForKey:@"CritiquesID"] forKey:@"critique_id"];
 }
 
 
 
 
 [AddPost setValue:txt.text forKey:@"comment"];
 
 AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
 NSLog(@"PARAM---%@",AddPost);
 
 [manager POST:[NSString stringWithFormat:@"%@add_solution",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
 NSLog(@"JSON: %@", responseObject);
 NSLog(@"---%@",responseObject);
 
 
 
 //[self CallCritiInfo];
 
 [APP_DELEGATE hideLoadingView];
 
 
 } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
 
 NSLog(@"Error: %@", error);
 [APP_DELEGATE hideLoadingView];
 }];
 
 
 
 
 [self.view endEditing:YES];
 }
 */
-(void)CallSend:(id)sender
{
    
    
    [self.view endEditing:YES];
    [APP_DELEGATE showLoadingView:@""];
    
    
    NSLog(@"User Like %@",sender);
    //NSInteger ival=[sender integerValue];
    
    UIButton *btn = (UIButton *)sender;
    CGPoint origin = btn.frame.origin;
    CGPoint point = [btn.superview convertPoint:origin toView:_tbl_criritab];
    NSIndexPath * indexPath = [_tbl_criritab indexPathForRowAtPoint:point];
    
    CritiInfoCell *cell=(CritiInfoCell *)[_tbl_criritab cellForRowAtIndexPath:indexPath];
    
    //Post Method Request
    [APP_DELEGATE showLoadingView:@""];
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    if ([_FromFlag integerValue]==2) {
        [AddPost setValue:[_mutDict valueForKey:@"ID"] forKey:@"critique_id"];
    }else
    {
        [AddPost setValue:[_mutDict valueForKey:@"CritiquesID"] forKey:@"critique_id"];
    }
    
    
    
    
    [AddPost setValue:cell.txt_comme.text forKey:@"comment"];
    
    //------------
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"PARAM---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@add_solution",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            cell.txt_comme.text=@"";
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Comment added successfully"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            //[alert show];
            
            _lbl_comment.text=[NSString stringWithFormat:@"%d",[_lbl_comment.text intValue]+1];
        }
        
        
        [self CallCritiInfo];
        
        [APP_DELEGATE hideLoadingView];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
}

#pragma mark - GetAllPosts
-(void)CallCritiInfo
{
    [APP_DELEGATE showLoadingView:@""];
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    if ([_FromFlag integerValue]==2) {
        [AddPost setValue:[_mutDict valueForKey:@"ID"] forKey:@"critique_id"];
    }else
    {
        [AddPost setValue:[_mutDict valueForKey:@"CritiquesID"] forKey:@"critique_id"];
    }
    
    
    [AddPost setValue:[NSString stringWithFormat:@"%@",page_no] forKey:@"page_no"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"PARAM---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@get_comment_list",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        //page_no=[responseObject valueForKey:@"page_no"];
        
        resDict=[responseObject valueForKey:@"data"];
        
        if (resDict.count<1) {
            NSLog(@"NO DATA");
        }
        else
        {
            [_tbl_criritab reloadData];
        }
        
        
        
        [APP_DELEGATE hideLoadingView];
        
        
        if ([[responseObject valueForKey:@"data"] isEqual:[NSNull null]]) {
            //mutDict=[[NSMutableArray alloc] init];
        }
        else
        {
            /*  int64_t delayInSeconds = 4.0;
             dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
             dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
             
             //[_tbl_event beginUpdates];
             
             });*/
            /*
             NSArray *arr = [responseObject valueForKey:@"data"];
             if (arr.count<1) {
             _coll_criti.showsInfiniteScrolling = NO;
             }
             NSMutableArray *newarr = [[NSMutableArray alloc] init];
             NSLog(@"---%@",newarr);
             for (NSString *str in mutDict) {
             [newarr addObject:str];
             }
             for (int i=0; i<arr.count; i++) {
             //[mutDict addObject:arr[i]];
             [newarr addObject:arr[i]];
             }
             NSLog(@"---%@",newarr);
             mutDict = [[NSMutableArray alloc] init];
             for (NSString*str  in newarr) {
             [mutDict addObject:str];
             }
             //[_coll_criti.infiniteScrollingView stopAnimating];
             [_coll_criti reloadData];
             */
            
        }
        
        
        
        //[_coll_criti reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
}

#pragma mark- Wrapper Class Delegate Methods
//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary* )dicsResponse
{
    if(wrapper == httpcritiinfo && httpcritiinfo != nil)
    {
        
    }
    [APP_DELEGATE hideLoadingView];
}


//fetchDataFail
- (void) HttpWrapper:(HttpWrapper* )wrapper fetchDataFail:(NSError* )error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
}

#pragma mark Table View Data Source Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 2;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (section==0) {
        return resDict.count;
    }
    else
    {
        return 1;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //self.tableView.separatorColor = [UIColor clearColor];
    
    
    if (indexPath.section == 0) {
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        static NSString *simpleTableIdentifier = @"Cell";
        CritiInfoCell* cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            // cell = [[CritiInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
            
            NSArray *topLevelObject = [[NSBundle mainBundle] loadNibNamed:@"CritiInfoCell" owner:nil options:nil];
            NSLog(@"%@",topLevelObject);
            cell = [topLevelObject objectAtIndex:0];
        }
        //cell.img_user_img.image=[]
        
        //cell.img_user_img.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",[[resDict valueForKey:@"ImageURL"] objectAtIndex:indexPath.row]]];
        NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
        NSString *postedID=[_mutDict  valueForKey:@"PostedBy"];
        NSString *selfUser=[[resDict valueForKey:@"UserID"] objectAtIndex:indexPath.row];
        
        
        if ([[standardUserDefaults valueForKey:@"UserID"]isEqualToString:[NSString stringWithFormat:@"%@",postedID]]) {
            
            if ([[standardUserDefaults valueForKey:@"UserID"]isEqualToString:[NSString stringWithFormat:@"%@",selfUser]]) {
                cell.btn_replay.hidden=YES;
            }
            else
            {
                cell.btn_replay.hidden=NO;
            }
            
        }
        else
        {
            cell.btn_replay.hidden=YES;
        }
        
        
        
        
        [cell.img_user_img sd_setImageWithURL:[NSURL URLWithString:[resDict valueForKey:@"ImageURL"][indexPath.row]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
        
        
        cell.txt_user_nm.text=[NSString stringWithFormat:@"%@ %@",[[resDict  valueForKey:@"FName"] objectAtIndex:indexPath.row],[[resDict  valueForKey:@"LName"] objectAtIndex:indexPath.row]];
        cell.txt_user_comment.text=[NSString stringWithFormat:@"%@",[[resDict  valueForKey:@"Comments"] objectAtIndex:indexPath.row]];
        
        //=================DATE FORMATE==========
        /*  NSString * enddtstr = [resDict valueForKey:@"CommentedDate"][indexPath.row];
         NSDateFormatter *currentDTFormatter1 = [[NSDateFormatter alloc] init];;
         [currentDTFormatter1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
         NSDate *dateFromString1 = [currentDTFormatter1 dateFromString:enddtstr];
         [currentDTFormatter1 setDateFormat:@"MMM"];
         NSString *eventDateStr1 = [currentDTFormatter1 stringFromDate:dateFromString1];
         [currentDTFormatter1 setDateFormat:@"dd"];
         NSString *onlydt1 = [currentDTFormatter1 stringFromDate:dateFromString1];
         [currentDTFormatter1 setDateFormat:@"MMMM"];
         NSString *onlymon1 = [currentDTFormatter1 stringFromDate:dateFromString1];
         [currentDTFormatter1 setDateFormat:@"yyyy"];
         NSString *onlyyear = [currentDTFormatter1 stringFromDate:dateFromString1];
         
         cell.lbl_comment_dt.text=[NSString stringWithFormat:@"%@ %@ %@",eventDateStr1,onlydt1,onlyyear];
         */
        //=============DATE CONVERT=================
        NSString * yourJSONString = [resDict valueForKey:@"CommentedDate"][indexPath.row];
        
        NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
        [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *dateFromString = [currentDTFormatter dateFromString:yourJSONString];
        [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *eventDateStr = [currentDTFormatter stringFromDate:dateFromString];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSDate *currentDate = [NSDate date];
        NSString *start = [formatter stringFromDate:dateFromString];
        NSString *end = [formatter stringFromDate:currentDate];
        //NSString *end = @"2018-01-11";
        
        NSDateFormatter *f = [[NSDateFormatter alloc] init];
        [f setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *startDate = [f dateFromString:start];
        NSDate *endDate = [f dateFromString:end];
        
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [gregorianCalendar components:(NSCalendarUnitDay |NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitWeekOfYear)
                                                            fromDate:startDate
                                                              toDate:endDate
                                                             options:0];
        
        NSLog(@"Day %ld", [components day]);
        NSLog(@"Hour %ld", [components hour]);
        NSLog(@"Minute %ld", [components minute]);
        
        
        
        if ([components weekOfYear]>0) {
            cell.lbl_comment_dt.text=[NSString stringWithFormat:@"%ld week ago",(long)[components weekOfYear]];
        }
        else if ([components hour]>0) {
            cell.lbl_comment_dt.text=[NSString stringWithFormat:@"%ld hours ago",(long)[components hour]];
        }
        else if ([components minute]>0)
        {
            cell.lbl_comment_dt.text=[NSString stringWithFormat:@"%ld min ago",(long)[components minute]];
        }
        else{
            cell.lbl_comment_dt.text=[NSString stringWithFormat:@"%ld day ago",(long)[components day]];
        }
        
        
        //===========================================
        
        
        
        
        
        //////////AUTO SPACING
        
        CGRect autosize4 = cell.txt_user_comment.frame;
        autosize4.size.height = [self heightForText:[NSString stringWithFormat:@"%@",[resDict valueForKey:@"Comments"][indexPath.row]]];
        cell.txt_user_comment.frame = autosize4;
        
        [cell.txt_user_comment setNumberOfLines:0];
        [cell.txt_user_comment sizeToFit];
        
        
        /*
         CGRect autosize5 = cell.ve_cellview.frame;
         autosize5.size.height = cell.lbl_comment_dt.frame.origin.y + cell.lbl_comment_dt.frame.size.height;
         cell.ve_cellview.frame = autosize5;
         */
        
        
        /*  CGRect autosize2 = cell.txt_user_nm.frame;
         autosize2.origin.y =cell.txt_user_comment.frame.origin.y+cell.txt_user_comment.frame.size.height;
         cell.txt_user_nm.frame = autosize2;
         
         CGRect autosize3 = cell.lbl_comment_dt.frame;
         autosize3.origin.y =cell.txt_user_nm.frame.origin.y+cell.txt_user_nm.frame.size.height;
         cell.lbl_comment_dt.frame = autosize3;
         */
        
        [cell.btn_replay setTag:indexPath.row];
        [cell.btn_replay addTarget:self action:@selector(checkBoxClicked:)forControlEvents:UIControlEventTouchUpInside];
        
        
        /*
         if ([[[resDict  valueForKey:@"Type"] objectAtIndex:indexPath.row] isEqualToString:@"Reply"]) {
         return cell;
         }
         */
        
        return cell;
    }
    else if (indexPath.section == 1)
    {
        /* CritiInfoCell *cell = (CritiInfoCell *)[_tbl_criritab dequeueReusableCellWithIdentifier:@"Cell2"];
         
         if (cell == nil)
         {
         NSArray *topLevelObject = [[NSBundle mainBundle] loadNibNamed:@"CritiInfoCell" owner:nil options:nil];
         NSLog(@"%@",topLevelObject);
         cell = [topLevelObject objectAtIndex:1];
         NSLog(@"%@",cell);
         }
         */
        static NSString *simpleTableIdentifier = @"Cell2";
        CritiInfoCell* cell1 = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell1 == nil) {
            // cell = [[CritiInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
            
            NSArray *topLevelObject = [[NSBundle mainBundle] loadNibNamed:@"CritiInfoCell" owner:nil options:nil];
            NSLog(@"%@",topLevelObject);
            cell1 = [topLevelObject objectAtIndex:1];
        }
        
        NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
        
        
        
        
        
        [cell1.img_prof sd_setImageWithURL:[NSURL URLWithString:[standardUserDefaults valueForKey:@"ImageURL"]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
        
        cell1.btn_send.tag=indexPath.row;
        NSString *strmsg=[NSString stringWithFormat:@"%@",cell1.txt_comme.text];
        [cell1.btn_send addTarget:self action:@selector(CallSend:) forControlEvents:UIControlEventTouchUpInside];
        
        //  cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell1;
    }
    
    return nil;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.selected) {
        // ... Uncheck
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        if ([[[resDict  valueForKey:@"Type"] objectAtIndex:indexPath.row] isEqualToString:@"Reply"]) {
            
            return 0;
            
        }
        else
        {
            NSString * text = [NSString stringWithFormat:@"%@",[[resDict  valueForKey:@"Comments"] objectAtIndex:indexPath.row]];
            
            NSInteger  height1 =  [self heightForText:text];
            
            
            if ([[NSString stringWithFormat:@"%ld",(long)height1] isEqualToString:@"24"])
            {
                return 73;
            }
            else
            {
                return 40 + [self heightForText:text];
            }
        }
    }
    else
    {
        return 50;
    }
    
    
    
    
    
}
- (void)checkBoxClicked:(id)sender
{
    UIButton *btn=sender;
    
    NSLog(@"button was clicked for the following row %li", (long)btn.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[btn tag]  inSection:0];
    NSLog(@"indexpath.row %ld", (long)indexPath.row);
    NSLog(@"indexpath.row %ld", (long)indexPath.section);
    
    ReplayVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"ReplayVC"];
    
    next.Dict1=resDict;
    next.Dict2=_mutDict;//full object that display here
    next.critiID=[[resDict valueForKey:@"CritiqueReplyID"] objectAtIndex:indexPath.row];
    next.replyID=[[resDict valueForKey:@"UserID"] objectAtIndex:indexPath.row];
    
    
    [self.navigationController pushViewController:next animated:NO];
    
    
    
    /*
     ReplayCritiques *homescreen = [self.storyboard instantiateViewControllerWithIdentifier:@"ReplayCritiques"];
     homescreen.dict=[resDict objectAtIndex:indexPath.row];
     homescreen._mutDict=_mutDict;
     
     homescreen.view.frame = self.view.bounds;
     [homescreen.view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5f]];
     [self.view addSubview:homescreen.view];
     [self addChildViewController:homescreen];
     [homescreen didMoveToParentViewController:self];
     */
    
    
    /* MessagePopVC *homescreen = [self.storyboard instantiateViewControllerWithIdentifier:@"MessagePopVC"];
     homescreen.Uid=[_mutDict valueForKey:@"UserID"];
     homescreen.view.frame = self.view.bounds;
     [homescreen.view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5f]];
     [self.view addSubview:homescreen.view];
     [self addChildViewController:homescreen];
     [homescreen didMoveToParentViewController:self];
     */
    
    
    
    
}



- (IBAction)btn_HOME:(id)sender {
    
    NewHomeVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:YES];
    
}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}


- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_NOTIFY:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}
@end
