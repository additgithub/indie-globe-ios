//
//  PDVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/2/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "PDVC.h"
#import "DeshboardVC.h"
#import "SWRevealViewController.h"
#import "UIColor+CL.h"
#import "InviteTeamVC.h"
#import "Constants.h"
#import "PDCell.h"
#import "NewHomeVC.h"
#import "MTSelectMemberVC.h"
#import "AllNotificationVC.h"
#import "AFTPagingBaseViewController.h"


@interface PDVC ()

@end

@implementation PDVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    [self customSetup];
    // Do any additional setup after loading the view.
    _btn_add_memb.layer.cornerRadius=_btn_add_memb.frame.size.height/2;
    
    _tbl_invite_list.tableHeaderView = _vw_header;
    
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];

    _lbl_title.text=[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"ProjectTitle"]];
    _lbl_desc.text=[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"ShortDesc"]];
    [_img_proj_image sd_setImageWithURL:[NSURL URLWithString:[_mutDict valueForKey:@"ProjectImage"]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
    
    _img_user.clipsToBounds=YES;
    _img_user.layer.cornerRadius=_img_user.frame.size.height/2;
    [_img_user sd_setImageWithURL:[NSURL URLWithString:[_mutDict valueForKey:@"ImageURL"]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
    _lbl_usernm.text=[NSString stringWithFormat:@"%@ %@",[_mutDict valueForKey:@"FName"],[_mutDict valueForKey:@"LName"]];
    
    
    CGRect mainframe = _lbl_desc.frame;
    mainframe.size.height =  [self heightForText:[_mutDict valueForKey:@"ShortDesc"]];
    _lbl_desc.frame = mainframe;
    
    CGRect mainframe1 = _vw_header.frame;
    mainframe1.size.height = [self heightForText:[_mutDict valueForKey:@"ShortDesc"]]+ _btn_add_memb.frame.size.height + _lbl_title.frame.size.height+_img_proj_image.frame.size.height+30;
    _vw_header.frame = mainframe1;
    
    CGRect buttonframe = _btn_add_memb.frame;
    buttonframe.origin.y = _lbl_desc.frame.origin.y+_lbl_desc.frame.size.height;
    _btn_add_memb.frame = buttonframe;
    
    CGRect buttonframeq = _btn_notifyuser.frame;
    buttonframeq.origin.y = _lbl_desc.frame.origin.y+_lbl_desc.frame.size.height;
    _btn_notifyuser.frame = buttonframeq;
    
    CGRect buttonframe1 = _lblborder.frame;
    buttonframe1.origin.y = _btn_add_memb.frame.origin.y+_btn_add_memb.frame.size.height+11;
    _lblborder.frame = buttonframe1;
    
    CGRect imageframe = _joinimage.frame;
    imageframe.origin.y = _btn_add_memb.frame.origin.y+7;
    _joinimage.frame = imageframe;

    
    _img_proj_image.userInteractionEnabled=YES;
    
    UITapGestureRecognizer *letterTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(highlightLetter:)];
    letterTapRecognizer.numberOfTapsRequired = 1;
    [_img_proj_image addGestureRecognizer:letterTapRecognizer];
    
    [_img_proj_image setContentMode:UIViewContentModeScaleAspectFill];
    [_img_proj_image setClipsToBounds:YES];
    
    _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
    _lbl_notifi_count.clipsToBounds=YES;
    _lbl_notifi_count.text=theAppDelegate.n_Count;
    
    if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
        _lbl_notifi_count.hidden=YES;
    }
    else
    {
        _lbl_notifi_count.hidden=NO;
    }
}

-(void)highlightLetter:(UITapGestureRecognizer*)sender
{
    NSLog(@"click on image");
    
    [APP_DELEGATE showLoadingView:@""];
    
    NSURL *url = [NSURL URLWithString:[_mutDict valueForKey:@"ProjectImage"]];
    NSData *data = [NSData dataWithContentsOfURL:url];
    UIImage* immg = [[UIImage alloc]initWithData:data];
    NSArray *landscapeImages = [[NSArray alloc]initWithObjects:immg, nil];
    
    Class cls = NSClassFromString(@"AFTNormalPagingViewController");
    AFTPagingBaseViewController *vc1 = [cls new];
    AFTPagingBaseViewController *bvc = (AFTPagingBaseViewController *)vc1;
    
    bvc.title = @"Photo";
    bvc.images = landscapeImages;
    [APP_DELEGATE hideLoadingView];
    [self presentViewController:bvc animated:YES completion:nil];
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    page_no=@"0";
    [self CallInvitedList];
    [self customSetup];
}
-(void)CallInvitedList
{
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",page_no] forKey:@"page_no"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",[_mutDict valueForKey:@"ProjectID"]] forKey:@"project_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    [manager POST:[NSString stringWithFormat:@"%@get_invited_joined_people_list",JsonUrlConstant] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        page_no=[responseObject valueForKey:@"page_no"];
        inviteList=[responseObject valueForKey:@"data"];
        [_tbl_invite_list reloadData];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];

}


- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    if (IsIphone7 || IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = 145;
    }
    else{
        self.revealViewController.rearViewRevealWidth = 145;
    }
}
-(CGFloat)heightForText:(NSString *)text
{
    NSInteger MAX_HEIGHT = 2000;
    UITextView * textView = [[UITextView alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width-80, MAX_HEIGHT+150)];
    textView.text = text;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        textView.font = [UIFont fontWithName:@"Roboto-Light" size:13.0];
    }
    else
    {
        textView.font = [UIFont fontWithName:@"Roboto-Light" size:13.0];
    }
    
    [textView sizeToFit];
    
    return textView.frame.size.height;
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return inviteList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Cell";
    tableView.separatorColor = [UIColor clearColor];
    PDCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[PDCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    [cell.img_profile sd_setImageWithURL:[NSURL URLWithString:[inviteList valueForKey:@"ImageURL"][indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
    cell.lbl_usernm.text=[NSString stringWithFormat:@"%@",[[inviteList valueForKey:@"FName"] objectAtIndex:indexPath.row]];
    //cell.lbl_status.text=[NSString stringWithFormat:@"Invited"];
    cell.lbl_status.text=[NSString stringWithFormat:@"%@",[[inviteList valueForKey:@"MemberType"] objectAtIndex:indexPath.row]];
    
    NSArray *arrT=[[inviteList valueForKey:@"ProfessionData"] objectAtIndex:indexPath.row];
    
    cell.lbl_prof.text=[arrT componentsJoinedByString:@","];
    NSMutableArray *NewArr = [[NSMutableArray alloc] init];
    for (int i=0; i<arrT.count; i++) {
        cell.lbl_prof.text=[arrT componentsJoinedByString:@","];
        [NewArr addObject:[[arrT objectAtIndex:i] valueForKey:@"Profession"]];
    }
    cell.lbl_prof.text=[NewArr componentsJoinedByString:@","];
    
    //DATE CONVERT
    //------TODATE--------------
    NSString * yourJSONString = [inviteList valueForKey:@"CreatedOn"][indexPath.row];
    NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
    [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateFromString = [currentDTFormatter dateFromString:yourJSONString];
    [currentDTFormatter setDateFormat:@"MMM"];
    NSString *eventDateStr = [currentDTFormatter stringFromDate:dateFromString];
    [currentDTFormatter setDateFormat:@"dd"];
    NSString *onlydt = [currentDTFormatter stringFromDate:dateFromString];
    [currentDTFormatter setDateFormat:@"yyyy"];
    NSString *onlyyear = [currentDTFormatter stringFromDate:dateFromString];
    
    cell.lbl_datetime.text=[NSString stringWithFormat:@"%@ %@ %@",eventDateStr,onlydt,onlyyear];


    
    //cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.selected) {
        // ... Uncheck
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    
    ProfileVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileVC"];
    next.mutDict=[inviteList objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:next animated:YES];
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_HOME:(id)sender {
    NewHomeVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:YES];

}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}


- (IBAction)btn_ADDMEMB:(id)sender {
    InviteTeamVC* next = [self.storyboard instantiateViewControllerWithIdentifier:@"InviteTeamVC"];
    
    next.projID=[_mutDict valueForKey:@"ProjectID"];
    [self.navigationController pushViewController:next animated:YES];

    
}

- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_NOTIFI:(id)sender {
    MTSelectMemberVC* next = [self.storyboard instantiateViewControllerWithIdentifier:@"MTSelectMemberVC"];
    
    next.Dict=inviteList;
    
    
    //next.projID=[_mutDict valueForKey:@"ProjectID"];
    [self.navigationController pushViewController:next animated:YES];
    
}

- (IBAction)btn_NOTIFY:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}
@end
