//
//  MyFundingVC.m
//  IndieGlobe
//
//  Created by mac on 14/08/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "MyFundingVC.h"

#import "MyFundingCell.h"
#import "HttpWrapper.h"
#import "ApplicationConstant.h"
#import "NewHomeSearchVC.h"
#import "HttpWrapper.h"
#import "ApplicationConstant.h"
#import "NewHomeSearchVC.h"
#import "UIImageView+WebCache.h"
#import "objc/runtime.h"
#import "UIView+WebCacheOperation.h"
#import "AddfundVC.h"

#import "NewHomeVC.h"
#import "AllNotificationVC.h"

#import "Constants.h"
#import "AFTPagingBaseViewController.h"



@interface MyFundingVC ()

@end

@implementation MyFundingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden=YES;
    
    [self customSetup];
    
    
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];
    
    
    ///////////
    
    _btn_addfundin.layer.cornerRadius=_btn_addfundin.frame.size.height/2;
    
    
    _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
    _lbl_notifi_count.clipsToBounds=YES;
    _lbl_notifi_count.text=theAppDelegate.n_Count;
    
    if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
        _lbl_notifi_count.hidden=YES;
    }
    else
    {
        _lbl_notifi_count.hidden=NO;
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self CallMyFunding];
}

-(void)CallMyFunding
{
    
    [APP_DELEGATE showLoadingView:@""];
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"PARAM---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@get_my_funding_list",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        arrMyFunding=[[NSMutableArray alloc]init];
        
        
        arrMyFunding=[responseObject valueForKey:@"data"];
       
        [APP_DELEGATE hideLoadingView];
        
        
        [_tbl_myfunding reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
        
    }];
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if (IsIphone7 || IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = 145;
    }
    else{
        self.revealViewController.rearViewRevealWidth = 145;
    }
}
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrMyFunding.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Cell";
    //tableView.separatorColor = [UIColor clearColor];
    
    
    MyFundingCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[MyFundingCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    NSLog(@"%ld,%ld",(long)indexPath.row,(long)indexPath.section);
    
    cell.lbl_desc.text= [NSString stringWithFormat:@"%@",[[arrMyFunding objectAtIndex:indexPath.row]valueForKey:@"ShortDesc"]];
    
    cell.lbl_title.text=[NSString stringWithFormat:@"%@",[[arrMyFunding objectAtIndex:indexPath.row] valueForKey:@"Title"]];
    
    cell.lbl_goal.text=[NSString stringWithFormat:@"GOAL:%@",[[arrMyFunding objectAtIndex:indexPath.row]valueForKey:@"GoalAmount"]];
    
    
    [cell.img_funding sd_setImageWithURL:[NSURL URLWithString:[arrMyFunding valueForKey:@"FRImage"][indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
    
    [cell.img_funding setContentMode:UIViewContentModeScaleAspectFill];
    [cell.img_funding setClipsToBounds:YES];
    
    
    [cell.btn_edit setTag:indexPath.row];
    [cell.btn_edit addTarget:self action:@selector(checkBoxClicked:)forControlEvents:UIControlEventTouchUpInside];
    
    [cell.btn_delete setTag:indexPath.row];
    [cell.btn_delete addTarget:self action:@selector(checkDelete:)forControlEvents:UIControlEventTouchUpInside];
    
    
    [cell.btn_donate setTag:indexPath.row];
    [cell.btn_donate addTarget:self action:@selector(checkDonet:)forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.selected) {
        // ... Uncheck
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    
    
    NSURL *url = [NSURL URLWithString:[arrMyFunding valueForKey:@"FRImage"][indexPath.row]];
    NSData *data = [NSData dataWithContentsOfURL:url];
    UIImage* immg = [[UIImage alloc]initWithData:data];
    NSArray *landscapeImages = [[NSArray alloc]initWithObjects:immg, nil];
    
    Class cls = NSClassFromString(@"AFTNormalPagingViewController");
    AFTPagingBaseViewController *vc1 = [cls new];
    AFTPagingBaseViewController *bvc = (AFTPagingBaseViewController *)vc1;
    
    bvc.title = @"Photo";
    bvc.images = landscapeImages;
    [APP_DELEGATE hideLoadingView];
    [self presentViewController:bvc animated:YES completion:nil];
    
}


- (void)checkBoxClicked:(id)sender
{
    UIButton *btn=sender;
    
    NSLog(@"button was clicked for the following row %li", (long)btn.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[btn tag]  inSection:0];
    NSLog(@"indexpath.row %ld", (long)indexPath.row);
    NSLog(@"indexpath.row %ld", (long)indexPath.section);
    
    
    AddfundVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"AddfundVC"];
    next.arrDetails=[arrMyFunding objectAtIndex:indexPath.row];
    next.isFrom=@"MyFundingVC";
    [self.navigationController pushViewController:next animated:YES];
  
    
}
- (void)checkDonet:(id)sender
{
    UIButton *btn=sender;
    
    NSLog(@"button was clicked for the following row %li", (long)btn.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[btn tag]  inSection:0];
    NSLog(@"indexpath.row %ld", (long)indexPath.row);
    NSLog(@"indexpath.row %ld", (long)indexPath.section);
    
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.gofundme.com/" ]];
    
}
- (void)checkDelete:(id)sender
{
    UIButton *btn=sender;
    
    NSLog(@"button was clicked for the following row %li", (long)btn.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[btn tag]  inSection:0];
    NSLog(@"indexpath.row %ld", (long)indexPath.row);
    NSLog(@"indexpath.row %ld", (long)indexPath.section);
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",[[arrMyFunding objectAtIndex:indexPath.row] valueForKey:@"FundRaisingID"]] forKey:@"fund_raising_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"PARAM---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@delete_funding",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
       
        [self CallMyFunding];
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
        
    }];
}



- (IBAction)btn_HOME:(id)sender {
    NewHomeVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:NO];
}

- (IBAction)btn_NOTIF:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}

- (IBAction)btn_ADD_FUNDING:(id)sender {
    
    AddfundVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AddfundVC"];
    [self.navigationController pushViewController:next animated:YES];
    
}
@end
