//
//  ProfilePhotoForHome.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 9/13/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "ProfilePhotoForHome.h"
#import "UIImageView+WebCache.h"

@interface ProfilePhotoForHome ()

@end

@implementation ProfilePhotoForHome

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    
    if ([_isFrom isEqualToString:@"NewProfileVC"]) {
        [_img_view sd_setImageWithURL:[NSString stringWithFormat:@"%@",[_arrimgs valueForKey:@"ImageURL"]]  placeholderImage:[UIImage imageNamed:@"Avatar"]];
    }
    else
    {
        [_img_view sd_setImageWithURL:[NSString stringWithFormat:@"%@",[_arrimgs valueForKey:@"Image"]]  placeholderImage:[UIImage imageNamed:@"Avatar"]];
    }
    
    //lbl_title.text=[NSString stringWithFormat:@"%@",[_arrimgs valueForKey:@"PortfolioName"]];
    //lbl_desc.text=[NSString stringWithFormat:@"%@",[_arrimgs valueForKey:@"Description"]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
