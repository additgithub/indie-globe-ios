//
//  HindiCell.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 3/24/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "HindiCell.h"

@implementation HindiCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.vw_shadow setShadow];
    
    _img_vw1.layer.cornerRadius=_img_vw1.layer.frame.size.height/2;
    _img_vw1.clipsToBounds=YES;
    
    _img_vw2.layer.cornerRadius=_img_vw2.layer.frame.size.height/2;
    _img_vw2.clipsToBounds=YES;
    
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
