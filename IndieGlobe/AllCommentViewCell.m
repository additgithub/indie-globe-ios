//
//  AllCommentViewCell.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 10/22/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "AllCommentViewCell.h"

@implementation AllCommentViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _img_prof.layer.cornerRadius=_img_prof.layer.frame.size.height/2;
    _img_prof.clipsToBounds=YES;
    
    _img_prof2.layer.cornerRadius=_img_prof2.layer.frame.size.height/2;
    _img_prof2.clipsToBounds=YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
