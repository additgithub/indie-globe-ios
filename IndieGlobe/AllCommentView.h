//
//  AllCommentView.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 10/22/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllCommentView : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    
    NSMutableArray *arrCommList;
    
}

@property (retain , nonatomic)NSString *user_portfolio_category_id;
@property ( retain , nonatomic )NSString *category_nm;

@property (weak, nonatomic) IBOutlet UILabel *lbl_header_title;

@property (weak, nonatomic) IBOutlet UITableView *tbl_comment;


- (IBAction)btn_BACK:(id)sender;



@end
