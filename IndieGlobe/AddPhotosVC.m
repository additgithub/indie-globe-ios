//
//  AddPhotosVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/25/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "AddPhotosVC.h"
#import "UIColor+CL.h"
#import "NewsVC.h"
#import "EventVC.h"
#import <MediaPlayer/MediaPlayer.h>
#import "NewHomeVC.h"

#define REGEX_EMAIL @"[A-Za-z://]{3,}+\\.[A-Za-z0-9]+\\.[A-Za-z]{2,8}"

@interface AddPhotosVC ()

@end

@implementation AddPhotosVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self CustomizeTextField:_txt_title];
    _txt_title.placeholder=@"Title";
    
    _txtlocation.placeholder = @"Location";
    _txtequipment.placeholder = @"Equipment";
    
    
    
    _lbl_add_titel.text=_cat_name;
    
    [self CustomizeTextField:_txt_urllink];
    _txt_urllink.placeholder=@"Paste your link eg:http://www.youtube.com/";
    
    _btn_post.layer.cornerRadius=_btn_post.frame.size.height/2;
    
    //Choose image
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
    [singleTap setNumberOfTapsRequired:1];
    [_img_chooseimg addGestureRecognizer:singleTap];
    
    NSLog(@"cat Id %@",_cat_id);
    
    _txt_desc.delegate=self;
    _txt_desc.text = @"Description";
    _txt_desc.textColor = [UIColor lightGrayColor];
    _txt_desc.layer.borderWidth=1.0;
    _txt_desc.layer.cornerRadius=6;
    _txt_desc.layer.borderColor=[UIColor colorWithHex:0xDEDEDE].CGColor;
    _txt_desc.textContainerInset = UIEdgeInsetsMake(5, 7, 0, 0);
    
}


-(void)viewWillAppear:(BOOL)animated
{
    
    if ([_cat_name isEqualToString:@"Photos"]) {
        _txt_urllink.hidden=YES;

        CGRect buttonframe1 = _btn_post.frame;
        buttonframe1.origin.y =_txt_urllink.frame.origin.y;
        _btn_post.frame = buttonframe1;
    }
    else if ([_cat_name isEqualToString:@"Feature Films"])
    {
        _txt_urllink.hidden=NO;
        
        _img_chooseimg.hidden=YES;
        _img_extra.hidden=YES;
        _lbl_extra.hidden=YES;
        
        CGRect buttonframe1 = _vw_details.frame;
        buttonframe1.origin.y =125;
        _vw_details.frame = buttonframe1;
    }
    else if ([_cat_name isEqualToString:@"Short Films"])
    {
        _txt_urllink.hidden=NO;
        
        _img_chooseimg.hidden=YES;
        _img_extra.hidden=YES;
        _lbl_extra.hidden=YES;
        
        CGRect buttonframe1 = _vw_details.frame;
        buttonframe1.origin.y =125;
        _vw_details.frame = buttonframe1;
    }
    else if ([_cat_name isEqualToString:@"Reel"])
    {
        _txt_urllink.hidden=NO;
        
        _img_chooseimg.hidden=YES;
        _img_extra.hidden=YES;
        _lbl_extra.hidden=YES;
        
        CGRect buttonframe1 = _vw_details.frame;
        buttonframe1.origin.y =125;
        _vw_details.frame = buttonframe1;
    }
    else if ([_cat_name isEqualToString:@"BTS"])
    {
        
        _seg_bts.hidden=NO;
        
        _txt_urllink.hidden=YES;
        //_img_chooseimg.hidden=NO;
       // _img_extra.hidden=NO;
        //_lbl_extra.hidden=NO;

        CGRect buttonframe1 = _btn_post.frame;
        buttonframe1.origin.y =_txt_urllink.frame.origin.y;
        _btn_post.frame = buttonframe1;
        
        CGRect buttonframe2 = _lbl_add_titel.frame;
        buttonframe2.origin.y =63;
        _lbl_add_titel.frame = buttonframe2;
        
        
    }
    else if ([_cat_name isEqualToString:@"Videos"])
    {
        _txt_urllink.hidden=NO;
        
        _img_chooseimg.hidden=YES;
        _img_extra.hidden=YES;
        _lbl_extra.hidden=YES;
        
        CGRect buttonframe1 = _vw_details.frame;
        buttonframe1.origin.y =125;
        _vw_details.frame = buttonframe1;
    }
    else if ([_cat_name isEqualToString:@"Head Shots"])
    {
        _txt_urllink.hidden=YES;
        
        CGRect buttonframe1 = _btn_post.frame;
        buttonframe1.origin.y =_txt_urllink.frame.origin.y;
        _btn_post.frame = buttonframe1;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if([textView.text isEqual: @"Description"])
    {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    return YES;
}



-(void)singleTapping:(UIGestureRecognizer *)recognizer {
    
    NSLog(@"image clicked");
    ci= [[UIImagePickerController alloc] init];
    ci.delegate = self;
    ci.allowsEditing=NO;
    ci.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
        [self presentViewController:ci animated:YES completion:nil];
    else
    {
        popover=[[UIPopoverController alloc]initWithContentViewController:ci];
        [popover presentPopoverFromRect:_img_chooseimg.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}

#pragma mark - ImagePickerController Delegate
-(void)imagePickerController:(UIImagePickerController *)pick didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        ///[pick dismissViewControllerAnimated:YES completion:nil];
    } else {
        [popover dismissPopoverAnimated:YES];
    }
    ISimage=[info objectForKey:UIImagePickerControllerOriginalImage];
    _img_chooseimg.image = [info objectForKey:UIImagePickerControllerEditedImage];
    
    
    if ([_cat_name isEqualToString:@"Photos"] || [_cat_name isEqualToString:@"Head Shots"]) {
       /* _imgCropperViewController = [[UzysImageCropperViewController alloc] initWithImage:ISimage andframeSize:ci.view.frame.size andcropSize:CGSizeMake(400, 240)];
        _imgCropperViewController.delegate = self;
        [ci presentViewController:_imgCropperViewController animated:YES completion:nil];
        [_imgCropperViewController release];
        */
        _img_extra.hidden=YES;
        _lbl_extra.hidden=YES;
        ISimage=ISimage;
        _img_chooseimg.image=ISimage;
        
    }
    else if ([_cat_name isEqualToString:@"BTS"])
    {
       /* _imgCropperViewController = [[UzysImageCropperViewController alloc] initWithImage:ISimage andframeSize:ci.view.frame.size andcropSize:CGSizeMake(400, 240)];
        _imgCropperViewController.delegate = self;
        [ci presentViewController:_imgCropperViewController animated:YES completion:nil];
        [_imgCropperViewController release];
        */
        _img_extra.hidden=YES;
        _lbl_extra.hidden=YES;
        ISimage=ISimage;
        _img_chooseimg.image=ISimage;
        
    }
    
     [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)imageCropper:(UzysImageCropperViewController *)cropper didFinishCroppingWithImage:(UIImage *)image
{
    _img_extra.hidden=YES;
    _lbl_extra.hidden=YES;
    
    if ([_cat_name isEqualToString:@"Photos"] || [_cat_name isEqualToString:@"Head Shots"]) {
        ISimage=image;
        _img_chooseimg.image=image;
    }
    else if ([_cat_name isEqualToString:@"BTS"])
    {
        ISimage=image;
        _img_chooseimg.image=image;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imageCropperDidCancel:(UzysImageCropperViewController *)cropper
{
    [self dismissViewControllerAnimated:YES completion:nil];
    // [_picker dismissViewControllerAnimated:YES completion:nil];
}



-(void)imagePickerControllerDidCancel:(UIImagePickerController *)pick
{
    [pick dismissViewControllerAnimated:YES completion:nil];
}


-(void)CustomizeTextField:(UITextField *)myTextField
{
    myTextField.layer.borderWidth=1.0;
    myTextField.layer.borderColor=[UIColor colorWithHex:0xADADAD].CGColor;
    CGRect frameRect = myTextField.frame;
    frameRect.size.height = 28;
    myTextField.frame = frameRect;
    myTextField.layer.cornerRadius=myTextField.frame.size.height/2;
    
    [myTextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
    myTextField.leftView = paddingView;
    myTextField.leftViewMode = UITextFieldViewModeAlways;
    
}

-(void)CallAddPortfolio
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:@"1" forKey:@"is_image"];
    [AddPost setValue:_cat_id forKey:@"portfolio_category_id"];
    [AddPost setValue:_txt_title.text forKey:@"portfolio_name"];
    [AddPost setValue:_txt_desc.text forKey:@"description"];
    [AddPost setValue:_txtequipment.text forKey:@"equipment"];
    [AddPost setValue:_txtlocation.text forKey:@"location"];
    [AddPost setValue:@"" forKey:@"portfolio_url"];
    
    //Create manager
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    
    //Now post
    NSLog(@"AddPost %@",AddPost);
    NSString *url = [NSString stringWithFormat:@"%@add_portfolio",JsonUrlConstant];
    [manager POST:url parameters:[AddPost copy] constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        //add img data one by one
        
        NSData *imageData = UIImageJPEGRepresentation(ISimage,0.5);
        [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"portfolio_image"] fileName:[NSString stringWithFormat:@"user_image.jpg"] mimeType:@"image/jpeg"];
        
    } success:^(AFHTTPRequestOperation *operation, id dicsResponse) {
        NSLog(@"Success: %@", dicsResponse);
        NSLog(@"Edit profile---%@",dicsResponse);
        NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
        
        [APP_DELEGATE hideLoadingView];
        
        if ([[dicsResponse valueForKey:@"status"] integerValue] == 0) {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Add Data not match" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //[alert show];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Add Successfull" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //[alert show];
            [self.navigationController popViewControllerAnimated:YES];
        }
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Success" message:@"Added" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
        
        [self.navigationController popViewControllerAnimated:YES];
        
        [APP_DELEGATE hideLoadingView];
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
              [APP_DELEGATE hideLoadingView];
          }];
    
}



- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_HOME:(id)sender {
    
    NewHomeVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:YES];
    
}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}

-(void)CallAddURLS
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:@"0" forKey:@"is_image"];
    [AddPost setValue:_cat_id forKey:@"portfolio_category_id"];
    [AddPost setValue:_txt_title.text forKey:@"portfolio_name"];
    [AddPost setValue:_txt_desc.text forKey:@"description"];
    [AddPost setValue:_txt_urllink.text forKey:@"portfolio_url"];
    [AddPost setValue:_txtequipment.text forKey:@"equipment"];
    [AddPost setValue:_txtlocation.text forKey:@"location"];
    //------------
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@add_portfolio",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSLog(@"---%@",responseObject);
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Success" message:@"Added" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            
           
            [self.navigationController popViewControllerAnimated:YES];
            
        }
        [APP_DELEGATE hideLoadingView];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
    
    
}

- (IBAction)btn_POST_ACTION:(id)sender {
    
    if ([_cat_name isEqualToString:@"Photos"] || [_cat_name isEqualToString:@"Head Shots"] ) {
        
        if (ISimage>0) {
            if ([self CheckValidate:_txt_title.text :@"Please Enter valid title"]) {
                if ([self CheckValidate:_txt_title.text :@"Please Enter Description"] && ![_txt_desc.text isEqualToString:@"Description"]) {
                    
                    if ([self CheckValidate:_txtlocation.text :@"Please Enter Location"] && ![_txtequipment.text isEqualToString:@"Please Enter Equipment"]) {
                        [self CallAddPortfolio];
                    }
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Enter Description" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                    [alert show];
                }
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Select Image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
    }
    else if ([_cat_name isEqualToString:@"Reel"])
    {
        if ([self CheckValidate:_txt_title.text :@"Please Enter valid title"]) {
            if ([self CheckValidate:_txt_title.text :@"Please Enter Description"] && ![_txt_desc.text isEqualToString:@"Description"]) {
                if ([self CheckValidate:_txt_urllink.text :@"Please Enter Video Link"]) {
                    if ([self CheckValidate:_txtlocation.text :@"Please Enter Location"] && ![_txtequipment.text isEqualToString:@"Please Enter Equipment"]) {
                        [self CallAddURLS];
                    }
                }
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Enter Description" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
            }
        }
    }
    else if ([_cat_name isEqualToString:@"Feature Films"])
    {
        if ([self CheckValidate:_txt_title.text :@"Please Enter valid title"]) {
            if ([self CheckValidate:_txt_title.text :@"Please Enter Description"] && ![_txt_desc.text isEqualToString:@"Description"]) {
                if ([self CheckValidate:_txt_urllink.text :@"Please Enter Video Link"]) {
                  
                    if ([self CheckValidate:_txtlocation.text :@"Please Enter Location"] && ![_txtequipment.text isEqualToString:@"Please Enter Equipment"]) {
                        [self CallAddURLS];
                    }
                }
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Enter Description" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
            }
        }
    }
    else if ([_cat_name isEqualToString:@"Short Films"])
    {
        if ([self CheckValidate:_txt_title.text :@"Please Enter valid title"]) {
            if ([self CheckValidate:_txt_title.text :@"Please Enter Description"] && ![_txt_desc.text isEqualToString:@"Description"]) {
                if ([self CheckValidate:_txt_urllink.text :@"Please Enter Video Link"]) {
                   
                    if ([self CheckValidate:_txtlocation.text :@"Please Enter Location"] && ![_txtequipment.text isEqualToString:@"Please Enter Equipment"]) {
                        [self CallAddURLS];
                    }
                }
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Enter Description" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
            }
        }
    }
    else if ([_cat_name isEqualToString:@"BTS"])
    {
        if (_seg_bts.selectedSegmentIndex==0) {
            if (ISimage>0) {
                if ([self CheckValidate:_txt_title.text :@"Please Enter valid title"]) {
                    if ([self CheckValidate:_txt_title.text :@"Please Enter Description"] && ![_txt_desc.text isEqualToString:@"Description"]) {
                        if ([self CheckValidate:_txtlocation.text :@"Please Enter Location"] && ![_txtequipment.text isEqualToString:@"Please Enter Equipment"]) {
                             [self CallAddPortfolio];
                        }
                       
                    }
                    else
                    {
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Enter Description" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                        [alert show];
                    }
                }
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Select Image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
            }
        }
        else
        {
            if ([self CheckValidate:_txt_title.text :@"Please Enter valid title"]) {
                if ([self CheckValidate:_txt_title.text :@"Please Enter Description"] && ![_txt_desc.text isEqualToString:@"Description"]) {
                    if ([self CheckValidate:_txt_urllink.text :@"Please Enter Video Link"] ) {
                       if ([self CheckValidate:_txtlocation.text :@"Please Enter Location"] && ![_txtequipment.text isEqualToString:@"Please Enter Equipment"]) {
                        [self CallAddURLS];
                       }
                    }
                    else
                    {
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Enter Valid Link" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                        [alert show];
                    }
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Enter Description" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                    [alert show];
                }
            }
        }
        
        
        
    }
    
    
}

- (IBAction)seg_img:(id)sender {
    
    
    switch (_seg_bts.selectedSegmentIndex) {
        case 0:
        {
            _txt_urllink.hidden=YES;
            _img_chooseimg.hidden=NO;
            _img_extra.hidden=NO;
            _lbl_extra.hidden=NO;
            
            CGRect buttonframe2 = _vw_details.frame;
            buttonframe2.origin.y =_img_chooseimg.frame.origin.y+_img_chooseimg.frame.size.height+15;
            _vw_details.frame = buttonframe2;
            
            CGRect buttonframe1 = _btn_post.frame;
            buttonframe1.origin.y =_txt_urllink.frame.origin.y;
            _btn_post.frame = buttonframe1;
            
        }
            break;
        case 1:
        {
            _txt_urllink.hidden=NO;
            _img_chooseimg.hidden=YES;
            _img_extra.hidden=YES;
            _lbl_extra.hidden=YES;
            
            
            
            CGRect buttonframe1 = _btn_post.frame;
            buttonframe1.origin.y =_txt_urllink.frame.origin.y+60;
            _btn_post.frame = buttonframe1;
            
            CGRect buttonframe2 = _vw_details.frame;
            buttonframe2.origin.y =160;
            _vw_details.frame = buttonframe2;
            
        }
            break;
        
    }
    
}

- (UIImage *)VideoThumbNail:(NSURL *)videoURL
{
    MPMoviePlayerController *player = [[MPMoviePlayerController alloc] initWithContentURL:videoURL];
    UIImage *thumbnail = [player thumbnailImageAtTime:52.0 timeOption:MPMovieTimeOptionNearestKeyFrame];
    [player stop];
    return thumbnail;
}


-(BOOL)CheckValidate :(NSString*)str :(NSString *)msg
{
    
    if (![str isEqualToString:@""] && ![str isKindOfClass:[NSNull class]] && str.length != 0) {
        return YES;
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    
}


- (BOOL) validateUrl: (NSString *) candidate {
    
    NSString *urlRegEx =@"(http[s]?://(?:www\\.)?youtube.com\\S+)";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:candidate];
    
}



@end
