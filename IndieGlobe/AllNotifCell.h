//
//  AllNotifCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 4/25/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllNotifCell : UITableViewCell

//Outlets
@property (strong, nonatomic) IBOutlet UILabel *txt_postdt;

@property (strong, nonatomic) IBOutlet UILabel *txt_desc;
@property (strong, nonatomic) IBOutlet UILabel *txt_title;
@property (strong, nonatomic) IBOutlet UIImageView *img_prof;

@property (weak, nonatomic) IBOutlet UIView *vw;

//Action



@end
