//
//  EditEventVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/23/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApplicationConstant.h"
#import "HttpWrapper.h"
#import "NewHomeSearchVC.h"
#import "NewProfileCell.h"
#import "UzysImageCropperViewController.h"


@interface EditEventVC : UIViewController<UzysImageCropperDelegate,HttpWrapperDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
{
    NSMutableArray *AddNew;
    //Image Picker Using
    UIImagePickerController *ipc;
    UITextField *currentTextField;
    UIView *picker;
    
    
    

    
    UIImagePickerController *ci;
    UIPopoverController *popover;
    UIDatePicker *datePicker;
    
    UIImage *ISimage;
    
    HttpWrapper* httpeditevent,*httpcountry,*httpstate,*httpcity;
    
    NSString *start_dt;
    NSString *end_dt;
    
    //VAR
    int Default;
    int isCheck;
    NSMutableArray* country;
    IBOutlet UITextField *txt_country;
    NSString *countryid;
    
    NSMutableArray* state;
    IBOutlet UITextField *txt_state;
    NSString * stateid;
    
    NSMutableArray* arrcity;
    IBOutlet UITextField *txt_city;
    NSString * cityid;

}

@property (nonatomic,retain) UzysImageCropperViewController *imgCropperViewController;
@property (nonatomic,retain) UIImagePickerController *imgpicker;

@property (nonatomic,retain)NSMutableDictionary *mutDict;
@property (weak, nonatomic) IBOutlet UIButton *lbl_notifi_count;

@property (weak, nonatomic) IBOutlet UICollectionView *colle_edit_event;

//Outlet
@property (strong, nonatomic) IBOutlet UIButton *btn_menu;
@property (strong, nonatomic) IBOutlet UITextField *txt_skv;
@property (strong, nonatomic) IBOutlet UITextField *txt_date;
@property (strong, nonatomic) IBOutlet UITextView *txt_disc;
@property (strong, nonatomic) IBOutlet UITextView *txt_new_disc;

@property (strong, nonatomic) IBOutlet UIButton *btn_save_event;
@property (strong, nonatomic) IBOutlet UICollectionView *collection_view;
@property (strong, nonatomic) IBOutlet UITextField *txt_from_date;

@property (strong, nonatomic) IBOutlet UIImageView *img_images;
@property (strong, nonatomic) IBOutlet UIImageView *img_extra;
@property (strong, nonatomic) IBOutlet UILabel *lbl_extra;


//Action

- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;
- (IBAction)btn_SAVEEVENT:(id)sender;
- (IBAction)btn_SSATET:(id)sender;
- (IBAction)btn_SCOUNTRY:(id)sender;
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_SCITY:(id)sender;
- (IBAction)btn_SFORM_DT:(id)sender;
- (IBAction)btn_STO_DT:(id)sender;
- (IBAction)btn_NOTIFY:(id)sender;



-(void)Popvalueselected:(NSString*)strValue;
-(void)Popvalveselected:(NSString*)strValve;
-(void)dismissPopUpViewController;


@end
