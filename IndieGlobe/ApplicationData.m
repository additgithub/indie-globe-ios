//
//  ApplicationData.m
//  Towing Service
//
//  Created by RAHUL MEHTA on 03/03/17.
//  Copyright © 2017 RAHUL MEHTA. All rights reserved.
//

#import "ApplicationData.h"

static ApplicationData *applicationData = nil;

@implementation ApplicationData
@synthesize ConsultId;

//Create a Delegate
- (void)initialize {
    [[Crashlytics sharedInstance] crash];
}
- (IBAction)crashButtonTapped:(id)sender {
    [[Crashlytics sharedInstance] crash];
}

+ (ApplicationData*)sharedInstance
{
    if (applicationData == nil)
    {
        applicationData = [[super allocWithZone:NULL] init];
        [applicationData initialize];
    }
    return applicationData;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [self sharedInstance];
}

- (id)init {
    if(self = [super init])
    {
    }
    return self;
}


//Valid Email CheckString
-(BOOL)ValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(UIView *)GradiantColorUsing :(UIView*)View
{
    //Gradiant View
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = View.bounds;
    gradient.colors = @[(id)[UIColor colorWithRed:46/255.0 green:190/255.0 blue:242/255.0 alpha:1].CGColor, (id)[UIColor colorWithRed:22/255.0 green:155/255.0 blue:210/255.0 alpha:1].CGColor];
    
    [View.layer insertSublayer:gradient atIndex:0];
    
    return View;
}
+ (id)cleanJsonToObject:(id)data {
    NSError* error;
    if (data == (id)[NSNull null]){
        return [[NSObject alloc] init];
    }
    id jsonObject;
    if ([data isKindOfClass:[NSData class]]){
        jsonObject = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    } else {
        jsonObject = data;
    }
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSMutableArray *array = [jsonObject mutableCopy];
        for (int i = array.count-1; i >= 0; i--) {
            id a = array[i];
            if (a == (id)[NSNull null]){
                [array removeObjectAtIndex:i];
            } else {
                array[i] = [self cleanJsonToObject:a];
            }
        }
        return array;
    } else if ([jsonObject isKindOfClass:[NSDictionary class]]) {
        NSMutableDictionary *dictionary = [jsonObject mutableCopy];
        for(NSString *key in [dictionary allKeys]) {
            id d = dictionary[key];
            if (d == (id)[NSNull null]){
                dictionary[key] = @"";
            } else {
                dictionary[key] = [self cleanJsonToObject:d];
            }
        }
        return dictionary;
    } else {
        return jsonObject;
    }
}
@end
