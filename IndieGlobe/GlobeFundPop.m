//
//  GlobeFundPop.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/23/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "GlobeFundPop.h"
#import "UIColor+CL.h"
#import "AddfundVC.h"

@interface GlobeFundPop ()

@end

@implementation GlobeFundPop

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _btn_already_have.layer.cornerRadius=_btn_already_have.frame.size.height/2;
    _btn_gfm.layer.cornerRadius=_btn_gfm.frame.size.height/2;
    _btn_ks.layer.cornerRadius=_btn_ks.frame.size.height/2;
    
    //_btn_already_have.layer.borderWidth=1.5;
    //_btn_already_have.layer.borderColor= [UIColor colorWithHex:0x17c4c7].CGColor;
    _btn_gfm.layer.borderWidth=1.5;
    _btn_gfm.layer.borderColor= [UIColor colorWithHex:0x196A84].CGColor;
    _btn_ks.layer.borderWidth=1.5;
    _btn_ks.layer.borderColor= [UIColor colorWithHex:0x196A84].CGColor;
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    
    FundingVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"FundingVC"];
    [self.navigationController pushViewController:next animated:NO];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_ALREADY:(id)sender {
    _btn_already_have.layer.backgroundColor=[UIColor colorWithHex:0x196A84].CGColor;
    _btn_gfm.layer.backgroundColor=[UIColor colorWithHex:0xFFFFFF].CGColor;
    _btn_ks.layer.backgroundColor=[UIColor colorWithHex:0xFFFFFF].CGColor;
    
    [_btn_already_have setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [_btn_gfm setTitleColor:[UIColor colorWithHex:0x196A84] forState:(UIControlStateNormal)];
    [_btn_ks setTitleColor:[UIColor colorWithHex:0x196A84] forState:(UIControlStateNormal)];
    
    
    FundingVC *next2 = [self.storyboard instantiateViewControllerWithIdentifier:@"FundingVC"];
    [self.navigationController pushViewController:next2 animated:NO];
    
    AddfundVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AddfundVC"];
    [self.navigationController pushViewController:next animated:YES];
   
    
}

- (IBAction)btn_GFM_ACTION:(id)sender {
  /*  _btn_already_have.layer.backgroundColor=[UIColor colorWithHex:0xFFFFFF].CGColor;
    _btn_gfm.layer.backgroundColor=[UIColor colorWithHex:0x196A84].CGColor;
    _btn_ks.layer.backgroundColor=[UIColor colorWithHex:0xFFFFFF].CGColor;
     [_btn_gfm setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    
    [_btn_already_have setTitleColor:[UIColor colorWithHex:0x196A84] forState:(UIControlStateNormal)];
    [_btn_gfm setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [_btn_ks setTitleColor:[UIColor colorWithHex:0x196A84] forState:(UIControlStateNormal)];*/
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.gofundme.com/" ]];
    
}

- (IBAction)btn_KS_ACTION:(id)sender {
    /*_btn_already_have.layer.backgroundColor=[UIColor colorWithHex:0xFFFFFF].CGColor;
    _btn_gfm.layer.backgroundColor=[UIColor colorWithHex:0xFFFFFF].CGColor;
    _btn_ks.layer.backgroundColor=[UIColor colorWithHex:0x196A84].CGColor;
     [_btn_ks setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    
    [_btn_already_have setTitleColor:[UIColor colorWithHex:0x196A84] forState:(UIControlStateNormal)];
    [_btn_gfm setTitleColor:[UIColor colorWithHex:0x196A84] forState:(UIControlStateNormal)];
    [_btn_ks setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];*/
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.kickstarter.com/" ]];
}

@end
