//
//  ExampleTableViewCell.m
//  HVTableView
//
//  Created by Parastoo Tabatabayi on 10/29/16.
//  Copyright © 2016 ParastooTb. All rights reserved.
//

#import "ExampleTableViewCell.h"
#import "UIImageView+Haneke.h"
@implementation ExampleTableViewCell

+(NSString*)cellIdentifier{
    static NSString* cellIdentifier;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cellIdentifier = @"Content1";
    });
    return cellIdentifier;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    

    _theImageView.layer.masksToBounds = YES;
    _theImageView.layer.cornerRadius = _theImageView.frame.size.width/2;
    
}
- (IBAction)purchaseButtonDidTap:(id)sender
{
    [_delegate ExampleTableViewCellDidTapPurchaseButton:self];
}
- (void)prepareForReuse
{
    [self.imageView hnk_cancelSetImage];
    self.imageView.image = nil;
}
@end
