//
//  TeamBuildVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/2/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TBCell.h"
#import "HttpWrapper.h"
#import "ApplicationConstant.h"
#import "NewHomeSearchVC.h"

@interface TeamBuildVC : UIViewController<UITableViewDelegate,UITableViewDataSource,HttpWrapperDelegate>
{
    UIView *picker;

    HttpWrapper* httpgetpro;
    NSString *page_no;
    
    NSMutableArray *mutDict;
}

//Outlet
@property (strong, nonatomic) IBOutlet UIButton *btn_menu;
@property (strong, nonatomic) IBOutlet UIButton *btn_create;
@property (strong, nonatomic) IBOutlet UITableView *tbl_proj_list;
@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;




//Action

- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;
- (IBAction)btn_CREATE_NEW:(id)sender;
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_NOTIFY:(id)sender;


@end
