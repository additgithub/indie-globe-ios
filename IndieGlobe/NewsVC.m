//
//  NewsVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/28/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "NewsVC.h"
#import <JSQSystemSoundPlayer/JSQSystemSoundPlayer.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "SWRevealViewController.h"
#import "DeshboardVC.h"
#import "Constants.h"
#import "DSCircularLayout.h"
#import "DSCollectionViewCell.h"
#import "circleVW.h"
#import "NewMenuDemoVC.h"
#import "UIColor+CL.h"
#import "NewHomeVC.h"

//---------------------
#import "DeshboardVC.h"
#import "ProfileVC.h"
#import "CritiquesVC.h"
#import "ClassifiedVC.h"
#import "PackageList.h"
#import "LearningVC.h"
#import "EventVC.h"
#import "TeamBuildVC.h"
#import "UIViewController+ENPopUp.h"
#import "MyEventVC.h"
#import "SettingVC.h"
#import "NewProfileVC.h"
#import "IndieVC.h"
#import "FundingVC.h"
#import "PlatinumInfoVC.h"
#import "TeamBuildVC.h"
#import "TeamBuild1VC.h"
#import "NewsCell.h"
#import "UserProfileVC.h"
#import "NewHomeVC.h"

#import "YKMediaPlayerKit.h"
#import "YKVimeoVideo.h"
#import "YKYouTubeVideo.h"
#import "YKDirectVideo.h"
#import "WebPlayerVC.h"
#import "AllNotificationVC.h"

//------------------------------


#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height
#define ITEM_WIDTH 75
#define ITEM_HEIGHT 75


@interface NewsVC ()

@end

@implementation NewsVC
{
    NSArray *tableData;
    int count;
    BOOL isShow;
    
    YKYouTubeVideo  *_youTubeVideo;
    YKVimeoVideo    *_vimeoVideo;
    YKDirectVideo   *_directVideo;
    YKDirectVideo   *_otherVideo;
    YKUnKnownVideo  *_unknownVideo;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden=YES;
    
    [self customSetup];
    //NEW MENU
    count = 0;
    isShow=false;
    [self setCircularLayout];
    
    
    
    arrimg= [[NSMutableArray alloc]initWithObjects:@"evets4553.png",@"news4557.png",@"critique4552.png",@"classified4551.png",@"learning4556.png",@"indie-pic4555.png",@"global4554.png",@"team4558.png",nil];
    
    teamimg= [[NSMutableArray alloc]initWithObjects:@"35.jpg",@"34.jpg",@"40.jpg",nil];
    
    
    
    //SEARCH BOX
    _txt_search.layer.cornerRadius=12;
    _txt_search.layer.borderColor=[UIColor lightGrayColor].CGColor;
    _txt_search.layer.borderWidth=0.4;
    CGRect frameRect = _txt_search.frame;
    frameRect.size.height = 24; // <-- Specify the height you want here.
    _txt_search.frame = frameRect;
    
    //btn_search
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.btn_search.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(10.0, 10.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    self.btn_search.layer.mask = maskLayer;
    
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];
    
    //HTTP
    
    [APP_DELEGATE showLoadingView:@""];
    [self CallGetNews];
    
    
    ///================
    
    
}


- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if (IsIphone7 || IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = 145;
    }
    else{
        self.revealViewController.rearViewRevealWidth = 145;
    }
}
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}
#pragma mark - GetAllPosts
-(void)CallGetNews
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpnews  = [[HttpWrapper alloc] init];
        httpnews.delegate=self;
        httpnews.getbool=NO;
        [httpnews requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_news",JsonUrlConstant] param:[AddPost copy]];
    });
}

-(void)CallLike:(NSInteger)intval
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[[mutDict valueForKey:@"NewsID"] objectAtIndex:intval] forKey:@"news_id"];
    
    if ([[[mutDict valueForKey:@"IsLike"] objectAtIndex:intval] intValue]==1) {
        [AddPost setValue:@"0" forKey:@"is_like"];
    }
    else
    {
        [AddPost setValue:@"1" forKey:@"is_like"];
    }
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httplike  = [[HttpWrapper alloc] init];
        httplike.delegate=self;
        httplike.getbool=NO;
        [httplike requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@news_like",JsonUrlConstant] param:[AddPost copy]];
    });
}

-(void)CallDislike:(NSInteger)intval
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[[mutDict valueForKey:@"NewsID"] objectAtIndex:intval] forKey:@"news_id"];
    if ([[[mutDict valueForKey:@"IsDisike"] objectAtIndex:intval] intValue]==1) {
        [AddPost setValue:@"0" forKey:@"is_dislike"];
    }
    else
    {
        [AddPost setValue:@"1" forKey:@"is_dislike"];
    }
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpdislike  = [[HttpWrapper alloc] init];
        httpdislike.delegate=self;
        httpdislike.getbool=NO;
        [httpdislike requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@news_dislike",JsonUrlConstant] param:[AddPost copy]];
    });
}


#pragma mark- Wrapper Class Delegate Methods
//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary* )dicsResponse
{
    if(wrapper == httpnews && httpnews != nil)
    {
        NSLog(@"---%@",dicsResponse);
        NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
        mutDict=[[NSMutableDictionary alloc] init];
        mutDict=[dicsResponse valueForKey:@"data"];
        
        theAppDelegate.n_Count = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"notification_count"]];
        
        _lbl_notifi_count.text=theAppDelegate.n_Count;
        
        if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
            _lbl_notifi_count.hidden=YES;
        }
        else
        {
            _lbl_notifi_count.hidden=NO;
        }

        
        if ([[dicsResponse valueForKey:@"status"] integerValue] == 0) {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Success" message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //[alert show];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Success" message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //[alert show];
            
            
            
            [_tbl_news reloadData];
            [APP_DELEGATE hideLoadingView];
            
        }
        [APP_DELEGATE hideLoadingView];
        
        if (!(mutDict.count>0)) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"NO NEWS AVAILABLE" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
        
    }
    else if(wrapper == httplike && httplike != nil)
    {
        NSLog(@"---%@",dicsResponse);
        [self CallGetNews];
        [APP_DELEGATE hideLoadingView];
        
    }
    else if(wrapper == httpdislike && httpdislike != nil)
    {
        NSLog(@"---%@",dicsResponse);
        [self CallGetNews];
        [APP_DELEGATE hideLoadingView];
    }
}


//fetchDataFail
- (void) HttpWrapper:(HttpWrapper* )wrapper fetchDataFail:(NSError* )error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self customSetup];
    [self CallGetNews];
    
    _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
    _lbl_notifi_count.clipsToBounds=YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UIImage *)loadThumbNail:(NSURL *)urlVideo
{
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:urlVideo options:nil];
    AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generate.appliesPreferredTrackTransform=TRUE;
    NSError *err = NULL;
    CMTime time = CMTimeMake(1, 60);
    CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
    NSLog(@"err==%@, imageRef==%@", err, imgRef);
    return [[UIImage alloc] initWithCGImage:imgRef];
}

-(CGFloat)heightForText:(NSString *)text
{
    NSInteger MAX_HEIGHT = 2000;
    UITextView * textView = [[UITextView alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width-80, MAX_HEIGHT+150)];
    textView.text = text;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        textView.font = [UIFont fontWithName:@"Roboto-Light" size:13.0];
    }
    else
    {
        textView.font = [UIFont fontWithName:@"Roboto-Light" size:13.0];
    }
    
    [textView sizeToFit];
    
    
    return textView.frame.size.height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return mutDict.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"NewsCell";
    
    NewsCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[NewsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    
    
    cell.lbl_title.text = [NSString stringWithFormat:@"%@",[[mutDict  valueForKey:@"NewsTitle"] objectAtIndex:indexPath.row]];
    
    NSString *strVI=[NSString stringWithFormat:@"%@",[[mutDict  valueForKey:@"IVUType"] objectAtIndex:indexPath.row]];
    
    if ([strVI isEqualToString:@"V"] || [strVI isEqualToString:@"U"]) {
        cell.img_playbtn.hidden=NO;
        
        
        
        ///////============================
        NSString *str=[NSString stringWithFormat:@"%@", [[mutDict  valueForKey:@"ImageVideoURL"] objectAtIndex:indexPath.row]];  //is your str
        if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
            
            NSString * getid = [self extractYoutubeIdFromLink:str];
            
            NSString *strImg=[NSString stringWithFormat:@"https://img.youtube.com/vi/%@/3.jpg",getid];
            
            [cell.img_newsimage sd_setImageWithURL:[NSURL URLWithString:strImg] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
            
            
        }
        else
        {
            //NSURL *movieURL = [NSURL URLWithString:[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
            _directVideo = [[YKDirectVideo alloc] initWithContent:[NSURL URLWithString:[[mutDict  valueForKey:@"ImageVideoURL"] objectAtIndex:indexPath.row]]];
            [_directVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
                cell.img_newsimage.image = thumbImage;
            }];
        }
        
        //[cell.img_newsimage sd_setImageWithURL:[NSURL URLWithString:[mutDict valueForKey:@"ImageVideoURL"][indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
        
        
        [cell.img_playbtn setTag:indexPath.row];
        [cell.img_playbtn addTarget:self action:@selector(checkBoxClicked:event:)forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        cell.img_playbtn.hidden=YES;
        [cell.img_newsimage sd_setImageWithURL:[NSURL URLWithString:[mutDict valueForKey:@"ImageVideoURL"][indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
    }
    NSString *strlike=[NSString stringWithFormat:@"%@",[[mutDict  valueForKey:@"IsLike"] objectAtIndex:indexPath.row]];
    NSString *strdilike=[NSString stringWithFormat:@"%@",[[mutDict  valueForKey:@"IsDisike"] objectAtIndex:indexPath.row]];
    
    if ([strlike isEqualToString:@"1"]) {
        cell.btn_likenews.selected=YES;
    }
    else{
        cell.btn_likenews.selected=NO;
    }
    
    if ([strdilike isEqualToString:@"1"]) {
        cell.btn_dislikenews.selected=YES;
    }
    else{
        cell.btn_dislikenews.selected=NO;
    }
    
    
    
    
    cell.lbl_like_count.text=[NSString stringWithFormat:@"%@",[[mutDict  valueForKey:@"TotalLikes"] objectAtIndex:indexPath.row]];
    cell.lbl_dislike_count.text=[NSString stringWithFormat:@"%@",[[mutDict  valueForKey:@"TotalDislikes"] objectAtIndex:indexPath.row]];
    
    
    [cell.btn_likenews addTarget:self action:@selector(likeNews:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.btn_dislikenews addTarget:self action:@selector(DislikeNews:) forControlEvents:UIControlEventTouchUpInside];
    
    //================DATE TIME CALCULATION------------
    
    cell.lbl_time.text=[APP_DELEGATE DateCovertforClassified:[[mutDict  valueForKey:@"CreatedOn"] objectAtIndex:indexPath.row]];
    
    
    return cell;
}
- (void)checkBoxClicked:(id)sender event:(id)event
{
    UIButton *btn=sender;
    
    NSLog(@"button was clicked for the following row %li", (long)btn.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[btn tag]  inSection:0];
    NSLog(@"indexpath.row %ld", (long)indexPath.row);
    NSLog(@"indexpath.row %ld", (long)indexPath.section);
    
    /*NSURL *movieURL = [NSURL URLWithString:[[mutDict  valueForKey:@"ImageVideoURL"] objectAtIndex:indexPath.row]];
     [[UIApplication sharedApplication] openURL:movieURL];
     */
    
    NSString *str=[NSString stringWithFormat:@"%@", [[mutDict  valueForKey:@"ImageVideoURL"] objectAtIndex:indexPath.row]];  //is your str
    if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
        
        WebPlayerVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"WebPlayerVC"];
        next.strurl=[[mutDict  valueForKey:@"ImageVideoURL"] objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:next animated:NO];
        
        
    }
    else
    {
        NSURL *videoURL = [NSURL URLWithString:[[mutDict  valueForKey:@"ImageVideoURL"] objectAtIndex:indexPath.row]];
        AVPlayer *player = [AVPlayer playerWithURL:videoURL];
        AVPlayerViewController *avplayerViewController = [AVPlayerViewController new];
        avplayerViewController.player = player;
        //[self.view addSubview:avplayerViewController.view];
        [self presentViewController:avplayerViewController animated:YES completion:nil];
    }
    
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.selected) {
        // ... Uncheck
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    
    
    
    
}
-(CGFloat)tableView:(UITableView* )tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    NSString * text = [NSString stringWithFormat:@"%@",[[mutDict  valueForKey:@"NewsTitle"] objectAtIndex:indexPath.row]];
    NSInteger  height1 =  [self heightForText:text];
    if ([[NSString stringWithFormat:@"%ld",(long)height1] isEqualToString:@"27"])
    {
        return 290;
    }
    else
    {
        return 290 + [self heightForText:text];
    }
}

-(void)likeNews:(id)sender
{
    NSLog(@"User Like %@",sender);
    //NSInteger ival=[sender integerValue];
    
    UIButton *btn = (UIButton *)sender;
    CGPoint origin = btn.frame.origin;
    CGPoint point = [btn.superview convertPoint:origin toView:_tbl_news];
    NSIndexPath * indexPath = [_tbl_news indexPathForRowAtPoint:point];
    
    NewsCell *cell=(NewsCell *)[_tbl_news cellForRowAtIndexPath:indexPath];
    
    [self CallLike:indexPath.row];
    
    
    NSString *a = [NSString stringWithFormat:@"%@",cell.lbl_like_count.text];
    NSInteger b = [a integerValue];
    
    if (btn.selected == YES)
    {
        btn.selected = NO;
        cell.lbl_like_count.text=[NSString stringWithFormat:@"%ld",b-1];
    }
    
    else
    {
        btn.selected = YES;
        cell.lbl_like_count.text=[NSString stringWithFormat:@"%ld",b+1];
        
        if (cell.btn_dislikenews.selected==YES) {
            cell.btn_dislikenews.selected=NO;
            cell.lbl_dislike_count.text=[NSString stringWithFormat:@"%ld",[cell.lbl_dislike_count.text integerValue]-1];
        }
    }
}
-(void)DislikeNews:(id)sender
{
    
    NSLog(@"Dislike NEWS  %@",sender);
    UIButton *btn = (UIButton *)sender;
    
    CGPoint origin = btn.frame.origin;
    CGPoint point = [btn.superview convertPoint:origin toView:_tbl_news];
    NSIndexPath * indexPath = [_tbl_news indexPathForRowAtPoint:point];
    NewsCell *cell=(NewsCell *)[_tbl_news cellForRowAtIndexPath:indexPath];
    
    [self CallDislike:indexPath.row];
    
    NSString *a = [NSString stringWithFormat:@"%@",cell.lbl_dislike_count.text];
    NSInteger b = [a integerValue];
    
    if (btn.selected == YES)
    {
        btn.selected = NO;
        cell.lbl_dislike_count.text=[NSString stringWithFormat:@"%ld",b-1];
    }
    
    else
    {
        btn.selected = YES;
        cell.lbl_dislike_count.text=[NSString stringWithFormat:@"%ld",b+1];
        
        if (cell.btn_likenews.selected==YES) {
            cell.btn_likenews.selected=NO;
            cell.lbl_like_count.text=[NSString stringWithFormat:@"%ld",[cell.lbl_like_count.text integerValue]-1];
        }
        
    }
    
}

- (IBAction)btn_DISLIKE:(id)sender {
}

- (IBAction)btn_LIKE:(id)sender {
}

- (IBAction)btn_HOME:(id)sender {
    /*UserProfileVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileVC"];
     [self.navigationController pushViewController:next animated:NO];*/
    
    NewHomeVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:NO];
}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}

- (IBAction)btn_SCROLL:(id)sender {
    circleVW *next = [self.storyboard instantiateViewControllerWithIdentifier:@"circleVW"];
    [self.navigationController pushViewController:next animated:YES];
}

- (IBAction)ButtonMenu:(id)sender {
    [self playAlertSoundPressed:nil];
    if (isShow==false) {
        _collectionview.hidden=NO;
        _img_colle_back.hidden=NO;
        isShow=true;
    }
    else
    {
        _collectionview.hidden=YES;
        _img_colle_back.hidden=YES;
        isShow=false;
    }
}

- (IBAction)btn_NOTIFY:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}
- (UIImage* )imageFromMovie:(NSURL* )movieURL atTime:(NSTimeInterval)time {
    // set up the movie player
    MPMoviePlayerController *mp = [[MPMoviePlayerController alloc]
                                   initWithContentURL:movieURL];
    mp.shouldAutoplay = NO;
    mp.initialPlaybackTime = time;
    mp.currentPlaybackTime = time;
    // get the thumbnail
    thumbnail = [mp thumbnailImageAtTime:time
                              timeOption:MPMovieTimeOptionNearestKeyFrame];
    // clean up the movie player
    [mp stop];
    return(thumbnail);
}



-(void)setCircularLayout{
    // your collectionView
    if ([[UIScreen mainScreen] bounds].size.height == 736.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(190, 270)
                                radius:180
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:72];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collectionview setCollectionViewLayout:circularLayout];
    }
    else  if ([[UIScreen mainScreen] bounds].size.height == 812.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(200, 260)
                                radius:180
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:72];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collectionview setCollectionViewLayout:circularLayout];
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 667.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(190, 270)
                                radius:180
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:72];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collectionview setCollectionViewLayout:circularLayout];
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 568.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(158, 230)
                                radius:150
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:58];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collectionview setCollectionViewLayout:circularLayout];
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 480.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(158, 230)
                                radius:160
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:58];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collectionview setCollectionViewLayout:circularLayout];
    }
    
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 8;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    DSCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    //cell.layer.cornerRadius=cell.frame.size.height/2;
    //cell.layer.borderWidth=1.5;
    //cell.layer.borderColor=[UIColor whiteColor].CGColor;
    cell.img_icon.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",[arrimg objectAtIndex:indexPath.row]]];
    
    [self playAlertSoundPressed:nil];
    
    return cell;
}
- (IBAction)playAlertSoundPressed:(UIButton *)sender
{
    [[JSQSystemSoundPlayer sharedPlayer] playAlertSoundWithFilename:@"Click2-Sebastian"
                                                      fileExtension:kJSQSystemSoundTypeAIFF
                                                         completion:nil];
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self playAlertSoundPressed:nil];
    [self SelectedMenu:indexPath.row];
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 100, 0, 0);
}

- (void)SelectedMenu:(NSInteger)index{
    
    NSLog(@"%ld",(long)index);
    
    switch (index) {
        case 0:
        {
            //Perform any action that u want on menu selection
            EventVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"EventVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            break;
            
        case 1:{
            /*  NewsVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsVC"];
             [self.navigationController pushViewController:next animated:NO];*/
            
        }
            
            break;
        case 2:
        {
            CritiquesVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"CritiquesVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 3:
        {
            ClassifiedVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"ClassifiedVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 4:
        {
            LearningVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"LearningVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 5:
        {
            IndieVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"IndieVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 6:
        {
            FundingVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"FundingVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            break;
        case 7:
        {
            TeamBuild1VC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"TeamBuild1VC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            break;
            
            
            break;
        default:
            break;
    }
}




#pragma video image
-(UIImage* )generateThumbImage : (NSURL* )filepath
{
    
    
    
    NSString *str=[NSString stringWithFormat:@"%@", filepath];  //is your str
    
    NSLog(@"%@",str);
    
    if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
        
        url = filepath;
        NSLog(@"string does not contain bla");
        
        AVAsset *asset = [AVAsset assetWithURL:url];
        AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
        imageGenerator.appliesPreferredTrackTransform = YES;
        CMTime time = [asset duration];
        time.value = 1000;
        CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
        thumbnail = [UIImage imageWithCGImage:imageRef];
        CGImageRelease(imageRef);
        
        
    }
    else if(([str rangeOfString:@"youtu.be"].location == NSNotFound)){
        NSArray *items;
        
        if ([str rangeOfString:@"embed/"].location == NSNotFound) {
            items = [str componentsSeparatedByString:@"="];   //take the one array for split the string
            NSLog(@"string does not contain bla");
        } else {
            NSLog(@"string contains bla!");
            items = [str componentsSeparatedByString:@"embed/"];   //take the one array for split the string
        }
        
        NSLog(@"items %@",items);
        NSLog(@"%@",[items objectAtIndex:1]);
        NSString *myString = [NSString stringWithFormat:@"%@",[items objectAtIndex:1]];
        
        myString  = [myString stringByReplacingOccurrencesOfString:@" -- file:///" withString:@" "];
        
        url = [NSURL URLWithString:[NSString stringWithFormat:@"https://img.youtube.com/vi/%@/default.jpg",myString]];
        
        NSData *data = [NSData dataWithContentsOfURL:url];
        thumbnail = [UIImage imageWithData:data];
        
        
        
    } else {
        NSLog(@"string contains bla!");
        
        NSArray *items;
        
        if ([str rangeOfString:@"embed/"].location == NSNotFound) {
            items = [str componentsSeparatedByString:@"="];   //take the one array for split the string
            NSLog(@"string does not contain bla");
        } else {
            NSLog(@"string contains bla!");
            items = [str componentsSeparatedByString:@"embed/"];   //take the one array for split the string
        }
        NSLog(@"items %@",items);
        
        
        
        NSLog(@"%@",[items objectAtIndex:1]);
        
        NSString *myString = [NSString stringWithFormat:@"%@",[items objectAtIndex:1]];
        
        myString  = [myString stringByReplacingOccurrencesOfString:@" -- file:///" withString:@" "];
        
        url = [NSURL URLWithString:[NSString stringWithFormat:@"https://img.youtube.com/vi/%@/default.jpg",myString]];
        
        NSData *data = [NSData dataWithContentsOfURL:url];
        thumbnail = [UIImage imageWithData:data];
        
    }
    
    // CGImageRef won't be released by ARC
    
    
    
    return thumbnail;
}
- (UIImage*)loadImage : (NSURL*) url{
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:url options:nil];
    AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generate.appliesPreferredTrackTransform = YES;
    NSError *err = NULL;
    CMTime time = CMTimeMake(1, 60);
    CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
    NSLog(@"err==%@, imageRef==%@", err, imgRef);
    
    return [[UIImage alloc] initWithCGImage:imgRef];
    
}
- (NSString *)extractYoutubeIdFromLink:(NSString *)link {
    NSString *regexString = @"((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)";
    NSRegularExpression *regExp = [NSRegularExpression regularExpressionWithPattern:regexString
                                                                            options:NSRegularExpressionCaseInsensitive
                                                                              error:nil];
    
    NSArray *array = [regExp matchesInString:link options:0 range:NSMakeRange(0,link.length)];
    if (array.count > 0) {
        NSTextCheckingResult *result = array.firstObject;
        return [link substringWithRange:result.range];
    }
    return nil;
}
@end
