//
//  UserProfileVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 2/5/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "UserProfileVC.h"

@interface UserProfileVC ()<UIActionSheetDelegate>

@property (nonatomic, assign) BOOL wrap;
@property (nonatomic, strong) NSMutableArray *items;

@end

@implementation UserProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    _wrap = YES;
    _vw_caro.type = iCarouselTypeRotary;
    arrimg=[[NSMutableArray alloc]initWithObjects:@"1.jpg",@"2.jpg",@"3.jpg",@"4.png",@"5.jpg",@"6.jpg",@"7.jpg", nil];
    
    // attach long press gesture to collectionView
    UILongPressGestureRecognizer *lpgr
    = [[UILongPressGestureRecognizer alloc]
       initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.delegate = self;
    lpgr.delaysTouchesBegan = YES;
    [_vw_caro addGestureRecognizer:lpgr];
    
}
- (void)setUp
{
    //set up data
    _wrap = YES;
    self.items = [NSMutableArray array];
    for (int i = 0; i < 7; i++)
    {
        [_items addObject:@(i)];
    }
    arrimg=[[NSMutableArray alloc]initWithObjects:@"1.jpg",@"2.jpg",@"3.jpg",@"4.png",@"5.jpg",@"6.jpg",@"7.jpg", nil];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder]))
    {
        [self setUp];
    }
    return self;
}
- (IBAction)reloadCarousel
{
    [_vw_caro reloadData];
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
        NSLog(@"couldn't find index path");
}

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return [_items count];
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            return _wrap;
        }
        case iCarouselOptionFadeMax:
        {
            if (carousel.type == iCarouselTypeCustom)
            {
                return 0.0f;
            }
            return value;
        }
        case iCarouselOptionArc:
        {
            return 2 * M_PI * 0.00100;
        }
        case iCarouselOptionRadius:
        {
            return value * 0.162824;
        }
        case iCarouselOptionTilt:
        {
            return 0.9977221;
        }
        case iCarouselOptionSpacing:
        {
            return value * 0.610219;
        }
        default:
        {
            return value;
        }
    }
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)Iview
{
    
    UILabel *label = nil;
    
    
   
    
    if (Iview == nil)
    {
        Iview=[[UIView alloc] initWithFrame:CGRectMake(0,0,200,200)];
        
        UIImageView *dot =[[UIImageView alloc] initWithFrame:Iview.bounds];
        NSLog(@"arra %@",arrimg[index]);
        dot.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",arrimg[index]]];
        [Iview addSubview:dot];
        
        label = [[UILabel alloc] initWithFrame:Iview.bounds];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [label.font fontWithSize:50];
        label.tag = 1;
        [Iview addSubview:label];

    }
    else
    {
        //get a reference to the label in the recycled view
        label = (UILabel *)[Iview viewWithTag:1];
    }
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    label.text = [_items[index] stringValue];
    
    return Iview;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
