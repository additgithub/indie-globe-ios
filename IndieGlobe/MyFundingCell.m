//
//  MyFundingCell.m
//  IndieGlobe
//
//  Created by mac on 14/08/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "MyFundingCell.h"
#import "UIColor+CL.h"


@implementation MyFundingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.vw_shadow setShadow];
    
    
    _btn_donate.layer.cornerRadius=_btn_donate.frame.size.height/2;
    _btn_donate.layer.borderWidth=1;
    _btn_donate.layer.borderColor=[UIColor colorWithHex:0x196a84].CGColor;
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
