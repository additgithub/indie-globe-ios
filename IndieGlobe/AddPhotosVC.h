//
//  AddPhotosVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/25/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
#import "NewHomeSearchVC.h"
#import "UzysImageCropperViewController.h"


@interface AddPhotosVC : UIViewController<UzysImageCropperDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,HttpWrapperDelegate>
{
    UIImagePickerController *ci;
    UIPopoverController *popover;
    
    UIImage *ISimage;
    
    HttpWrapper* httpnewprof;
}

@property (nonatomic,retain) UzysImageCropperViewController *imgCropperViewController;
@property (weak, nonatomic) IBOutlet UITextField *txtequipment;
@property (weak, nonatomic) IBOutlet UITextField *txtlocation;

@property (retain , nonatomic)NSString *cat_id;
@property (retain , nonatomic)NSString *cat_name;
//OUTLET
@property (weak, nonatomic) IBOutlet UIView *vw_details;
@property (strong, nonatomic) IBOutlet UITextField *txt_title;
@property (strong, nonatomic) IBOutlet UITextField *txt_urllink;
@property (strong, nonatomic) IBOutlet UIButton *btn_post;

@property (strong, nonatomic) IBOutlet UIImageView *img_chooseimg;
@property (strong, nonatomic) IBOutlet UIImageView *img_extra;
@property (weak, nonatomic) IBOutlet UILabel *lbl_extra;

@property (strong, nonatomic) IBOutlet UIImageView *img_thum_gen;

@property (weak, nonatomic) IBOutlet UITextView *txt_desc;

@property (weak, nonatomic) IBOutlet UILabel *lbl_add_titel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *seg_bts;

//ACTION
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;
- (IBAction)btn_POST_ACTION:(id)sender;
- (IBAction)seg_img:(id)sender;

@end
