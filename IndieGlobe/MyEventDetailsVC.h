//
//  MyEventDetailsVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/7/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
#import "ApplicationConstant.h"
#import "NewHomeSearchVC.h"

@interface MyEventDetailsVC : UIViewController<HttpWrapperDelegate>
{
    HttpWrapper* httpmyevetdetails;
    
   
    
}
@property(nonatomic,retain)NSMutableArray *mutdict;

@property(nonatomic,retain)NSMutableArray *resdict;

//Outlet
@property (strong, nonatomic) IBOutlet UIButton *btn_menu;
@property (strong, nonatomic) IBOutlet UITableView *tbl_main;
@property (strong, nonatomic) IBOutlet UIView *vw_main;

@property (strong, nonatomic) IBOutlet UIImageView *img_image;
@property (strong, nonatomic) IBOutlet UILabel *lbl_s_mon;
@property (strong, nonatomic) IBOutlet UILabel *lbl_s_date;
@property (strong, nonatomic) IBOutlet UILabel *lbl_rsvp_count;
@property (strong, nonatomic) IBOutlet UILabel *lbl_post_date;
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;
@property (strong, nonatomic) IBOutlet UILabel *lbl_desc;
@property (strong, nonatomic) IBOutlet UILabel *lbl_address;

@property (weak, nonatomic) IBOutlet UIScrollView *scroll_event_img;
@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;


//Action
- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_NOTIFY:(id)sender;


@end
