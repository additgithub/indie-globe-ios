//
//  PackageList.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/29/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PayPalMobile.h"
#import "PayPalConfiguration.h"
#import "PayPalPaymentViewController.h"

@interface PackageList : UIViewController<PayPalPaymentDelegate>
{
    NSMutableArray *arrplan;
    
    int ButtonFlage;
    int PackageFlage;
    
}
@property(nonatomic, strong, readwrite) NSString *environment;
@property(nonatomic, strong, readwrite) NSString *resultText;



//Outlet


@property (strong, nonatomic) IBOutlet UILabel *lbl_gold;
@property (strong, nonatomic) IBOutlet UILabel *lbl_platinum;
@property (strong, nonatomic) IBOutlet UILabel *lbl_diamond;

@property (strong, nonatomic) IBOutlet UIButton *btn_pay1;
@property (strong, nonatomic) IBOutlet UIButton *btn_pay2;
@property (weak, nonatomic) IBOutlet UIButton *btn_act_glod;

@property (strong, nonatomic) IBOutlet UIView *vw_package;
@property (strong, nonatomic) IBOutlet UIImageView *img_gold;
@property (strong, nonatomic) IBOutlet UIImageView *img_platinum;
@property (strong, nonatomic) IBOutlet UIImageView *img_diamond;
@property (strong, nonatomic) IBOutlet UIButton *btn_cancel;
@property (strong, nonatomic) IBOutlet UILabel *lbl_plat_plan;
@property (strong, nonatomic) IBOutlet UILabel *lbl_daim_plan;
@property (strong, nonatomic) IBOutlet UIView *vw_alpha;


//RADIO BUTTONS
@property (strong, nonatomic) IBOutlet UIImageView *img_plat_mo;
@property (strong, nonatomic) IBOutlet UIImageView *img_plat_yr;
@property (strong, nonatomic) IBOutlet UIImageView *img_diam_mo;
@property (strong, nonatomic) IBOutlet UIImageView *img_diam_yr;
- (IBAction)btn_SP_MO:(id)sender;
- (IBAction)btn_SP_YR:(id)sender;
- (IBAction)btn_SD_MO:(id)sender;
- (IBAction)btn_SD_YR:(id)sender;




//Action
- (IBAction)btn_SGOLD:(id)sender;
- (IBAction)btn_SPLATINUM:(id)sender;
- (IBAction)btn_SDIAMOND:(id)sender;
- (IBAction)btn_PAY_ACTION:(id)sender;
- (IBAction)btn_PAY_ACTION1:(id)sender;
- (IBAction)btn_ACTIVE_GLOD:(id)sender;






@end
