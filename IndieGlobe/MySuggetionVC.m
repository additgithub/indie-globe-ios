//
//  MySuggetionVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 8/11/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "MySuggetionVC.h"
#import "NewHomeVC.h"



@interface MySuggetionVC ()

@end

@implementation MySuggetionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     self.navigationController.navigationBarHidden=YES;
    
    _txt_suggetion.delegate=self;
    _txt_suggetion.text = @"Your Suggetion";
    _txt_suggetion.textColor = [UIColor lightGrayColor];
    _txt_suggetion.layer.borderWidth=1.0;
    _txt_suggetion.layer.cornerRadius=11;
    _txt_suggetion.layer.borderColor=[UIColor colorWithRed:(160.0f/255.0) green:(160.0f/255.0) blue:(160.0f/255.0) alpha:1].CGColor;
    
    
    

    _submit_button.layer.cornerRadius=5;
    _cancel_button.layer.cornerRadius=5;
    
    
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if([textView.text isEqual: @"Your Suggetion"])
    {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)btn_BACK:(id)sender {
     //[self.navigationController popViewControllerAnimated:YES];
    
    NewHomeVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:NO];
}

- (IBAction)btn_cancel:(id)sender {
    
    NewHomeVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:NO];
}

- (IBAction)btn_submit:(id)sender {
    
    if ([_txt_suggetion.text isEqualToString:@""]||[_txt_suggetion.text isEqualToString:@"Your Suggetion"] || _txt_suggetion.text.length <= 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Enter Suggetion First.!"
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"OK",nil];
        [alert show];
    }
    else
    {
        [self CallSuggetion];
    }
    
}

-(void)CallSuggetion
{
    
    [APP_DELEGATE showLoadingView:@""];
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:_txt_suggetion.text forKey:@"message"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"PARAM---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@add_suggestion",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                                        message:@"You Added Suggestion"
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"OK",nil];
        [alert show];
        
        NewHomeVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
        [self.navigationController pushViewController:next animated:NO];
        
        _txt_suggetion.text=@"";
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
        
    }];
}



@end
