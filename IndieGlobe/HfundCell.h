//
//  HfundCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 3/23/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShadowView.h"


@interface HfundCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *img_prof;
@property (strong, nonatomic) IBOutlet UILabel *lbl_usernm;
@property (strong, nonatomic) IBOutlet UILabel *lbl_postdt;
@property (strong, nonatomic) IBOutlet UIImageView *img_image;
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;
//@property (strong, nonatomic) IBOutlet UITextView *lbl_disc;
@property (weak, nonatomic) IBOutlet UILabel *lbl_desc;

@property (strong, nonatomic) IBOutlet UIButton *btn_donet;
@property (strong, nonatomic) IBOutlet UILabel *lbl_amt;
@property (weak, nonatomic) IBOutlet UIButton *btn_follow;


@property (weak, nonatomic) IBOutlet ShadowView *shadow_vw;

@end
