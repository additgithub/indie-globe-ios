//
//  TeamBuild1VC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/25/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "TeamBuild1VC.h"
#import <JSQSystemSoundPlayer/JSQSystemSoundPlayer.h>
#import "SWRevealViewController.h"
#import "UIColor+CL.h"
#import "DeshboardVC.h"
#import "ProfileVC.h"
#import "PDVC.h"
#import "ProjectVC.h"
#import "TBCell.h"
#import "InviteTeamVC.h"
#import "Constants.h"
#import "TB1.h"
#import "DSCircularLayout.h"
#import "DSCollectionViewCell.h"
#import "PD1VC.h"
#import "SVPullToRefresh.h"
#import "NewHomeVC.h"
#import "AllNotificationVC.h"
#import <JSQSystemSoundPlayer/JSQSystemSoundPlayer.h>
#import "ProjectVC.h"






#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height
#define ITEM_WIDTH 75
#define ITEM_HEIGHT 75



@interface TeamBuild1VC ()

@end

@implementation TeamBuild1VC
{
    NSArray *tableData;
    int count;
    BOOL isShow;
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    // Do any additional setup after loading the view.
     [self customSetup];
    _btn_create.layer.cornerRadius=_btn_create.frame.size.height/2;
    
    //NEW MENU
    page_no=@"0";
    count = 0;
    isShow=false;
    [self setCircularLayout];
    
    arrimg= [[NSMutableArray alloc]initWithObjects:@"evets4553.png",@"news4557.png",@"critique4552.png",@"classified4551.png",@"learning4556.png",@"indie-pic4555.png",@"global4554.png",@"team4558.png",nil];
    
    
    
    
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];
    
    
    
    
    _btn_createnew.layer.cornerRadius=_btn_createnew.layer.frame.size.height/2;
    _btn_createnew.clipsToBounds=YES;
    
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    page_no=@"0";
    [self CallProjectList];
    [self customSetup];
    
   
}


- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    if (IsIphone7 || IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = 145;
    }
    else{
        self.revealViewController.rearViewRevealWidth = 145;
    }
}


- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}

#pragma mark - GetAllPosts
-(void)CallProjectList
{
    [APP_DELEGATE showLoadingView:@""];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",page_no] forKey:@"page_no"];
    //[AddPost setValue:[NSString stringWithFormat:@"5"] forKey:@"page_no"];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);

    [manager POST:[NSString stringWithFormat:@"%@get_project_list",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        mutDict=[[NSMutableArray alloc] init];
        mutDict=[responseObject valueForKey:@"data"];
        page_no=[responseObject valueForKey:@"page_no"];
        
        theAppDelegate.n_Count = [NSString stringWithFormat:@"%@",[responseObject valueForKey:@"notification_count"]];
        
        _lbl_notifi_count.text=theAppDelegate.n_Count;
        
        if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
            _lbl_notifi_count.hidden=YES;
            
        }
        else
        {
            _lbl_notifi_count.hidden=NO;
            _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
            _lbl_notifi_count.clipsToBounds=YES;
        }

        
        if (mutDict <=0) {
            _tbl_list.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        }
        else
        {
            [_tbl_list reloadData];
            
        }
        [APP_DELEGATE hideLoadingView];
        
        if (mutDict.count<1) {
            [_lbl_nodata setCenter:self.view.center];
            _lbl_nodata.hidden=NO;
        }
        else
        {
            _lbl_nodata.hidden=YES;
        }
        

        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];

    }];

}

-(void)CallGetProjects1
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",page_no] forKey:@"page_no"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    [manager POST:[NSString stringWithFormat:@"%@get_project_list",JsonUrlConstant] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        page_no=[responseObject valueForKey:@"page_no"];
        
        if ([[responseObject valueForKey:@"data"] count]>0) {
            
            int64_t delayInSeconds = 2.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                //[_tbl_event beginUpdates];
                NSArray *arr = [responseObject valueForKey:@"data"];
                NSMutableArray *newarr = [[NSMutableArray alloc] init];
                NSLog(@"---%@",newarr);
                for (NSString *str in mutDict) {
                    [newarr addObject:str];
                }
                for (int i=0; i<arr.count; i++) {
                    //[mutDict addObject:arr[i]];
                    [newarr addObject:arr[i]];
                }
                NSLog(@"---%@",newarr);
                mutDict = [[NSMutableArray alloc] init];
                for (NSString*str  in newarr) {
                    [mutDict addObject:str];
                }
                
                
                NSLog(@"--%@",mutDict);
                [_tbl_list.infiniteScrollingView stopAnimating];
                [_tbl_list reloadData];
                [APP_DELEGATE hideLoadingView];
                
            });
            
            
            
        }
        else
        {
            [_tbl_list.infiniteScrollingView stopAnimating];
            [APP_DELEGATE hideLoadingView];
            
        }
        
        if (mutDict.count<1) {
            [_lbl_nodata setCenter:self.view.center];
            _lbl_nodata.hidden=NO;
        }
        else
        {
            _lbl_nodata.hidden=YES;
        }
        
        
                
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    
}



/*-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
 
   /* [self.collection_view performBatchUpdates:^{
        [self.collection_view insertItemsAtIndexPaths:[NSArray arrayWithObjects:
                                                      [NSIndexPath indexPathForRow:0 inSection:0],
                                                      [NSIndexPath indexPathForRow:1 inSection:0],
                                                      [NSIndexPath indexPathForRow:2 inSection:0],
                                                      [NSIndexPath indexPathForRow:3 inSection:0],
                                                      [NSIndexPath indexPathForRow:4 inSection:0],
                                                      [NSIndexPath indexPathForRow:5 inSection:0],
                                                      [NSIndexPath indexPathForRow:6 inSection:0],
                                                      [NSIndexPath indexPathForRow:7 inSection:0],
                                                      
                                                      nil]];
        count = 8;
    } completion:^(BOOL finished) {
        [self.collection_view reloadData];
    }];
    
    count = 8;
}*/


-(void)setCircularLayout{
    // your collectionView
    if ([[UIScreen mainScreen] bounds].size.height == 736.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(190, 270)
                                radius:180
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:72];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }
    else  if ([[UIScreen mainScreen] bounds].size.height == 812.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(200, 260)
                                radius:180
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:72];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 667.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(190, 270)
                                radius:180
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:72];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 568.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(158, 230)
                                radius:150
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:58];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 480.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(158, 230)
                                radius:160
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:58];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }
    
    
    
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 8;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    DSCollectionViewCell *cell = (DSCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    //cell.layer.cornerRadius=cell.frame.size.height/2;
    //cell.layer.borderWidth=1.5;
    //cell.layer.borderColor=[UIColor whiteColor].CGColor;
    cell.img_icon.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",[arrimg objectAtIndex:indexPath.row]]];
    
    
    [self playAlertSoundPressed:nil];
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self playAlertSoundPressed:nil];
    [self SelectedMenu:indexPath.row];
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 100, 0, 0);
}

- (void)SelectedMenu:(NSInteger)index{
    
    NSLog(@"%ld",(long)index);
    
    switch (index) {
        case 0:
        {
            //Perform any action that u want on menu selection
            EventVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"EventVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            break;
            
        case 1:{
              NewsVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsVC"];
             [self.navigationController pushViewController:next animated:NO];
            
        }
            
            break;
        case 2:
        {
            CritiquesVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"CritiquesVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 3:
        {
            ClassifiedVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"ClassifiedVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 4:
        {
            LearningVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"LearningVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 5:
        {
            IndieVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"IndieVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 6:
        {
            FundingVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"FundingVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            break;
        case 7:
        {
          /*  TeamBuild1VC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"TeamBuild1VC"];
            [self.navigationController pushViewController:next animated:NO];
           */
        }
            break;
            
            
            break;
        default:
            break;
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return mutDict.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Cell";
    tableView.separatorColor = [UIColor clearColor];
    TB1 *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[TB1 alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.btn_join_team.tag = indexPath.row;
    [cell.btn_join_team addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    cell.lbl_title.text=[NSString stringWithFormat:@"%@",[[mutDict valueForKey:@"ProjectTitle"] objectAtIndex:indexPath.row]];
    cell.lbl_desc.text=[NSString stringWithFormat:@"%@",[[mutDict valueForKey:@"ShortDesc"] objectAtIndex:indexPath.row]];
    
    cell.lbl_tot_invited.text=[NSString stringWithFormat:@"%@ Invited",[[mutDict valueForKey:@"TotalInvited"] objectAtIndex:indexPath.row]];
    cell.lbl_tot_join.text=[NSString stringWithFormat:@"%@ Joined",[[mutDict valueForKey:@"TotalJoined"] objectAtIndex:indexPath.row]];
    
    cell.lbl_usernm.text=[NSString stringWithFormat:@"%@ %@",[mutDict valueForKey:@"FName"][indexPath.row],[mutDict valueForKey:@"LName"][indexPath.row]];
    [cell.img_user sd_setImageWithURL:[NSURL URLWithString:[mutDict valueForKey:@"ImageURL"][indexPath.row]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
    
    [cell.img_proj_image sd_setImageWithURL:[NSURL URLWithString:[mutDict valueForKey:@"ProjectImage"][indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
    
    
    [cell.img_proj_image setContentMode:UIViewContentModeScaleAspectFill];
    [cell.img_proj_image setClipsToBounds:YES];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    if ([[[mutDict valueForKey:@"CreatedBy"] objectAtIndex:indexPath.row]isEqualToString:[NSString stringWithFormat:@"%@",[standardUserDefaults valueForKey:@"UserID"]]]) {
        cell.btn_join_team.alpha=0.40;
        cell.btn_join_team.enabled=NO;
    }
    else
    {
        if ([[[mutDict valueForKey:@"IsJoined"] objectAtIndex:indexPath.row]isEqualToString:@"1"] || [[[mutDict valueForKey:@"IsInvited"] objectAtIndex:indexPath.row]isEqualToString:@"1"])
        {
            cell.btn_join_team.alpha=0.40;
            cell.btn_join_team.enabled=NO;
        }
        else
        {
            cell.btn_join_team.alpha=1.00;
            cell.btn_join_team.enabled=YES;
        }
    }
    
    //cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
    return cell;
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    int lastRow=[mutDict count]-1;
    
    if(([indexPath row] == lastRow)&&(lastRow<[mutDict count]))
    {
        //[self Getnewdata];
        NSLog(@"i m call");
        
        if ([mutDict count]>=2) {
            __weak TeamBuild1VC *weakSelf = self;
            // setup infinite scrolling
            [_tbl_proj_list addInfiniteScrollingWithActionHandler:^{
                [weakSelf CallGetProjects1];
            }];
        }
        
    }
    
}


-(void)yourButtonClicked:(UIButton*)sender
{
    
        // Your code here
        NSLog(@"Button click");
        NSLog(@"buttonPressedDeny: %ld", (long)sender.tag);
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Your request to join the team has been sent"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        //Post Method Request
        NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
        NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
        
        [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
        [AddPost setValue:[NSString stringWithFormat:@"%@",[[mutDict valueForKey:@"ProjectID"] objectAtIndex:sender.tag]] forKey:@"project_id"];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
        NSLog(@"---%@",AddPost);//get_invited_people_list
        [manager POST:[NSString stringWithFormat:@"%@join_project",JsonUrlConstant] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            page_no=@"0";
            [self CallProjectList];
            [_tbl_list reloadData];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];

}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
     UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
     if (cell.selected) {
     // ... Uncheck
     [tableView deselectRowAtIndexPath:indexPath animated:YES];
     }
   
    PD1VC* next = [self.storyboard instantiateViewControllerWithIdentifier:@"PD1VC"];
    next.mutDict=[mutDict objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:next animated:YES];
    
    
}



- (IBAction)btn_HOME:(id)sender {
    
    NewHomeVC* next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:YES];

    
}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}

- (IBAction)btn_MENU:(id)sender {
    
    [self playAlertSoundPressed:nil];
    
    if (isShow==false) {
        _collection_view.hidden=NO;
        _img_colle_back.hidden=NO;
        isShow=true;
    }
    else
    {
        _collection_view.hidden=YES;
        _img_colle_back.hidden=YES;
        isShow=false;
    }

}
- (IBAction)playAlertSoundPressed:(UIButton *)sender
{
    [[JSQSystemSoundPlayer sharedPlayer] playAlertSoundWithFilename:@"Click2-Sebastian"
                                                      fileExtension:kJSQSystemSoundTypeAIFF
                                                         completion:nil];
}

- (IBAction)btn_NOTIFY:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}

- (IBAction)btn_CREATE_NEW:(id)sender {
    ProjectVC* next = [self.storyboard instantiateViewControllerWithIdentifier:@"ProjectVC"];
    [self.navigationController pushViewController:next animated:YES];
    
}



@end
