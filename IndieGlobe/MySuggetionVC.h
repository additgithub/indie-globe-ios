//
//  MySuggetionVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 8/11/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MySuggetionVC : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *txt_suggetion;
@property (weak, nonatomic) IBOutlet UIButton *submit_button;
@property (weak, nonatomic) IBOutlet UIButton *cancel_button;





- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_cancel:(id)sender;
- (IBAction)btn_submit:(id)sender;



@end
