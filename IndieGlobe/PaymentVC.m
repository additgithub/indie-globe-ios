//
//  PaymentVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/22/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "PaymentVC.h"
#import "UIColor+CL.h"
#import "SettingVC.h"
#import "RegisterVC.h"
#import "LTHMonthYearPickerView.h"
#import "IQKeyboardManager.h"


#define REGEX_USER_NAME_LIMIT @"^.{3,10}$"
#define REGEX_USER_NAME @"[A-Za-z0-9]{3,10}"
#define REGEX_EMAIL @"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
#define REGEX_PASSWORD_LIMIT @"^.{6,20}$"
#define REGEX_PASSWORD @"[A-Za-z0-9]{6,20}"
#define REGEX_PHONE_DEFAULT @"[0-9]{4}\\ [0-9]{4}\\ [0-9]{4}\\ [0-9]{4}"

@interface PaymentVC ()


@end

@implementation PaymentVC
{
    BOOL isCheck;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    //[self setupAlerts];
 
    isCheck=false;
    // Do any additional setup after loading the view.
    _txt_nameoncard.placeholder=@"Name on card";
    _txt_cardnumber.placeholder=@"**** **** **** ****";
    _txt_expirydate.placeholder=@"12/12";
    _txt_cvv.placeholder=@"***";
   
    
    [self textlayers:_txt_nameoncard];
    [self textlayers:_txt_cardnumber];
    [self textlayers:_txt_expirydate];
    [self textlayers:_txt_cvv];
    
    _btn_paynow.layer.cornerRadius=_btn_paynow.frame.size.height/2;
    _btn_back_btn.layer.cornerRadius=_btn_back_btn.frame.size.height/2;
    
    _txt_cardnumber.delegate=self;
    _txt_cvv.delegate=self;
    
    

    

    [self DatePicker];
    
}
-(void)DatePicker
{
    /////////////////////////////DateSelector
    

    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"mm / yyyy"];
    
    NSDate *minDate = [dateFormatter dateFromString:[NSString stringWithFormat: @"%i / %i", 3, 2014]];
    NSDate *maxDate = [dateFormatter dateFromString:[NSString stringWithFormat: @"%i / %i", 3, 2115]];
    
    
    monthYearPicker = [[LTHMonthYearPickerView alloc] initWithDate: [NSDate date]
                                                       shortMonths: NO
                                                    numberedMonths: YES
                                                        andToolbar: YES
                                                           minDate: [NSDate date]
                                                        andMaxDate: maxDate];
    
    monthYearPicker.delegate = self;
    _txt_expirydate.inputView = monthYearPicker;
}
#pragma mark - LTHMonthYearPickerView Delegate
- (void)pickerDidPressCancelWithInitialValues:(NSDictionary *)initialValues {
    _txt_expirydate.text = [NSString stringWithFormat:
                           @"%@ / %@",
                           initialValues[@"month"],
                           initialValues[@"year"]];
    [_txt_expirydate resignFirstResponder];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
}


- (void)pickerDidPressDoneWithMonth:(NSString *)month andYear:(NSString *)year {
    _txt_expirydate.text = [NSString stringWithFormat: @"%@ / %@", month, year];
    [_txt_expirydate resignFirstResponder];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
}


- (void)pickerDidPressCancel {
    [_txt_expirydate resignFirstResponder];
}


- (void)pickerDidSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    NSLog(@"row: %zd in component: %zd", row, component);
}


- (void)pickerDidSelectMonth:(NSString *)month {
    NSLog(@"month: %@ ", month);
}


- (void)pickerDidSelectYear:(NSString *)year {
    NSLog(@"year: %@ ", year);
}


- (void)pickerDidSelectMonth:(NSString *)month andYear:(NSString *)year {
    _txt_expirydate.text = [NSString stringWithFormat: @"%@ / %@", month, year];
}







-(void)viewWillAppear:(BOOL)animated
{
    if (_FromPayment==1) {
        [self viewDidLoad];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setupAlerts{
    [_txt_nameoncard addRegx:REGEX_USER_NAME_LIMIT withMsg:@"First name charaters limit should be come between 3-10"];
    
    [_txt_cardnumber addRegx:REGEX_PHONE_DEFAULT withMsg:@"Enter valid card number"];
    
    [_txt_expirydate addRegx:REGEX_USER_NAME withMsg:@"enter valid expiry"];
    
    [_txt_cvv addRegx:REGEX_USER_NAME withMsg:@"Enter valid CVV."];
    
    
}
#pragma Mark - TextChange
- (IBAction)cardnumberchanged:(id)sender {
    
    NSLog(@"%lu",   (unsigned long)_txt_cardnumber.text.length);
    
    if ((_txt_cardnumber.text.length == 4 || _txt_cardnumber.text.length == 9 || _txt_cardnumber.text.length == 14) && _txt_cardnumber.text.length >oldLength) {
        _txt_cardnumber.text = [NSString stringWithFormat:@"%@ ",_txt_cardnumber.text];
    }else{
        if (_txt_cardnumber.text.length == 4 || _txt_cardnumber.text.length == 9 || _txt_cardnumber.text.length == 14) {
            _txt_cardnumber.text = [NSString stringWithFormat:@"%@",[_txt_cardnumber.text substringToIndex:_txt_cardnumber.text.length-1]];
        }
    }
    
    oldLength = _txt_cardnumber.text.length;
}


- (BOOL)textField:(UITextField* )textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString* )string {
    if (textField == _txt_cardnumber) {
        //NSString *resultText = [textField.text stringByReplacingCharactersInRange:range
        //withString:string];
        //return resultText.length <= 16;
        
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        NSUInteger MAXLENGTH;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        if (textField == _txt_cardnumber) {
            MAXLENGTH = 19;
        }
        else
        {
            MAXLENGTH=300;
        }
        
        return newLength <= MAXLENGTH || returnKey;
    }
    if (textField == _txt_cvv) {
        NSString *resultText = [textField.text stringByReplacingCharactersInRange:range
                                                                       withString:string];
        return resultText.length <= 3;
    }
    
    return YES;
}
-(void)textlayers :(UITextField *)UITextField
{
    
    UITextField.layer.borderWidth=1.5;
    UITextField.layer.borderColor=[UIColor colorWithHex:0x17C4C7].CGColor;
    CGRect frameRect = UITextField.frame;
    frameRect.size.height = 30;
    UITextField.frame = frameRect;
    UITextField.layer.cornerRadius=UITextField.frame.size.height/2;
    
    [UITextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
    UITextField.leftView = paddingView;
    UITextField.leftViewMode = UITextFieldViewModeAlways;

    
}

#pragma mark - GetAllPosts
-(void)CallMyMethod
{
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    [AddPost setValue:@"Admin" forKey:@"email"];
    [AddPost setValue:@"Admin" forKey:@"password"];
    
    NSLog(@" url is %@login",JsonUrlConstant);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httppayment  = [[HttpWrapper alloc] init];
        httppayment.delegate=self;
        httppayment.getbool=NO;
        [httppayment requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@login",JsonUrlConstant] param:[AddPost copy]];
    });
}

#pragma mark- Wrapper Class Delegate Methods
//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary* )dicsResponse
{
    if(wrapper == httppayment && httppayment != nil)
    {
        
    }
}


//fetchDataFail
- (void) HttpWrapper:(HttpWrapper* )wrapper fetchDataFail:(NSError* )error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_PAY_NOW:(id)sender {
    
    if([_txt_nameoncard validate] & [_txt_cardnumber validate] & [_txt_expirydate validate] & [_txt_cvv validate]){
        /* secondViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SecondViewController"];
         [self.navigationController pushViewController:secondViewController animated:YES];*/
    }

}

- (IBAction)btn_BACK:(id)sender {
    //[self.navigationController popViewControllerAnimated:YES];
       
    
    if (_ButtonFlag==2 || _ButtonFlag==3) {
        RegisterVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterVC"];
        [self.navigationController pushViewController:next animated:YES];
       
    }
    else
    {
        SettingVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingVC"];
        [self.navigationController pushViewController:next animated:YES];
    }
    

}

- (IBAction)btn_BACK_ACTION:(id)sender {
    
    if (_ButtonFlag==2 || _ButtonFlag==3) {
        RegisterVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterVC"];
        [self.navigationController pushViewController:next animated:NO];
    }
    else
    {
        SettingVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingVC"];
        [self.navigationController pushViewController:next animated:NO];
    }

}

- (IBAction)btn_CHECKBOX:(id)sender {
    if (isCheck) {
        
        isCheck =false;
        [_img_checkbox setImage:[UIImage imageNamed:@"unchecked-box1.png"]];
        
        
        
    }else{
        
        isCheck = true;
        [_img_checkbox setImage:[UIImage imageNamed:@"checked-box1.png"]];
        
    }

}
@end
