//
//  Created by Jesse Squires
//  htps://www.jessesquires.com
//
//
//  Documentation
//  htps://cocoadocs.org/docsets/JSQMessagesViewController
//
//
//  GitHub
//  https://github.com/jessesquires/JSQMessagesViewController
//
//
//  License
//  Copyright (c) 2014 Jesse Squires
//  Released under an MIT license: htps://opensource.org/licenses/MIT
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "ChatListVC.h"

@interface TableViewController : UITableViewController <JSQDemoViewControllerDelegate>

- (IBAction)unwindSegue:(UIStoryboardSegue *)sender;

@end
