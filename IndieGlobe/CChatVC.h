//
//  CChatVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 6/29/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "HttpWrapper.h"
#import "ApplicationConstant.h"

@interface CChatVC : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *arrChatList;
}

@property (retain , nonatomic)NSString *critiID;
@property (retain , nonatomic)NSString *isFrom;
@property (retain , nonatomic)NSMutableDictionary *mutDict;

//Outlet
@property (weak, nonatomic) IBOutlet UITableView *tbl_chatlist;




//Action

- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_HOME:(id)sender;




@end
