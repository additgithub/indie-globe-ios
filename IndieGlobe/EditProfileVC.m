//
//  EditProfileVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/16/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "EditProfileVC.h"
#import "UIColor+CL.h"
#import "TextFieldValidator.h"
#import "WYPopoverController.h"
#import "PopOverView.h"
#import "TextFieldValidator.h"
#import "PaymentVC.h"
#import "LoginVC.h"
#import "PlatinumInfoVC.h"
#import "ApplicationConstant.h"
#import "HcdDateTimePickerView.h"



#define REGEX_USER_NAME_LIMIT @"^.{3,10}$"
#define REGEX_USER_NAME @"[A-Za-z0-9]{3,10}"
#define REGEX_EMAIL @"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
#define REGEX_PASSWORD_LIMIT @"^.{6,20}$"
#define REGEX_PASSWORD @"[A-Za-z0-9]{6,20}"
#define REGEX_PHONE_DEFAULT @"[0-9]{3}\\-[0-9]{3}\\-[0-9]{4}"
#define REGEX_FNAME @"[A-Za-z]{1,}"


@interface EditProfileVC ()<WYPopoverControllerDelegate>{
    WYPopoverController* popoverController;
    HcdDateTimePickerView * dateTimePickerView;
}


@end

@implementation EditProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    [self setupAlerts];
    
    Default=0;


    // Do any additional setup after loading the view.
    //TEXT FIELD
    [self CustomizeTextField:_txt_fname];
    _txt_fname.placeholder=@"First name";
    [self CustomizeTextField:_txt_lname];
    _txt_lname.placeholder=@"Last Name";
    [self CustomizeTextField:_txt_email];
    _txt_email.placeholder=@"abc@aa.cc";
    _txt_email.userInteractionEnabled=NO;
    [self CustomizeTextField:_txt_short_bio];
    _txt_short_bio.placeholder=@"Short Bio";
    
    [self CustomizeTextField:_txt_cate];
    _txt_cate.placeholder=@"Select Category";
    [self CustomizeTextField:txt_country];
    txt_country.placeholder=@"select country";
    [self CustomizeTextField:txt_state];
    txt_state.placeholder=@"Select State";
    [self CustomizeTextField:txt_city];
    txt_city.placeholder=@"Select City";
    
    [self CustomizeTextField:txt_gender];
    txt_gender.placeholder=@"Select Gender";
    [self CustomizeTextField:_txt_bod];
    _txt_bod.placeholder=@"Select Date Of Birth";
    
    
    txt_country.userInteractionEnabled=NO;
    txt_state.userInteractionEnabled=NO;
    txt_city.userInteractionEnabled=NO;
    
    
    
    _btn_save_prof.layer.cornerRadius=_btn_save_prof.frame.size.height/2;
   
    _btn_cancel_prof.layer.cornerRadius=_btn_save_prof.frame.size.height/2;
   
    
    _img_profile.layer.cornerRadius=_img_profile.frame.size.height/2;
    _img_profile.layer.borderWidth=2;
    _img_profile.layer.borderColor=[UIColor colorWithHex:0x17C4C7].CGColor;
    
    
    //arryList=@[@"ACTOR",@"SINGER",@"DIRECTOR",@"SCREEN WRITER",@"MANAGER",@"M D"];
    
    NSLog(@"---%@",_responcedic);
    
    dictres=[_responcedic valueForKey:@"data"];
    _txt_fname.text=[dictres valueForKey:@"FName"];
    _txt_lname.text=[dictres valueForKey:@"LName"];
    _txt_email.text=[dictres valueForKey:@"Email"];
    _txt_short_bio.text=[dictres valueForKey:@"ShortDesc"];
    
    txt_country.text=[dictres valueForKey:@"CountryName"];
    txt_state.text=[dictres valueForKey:@"StateName"];
    txt_city.text=[dictres valueForKey:@"CityName"];
    
    countryid=[dictres valueForKey:@"CountryID"];
    stateid=[dictres valueForKey:@"StateID"];
    cityid=[dictres valueForKey:@"CityID"];
    
    txt_gender.text=[dictres valueForKey:@"Gender"];
    dateofb=[dictres valueForKey:@"BirthDate"];
    //--------DATE CONVERT------------------
    NSString *strdt=[[NSString alloc]init];
    strdt=[dictres valueForKey:@"BirthDate"];
    NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
    [formatedt setDateFormat:@"yyyy-MM-dd"];
    NSDate *fulldate=[formatedt dateFromString:strdt];
    [formatedt setDateFormat:@"MM-dd-yyyy"];
    NSString *finaldt=[formatedt stringFromDate:fulldate];
    _txt_bod.text=[NSString stringWithFormat:@"%@",finaldt];
    
    
    
    
    [_img_profile sd_setImageWithURL:[NSURL URLWithString:[dictres valueForKey:@"ImageURL"]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
    
    NSURL * url = [NSURL URLWithString:[dictres valueForKey:@"ImageURL"]];
    NSData * data = [NSData dataWithContentsOfURL:url];
    select_image= [UIImage imageWithData:data];
    
    
    
    
    NSArray *arrT=[dictres valueForKey:@"ProfessionData"];
    
    _txt_cate.text=[arrT componentsJoinedByString:@","];
    NSMutableArray *NewArr = [[NSMutableArray alloc] init];
    for (int i=0; i<arrT.count; i++) {
        _txt_cate.text=[arrT componentsJoinedByString:@","];
        [NewArr addObject:[[arrT objectAtIndex:i] valueForKey:@"Profession"]];
    }
    _txt_cate.text=[NewArr componentsJoinedByString:@","];
    
    NSMutableArray *NewArrID = [[NSMutableArray alloc] init];
    for (int i=0; i<arrT.count; i++) {
        [NewArrID addObject:[[arrT objectAtIndex:i] valueForKey:@"ProfessionalID"]];
    }
    NSLog(@"my arr %@",NewArrID);
    
    ProfessionalID = [NewArrID componentsJoinedByString:@","];
    
    NSLog(@"ProfessionalID %@",ProfessionalID);
    
    _txt_short_bio.delegate=self;

    
    
    
    [self CallProfession];
    [self CallContry];
    [self makeGender];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)makeGender
{
    arrgender= [[NSMutableArray alloc]initWithObjects:@"Male",@"Female",nil];
    
    NSLog(@"ARR --- %@",arrgender);
}

-(void)CustomizeTextField:(UITextField *)myTextField
{
    myTextField.layer.borderWidth=1;
    myTextField.layer.borderColor=[UIColor colorWithHex:0x17C4C7].CGColor;
    CGRect frameRect = myTextField.frame;
    frameRect.size.height = 30;
    myTextField.frame = frameRect;
    myTextField.layer.cornerRadius=myTextField.frame.size.height/2;
    
    [myTextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
    myTextField.leftView = paddingView;
    myTextField.leftViewMode = UITextFieldViewModeAlways;
  /*
    myTextField.layer.borderColor=[[UIColor clearColor]CGColor];
    myTextField.layer.borderWidth=1;
    myTextField.layer.borderColor=[[UIColor whiteColor] CGColor];
    myTextField.layer.cornerRadius=15.0;
     
    [UITextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
    myTextField.leftView = paddingView;
    myTextField.leftViewMode = UITextFieldViewModeAlways;*/
    
}


-(void)setupAlerts{
    
    [_txt_fname addRegx:REGEX_FNAME withMsg:@"Only A-Z Allow."];
    [_txt_lname addRegx:REGEX_FNAME withMsg:@"Only A-Z Allow."];
    
    
}

#pragma mark - GetAllPosts
-(void)CallEditProfile
{
   [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:_txt_fname.text forKey:@"first_name"];
    [AddPost setValue:_txt_lname.text forKey:@"last_name"];
    [AddPost setValue:_txt_email.text forKey:@"email"];
    [AddPost setValue:_txt_short_bio.text forKey:@"short_desc"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",countryid] forKey:@"country"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",stateid] forKey:@"state"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",cityid] forKey:@"city"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",ProfessionalID] forKey:@"profession_ids"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",dateofb] forKey:@"birth_date"];
    [AddPost setValue:txt_gender.text forKey:@"gender"];
    
   
    
    
    if (self.img_profile.image>0) {
        //Create manager
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
        //Now post
        NSLog(@"AddPost %@",AddPost);
        NSString *url = [NSString stringWithFormat:@"%@edit_profile",JsonUrlConstant];
        [manager POST:url parameters:[AddPost copy] constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            
            //add img data one by one
            
            NSData *imageData = UIImageJPEGRepresentation(self.img_profile.image,0.5);
            [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"user_image"] fileName:[NSString stringWithFormat:@"user_image.jpg"] mimeType:@"image/jpeg"];
            
        } success:^(AFHTTPRequestOperation *operation, id dicsResponse) {
           // [(AppDelegate *)[[UIApplication sharedApplication] delegate] hideProgress];
            
            NSLog(@"Success: %@", dicsResponse);
            NSLog(@"Edit profile---%@",dicsResponse);
            NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            
            if ([[dicsResponse valueForKey:@"status"] integerValue] == 0) {
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                //[alert show];
                
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                //[alert show];
                [self.navigationController popViewControllerAnimated:YES];
                
            }

            [APP_DELEGATE hideLoadingView];
        }
        failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
            [APP_DELEGATE hideLoadingView];
         }];
    }
    else{
        NSLog(@" url is %@login",JsonUrlConstant);
        [AddPost setObject:@"" forKey:@"user_image"];
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
            httpeditprof  = [[HttpWrapper alloc] init];
            httpeditprof.delegate=self;
            httpeditprof.getbool=NO;
            [httpeditprof requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@edit_profile",JsonUrlConstant] param:[AddPost copy]];
        });
    }

    
    

}

-(void)CallProfession
{
    [APP_DELEGATE showLoadingView:@""];
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpprofes  = [[HttpWrapper alloc] init];
        httpprofes.delegate=self;
        httpprofes.getbool=NO;
        [httpprofes requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_profession"
                                                   ,JsonUrlConstant] param:nil];
    });
    
}
-(void)CallContry
{
    [APP_DELEGATE showLoadingView:@""];
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httcontry  = [[HttpWrapper alloc] init];
        httcontry.delegate=self;
        httcontry.getbool=NO;
        [httcontry requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_country"
                                                  ,JsonUrlConstant] param:nil];
    });
    
}
-(void)Callstate:(NSString *)str
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue:str forKey:@"country_id"];
    NSLog(@"%@",AddPost);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpstate  = [[HttpWrapper alloc] init];
        httpstate.delegate=self;
        httpstate.getbool=NO;
        [httpstate requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_state"
                                                  ,JsonUrlConstant] param:[AddPost copy]];
    });
    
}

-(void)CallCity:(NSString *)str
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue:str forKey:@"state_id"];
    NSLog(@"%@",AddPost);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpcity  = [[HttpWrapper alloc] init];
        httpcity.delegate=self;
        httpcity.getbool=NO;
        [httpcity requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_city"
                                                 ,JsonUrlConstant] param:[AddPost copy]];
    });
    
}


#pragma mark- Wrapper Class Delegate Methods

//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary* )dicsResponse
{
    if(wrapper == httpeditprof && httpeditprof != nil)
    {
        NSLog(@"Edit profile---%@",dicsResponse);
        NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
        
        if ([[dicsResponse valueForKey:@"status"] integerValue] == 0) {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //[alert show];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //[alert show];
            [self.navigationController popViewControllerAnimated:YES];

        }
        [APP_DELEGATE hideLoadingView];
    }
    else if (wrapper == httpprofes && httpprofes != nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        if ([dicsResponse valueForKey:@"status"]!=0) {
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[dicsResponse valueForKey:@"data"];
            arryList= [dicsResponse valueForKey:@"data"];
            NSLog(@"%@",arryList);
        }
        [APP_DELEGATE hideLoadingView];
    }
    else if (wrapper==httcontry && httcontry !=nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        dict=[dicsResponse valueForKey:@"data"];
        country= [dicsResponse valueForKey:@"data"];
        NSLog(@"%@",country);
        
        ///===============================
        if (Default==0) {
            NSString *stringToSearch = [dictres valueForKey:@"CountryID"];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
            
            NSArray *results = [country filteredArrayUsingPredicate:predicate];
            NSLog(@"%@",[results valueForKey:@"LocationName"]);
            txt_country.text = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationName"] objectAtIndex:0]];
            countryid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
            
            [self Callstate:countryid];
        }
        [APP_DELEGATE hideLoadingView];

        
        
        
    }
    else if (wrapper==httpstate && httpstate !=nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        if ([dicsResponse valueForKey:@"status"]!=0) {
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[dicsResponse valueForKey:@"data"];
            state= [dicsResponse valueForKey:@"data"];
            NSLog(@"%@",state);
        }
        //////-------------------
        
        if (Default==0) {
            NSString *stringToSearch = [dictres valueForKey:@"StateID"];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
            
            NSArray *results = [state filteredArrayUsingPredicate:predicate];
            NSLog(@"%@",[results valueForKey:@"LocationName"]);
            txt_state.text = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationName"] objectAtIndex:0]];
            stateid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
            
            [self CallCity:stateid];
        }
        [APP_DELEGATE hideLoadingView];

    }
    else if (wrapper==httpcity && httpcity !=nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        if ([dicsResponse valueForKey:@"status"]!=0) {
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[dicsResponse valueForKey:@"data"];
            arrcity= [dicsResponse valueForKey:@"data"];
            NSLog(@"%@",arrcity);
        }
        /////////////------------------------
        
        if (Default==0) {
            Default=1;
            NSString *stringToSearch = [dictres valueForKey:@"CityID"];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
            
            NSArray *results = [arrcity filteredArrayUsingPredicate:predicate];
            NSLog(@"%@",[results valueForKey:@"LocationID"]);
            txt_city.text = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationName"] objectAtIndex:0]];
            cityid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        }

    }
    [APP_DELEGATE hideLoadingView];

}


//fetchDataFail
- (void) HttpWrapper:(HttpWrapper* )wrapper fetchDataFail:(NSError* )error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Prevent crashing undo bug – see note below.
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    
    if (newLength <= 160) {
        
        return YES;
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"You can add only 160 characters"
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"OK",nil];
        [alert show];
        return NO;
    }
    
    
    
    
    return newLength <= 25;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_BACK:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];

}


- (IBAction)btn_SAVEPROF:(id)sender {
    if ([_txt_fname validate] &&[_txt_lname validate] &&[_txt_email validate] &&[_txt_cate validate] && [_txt_short_bio validate] )
    {
        NSLog(@"Validation is OK.....GO AHAD");
        
        [self CallEditProfile];
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Enter valid data." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    
    
}

- (IBAction)btn_CHOOSE_IMG:(id)sender {
    ipc= [[UIImagePickerController alloc] init];
    ipc.delegate = self;
    ipc.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    ipc.allowsEditing = NO;
    [self presentViewController:ipc animated:YES completion:nil];
}

- (IBAction)btn_SPROFESSION:(id)sender {
    
   // [self showPopUpWithTitle:@"Select Profession" withOption:arryList xy:CGPointMake(35, 58) size:CGSizeMake(300, 330) isMultiple:YES];
    
    NSMutableArray *arrteam=[[NSMutableArray alloc] init];
    
    for (int i=0; i<arryList.count; i++) {
        //arrteam=[[arryList objectAtIndex:i] valueForKey:@"Profession"];
        [arrteam addObject:[[arryList objectAtIndex:i] valueForKey:@"Profession"]];
    }
    
    [Dropobj fadeOut];
    [self showPopUpWithTitle:@"Select Profession" withOption:arrteam xy:CGPointMake(35, 58) size:CGSizeMake(300, 330) isMultiple:YES];
    
}

- (IBAction)btn_SCOUNTRY:(id)sender {
    isCheck=0;
    [txt_country setTag:0];
    currentTextField = txt_country;
    [self showPopover:sender];
}

- (IBAction)btn_SSTATE:(id)sender {
    isCheck=1;
    [txt_state setTag:1];
    currentTextField = txt_state;
    [self showPopover:sender];
}

- (IBAction)btn_SCITY:(id)sender {
    isCheck=2;
    [txt_city setTag:2];
    currentTextField = txt_city;
    [self showPopover:sender];
}

- (IBAction)btn_CANCLE:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)btn_SGENDER:(id)sender {
   
    isCheck=5;
    [txt_gender setTag:5];
    currentTextField = txt_gender;
    [self showPopover:sender];

}

- (IBAction)btn_SBOD:(id)sender {
    
 
    
     __block EditProfileVC *weakSelf = self;
    dateTimePickerView = [[HcdDateTimePickerView alloc] initWithDatePickerMode:DatePickerDateMode defaultDateTime:[[NSDate alloc]initWithTimeIntervalSinceNow:1000]];
    [dateTimePickerView setMinYear:1900];
    [dateTimePickerView setMaxYear:2018];
    
    dateTimePickerView.topViewColor = [UIColor darkGrayColor];
    dateTimePickerView.buttonTitleColor = [UIColor whiteColor];
    
    dateTimePickerView.clickedOkBtn = ^(NSString * datetimeStr){
        NSLog(@"%@", datetimeStr);
        dateofb=datetimeStr;
        
        //--------DATE CONVERT------------------
        NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
        [formatedt setDateFormat:@"yyyy-MM-dd"];
        NSDate *fulldate=[formatedt dateFromString:datetimeStr];
        [formatedt setDateFormat:@"MM-dd-yyyy"];
        NSString *finaldt=[formatedt stringFromDate:fulldate];
        
        weakSelf.txt_bod.text = finaldt;
    };

    if (dateTimePickerView) {
        [self.view addSubview:dateTimePickerView];
        [dateTimePickerView showHcdDateTimePicker];
    }

    
}
#pragma POPUP_COUNTRY
- (void)showPopover:(UIView*)btn;
{
    
    if (popoverController == nil)
    {
        PopOverView *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PopOverView"];
        
        settingsViewController.index = currentTextField.tag;
        
        if (isCheck == 0) {
            settingsViewController.marrcountry = country;
        }
        else if (isCheck==1)
        {
            settingsViewController.marrstate = state;
        }
        else if (isCheck==2)
        {
            settingsViewController.marrcity = arrcity;
        }else if (isCheck==5)
        {
            settingsViewController.marrgender = arrgender;
        }
        else{
            NSLog(@"NO DATA FROM API");
        }
        
        
        
        if ([settingsViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
            settingsViewController.preferredContentSize = CGSizeMake(200, 170);             // iOS 7
        }
        else {
#pragma clang diagnostic pushc
#pragma GCC diagnostic ignored "-Wdeprecated"
            settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 170);      // iOS < 7
#pragma clang diagnostic pop
        }
        settingsViewController.delegates = self;
        
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        
        settingsViewController.modalInPopover = NO;
        popoverController = [[WYPopoverController alloc] initWithContentViewController:settingsViewController];
        
        popoverController.delegate = self;
        
        popoverController.passthroughViews = @[btn];
        popoverController.popoverLayoutMargins = UIEdgeInsetsMake(0, 10, 10 , 10);
        popoverController.wantsDefaultContentAppearance = YES;
        
        [popoverController presentPopoverFromRect:btn.bounds
                                           inView:btn
                         permittedArrowDirections:WYPopoverArrowDirectionAny
                                         animated:YES
                                          options:WYPopoverAnimationOptionFadeWithScale];
    }
    
}

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    popoverController.delegate = nil;
    popoverController = nil;
}

-(void)dismissPopUpViewController
{
    [popoverController dismissPopoverAnimated:YES];
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)Popvalueselected:(NSString*)strValue{
    NSLog(@"POP:%@",strValue);
    
    currentTextField.text = strValue;
    
    if (isCheck==0) {
        
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [country filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        countryid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        
        txt_state.text=@"";
        txt_city.text=@"";
        
        [self Callstate:countryid];
        
    }
    else if (isCheck==1)
    {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [state filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        stateid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        
        txt_city.text=@"";
        
        [self CallCity:stateid];
    }
    else if (isCheck==2)
    {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [arrcity filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        cityid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        
    }
    else if (isCheck==5)
    {
        currentTextField.text = strValue;
    }
    else
    {
        currentTextField.text = strValue;
    }
    
    
    
}




-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple{
    
    
    Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
    Dropobj.delegate = self;
    [Dropobj showInView:self.view animated:YES];
    
    /*----------------Set DropDown backGroundColor-----------------*/
    [Dropobj SetBackGroundDropDown_R:1.0 G:181.0 B:169.0 alpha:0.70];
    
}
- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
    /*----------------Get Selected Value[Single selection]-----------------*/
    _txt_cate.text=[arryList objectAtIndex:anIndex];
}
- (void)DropDownListView:(DropDownListView *)dropdownListView Datalist:(NSMutableArray*)ArryData{
    
    /*----------------Get Selected Value[Multiple selection]-----------------*/
    if (ArryData.count>0) {
        _txt_cate.text=[ArryData componentsJoinedByString:@","];
        NSMutableArray *NewArr = [[NSMutableArray alloc] init];
        NSMutableArray *myarr = [[NSMutableArray alloc] init];
        
        for (int i=0; i<arryList.count; i++) {
            [NewArr addObject:[[arryList objectAtIndex:i] valueForKey:@"Profession"]];
        }
        
        for (int i=0; i<ArryData.count; i++) {
            NSUInteger index = [NewArr indexOfObject:ArryData[i]];
            NSLog(@"index %lu",(unsigned long)index);
            [myarr addObject:[[arryList objectAtIndex:index] valueForKey:@"ProfessionalID"]];
        }
        NSLog(@"my arr %@",myarr);
        
        ProfessionalID = [myarr componentsJoinedByString:@","];
        
        NSLog(@"ProfessionalID %@",ProfessionalID);
        //CGSize size=[self GetHeightDyanamic:_txt_profession];
        //_txt_profession.frame=CGRectMake(16, 240, 287, size.height);
    }
    else{
        _txt_cate.text=@"";
    }
    
}
- (void)DropDownListViewDidCancel{
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    [self.view endEditing:YES];

    if ([touch.view isKindOfClass:[UIView class]]) {
        [Dropobj fadeOut];
    }
}


-(CGSize)GetHeightDyanamic:(UILabel*)lbl
{
    NSRange range = NSMakeRange(0, [lbl.text length]);
    CGSize constraint;
    constraint= CGSizeMake(288 ,MAXFLOAT);
    CGSize size;
    
    if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)) {
        NSDictionary *attributes = [lbl.attributedText attributesAtIndex:0 effectiveRange:&range];
        CGSize boundingBox = [lbl.text boundingRectWithSize:constraint options: NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
        
        size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    }
    else{
        
        
        size = [lbl.text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    }
    return size;
}

#pragma mark - ImagePickerController Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    // Dismiss the image selection, hide the picker and
    
    //show the image view with the picked image
    
//    UIImage* originalImage = nil;
//    originalImage = [info objectForKey:UIImagePickerControllerOriginalImage];
//    if(originalImage==nil)
//    {
//        originalImage = [info objectForKey:UIImagePickerControllerOriginalImage];
//    }
//    if(originalImage==nil)
//    {
//        originalImage = [info objectForKey:UIImagePickerControllerOriginalImage];
//    }
    
   // image = originalImage;
    

    //[ipc dismissViewControllerAnimated:YES completion:nil];
    //self.img_profile.image = originalImage;
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    NSLog(@"original Image Size : %@", NSStringFromCGSize(image.size));
    
    
    _imgCropperViewController = [[UzysImageCropperViewController alloc] initWithImage:image andframeSize:picker.view.frame.size andcropSize:CGSizeMake(320, 320)];
    _imgCropperViewController.delegate = self;
    [picker presentViewController:_imgCropperViewController animated:YES completion:nil];
    [_imgCropperViewController release];
    
}
- (void)imageCropper:(UzysImageCropperViewController *)cropper didFinishCroppingWithImage:(UIImage *)image
{
    self.img_profile.image=image;
   
    [self dismissViewControllerAnimated:YES completion:nil];
}



-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imageCropperDidCancel:(UzysImageCropperViewController *)cropper
{
    [self dismissViewControllerAnimated:YES completion:nil];
    // [_picker dismissViewControllerAnimated:YES completion:nil];
}
- (void)dealloc
{
    [_picker release];
    [_resultImgView release];
    [_imgCropperViewController release];
    [super ah_dealloc];
}

- (void)viewDidUnload
{
    self.imgCropperViewController = nil;
    self.picker = nil;
    self.resultImgView = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

@end
