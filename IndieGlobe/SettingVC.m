//
//  SettingVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/16/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "SettingVC.h"
#import "UIColor+CL.h"
#import "DeshboardVC.h"
#import "PackageList.h"
#import "SWRevealViewController.h"
#import "DeshboardVC.h"
#import "Constants.h"
#import "DSCircularLayout.h"
#import "DSCollectionViewCell.h"
#import "circleVW.h"
#import "NewMenuDemoVC.h"
#import "UIColor+CL.h"

//---------------------
#import "DeshboardVC.h"
#import "ProfileVC.h"
#import "CritiquesVC.h"
#import "ClassifiedVC.h"
#import "PackageList.h"
#import "LearningVC.h"
#import "EventVC.h"
#import "TeamBuildVC.h"
#import "UIViewController+ENPopUp.h"
#import "MyEventVC.h"
#import "SettingVC.h"
#import "NewProfileVC.h"
#import "IndieVC.h"
#import "FundingVC.h"
#import "PlatinumInfoVC.h"
#import "TeamBuildVC.h"
#import "TeamBuild1VC.h"
#import "ChangePass.h"
#import "LoginVC.h"
#import "NewHomeVC.h"
#import "AllNotificationVC.h"


@interface SettingVC ()

@end

@implementation SettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    [self customSetup];
    // Do any additional setup after loading the view.
    
    [self CustomizeTextField:_txt_select_plan];
    //_txt_select_plan.placeholder=@"GOLD";
    
    _btn_back.layer.cornerRadius=_btn_back.frame.size.height/2;
    _btn_logout.layer.cornerRadius=_btn_logout.frame.size.height/2;
    _btn_chage_pas.layer.cornerRadius=_btn_chage_pas.frame.size.height/2;

    
    
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];

    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *emial=[standardUserDefaults valueForKey:@"Email"];
    _txt_select_plan.text=[NSString stringWithFormat:@"%@",[standardUserDefaults valueForKey:@"PlanName"]];
    _lbl_mailid.text=emial;
    
    
    //NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"Notification is %@",[standardUserDefaults valueForKey:@"notification_status"]);
    if ([[standardUserDefaults valueForKey:@"notification_status"]isEqualToString:@"0"]) {
        [_sw_notification setOn:NO animated:YES];
    }
    else
    {
        [_sw_notification setOn:YES animated:YES];
    }
    
    
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSString * build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    
    
    _lbl_version.text=[NSString stringWithFormat:@"Version & Build : %@ - %@",version,build];
    
    _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
    _lbl_notifi_count.clipsToBounds=YES;
    _lbl_notifi_count.text=theAppDelegate.n_Count;
    
    if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
        _lbl_notifi_count.hidden=YES;
    }
    else
    {
        _lbl_notifi_count.hidden=NO;
    }

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *emial=[standardUserDefaults valueForKey:@"Email"];
    _txt_select_plan.text=[NSString stringWithFormat:@"%@",[standardUserDefaults valueForKey:@"PlanName"]];
    [self customSetup];
}

-(void)viewWillAppear:(BOOL)animated
{
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *emial=[standardUserDefaults valueForKey:@"Email"];
    _txt_select_plan.text=[NSString stringWithFormat:@"%@",[standardUserDefaults valueForKey:@"PlanName"]];
    _lbl_mailid.text=emial;
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if (IsIphone7 || IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = 145;
    }
    else{
        self.revealViewController.rearViewRevealWidth = 145;
    }
}
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}



////////Diddeselect
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.selected) {
        // ... Uncheck
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}


-(void)CustomizeTextField:(UITextField *)myTextField
{
    myTextField.layer.borderWidth=1.5;
    myTextField.layer.borderColor=[UIColor colorWithHex:0x17C4C7].CGColor;
    CGRect frameRect = myTextField.frame;
    frameRect.size.height = 30;
    myTextField.frame = frameRect;
    myTextField.layer.cornerRadius=myTextField.frame.size.height/2;
    
    [myTextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
    myTextField.leftView = paddingView;
    myTextField.leftViewMode = UITextFieldViewModeAlways;
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_BACK:(id)sender {
    
    //[self.navigationController popViewControllerAnimated:YES];
    NewsVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsVC"];
    [self.navigationController pushViewController:next animated:YES];

}

- (IBAction)btn_BACK_ACTION:(id)sender {
    
    //[self.navigationController popViewControllerAnimated:YES];
    NewsVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsVC"];
    [self.navigationController pushViewController:next animated:YES];

}

- (IBAction)btn_CHOOSE_PLAN:(id)sender {
   /* UIViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PackageList"];
    vc.view.frame = CGRectMake(0, 0, 233.0f, 236.0f);
    [self presentPopUpViewController:vc];*/
    
    
    PackageList *homescreen = [self.storyboard instantiateViewControllerWithIdentifier:@"PackageList"];
    
    homescreen.view.frame = self.view.bounds;
    [homescreen.view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5f]];
    [self.view addSubview:homescreen.view];
    [self addChildViewController:homescreen];
    [homescreen didMoveToParentViewController:self];
    //

}

- (IBAction)btn_HOME:(id)sender {
    NewHomeVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:YES];
}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}


- (IBAction)btn_CHANGEPASS:(id)sender {
    ChangePass * next = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePass"];
    [self.navigationController pushViewController:next animated:YES];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        // Yes, do something
         //NSDictionary * myDictionary = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"dictionaryKey"];
        
        [[NSUserDefaults standardUserDefaults] setPersistentDomain:[NSDictionary dictionary] forName:[[NSBundle mainBundle] bundleIdentifier]];
        
        LoginVC* next = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        [self.navigationController pushViewController:next animated:YES];
        
        [NSUserDefaults resetStandardUserDefaults];
        [NSUserDefaults standardUserDefaults];
        
        
        
        
        [newRefrence setValue:@"0"];
        
        //dictionaryKey

    }
    else if (buttonIndex == 1)
    {
        // No, cancel etc...
        NSLog(@"Cancel logout");
    }
}
- (IBAction)btn_LOGOUT_ACTION:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setTitle:@"Alert"];
    [alert setMessage:@"Are you sure logout!"];
    [alert setDelegate:self];
    [alert addButtonWithTitle:@"Yes"];
    [alert addButtonWithTitle:@"No"];
    [alert show];
    
}

- (IBAction)sw_NOTIFI:(id)sender {
    UISwitch *mySwitch = (UISwitch *)sender;
    if ([mySwitch isOn]) {
        NSLog(@"its on!");
        //Post Method Request
        NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
        NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
        [AddPost setValue:@"1" forKey:@"status"];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
        manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
        [manager POST:[NSString stringWithFormat:@"%@update_notification_status",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
            [standardUserDefaults setValue:@"1" forKey:@"notification_status"];
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        
        }];

        
    } else {
        NSLog(@"its off!");
        //Post Method Request
        NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
        NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
        [AddPost setValue:@"0" forKey:@"status"];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
        manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
        [manager POST:[NSString stringWithFormat:@"%@update_notification_status",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
            [standardUserDefaults setValue:@"0" forKey:@"notification_status"];
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            
        }];

    }
}

- (IBAction)btn_NOTIFY:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}
@end
