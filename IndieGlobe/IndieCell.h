//
//  IndieCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/1/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShadowView.h"



@interface IndieCell : UITableViewCell
//Outlet
@property (strong, nonatomic) IBOutlet UIImageView *img_profile;
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;
@property (strong, nonatomic) IBOutlet UILabel *lbl_time;
@property (strong, nonatomic) IBOutlet UIImageView *img_play_bt;
@property (strong, nonatomic) IBOutlet UIButton *btn_play_v;
@property (weak, nonatomic) IBOutlet ShadowView *vw_shadow;
@property (weak, nonatomic) IBOutlet UILabel *lbl_commont_count;
@property (weak, nonatomic) IBOutlet UIButton *btn_comment;

//View
@property (weak, nonatomic) IBOutlet UIView *vw_1;
@property (weak, nonatomic) IBOutlet UIImageView *img_vw1;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title_vw1;
@property (weak, nonatomic) IBOutlet UILabel *lbl_desc_vw1;

@property (weak, nonatomic) IBOutlet UIView *vw_2;
@property (weak, nonatomic) IBOutlet UIImageView *img_vw2;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title_vw2;
@property (weak, nonatomic) IBOutlet UILabel *lbl_desc_vw2;

@property (weak, nonatomic) IBOutlet UILabel *lbl_like_count;
@property (weak, nonatomic) IBOutlet UIButton *btn_like;
@property (weak, nonatomic) IBOutlet UILabel *lbl_dislike_count;
@property (weak, nonatomic) IBOutlet UIButton *btn_dislike;




//Action



@end
