//
//  MyEventCell.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/7/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "MyEventCell.h"

@implementation MyEventCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _img_user_prof.clipsToBounds=YES;
    _img_user_prof.layer.cornerRadius=_img_user_prof.frame.size.height/2;
    
    [self.vw_shadow setShadow];
    
    _img_vw1.layer.cornerRadius=_img_vw1.layer.frame.size.height/2;
    _img_vw1.clipsToBounds=YES;
    
    _img_vw2.layer.cornerRadius=_img_vw2.layer.frame.size.height/2;
    _img_vw2.clipsToBounds=YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
