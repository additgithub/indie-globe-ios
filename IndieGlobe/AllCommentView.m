//
//  AllCommentView.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 10/22/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "AllCommentView.h"
#import "AllCommentViewCell.h"
#import "ApplicationConstant.h"
#import "UIImage+WebP.h"
#import "UIImageView+WebCache.h"
#import "NewHomeVC.h"
#import "IndieVC.h"



@interface AllCommentView ()

@end

@implementation AllCommentView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

    
    if ([_category_nm isEqualToString:@"Portfolio"]) {
        
        _lbl_header_title.text=@"Photo";
        [self CallGetComments];
    }
    else if ([_category_nm isEqualToString:@"IndiePick"])
    {
        _lbl_header_title.text=@"IndiePick";
        [self CallGetCommentsIndie];//Critiques
    }
    else if ([_category_nm isEqualToString:@"Critiques"])
    {
        _lbl_header_title.text=@"Critiques";
        [self CallGetCommentsCriti];//
    }
    else if ([_category_nm isEqualToString:@"Events"])
    {
        _lbl_header_title.text=@"Events";
        [self CallGetCommentsEvent];//Critiques
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Table View Data Source Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 2;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section==0) {
        return arrCommList.count;
    }
    else
    {
        return 1;
    }
    
  
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (indexPath.section == 0) {
        
        AllCommentViewCell *cell = (AllCommentViewCell *)[_tbl_comment dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell == nil)
        {
            NSArray *topLevelObject = [[NSBundle mainBundle] loadNibNamed:@"AllCommentViewCell" owner:nil options:nil];
            NSLog(@"%@",topLevelObject);
            cell = [topLevelObject objectAtIndex:0];
            NSLog(@"%@",cell);
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.lbl_nm.text=[NSString stringWithFormat:@"%@",[arrCommList valueForKey:@"FName"][indexPath.row]];
        cell.lbl_comm.text=[NSString stringWithFormat:@"%@",[arrCommList valueForKey:@"Comments"][indexPath.row]];
        
        [cell.img_prof sd_setImageWithURL:[NSURL URLWithString:[[arrCommList valueForKey:@"ImageURL"] objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"Avatar"]];

        
        
        return cell;
    }
    else if (indexPath.section == 1) {
        
        AllCommentViewCell *cell = (AllCommentViewCell *)[_tbl_comment dequeueReusableCellWithIdentifier:@"Cell2"];
        if (cell == nil)
        {
            NSArray *topLevelObject = [[NSBundle mainBundle] loadNibNamed:@"AllCommentViewCell" owner:nil options:nil];
            NSLog(@"%@",topLevelObject);
            cell = [topLevelObject objectAtIndex:1];
            NSLog(@"%@",cell);
        }
        
        NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
       
        
        [cell.img_prof2 sd_setImageWithURL:[NSURL URLWithString:[standardUserDefaults valueForKey:@"ImageURL"]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
        
        cell.btn_send.tag=indexPath.row;
        //NSString *strmsg=[NSString stringWithFormat:@"%@",cell.txt_comm.text];
        [cell.btn_send addTarget:self action:@selector(CallSend:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Change the selected background view of the cell.
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
   
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *myID=[standardUserDefaults valueForKey:@"UserID"];
    
   
        
        //[[_arrFollow objectAtIndex:indexPath]valueForKey:@"UserID"]
        
        if ([[[arrCommList objectAtIndex:indexPath.row]valueForKey:@"UserID"] intValue]==[myID intValue]) {
            NSLog(@"Same User");
            NewProfileVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewProfileVC"];
            [self.navigationController pushViewController:next animated:YES];
        }
        else
        {
            [self CallProfile:indexPath];
        }
    
}

-(void)CallSend:(id)sender
{
    
    if ([_category_nm isEqualToString:@"Portfolio"]) {
        [self.view endEditing:YES];
        [APP_DELEGATE showLoadingView:@""];
        
        
        NSLog(@"User Like %@",sender);
        //NSInteger ival=[sender integerValue];
        
        UIButton *btn = (UIButton *)sender;
        CGPoint origin = btn.frame.origin;
        CGPoint point = [btn.superview convertPoint:origin toView:_tbl_comment];
        NSIndexPath * indexPath = [_tbl_comment indexPathForRowAtPoint:point];
        
        AllCommentViewCell *cell=(AllCommentViewCell *)[_tbl_comment cellForRowAtIndexPath:indexPath];
        
        //Post Method Request
        NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
        NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];//
        [AddPost setValue:_user_portfolio_category_id forKey:@"user_portfolio_category_id"];//comment
        [AddPost setValue:cell.txt_comm.text forKey:@"comment"];
        
        cell.txt_comm.text=@"";
        
        //------------
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
        [manager POST:[NSString stringWithFormat:@"%@add_user_portfolio_comment",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            
            NSLog(@"---%@",responseObject);
            if ([[responseObject valueForKey:@"status"] intValue]==1) {
                
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                                message:@"Comment added successfully"
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                //[alert show];
                
                 [self CallGetComments];
                
                [APP_DELEGATE hideLoadingView];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                                message:@"Comment has not been successfully. kindly contact administrator"
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
            [APP_DELEGATE hideLoadingView];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [APP_DELEGATE hideLoadingView];
        }];
    }
    else if ([_category_nm isEqualToString:@"IndiePick"])
    {
        [self.view endEditing:YES];
        [APP_DELEGATE showLoadingView:@""];
        
        
        NSLog(@"User Like %@",sender);
        //NSInteger ival=[sender integerValue];
        
        UIButton *btn = (UIButton *)sender;
        CGPoint origin = btn.frame.origin;
        CGPoint point = [btn.superview convertPoint:origin toView:_tbl_comment];
        NSIndexPath * indexPath = [_tbl_comment indexPathForRowAtPoint:point];
        
        AllCommentViewCell *cell=(AllCommentViewCell *)[_tbl_comment cellForRowAtIndexPath:indexPath];
        
        //Post Method Request
        NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
        NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];//
        [AddPost setValue:_user_portfolio_category_id forKey:@"indie_pick_id"];//comment
        [AddPost setValue:cell.txt_comm.text forKey:@"comment"];
        
        cell.txt_comm.text=@"";
        
        //------------
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
        [manager POST:[NSString stringWithFormat:@"%@add_indie_picks_comment",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            
            NSLog(@"---%@",responseObject);
            if ([[responseObject valueForKey:@"status"] intValue]==1) {
                
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                                message:@"Comment added successfully"
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                //[alert show];
                [self CallGetCommentsIndie];
                
                [APP_DELEGATE hideLoadingView];
            }
            [APP_DELEGATE hideLoadingView];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [APP_DELEGATE hideLoadingView];
        }];
    }
    else if ([_category_nm isEqualToString:@"Critiques"])
    {
        [self.view endEditing:YES];
        [APP_DELEGATE showLoadingView:@""];
        
        
        NSLog(@"User Like %@",sender);
        //NSInteger ival=[sender integerValue];
        
        UIButton *btn = (UIButton *)sender;
        CGPoint origin = btn.frame.origin;
        CGPoint point = [btn.superview convertPoint:origin toView:_tbl_comment];
        NSIndexPath * indexPath = [_tbl_comment indexPathForRowAtPoint:point];
        
        AllCommentViewCell *cell=(AllCommentViewCell *)[_tbl_comment cellForRowAtIndexPath:indexPath];
        
        //Post Method Request
        NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
        NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];//
        [AddPost setValue:_user_portfolio_category_id forKey:@"critique_id"];//comment
        [AddPost setValue:cell.txt_comm.text forKey:@"comment"];
        
        cell.txt_comm.text=@"";
        
        //------------
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
        [manager POST:[NSString stringWithFormat:@"%@add_solution",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            
            NSLog(@"---%@",responseObject);
            if ([[responseObject valueForKey:@"status"] intValue]==1) {
                
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                                message:@"Comment added successfully"
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                //[alert show];
                
                [self CallGetCommentsCriti];
                
                [APP_DELEGATE hideLoadingView];
            }
            [APP_DELEGATE hideLoadingView];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [APP_DELEGATE hideLoadingView];
        }];
    }
    else if ([_category_nm isEqualToString:@"Events"])
    {
        [self.view endEditing:YES];
        [APP_DELEGATE showLoadingView:@""];
        
        
        NSLog(@"User Like %@",sender);
        //NSInteger ival=[sender integerValue];
        
        UIButton *btn = (UIButton *)sender;
        CGPoint origin = btn.frame.origin;
        CGPoint point = [btn.superview convertPoint:origin toView:_tbl_comment];
        NSIndexPath * indexPath = [_tbl_comment indexPathForRowAtPoint:point];
        
        AllCommentViewCell *cell=(AllCommentViewCell *)[_tbl_comment cellForRowAtIndexPath:indexPath];
        
        //Post Method Request
        NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
        NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];//
        [AddPost setValue:_user_portfolio_category_id forKey:@"event_id"];//comment
        [AddPost setValue:cell.txt_comm.text forKey:@"comment"];
        
        cell.txt_comm.text=@"";
        
        //------------
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
        [manager POST:[NSString stringWithFormat:@"%@add_event_comment",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            
            NSLog(@"---%@",responseObject);
            if ([[responseObject valueForKey:@"status"] intValue]==1) {
                
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                                message:@"Comment added successfully"
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                //[alert show];
                [self CallGetCommentsEvent];
                [APP_DELEGATE hideLoadingView];
            }
            [APP_DELEGATE hideLoadingView];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [APP_DELEGATE hideLoadingView];
        }];
    }
    
  
}

-(void)CallGetComments
{
    [self.view endEditing:YES];
    [APP_DELEGATE showLoadingView:@""];
    
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];//
    [AddPost setValue:_user_portfolio_category_id forKey:@"user_portfolio_category_id"];
    //------------
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@get_user_portfolio_comment_list",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSLog(@"---%@",responseObject);
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            arrCommList=[[NSMutableArray alloc]init];
            arrCommList=[responseObject valueForKey:@"data"];
            
            [_tbl_comment reloadData];
            
            //[self.navigationController pushViewController:next animated:YES];
            [APP_DELEGATE hideLoadingView];
        }
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
}

-(void)CallGetCommentsIndie
{
    [self.view endEditing:YES];
    [APP_DELEGATE showLoadingView:@""];
    
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];//
    [AddPost setValue:_user_portfolio_category_id forKey:@"indie_pick_id"];
    //------------
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@get_indie_picks_comment_list",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSLog(@"---%@",responseObject);
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            arrCommList=[[NSMutableArray alloc]init];
            arrCommList=[responseObject valueForKey:@"data"];
            
            [_tbl_comment reloadData];
            
            //[self.navigationController pushViewController:next animated:YES];
            [APP_DELEGATE hideLoadingView];
        }
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
}

-(void)CallGetCommentsCriti
{
    [self.view endEditing:YES];
    [APP_DELEGATE showLoadingView:@""];
    
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];//
    [AddPost setValue:_user_portfolio_category_id forKey:@"critique_id"];
    //------------
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@get_comment_list",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSLog(@"---%@",responseObject);
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            arrCommList=[[NSMutableArray alloc]init];
            arrCommList=[responseObject valueForKey:@"data"];
            
            [_tbl_comment reloadData];
            
            //[self.navigationController pushViewController:next animated:YES];
            [APP_DELEGATE hideLoadingView];
        }
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
}

-(void)CallGetCommentsEvent
{
    [self.view endEditing:YES];
    [APP_DELEGATE showLoadingView:@""];
    
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];//
    [AddPost setValue:_user_portfolio_category_id forKey:@"event_id"];
    //------------
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@get_event_comment_list",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSLog(@"---%@",responseObject);
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            arrCommList=[[NSMutableArray alloc]init];
            arrCommList=[responseObject valueForKey:@"data"];
            
            [_tbl_comment reloadData];
            
            //[self.navigationController pushViewController:next animated:YES];
            [APP_DELEGATE hideLoadingView];
        }
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
}

-(void)CallProfile :(NSIndexPath*)indexp
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    
 
    
        NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
        [AddPost setValue:[[arrCommList objectAtIndex:indexp.row]valueForKey:@"UserID"] forKey:@"profile_id"];
   
    //------------
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@user_profile",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        if ([responseObject valueForKey:@"status"]) {
            responseObject=[responseObject valueForKey:@"data"];
            
            ProfileVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileVC"];
            next.mutDict=responseObject;
            [self.navigationController pushViewController:next animated:YES];
            
        }
        
        [APP_DELEGATE hideLoadingView];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
}


- (IBAction)btn_BACK:(id)sender {

   // [self.navigationController popViewControllerAnimated:YES];
    if ([_category_nm isEqualToString:@"Portfolio"]) {
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if ([_category_nm isEqualToString:@"IndiePick"])
    {
        IndieVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"IndieVC"];
        [self.navigationController pushViewController:next animated:YES];
    }
    else if ([_category_nm isEqualToString:@"Critiques"])
    {
       [self.navigationController popViewControllerAnimated:YES];
    }
    else if ([_category_nm isEqualToString:@"Events"])
    {
       [self.navigationController popViewControllerAnimated:YES];
    }
    
    
   //[self.navigationController popToRootViewControllerAnimated:YES];

}
@end
