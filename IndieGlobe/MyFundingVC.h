//
//  MyFundingVC.h
//  IndieGlobe
//
//  Created by mac on 14/08/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyFundingVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *arrMyFunding;
    
    UIView *picker;
        
}


@property (weak, nonatomic) IBOutlet UITableView *tbl_myfunding;


@property (weak, nonatomic) IBOutlet UIButton *btn_menu;
@property (weak, nonatomic) IBOutlet UIButton *btn_addfundin;
@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;

- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_NOTIF:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;
- (IBAction)btn_ADD_FUNDING:(id)sender;




@end
