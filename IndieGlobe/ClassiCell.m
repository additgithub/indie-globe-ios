//
//  ClassiCell.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/29/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "ClassiCell.h"
#import "UIColor+CL.h"

@implementation ClassiCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _img_user_prof.clipsToBounds=YES;
    _img_user_prof.layer.cornerRadius=_img_user_prof.frame.size.height/2;
    
    _img_profile.clipsToBounds=YES;
    _img_profile.layer.borderWidth=1;
    _img_profile.layer.borderColor=[UIColor colorWithHex:0xDDDDDD].CGColor;
    
    
    [self.shadow_vw setShadow];
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
