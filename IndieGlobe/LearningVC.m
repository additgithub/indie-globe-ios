//
//  LearningVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/30/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "LearningVC.h"
#import <JSQSystemSoundPlayer/JSQSystemSoundPlayer.h>
#import "SWRevealViewController.h"
#import "LearningUploadVC.h"
#import "DeshboardVC.h"
#import "UIColor+CL.h"
#import "WYPopoverController.h"
#import "Constants.h"
#import "DSCircularLayout.h"
#import "DSCollectionViewCell.h"
#import "TeamBuild1VC.h"
#import "LearningCell.h"
#import "SVPullToRefresh.h"
#import "NewHomeVC.h"

#import "YKMediaPlayerKit.h"
#import "YKVimeoVideo.h"
#import "YKYouTubeVideo.h"
#import "YKDirectVideo.h"
#import "WebPlayerVC.h"
#import "AllNotificationVC.h"


#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height
#define ITEM_WIDTH 75
#define ITEM_HEIGHT 75



@interface LearningVC ()<WYPopoverControllerDelegate>{
    WYPopoverController* popoverController;
    MPMoviePlayerController *_player;
    
    // Player in new view
    //MPMoviePlayerViewController *_newViewPlayer;
}

@end

@implementation LearningVC
{
    
    int count;
    BOOL isShow;
    
    YKYouTubeVideo  *_youTubeVideo;
    YKVimeoVideo    *_vimeoVideo;
    YKDirectVideo   *_directVideo;
    YKDirectVideo   *_otherVideo;
    YKUnKnownVideo  *_unknownVideo;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    // Do any additional setup after loading the view.
    
    

    
    
    //NEW MENU
    count = 0;
    isShow=false;
    [self setCircularLayout];
    
    arrimg= [[NSMutableArray alloc]initWithObjects:@"evets4553.png",@"news4557.png",@"critique4552.png",@"classified4551.png",@"learning4556.png",@"indie-pic4555.png",@"global4554.png",@"team4558.png",nil];
    
    
    txt_city.layer.borderWidth=0.7;
    txt_city.layer.borderColor=[UIColor colorWithHex:0x17c4c7].CGColor;
    
    
    
    //btn_search
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.btn_search.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(10.0, 10.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    self.btn_search.layer.mask = maskLayer;
    [self CustomizeTextField:_txt_select_cat];
    [self textlayers:_txt_select_cat];
    _txt_select_cat.placeholder=@"Search Keyword";

    
    
    
    [self CustomizeTextField:txt_city];
    [self textlayers:txt_city];
    txt_city.placeholder=@"City";
    [self CustomizeTextField:txt_country];
    [self textlayers:txt_country];
    txt_country.placeholder=@"Country";

    [self CustomizeTextField:txt_state];
    [self textlayers:txt_state];
    txt_state.placeholder=@"State";
    
    [self CustomizeTextField:txt_cate_new];
    [self textlayers:txt_cate_new];
    txt_cate_new.placeholder=@"Select Category";

    
    [self customSetup];
    
    
    _btn_add_new.layer.cornerRadius=_btn_add_new.layer.frame.size.height/2;
    _btn_add_new.clipsToBounds=YES;
    
    [_btn_add_new addTarget:self action:@selector(addnew) forControlEvents:UIControlEventTouchUpInside];
    
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];
    
    [_btn_search addTarget:self
                    action:@selector(CallSearchMethod)
          forControlEvents:UIControlEventTouchUpInside];
    
    [self CallCountry];

    [self CallGetCategory];

}

-(void)addnew
{
    LearningUploadVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"LearningUploadVC"];
    [self.navigationController pushViewController:next animated:YES];
}

-(void)CallGetCategory
{
    [APP_DELEGATE showLoadingView:@""];
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    
    [manager POST:[NSString stringWithFormat:@"%@get_profession",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
       
        
        ////////////////////
        NSLog(@"Responce : %@",responseObject);
        
        NSLog(@"DATA :%@ ",[responseObject valueForKey:@"data"]);
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        dict=[responseObject valueForKey:@"data"];
        NSMutableDictionary *dictval=[[NSMutableDictionary alloc]init];
        [dictval setValue:@"SelectAll" forKey:@"Profession"];
        [dictval setValue:@"" forKey:@"ProfessionalID"];
        categor=[[NSMutableArray alloc]init];
        
        //country= [dicsResponse valueForKey:@"data"];
        [categor insertObject:dictval atIndex:0];
        for (int i=0; i<[[responseObject valueForKey:@"data"] count]; i++) {
            [categor insertObject:[[responseObject valueForKey:@"data"]objectAtIndex:i] atIndex:i+1];
        }
        
        NSLog(@"%@",categor);
        [APP_DELEGATE hideLoadingView];
        
        
        
        
       [APP_DELEGATE hideLoadingView];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
    
    
}

-(void)CallSearchMethod
{
    
    page_no=@"0";
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    [AddPost setValue:categorid forKey:@"search_category_id"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",page_no] forKey:@"page_no"];
    
    if (![_txt_select_cat.text isEqualToString:@""]) {
        [AddPost setValue:_txt_select_cat.text forKey:@"search_text"];
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@get_learning_list",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        page_no=[responseObject valueForKey:@"page_no"];
        
        mutDict=[responseObject valueForKey:@"data"];
        
        if (mutDict.count>0) {
            [_tbl_learning reloadData];
        }
        else
        {
            _tbl_learning.showsInfiniteScrolling = NO;
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"NO DATA AVAILABLE" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
        [_tbl_learning reloadData];
        
        
        [APP_DELEGATE hideLoadingView];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
}
-(void)CallSearchMethod1
{
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    //Optional
    
    [AddPost setValue:[NSString stringWithFormat:@"%@",page_no] forKey:@"page_no"];
    if (![_txt_select_cat.text isEqualToString:@""]) {
        [AddPost setValue:_txt_select_cat.text forKey:@"search_text"];
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@get_learning_list",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        page_no=[responseObject valueForKey:@"page_no"];
        
        if ([[responseObject valueForKey:@"data"] count]>0) {
            
            int64_t delayInSeconds = 2.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                //[_tbl_event beginUpdates];
                NSArray *arr = [responseObject valueForKey:@"data"];
                NSMutableArray *newarr = [[NSMutableArray alloc] init];
                NSLog(@"---%@",newarr);
                for (NSString *str in mutDict) {
                    [newarr addObject:str];
                }
                for (int i=0; i<arr.count; i++) {
                    //[mutDict addObject:arr[i]];
                    [newarr addObject:arr[i]];
                }
                NSLog(@"---%@",newarr);
                mutDict = [[NSMutableArray alloc] init];
                for (NSString*str  in newarr) {
                    [mutDict addObject:str];
                }
                
                
                NSLog(@"--%@",mutDict);
                [_tbl_learning.infiniteScrollingView stopAnimating];
                [_tbl_learning reloadData];
            });
            
            
            
            if (!(mutDict.count>0)) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"NO DATA AVAILABLE" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
            }
            
            
        }
        else
        {
            [_tbl_learning.infiniteScrollingView stopAnimating];
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    
    
}



- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if (IsIphone7 || IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = 145;
    }
    else{
        self.revealViewController.rearViewRevealWidth = 145;
    }
}
#pragma mark - GetAllPosts
-(void)CallLearning
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",page_no] forKey:@"page_no"];
    
    //--------------------
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@get_learning_list",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        mutDict=[[NSMutableArray alloc] init];
        mutDict=[responseObject valueForKey:@"data"];
        page_no=[responseObject valueForKey:@"page_no"];
        
        theAppDelegate.n_Count = [NSString stringWithFormat:@"%@",[responseObject valueForKey:@"notification_count"]];
        
        _lbl_notifi_count.text=theAppDelegate.n_Count;
        
        if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
            _lbl_notifi_count.hidden=YES;
            
           
            
        }
        else
        {
            _lbl_notifi_count.hidden=NO;
            _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
            _lbl_notifi_count.clipsToBounds=YES;
        }
        
        if (mutDict <=0) {
            _tbl_learning.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        }
        else
        {
            [_tbl_learning reloadData];
            
        }
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];

    }];
}
-(void)CallCountry
{
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];

    
    NSLog(@" url is %@login",JsonUrlConstant);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpcountry  = [[HttpWrapper alloc] init];
        httpcountry.delegate=self;
        httpcountry.getbool=NO;
        [httpcountry requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_country",JsonUrlConstant] param:[AddPost copy]];
        
        
        
        
    });
}
-(void)Callstate:(NSString *)str
{
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];

    [AddPost setValue:str forKey:@"country_id"];
    NSLog(@"%@",AddPost);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpstate  = [[HttpWrapper alloc] init];
        httpstate.delegate=self;
        httpstate.getbool=NO;
        [httpstate requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_state"
                                                  ,JsonUrlConstant] param:[AddPost copy]];
    });
    
}
-(void)CallCity:(NSString *)str
{
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];

    [AddPost setValue:str forKey:@"state_id"];
    NSLog(@"%@",AddPost);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpcity  = [[HttpWrapper alloc] init];
        httpcity.delegate=self;
        httpcity.getbool=NO;
        [httpcity requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_city"
                                                 ,JsonUrlConstant] param:[AddPost copy]];
    });
    
}

#pragma mark- Wrapper Class Delegate Methods
//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary* )dicsResponse
{
    if(wrapper == httplearning && httplearning != nil)
    {
        
    }
    else if(wrapper == httpcountry && httpcountry != nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        dict=[dicsResponse valueForKey:@"data"];
        country= [dicsResponse valueForKey:@"data"];
        NSLog(@"%@",country);
        
    }
    else if (wrapper==httpstate && httpstate !=nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        if ([dicsResponse valueForKey:@"status"]!=0) {
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[dicsResponse valueForKey:@"data"];
            state= [dicsResponse valueForKey:@"data"];
            NSLog(@"%@",state);
        }
    }
    else if (wrapper==httpcity && httpcity !=nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        if ([dicsResponse valueForKey:@"status"]!=0) {
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[dicsResponse valueForKey:@"data"];
            arrcity= [dicsResponse valueForKey:@"data"];
            NSLog(@"%@",arrcity);
        }
    }

}


//fetchDataFail
- (void) HttpWrapper:(HttpWrapper* )wrapper fetchDataFail:(NSError* )error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
}



-(void)setCircularLayout{
    if ([[UIScreen mainScreen] bounds].size.height == 736.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(190, 270)
                                radius:180
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:72];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }
    else  if ([[UIScreen mainScreen] bounds].size.height == 812.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(200, 260)
                                radius:180
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:72];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 667.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(190, 270)
                                radius:180
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:72];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 568.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(158, 230)
                                radius:150
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:58];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 480.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(158, 230)
                                radius:160
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:58];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }

    
    
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}


-(void)CustomizeTextField:(UITextField *)myTextField
{
    myTextField.layer.borderWidth=1.0;
    myTextField.layer.borderColor=[UIColor colorWithHex:0x196A84].CGColor;
    CGRect frameRect = myTextField.frame;
    frameRect.size.height = 28;
    myTextField.frame = frameRect;
    myTextField.layer.cornerRadius=myTextField.frame.size.height/2;
    
    
}

-(void)textlayers :(UITextField *)UITextField
{
    
    //UITextField.layer.borderColor=[[UIColor clearColor]CGColor];
    //UITextField.layer.borderWidth=1.5;
    //UITextField.layer.borderColor=[[UIColor whiteColor] CGColor];
    //UITextField.layer.cornerRadius=15.0;
    [UITextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
    UITextField.leftView = paddingView;
    UITextField.leftViewMode = UITextFieldViewModeAlways;
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self customSetup];
    page_no=@"0";
    [self CallLearning];

  /*  [self.collection_view performBatchUpdates:^{
        [self.collection_view insertItemsAtIndexPaths:[NSArray arrayWithObjects:
                                                      [NSIndexPath indexPathForRow:0 inSection:0],
                                                      [NSIndexPath indexPathForRow:1 inSection:0],
                                                      [NSIndexPath indexPathForRow:2 inSection:0],
                                                      [NSIndexPath indexPathForRow:3 inSection:0],
                                                      [NSIndexPath indexPathForRow:4 inSection:0],
                                                      [NSIndexPath indexPathForRow:5 inSection:0],
                                                      [NSIndexPath indexPathForRow:6 inSection:0],
                                                      [NSIndexPath indexPathForRow:7 inSection:0],
                                                      nil]];
        count = 8;
    } completion:^(BOOL finished) {
        [self.collection_view reloadData];
    }];*/
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (mutDict.count>0) {
        return mutDict.count;
    }
    else
    {
        
        
        return mutDict.count;
    }
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Cell";
    
    //tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.separatorColor = [UIColor clearColor];
    
    LearningCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[LearningCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
  //  [cell.img_main_image sd_setImageWithURL:[NSURL URLWithString:[mutDict valueForKey:@"VideoURL"][indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
    
    
    
    
    cell.lbl_title.text=[NSString stringWithFormat:@"%@",[[mutDict valueForKey:@"Title"] objectAtIndex:indexPath.row]];
    cell.lbl_desc.text=[NSString stringWithFormat:@"%@",[[mutDict valueForKey:@"ShortDesc"] objectAtIndex:indexPath.row]];
    cell.lbl_profe.text=[NSString stringWithFormat:@"%@",[[mutDict valueForKey:@"Category"] objectAtIndex:indexPath.row]];
    
    //------TODATE--------------
    NSString * yourJSONString = [mutDict valueForKey:@"PostedDate"][indexPath.row];
    NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
    [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateFromString = [currentDTFormatter dateFromString:yourJSONString];
    [currentDTFormatter setDateFormat:@"dd"];
    NSString *onlydt = [currentDTFormatter stringFromDate:dateFromString];
    [currentDTFormatter setDateFormat:@"MMM"];
    NSString *onlymon = [currentDTFormatter stringFromDate:dateFromString];
    
    cell.lbl_smon.text=[NSString stringWithFormat:@"%@",onlymon];
    cell.lbl_sdate.text=[NSString stringWithFormat:@"%@",onlydt];
    
    [cell.img_playbtn setTag:indexPath.row];
    [cell.img_playbtn addTarget:self action:@selector(checkBoxClicked:event:)forControlEvents:UIControlEventTouchUpInside];

    NSString *str=[NSString stringWithFormat:@"%@", [[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];  //is your str
    if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
    
        /*NSURL *movieURL = [NSURL URLWithString:[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
        //Thard party thambanil
        _youTubeVideo = [[YKYouTubeVideo alloc] initWithContent:[NSURL URLWithString:[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]]];
        [_youTubeVideo parseWithCompletion:^(NSError *error) {
        [_youTubeVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
            cell.img_main_image.image = thumbImage;
            }];
        }];
         */
        NSString * getid = [self extractYoutubeIdFromLink:str];
        
        NSString *strImg=[NSString stringWithFormat:@"https://img.youtube.com/vi/%@/3.jpg",getid];
        
        [cell.img_main_image sd_setImageWithURL:[NSURL URLWithString:strImg] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
        
        
        
        
    }
    else
    {
        //NSURL *movieURL = [NSURL URLWithString:[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
        _directVideo = [[YKDirectVideo alloc] initWithContent:[NSURL URLWithString:[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]]];
        [_directVideo thumbImage:YKQualityLow completion:^(UIImage *thumbImage, NSError *error) {
            cell.img_main_image.image = thumbImage;
        }];
    }
    
    
    
    
    
    //NOTE://-CAN NOT GENERATE THUMBNIL FROM YOUTUBE.
    //cell.img_main_image.image= [self imageFromMovie:movieURL atTime:1.0];
     //cell.img_main_image.image= [self generateThumbImage:movieURL];
    
    
    //NSURL *videoURL = [NSURL fileURLWithPath:url];
    
   /* MPMoviePlayerController *player = [[MPMoviePlayerController alloc] initWithContentURL:movieURL];
    
    UIImage *thumbnail = [player thumbnailImageAtTime:1.0 timeOption:MPMovieTimeOptionNearestKeyFrame];
*/
    
    
   // [cell.img_main_image sd_setImageWithURL:[NSURL URLWithString:[mutDict valueForKey:@"VideoURL"][indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
//    NSString *stre=[[mutDict valueForKey:@"VUType"] objectAtIndex:indexPath.row];
//    if ([stre isEqualToString:@"V"] ||[stre isEqualToString:@"v"]) {
//        cell.img_playbtn.hidden=NO;
//    }
//    else
//    {
//        cell.img_playbtn.hidden=YES;
//    }
    
    
    //cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
    return cell;
}
- (void)checkBoxClicked:(id)sender event:(id)event
{
    UIButton *btn=sender;
    
    NSLog(@"button was clicked for the following row %li", (long)btn.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[btn tag]  inSection:0];
    NSLog(@"indexpath.row %ld", (long)indexPath.row);
    NSLog(@"indexpath.row %ld", (long)indexPath.section);
    
    
    NSLog(@"Video URL : %@",[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]);
    
    /*
    NSURL *movieURL = [NSURL URLWithString:[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
    [[UIApplication sharedApplication] openURL:movieURL];*/
    
 /*   MPMoviePlayerController *theMoviPlayer;
    NSURL *urlString=[NSURL URLWithString:[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
    theMoviPlayer = [[MPMoviePlayerController alloc] initWithContentURL:urlString];
    theMoviPlayer.scalingMode = MPMovieScalingModeFill;
    theMoviPlayer.view.frame = CGRectMake(0, 60, 320, 350);
    [self.view addSubview:theMoviPlayer.view];
    [theMoviPlayer play];
    */
    
    NSString *str=[NSString stringWithFormat:@"%@", [[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];  //is your str
    if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
        
      /*  UIWebView *webView = [[UIWebView alloc] initWithFrame:self.view.bounds];  //Change self.view.bounds to a smaller CGRect if you don't want it to take up the whole screen
        [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]]]];
        [self.view addSubview:webView];*/
        
        WebPlayerVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"WebPlayerVC"];
        next.strurl=[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:next animated:NO];
        
        
    }
    else
    {
        NSURL *videoURL = [NSURL URLWithString:[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
        AVPlayer *player = [AVPlayer playerWithURL:videoURL];
        AVPlayerViewController *avplayerViewController = [AVPlayerViewController new];
        avplayerViewController.player = player;
        //[self.view addSubview:avplayerViewController.view];
        [self presentViewController:avplayerViewController animated:YES completion:nil];
    }
    
    
   
    
    
   
    
    
    //NSURL *fileURL = [NSURL URLWithString:@"htps://www.jplayer.org/video/m4v/Big_Buck_Bunny_Trailer.m4v"];
 /*   NSURL *fileURL = [NSURL URLWithString:[[mutDict  valueForKey:@"VideoURL"] objectAtIndex:indexPath.row]];
  
     moviePlayerController = [[MPMoviePlayerViewController alloc] initWithContentURL:fileURL];
    //[moviePlayerController.view setFrame:CGRectMake(0, 70, 320, 270)];
    //[self.view addSubview:moviePlayerController.view];
    //moviePlayerController.fullscreen = YES;
    //moviePlayerController.allowsAirPlay = YES;
    // moviePlayerController.shouldAutoplay = YES;
    //moviePlayerController.moviePlayer = MPMovieControlStyleDefault;
    [moviePlayerController.moviePlayer prepareToPlay];
    moviePlayerController.moviePlayer.fullscreen = YES;
    [moviePlayerController.moviePlayer setFullscreen:YES animated:YES];
    [moviePlayerController.moviePlayer play];
  
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(movieFinishedCallback:)
     name:MPMoviePlayerPlaybackDidFinishNotification
     object:moviePlayerController];
    //[self.view addSubview:moviePlayerController.view];
    [self presentMoviePlayerViewControllerAnimated:moviePlayerController];*/
    
    
}

- (void) movieFinishedCallback:(NSNotification*) aNotification
{
    moviePlayerController = [aNotification object];
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:MPMoviePlayerPlaybackDidFinishNotification
     object:moviePlayerController];
}



    

NSString *const MPMoviePlayerFullscreenAnimationDurationUserInfoKey;
NSString *const MPMoviePlayerFullscreenAnimationCurveUserInfoKey;
- (UIImage* )imageFromMovie:(NSURL* )movieURL atTime:(NSTimeInterval)time {
    // set up the movie player
   
    MPMoviePlayerController *mp = [MPMoviePlayerController alloc];
    mp.shouldAutoplay = NO;
    mp.initialPlaybackTime = time;
    mp.currentPlaybackTime = time;
    // get the thumbnail
    thumbnail = [mp thumbnailImageAtTime:time
                              timeOption:MPMovieTimeOptionNearestKeyFrame];
    // clean up the movie player
    
    [mp stop];
    return(thumbnail);
}
-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    int lastRow=[mutDict count]-1;
    if(([indexPath row] == lastRow)&&(lastRow<[mutDict count]))
    {
        //[self Getnewdata];
        NSLog(@"i m call");
        
        if ([mutDict count]>=2) {
            __weak LearningVC *weakSelf = self;
            // setup infinite scrolling
            [_tbl_learning addInfiniteScrollingWithActionHandler:^{
                [weakSelf CallSearchMethod1];
            }];
        }
        
       /* last_eventid=[[mutDict valueForKey:@"EventID"]lastObject];
        if ([last_eventid integerValue]<1) {
            _tbl_event.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        }*/
        //[self CallSearchMethod];
        
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.selected) {
        // ... Uncheck
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

#pragma NEW MENU
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 8;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    DSCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    //cell.layer.cornerRadius=cell.frame.size.height/2;
    //cell.layer.borderWidth=1.5;
    //cell.layer.borderColor=[UIColor whiteColor].CGColor;
    cell.img_icon.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",[arrimg objectAtIndex:indexPath.row]]];
    [self playAlertSoundPressed:nil];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self playAlertSoundPressed:nil];
    [self SelectedMenu:indexPath.row];
}
- (void)SelectedMenu:(NSInteger)index{
    
    NSLog(@"%ld",(long)index);
    
    switch (index) {
        case 0:
        {
            //Perform any action that u want on menu selection
            EventVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"EventVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            break;
            
        case 1:{
              NewsVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsVC"];
             [self.navigationController pushViewController:next animated:NO];
            
        }
            
            break;
        case 2:
        {
            CritiquesVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"CritiquesVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 3:
        {
            ClassifiedVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"ClassifiedVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 4:
        {
           /* LearningVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"LearningVC"];
            [self.navigationController pushViewController:next animated:YES];*/
        }
            
            break;
        case 5:
        {
            IndieVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"IndieVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 6:
        {
            FundingVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"FundingVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            break;
        case 7:
        {
            TeamBuild1VC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"TeamBuild1VC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            break;

            
            break;
        default:
            break;
    }
}




- (IBAction)btn_NEWMENU:(id)sender {
    [self playAlertSoundPressed:nil];
    if (isShow==false) {
        _collection_view.hidden=NO;
        _img_colle_back.hidden=NO;
        isShow=true;
        
    }
    else
    {
        _collection_view.hidden=YES;
        _img_colle_back.hidden=YES;
        isShow=false;
    }
    
}

- (IBAction)playAlertSoundPressed:(UIButton *)sender
{
    [[JSQSystemSoundPlayer sharedPlayer] playAlertSoundWithFilename:@"Click2-Sebastian"
                                                      fileExtension:kJSQSystemSoundTypeAIFF
                                                         completion:nil];
}

- (IBAction)btn_SCOUNTRY:(id)sender {
    isCheck=0;
    [txt_country setTag:0];
    currentTextField = txt_country;
    [self showPopover:sender];


}

- (IBAction)btn_SSTATE:(id)sender {
    isCheck=1;
    [txt_state setTag:1];
    currentTextField = txt_state;
    [self showPopover:sender];
}
- (IBAction)btn_SCITY:(id)sender {
    isCheck=2;
    [txt_city setTag:2];
    currentTextField = txt_city;
    [self showPopover:sender];
    
}

- (IBAction)btn_SCAT_NEW:(id)sender {
    [self.view endEditing:YES];
    isCheck=3;
    [txt_cate_new setTag:3];
    currentTextField = txt_cate_new;
    [self showPopover:sender];
}

- (IBAction)btn_NOTIFY:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_HOME:(id)sender {
    
    NewHomeVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:YES];
}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}


- (void)touchesBegan:(NSSet* )touches withEvent:(UIEvent* )event {
    [self.view endEditing:YES];
}
- (void)showPopover:(UIView*)btn;
{
    
    if (popoverController == nil)
    {
        PopOverView *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PopOverView"];
        
        settingsViewController.index = currentTextField.tag;
        if (isCheck == 0) {
            settingsViewController.marrcountry = country;
        }
        else if (isCheck==1)
        {
            settingsViewController.marrstate = state;
        }
        else if (isCheck==2)
        {
            settingsViewController.marrcity = arrcity;
        }
        else if (isCheck==3)
        {
            settingsViewController.marrcategor = categor;
        }
        else
        {
            NSLog(@"NO DATA FROM API");
        }

        
        if ([settingsViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
            settingsViewController.preferredContentSize = CGSizeMake(200, 170);             // iOS 7
        }
        else {
#pragma clang diagnostic pushc
#pragma GCC diagnostic ignored "-Wdeprecated"
            settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 170);      // iOS < 7
#pragma clang diagnostic pop
        }
        settingsViewController.delegates = self;
        
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        
        settingsViewController.modalInPopover = NO;
        popoverController = [[WYPopoverController alloc] initWithContentViewController:settingsViewController];
        
        popoverController.delegate = self;
        
        popoverController.passthroughViews = @[btn];
        popoverController.popoverLayoutMargins = UIEdgeInsetsMake(0, 10, 10 , 10);
        popoverController.wantsDefaultContentAppearance = YES;
        
        [popoverController presentPopoverFromRect:btn.bounds
                                           inView:btn
                         permittedArrowDirections:WYPopoverArrowDirectionAny
                                         animated:YES
                                          options:WYPopoverAnimationOptionFadeWithScale];
    }
    
}

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    popoverController.delegate = nil;
    popoverController = nil;
}

-(void)dismissPopUpViewController
{
    [popoverController dismissPopoverAnimated:YES];
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)Popvalueselected:(NSString*)strValue{
    NSLog(@"POP:%@",strValue);
    
    currentTextField.text = strValue;
    
    if (isCheck==0) {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [country filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        countryid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        
        txt_state.text=@"";
        txt_city.text=@"";
        
        [self Callstate:countryid];
        
    }
    else if (isCheck==1)
    {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [state filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        stateid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        txt_city.text=@"";
        [self CallCity:stateid];
    }
    else if (isCheck==2)
    {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [arrcity filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        cityid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        
    }
    else if (isCheck==3)
    {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [categor filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"ProfessionalID"]);
        categorid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"ProfessionalID"] objectAtIndex:0]];
        [self CallSearchMethod];
    }
    else
    {
        currentTextField.text = strValue;
    }

    
}
#pragma video image
-(UIImage* )generateThumbImage : (NSURL* )filepath
{
    
    
    
    NSString *str=[NSString stringWithFormat:@"%@", filepath];  //is your str
    
    NSLog(@"%@",str);
    
    if (!([str rangeOfString:@"youtube"].location == NSNotFound)) {
        
     /*    url = filepath;
       NSLog(@"string  contain youtube");
        
        AVAsset *asset = [AVAsset assetWithURL:url];
        AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
        imageGenerator.appliesPreferredTrackTransform = YES;
        CMTime actualTime;
       
        //CMTime time = [asset duration];
        //time.value = 3;
        CMTime time = CMTimeMakeWithSeconds(1,5);
        CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:&actualTime error:NULL];
        thumbnail = [UIImage imageWithCGImage:imageRef];
        CGImageRelease(imageRef);*/
        
        
      /*  AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:url options:nil];
        AVAssetImageGenerator *gen = [[AVAssetImageGenerator alloc] initWithAsset:asset];
        gen.appliesPreferredTrackTransform = YES;
        CMTime time = CMTimeMakeWithSeconds(0.0, 600);
        NSError *error = nil;
        CMTime actualTime;
        
        CGImageRef image = [gen copyCGImageAtTime:time actualTime:&actualTime error:&error];
        thumbnail = [[UIImage alloc] initWithCGImage:image];
        CGImageRelease(image);*/
        
        /////////////
        
     /*   AVAsset *asset = [AVAsset assetWithURL:filepath];
        AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
        imageGenerator.appliesPreferredTrackTransform = YES;
        CMTime time = [asset duration];
        time.value = 5000;
        CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
        UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
        CGImageRelease(imageRef);  // CGImageRef won't be released by ARC
        
        return thumbnail;*/
        
        ////////
        AVAsset *asset = [AVAsset assetWithURL:filepath];
        AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
        CMTime time = CMTimeMake(5, 600);
        CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
        thumbnail = [UIImage imageWithCGImage:imageRef];
        CGImageRelease(imageRef);
        
        
    }
    else if(([str rangeOfString:@"mp4"].location == NSNotFound)){
//        NSArray *items;
//        
//        if ([str rangeOfString:@"embed/"].location == NSNotFound) {
//            items = [str componentsSeparatedByString:@"="];   //take the one array for split the string
//            NSLog(@"string does not contain bla");
//        } else {
//            NSLog(@"string contains bla!");
//            items = [str componentsSeparatedByString:@"embed/"];   //take the one array for split the string
//        }
//        
//        NSLog(@"items %@",items);
//        NSLog(@"%@",[items objectAtIndex:1]);
//        NSString *myString = [NSString stringWithFormat:@"%@",[items objectAtIndex:1]];
//        
//        myString  = [myString stringByReplacingOccurrencesOfString:@" -- file:///" withString:@" "];
//        
//        url = [NSURL URLWithString:[NSString stringWithFormat:@"https://img.youtube.com/vi/%@/default.jpg",myString]];
//        
//        NSData *data = [NSData dataWithContentsOfURL:url];
//        thumbnail = [UIImage imageWithData:data];
        
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",str]];
        
        MPMoviePlayerController *mp = [[MPMoviePlayerController alloc]
                                       initWithContentURL:url];
        mp.shouldAutoplay = NO;
        mp.initialPlaybackTime = 5;
        mp.currentPlaybackTime = 5;
        // get the thumbnail
        thumbnail = [mp thumbnailImageAtTime:5
                                  timeOption:MPMovieTimeOptionNearestKeyFrame];
        // clean up the movie player
        [mp stop];
  //      return(thumbnail);
        
        
        
    } else {
        NSLog(@"string contains mp4!");
        
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",str]];
        
        MPMoviePlayerController *mp = [[MPMoviePlayerController alloc]
                                       initWithContentURL:url];
        mp.shouldAutoplay = NO;
        mp.initialPlaybackTime = 5;
        mp.currentPlaybackTime = 5;
        // get the thumbnail
        thumbnail = [mp thumbnailImageAtTime:5
                                  timeOption:MPMovieTimeOptionNearestKeyFrame];
        // clean up the movie player
        [mp stop];

        
//        NSArray *items;
//        
//        if ([str rangeOfString:@"embed/"].location == NSNotFound) {
//            items = [str componentsSeparatedByString:@"="];   //take the one array for split the string
//            NSLog(@"string does not contain bla");
//        } else {
//            NSLog(@"string contains bla!");
//            items = [str componentsSeparatedByString:@"embed/"];   //take the one array for split the string
//        }
//        NSLog(@"items %@",items);
//        
//        
//        
//        NSLog(@"%@",[items objectAtIndex:1]);
//        
//        NSString *myString = [NSString stringWithFormat:@"%@",[items objectAtIndex:1]];
//        
//        myString  = [myString stringByReplacingOccurrencesOfString:@" -- file:///" withString:@" "];
//        
//        url = [NSURL URLWithString:[NSString stringWithFormat:@"https://img.youtube.com/vi/%@/default.jpg",myString]];
//        
//        NSData *data = [NSData dataWithContentsOfURL:url];
//        thumbnail = [UIImage imageWithData:data];
        
    }
    
    
    return thumbnail;
}

- (NSString *)extractYoutubeIdFromLink:(NSString *)link {
    NSString *regexString = @"((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)";
    NSRegularExpression *regExp = [NSRegularExpression regularExpressionWithPattern:regexString
                                                                            options:NSRegularExpressionCaseInsensitive
                                                                              error:nil];
    
    NSArray *array = [regExp matchesInString:link options:0 range:NSMakeRange(0,link.length)];
    if (array.count > 0) {
        NSTextCheckingResult *result = array.firstObject;
        return [link substringWithRange:result.range];
    }
    return nil;
}

@end
