//
//  ClassInfoVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/26/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
#import "ApplicationConstant.h"
#import "NewHomeSearchVC.h"

@interface ClassInfoVC : UIViewController<HttpWrapperDelegate>
{
    HttpWrapper *httpclassiinfo;
}
//Outlet

@property (nonatomic,retain)NSMutableDictionary *mutDict;
@property (nonatomic,retain)NSString *isFrom;

@property (strong, nonatomic) IBOutlet UIImageView *img_image;
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;
@property (strong, nonatomic) IBOutlet UILabel *lbl_postdt;
@property (strong, nonatomic) IBOutlet UILabel *lbl_address;
@property (strong, nonatomic) IBOutlet UILabel *lbl_contact;
@property (strong, nonatomic) IBOutlet UITextView *lbl_desc;
@property (strong, nonatomic) IBOutlet UILabel *lbl_usernm;
@property (strong, nonatomic) IBOutlet UIImageView *img_user_image;

@property (weak, nonatomic) IBOutlet UIScrollView *scroll_classi_img;
@property (weak, nonatomic) IBOutlet UILabel *lbl_sell_price;
@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;


//Action
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;
- (IBAction)btn_NOTIFY:(id)sender;
- (IBAction)btn_MSGCHAT:(id)sender;




@end
