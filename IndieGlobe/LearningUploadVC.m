//
//  LearningUploadVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/30/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "LearningUploadVC.h"
#import "DeshboardVC.h"
#import "SWRevealViewController.h"
#import "WYPopoverController.h"
#import "Constants.h"
#import "AllNotificationVC.h"

@interface LearningUploadVC ()<WYPopoverControllerDelegate>{
    WYPopoverController* popoverController;
}


@end

@implementation LearningUploadVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customSetup];
    isVideo=0;
    // Do any additional setup after loading the view.
    [self CustomizeTextField:txt_select_cate];
    txt_select_cate.placeholder=@"Select Category";
    [self CustomizeTextField:_txt_topic];
    _txt_topic.placeholder=@"Topic Title";
    //[self commentview:_txt_workVW];
    //_txt_workVW.placeholder=@"Mention your words how it works?";
    [self CustomizeTextField:_txt_link];
    _txt_link.placeholder=@"Paste Your link";
    
    _btn_submitpost.layer.cornerRadius=_btn_submitpost.frame.size.height/2;
    _img_select_img.layer.cornerRadius=5;
    
    _btn_submitpost1.layer.cornerRadius=_btn_submitpost1.frame.size.height/2;
    
    
    _img_select_img.layer.cornerRadius=5;
    
    _txt_workVW.delegate=self;
    _txt_workVW.text = @"Enter your description";
    _txt_workVW.textColor = [UIColor colorWithRed:(200.0f/255.0) green:(200.0f/255.0) blue:(200.0f/255.0) alpha:1];
    _txt_workVW.layer.borderWidth=1.0;
    _txt_workVW.layer.cornerRadius=6;
    _txt_workVW.layer.borderColor=[UIColor colorWithRed:(180.0f/255.0) green:(180.0f/255.0) blue:(180.0f/255.0) alpha:1].CGColor;
    _txt_workVW.textContainerInset = UIEdgeInsetsMake(6, 7, 0,0);
  


    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];
    
    
    //Choose image
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
    [singleTap setNumberOfTapsRequired:1];
    [_img_select_img addGestureRecognizer:singleTap];

    [self CallGetCategory];
    
    _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
    _lbl_notifi_count.clipsToBounds=YES;
    _lbl_notifi_count.text=theAppDelegate.n_Count;
    
    if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
        _lbl_notifi_count.hidden=YES;
    }
    else
    {
        _lbl_notifi_count.hidden=NO;
    }
    
}
-(void)CallGetCategory
{
    [APP_DELEGATE showLoadingView:@""];
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    
    [manager POST:[NSString stringWithFormat:@"%@get_profession",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        dict=[responseObject valueForKey:@"data"];
        categor= [responseObject valueForKey:@"data"];
        NSLog(@"%@",categor);
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
    
    
}


-(void)singleTapping:(UIGestureRecognizer *)recognizer {
    if(isVideo == 1){
        
        /*UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self; // ensure you set the delegate so when a video is chosen the right method can be called
        
        picker.modalPresentationStyle = UIModalPresentationCurrentContext;
        // This code ensures only videos are shown to the end user
        picker.mediaTypes = @[(NSString*)kUTTypeMovie, (NSString*)kUTTypeAVIMovie, (NSString*)kUTTypeVideo, (NSString*)kUTTypeMPEG4];
        
        picker.videoQuality = UIImagePickerControllerQualityTypeHigh;
        [self presentViewController:picker animated:YES completion:nil];*/
        
        UIImagePickerController *videoPicker = [[UIImagePickerController alloc] init];
        videoPicker.delegate = self;
        videoPicker.modalPresentationStyle = UIModalPresentationCurrentContext;
        videoPicker.mediaTypes =[UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        videoPicker.mediaTypes = @[(NSString*)kUTTypeMovie, (NSString*)kUTTypeAVIMovie, (NSString*)kUTTypeVideo, (NSString*)kUTTypeMPEG4];
        videoPicker.videoQuality = UIImagePickerControllerQualityTypeHigh;
        [self presentViewController:videoPicker animated:YES completion:nil];
        
        
        
    }
    else
    {
        NSLog(@"image clicked");
        ci= [[UIImagePickerController alloc] init];
        ci.delegate = self;
        ci.allowsEditing=NO;
        ci.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        
        if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
            [self presentViewController:ci animated:YES completion:nil];
        else
        {
            popover=[[UIPopoverController alloc]initWithContentViewController:ci];
            [popover presentPopoverFromRect:_img_select_img.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
    }
}
#pragma mark - GetAllPosts
-(void)CallMyMethod
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    [AddPost setValue:[NSString stringWithFormat:@"%@",categorid] forKey:@"category_id"];
    [AddPost setValue:_txt_topic.text forKey:@"title"];
    [AddPost setValue:_txt_workVW.text forKey:@"description"];
    
    if (isVideo==1) {
        [AddPost setValue:@"V" forKey:@"v_u_type"];
       
        //Create manager
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
        NSLog(@"AddPost %@",AddPost);
        NSString *url = [NSString stringWithFormat:@"%@add_lerning_center",JsonUrlConstant];
        [manager POST:url parameters:[AddPost copy] constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            
            //add img data one by one
            NSURL *urlPath = [NSURL URLWithString:filePath];
            NSData *vdata = [NSData dataWithContentsOfURL:urlPath];
            [formData appendPartWithFileData:vdata name:[NSString stringWithFormat:@"video"] fileName:[NSString stringWithFormat:@"user_video.mov"] mimeType:@"video/.mov"];
            
        } success:^(AFHTTPRequestOperation *operation, id dicsResponse) {
            
            NSLog(@"JSON: %@", dicsResponse);
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[dicsResponse valueForKey:@"data"];
            [APP_DELEGATE hideLoadingView];
            [self.navigationController popViewControllerAnimated:YES];
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [APP_DELEGATE hideLoadingView];
        }];
        
    }
    else
    {
        [AddPost setValue:@"U" forKey:@"v_u_type"];
        [AddPost setValue:_txt_link.text forKey:@"learning_url"];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
        NSLog(@"---%@",AddPost);
        [manager POST:[NSString stringWithFormat:@"%@add_lerning_center",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id dicsResponse) {
            NSLog(@"JSON: %@", dicsResponse);
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[dicsResponse valueForKey:@"data"];
            [APP_DELEGATE hideLoadingView];
            [self.navigationController popViewControllerAnimated:YES];
            
        }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  NSLog(@"Error: %@", error);
                  [APP_DELEGATE hideLoadingView];
              }];
    }
}
/*
-(void)setImg_
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:[NSString stringWithFormat:@"%@add_lerning_center",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
}
*/

#pragma mark- Wrapper Class Delegate Methods
//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary* )dicsResponse
{
    if(wrapper == httplearningadd && httplearningadd != nil)
    {
          [self.navigationController popViewControllerAnimated:YES];
    }
}
//fetchDataFail
- (void) HttpWrapper:(HttpWrapper* )wrapper fetchDataFail:(NSError* )error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
}



#pragma mark - ImagePickerController Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if(isVideo == 1){
        NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        
        //    _img_select_img.image = [info objectForKey:UIImagePickerControllerOriginalImage];
        filePath = [videoURL absoluteString];
        //  _img_select_img.image = [info objectForKey:UIImagePickerControllerReferenceURL];
        NSLog(@"VideoURL = %@", videoURL);
       // [picker dismissViewControllerAnimated:YES completion:NULL];
    }else{
        if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
            [picker dismissViewControllerAnimated:YES completion:nil];
        } else {
            [popover dismissPopoverAnimated:YES];
        }
        _img_select_img.image = [info objectForKey:UIImagePickerControllerOriginalImage];
        _img_extra.hidden=YES;
        
        NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        filePath = [videoURL absoluteString];
        NSLog(@"VideoURL = %@", videoURL);
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
        
       /*
        _imgCropperViewController = [[UzysImageCropperViewController alloc] initWithImage:_img_select_img.image andframeSize:ci.view.frame.size andcropSize:CGSizeMake(320, 240)];
        _imgCropperViewController.delegate = self;
        [ci presentViewController:_imgCropperViewController animated:YES completion:nil];
        [_imgCropperViewController release];
        */
        
    }
    
}

- (void)imageCropper:(UzysImageCropperViewController *)cropper didFinishCroppingWithImage:(UIImage *)image
{
    //Isimage=image;
    _img_select_img.image=image;
    //_img_chooseimg.image=image;
    _img_extra.hidden=YES;
    //_lbl_extra_lab.hidden=YES;
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)pick
{
    [pick dismissViewControllerAnimated:YES completion:nil];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self customSetup];
}


- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if (IsIphone7 || IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = 145;
    }
    else{
        self.revealViewController.rearViewRevealWidth = 145;
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if([textView.text isEqual: @"Enter your description"])
    {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)CustomizeTextField:(UITextField *)myTextField
{
    myTextField.layer.borderWidth=1.0;
    myTextField.layer.borderColor=[UIColor colorWithRed:(160.0f/255.0) green:(160.0f/255.0) blue:(160.0f/255.0) alpha:1].CGColor;
    CGRect frameRect = myTextField.frame;
    frameRect.size.height = 24;
    myTextField.frame = frameRect;
    myTextField.layer.cornerRadius=myTextField.frame.size.height/2;
    
    [myTextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
    myTextField.leftView = paddingView;
    myTextField.leftViewMode = UITextFieldViewModeAlways;
    
    
}
-(void)commentview:(UITextField *)myTextField
{
    myTextField.layer.borderWidth=1.0;
    myTextField.layer.borderColor=[UIColor colorWithRed:(160.0f/255.0) green:(160.0f/255.0) blue:(160.0f/255.0) alpha:1].CGColor;
    CGRect frameRect = myTextField.frame;
    frameRect.size.height = 110;
    myTextField.frame = frameRect;
    myTextField.layer.cornerRadius=15;
    
    [myTextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
    myTextField.leftView = paddingView;
    myTextField.leftViewMode = UITextFieldViewModeAlways;
    
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)btn_SUBMIT_POST:(id)sender {
    
    if (isVideo==1) {
        [self CallMyMethod];
    }
    else
    {
        if ([self CheckTextValidation:txt_select_cate :@"please Select Category"]) {
            if ([self CheckTextValidation:_txt_topic :@"please Enter Topic"]) {
                if (![_txt_workVW.text isEqualToString:@""] && ![_txt_workVW.text isEqualToString:@"Enter your description"] ) {
                    if ([self CheckTextValidation:_txt_link :@"Please Enter your link"]) {
                        [self CallMyMethod];
                       /* if ([self validateUrl:_txt_link.text]) {
                            [self CallMyMethod];
                        }
                        else
                        {
                            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Enter valid link." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                            [alert show];
                        }*/
                    }
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Description." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                    [alert show];
                }
            }
        }
    }
    
    
    
}

- (BOOL) validateUrl: (NSString *) candidate {
    NSString *urlRegEx =
    @"[A-Za-z]+\\.[A-Z0-9a-z._%+-]+\\.[A-Za-z]{2,}";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:candidate];
}


-(BOOL)CheckTextValidation :(UITextField*)txtF : (NSString *)msg
{
    if ([txtF.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    else
    {
        return  YES;
    }
}

- (IBAction)btn_NOTIFY:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}


- (IBAction)btn_HOME:(id)sender {
    
    EventVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"EventVC"];
    [self.navigationController pushViewController:next animated:YES];

}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}

- (IBAction)btn_PICTURE:(id)sender {
    isVideo=1;
    NSData *imgData1 = UIImagePNGRepresentation([UIImage imageNamed:@"radio-unselect.png"]);
    NSData *imgData2 = UIImagePNGRepresentation(_img_picture.image);
    BOOL isCompare =  [imgData1 isEqualToData:imgData2];
    if(isCompare)
    {
        NSLog(@"Image View contains image.png");
        _img_picture.image=[UIImage imageNamed:@"radio-select.png"];
        _img_video.image=[UIImage imageNamed:@"radio-unselect.png"];
        _vw_picture.hidden=NO;
        _vw_video.hidden=YES;
    }
}

- (IBAction)btn_VIDEO:(id)sender {
    isVideo=0;
    NSData *imgData1 = UIImagePNGRepresentation([UIImage imageNamed:@"radio-unselect.png"]);
    NSData *imgData2 = UIImagePNGRepresentation(_img_video.image);
    BOOL isCompare =  [imgData1 isEqualToData:imgData2];
    if(isCompare)
    {
        NSLog(@"Image View contains image.png");
        _img_video.image=[UIImage imageNamed:@"radio-select.png"];
        _img_picture.image=[UIImage imageNamed:@"radio-unselect.png"];
        _vw_picture.hidden=YES;
        _vw_video.hidden=NO;
    }
}



- (BOOL)isCompare:(UIImage *)image1 isEqualTo:(UIImage *)image2
{
    NSData *data1 = UIImagePNGRepresentation(image1);
    NSData *data2 = UIImagePNGRepresentation(image2);
    return [data1 isEqual:data2];
}

- (IBAction)btn_SCATEG:(id)sender {
    [self.view endEditing:YES];
    isCheck=3;
    [txt_select_cate setTag:3];
    currentTextField = txt_select_cate;
    [self showPopover:sender];
}

- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)touchesBegan:(NSSet* )touches withEvent:(UIEvent* )event {
    [self.view endEditing:YES];
}
- (void)showPopover:(UIView*)btn;
{
    
    if (popoverController == nil)
    {
        PopOverView *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PopOverView"];
        
        settingsViewController.index = currentTextField.tag;
       
        if (isCheck == 3) {
            settingsViewController.marrcategor = categor;
        }
        
        
        if ([settingsViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
            settingsViewController.preferredContentSize = CGSizeMake(200, 170);             // iOS 7
        }
        else {
#pragma clang diagnostic pushc
#pragma GCC diagnostic ignored "-Wdeprecated"
            settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 170);      // iOS < 7
#pragma clang diagnostic pop
        }
        settingsViewController.delegates = self;
        
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        
        settingsViewController.modalInPopover = NO;
        popoverController = [[WYPopoverController alloc] initWithContentViewController:settingsViewController];
        
        popoverController.delegate = self;
        
        popoverController.passthroughViews = @[btn];
        popoverController.popoverLayoutMargins = UIEdgeInsetsMake(0, 10, 10 , 10);
        popoverController.wantsDefaultContentAppearance = YES;
        
        [popoverController presentPopoverFromRect:btn.bounds
                                           inView:btn
                         permittedArrowDirections:WYPopoverArrowDirectionAny
                                         animated:YES
                                          options:WYPopoverAnimationOptionFadeWithScale];
    }
    
}

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    popoverController.delegate = nil;
    popoverController = nil;
}

-(void)dismissPopUpViewController
{
    [popoverController dismissPopoverAnimated:YES];
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)Popvalueselected:(NSString*)strValue{
    NSLog(@"POP:%@",strValue);
    
    currentTextField.text = strValue;
    
    if (isCheck==3)
    {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [categor filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"ProfessionalID"]);
        categorid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"ProfessionalID"] objectAtIndex:0]];
        
    }
    else
    {
        currentTextField.text = strValue;
    }

    
}


@end
