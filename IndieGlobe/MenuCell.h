//
//  MenuCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/5/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopOverView.h"

@interface MenuCell : UITableViewCell
{
    
    UITextField *currentTextField;

}
@property (strong, nonatomic) IBOutlet UITextField *txt_selec_plan;

- (IBAction)btn_selectplan:(id)sender;

-(void)Popvalueselected:(NSString*)strValue;
-(void)Popvalveselected:(NSString*)strValve;
-(void)dismissPopUpViewController;


@end
