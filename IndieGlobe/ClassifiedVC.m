//
//  ClassifiedVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/28/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "ClassifiedVC.h"
#import <JSQSystemSoundPlayer/JSQSystemSoundPlayer.h>
#import "SWRevealViewController.h"
#import "DeshboardVC.h"
#import "WYPopoverController.h"
#import "Constants.h"
#import "UIColor+CL.h"
#import "DSCollectionViewCell.h"
#import "DSCircularLayout.h"
#import "AddClassi.h"
#import "UIColor+CL.h"
//---------------------
#import "DeshboardVC.h"
#import "ProfileVC.h"
#import "CritiquesVC.h"
#import "ClassifiedVC.h"
#import "PackageList.h"
#import "LearningVC.h"
#import "EventVC.h"
#import "TeamBuildVC.h"
#import "UIViewController+ENPopUp.h"
#import "MyEventVC.h"
#import "SettingVC.h"
#import "NewProfileVC.h"
#import "IndieVC.h"
#import "FundingVC.h"
#import "TeamBuild1VC.h"
#import "ClassInfoVC.h"
#import "ClassiCell.h"
#import "SVPullToRefresh.h"
#import "NewHomeVC.h"
#import "AllNotificationVC.h"
#import "UIImageView+Haneke.h"

//------------------------------

#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height
#define ITEM_WIDTH 75
#define ITEM_HEIGHT 75


@interface ClassifiedVC ()<WYPopoverControllerDelegate>{
    WYPopoverController* popoverController;
}


@end

@implementation ClassifiedVC
{
    IGCMenu *igcMenu;
    int count;
    BOOL isShow;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    // Do any additional setup after loading the view.
    
    //NEW MENU
    page_no=@"0";
    count = 0;
    isShow=false;
    [self setCircularLayout];
    
    arrimg= [[NSMutableArray alloc]initWithObjects:@"evets4553.png",@"news4557.png",@"critique4552.png",@"classified4551.png",@"learning4556.png",@"indie-pic4555.png",@"global4554.png",@"team4558.png",nil];
    
    //btn_search
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.btn_search.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(10.0, 10.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    self.btn_search.layer.mask = maskLayer;
    
    //SEARCH BOX
    /* _txt_serachbox.layer.cornerRadius=12;
     _txt_serachbox.layer.borderColor=[UIColor lightGrayColor].CGColor;
     _txt_serachbox.layer.borderWidth=0.4;
     CGRect frameRect = _txt_serachbox.frame;
     frameRect.size.height = 28; // <-- Specify the height you want here.
     _txt_serachbox.frame = frameRect;*/
    
    _btn_add_classi.layer.borderColor=[UIColor colorWithRed:(23.0f/255.0) green:(196.0f/255.0) blue:(199.0f/255.0) alpha:1].CGColor;
    _btn_add_classi.layer.cornerRadius=_btn_add_classi.frame.size.height/2;
    
    [self CustomizeTextField:txt_categ];
    [self CustomizeTextField:txt_country];
    [self CustomizeTextField:txt_state];
    [self CustomizeTextField:txt_city];
    [self CustomizeTextField:_txt_shortby];
    [self CustomizeTextField:_txt_serachbox];
    
    
    
    [self textlayers:_txt_serachbox];
    [self textlayers:txt_categ];
    [self textlayers:txt_country];
    [self textlayers:txt_state];
    [self textlayers:txt_city];
    [self textlayers:_txt_shortby];
    [self textlayers:_txt_serachbox];
    
    
    txt_categ.placeholder=@"Select Category";
    txt_country.placeholder=@"Country";
    txt_state.placeholder=@"State";
    txt_city.placeholder=@"City";
    _txt_shortby.placeholder=@"Sort By";
    _txt_serachbox.placeholder=@"Search Classified";
    
    [self customSetup];
    
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];
    
    [_btn_search addTarget:self
                    action:@selector(CallSearchClassified)
          forControlEvents:UIControlEventTouchUpInside];
    
    [self CallCountry];
    [self CallGetCategory];
    
}



- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if (IsIphone7 || IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = 145;
    }
    else{
        self.revealViewController.rearViewRevealWidth = 145;
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}



-(void)CustomizeTextField:(UITextField *)myTextField
{
    myTextField.layer.borderWidth=1.0;
    myTextField.layer.borderColor=[UIColor colorWithHex:0x196A84].CGColor;
    
    CGRect frameRect = myTextField.frame;
    frameRect.size.height = 28;
    myTextField.frame = frameRect;
    myTextField.layer.cornerRadius=myTextField.frame.size.height/2;
    
    
    
}

-(void)textlayers :(UITextField *)UITextField
{
    
    [UITextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
    UITextField.leftView = paddingView;
    UITextField.leftViewMode = UITextFieldViewModeAlways;
    
}

#pragma mark - GetAllPosts
-(void)CallGetClassified
{
    [self.view endEditing:YES];
    [APP_DELEGATE showLoadingView:@""];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",page_no] forKey:@"page_no"];
    
    //--------------------
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@get_classified",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        mutDict=[[NSMutableArray alloc] init];
        mutDict=[responseObject valueForKey:@"data"];
        page_no=[responseObject valueForKey:@"page_no"];
        
        
        theAppDelegate.n_Count = [NSString stringWithFormat:@"%@",[responseObject valueForKey:@"notification_count"]];
        
        _lbl_notifi_count.text=theAppDelegate.n_Count;
        
        if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
            _lbl_notifi_count.hidden=YES;
            
           
            
        }
        else
        {
            _lbl_notifi_count.hidden=NO;
            
            _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
            _lbl_notifi_count.clipsToBounds=YES;
        }

        
        if (!(mutDict.count>0)) {
            _tbl_classi.hidden=YES;
        }
        
        
        if (mutDict.count<=0) {
            // _tbl_classi .hidden=YES;
            _tbl_classi.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        }
        else
        {
            //_tbl_classi.hidden=NO;
            [_tbl_classi reloadData];
        }
        
        [APP_DELEGATE hideLoadingView];
        
        [_tbl_classi reloadData];
        
        
        if (!(mutDict.count>0)) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"NO CLASSIFIED AVAILABLE" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
}
-(void)CallSearchMethod1
{
    [self.view endEditing:YES];
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    //Optional
    [AddPost setValue:countryid forKey:@"country"];
    [AddPost setValue:stateid forKey:@"state"];
    [AddPost setValue:cityid forKey:@"city"];
    
    [AddPost setValue:[NSString stringWithFormat:@"%@",page_no] forKey:@"page_no"];
    if (![_txt_serachbox.text isEqualToString:@""]) {
        [AddPost setValue:_txt_serachbox.text forKey:@"search_text"];
    }
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@get_classified",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        page_no=[responseObject valueForKey:@"page_no"];
        
        if ([[responseObject valueForKey:@"data"] count]<=0) {
            //mutDict=[[NSMutableArray alloc] init];
            [_tbl_classi.infiniteScrollingView stopAnimating];
            _tbl_classi.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
            [APP_DELEGATE hideLoadingView];
            
        }
        else
        {
            int64_t delayInSeconds = 2.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                NSArray *arr = [responseObject valueForKey:@"data"];
                if (arr.count<1) {
                    _tbl_classi.showsInfiniteScrolling = NO;
                }
                NSMutableArray *newarr = [[NSMutableArray alloc] init];
                NSLog(@"---%@",newarr);
                for (NSString *str in mutDict) {
                    [newarr addObject:str];
                }
                for (int i=0; i<arr.count; i++) {
                    //[mutDict addObject:arr[i]];
                    [newarr addObject:arr[i]];
                }
                NSLog(@"---%@",newarr);
                mutDict = [[NSMutableArray alloc] init];
                for (NSString*str  in newarr) {
                    [mutDict addObject:str];
                }
                [_tbl_classi.infiniteScrollingView stopAnimating];
                [_tbl_classi reloadData];
                [APP_DELEGATE hideLoadingView];
                
            });
            
        }
        
        [APP_DELEGATE hideLoadingView];
        
        if (!(mutDict.count>0)) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"NO CLASSIFIED AVAILABLE" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
}

-(void)CallSearchClassified
{
    
    [self.view endEditing:YES];
    
    [APP_DELEGATE showLoadingView:@""];
    page_no=@"0";
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    //Optional
    [AddPost setValue:countryid forKey:@"country"];
    [AddPost setValue:stateid forKey:@"state"];
    [AddPost setValue:cityid forKey:@"city"];
    [AddPost setValue:categorid forKey:@"search_category_id"];
    
    
    [AddPost setValue:[NSString stringWithFormat:@"%@",page_no] forKey:@"page_no"];
    if (![_txt_serachbox.text isEqualToString:@""]) {
        [AddPost setValue:_txt_serachbox.text forKey:@"search_text"];
    }
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    
    [manager POST:[NSString stringWithFormat:@"%@get_classified",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        page_no=[responseObject valueForKey:@"page_no"];
        
        
        mutDict=[responseObject valueForKey:@"data"];
        
        if (mutDict.count>0) {
            [_tbl_classi reloadData];
            
        }
        else
        {
            _tbl_classi.showsInfiniteScrolling = NO;
            [_tbl_classi reloadData];
        }
        
        [APP_DELEGATE hideLoadingView];
        
        if (!(mutDict.count>0)) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"NO CLASSIFIED AVAILABLE" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
}


-(void)CallGetCategory
{
    [APP_DELEGATE showLoadingView:@""];
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    
    [manager POST:[NSString stringWithFormat:@"%@get_profession",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        dict=[responseObject valueForKey:@"data"];
        categor= [responseObject valueForKey:@"data"];
        NSLog(@"%@",categor);
        [APP_DELEGATE hideLoadingView];
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
    
    
}



-(void)CallCountry
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    NSLog(@" url is %@login",JsonUrlConstant);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpcountry  = [[HttpWrapper alloc] init];
        httpcountry.delegate=self;
        httpcountry.getbool=NO;
        [httpcountry requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_country",JsonUrlConstant] param:[AddPost copy]];
        
        
        
        
    });
}
-(void)Callstate:(NSString *)str
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:str forKey:@"country_id"];
    
    NSLog(@"%@",AddPost);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpstate  = [[HttpWrapper alloc] init];
        httpstate.delegate=self;
        httpstate.getbool=NO;
        [httpstate requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_state"
                                                  ,JsonUrlConstant] param:[AddPost copy]];
    });
    
}
-(void)CallCity:(NSString *)str
{
    [self.view endEditing:YES];
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    [AddPost setValue:str forKey:@"state_id"];
    NSLog(@"%@",AddPost);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpcity  = [[HttpWrapper alloc] init];
        httpcity.delegate=self;
        httpcity.getbool=NO;
        [httpcity requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_city"
                                                 ,JsonUrlConstant] param:[AddPost copy]];
    });
    
}
-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    
}

#pragma mark- Wrapper Class Delegate Methods
//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary* )dicsResponse
{
    if(wrapper == httpclassi && httpclassi != nil)
    {
        NSLog(@"RESPONSE--- %@",dicsResponse);
        NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
        mutDict=[[NSMutableDictionary alloc] init];
        mutDict=[dicsResponse valueForKey:@"data"];
        
        if ([[dicsResponse valueForKey:@"status"] integerValue] == 0) {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Success" message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //[alert show];
            [APP_DELEGATE hideLoadingView];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Success" message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //[alert show];
            
            [_tbl_classi reloadData];
            
            [APP_DELEGATE hideLoadingView];
        }
        
    }
    else if(wrapper == httpcountry && httpcountry != nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        dict=[dicsResponse valueForKey:@"data"];
        NSMutableDictionary *dictval=[[NSMutableDictionary alloc]init];
        [dictval setValue:@"SelectAll" forKey:@"LocationName"];
        [dictval setValue:@"" forKey:@"LocationID"];
        country=[[NSMutableArray alloc]init];
        
        //country= [dicsResponse valueForKey:@"data"];
        [country insertObject:dictval atIndex:0];
        for (int i=0; i<[[dicsResponse valueForKey:@"data"] count]; i++) {
            [country insertObject:[[dicsResponse valueForKey:@"data"]objectAtIndex:i] atIndex:i+1];
        }
        
        NSLog(@"%@",country);
        [APP_DELEGATE hideLoadingView];
    }
    else if (wrapper==httpstate && httpstate !=nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        if ([dicsResponse valueForKey:@"status"]!=0) {
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[dicsResponse valueForKey:@"data"];
            state= [dicsResponse valueForKey:@"data"];
            NSLog(@"%@",state);
        }
        [APP_DELEGATE hideLoadingView];
    }
    else if (wrapper==httpcity && httpcity !=nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        if ([dicsResponse valueForKey:@"status"]!=0) {
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[dicsResponse valueForKey:@"data"];
            arrcity= [dicsResponse valueForKey:@"data"];
            NSLog(@"%@",arrcity);
        }
    }
    
    [APP_DELEGATE hideLoadingView];
}
//fetchDataFail
- (void) HttpWrapper:(HttpWrapper* )wrapper fetchDataFail:(NSError* )error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self customSetup];
    page_no=@"0";
    [self CallGetClassified];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setCircularLayout{
    // your collectionView
    if ([[UIScreen mainScreen] bounds].size.height == 736.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(190, 270)
                                radius:180
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:72];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }
    else  if ([[UIScreen mainScreen] bounds].size.height == 812.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(200, 260)
                                radius:180
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:72];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 667.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(190, 270)
                                radius:180
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:72];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 568.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(158, 230)
                                radius:150
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:58];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }
    else if ([[UIScreen mainScreen] bounds].size.height == 480.0f)
    {
        DSCircularLayout *circularLayout = [[DSCircularLayout alloc] init];
        [circularLayout initWithCentre:CGPointMake(158, 230)
                                radius:160
                              itemSize:CGSizeMake(ITEM_WIDTH, ITEM_HEIGHT)
                     andAngularSpacing:58];
        [circularLayout setStartAngle:M_E endAngle:0];
        circularLayout.mirrorX = NO;
        circularLayout.mirrorY = NO;
        circularLayout.rotateItems = NO;
        circularLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        [self.collection_view setCollectionViewLayout:circularLayout];
    }
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 8;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    DSCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    //cell.layer.cornerRadius=cell.frame.size.height/2;
    //cell.layer.borderWidth=1.5;
    cell.layer.borderColor=[UIColor whiteColor].CGColor;
    cell.img_icon.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",[arrimg objectAtIndex:indexPath.row]]];
    
    [self playAlertSoundPressed:nil];
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self playAlertSoundPressed:nil];
    [self SelectedMenu:indexPath.row];
}
- (void)SelectedMenu:(NSInteger)index{
    
    NSLog(@"%ld",(long)index);
    
    switch (index) {
        case 0:
        {
            //Perform any action that u want on menu selection
            EventVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"EventVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            break;
            
        case 1:{
            NewsVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsVC"];
            [self.navigationController pushViewController:next animated:NO];
            
        }
            
            break;
        case 2:
        {
            CritiquesVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"CritiquesVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 3:
        {
            /* ClassifiedVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"ClassifiedVC"];
             [self.navigationController pushViewController:next animated:NO];*/
        }
            
            break;
        case 4:
        {
            LearningVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"LearningVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 5:
        {
            IndieVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"IndieVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            
            break;
        case 6:
        {
            FundingVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"FundingVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            break;
        case 7:
        {
            TeamBuild1VC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"TeamBuild1VC"];
            [self.navigationController pushViewController:next animated:NO];
        }
            break;
            
            
            break;
        default:
            break;
    }
}
-(CGFloat)heightForText:(NSString *)text
{
    NSInteger MAX_HEIGHT = 2000;
    UITextView * textView = [[UITextView alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width-80, MAX_HEIGHT+150)];
    textView.text = text;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        textView.font = [UIFont fontWithName:@"Roboto-Light" size:13.0];
    }
    else
    {
        textView.font = [UIFont fontWithName:@"Roboto-Light" size:13.0];
    }
    
    [textView sizeToFit];
    
    return textView.frame.size.height;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return mutDict.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"ClassiCell";
    
    //tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.separatorColor = [UIColor clearColor];
    
    
    ClassiCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[ClassiCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    
    ///=============================================
    
    
    
    
    
    NSMutableArray *image_arr=[[NSMutableArray alloc]init];
    image_arr=[mutDict valueForKey:@"Clasified_images"][indexPath.row];
    
    cell.scroll_classi.pagingEnabled=YES;
    
    int x=0;
    
    //Load images on ImageView
    NSLog(@"arr  :: %@", image_arr);
    
    if (image_arr.count>0) {
        for (int k=0; k<[image_arr count]; k++)
        {
            
            NSString * strImage = [[image_arr objectAtIndex:k] valueForKey:@"ImageURL"];
            
            strImage = [strImage stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
            UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(x, 0,cell.scroll_classi.frame.size.width, cell.scroll_classi.frame.size.height)];
            
            
            [img sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:@"avatar"]];
            
            
            
            x=x+cell.scroll_classi.frame.size.width;
            
            [img setContentMode:UIViewContentModeScaleAspectFill];
            [img setClipsToBounds:YES];
            
            [cell.scroll_classi addSubview:img];
            
        }
    }
    else
    {
        //https://indieglobe.blenzabi.com/uploads/events/
        
        
        
        NSString * strImage = @"https://indieglobe.blenzabi.com/uploads/events/";
        
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(x, 0,cell.scroll_classi.frame.size.width, cell.scroll_classi.frame.size.height)];
        
        
        [img sd_setImageWithURL:[NSURL URLWithString:strImage] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
        
        x=x+cell.scroll_classi.frame.size.width;
        [img setContentMode:UIViewContentModeScaleAspectFill];
        [img setClipsToBounds:YES];//btn_replay.hidden
        
        [cell.scroll_classi addSubview:img];
    }
    
    cell.scroll_classi.contentSize=CGSizeMake(x, cell.scroll_classi.frame.size.height);
    cell.scroll_classi.contentOffset=CGPointMake(0, 0);
    
    //==============================================
    
    [cell.img_user_prof sd_setImageWithURL:[NSURL URLWithString:[mutDict valueForKey:@"UserImageURL"][indexPath.row]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
    cell.lbl_usernm.text=[NSString stringWithFormat:@"%@ %@",[[mutDict valueForKey:@"FName"] objectAtIndex:indexPath.row],[[mutDict valueForKey:@"LName"] objectAtIndex:indexPath.row]];
    
    
    cell.lbl_title.text=[NSString stringWithFormat:@"%@",[[mutDict  valueForKey:@"Title"] objectAtIndex:indexPath.row]];
    cell.lbl_short_desc.text=[NSString stringWithFormat:@"%@",[[mutDict  valueForKey:@"ShortDesc"] objectAtIndex:indexPath.row]];
    
    cell.lbl_sell_price.text=[NSString stringWithFormat:@"$ %@",[mutDict valueForKey:@"Price"][indexPath.row]];
    
    [cell.lbl_short_desc sizeToFit];
    
    cell.lbl_address.text=[NSString stringWithFormat:@"%@, %@",[[mutDict  valueForKey:@"Cityname"] objectAtIndex:indexPath.row],[[mutDict  valueForKey:@"Statename"] objectAtIndex:indexPath.row]];
    
    cell.cat_nm.text=[NSString stringWithFormat:@"%@",[[mutDict  valueForKey:@"Profession"] objectAtIndex:indexPath.row]];
    
    NSString * yourJSONString = [NSString stringWithFormat:@"%@",[[mutDict  valueForKey:@"PostedDate"] objectAtIndex:indexPath.row]];
    
    cell.lbl_days.text=[NSString stringWithFormat:@"%@",[APP_DELEGATE DateCovertforClassified:yourJSONString]];
    
    [cell.img_profile sd_setImageWithURL:[NSURL URLWithString:[mutDict valueForKey:@"PhotoURL"][indexPath.row]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
    
    
    return cell;
}


-(void)alignTextToTopTextView :(UITextView*)textView{
    
    CGRect frame = textView.frame;
    frame.size.height = textView.contentSize.height;
    textView.frame = frame;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.selected) {
        // ... Uncheck
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    
    ClassInfoVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"ClassInfoVC"];
    NSArray *arr = [[NSArray alloc] initWithObjects:mutDict, nil][0];
    NSLog(@"arr %@",arr);
    next.mutDict=[arr objectAtIndex:indexPath.row];
    next.isFrom=@"ClassifiedVC";
    [self.navigationController pushViewController:next animated:YES];
    
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    int lastRow=[mutDict count]-1;
    if(([indexPath row] == lastRow)&&(lastRow<[mutDict count]))
    {
        //[self Getnewdata];
        NSLog(@"i m call");
        
        if ([mutDict count]>=2) {
            __weak ClassifiedVC *weakSelf = self;
            // setup infinite scrolling
            [_tbl_classi addInfiniteScrollingWithActionHandler:^{
                [weakSelf CallSearchMethod1];
            }];
        }
    }
}





- (IBAction)btn_HOME:(id)sender {
    NewHomeVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:YES];
    
}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}


- (IBAction)btn_SCATEG:(id)sender {
    [self.view endEditing:YES];
    isCheck=3;
    [txt_categ setTag:3];
    currentTextField = txt_categ;
    [self showPopover:sender];
}

- (IBAction)btn_SSORT:(id)sender {
}

- (IBAction)btn_SCOUNTRY:(id)sender {
    [self.view endEditing:YES];
    isCheck=0;
    [txt_country setTag:0];
    currentTextField = txt_country;
    [self showPopover:sender];
}

- (IBAction)btn_SSTATE:(id)sender {
    [self.view endEditing:YES];
    isCheck=1;
    [txt_state setTag:1];
    currentTextField = txt_state;
    [self showPopover:sender];
}
- (IBAction)btn_SCITY:(id)sender {
    [self.view endEditing:YES];
    isCheck=2;
    [txt_city setTag:2];
    currentTextField = txt_city;
    [self showPopover:sender];
}


- (IBAction)btn_ADD_CLASSIFIED:(id)sender {
    AddClassi *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AddClassi"];
    [self.navigationController pushViewController:next animated:YES];
}

- (IBAction)btn_NOTIFY:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}

- (IBAction)btn_NEWMENU:(id)sender {
    [self playAlertSoundPressed:nil];
    if (isShow==false) {
        _collection_view.hidden=NO;
        _img_colle_back.hidden=NO;
        
        isShow=true;
    }
    else
    {
        _collection_view.hidden=YES;
        _img_colle_back.hidden=YES;
        isShow=false;
        
    }
    
}
- (IBAction)playAlertSoundPressed:(UIButton *)sender
{
    [[JSQSystemSoundPlayer sharedPlayer] playAlertSoundWithFilename:@"Click2-Sebastian"
                                                      fileExtension:kJSQSystemSoundTypeAIFF
                                                         completion:nil];
}

- (void)touchesBegan:(NSSet* )touches withEvent:(UIEvent* )event {
    [self.view endEditing:YES];
}
- (void)showPopover:(UIView*)btn;
{
    
    if (popoverController == nil)
    {
        PopOverView *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PopOverView"];
        
        settingsViewController.index = currentTextField.tag;
        
        if (isCheck == 0) {
            settingsViewController.marrcountry = country;
        }
        else if (isCheck==1)
        {
            settingsViewController.marrstate = state;
        }
        else if (isCheck==2)
        {
            settingsViewController.marrcity = arrcity;
        }
        else if (isCheck==3)
        {
            settingsViewController.marrcategor = categor;
        }
        
        else{
            NSLog(@"NO DATA FROM API");
        }
        
        
        
        if ([settingsViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
            settingsViewController.preferredContentSize = CGSizeMake(200, 170);             // iOS 7
        }
        else {
#pragma clang diagnostic pushc
#pragma GCC diagnostic ignored "-Wdeprecated"
            settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 170);      // iOS < 7
#pragma clang diagnostic pop
        }
        settingsViewController.delegates = self;
        
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        
        settingsViewController.modalInPopover = NO;
        popoverController = [[WYPopoverController alloc] initWithContentViewController:settingsViewController];
        
        popoverController.delegate = self;
        
        popoverController.passthroughViews = @[btn];
        popoverController.popoverLayoutMargins = UIEdgeInsetsMake(0, 10, 10 , 10);
        popoverController.wantsDefaultContentAppearance = YES;
        
        [popoverController presentPopoverFromRect:btn.bounds
                                           inView:btn
                         permittedArrowDirections:WYPopoverArrowDirectionAny
                                         animated:YES
                                          options:WYPopoverAnimationOptionFadeWithScale];
    }
    
}

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    popoverController.delegate = nil;
    popoverController = nil;
}

-(void)dismissPopUpViewController
{
    [popoverController dismissPopoverAnimated:YES];
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)Popvalueselected:(NSString*)strValue{
    NSLog(@"POP:%@",strValue);
    
    currentTextField.text = strValue;
    
    if (isCheck==0) {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [country filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        countryid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        txt_state.text=@"";
        txt_city.text=@"";
        
        
        if ([countryid isEqualToString:@""]) {
            page_no=@"0";
            
            countryid=@"";
            stateid=@"";
            cityid=@"";
            
            [self CallSearchMethod1];
            
            
        }
        else
        {
            
            [self Callstate:countryid];
            [self CallSearchClassified];
        }
        
    }
    else if (isCheck==1)
    {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [state filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        stateid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        txt_city.text=@"";
        [self CallCity:stateid];
    }
    else if (isCheck==2)
    {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [arrcity filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        cityid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        [self CallSearchClassified];
    }
    else if (isCheck==3)
    {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [categor filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"ProfessionalID"]);
        categorid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"ProfessionalID"] objectAtIndex:0]];
        
        [self CallSearchClassified];
    }
    else
    {
        currentTextField.text = strValue;
    }
    
}

@end
