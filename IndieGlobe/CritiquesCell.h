//
//  CritiquesCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 1/22/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShadowView.h"


@interface CritiquesCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UIImageView *img_ds_img;
@property (strong, nonatomic) IBOutlet UIImageView *img_critic;
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;
@property (strong, nonatomic) IBOutlet UILabel *lbl_date;
@property (strong, nonatomic) IBOutlet UILabel *lbl_comments;
@property (strong, nonatomic) IBOutlet UILabel *lbl_about_que;
@property (strong, nonatomic) IBOutlet UIImageView *img_user_image;
@property (weak, nonatomic) IBOutlet ShadowView *shadow_vw;

@property (weak, nonatomic) IBOutlet UIButton *btn_like;
@property (weak, nonatomic) IBOutlet UILabel *lbl_like;
@property (weak, nonatomic) IBOutlet UILabel *lbl_dislike;
@property (weak, nonatomic) IBOutlet UIButton *btn_comment;
@property (weak, nonatomic) IBOutlet UILabel *lbl_comment;
@property (weak, nonatomic) IBOutlet UIButton *btn_dislike;

@property (weak, nonatomic) IBOutlet UIView *vw_1;
@property (weak, nonatomic) IBOutlet UIImageView *img_vw1;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title_vw1;
@property (weak, nonatomic) IBOutlet UILabel *lbl_desc_vw1;

@property (weak, nonatomic) IBOutlet UIView *vw_2;
@property (weak, nonatomic) IBOutlet UIImageView *img_vw2;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title_vw2;
@property (weak, nonatomic) IBOutlet UILabel *lbl_desc_vw2;









@end
