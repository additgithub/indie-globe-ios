//
//  AddClassi.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/22/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "AddClassi.h"
#import "UIColor+CL.h"
#import "WYPopoverController.h"
#import "DeshboardVC.h"
#import "NewHomeVC.h"
#import "AllNotificationVC.h"
#import "NewProfileCell.h"


@interface AddClassi ()<WYPopoverControllerDelegate>{
    WYPopoverController* popoverController;
}


@end

@implementation AddClassi

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customSetup];
    // Do any additional setup after loading the view.
    _img_newimg.layer.cornerRadius=11;
    _btn_submit.layer.cornerRadius=_btn_submit.frame.size.height/2;
    
    [self CustomizeTextField:_txt_title];
    _txt_title.placeholder=@"Classified Title";
    [self CustomizeTextField:_txt_lat];
    _txt_lat.placeholder=@"Letitude";
    [self CustomizeTextField:_txt_longi];
    _txt_longi.placeholder=@"Longitude";
    [self CustomizeTextField:txt_country];
    txt_country.placeholder=@"Country";
    [self CustomizeTextField:txt_city];
    txt_city.placeholder=@"City";
    [self CustomizeTextField:txt_state];
    txt_state.placeholder=@"State";
    [self CustomizeTextField:_txt_date];
    _txt_date.placeholder=@"25-12-2017";
    [self CustomizeTextField:_txt_contact];
    _txt_contact.placeholder=@"Contact";
    [self CustomizeTextField:txt_category];
    txt_category.placeholder=@"Category";
    [self CustomizeTextField:_txt_classi_price];
    _txt_classi_price.placeholder=@"Price";
    
    _txt_location.delegate=self;
    _txt_location.text = @"Address";
    _txt_location.textColor = [UIColor lightGrayColor];
    _txt_location.layer.borderWidth=1.0;
    _txt_location.layer.cornerRadius=6;
    _txt_location.layer.borderColor=[UIColor colorWithHex:0xDEDEDE].CGColor;
    
    _txt_discr.delegate=self;
    _txt_discr.text = @"Description";
    _txt_discr.textColor = [UIColor lightGrayColor];
    _txt_discr.layer.borderWidth=1.0;
    _txt_discr.layer.cornerRadius=6;
    _txt_discr.layer.borderColor=[UIColor colorWithHex:0xDEDEDE].CGColor;
    
    
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];
    
    
    //Choose image
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
    [singleTap setNumberOfTapsRequired:1];
    [_img_newimg addGestureRecognizer:singleTap];
    
    
    //[self CallMyMethod];
    [self CallGetCategory];
    [self CallCountry];
    
    //GET LET LONG
    /*  CLLocationCoordinate2D coordinate = [self getLocation];
     NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
     NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
     
     NSLog(@"*dLatitude : %@", latitude);
     NSLog(@"*dLongitude : %@",longitude);
     
     _txt_lat.text=[NSString stringWithFormat:@"%f", coordinate.latitude];
     _txt_longi.text=[NSString stringWithFormat:@"%f", coordinate.longitude];*/
    
    
    AddNew=[[NSMutableArray alloc] init];
    
    if (AddNew.count <= 0) {
        //Array Allocation
        AddNew = [[NSMutableArray alloc] initWithObjects:@"addNew", nil];
        
    }
    
    _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
    _lbl_notifi_count.clipsToBounds=YES;
    _lbl_notifi_count.text=theAppDelegate.n_Count;
    
    if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
        _lbl_notifi_count.hidden=YES;
    }
    else
    {
        _lbl_notifi_count.hidden=NO;
    }
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return AddNew.count;
    
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NewProfileCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    cell = nil;
    if (cell == nil)
    {
        cell=(NewProfileCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    }
    cell.img_movi_image.clipsToBounds = YES;
    cell.img_movi_image.layer.masksToBounds = YES;
    cell.img_movi_image.layer.cornerRadius = 10.0f;
    cell.layer.cornerRadius = 10.0f;
    
    NSLog(@"AddNew %@",AddNew);
    if ([AddNew[indexPath.row] isEqual:@"addNew"]) {
        cell.img_movi_image.image = [UIImage imageNamed:@"add-photo-withplus"];
        cell.btn_close_b.hidden = YES;
        cell.lbl_movie_title.hidden = YES;
        cell.btn_play_b.hidden=YES;
    }
    else
    {
        cell.btn_close_b.hidden = NO;
        cell.lbl_movie_title.hidden = YES;
        cell.btn_play_b.hidden=YES;
        cell.btn_close_b.tag = indexPath.row;
        [cell.btn_close_b addTarget:self action:@selector(masterAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.img_movi_image.image = AddNew[indexPath.row];
        
        //[cell.img_movi_image sd_setImageWithURL:AddNew[indexPath.row] placeholderImage:[UIImage imageNamed:@"Avatar"]];
        //cell.Img_Photos.image=[UIImage imageNamed:[AddNew objectAtIndex:indexPath.row]];
    }
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    if ([AddNew[indexPath.row] isEqual:@"addNew"]) {
        ci= [[UIImagePickerController alloc] init];
        ci.delegate = self;
        ci.allowsEditing=NO;
        ci.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        
        if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
            [self presentViewController:ci animated:YES completion:nil];
        else
        {
            popover=[[UIPopoverController alloc]initWithContentViewController:ci];
            [popover presentPopoverFromRect:_img_newimg.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
    }
    
    
    
}

-(void)masterAction:(UIButton*)sender
{
    UIAlertController * alert=[UIAlertController
                               
                               alertControllerWithTitle:@"" message:@"Are you sure you want to delete"preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Yes"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    //What we write here????????**
                                    NSLog(@"YES");
                                    
                                    [AddNew removeObjectAtIndex:sender.tag];
                                    
                                    [_coll_addClassi reloadData];
                                    
                                }];
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"No"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   //What we write here????????**
                                   NSLog(@"NO");
                                   
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}


-(CLLocationCoordinate2D) getLocationFromAddressString: (NSString*) addressStr {
    double latitude = 0, longitude = 0;
    NSString *esc_addr =  [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"https://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result) {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }
    CLLocationCoordinate2D center;
    center.latitude=latitude;
    center.longitude = longitude;
    _txt_lat.text=[NSString stringWithFormat:@"%f",center.latitude];
    _txt_longi.text=[NSString stringWithFormat:@"%f",center.longitude];
    
    
    NSLog(@"View Controller get Location Logitute : %f",center.latitude);
    NSLog(@"View Controller get Location Latitute : %f",center.longitude);
    return center;
    
}





-(void)CallGetCategory
{
    [APP_DELEGATE showLoadingView:@""];
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    
    [manager POST:[NSString stringWithFormat:@"%@get_profession",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        dict=[responseObject valueForKey:@"data"];
        categor= [responseObject valueForKey:@"data"];
        NSLog(@"%@",categor);
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    
    
}


#pragma mark - GetAllPosts
-(void)CallAddClassi
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:_txt_title.text forKey:@"title"];
    [AddPost setValue:@"" forKey:@"address"];
    [AddPost setValue:countryid forKey:@"country"];
    [AddPost setValue:stateid forKey:@"state"];
    [AddPost setValue:cityid forKey:@"city"];
    [AddPost setValue:_txt_lat.text forKey:@"letitude"];
    [AddPost setValue:_txt_longi.text forKey:@"longitude"];
    [AddPost setValue:_txt_contact.text forKey:@"contact_no"];
    [AddPost setValue:_txt_discr.text forKey:@"description"];//
    [AddPost setValue:categorid forKey:@"category_id"];//classified_image//price
    
    
    
    //Create manager
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"application/json", nil];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    //Now post
    NSLog(@"AddPost %@",AddPost);
    NSString *url = [NSString stringWithFormat:@"%@add_classified",JsonUrlConstant];
    [manager POST:url parameters:[AddPost copy] constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        //add img data one by one
        
        
        
        
        for(int i=0; i<[AddNew count];i++)
        {
            
            if(i!=0){
                
                
                UIImage *eachImage  = [AddNew objectAtIndex:i];
                NSData *imageData = UIImageJPEGRepresentation(eachImage,0.2);
              
                [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"classified_image"] fileName:[NSString stringWithFormat:@"classified_image%d.jpg",i ] mimeType:@"image/jpeg"];
            }
            
        }
        
        [APP_DELEGATE showLoadingView:@""];
        
    } success:^(AFHTTPRequestOperation *operation, id dicsResponse) {
        // [(AppDelegate *)[[UIApplication sharedApplication] delegate] hideProgress];
        NSLog(@"Success: %@", dicsResponse);
        NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
        //NSString *msg=@"Your classified has been submitted for review and when approved            posting will appear";
        
        if ([[dicsResponse valueForKey:@"status"] integerValue] == 0) {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Your classified has been submitted for review and when approved posting will appear" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            [self.navigationController popViewControllerAnimated:YES];
            
        }
        [APP_DELEGATE hideLoadingView];
        
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
              [APP_DELEGATE hideLoadingView];
          }];
    
    
}


-(void)CallAddClassifiedWithImage
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:_txt_title.text forKey:@"title"];
    [AddPost setValue:@"" forKey:@"address"];
    [AddPost setValue:countryid forKey:@"country"];
    [AddPost setValue:stateid forKey:@"state"];
    [AddPost setValue:cityid forKey:@"city"];
    [AddPost setValue:_txt_lat.text forKey:@"letitude"];
    [AddPost setValue:_txt_longi.text forKey:@"longitude"];
    [AddPost setValue:_txt_contact.text forKey:@"contact_no"];
    [AddPost setValue:_txt_discr.text forKey:@"description"];//
    [AddPost setValue:categorid forKey:@"category_id"];//classified_image
    [AddPost setValue:_txt_classi_price.text forKey:@"price"];
    
    // NSLog(@"myRequestString:%@",strURL);
    // Create Data from request
    NSString *url = [NSString stringWithFormat:@"%@add_classified",JsonUrlConstant];
    NSMutableURLRequest *urequest = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString:url]];
    
    NSLog(@"myRequestString:%@",urequest);
    
    [urequest setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [urequest setHTTPShouldHandleCookies:NO];
    [urequest setTimeoutInterval:60];
    [urequest setHTTPMethod:@"POST"];
    
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [urequest setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in AddPost) {
        NSLog(@"Param:%@",param);
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [AddPost objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // Add __VIEWSTATE
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"__VIEWSTATE\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"/wEPDwUKLTQwMjY2MDA0M2RkXtxyHItfb0ALigfUBOEHb/mYssynfUoTDJNZt/K8pDs=" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"Body:%@",body);
    
    // add image data profileImage
    
    for (int i = 0; i < AddNew.count-1; i++) {
        
        NSData* imgData = [[NSData alloc] init];
        UIImage *eachImage  = [AddNew objectAtIndex:i+1];
        
        imgData = UIImageJPEGRepresentation(eachImage, 0.5);
        
     
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"classified_image[%@]\"; filename=\"IMG_%@_%f.jpg\"\r\n",[NSString stringWithFormat:@"%d",i],[NSString stringWithFormat:@"%d",i],[[NSDate date] timeIntervalSince1970]] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:imgData];
            [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        
    }
    
    
    ///....................remove
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [urequest setHTTPBody:body];
    //return and test
    NSHTTPURLResponse *response = nil;
    NSData *returnData = [NSURLConnection sendSynchronousRequest:urequest returningResponse:&response error:nil];
    //remove..
    NSLog(@"jsourequestn :%@",urequest);
    
    NSError* error;
    // NSLog(@"return data..%@",returnData);
    if(returnData != nil)
    {
        
        NSLog(@"jsourequestn :%@",returnData);
        NSLog(@"response: %@", [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding]);
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:returnData
                                                             options:kNilOptions
                                                               error:&error];
        
        NSLog(@"json :%@",json);
        
     
        
        NSLog(@"json :%@",json);
        
        if([json valueForKey:@"status"])
        {
            UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Classified added." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [Notpermitted show];
            
            
            [self.navigationController popViewControllerAnimated:YES];
            // [self saveSignUpData:json];
           // return json;
        }else{
            UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [Notpermitted show];
            
            
           // return json;
        }
        
    }
    
}



-(void)CallCountry
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    
    NSLog(@" url is %@login",JsonUrlConstant);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpcountry  = [[HttpWrapper alloc] init];
        httpcountry.delegate=self;
        httpcountry.getbool=NO;
        [httpcountry requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_country",JsonUrlConstant] param:[AddPost copy]];
        
        
        
        
    });
}
-(void)Callstate:(NSString *)str
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:str forKey:@"country_id"];
    NSLog(@"%@",AddPost);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpstate  = [[HttpWrapper alloc] init];
        httpstate.delegate=self;
        httpstate.getbool=NO;
        [httpstate requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_state"
                                                  ,JsonUrlConstant] param:[AddPost copy]];
    });
    
}
-(void)CallCity:(NSString *)str
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:str forKey:@"state_id"];
    NSLog(@"%@",AddPost);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpcity  = [[HttpWrapper alloc] init];
        httpcity.delegate=self;
        httpcity.getbool=NO;
        [httpcity requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_city"
                                                 ,JsonUrlConstant] param:[AddPost copy]];
    });
    
}

#pragma mark- Wrapper Class Delegate Methods
//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary* )dicsResponse
{
    if(wrapper == httpaddclassi && httpaddclassi != nil)
    {
        NSLog(@"%@",dicsResponse);
        [APP_DELEGATE hideLoadingView];
    }
    else if(wrapper == httpcountry && httpcountry != nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        dict=[dicsResponse valueForKey:@"data"];
        country= [dicsResponse valueForKey:@"data"];
        NSLog(@"%@",country);
        [APP_DELEGATE hideLoadingView];
    }
    else if (wrapper==httpstate && httpstate !=nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        if ([dicsResponse valueForKey:@"status"]!=0) {
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[dicsResponse valueForKey:@"data"];
            state= [dicsResponse valueForKey:@"data"];
            NSLog(@"%@",state);
        }
        [APP_DELEGATE hideLoadingView];
    }
    else if (wrapper==httpcity && httpcity !=nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        if ([dicsResponse valueForKey:@"status"]!=0) {
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            dict=[dicsResponse valueForKey:@"data"];
            arrcity= [dicsResponse valueForKey:@"data"];
            NSLog(@"%@",arrcity);
        }
        [APP_DELEGATE hideLoadingView];
    }
    [APP_DELEGATE hideLoadingView];
    
}


//fetchDataFail
- (void) HttpWrapper:(HttpWrapper* )wrapper fetchDataFail:(NSError* )error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if([textView.text isEqual: @"Address"])
    {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];//
    }
    else if([textView.text isEqual:@"Description"])
    {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    
    return YES;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self customSetup];
}
-(void)singleTapping:(UIGestureRecognizer *)recognizer {
    NSLog(@"image clicked");
    ci= [[UIImagePickerController alloc] init];
    ci.delegate = self;
    ci.allowsEditing=NO;
    ci.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
        [self presentViewController:ci animated:YES completion:nil];
    else
    {
        popover=[[UIPopoverController alloc]initWithContentViewController:ci];
        [popover presentPopoverFromRect:_img_newimg.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}

#pragma mark - ImagePickerController Delegate
-(void)imagePickerController:(UIImagePickerController *)pick didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    /*if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        // [pick dismissViewControllerAnimated:YES completion:nil];
    } else {
        [popover dismissPopoverAnimated:YES];
    }
     */
    
    _img_newimg.image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    UIImage* originalImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    Isimage=originalImage;
    _img_newimg.image=originalImage;
    //_img_chooseimg.image=image;
    
    _img_extra.hidden=YES;
    //_lbl_extra_lab.hidden=YES;
    
    [AddNew insertObject:originalImage atIndex:[AddNew count]];
    [_coll_addClassi reloadData];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
    /*_imgCropperViewController = [[UzysImageCropperViewController alloc] initWithImage:originalImage andframeSize:ci.view.frame.size andcropSize:CGSizeMake(400, 240)];
    _imgCropperViewController.delegate = self;
    [ci presentViewController:_imgCropperViewController animated:YES completion:nil];
    [_imgCropperViewController release];
    */
    
    //[AddNew insertObject:originalImage atIndex:[AddNew count]];
    //[_coll_addClassi reloadData];
    
    
    //[self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)imageCropper:(UzysImageCropperViewController *)cropper didFinishCroppingWithImage:(UIImage *)image
{
    Isimage=image;
    _img_newimg.image=image;
    //_img_chooseimg.image=image;
    
    _img_extra.hidden=YES;
    //_lbl_extra_lab.hidden=YES;
    
    [AddNew insertObject:image atIndex:[AddNew count]];
    [_coll_addClassi reloadData];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}



-(void)imagePickerControllerDidCancel:(UIImagePickerController *)pick
{
    [pick dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-200;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}

-(void)CustomizeTextField:(UITextField *)myTextField
{
    myTextField.layer.borderWidth=1.0;
    myTextField.layer.borderColor=[UIColor colorWithHex:0xDEDEDE].CGColor;
    CGRect frameRect = myTextField.frame;
    frameRect.size.height = 28;
    myTextField.frame = frameRect;
    myTextField.layer.cornerRadius=myTextField.frame.size.height/2;
    
    
    
    if (myTextField==_txt_date) {
        [myTextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 14, 20)];
        myTextField.leftView = paddingView;
        myTextField.leftViewMode = UITextFieldViewModeAlways;
    }
    else
    {
        [myTextField setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
        myTextField.leftView = paddingView;
        myTextField.leftViewMode = UITextFieldViewModeAlways;
    }
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)btn_HOME:(id)sender {
    NewHomeVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:YES];
}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}

- (IBAction)btn_SCOUNTRY:(id)sender {
    [self.view endEditing:YES];
    isCheck=0;
    [txt_country setTag:0];
    currentTextField = txt_country;
    [self showPopover:sender];
}

- (IBAction)btn_SSTATE:(id)sender {
    [self.view endEditing:YES];
    isCheck=1;
    [txt_state setTag:1];
    currentTextField = txt_state;
    [self showPopover:sender];
}


- (IBAction)btn_SCITY:(id)sender {
    [self.view endEditing:YES];
    isCheck=2;
    [txt_city setTag:2];
    currentTextField = txt_city;
    [self showPopover:sender];
}
- (IBAction)btn_SCATEGORY:(id)sender {
    [self.view endEditing:YES];
    isCheck=3;
    [txt_category setTag:3];
    currentTextField = txt_category;
    [self showPopover:sender];
}

- (IBAction)btn_NOTIFY:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}

- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_ADD_CLASSI:(id)sender {

    if ([AddNew count]>1) {
        if ([self CheckTextValidation:_txt_title :@"please Enter Title."]) {
            if ([self CheckTextValidation:txt_country :@"please Select Country."]) {
                if ([self CheckTextValidation:txt_state :@"please Select State."]) {
                    if ([self CheckTextValidation:txt_city :@"please Select City."]) {
                        if ([self CheckTextValidation:txt_category :@"please Select Category."]) {
                            if ((![_txt_discr.text isEqualToString:@""] && ![_txt_discr.text isEqualToString:@"Description"] )) {
                                if ([self CheckTextValidation:_txt_classi_price :@"please Enter Price."]) {
                                    if ([self CheckDecimal:_txt_classi_price.text]) {
                                        [self CallAddClassifiedWithImage];
                                    }
                                    else
                                    {
                                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Enter Valid Price." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                                        [alert show];
                                    }
                                    
                                }
                            }
                            else
                            {
                                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Enter Description" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                                [alert show];
                            }
                        }
                    }
                }
            }
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please Select Image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    
}

-(BOOL)CheckTextValidation :(UITextField*)txtF : (NSString *)msg
{
    
    if ([txtF.text isEqualToString:@""]) {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
        return NO;
    }
    else
    {
        return  YES;
    }
    
    
    
}


- (void)touchesBegan:(NSSet* )touches withEvent:(UIEvent* )event {
    [self.view endEditing:YES];
}
- (void)showPopover:(UIView*)btn;
{
    
    if (popoverController == nil)
    {
        PopOverView *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PopOverView"];
        
        settingsViewController.index = currentTextField.tag;
        
        if (isCheck == 0) {
            settingsViewController.marrcountry = country;
        }
        else if (isCheck==1)
        {
            settingsViewController.marrstate = state;
        }
        else if (isCheck==2)
        {
            settingsViewController.marrcity = arrcity;
        }
        else if (isCheck==3)
        {
            settingsViewController.marrcategor = categor;
        }
        else{
            NSLog(@"NO DATA FROM API");
        }
        
        
        if ([settingsViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
            settingsViewController.preferredContentSize = CGSizeMake(200, 170);             // iOS 7
        }
        else {
#pragma clang diagnostic pushc
#pragma GCC diagnostic ignored "-Wdeprecated"
            settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 170);      // iOS < 7
#pragma clang diagnostic pop
        }
        settingsViewController.delegates = self;
        
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        
        settingsViewController.modalInPopover = NO;
        popoverController = [[WYPopoverController alloc] initWithContentViewController:settingsViewController];
        
        popoverController.delegate = self;
        
        popoverController.passthroughViews = @[btn];
        popoverController.popoverLayoutMargins = UIEdgeInsetsMake(0, 10, 10 , 10);
        popoverController.wantsDefaultContentAppearance = YES;
        
        [popoverController presentPopoverFromRect:btn.bounds
                                           inView:btn
                         permittedArrowDirections:WYPopoverArrowDirectionAny
                                         animated:YES
                                          options:WYPopoverAnimationOptionFadeWithScale];
    }
    
}

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    popoverController.delegate = nil;
    popoverController = nil;
}

-(void)dismissPopUpViewController
{
    [popoverController dismissPopoverAnimated:YES];
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)Popvalueselected:(NSString*)strValue{
    NSLog(@"POP:%@",strValue);
    
    currentTextField.text = strValue;
    
    if (isCheck==0) {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [country filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        countryid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        
        txt_state.text=@"";
        txt_city.text=@"";
        
        [self Callstate:countryid];
        
    }
    else if (isCheck==1)
    {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [state filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        stateid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        txt_city.text=@"";
        [self CallCity:stateid];
    }
    else if (isCheck==2)
    {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [arrcity filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"LocationID"]);
        cityid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"LocationID"] objectAtIndex:0]];
        
        //GET LET LONG
        [self getLocationFromAddressString:[NSString stringWithFormat:@"%@,%@,%@",txt_city.text,txt_state.text,txt_country.text]];
        
    }
    else if (isCheck==3)
    {
        NSString *stringToSearch = strValue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",stringToSearch]; // if you need case sensitive search avoid '[c]' in the predicate
        
        NSArray *results = [categor filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",[results valueForKey:@"ProfessionalID"]);
        categorid = [NSString stringWithFormat:@"%@",[[results valueForKey:@"ProfessionalID"] objectAtIndex:0]];
        
    }
    else
    {
        currentTextField.text = strValue;
    }
    
}

-(BOOL)CheckDecimal :(NSString*)strdigit
{
    NSString *pattern = @"[0-9]+(?:\\.[0-9]{2})?$";
    NSString *string = strdigit;
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:&error];
    
    NSRange textRange = NSMakeRange(0, string.length);
    NSRange matchRange = [regex rangeOfFirstMatchInString:string options:NSMatchingReportProgress range:textRange];
    
    // Did we find a matching range
    if (matchRange.location != NSNotFound)
    {
        NSLog (@"YES! It is matched!");
        return YES;
    }
    else
    {
        NSLog (@"NO MATCH!");
        return NO;
    }
}


@end
