//
//  FollowListVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 3/10/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>

@interface FollowListVC : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    int flag;
    
     //NSMutableArray *arrFollow;
     //NSMutableArray *arrFollowing;
    
}
@property ( retain , nonatomic )NSString *isFollow;
@property (retain,nonatomic)NSMutableArray *arrFollow;
@property (retain ,nonatomic)NSMutableArray *arrFollowing;


//Outlets
@property (strong, nonatomic) IBOutlet UIView *vw_followbtns;
@property (strong, nonatomic) IBOutlet UIButton *btn_followers;
@property (strong, nonatomic) IBOutlet UIButton *btn_following;
@property (strong, nonatomic) IBOutlet UIButton *btn_home;
@property (strong, nonatomic) IBOutlet UITableView *tbl_fololist;
@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;



//Action
- (IBAction)btn_FOLLOW_A:(id)sender;
- (IBAction)btn_FOLLOWING_A:(id)sender;
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_NOTIFY:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;




@end
