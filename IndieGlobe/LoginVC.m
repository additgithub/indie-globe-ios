//
//  LoginVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/24/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//
#import <OneSignal/OneSignal.h>

#import "LoginVC.h"
#import "RegisterVC.h"
#import "menubarVC.h"
#import "ForgotpassVC.h"
#import "HttpWrapper.h"
#import "ApplicationData.h"
#import "ApplicationConstant.h"
#import "NewHomeVC.h"


#define REGEX_USER_NAME_LIMIT @"^.{1,10}$"
#define REGEX_USER_NAME @"[A-Za-z0-9]{3,10}"
#define REGEX_EMAIL @"[A-Z0-9a-z._%+-]{1,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
#define REGEX_PASSWORD_LIMIT @"^.{6,20}$"
#define REGEX_PASSWORD @"[A-Za-z0-9]{6,20}"
#define REGEX_PHONE_DEFAULT @"[0-9]{3}\\-[0-9]{3}\\-[0-9]{4}"





@interface LoginVC ()

@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    isChecked=false;
    [self setupAlerts];
    
    //NSArray *arr=[[NSArray alloc]init];
    //arr=[arr objectAtIndex:2];
    
    
    [OneSignal IdsAvailable:^(NSString *userId, NSString *pushToken) {
        if (pushToken)
            
            textMultiLine1 = [NSString stringWithFormat:@"PlayerId:\n%@\n\nPushToken:\n%@\n", userId, pushToken];
        
        else
            textMultiLine1 = @"ERROR: Could not get a pushToken from Apple! Make sure your provisioning profile has 'Push Notifications' enabled and rebuild your app.";
        
        NSLog(@"textMultiLine1---%@", textMultiLine1);
        
        PlayerId = [NSString stringWithFormat:@"%@", userId];
        NSLog(@"PlayerId %@",PlayerId);
    }];

    
    
    
    // Do any additional setup after loading the view.
    
    _txt_username.placeholder=@"EMAIL";
    _txt_password.placeholder=@"PASSWORD";
    
    [self textlayers:_txt_username];
    [self textlayers:_txt_password];
    
   

    
    _btn_login.layer.cornerRadius=16.0;
    
   // [self CallMyMethod:@"Admin" :@"Admin"];
    
    //Remember User
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"remember"]) {
        NSLog(@"remember no");
    } else {
        NSLog(@"remember yes");
        [_img_chkbox setImage:[UIImage imageNamed:@"check-white-with-check.png"]];
        isChecked = YES;
        
        NSDictionary * myDictionary = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"dictionaryKey"];
        NSLog(@"dictionary %@",myDictionary);
        _txt_username.text = [myDictionary valueForKey:@"username"];
        _txt_password.text = [myDictionary valueForKey:@"password"];
        
        double delayInSeconds = 0.3;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            NSLog(@"Do some work");
            
            [self CallLogin];
            
        });
        
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)textlayers :(UITextField *)UITextField
{
    
    UITextField.layer.borderColor=[[UIColor clearColor]CGColor];
    UITextField.layer.borderWidth=1.5;
    UITextField.layer.borderColor=[[UIColor whiteColor] CGColor];
    UITextField.layer.cornerRadius=15.0;
    [UITextField setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
    UITextField.leftView = paddingView;
    UITextField.leftViewMode = UITextFieldViewModeAlways;
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch * touch = [touches anyObject];
    if(touch.phase == UITouchPhaseBegan) {
        [_txt_username resignFirstResponder];
        [_txt_password resignFirstResponder];
    }
}


#pragma mark - GetAllPosts
-(void)CallLogin
{
    [APP_DELEGATE showLoadingView:@""];

  //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    [AddPost setValue:_txt_username.text forKey:@"email"];
    [AddPost setValue:_txt_password.text forKey:@"password"];
    [AddPost setValue:PlayerId forKey:@"token_id"];
    //------------
    NSLog(@"PARAM ---%@",AddPost);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];

    [manager POST:[NSString stringWithFormat:@"%@login",JsonUrlConstant] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[responseObject valueForKey:@"data"];
        NSString *msg=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"message"]];
        
        if ([[responseObject valueForKey:@"status"] integerValue] == 0) {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Login" message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            [APP_DELEGATE hideLoadingView];
        }
        else
        {
            NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
            [standardUserDefaults setObject:[dic valueForKey:@"UserID"] forKey:@"UserID"];
            [standardUserDefaults setObject:[dic valueForKey:@"Email"] forKey:@"Email"];
            [standardUserDefaults setObject:[dic valueForKey:@"FName"] forKey:@"FName"];
            [standardUserDefaults setObject:[dic valueForKey:@"LName"] forKey:@"LName"];
            [standardUserDefaults setObject:[dic valueForKey:@"PlanID"] forKey:@"PlanID"];
            [standardUserDefaults setObject:[dic valueForKey:@"PlanTerms"] forKey:@"PlanTerms"];
            [standardUserDefaults setObject:[dic valueForKey:@"PhotoURL"] forKey:@"PhotoURL"];
            [standardUserDefaults setObject:[dic valueForKey:@"PlanName"] forKey:@"PlanName"];
            [standardUserDefaults setObject:[dic valueForKey:@"NotificationStatus"] forKey:@"notification_status"];
            
            [standardUserDefaults setObject:[dic valueForKey:@"CountryID"] forKey:@"CountryID"];
            [standardUserDefaults setObject:[dic valueForKey:@"StateID"] forKey:@"StateID"];
            [standardUserDefaults setObject:[dic valueForKey:@"CityID"] forKey:@"CityID"];
            [standardUserDefaults setObject:[dic valueForKey:@"ImageURL"] forKey:@"ImageURL"];
            
            int msge=[[responseObject valueForKey:@"total_message"] intValue];
            int req=[[responseObject valueForKey:@"total_request"] intValue];
            
            [standardUserDefaults setObject:[NSString stringWithFormat:@"%d",msge+req] forKey:@"msg_req_count"];
            
            [standardUserDefaults setObject:[dic valueForKey:@"CityID"] forKey:@"CityID"];
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Login" message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            
            
            //Remember User
            if(![[NSUserDefaults standardUserDefaults] boolForKey:@"remember"]) {
                NSLog(@"remember no");
            } else {
                NSLog(@"remember yes");
                
                NSMutableDictionary *Dict = [[NSMutableDictionary alloc]init];
                
                [Dict setValue:_txt_username.text forKey:@"username"];
                [Dict setValue:_txt_password.text forKey:@"password"];
                
                [[NSUserDefaults standardUserDefaults] setObject:Dict forKey:@"dictionaryKey"];
            }
            
           /* NewsVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsVC"];
            [self.navigationController pushViewController:next animated:YES];*/
            NewHomeVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
            [self.navigationController pushViewController:next animated:YES];
        }
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        dict=[responseObject valueForKey:@"dat"];
        [APP_DELEGATE hideLoadingView];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];

}

#pragma mark- Wrapper Class Delegate Methods

//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary* )dicsResponse
{
    if(wrapper == httplogin && httplogin != nil)
    {
        NSLog(@"Responce : %@",dicsResponse);
        
        NSLog(@"DATA :%@ ",[dicsResponse valueForKey:@"data"]);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
        
        if ([[dicsResponse valueForKey:@"status"] integerValue] == 0) {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Login" message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //[alert show];
           
        }
        else
        {
            NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
            [standardUserDefaults setObject:[dic valueForKey:@"UserID"] forKey:@"UserID"];
            [standardUserDefaults setObject:[dic valueForKey:@"Email"] forKey:@"Email"];
            [standardUserDefaults setObject:[dic valueForKey:@"FName"] forKey:@"FName"];
            [standardUserDefaults setObject:[dic valueForKey:@"LName"] forKey:@"LName"];
            [standardUserDefaults setObject:[dic valueForKey:@"PlanID"] forKey:@"PlanID"];
            [standardUserDefaults setObject:[dic valueForKey:@"PhotoURL"] forKey:@"PhotoURL"];
            [standardUserDefaults setObject:[dic valueForKey:@"PlanName"] forKey:@"PlanName"];
            [standardUserDefaults setObject:[dic valueForKey:@"ImageURL"] forKey:@"ImageURL"];
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Login" message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //[alert show];
            
            
            //Remember User
            if(![[NSUserDefaults standardUserDefaults] boolForKey:@"remember"]) {
                NSLog(@"remember no");
            } else {
                NSLog(@"remember yes");
                
                NSMutableDictionary *Dict = [[NSMutableDictionary alloc]init];
                
                [Dict setValue:_txt_username.text forKey:@"username"];
                [Dict setValue:_txt_password.text forKey:@"password"];
                
                [[NSUserDefaults standardUserDefaults] setObject:Dict forKey:@"dictionaryKey"];
            }
            
            NewsVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsVC"];
            [self.navigationController pushViewController:next animated:YES];
        }
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
        dict=[dicsResponse valueForKey:@"data"];
        
    }
}


//fetchDataFail
- (void) HttpWrapper:(HttpWrapper* )wrapper fetchDataFail:(NSError* )error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)setupAlerts{
    [_txt_username addRegx:REGEX_EMAIL withMsg:@"Please enter valid email."];
    
    [_txt_password addRegx:REGEX_PASSWORD_LIMIT withMsg:@"Password characters limit should be come between 6-20"];
    
}

-(void)validationcheck
{
    
    if (![_txt_username.text isEqualToString:@""] && ![_txt_password.text isEqualToString:@""]) {
        NSLog(@" valid !!");
    }
    else
    {
        NSLog(@"not valid !!");

    }
    
}


- (IBAction)btn_LOGIN:(id)sender {
    
    //[self validationcheck];
    
    if([_txt_username validate] & [_txt_password validate] ){
        /* secondViewController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SecondViewController"];
         [self.navigationController pushViewController:secondViewController animated:YES];*/
        
        if ([self connectedToInternet]) {
            [self CallLogin];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Login" message:@"Internet not connected." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
        
        
    }
    //NewHomeVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    //[self.navigationController pushViewController:next animated:YES];
    
}

- (IBAction)btn_FORGOT:(id)sender {
    
    ForgotpassVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"ForgotpassVC"];
    [self.navigationController pushViewController:next animated:YES];
}

- (IBAction)btn_SIGNUP:(id)sender {
    
    RegisterVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterVC"];
    [self.navigationController pushViewController:next animated:YES];
    
}

- (IBAction)btn_CHKBOX:(id)sender {
    
    if (isChecked) {
        
        isChecked =false;
        [_img_chkbox setImage:[UIImage imageNamed:@"check-white.png"]];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"remember"];//Remember User
    }else{
        isChecked = true;
        [_img_chkbox setImage:[UIImage imageNamed:@"check-white-with-check.png"]];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"remember"];//Remember User
    }

}




- (IBAction)btn_MENUBAR:(id)sender {
    
   /* menubarVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"menubarVC"];
    [self.navigationController pushViewController:next animated:YES];*/
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    menubarVC *VC = [storyboard instantiateViewControllerWithIdentifier:@"menubarVC"];
                          CATransition *transition = [CATransition animation];
                          transition.duration = 0.45;
                          transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
                          transition.type = kCATransitionPush;
                          transition.subtype = kCATransitionFromLeft;
                          transition.delegate = self;
                          [self.navigationController.view.layer addAnimation:transition forKey:nil];
                          [self.navigationController pushViewController:VC animated:NO];
    
    
}

- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)connectedToInternet
{
    NSString *urlString = @"http://www.google.com/";
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    [request setTimeoutInterval:10];
    NSHTTPURLResponse *response;
    
    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error: NULL];
    
    return ([response statusCode] == 200) ? YES : NO;
}


@end
