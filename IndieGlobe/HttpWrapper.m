
//
//  HttpWrapper.m
//  Towing Service
//
//  Created by RAHUL MEHTA on 03/03/17.
//  Copyright © 2017 RAHUL MEHTA. All rights reserved.
//

#import "HttpWrapper.h"
#import "AppDelegate.h"
#import <AFNetworking/UIKit+AFNetworking.h>
#import <AFNetworking/AFHTTPRequestOperation.h>


@implementation HttpWrapper

AppDelegate *appDelegate;

@synthesize requestMain;
@synthesize delegate;

-(id)init
{
    self = [super init];
    if(self)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        });
        
    }
    return self;
}
/*
-(void) requestWithMethod:(NSString*)method url:(NSString*)strUrl param:(NSMutableDictionary*)dictParam
{
    NSLog(@"dict is %@",dictParam);
    if(requestMain)
    {
        requestMain = nil;
    }
    
    operation = [AFHTTPSessionManager manager];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    operation.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json",nil];
    operation.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    if (_getbool == YES)
    {
        [operation GET:strUrl parameters:dictParam success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (delegate != nil)
            {
                NSLog(@"dictparam %@",dictParam);
                
                if([delegate respondsToSelector:@selector(fetchDataSuccess:)])
                {
                    [delegate performSelector:@selector(fetchDataSuccess:) withObject:responseObject];
                }
                
                if([delegate respondsToSelector:@selector(HttpWrapper:fetchDataSuccess:)])
                {
                    [delegate HttpWrapper:self fetchDataSuccess:responseObject];
                }
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            if (delegate == nil)
                return;
            
            if([delegate respondsToSelector:@selector(fetchDataFail:)])
                [delegate performSelector:@selector(fetchDataFail:) withObject:error];
            
            if([delegate respondsToSelector:@selector(HttpWrapper:fetchDataFail:)])
                [delegate HttpWrapper:self fetchDataFail:error];
        }];
    }
    else
    {
        [operation POST:strUrl parameters:dictParam success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (delegate != nil)
            {
                NSLog(@"dictparam %@",dictParam);
                
                if([delegate respondsToSelector:@selector(fetchDataSuccess:)])
                {
                    [delegate performSelector:@selector(fetchDataSuccess:) withObject:responseObject];
                }
                
                if([delegate respondsToSelector:@selector(HttpWrapper:fetchDataSuccess:)])
                {
                    [delegate HttpWrapper:self fetchDataSuccess:responseObject];
                }
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            if (delegate == nil)
                return;
            
            if([delegate respondsToSelector:@selector(fetchDataFail:)])
                [delegate performSelector:@selector(fetchDataFail:) withObject:error];
            
            if([delegate respondsToSelector:@selector(HttpWrapper:fetchDataFail:)])
                [delegate HttpWrapper:self fetchDataFail:error];
        }];
    }
}*/

-(void) requestWithMethod:(NSString*)method url:(NSString*)strUrl param:(NSMutableDictionary*)dictParam
{
    NSLog(@"URL : %@",strUrl);
    
    NSLog(@"dict is %@",dictParam);
    if(requestMain)
    {
        requestMain = nil;
    }
    
   /* operation = [AFHTTPRequestOperationManager manager];
    operation.requestSerializer = [AFJSONRequestSerializer serializer];
    operation.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json",nil];
    operation.responseSerializer = [AFHTTPResponseSerializer serializer];*/
    
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
 
   // AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    
    //manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    //manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json",@"text/plain",nil];
    
    
    
    
    
    [manager POST:strUrl parameters:dictParam success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"JSON: %@", responseObject);
        
        if([delegate respondsToSelector:@selector(fetchDataSuccess:)])
        {
            [delegate performSelector:@selector(fetchDataSuccess:) withObject:responseObject];
        }
        
        if([delegate respondsToSelector:@selector(HttpWrapper:fetchDataSuccess:)])
        {
            [delegate HttpWrapper:self fetchDataSuccess:responseObject];
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
        if([delegate respondsToSelector:@selector(fetchDataFail:)])
        [delegate performSelector:@selector(fetchDataFail:) withObject:error];
        
        if([delegate respondsToSelector:@selector(HttpWrapper:fetchDataFail:)])
        [delegate HttpWrapper:self fetchDataFail:error];
    }];
    
    /*
    [operation POST:strUrl parameters:dictParam success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (delegate != nil)
        {
            NSLog(@"dictparam %@",dictParam);
           
            NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            
            if([delegate respondsToSelector:@selector(fetchDataSuccess:)])
            {
                [delegate performSelector:@selector(fetchDataSuccess:) withObject:json];
            }
            
            if([delegate respondsToSelector:@selector(HttpWrapper:fetchDataSuccess:)])
            {
                [delegate HttpWrapper:self fetchDataSuccess:json];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (delegate == nil)
            return;
        
        if([delegate respondsToSelector:@selector(fetchDataFail:)])
            [delegate performSelector:@selector(fetchDataFail:) withObject:error];
        
        if([delegate respondsToSelector:@selector(HttpWrapper:fetchDataFail:)])
            [delegate HttpWrapper:self fetchDataFail:error];
    }];*/
    
        
   // }
}



@end
