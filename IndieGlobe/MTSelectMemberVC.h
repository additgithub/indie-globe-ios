//
//  MTSelectMemberVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 4/23/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MTSelectMemberVC : UIViewController<UIAlertViewDelegate>
{
    NSMutableArray *arrSelected;
    NSMutableArray *chkArr;
    int selected;
}


@property (retain , nonatomic)NSMutableArray *Dict;
@property (strong, nonatomic) IBOutlet UIButton *btn_select_all;

@property (strong, nonatomic) IBOutlet UITableView *tbl_seelct;




- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_SELECTALL:(id)sender;
- (IBAction)btn_NEXT:(id)sender;



@end
