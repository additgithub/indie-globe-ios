//
//  MenuCell.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/5/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "MenuCell.h"
#import "UIColor+CL.h"

@implementation MenuCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _txt_selec_plan.layer.borderWidth=1;
    _txt_selec_plan.layer.borderColor=[UIColor colorWithHex:0x17c4c7].CGColor;
    _txt_selec_plan.layer.cornerRadius=_txt_selec_plan.frame.size.height/2;
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)btn_selectplan:(id)sender {
}
@end
