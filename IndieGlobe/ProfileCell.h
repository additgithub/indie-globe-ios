//
//  ProfileCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/29/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *img_photo;
@property (strong, nonatomic) IBOutlet UIImageView *img_movies;
@property (strong, nonatomic) IBOutlet UIImageView *img_sfilm;
@property (strong, nonatomic) IBOutlet UILabel *lbl_movie_title;
@property (strong, nonatomic) IBOutlet UIButton *btn_play_b;


@end
