//
//  NewsCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/28/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShadowView.h"


@interface NewsCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UIButton *btn_likenews;
@property (strong, nonatomic) IBOutlet UIButton *btn_dislikenews;
@property (strong, nonatomic) IBOutlet UIImageView *img_dislikenews;
@property (strong, nonatomic) IBOutlet UIImageView *img_likenews;


@property (strong, nonatomic) IBOutlet UIImageView *img_newsimage;
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;

@property (strong, nonatomic) IBOutlet UIButton *img_playbtn;

@property (strong, nonatomic) IBOutlet UILabel *lbl_like_count;
@property (strong, nonatomic) IBOutlet UILabel *lbl_dislike_count;
@property (strong, nonatomic) IBOutlet UILabel *lbl_time;

@property (weak, nonatomic) IBOutlet ShadowView *shadow_vw;



@end
