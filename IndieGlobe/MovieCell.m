//
//  MovieCell.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/18/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "MovieCell.h"

@implementation MovieCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _img_movies.clipsToBounds = YES;
    _img_movies.layer.masksToBounds = YES;
    _img_movies.layer.cornerRadius = 10.0f;
}

@end
