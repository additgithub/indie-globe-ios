//
//  ApplicationConstant.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 1/4/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//
//


#import "ApplicationConstant.h"
#import "ApplicationData.h"
#import "UIColor+CL.h"

#ifndef Doctor_Pocket_h
#define Doctor_Pocket_h

#ifndef __IPHONE_5_0
#warning "This project uses features only available in iOS SDK 5.0 and later."
#endif

//HUD
#define APP_DELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])



/*
//Stripe Keys
#define Publishable_Key @"pk_live_qcRmzopjw213yPDc3gl5K6zx"
#define Publishable_Test_Key @"pk_test_fkwHwMmNNIxWtlDzBcyjVJdC"
*/
//PaypalAccountProduction
#define PayPalEnvironmentProduction_Key @"AewofbqxRuMIARW3EII6ZMyN7M5evErzXgzQsw6kkjErVpcWE0K3iEyx5NnzTq5B-pEBS3xoBzW-8RCs"
#define PayPalEnvironmentSandbox_Key @"ATqUrr5VkejxbFLie-nb15jqYcAVdf4VzQIG9ouC_kHuN_O3ePRvzzFX49VpOaqUmD6vMHBTicuQ11Gk"



//LOCALHOST URL
#define JsonUrlConstant @"https://indieglobe.blenzabi.com/web_services/version_1/web_services/"
#define ImageFetchConstant @"https://indieglobe.blenzabi.com/web_services/version_1/web_services/"
#define VersionNumber @"1"

#define BaseURLAPI @"https://indieglobe.blenzabi.com/web_services/version_1/web_services/"


//Normal Alerts
#define ERROR_HEADER @"Hey there!"

//Alert Messages
#define FAIL_DATA_MSG @"Internet Connection not Detected. Please Try Again Later."

#define TOKENSTRING         @"testermanishrahul234142test"
#define DEVICENAME          @"iphone"
#define kGCMMessageIDKey    @"gcm.message_id"

#define iphone4 [UIScreen mainScreen].bounds.size.height == 480
#define placeHolderImage @"ic_avatar.png"
#define Font @"Trebuchet MS"

//Alert Define
#define Alert(title,msg)  [[[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:@"Continue" otherButtonTitles:nil, nil]  show];

#endif
