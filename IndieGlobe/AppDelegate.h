//
//  AppDelegate.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/24/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PayPalMobile.h"
#import "iCarousel.h"
#import "NewHomeSearchVC.h"
#import "SWRevealViewController.h"
#import "MenuViewController.h"
#import "Firebase.h"
#import "ApplicationConstant.h"
#import "JhtFloatingBall.h"

/** 颜色转换  例：#000000 UIColorFromRGB(0x000000) */
#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define theAppDelegate ((AppDelegate *)[UIApplication sharedApplication].delegate)

@interface AppDelegate : UIResponder <UIApplicationDelegate,JhtFloatingBallDelegate,UIApplicationDelegate,SWRevealViewControllerDelegate>
{
   FIRDatabaseReference  *newRefrence;
    
    NSUserDefaults *standardUserDefaults;
}
/** folatingball */
@property (nonatomic, strong) JhtFloatingBall *folatingball;

@property (retain , nonatomic) NSString *n_Count;


@property (strong, nonatomic) SWRevealViewController *viewController;

@property (strong, nonatomic) UIWindow *window;
//Shared Prefernce
+(AppDelegate *)sharedAppDelegate;
//External Propertys
-(NSString *)applicationDocumentDirectoryString;

+(AppDelegate *)sharedDelegate;

//PushNotification Dictionary
@property (retain, nonatomic)NSDictionary* additionalData;
@property (retain, nonatomic)NSDictionary* PayloadData;

-(void)showLoadingView:(NSString *)strMSG;
-(void)hideLoadingView;


//Inrnet check
- (BOOL)connectedToInternet;


//DateConvert
-(NSString*)DateCovertforClassified:(NSString *)strDate;
-(NSString*)DateCovertforEvent:(NSString *)strDate;









@end

