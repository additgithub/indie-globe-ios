//
//  TeamBuild1VC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/25/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
#import "ApplicationConstant.h"
#import "NewHomeSearchVC.h"

@interface TeamBuild1VC : UIViewController<UITableViewDelegate,UITableViewDataSource,HttpWrapperDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
{
    UIView *picker;
    NSMutableArray *arrimg;
    
    HttpWrapper* httpteambuild1;
    
    NSMutableArray *mutDict;
    NSString *page_no;

}


//Outlet
@property (strong, nonatomic) IBOutlet UITableView *tbl_list;

@property (strong, nonatomic) IBOutlet UIButton *btn_menu;
@property (strong, nonatomic) IBOutlet UIButton *btn_create;
@property (strong, nonatomic) IBOutlet UICollectionView *collection_view;
@property (strong, nonatomic) IBOutlet UITableView *tbl_proj_list;

@property (strong, nonatomic) IBOutlet UIImageView *img_colle_back;
@property (strong, nonatomic) IBOutlet UILabel *lbl_nodata;
@property (weak, nonatomic) IBOutlet UIButton *btn_createnew;

@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;



//Action

- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;
- (IBAction)btn_CREATE_NEW:(id)sender;
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_MENU:(id)sender;

- (IBAction)btn_NOTIFY:(id)sender;
- (IBAction)btn_CREATE_NEW:(id)sender;



@end
