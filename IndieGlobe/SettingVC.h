//
//  SettingVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/16/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopOverView.h"
#import "NewHomeSearchVC.h"

#import "Firebase.h"

@interface SettingVC : UIViewController
{
    UIView *picker;
    FIRDatabaseReference  *newRefrence;
    
}

@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;

//Outlet
@property (strong, nonatomic) IBOutlet UIButton *btn_back;
@property (strong, nonatomic) IBOutlet UISwitch *sw_notification;
@property (strong, nonatomic) IBOutlet UITextField *txt_select_plan;
@property (strong, nonatomic) IBOutlet UIButton *btn_menu;
@property (strong, nonatomic) IBOutlet UIButton *btn_chage_pas;
@property (strong, nonatomic) IBOutlet UIButton *brn_changepass;
@property (strong, nonatomic) IBOutlet UIButton *btn_logout;
@property (strong, nonatomic) IBOutlet UILabel *lbl_mailid;

@property (weak, nonatomic) IBOutlet UILabel *lbl_version;


//Action
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_BACK_ACTION:(id)sender;
- (IBAction)btn_CHOOSE_PLAN:(id)sender;
- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;
- (IBAction)btn_CHANGEPASS:(id)sender;
- (IBAction)btn_LOGOUT_ACTION:(id)sender;
- (IBAction)sw_NOTIFI:(id)sender;
- (IBAction)btn_NOTIFY:(id)sender;






@end
