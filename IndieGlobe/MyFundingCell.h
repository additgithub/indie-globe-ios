//
//  MyFundingCell.h
//  IndieGlobe
//
//  Created by mac on 14/08/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShadowView.h"


@interface MyFundingCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *img_funding;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title;

@property (weak, nonatomic) IBOutlet UIButton *btn_donate;
@property (weak, nonatomic) IBOutlet UILabel *lbl_goal;
@property (weak, nonatomic) IBOutlet UILabel *lbl_desc;

@property (weak, nonatomic) IBOutlet UIButton *btn_edit;
@property (weak, nonatomic) IBOutlet UIButton *btn_delete;
@property (weak, nonatomic) IBOutlet ShadowView *vw_shadow;

@end
