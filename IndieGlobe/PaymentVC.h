//
//  PaymentVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/22/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextFieldValidator.h"
#import "LTHMonthYearPickerView.h"
#import "ApplicationConstant.h"
#import "HttpWrapper.h"


@interface PaymentVC : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource,HttpWrapperDelegate,LTHMonthYearPickerViewDelegate,UITextFieldDelegate>
{
    //DatePicker
    UIPickerView *pickerView;
    NSMutableArray *dataArray;
    LTHMonthYearPickerView *monthYearPicker;
    NSInteger *oldLength;
    
    HttpWrapper* httppayment;
}


@property int FromPayment;

//Outlat
@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_cardnumber;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_expirydate;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_cvv;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_nameoncard;
@property (strong, nonatomic) IBOutlet UIButton *btn_paynow;
@property (strong, nonatomic) IBOutlet UIImageView *img_checkbox;
@property (strong, nonatomic) IBOutlet UIButton *btn_back_btn;


@property int ButtonFlag;






//Action
- (IBAction)btn_PAY_NOW:(id)sender;
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_BACK_ACTION:(id)sender;


- (IBAction)btn_CHECKBOX:(id)sender;





@end
