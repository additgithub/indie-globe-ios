//
//  MTSelectMemberCell.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 4/23/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "MTSelectMemberCell.h"

@implementation MTSelectMemberCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
