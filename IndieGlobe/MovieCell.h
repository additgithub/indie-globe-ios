//
//  MovieCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/18/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MovieCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *img_movies;
@property (strong, nonatomic) IBOutlet UIButton *btn_close_m;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Maxmsg_m;



@end
