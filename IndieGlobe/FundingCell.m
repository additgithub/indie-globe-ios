//
//  FundingCell.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/1/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "FundingCell.h"
#import "UIColor+CL.h"

@implementation FundingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _btn_donatenow.layer.cornerRadius=_btn_donatenow.frame.size.height/2;
    _btn_donatenow.layer.borderWidth=1;
    _btn_donatenow.layer.borderColor=[UIColor colorWithHex:0x196a84].CGColor;
    
    _btn_follow.layer.cornerRadius=_btn_follow.frame.size.height/2;
    _btn_follow.layer.borderWidth=1;
    _btn_follow.layer.borderColor=[UIColor colorWithHex:0x196a84].CGColor;
    
    _txt_desc.textContainerInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    _img_profile.layer.borderWidth=1;
    _img_profile.layer.borderColor=[UIColor colorWithRed:(220.0f/255.0) green:(220.0f/255.0) blue:(220.0f/255.0) alpha:1].CGColor;
    

    [self.vw_shadow setShadow];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
