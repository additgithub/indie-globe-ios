//
//  CritiquesCell.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 1/22/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "CritiquesCell.h"

@implementation CritiquesCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _img_user_image.clipsToBounds=YES;
    _img_user_image.layer.cornerRadius=_img_user_image.frame.size.height/2;
    
    [self.shadow_vw setShadow];
    
    _img_vw1.layer.cornerRadius=_img_vw1.layer.frame.size.height/2;
    _img_vw1.clipsToBounds=YES;
    
    _img_vw2.layer.cornerRadius=_img_vw2.layer.frame.size.height/2;
    _img_vw2.clipsToBounds=YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
