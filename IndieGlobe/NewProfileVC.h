//
//  NewProfileVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/16/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
#import "ApplicationConstant.h"
#import "PopOverView.h"
#import "TextFieldValidator.h"
#import "DropDownListView.h"
#import "HttpWrapper.h"
#import <AFNetworking/AFNetworking.h>
#import "AsyncImageDetail.h"
#import "UIImageView+WebCache.h"
#import "iCarousel.h"
#import "NewHomeSearchVC.h"
#import "TTTAttributedLabel.h"
#import <MessageUI/MessageUI.h>
#import "UzysImageCropperViewController.h"


@interface NewProfileVC : UIViewController<UzysImageCropperDelegate,UICollectionViewDelegate,UICollectionViewDataSource,HttpWrapperDelegate,TTTAttributedLabelDelegate,MFMailComposeViewControllerDelegate>
{
    UIView *picker;
    //Array Allocation
    NSArray *img_prof;
    
    NSMutableArray *AddNew_movie;
    NSMutableArray *AddNew_sf;
    
    NSMutableArray *AddNew;
    NSMutableArray *FFilms;
    NSMutableArray *SFilms;
    NSMutableArray *Reel;
    NSMutableArray *BTS;
    NSMutableArray *Video;
    NSMutableArray *HShots;
    
    NSMutableArray *AddNewnm;
    NSMutableArray *FFilmsnm;
    NSMutableArray *SFilmsnm;
    NSMutableArray *Reelnm;
    NSMutableArray *BTSnm;
    NSMutableArray *Videonm;
    NSMutableArray *HShotsnm;
    
    NSMutableArray *ThumbnailCreated;
    
    UIImage *thumbnail ;
    UIImage * image;
    
    
    
    
    //Image Picker Using
    UIImagePickerController *ipc;
    
    //Views
    IBOutlet UICollectionView *Collect_Vw;
    IBOutlet UICollectionView *collection_SF;
    IBOutlet UICollectionView *collectinmovie;
    
    HttpWrapper* httpnewprof;
    
    UIImage *ISimage;
    UIImage *ISimageDtl;
    
    NSMutableArray *arrimg;
    NSMutableArray *arrimgnm;
    
    NSMutableArray *arrprotdtl;
    
    NSMutableDictionary *dictres;
    NSMutableArray * arrdictres;
    
    int resFlag;
    
    NSMutableArray *arrFollow;
    NSMutableArray *arrFollowing;
    
    
    
}

@property (nonatomic,retain) UzysImageCropperViewController *imgCropperViewController;
@property (nonatomic,retain) UIImagePickerController *imgpicker;


@property (weak, nonatomic) IBOutlet UIWebView *web_sbio;

@property (strong, nonatomic) IBOutlet TTTAttributedLabel *lbl_sbio;

@property (strong, nonatomic) IBOutlet iCarousel *vw_caro;
@property (strong, nonatomic) IBOutlet UILabel *lbl_user_age;

@property(nonatomic,retain)NSMutableDictionary *responcedic;

@property (strong, nonatomic) IBOutlet UIScrollView *scroll_vw;
@property (strong, nonatomic) IBOutlet UICollectionView *collec_vw;



@property (strong, nonatomic) IBOutlet UIButton *btn_msgme;
@property (strong, nonatomic) IBOutlet UIImageView *img_profile;
@property (strong, nonatomic) IBOutlet UIButton *btn_GO_BACK;
@property (strong, nonatomic) IBOutlet UIButton *btn_menu;
@property (strong, nonatomic) IBOutlet UILabel *lbl_username;
@property (strong, nonatomic) IBOutlet UILabel *lbl_profession;
@property (strong, nonatomic) IBOutlet UILabel *lbl_location;
@property (strong, nonatomic) IBOutlet UILabel *lbl_cover_title;
@property (strong, nonatomic) IBOutlet UIView *vw_details;

@property (strong, nonatomic) IBOutlet UILabel *lbl_cat_title;
@property (strong, nonatomic) IBOutlet UILabel *lbl_cat_id;

//VW
@property (strong, nonatomic) IBOutlet UIView *vw_gen_age;
@property (strong, nonatomic) IBOutlet UIView *vw_address;
@property (strong, nonatomic) IBOutlet UILabel *lbl_follow_count;
@property (strong, nonatomic) IBOutlet UILabel *lbl_following_count;
@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;



- (IBAction)btn_SUB_BACK:(id)sender;
- (IBAction)btn_MSG_ME:(id)sender;
- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;
- (IBAction)btn_EDIT:(id)sender;
- (IBAction)btn_MOVE_LEFT:(id)sender;
- (IBAction)btn_MOVE_RIGHT:(id)sender;
- (IBAction)btn_FOLLOWLIST:(id)sender;
- (IBAction)btn_FOLLOWING:(id)sender;

- (IBAction)btn_NOTIFY:(id)sender;
- (IBAction)btn_IMG_PROFILE:(id)sender;



@end
