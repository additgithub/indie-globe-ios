//
//  AddCriticsVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/22/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
#import "ApplicationConstant.h"
#import "NewHomeSearchVC.h"
#import "UzysImageCropperViewController.h"




@interface AddCriticsVC : UIViewController<UzysImageCropperDelegate,UIImagePickerControllerDelegate,HttpWrapperDelegate,UINavigationControllerDelegate>
{
     UIView *picker;
    UIImagePickerController *ci;
    UIPopoverController *popover;
    
    HttpWrapper* httpaddcriti;
    
    UIImage *ISimage;
    
}

@property (nonatomic,retain) UzysImageCropperViewController *imgCropperViewController;
@property (nonatomic,retain) UIImagePickerController *imgpicker;


//Outlet
@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;
@property (strong, nonatomic) IBOutlet UIButton *btn_menu;
@property (strong, nonatomic) IBOutlet UITextField *txt_title;
@property (strong, nonatomic) IBOutlet UITextField *txt_conc;
@property (strong, nonatomic) IBOutlet UITextField *txt_question;
@property (strong, nonatomic) IBOutlet UITextView *txt_short_disc;
@property (strong, nonatomic) IBOutlet UIButton *btn_sumbit_critic;
@property (strong, nonatomic) IBOutlet UIImageView *img_chooseimg;
@property (strong, nonatomic) IBOutlet UIImageView *img_extra;
@property (strong, nonatomic) IBOutlet UILabel *lbl_extra_lab;





//Action
- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;
- (IBAction)btn_SUBMIT:(id)sender;
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_NOTIFY:(id)sender;




@end
