//
//  TB1.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/25/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TB1 : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *btn_join_team;

@property (strong, nonatomic) IBOutlet UILabel *lbl_title;
@property (strong, nonatomic) IBOutlet UITextView *lbl_desc;

@property (strong, nonatomic) IBOutlet UILabel *lbl_tot_invited;
@property (strong, nonatomic) IBOutlet UILabel *lbl_tot_join;
@property (strong, nonatomic) IBOutlet UILabel *lbl_usernm;
@property (strong, nonatomic) IBOutlet UIImageView *img_user;




@property (strong, nonatomic) IBOutlet UIImageView *img_proj_image;

@end
