//
//  menubarVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/25/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "menubarVC.h"

@interface menubarVC ()

@end

@implementation menubarVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *gesRecognizer = [[UITapGestureRecognizer alloc]
                                             initWithTarget:self
                                             action:@selector(handleTap:)];
    gesRecognizer.delegate = self;
    
    
    
    // Add Gesture to your view.
    [_vw_main addGestureRecognizer:gesRecognizer];
    
    
    
    
}

- (void)handleTap:(UITapGestureRecognizer *)gestureRecognizer{
    NSLog(@"Tapped");
    [self.navigationController popViewControllerAnimated:YES];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
