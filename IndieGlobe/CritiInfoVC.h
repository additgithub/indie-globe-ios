//
//  CritiInfoVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 11/30/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
#import "ApplicationConstant.h"
#import "NewHomeSearchVC.h"
#import "ShadowView.h"




@interface CritiInfoVC : UIViewController<UITableViewDataSource,UITableViewDelegate,HttpWrapperDelegate>
{
    UIView *picker;
    
    HttpWrapper* httpcritiinfo;
    
    NSMutableArray *resDict;
    NSString *page_no;
}
@property (retain,nonatomic) NSString *fromMyCriti;

@property (nonatomic,retain)NSMutableArray *mutDict;
@property (nonatomic,retain)NSString *FromFlag;

@property (strong, nonatomic) IBOutlet UIImageView *img_image;
@property (strong, nonatomic) IBOutlet UILabel *lbl_postdt;
@property (strong, nonatomic) IBOutlet UILabel *lbl_post_name;
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;
@property (strong, nonatomic) IBOutlet UILabel *lbl_desc;
@property (strong, nonatomic) IBOutlet UIImageView *img_user_image;

@property (weak, nonatomic) IBOutlet UILabel *lbl_header_title;


//Outlet
@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;

@property (strong, nonatomic) IBOutlet UIButton *btn_menu;
@property (strong, nonatomic) IBOutlet UITableView *tbl_criritab;

@property (weak, nonatomic) IBOutlet ShadowView *vw_critiVW;

@property (strong, nonatomic) IBOutlet UIView *vw_critiVW1;

@property (strong, nonatomic) IBOutlet UIButton *btn_post_comm;
@property (strong, nonatomic) IBOutlet UITextView *txt_comment;
@property (strong, nonatomic) IBOutlet UITextView *txt_infovw;
@property (strong, nonatomic) IBOutlet UITextField *txt_opinion;
@property (strong, nonatomic) IBOutlet UITextField *txt_solution;
@property (strong, nonatomic) IBOutlet UILabel *lbl_solution_lab;
@property (strong, nonatomic) IBOutlet UILabel *lbl_other_lab;
@property (strong, nonatomic) IBOutlet UIView *vw_cellview;


@property (weak, nonatomic) IBOutlet UIView *header_vw;
@property (weak, nonatomic) IBOutlet ShadowView *shadow_vw;

@property (weak, nonatomic) IBOutlet UIButton *btn_like;
@property (weak, nonatomic) IBOutlet UILabel *lbl_like;
@property (weak, nonatomic) IBOutlet UILabel *lbl_dislike;
@property (weak, nonatomic) IBOutlet UIButton *btn_comment;
@property (weak, nonatomic) IBOutlet UILabel *lbl_comment;
@property (weak, nonatomic) IBOutlet UIButton *btn_dislike;








//Action
- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_NOTIFY:(id)sender;



@end
