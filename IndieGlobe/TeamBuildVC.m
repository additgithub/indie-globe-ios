//
//  TeamBuildVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/2/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "TeamBuildVC.h"
#import "SWRevealViewController.h"
#import "UIColor+CL.h"
#import "DeshboardVC.h"
#import "ProfileVC.h"
#import "PDVC.h"
#import "ProjectVC.h"
#import "TBCell.h"
#import "InviteTeamVC.h"
#import "Constants.h"
#import "SVPullToRefresh.h"
#import "NewHomeVC.h"
#import "AllNotificationVC.h"


@interface TeamBuildVC ()

@end

@implementation TeamBuildVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    [self customSetup];
    page_no=@"0";
    // Do any additional setup after loading the view.
    _btn_create.layer.cornerRadius=_btn_create.frame.size.height/2;
    
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview:picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];

   // [self CallGetProjects];
    
    _lbl_notifi_count.layer.cornerRadius=_lbl_notifi_count.layer.frame.size.height/2;
    _lbl_notifi_count.clipsToBounds=YES;
    _lbl_notifi_count.text=theAppDelegate.n_Count;
    
    if ([_lbl_notifi_count.text isEqualToString:@"0"]) {
        _lbl_notifi_count.hidden=YES;
    }
    else
    {
        _lbl_notifi_count.hidden=NO;
    }
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self customSetup];
    page_no=@"0";
    [self CallGetProjects];
    
}
#pragma mark - GetAllPosts
-(void)CallGetProjects
{
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",page_no] forKey:@"page_no"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    [manager POST:[NSString stringWithFormat:@"%@get_user_project_list",JsonUrlConstant] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        page_no=[responseObject valueForKey:@"page_no"];
        mutDict=[responseObject valueForKey:@"data"];
        [_tbl_proj_list reloadData];
        
     

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];

    
}
-(void)CallGetProjects1
{
        //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",page_no] forKey:@"page_no"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    [manager POST:[NSString stringWithFormat:@"%@get_user_project_list",JsonUrlConstant] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"---%@",responseObject);
        page_no=[responseObject valueForKey:@"page_no"];
        
        if ([[responseObject valueForKey:@"data"] count]>0) {
            
            int64_t delayInSeconds = 2.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                //[_tbl_event beginUpdates];
                NSArray *arr = [responseObject valueForKey:@"data"];
                NSMutableArray *newarr = [[NSMutableArray alloc] init];
                NSLog(@"---%@",newarr);
                for (NSString *str in mutDict) {
                    [newarr addObject:str];
                }
                for (int i=0; i<arr.count; i++) {
                    //[mutDict addObject:arr[i]];
                    [newarr addObject:arr[i]];
                }
                NSLog(@"---%@",newarr);
                mutDict = [[NSMutableArray alloc] init];
                for (NSString*str  in newarr) {
                    [mutDict addObject:str];
                }
                
                
                NSLog(@"--%@",mutDict);
                [_tbl_proj_list.infiniteScrollingView stopAnimating];
                [_tbl_proj_list reloadData];
                [APP_DELEGATE hideLoadingView];
                
            });
            
        }
        else
        {
            [_tbl_proj_list.infiniteScrollingView stopAnimating];
            [APP_DELEGATE hideLoadingView];
            
        }

        
        
      /*  if ([[responseObject valueForKey:@"data"] count]==0) {
            //mutDict=[[NSMutableArray alloc] init];
            [_tbl_proj_list.infiniteScrollingView stopAnimating];
        }
        else
        {
            int64_t delayInSeconds = 2.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                //[_tbl_event beginUpdates];
                
                NSArray *arr = [responseObject valueForKey:@"data"];
                if (arr.count<1) {
                    _tbl_proj_list.showsInfiniteScrolling = NO;
                }
                NSMutableArray *newarr = [[NSMutableArray alloc] init];
                NSLog(@"---%@",newarr);
                for (NSString *str in mutDict) {
                    [newarr addObject:str];
                }
                for (int i=0; i<arr.count; i++) {
                    //[mutDict addObject:arr[i]];
                    [newarr addObject:arr[i]];
                }
                NSLog(@"---%@",newarr);
                mutDict = [[NSMutableArray alloc] init];
                for (NSString*str  in newarr) {
                    [mutDict addObject:str];
                }
                [_tbl_proj_list.infiniteScrollingView stopAnimating];
                [_tbl_proj_list reloadData];
                
            });
            
        }*/
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    
}


#pragma mark- Wrapper Class Delegate Methods
//fetchDataSuccess
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary* )dicsResponse
{
    if(wrapper == httpgetpro && httpgetpro != nil)
    {
        NSLog(@"---%@",dicsResponse);
        NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
        //mutDict=[[NSMutableArray alloc] init];
        //mutDict=[dicsResponse valueForKey:@"data"];
        page_no=[dicsResponse valueForKey:@"page_no"];
    
        
        if ([[dicsResponse valueForKey:@"data"] isEqual:[NSNull null]]) {
            //mutDict=[[NSMutableArray alloc] init];
            [_tbl_proj_list.infiniteScrollingView stopAnimating];
        }
        else
        {
            int64_t delayInSeconds = 2.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                //[_tbl_event beginUpdates];
                
                NSArray *arr = [dicsResponse valueForKey:@"data"];
                if (arr.count<1) {
                    _tbl_proj_list.showsInfiniteScrolling = NO;
                }
                NSMutableArray *newarr = [[NSMutableArray alloc] init];
                NSLog(@"---%@",newarr);
                for (NSString *str in mutDict) {
                    [newarr addObject:str];
                }
                for (int i=0; i<arr.count; i++) {
                    //[mutDict addObject:arr[i]];
                    [newarr addObject:arr[i]];
                }
                NSLog(@"---%@",newarr);
                mutDict = [[NSMutableArray alloc] init];
                for (NSString*str  in newarr) {
                    [mutDict addObject:str];
                }
                [_tbl_proj_list.infiniteScrollingView stopAnimating];
                [_tbl_proj_list reloadData];
                
            });
            
        }

    }
}


//fetchDataFail
- (void) HttpWrapper:(HttpWrapper* )wrapper fetchDataFail:(NSError* )error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if (IsIphone7 || IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = 145;
    }
    else{
        self.revealViewController.rearViewRevealWidth = 145;
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return mutDict.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Cell";
    
    //tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.separatorColor = [UIColor clearColor];
    
    TBCell *cell = [[TBCell alloc]init];
    cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[TBCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
   // UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell.btn_add_memb.tag = indexPath.row;
    [cell.btn_add_memb addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];

    
    cell.lbl_title.text=[NSString stringWithFormat:@"%@",[[mutDict  valueForKey:@"ProjectTitle"] objectAtIndex:indexPath.row]];
    cell.txt_desc.text=[NSString stringWithFormat:@"%@",[[mutDict  valueForKey:@"ShortDesc"] objectAtIndex:indexPath.row]];
    
    cell.lbl_joined.text=[NSString stringWithFormat:@"%@ Joined",[[mutDict valueForKey:@"TotalJoined"] objectAtIndex:indexPath.row]];
     cell.lbl_invited.text=[NSString stringWithFormat:@"%@ Invited",[[mutDict valueForKey:@"TotalInvited"] objectAtIndex:indexPath.row]];
    
    [cell.img_proj_image sd_setImageWithURL:[NSURL URLWithString:[mutDict valueForKey:@"ProjectImage"][indexPath.row]] placeholderImage:[UIImage imageNamed:@"No_IMAGE_PLACE"]];
    
    cell.lbl_usernm.text=[NSString stringWithFormat:@"%@ %@",[mutDict valueForKey:@"FName"][indexPath.row],[mutDict valueForKey:@"LName"][indexPath.row]];
    [cell.img_user sd_setImageWithURL:[NSURL URLWithString:[mutDict valueForKey:@"ImageURL"][indexPath.row]] placeholderImage:[UIImage imageNamed:@"Avatar"]];
    
    [cell.img_user setContentMode:UIViewContentModeScaleAspectFill];
    [cell.img_user setClipsToBounds:YES];
    
    [cell.img_proj_image setContentMode:UIViewContentModeScaleAspectFill];
    [cell.img_proj_image setClipsToBounds:YES];
    
    //cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
    return cell;
}
-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    int lastRow=[mutDict count]-1;
    
    if(([indexPath row] == lastRow)&&(lastRow<[mutDict count]))
    {
        //[self Getnewdata];
        NSLog(@"i m call");
        
        if ([mutDict count]>=2) {
            __weak TeamBuildVC *weakSelf = self;
            // setup infinite scrolling
            [_tbl_proj_list addInfiniteScrollingWithActionHandler:^{
                [weakSelf CallGetProjects1];
            }];
        }
        
    }

}



-(void)yourButtonClicked:(UIButton*)sender
{
        // Your code here
        NSLog(@"Button click");
          NSLog(@"buttonPressedDeny: %ld", (long)sender.tag);
        
        InviteTeamVC* next = [self.storyboard instantiateViewControllerWithIdentifier:@"InviteTeamVC"];
        next.projID=[[mutDict valueForKey:@"ProjectID"] objectAtIndex:sender.tag];
        
        [self.navigationController pushViewController:next animated:YES];

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.selected) {
        // ... Uncheck
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
   

    
    PDVC* next = [self.storyboard instantiateViewControllerWithIdentifier:@"PDVC"];
    next.mutDict=[mutDict objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:next animated:YES];
    
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_HOME:(id)sender {
    NewHomeVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeVC"];
    [self.navigationController pushViewController:next animated:YES];

}

- (IBAction)btn_SEARCH:(id)sender {
    NewHomeSearchVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"NewHomeSearchVC"];
    [self.navigationController pushViewController:next animated:NO];
}



- (IBAction)btn_CREATE_NEW:(id)sender {
     ProjectVC* next = [self.storyboard instantiateViewControllerWithIdentifier:@"ProjectVC"];
    [self.navigationController pushViewController:next animated:YES];

    
}

- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_NOTIFY:(id)sender {
    AllNotificationVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"AllNotificationVC"];
    [self.navigationController pushViewController:next animated:NO];
}
@end
