//
//  HomeSearchCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 6/28/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeSearchCell : UITableViewCell


//Outlets
@property (weak, nonatomic) IBOutlet UIImageView *img_user;
@property (weak, nonatomic) IBOutlet UILabel *lbl_usernm;
@property (weak, nonatomic) IBOutlet UILabel *lbl_profe;
@property (weak, nonatomic) IBOutlet UIButton *btn_follow;




//Action




@end
