//
//  PopOverView.m
//  f2f
//
//  Created by Parth Patoliya on 11/05/16.
//  Copyright (c) 2016 rishi. All rights reserved.
//

#import "PopOverView.h"

@interface PopOverView ()

@end

@implementation PopOverView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _tblPopOver.delegate = self;
    _tblPopOver.dataSource = self;
    
  //  arrHostel = [[NSMutableArray alloc]initWithObjects:@"HOSTELS",@"MUMBAI - ANDHERI BOYS",@"MUMBAI-SANDHRUST ROAD BOYS",@"VADODARA BOYS",@"VALLABH VIDYANAGAR BOYS",@"VALLABH VIDYANAGAR GIRLS",@"AMEHADABAD BOYS",@"AMEHADABAD GIRLS",@"PUNE BOYS",@"PUNE GIRLS",@"BHAVNAGAR BOYS",@"UDAIPUR BOYS",nil];
    arrYear = [[NSMutableArray alloc]initWithObjects:@"AL",@"ST", nil];
    arrMonth = [[NSMutableArray alloc]initWithObjects:@"JANUARY",@"FEBRUARY",@"MARCH",@"APRIL",@"MAY",@"JUNE",@"JULY",@"AUGUST",@"SEPTEMBER",@"OCTOBER",@"NOVEMBER",@"DECEMBER", nil];
    arrstate = [[NSMutableArray alloc]initWithObjects:@"MALE",@"FEMALE",nil];
      arrtype = [[NSMutableArray alloc]initWithObjects:@"Rajkot",@"Surat",@"Jamnagar",nil];
     arrhours = [[NSMutableArray alloc]initWithObjects:@"Delay by 30 min",@"Delay by 1 hour",@"Delay by 2 hour",nil];
    
    ///INDIE-------
    
    arrCity = [[NSMutableArray alloc]initWithObjects:@"Los Angeles",@"Washington",@"San Francisco",nil];
    arrcountry = [[NSMutableArray alloc]initWithObjects:@"USA",@"Canada",nil];
    arrstate = [[NSMutableArray alloc]initWithObjects:@"California",@"Florida",@"New York",nil];
    arrCategory = [[NSMutableArray alloc]initWithObjects:@"Manager",@"Director",@"Artiest",nil];
    arrsortby = [[NSMutableArray alloc]initWithObjects:@"Date",@"Name",@"Location",nil];
    arrlearn = [[NSMutableArray alloc]initWithObjects:@"Diu",@"Shomnath",@"Veraval",nil];
    arrprofe = [[NSMutableArray alloc]initWithObjects:@"JOB",@"BUSINESS",nil];
    
    arrDuration = [[NSMutableArray alloc]initWithObjects:@"30 Days",@"60 Days",@"90 Days",nil];
    
    

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    switch (self.index) {
        case 0:
        {
            arrCurrent = _marrcountry;
            ischecked = 0;
        }
            break;
        case 1:
        {
            arrCurrent = _marrstate;
            ischecked = 1;
        }
            break;
        case 2:
        {
            arrCurrent = _marrcity;
            ischecked = 2;
        }
            break;
        case 3:
        {
            arrCurrent = _marrcategor;
            ischecked = 3;
        }
            break;
        case 4:
        {
            arrCurrent = _marrdays;
            ischecked = 4;
        }
            break;
        case 5:
        {
            arrCurrent = _marrgender;
            ischecked=5;
        }
            break;
        case 6:
        {
            arrCurrent = arrprofe;
        }
            break;
        case 7:
        {
            arrCurrent = arrcountry;
        }
            break;
        case 8:
        {
            arrCurrent = arrstate;
        }
            break;
        case 11:
        {
            arrCurrent = arrCity;
        }
            break;
        case 12:
        {
            arrCurrent = arrstate;
        }
            break;
        case 13:
        {
            arrCurrent = arrcountry;
        }
            break;
        case 14:
        {
         arrCurrent = arrDuration;
        }
            break;

    }

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrCurrent.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.textLabel.textColor = [UIColor blackColor];
    cell.backgroundColor = [UIColor clearColor];
    cell.tag = indexPath.row;
    //cell.textLabel.text = [arrCurrent objectAtIndex:indexPath.row];
    if (ischecked == 0) {
       // cell.textLabel.text = [arrCurrent objectAtIndex:indexPath.row];
         cell.textLabel.text = [[arrCurrent objectAtIndex:indexPath.row] valueForKey:@"LocationName"];
         ischecked = 0;
               
        NSLog(@"%@",cell.textLabel.text);
       
    }
    else if (ischecked == 1) {
        // cell.textLabel.text = [arrCurrent objectAtIndex:indexPath.row];
        cell.textLabel.text = [[arrCurrent objectAtIndex:indexPath.row] valueForKey:@"LocationName"];
        ischecked = 1;
        
        NSLog(@"%@",cell.textLabel.text);
        
    }
    else if (ischecked == 2) {
        // cell.textLabel.text = [arrCurrent objectAtIndex:indexPath.row];
        cell.textLabel.text = [[arrCurrent objectAtIndex:indexPath.row] valueForKey:@"LocationName"];
        ischecked = 2;
        
        NSLog(@"%@",cell.textLabel.text);
        
    }
    else if (ischecked == 3) {
        // cell.textLabel.text = [arrCurrent objectAtIndex:indexPath.row];
        cell.textLabel.text = [[arrCurrent objectAtIndex:indexPath.row] valueForKey:@"Profession"];
        ischecked = 3;
        
        NSLog(@"%@",cell.textLabel.text);
        
    }
    else if (ischecked == 4) {
        // cell.textLabel.text = [arrCurrent objectAtIndex:indexPath.row];
        cell.textLabel.text = [arrCurrent objectAtIndex:indexPath.row];
        ischecked = 4;
        
        NSLog(@"%@",cell.textLabel.text);
        
    }
    else if (ischecked == 5) {
        // cell.textLabel.text = [arrCurrent objectAtIndex:indexPath.row];
        cell.textLabel.text = [arrCurrent objectAtIndex:indexPath.row];
        ischecked = 5;
        
        NSLog(@"%@",cell.textLabel.text);
        
    }
    else
    {
        cell.textLabel.text = [arrCurrent objectAtIndex:indexPath.row];
        
    }
    //self.tblPopOver.frame = CGRectMake(0,0,cell.frame.size.width,(cell.frame.size.height)*(arrCurrent.count));
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([self.delegates respondsToSelector:@selector(Popvalueselected:)])
    {
        
        //[self.delegates Popvalueselected:[arrCurrent objectAtIndex:indexPath.row]];
        
        if (ischecked == 0) {
           [self.delegates Popvalueselected:[[arrCurrent objectAtIndex:indexPath.row]valueForKey:@"LocationName"]];
        }
        else if (ischecked == 1) {
            [self.delegates Popvalueselected:[[arrCurrent objectAtIndex:indexPath.row]valueForKey:@"LocationName"]];
        }
        else if (ischecked == 2) {
            [self.delegates Popvalueselected:[[arrCurrent objectAtIndex:indexPath.row]valueForKey:@"LocationName"]];
        }
        else if (ischecked == 3) {
            [self.delegates Popvalueselected:[[arrCurrent objectAtIndex:indexPath.row]valueForKey:@"Profession"]];
        }
        else if (ischecked == 4) {
            [self.delegates Popvalueselected:[arrCurrent objectAtIndex:indexPath.row]];
        }
        else if (ischecked == 5) {
            [self.delegates Popvalueselected:[arrCurrent objectAtIndex:indexPath.row]];
        }
        else
        {
            [self.delegates Popvalueselected:[arrCurrent objectAtIndex:indexPath.row]];
        }
       
        
        [self.delegates dismissPopUpViewController];

    }
}

@end
