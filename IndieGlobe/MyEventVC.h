//
//  MyEventVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/7/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopOverView.h"
#import "IGCMenu.h"
#import "DeshboardVC.h"
#import "ProfileVC.h"
#import "CritiquesVC.h"
#import "ClassifiedVC.h"
#import "PackageList.h"
#import "LearningVC.h"
#import "EventVC.h"
#import "TeamBuildVC.h"
#import "UIViewController+ENPopUp.h"
#import "MyEventVC.h"
#import "SettingVC.h"
#import "NewProfileVC.h"
#import "IndieVC.h"
#import "FundingVC.h"
#import "NewsVC.h"
#import "HttpWrapper.h"
#import "ApplicationConstant.h"
#import "NewHomeSearchVC.h"

@interface MyEventVC : UIViewController<UITableViewDataSource,UITableViewDelegate,IGCMenuDelegate,HttpWrapperDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
{
    UIView *picker;
    
    UITextField *currentTextField;
    NSMutableArray *arrimg;
    
    HttpWrapper* httpmyevent;
        
    
    //RESPONCE DICT
    NSMutableArray *mutDict;
    NSString *page_no;
 

}


///Outlet

@property (weak, nonatomic) IBOutlet UIButton *btn_addevent;


@property (strong, nonatomic) IBOutlet UIButton *btn_menu;
@property (strong, nonatomic) IBOutlet UITextField *txt_searchbox;
@property (strong, nonatomic) IBOutlet UIButton *btn_searchbtn;
@property (strong, nonatomic) IBOutlet UITableView *lbl_myevent;

@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;

///Action
- (IBAction)btn_ADDEVENT:(id)sender;

- (IBAction)btn_NOTIFY:(id)sender;

- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;
- (IBAction)btn_EDIT_EVENT:(id)sender;
- (IBAction)btn_BACK:(id)sender;


@end
