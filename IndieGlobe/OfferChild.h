//
//  FoodChild.h
//  SMJV
//
//  Created by My Mac on 12/07/17.
//  Copyright © 2017 rishi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OfferChild : NSObject
//@property (assign) int FoodID;
@property (nonatomic,retain) NSString *Comment;
@property (nonatomic,retain) NSString *CommentDate;
@property (nonatomic,retain) NSString *CommentID;
@property (nonatomic,retain) NSString *CreatedBy;
@property (nonatomic,retain) NSString *CreatedOn;
@property (nonatomic,retain) NSString *FlexID;
@property (nonatomic,retain) NSString *ModifiedBy;
@property (nonatomic,retain) NSString *ModifiedOn;
@property (nonatomic,retain) NSString *UserID;
@property (nonatomic,retain) NSString *UserImage;
@property (nonatomic,retain) NSString *username;

@end
