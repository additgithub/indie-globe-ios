//
//  WebPlayerVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 3/26/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebPlayerVC : UIViewController



@property(retain,nonatomic)NSString *strurl;


//Outlet
@property (strong, nonatomic) IBOutlet UIWebView *web_view;


//Action
- (IBAction)btn_BACK:(id)sender;

@end
