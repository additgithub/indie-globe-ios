//
//  ReplayVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 6/8/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "ReplayVC.h"
#import "ReplayCell.h"
#import "Constants.h"
#import "ApplicationConstant.h"
#import "SDWebImageCompat.h"
#import "HttpWrapper.h"
#import "UIImage+HanekeDemo.h"
#import "UIImageView+WebCache.h"
#import "AppDelegate.h"



@interface ReplayVC ()

@end

@implementation ReplayVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    NSLog(@"DICT1 %@",_Dict1);///Comment Array
    NSLog(@"DICT2 %@",_Dict2);///Cretiques Details
    
    
    NSUserDefaults *userD=[[NSUserDefaults alloc]init];
    
    if ([[userD valueForKey:@"critiComment"]isEqualToString:@"1"]) {
        Dict3=[[NSMutableArray alloc]init];
        for (int i=0; i<_Dict1.count; i++) {
            if ([[[_Dict1 valueForKey:@"ParentCritiqueReplyID"] objectAtIndex:i]isEqualToString:_critiID]) {
                NSLog(@"----");
                //[Dict3 setDictionary:[_Dict1 objectAtIndex:i]];
                [Dict3 addObject:[_Dict1 objectAtIndex:i]];
            }
            
        }
        
    }
    else
    {
        Dict3=[[NSMutableArray alloc]init];
        Dict3=[userD valueForKey:@"arrcritiComment"];
        
    }
    
    
    if (Dict3.count<1) {
        [_lbl_nodata setCenter:self.view.center];
        _lbl_nodata.hidden=NO;
    }
    else
    {
        _lbl_nodata.hidden=YES;
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return Dict3.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //static NSString *simpleTableIdentifier = @"CellComment";
    //tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    //tableView.separatorColor = [UIColor clearColor];
    
    
    static NSString * CellIdentifier = @"Cell";
    ReplayCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
   // ReplayCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (cell == nil) {
        cell = [[ReplayCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    cell.backgroundColor = [UIColor whiteColor];
    //==========================DATE FORMATE================
    NSString * yourJSONString = [[Dict3 valueForKey:@"CommentedDate"] objectAtIndex:indexPath.row];
    
    NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
    [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateFromString = [currentDTFormatter dateFromString:yourJSONString];
    [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *eventDateStr = [currentDTFormatter stringFromDate:dateFromString];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *currentDate = [NSDate date];
    NSString *start = [formatter stringFromDate:dateFromString];
    NSString *end = [formatter stringFromDate:currentDate];
    //NSString *end = @"2018-01-11";
    
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *startDate = [f dateFromString:start];
    NSDate *endDate = [f dateFromString:end];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:(NSCalendarUnitDay |NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitWeekOfYear)
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    
    NSLog(@"Day %ld", [components day]);
    NSLog(@"Hour %ld", [components hour]);
    NSLog(@"Minute %ld", [components minute]);
    
    
    
    if ([components weekOfYear]>0) {
        cell.lbl_postdt.text=[NSString stringWithFormat:@"%ld week ago",(long)[components weekOfYear]];
    }
    else if ([components hour]>0) {
        cell.lbl_postdt.text=[NSString stringWithFormat:@"%ld hours ago",(long)[components hour]];
    }
    else if ([components minute]>0)
    {
        cell.lbl_postdt.text=[NSString stringWithFormat:@"%ld min ago",(long)[components minute]];
    }
    else{
        cell.lbl_postdt.text=[NSString stringWithFormat:@"%ld day ago",(long)[components day]];
    }
    
    
    //====================================================
    //cell.lbl_postdt.text = [NSString stringWithFormat:@"%@",[[Dict3 valueForKey:@"CommentedDate"] objectAtIndex:indexPath.row] ];
    cell.lbl_usernm.text = [NSString stringWithFormat:@"%@ %@",[[Dict3 valueForKey:@"FName"] objectAtIndex:indexPath.row],[[Dict3 valueForKey:@"LName"] objectAtIndex:indexPath.row]];
    
    
    //NSString *url=[_Dict1 valueForKey:@"ImageURL"][indexPath.row];
    //[cell.theImageView sd_setImageWithURL:[NSURL URLWithString:url]
    // placeholderImage:[UIImage imageNamed:@"Avatar"]];

    cell.lbl_comment.text =[NSString stringWithFormat:@"%@",[Dict3 valueForKey:@"Comments"][indexPath.row]];
    
    
    return cell;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

-(void)CallSendMSG
{
    [APP_DELEGATE showLoadingView:@""];
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"reply_to_id"];
    [AddPost setValue:_txt_comment.text forKey:@"comment"];
    [AddPost setValue:[_Dict2 valueForKey:@"CritiquesID"]  forKey:@"critique_id"];
    [AddPost setValue:[NSString stringWithFormat:@"%@",_critiID] forKey:@"critique_reply_id"];
    [AddPost setValue:_replyID forKey:@"user_id"];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    [manager POST:[NSString stringWithFormat:@"%@add_solution_reply",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        responseObject=[responseObject valueForKey:@"data"];
        NSUserDefaults *userD=[NSUserDefaults standardUserDefaults];
        
        Dict4=[[NSMutableDictionary alloc]init];
        [Dict4 setValue:[responseObject valueForKey:@"CommentedDate"] forKey:@"CommentedDate"];
        [Dict4 setValue:@"htps://indieglobe.blenzabi.com/uploads/user/" forKey:@"ImageURL"];
        [Dict4 setValue:[userD valueForKey:@"FName"] forKey:@"FName"];
        [Dict4 setValue:[userD valueForKey:@"LName"] forKey:@"LName"];
        [Dict4 setValue:[responseObject valueForKey:@"Comments"] forKey:@"Comments"];
        
        [Dict3 addObject:Dict4];
        
        [userD setObject:Dict3 forKey:@"arrcritiComment"];
        [userD setObject:@"2" forKey:@"critiComment"];
        
        [APP_DELEGATE hideLoadingView];
        
        _lbl_nodata.hidden=YES;
        
        [_tbl_view reloadData];
        
        
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
              [APP_DELEGATE hideLoadingView];
          }];
    
}





- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_SEND:(id)sender {
    if ((![_txt_comment.text isEqualToString:@""]))
    {
        [self CallSendMSG];
        _txt_comment.text=@"";
        [self.view endEditing:YES];
    }
}
@end
