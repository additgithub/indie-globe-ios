//
//  ReplayCritiques.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 4/23/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReplayCritiques : UIViewController


@property (retain , nonatomic)NSMutableDictionary *dict;
@property (retain , nonatomic)NSMutableDictionary *_mutDict;
@property (strong, nonatomic) IBOutlet UITextView *txt_repaly;
@property (strong, nonatomic) IBOutlet UIButton *btn_send;

@property (strong, nonatomic) IBOutlet UIView *vw_background;

- (IBAction)btn_SEND_A:(id)sender;

@end
