//
//  EventCell.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/1/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import "EventCell.h"

@implementation EventCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _btn_rsvp.layer.cornerRadius=_btn_rsvp.frame.size.height/2;
    _btn_scmi.layer.cornerRadius=_btn_scmi.frame.size.height/2;
    
    _img_date.layer.cornerRadius=5;
    
    _img_user_prf_img.clipsToBounds=YES;
    _img_user_prf_img.layer.cornerRadius=_img_user_prf_img.frame.size.height/2;
    
    _img_vw1.layer.cornerRadius=_img_vw1.layer.frame.size.height/2;
    _img_vw1.clipsToBounds=YES;
    
    _img_vw2.layer.cornerRadius=_img_vw2.layer.frame.size.height/2;
    _img_vw2.clipsToBounds=YES;

    [self.vw_shadow setShadow];
    
    
    
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
