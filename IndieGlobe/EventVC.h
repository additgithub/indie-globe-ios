//
//  EventVC.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/1/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopOverView.h"
#import "IGCMenu.h"
#import "DeshboardVC.h"
#import "ProfileVC.h"
#import "CritiquesVC.h"
#import "ClassifiedVC.h"
#import "PackageList.h"
#import "LearningVC.h"
#import "EventVC.h"
#import "TeamBuildVC.h"
#import "UIViewController+ENPopUp.h"
#import "MyEventVC.h"
#import "SettingVC.h"
#import "NewProfileVC.h"
#import "IndieVC.h"
#import "FundingVC.h"
#import "NewsVC.h"
#import "HttpWrapper.h"
#import "ApplicationConstant.h"
#import "NewHomeSearchVC.h"


@interface EventVC : UIViewController<UITableViewDataSource,UITableViewDelegate,IGCMenuDelegate,HttpWrapperDelegate,UICollectionViewDelegate,UICollectionViewDataSource>{

    UITextField *currentTextField;
    UIView *picker;
    NSMutableArray *arrimg;
    
    HttpWrapper* httpevent,*httpcountry,*httpstate,*httpcity;
    
    NSString *last_eventid;
    NSString *page_no;
    NSString *isSearch;
    
    NSIndexPath *index_event;
    NSString *btn_enable;
    
    //VAR
    int isCheck;
    NSMutableArray* country;
    IBOutlet UITextField *txt_country;
    NSString *countryid;
    
    NSMutableArray* state;
    IBOutlet UITextField *txt_state;
    NSString * stateid;
    
    NSMutableArray* arrcity;
    IBOutlet UITextField *txt_city;
    NSString * cityid;
    
    
    //RESPONCE DICT
    NSMutableArray *mutDict;
    


}
//Outlet
@property (strong, nonatomic) IBOutlet UIButton *btn_menu;
@property (strong, nonatomic) IBOutlet UITableView *tbl_event;
@property (weak, nonatomic) IBOutlet UILabel *lbl_notifi_count;


@property (strong, nonatomic) IBOutlet UITextField *txt_search;
@property (strong, nonatomic) IBOutlet UIButton *btn_search;
@property (strong, nonatomic) IBOutlet UIButton *btn_addevent;
@property (strong, nonatomic) IBOutlet UIButton *btn_new_menu;
@property (strong, nonatomic) IBOutlet UICollectionView *collection_view;

@property (strong, nonatomic) IBOutlet UIImageView *img_colle_back;


@property (strong, nonatomic) IBOutlet UIButton *btn_country;
@property (strong, nonatomic) IBOutlet UIButton *btn_state;
@property (strong, nonatomic) IBOutlet UIButton *btn_city;



//Action
- (IBAction)btncity:(id)sender;
- (IBAction)btn_HOME:(id)sender;
- (IBAction)btn_SEARCH:(id)sender;
- (IBAction)btn_ADD_EVENT:(id)sender;
- (IBAction)btn_NEWMENU:(id)sender;
- (IBAction)btn_SCOUNTRY:(id)sender;
- (IBAction)btn_SSTATE:(id)sender;
- (IBAction)btn_NOTIFY:(id)sender;





-(void)Popvalueselected:(NSString*)strValue;
-(void)Popvalveselected:(NSString*)strValve;
-(void)dismissPopUpViewController;

@end
