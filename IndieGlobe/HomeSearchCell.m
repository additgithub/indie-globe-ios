//
//  HomeSearchCell.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 6/28/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "HomeSearchCell.h"

@implementation HomeSearchCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _img_user.clipsToBounds=YES;
    _img_user.layer.cornerRadius=_img_user.frame.size.height/2;
    _btn_follow.layer.cornerRadius=_btn_follow.frame.size.height/2;
        
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
