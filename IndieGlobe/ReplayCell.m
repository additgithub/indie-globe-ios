//
//  ReplayCell.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 6/8/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "ReplayCell.h"

@implementation ReplayCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _img_user.clipsToBounds=YES;
    _img_user.layer.cornerRadius=_img_user.frame.size.height/2;
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
