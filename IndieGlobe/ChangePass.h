//
//  ChangePass.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 1/4/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextFieldValidator.h"
#import "HttpWrapper.h"
#import "ApplicationConstant.h"

@interface ChangePass : UIViewController<HttpWrapperDelegate>
{
    HttpWrapper* httpchangepass;
}

//Outlet
@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_currpass;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_newpass;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_confpass;
@property (strong, nonatomic) IBOutlet UIButton *btn_save;
@property (strong, nonatomic) IBOutlet UIButton *btn_cancle;





//Action
- (IBAction)btn_SAVE_ACTION:(id)sender;
- (IBAction)btn_CANCLE_ACTION:(id)sender;
- (IBAction)btn_BACK:(id)sender;




@end
