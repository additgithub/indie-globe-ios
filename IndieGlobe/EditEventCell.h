//
//  EditEventCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/23/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditEventCell : UICollectionViewCell<UICollectionViewDelegate,UICollectionViewDataSource>


//Button
@property (strong, nonatomic) IBOutlet UIButton *Btn_Close;

//ImageView
@property (strong, nonatomic) IBOutlet UIImageView *Img_Photos;

//Label
@property (strong, nonatomic) IBOutlet UILabel *Lbl_MaxMsg;

@end
