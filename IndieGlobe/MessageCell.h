//
//  MessageCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/26/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *img_profile_image;
@property (strong, nonatomic) IBOutlet UILabel *lbl_sender_name;
@property (strong, nonatomic) IBOutlet UILabel *lbl_sender_msg;
@property (strong, nonatomic) IBOutlet UILabel *lbl_dt_time;



@end
