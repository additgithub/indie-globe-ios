//
//  HnewsCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 2/3/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShadowView.h"


@interface HnewsCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *img_prof;
@property (strong, nonatomic) IBOutlet UILabel *lbl_usernm;
@property (strong, nonatomic) IBOutlet UIImageView *img_image;
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;
@property (strong, nonatomic) IBOutlet UILabel *lbl_postdt;
@property (strong, nonatomic) IBOutlet UIButton *btn_play_v;
@property (weak, nonatomic) IBOutlet ShadowView *shadow_vw;



@end
