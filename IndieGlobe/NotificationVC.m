//
//  NotificationVC.m
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 4/23/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "NotificationVC.h"
#import "UIColor+CL.h"
#import "AppDelegate.h"
#import "ApplicationData.h"
#import "ApplicationConstant.h"
#import "HttpWrapper.h"
#import "PDVC.h"


@interface NotificationVC ()

@end

@implementation NotificationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _txt_notif.delegate=self;
    _txt_notif.text = @"Enter your message";
    _txt_notif.textColor = [UIColor lightGrayColor];
    _txt_notif.layer.borderWidth=1.0;
    _txt_notif.layer.cornerRadius=10;
    _txt_notif.layer.borderColor=[UIColor colorWithHex:0x01B5A9].CGColor;
    
    _btn_send.layer.cornerRadius=_btn_send.frame.size.height/2;
    
    NSLog(@"USER ID %@",_strUserID);
    
    _vw_background.layer.cornerRadius=5;
    
    
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [_vw_background addGestureRecognizer:singleFingerTap];
    
    
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if([textView.text isEqual: @"Enter your message"])
    {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    return YES;
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    
    /* ProfileVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileVC"];
     [self.navigationController pushViewController:next animated:NO];*/
    
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
///Users/mymac/Documents/Project/IndieGlobe/IndieGlobe.xcodeproj
-(void)CallSendMSG
{
    [APP_DELEGATE showLoadingView:@""];
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [AddPost setValue:[standardUserDefaults valueForKey:@"UserID"] forKey:@"user_id"];
    [AddPost setValue:_txt_notif.text forKey:@"message"];
    [AddPost setValue:_strUserID forKey:@"receiver_ids"];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSLog(@"---%@",AddPost);
    [manager POST:[NSString stringWithFormat:@"%@send_team_member_notification",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        [APP_DELEGATE hideLoadingView];
        
        NSString *msg=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"message"]];
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        alert.delegate = self;
        [alert show];
         [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil];
         [self.navigationController popViewControllerAnimated:YES];
        [self dismissViewControllerAnimated:NO completion:nil];
//        UIAlertController * alert = [UIAlertController
//                                     alertControllerWithTitle:@""
//                                     message:msg
//                                     preferredStyle:UIAlertControllerStyleAlert];
//
//        //Add Buttons
//
//        UIAlertAction* yesButton = [UIAlertAction
//                                    actionWithTitle:@"Ok"
//                                    style:UIAlertActionStyleDefault
//                                    handler:^(UIAlertAction * action) {
//                                        //Handle your yes please button action here
//
//
//
////
//
//                                         PDVC *homescreen = [self.storyboard instantiateViewControllerWithIdentifier:@"PDVC"];
//                                        [self.navigationController popToViewController:homescreen animated:YES];
//                                         [self dismissViewControllerAnimated:NO completion:nil];
//                                      //  [self.navigationController pushViewController:homescreen animated:NO];
//                                      //  [self presentModalViewController:homescreen animated:YES];
//                                    }];
//
//        //Add your buttons to alert controller
//
//        [alert addAction:yesButton];
//        [self presentViewController:alert animated:YES completion:nil];
        
        
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
              [APP_DELEGATE hideLoadingView];
          }];
    
}



- (IBAction)btn_SEND_A:(id)sender {
    if ((![_txt_notif.text isEqualToString:@"Enter your message"]) && (![_txt_notif.text isEqualToString:@""]))
    {
        [self CallSendMSG];
        _txt_notif.text=@"";
        [self performSelector:@selector(handleSingleTap:) withObject:nil afterDelay:0.5];
    }
    
}
@end
