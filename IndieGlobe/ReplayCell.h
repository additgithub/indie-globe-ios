//
//  ReplayCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 6/8/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReplayCell : UITableViewCell


//Outlet
@property (strong, nonatomic) IBOutlet UIImageView *img_user;
@property (strong, nonatomic) IBOutlet UILabel *lbl_comment;
@property (strong, nonatomic) IBOutlet UILabel *lbl_usernm;
@property (strong, nonatomic) IBOutlet UILabel *lbl_postdt;



//Action

@end
