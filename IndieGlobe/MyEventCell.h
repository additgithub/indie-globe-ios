//
//  MyEventCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/7/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShadowView.h"


@interface MyEventCell : UITableViewCell

//Outlet

@property (strong, nonatomic) IBOutlet UIButton *btn_edit_cell;
@property (strong, nonatomic) IBOutlet UIImageView *img_profile;
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;
@property (strong, nonatomic) IBOutlet UILabel *lbl_rsvp;
@property (strong, nonatomic) IBOutlet UILabel *lbl_postdt;
@property (strong, nonatomic) IBOutlet UILabel *lbl_address;
@property (strong, nonatomic) IBOutlet UILabel *lbl_short_m;
@property (strong, nonatomic) IBOutlet UILabel *lbl_short_d;
@property (strong, nonatomic) IBOutlet UIImageView *img_user_prof;
@property (strong, nonatomic) IBOutlet UILabel *lbl_usernm;

@property (weak, nonatomic) IBOutlet UIButton *btn_prof_vw;

@property (weak, nonatomic) IBOutlet ShadowView *vw_shadow;

@property (weak, nonatomic) IBOutlet UIScrollView *scroll_event_img;


@property (weak, nonatomic) IBOutlet UILabel *lbl_like;
@property (weak, nonatomic) IBOutlet UILabel *lbl_dislike;
@property (weak, nonatomic) IBOutlet UILabel *lbl_comment;

@property (weak, nonatomic) IBOutlet UIButton *btn_like;
@property (weak, nonatomic) IBOutlet UIButton *btn_dislike;
@property (weak, nonatomic) IBOutlet UIButton *btn_comment;

@property (weak, nonatomic) IBOutlet UIView *vw_1;
@property (weak, nonatomic) IBOutlet UIImageView *img_vw1;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title_vw1;
@property (weak, nonatomic) IBOutlet UILabel *lbl_desc_vw1;

@property (weak, nonatomic) IBOutlet UIView *vw_2;
@property (weak, nonatomic) IBOutlet UIImageView *img_vw2;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title_vw2;
@property (weak, nonatomic) IBOutlet UILabel *lbl_desc_vw2;




//Action



@end
