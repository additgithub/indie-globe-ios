#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "HCYoutubeParser.h"
#import "YKDirectVideo.h"
#import "YKMediaPlayerKit.h"
#import "YKUnKnownVideo.h"
#import "YKVideo.h"
#import "YKVimeoVideo.h"
#import "YKYouTubeVideo.h"

FOUNDATION_EXPORT double YKMediaPlayerKitVersionNumber;
FOUNDATION_EXPORT const unsigned char YKMediaPlayerKitVersionString[];

